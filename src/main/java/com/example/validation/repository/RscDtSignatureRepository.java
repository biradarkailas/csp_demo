package com.example.validation.repository;

import com.example.validation.domain.RscDtSignature;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtSignatureRepository extends JpaRepository<RscDtSignature, Long> {
}
