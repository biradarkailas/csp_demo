package com.example.validation.repository;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartyNameRepo extends JpaRepository<PartyName, Long> {
    public PartyName findByParty(Party party);
    public PartyName findByOrganizationName(String organizationName);
}
