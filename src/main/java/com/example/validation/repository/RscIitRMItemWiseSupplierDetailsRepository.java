package com.example.validation.repository;

import com.example.validation.domain.RscIitRMItemWiseSupplierDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIitRMItemWiseSupplierDetailsRepository extends JpaRepository<RscIitRMItemWiseSupplierDetails, Long> {
    RscIitRMItemWiseSupplierDetails findOneByRscItItemWiseSupplierId(Long itemWiseSupplierId);
}