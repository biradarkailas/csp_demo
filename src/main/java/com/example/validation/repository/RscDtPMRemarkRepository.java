package com.example.validation.repository;

import com.example.validation.domain.RscDtPMRemark;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface RscDtPMRemarkRepository extends JpaRepository<RscDtPMRemark, Long> {
    Optional<RscDtPMRemark> findByAliasName(String aliasName);
}
