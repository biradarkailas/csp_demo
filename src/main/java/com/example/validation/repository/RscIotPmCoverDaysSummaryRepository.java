package com.example.validation.repository;

import com.example.validation.domain.RscIotPmCoverDaysSummary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscIotPmCoverDaysSummaryRepository extends JpaRepository<RscIotPmCoverDaysSummary, Long> {
    RscIotPmCoverDaysSummary findAllByRscMtPPheaderId(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long rscMtPPheaderId);
}
