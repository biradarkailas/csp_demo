package com.example.validation.repository;

import com.example.validation.domain.PartyAddressDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartyAddressDetailsRepo extends JpaRepository<PartyAddressDetails, Long> {
}
