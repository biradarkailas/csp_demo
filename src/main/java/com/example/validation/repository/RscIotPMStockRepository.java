package com.example.validation.repository;

import com.example.validation.domain.RscDtStockType;
import com.example.validation.domain.RscIotPMStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIotPMStockRepository extends JpaRepository<RscIotPMStock, Long> {

    List<RscIotPMStock> findAllByRscMtItemId(Long rscMtItemId);

    List<RscIotPMStock> findAllByRscMtItemIdAndRscDtStockTypeIn(Long rscMtItemId, List<RscDtStockType> stockTypes);

    RscIotPMStock findByRscMtItemId(Long mtItemId);

    List<RscIotPMStock> findAllRscItPmStockByStockDate(LocalDate mpsDate);

    @Query(
            value = "SELECT * FROM csp_loreal.rsc_iot_pm_stock where stock_type_id in (SELECT id FROM csp_loreal.rsc_dt_stock_type where tag like  'PM_MES' or tag like 'PM_Rej')",
            nativeQuery = true)
    List<RscIotPMStock> findAllByStockType();

    List<RscIotPMStock> findAllByRscDtStockTypeTagIn(List<String> stockTypeList);

    void deleteAllByStockDate(LocalDate stockDate);
}