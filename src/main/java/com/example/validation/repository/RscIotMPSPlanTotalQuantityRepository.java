package com.example.validation.repository;

import com.example.validation.domain.RscIotMPSPlanTotalQuantity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotMPSPlanTotalQuantityRepository extends JpaRepository<RscIotMPSPlanTotalQuantity, Long> {
    long countByRscMtPPheaderId(Long ppHeaderId);

    RscIotMPSPlanTotalQuantity findByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotMPSPlanTotalQuantity> findByMpsPlanNameAndMpsPlanSequenceIsLessThanEqual(String mpsPlanName, int mpsPlanSequence);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}