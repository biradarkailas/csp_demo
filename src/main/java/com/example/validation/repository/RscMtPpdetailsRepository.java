package com.example.validation.repository;

import com.example.validation.domain.RscMtPPdetails;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscMtPpdetailsRepository extends JpaRepository<RscMtPPdetails, Long>, JpaSpecificationExecutor<RscMtPPdetails> {
    List<RscMtPPdetails> findAllByRscMtPPheaderId(Long ppheaderId);

    RscMtPPdetails findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long fgItemId);

    @Query(
            value = "SELECT DISTINCT rsc_dt_line_id from csp_loreal.rsc_mt_ppdetails where rsc_mt_ppheader_id=?1",
            nativeQuery = true)
    List<Long> findAllUniqueRscDtLines(Long ppHeaderId);

    @Query(
            value = "SELECT DISTINCT rsc_dt_skid_id from csp_loreal.rsc_mt_ppdetails where rsc_mt_ppheader_id=?1",
            nativeQuery = true)
    List<Long> findAllUniqueRscDtSkids(Long ppHeaderId);

    List<RscMtPPdetails> findByRscMtPPheaderIdAndRscMtItemIdIn(Long ppHeaderId, List<Long> fgItemId);

    List<RscMtPPdetails> findAllByRscDtSkidsIdAndRscMtPPheaderId(Long rscDtSkidsId, Long ppHeaderId);

    List<RscMtPPdetails> findAllByRscDtLinesIdAndRscMtPPheaderId(Long rscDtLinesId, Long ppHeaderId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtDivisionIdAndRscDtSignatureIdAndRscDtBrandId(Long ppHeaderId, Long divisionId, Long signatureId, Long brandId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtDivisionIdAndRscDtSignatureId(Long ppHeaderId, Long divisionId, Long signatureId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtDivisionIdAndRscDtBrandId(Long ppHeaderId, Long divisionId, Long brandId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtSignatureIdAndRscDtBrandId(Long ppHeaderId, Long signatureId, Long brandId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtDivisionId(Long ppHeaderId, Long divisionId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtSignatureId(Long ppHeaderId, Long signatureId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtBrandId(Long ppHeaderId, Long brandId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtMaterialCategoryIdAndRscDtMaterialSubCategoryId(Long ppHeaderId, Long categoryId, Long subCategoryId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtMaterialCategoryId(Long ppHeaderId, Long categoryId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtMaterialSubCategoryId(Long ppHeaderId, Long subCategoryId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtLinesIdAndRscDtSkidsId(Long ppHeaderId, Long rscDtLinesId, Long rscDtSkidsId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtLinesId(Long ppHeaderId, Long rscDtLinesId);

    List<RscMtPPdetails> findAllByRscMtPPheaderIdAndRscDtSkidsId(Long ppHeaderId, Long rscDtSkidsId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
