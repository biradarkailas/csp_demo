package com.example.validation.repository;

import com.example.validation.domain.RscIotFGStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIotFGStockRepository extends JpaRepository<RscIotFGStock, Long> {
    List<RscIotFGStock> findByRscDtStockTypeTag(String stockTypeTag);

    void deleteAllByStockDate(LocalDate stockDate);
}