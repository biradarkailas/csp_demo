package com.example.validation.repository;

import com.example.validation.domain.RscDtMap;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtMapRepository extends JpaRepository<RscDtMap,Long> {
}
