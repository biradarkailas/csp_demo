package com.example.validation.repository;

import com.example.validation.domain.RscItBulkGrossConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItBulkGrossConsumptionRepository extends JpaRepository<RscItBulkGrossConsumption, Long> {
    RscItBulkGrossConsumption findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bulkItemId);

    List<RscItBulkGrossConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}