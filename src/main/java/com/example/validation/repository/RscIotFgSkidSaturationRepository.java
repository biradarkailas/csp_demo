package com.example.validation.repository;

import com.example.validation.domain.RscIotFgSkidSaturation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotFgSkidSaturationRepository extends JpaRepository<RscIotFgSkidSaturation, Long> {
    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFgSkidSaturation> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotFgSkidSaturation findByRscMtPPheaderIdAndRscDtSkidsId(Long ppHeaderId, Long rscDtSkidId);

    List<RscIotFgSkidSaturation> findAllByRscDtSkidsIdAndRscMtPPheaderId(Long rscDtSkidId, Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFgSkidSaturation> findAllByRscMtPPheaderRscDtTagName(String tagName);
}