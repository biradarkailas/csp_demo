package com.example.validation.repository;

import com.example.validation.domain.RscItPMGrossConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItPMGrossConsumptionRepository extends JpaRepository<RscItPMGrossConsumption, Long> {
    List<RscItPMGrossConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscItPMGrossConsumption> findAllByRscMtPPheaderIdAndRscMtItemRscDtItemCategoryDescription(Long ppHeaderId, String description);

    RscItPMGrossConsumption findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}