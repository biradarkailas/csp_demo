package com.example.validation.repository;

import com.example.validation.domain.RscItRMLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItRMLogicalConsumptionRepository extends JpaRepository<RscItRMLogicalConsumption, Long> {
    List<RscItRMLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(Long ppHeaderId, Long fgMtItemId);

    List<RscItRMLogicalConsumption> findAllByRscMtItemChildRscDtItemCategoryDescription(String itemDescription);

    List<RscItRMLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}