package com.example.validation.repository;

import com.example.validation.domain.RscDtMaterialSubCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtMaterialSubCategoryRepository extends JpaRepository<RscDtMaterialSubCategory, Long> {
}
