package com.example.validation.repository;

import com.example.validation.domain.RscIotPMPurchasePlanCalculations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotPmSupplyCalculationsRepository extends JpaRepository<RscIotPMPurchasePlanCalculations, Long> {
    List<RscIotPMPurchasePlanCalculations> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotPMPurchasePlanCalculations findByRscMtItemIdAndRscMtPPheaderId(Long rscMtItemId, Long ppHeaderId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}