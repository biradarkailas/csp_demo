package com.example.validation.repository;

import com.example.validation.domain.RscItPMLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItPMLogicalConsumptionRepository extends JpaRepository<RscItPMLogicalConsumption, Long> {
    List<RscItPMLogicalConsumption> findAllByRscMtPPheaderId(Long ppheaderId);

    List<RscItPMLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(Long ppHeaderId, Long fgMtItemId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}