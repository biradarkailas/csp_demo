package com.example.validation.repository;

import com.example.validation.domain.RscDtMoq;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtMoqRepository extends JpaRepository<RscDtMoq,Long> {
}
