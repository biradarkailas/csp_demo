package com.example.validation.repository;

import com.example.validation.domain.RscIotRmLatestSlobReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotRmLatestSlobReasonRepository extends JpaRepository<RscIotRmLatestSlobReason, Long> {
}
