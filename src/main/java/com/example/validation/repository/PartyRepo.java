package com.example.validation.repository;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyTypeOrganization;
import com.example.validation.domain.PartyTypeUser;
import com.example.validation.util.enums.PartyType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartyRepo extends JpaRepository<Party, Long> {
    public List<Party> findAllByType(PartyType partyType);

    public Party findByPartyTypeOrganization(PartyTypeOrganization partyTypeOrganization);

    public Party findByPartyTypeUser(PartyTypeUser partyTypeUser);
}
