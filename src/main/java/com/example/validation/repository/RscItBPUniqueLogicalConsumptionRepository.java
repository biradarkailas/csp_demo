package com.example.validation.repository;

import com.example.validation.domain.RscItBPLogicalConsumption;
import com.example.validation.domain.RscItBPUniqueLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItBPUniqueLogicalConsumptionRepository extends JpaRepository<RscItBPUniqueLogicalConsumption, Long> {
    List<RscItBPUniqueLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemParentId(Long ppHeaderId, Long fgMtItemId);

    List<RscItBPUniqueLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}