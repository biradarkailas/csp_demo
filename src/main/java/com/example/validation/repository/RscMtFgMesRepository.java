package com.example.validation.repository;

import com.example.validation.domain.RscMtFgMes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface RscMtFgMesRepository extends JpaRepository<RscMtFgMes, Long> {
    void deleteAllByStockDate(LocalDate stockDate);
}
