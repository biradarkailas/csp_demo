package com.example.validation.repository;

import com.example.validation.domain.RscItSupplierMould;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscItSupplierMouldRepository extends JpaRepository<RscItSupplierMould, Long>{
}
