package com.example.validation.repository;

import com.example.validation.domain.RscIotLatestTotalMouldSaturationSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotLatestTotalMouldSaturationSummaryRepository extends JpaRepository<RscIotLatestTotalMouldSaturationSummary, Long> {
    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndThreeMonthsAverageGreaterThanEqual(Long ppHeaderId, Double minValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndThreeMonthsAverageGreaterThanEqualAndThreeMonthsAverageLessThanEqual(Long ppHeaderId, Double minValue, Double maxValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndThreeMonthsAverageLessThan(Long ppHeaderId, Double maxValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndSixMonthsAverageGreaterThanEqual(Long ppHeaderId, Double minValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndSixMonthsAverageGreaterThanEqualAndSixMonthsAverageLessThanEqual(Long ppHeaderId, Double minValue, Double maxValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndSixMonthsAverageLessThan(Long ppHeaderId, Double maxValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndTwelveMonthsAverageGreaterThanEqual(Long ppHeaderId, Double minValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndTwelveMonthsAverageGreaterThanEqualAndTwelveMonthsAverageLessThanEqual(Long ppHeaderId, Double minValue, Double maxValue);

    List<RscIotLatestTotalMouldSaturationSummary> findAllByRscMtPPheaderIdAndTwelveMonthsAverageLessThan(Long ppHeaderId, Double maxValue);

    RscIotLatestTotalMouldSaturationSummary findByRscMtPPheaderIdAndRscIotLatestTotalMouldSaturationId(Long ppHeaderId,Long mouldId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
