package com.example.validation.repository;

import com.example.validation.domain.RscDtTransportMode;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtTransportModeRepository extends JpaRepository<RscDtTransportMode,Long> {
}
