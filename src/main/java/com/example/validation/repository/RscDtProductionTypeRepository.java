package com.example.validation.repository;

import com.example.validation.domain.RscDtProductionType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtProductionTypeRepository extends JpaRepository<RscDtProductionType,Long> {
}
