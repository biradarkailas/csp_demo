package com.example.validation.repository;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotFGTypeSlobSummaryRepository extends JpaRepository<RscIotFGTypeSlobSummary, Long> {
    Long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFGTypeSlobSummary> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotFGTypeSlobSummary findByRscMtPPheaderIdAndRscDtFGTypeId(Long ppHeaderId, Long fgTypeId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}