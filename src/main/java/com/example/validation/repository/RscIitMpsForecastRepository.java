package com.example.validation.repository;

import com.example.validation.domain.RscIitMpsForecast;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIitMpsForecastRepository extends JpaRepository<RscIitMpsForecast, Long> {
    RscIitMpsForecast findByRscMtItemIdAndRscMtPPheaderId(Long fgItemId, Long ppHeaderId);

    List<RscIitMpsForecast> findByEightDigitCodeAndRscMtPPheaderId(String eightDigitCode, Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}