package com.example.validation.repository;

import com.example.validation.domain.RscItBulkLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItBulkLogicalConsumptionRepository extends JpaRepository<RscItBulkLogicalConsumption, Long> {
    List<RscItBulkLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(Long ppHeaderId, Long fgMtItemId);

    List<RscItBulkLogicalConsumption> findAllByRscMtItemChildRscDtItemCategoryDescription(String itemDescription);

    List<RscItBulkLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscItBulkLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemChildId(Long ppHeaderId, Long itemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
