package com.example.validation.repository;

import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.dto.RscIotOriginalMouldSaturationDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotOriginalMouldSaturationRepository extends JpaRepository<RscIotOriginalMouldSaturation, Long> {

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}