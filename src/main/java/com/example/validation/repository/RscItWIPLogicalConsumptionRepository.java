package com.example.validation.repository;

import com.example.validation.domain.RscItWIPLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItWIPLogicalConsumptionRepository extends JpaRepository<RscItWIPLogicalConsumption, Long> {
    List<RscItWIPLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemParentId(Long ppHeaderId, Long fgMtItemId);

    List<RscItWIPLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscItWIPLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemChildId(Long ppHeaderId, Long wipItemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}