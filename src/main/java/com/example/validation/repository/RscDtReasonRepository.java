package com.example.validation.repository;

import com.example.validation.domain.RscDtReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtReasonRepository extends JpaRepository<RscDtReason,Long> {
    RscDtReason findByReason(String reason);
}
