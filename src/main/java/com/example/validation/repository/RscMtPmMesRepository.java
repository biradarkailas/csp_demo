package com.example.validation.repository;

import com.example.validation.domain.RscMtPmMes;
import com.example.validation.dto.PmMesStockPaginationDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtPmMesRepository extends JpaRepository<RscMtPmMes, Long> {
    List<RscMtPmMes> findByRscMtItemId(Long rscMtItemId);

    List<RscMtPmMes> findAllByStockDate(LocalDate mpsDate);

    void deleteAllByStockDate(LocalDate stockDate);

    @Query(name = "Pm_Mes_Stock", nativeQuery = true)
    List<PmMesStockPaginationDto> findPmMesStockByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Pm_Mes_Stock", nativeQuery = true)
    List<PmMesStockPaginationDto> findPmMesStockByMpsDateAndStockType(String mpsDate, String stockType);

}