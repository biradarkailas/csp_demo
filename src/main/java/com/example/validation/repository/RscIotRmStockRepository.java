package com.example.validation.repository;

import com.example.validation.domain.RscIotRmStock;
import com.example.validation.dto.*;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIotRmStockRepository extends JpaRepository<RscIotRmStock, Long> {
    List<RscIotRmStock> findAllByRscMtItemId(Long rscMtItemId);

    List<RscIotRmStock> findByRscMtItemId(Long rscMtItemId);

    @Query(value = "SELECT * FROM csp_loreal.rsc_iot_rm_stock where stock_type_id in (SELECT id FROM csp_loreal.rsc_dt_stock_type where tag like  'RM_MES' or tag like 'RM_EXPIRED')", nativeQuery = true)
    List<RscIotRmStock> findAllRmStocksByStockTypeTag();

    RscIotRmStock findByRscMtItemIdAndStockDate(Long rscMtItemId, LocalDate stockDate);

    List<RscIotRmStock> findAllByRscDtStockTypeTagIn(List<String> stockTypeList);

    void deleteAllByStockDate(LocalDate stockDate);

  /*  @Query(value = "select csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date,rsc_mt_item_id,csp_loreal.rsc_iot_rm_stock.quantity,csp_loreal.rsc_mt_rm_mes_rejected_stock.quantity lot_wise_stock,lot_number,location,stock_status from csp_loreal.rsc_iot_rm_stock, csp_loreal.rsc_mt_rm_mes_rejected_stock  where YEAR(csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date)=YEAR(?1) and month(csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date)=month(?1)  and csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date=csp_loreal.rsc_iot_rm_stock.stock_date and rsc_mt_item_id=rm_item_id and stock_type_id in(SELECT id FROM csp_loreal.rsc_dt_stock_type where tag=?2)", nativeQuery = true)
    List findByMpsDateAndStockType(LocalDate mpsDate, String stockType, Pageable var1);

    @Query(value = "select csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date as stockDate,rsc_mt_item_id,csp_loreal.rsc_iot_rm_stock.quantity as quantity,csp_loreal.rsc_mt_rm_mes_rejected_stock.quantity as lotWiseStock,lot_number,location,stock_status from csp_loreal.rsc_iot_rm_stock, csp_loreal.rsc_mt_rm_mes_rejected_stock  where YEAR(csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date)=YEAR(?1) and month(csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date)=month(?1)  and csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date=csp_loreal.rsc_iot_rm_stock.stock_date and rsc_mt_item_id=rm_item_id and stock_type_id in(SELECT id FROM csp_loreal.rsc_dt_stock_type where tag=?2)", nativeQuery = true)
    List<StockPaginationDto> findByMpsDateAndStockType(LocalDate mpsDate, String stockType);*/

    @Query(name = "Mes_Rejected_Stock", nativeQuery = true)
    List<StockPaginationDto> findByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Bulk_Op_Rm_Stock", nativeQuery = true)
    List<RmBulkOpRmStockPaginationDto> findRmBulkOpRmByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Mes_Stock", nativeQuery = true)
    List<RmMesStockPaginationDto> findMesByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);


    @Query(name = "Rm_Receipts_Stock", nativeQuery = true)
    List<ReceiptStockPaginationDto> findRtdByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);


    @Query(name = "Rm_Transfer_Stock", nativeQuery = true)
    List<RmTransferStockPaginationDTO> findRmTransferStockByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Rm_Rejection_Stock", nativeQuery = true)
    List<RmRejectionStockPaginationDTO> findRmRejectionStockByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Bulk_Op_Rm_Stock", nativeQuery = true)
    List<RmBulkOpRmStockPaginationDto> findRmBulkOpRmByMpsDateAndStockType(String mpsDate, String stockType);

    @Query(name = "Mes_Stock", nativeQuery = true)
    List<RmMesStockPaginationDto> findMesByMpsDateAndStockType(String mpsDate, String stockType);

    @Query(name = "Mes_Rejected_Stock", nativeQuery = true)
    List<StockPaginationDto> findByMpsDateAndStockType(String mpsDate, String stockType);


    @Query(name = "Rm_Receipts_Stock", nativeQuery = true)
    List<ReceiptStockPaginationDto> findRtdByMpsDateAndStockType(String mpsDate, String stockType);


    @Query(name = "Rm_Transfer_Stock", nativeQuery = true)
    List<RmTransferStockPaginationDTO> findRmTransferStockByMpsDateAndStockType(String mpsDate, String stockType);

    @Query(name = "Rm_Rejection_Stock", nativeQuery = true)
    List<RmRejectionStockPaginationDTO> findRmRejectionStockByMpsDateAndStockType(String mpsDate, String stockType);

    //    Mes New Api with optimization
    List<RscIotRmStock> findAllByStockDateBetweenAndRscDtStockTypeTag(LocalDate startStockDate, LocalDate endStockDate1, String stockTag);
}
