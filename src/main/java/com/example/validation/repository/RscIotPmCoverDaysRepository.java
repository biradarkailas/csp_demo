package com.example.validation.repository;

import com.example.validation.domain.RscIotPmCoverDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotPmCoverDaysRepository extends JpaRepository<RscIotPmCoverDays, Long> {
    List<RscIotPmCoverDays> findAllByRscMtPPheaderId(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotPmCoverDays> findAllByRscMtPPheaderIdAndRateIsNull(Long rscMtPPheaderId);

    void deleteAllByRscMtPPheaderId(Long rscMtPPheaderId);
}
