package com.example.validation.repository;

import com.example.validation.domain.RscDtItemCode;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtItemCodeRepository extends JpaRepository<RscDtItemCode,Long> {
}
