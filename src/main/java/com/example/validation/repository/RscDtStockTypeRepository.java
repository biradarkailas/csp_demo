package com.example.validation.repository;

import com.example.validation.domain.RscDtStockType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Set;

public interface RscDtStockTypeRepository extends JpaRepository<RscDtStockType, Long> {
    RscDtStockType findOneByTag(String aliasName);

    RscDtStockType findOneById(Long stockTypeId);

    List<RscDtStockType> findByTagIn(Set<String> tags);

}
