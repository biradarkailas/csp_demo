package com.example.validation.repository;

import com.example.validation.domain.RscItBPGrossConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItBPGrossConsumptionRepository extends JpaRepository<RscItBPGrossConsumption, Long> {
    RscItBPGrossConsumption findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId);

    List<RscItBPGrossConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}