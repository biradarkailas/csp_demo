package com.example.validation.repository;

import com.example.validation.domain.RscIotPMPurchasePlanCoverDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotPMPurchasePlanCoverDaysRepository extends JpaRepository<RscIotPMPurchasePlanCoverDays, Long> {
    Long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotPMPurchasePlanCoverDays> findAllByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}