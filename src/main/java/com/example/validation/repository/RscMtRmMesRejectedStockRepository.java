package com.example.validation.repository;

import com.example.validation.domain.RscMtRmMesRejectedStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtRmMesRejectedStockRepository extends JpaRepository<RscMtRmMesRejectedStock, Long> {
    void deleteAllByStockDate(LocalDate stockDate);

    List<RscMtRmMesRejectedStock> findAllByRscMtItemId(Long rscMtItemId);
}
