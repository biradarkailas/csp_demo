package com.example.validation.repository;

import com.example.validation.domain.RscIitIdpStatus;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Set;

public interface RscIitIdpStatusRepository extends JpaRepository<RscIitIdpStatus, Long> {
    List<RscIitIdpStatus> findByMpsName(String mpsName);

    List<RscIitIdpStatus> findByMpsNameAndIsActiveFalse(String mpsName);

    List<RscIitIdpStatus> findByMpsNameAndMpsDate(String mpsName, LocalDate mpsDate);

    List<RscIitIdpStatus> findByMpsDate(LocalDate mpsDate);

    List<RscIitIdpStatus> findAll();

    // List<RscIitIdpStatus> saveAll(List<RscIitIdpStatus> rscIitIdpStatuses);
    @Query(value = "SELECT max(mpsDate) from RscIitIdpStatus")
    LocalDate getMaxMpsDate();

    @Query(value = "SELECT distinct mpsName from RscIitIdpStatus where uploadStatus = 'PENDING' or validationStatus = 'PENDING' or resourceStatus = 'PENDING'")
    List<String> getPendingMpsPlanNames();

    @Query(value = "SELECT distinct mpsName from RscIitIdpStatus")
    List<String> getAllMpsPlanNames();

    @Query(value = "SELECT id from RscIitIdpStatus where mpsName=mps_name")
    Set<Long> getIdsByPlanName(@Param("mps_name") String mpsName);

    /*@Query(value = "SELECT mpsMonth from RscIitIdpStatus")
    Set<String> getDistinctMonths();*/

    @Transactional
    void deleteRscIitIdpStatusByIdIn(Set<Long> ids);

    RscIitIdpStatus findByMpsNameAndRscDtFileNameFileNameAndRscDtFileNameExtension(String mpsName, String fileName, String extension);

    List<RscIitIdpStatus> findByIsActive(Boolean isActive);

    List<RscIitIdpStatus> findByMpsNameAndUploadStatus(String mpsName, Boolean uploadStatus);

    List<RscIitIdpStatus> findByMpsNameAndIsRequiredAndRscDtFileNameRscDtFolderNameFolderName(String mpsName, Boolean isRequired, String folderName);

    List<RscIitIdpStatus> findByMpsNameAndIsRequired(String mpsName, Boolean isRequired);

    RscIitIdpStatus findByMpsNameAndRscDtFileNameFileName(String mpsName, String fileName);

    void deleteAllByMpsDateBetween(LocalDate startDate, LocalDate endDate);

    List<RscIitIdpStatus> findByMpsNameAndIsActiveTrue(String mpsName);
}