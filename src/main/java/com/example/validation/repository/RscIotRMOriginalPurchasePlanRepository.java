package com.example.validation.repository;

import com.example.validation.domain.RscIotRMOriginalPurchasePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotRMOriginalPurchasePlanRepository extends JpaRepository<RscIotRMOriginalPurchasePlan, Long> {
    List<RscIotRMOriginalPurchasePlan> findByRscMtPPheaderId(Long ppHeaderId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
