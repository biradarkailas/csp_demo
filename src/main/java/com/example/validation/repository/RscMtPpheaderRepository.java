package com.example.validation.repository;

import com.example.validation.domain.RscMtPPheader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface RscMtPpheaderRepository extends JpaRepository<RscMtPPheader, Long> {
    List<RscMtPPheader> findAllByActive(boolean active);

    RscMtPPheader findFirstByOrderByIdDesc();

    Optional<RscMtPPheader> findByMpsDate(LocalDate mpsDate);

    RscMtPPheader findByMpsNameAndMpsDate(String mpsName, LocalDate mpsDate);

    List<RscMtPPheader> findAllByActiveAndRscDtTagName(boolean active, String tagName);

    List<RscMtPPheader> findAllByRscDtTagName(String tagName);

    List<RscMtPPheader> findAllByIsBackUpDoneIsNullOrIsBackUpDoneEquals(Boolean status);

    List<RscMtPPheader> findAllByActiveAndIsBackUpDoneIsNullOrIsBackUpDoneEquals(boolean active, Boolean status);

    List<RscMtPPheader> findByMpsNameAndIsBackUpDoneIsNullOrIsBackUpDoneEqualsAndIdNotLike(String mpsName, Boolean status, Long id);

    List<RscMtPPheader> findByMpsNameAndIdIsNotAndIsBackUpDoneIsNullOrIsBackUpDoneEquals(String mpsName, Long id, Boolean status);

    List<RscMtPPheader> findAllByActiveAndIsBackUpDone(boolean active, Boolean status);

}