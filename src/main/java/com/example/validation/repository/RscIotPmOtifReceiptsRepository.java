package com.example.validation.repository;

import com.example.validation.domain.RscIotPmOtifReceipts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIotPmOtifReceiptsRepository extends JpaRepository<RscIotPmOtifReceipts, Long> {

    List<RscIotPmOtifReceipts> findAllByRscMtItemIdAndRscMtOtifPmReceiptsHeaderId(Long rscMtItemId, Long latestReceiptHeaderId);

    RscIotPmOtifReceipts findByRscMtItemIdAndRscMtOtifPmReceiptsHeaderIdAndRscDtSupplierId(Long rscMtItemId, Long latestReceiptHeaderId, Long supplierId);

    void deleteAllByReceiptDateBetween(LocalDate startDate, LocalDate endDate);

    void deleteAllByRscMtOtifPmReceiptsHeaderIdIn(List<Long> ids);
}