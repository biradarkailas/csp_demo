package com.example.validation.repository;

import com.example.validation.domain.RscMtPmMesRejectedStock;
import com.example.validation.dto.PmMesRejectedStockDTO;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtPmMesRejectedStockRepository extends JpaRepository<RscMtPmMesRejectedStock, Long> {
    void deleteAllByStockDate(LocalDate stockDate);

    @Query(name = "Pm_Mes_Rejected_Stock", nativeQuery = true)
    List<PmMesRejectedStockDTO> findPmMesRejectedStockByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Pm_Mes_Rejected_Stock", nativeQuery = true)
    List<PmMesRejectedStockDTO> findPmMesRejectedStockByMpsDateAndStockType(String mpsDate, String stockType);
}