package com.example.validation.repository;

import com.example.validation.domain.RscMtRmMes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtRmMesRepository extends JpaRepository<RscMtRmMes, Long> {
    List<RscMtRmMes> findAllByRscMtItemId(Long rscMtItemId);

    List<RscMtRmMes> findAllByStockDate(LocalDate stockDate);

    void deleteAllByStockDate(LocalDate stockDate);

    //    Mes New Api with optimization
    List<RscMtRmMes> findAllByStockDateBetween(LocalDate start, LocalDate end);
}
