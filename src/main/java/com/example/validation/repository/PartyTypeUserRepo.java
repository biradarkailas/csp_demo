package com.example.validation.repository;

import com.example.validation.domain.PartyTypeUser;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartyTypeUserRepo extends JpaRepository<PartyTypeUser, Long> {
    public PartyTypeUser findByUsername(String username);

    List<PartyTypeUser> findAllByEnabled(boolean enabled);
}
