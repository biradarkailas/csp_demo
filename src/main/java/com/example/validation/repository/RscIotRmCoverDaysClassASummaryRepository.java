package com.example.validation.repository;

import com.example.validation.domain.RscIotRmCoverDaysClassASummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotRmCoverDaysClassASummaryRepository extends JpaRepository<RscIotRmCoverDaysClassASummary, Long> {
    RscIotRmCoverDaysClassASummary findByRscMtPPheaderId(Long ppheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderId(Long ppHeaderId);
}
