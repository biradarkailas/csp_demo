package com.example.validation.repository;

import com.example.validation.domain.RscItItemMould;
import com.example.validation.dto.RscItItemMouldDTO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItItemMouldRepository extends JpaRepository<RscItItemMould,Long>{
    RscItItemMould findByRscMtItemId(Long mtItemId);
    List<RscItItemMould> findByRscDtMouldId(Long mouldId);

}
