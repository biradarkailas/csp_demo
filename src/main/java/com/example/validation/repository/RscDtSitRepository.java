package com.example.validation.repository;

import com.example.validation.domain.RscDtSit;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtSitRepository extends JpaRepository<RscDtSit,Long> {
}
