package com.example.validation.repository;

import com.example.validation.domain.RscMtRmOpenPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscMtRmOpenPoRepository extends JpaRepository<RscMtRmOpenPo, Long> {
    List<RscMtRmOpenPo> findAllByRscMtPPheaderIdAndRscMtItemIdAndRscMtSupplierId(Long mpsId, Long rscMtItemId, Long rscMtSupplierId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
