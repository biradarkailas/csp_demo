package com.example.validation.repository;

import com.example.validation.domain.RscItRMUniqueLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItRMUniqueLogicalConsumptionRepository extends JpaRepository<RscItRMUniqueLogicalConsumption, Long> {
    List<RscItRMUniqueLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(Long ppHeaderId, Long fgMtItemId);

    List<RscItRMUniqueLogicalConsumption> findAllByRscMtItemChildRscDtItemCategoryDescription(String itemDescription);

    List<RscItRMUniqueLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}