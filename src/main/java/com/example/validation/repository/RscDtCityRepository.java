package com.example.validation.repository;

import com.example.validation.domain.RscDtCity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtCityRepository extends JpaRepository<RscDtCity,Long> {
}
