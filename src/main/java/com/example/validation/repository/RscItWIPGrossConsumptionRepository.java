package com.example.validation.repository;

import com.example.validation.domain.RscItWIPGrossConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItWIPGrossConsumptionRepository extends JpaRepository<RscItWIPGrossConsumption, Long> {
    RscItWIPGrossConsumption findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long wipItemId);

    List<RscItWIPGrossConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long wipItemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}