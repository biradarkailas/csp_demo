package com.example.validation.repository;

import com.example.validation.domain.RscIotPMLatestPurchasePlan;
import com.example.validation.domain.RscIotPMOriginalPurchasePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotPMOriginalPurchasePlanRepository extends JpaRepository<RscIotPMOriginalPurchasePlan, Long> {
    List<RscIotPMOriginalPurchasePlan> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotPMLatestPurchasePlan findByRscMtItemId(Long mtItemId);

    RscIotPMOriginalPurchasePlan findAllByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId);

    RscIotPMOriginalPurchasePlan findByrscMtItemId(Long rscMtItemId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}