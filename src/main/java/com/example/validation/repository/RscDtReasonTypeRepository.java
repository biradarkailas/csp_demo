package com.example.validation.repository;

import com.example.validation.domain.RscDtReasonType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtReasonTypeRepository extends JpaRepository<RscDtReasonType, Long> {
}
