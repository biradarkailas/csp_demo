package com.example.validation.repository;

import com.example.validation.domain.RscMtRmOtherStocks;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtRmOtherStocksRepository extends JpaRepository<RscMtRmOtherStocks, Long> {
    List<RscMtRmOtherStocks> findAllByRscMtItemId(Long rscMtItemId);

    List<RscMtRmOtherStocks> findAllByRscDtStockTypeIdAndRscMtItemId(Long stockTypeId, Long rscMtItemId);

    void deleteAllByStockDate(LocalDate stockDate);
}