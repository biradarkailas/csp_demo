package com.example.validation.repository;

import com.example.validation.domain.RscDtBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtBrandRepository extends JpaRepository<RscDtBrand, Long> {
}
