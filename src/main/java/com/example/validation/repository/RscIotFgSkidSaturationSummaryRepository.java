package com.example.validation.repository;

import com.example.validation.domain.RscIotFgSkidSaturationSummary;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RscIotFgSkidSaturationSummaryRepository extends JpaRepository<RscIotFgSkidSaturationSummary, Long> {

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFgSkidSaturationSummary> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotFgSkidSaturationSummary findByRscMtPPheaderIdAndRscDtSkidsId(Long ppHeaderId, Long rscDtSkidsId);

    List<RscIotFgSkidSaturationSummary> findAllByRscMtPPheaderIdAndSaturationStatus(Long ppHeaderId, Enum saturationStatus);

    List<RscIotFgSkidSaturationSummary> findAllByRscDtSkidsId(Long rscDtSkidsId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}