package com.example.validation.repository;

import com.example.validation.domain.RscDtTechnicalSeries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtTechnicalSeriesRepository extends JpaRepository<RscDtTechnicalSeries,Long> {
}
