package com.example.validation.repository;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyRelationship;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartyRelationshipRepo extends JpaRepository<PartyRelationship, Long> {
    public PartyRelationship findByFirstParty(Party firstParty);
}
