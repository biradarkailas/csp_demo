package com.example.validation.repository;

import com.example.validation.domain.RscIotFGSlobSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotFGSlobSummaryRepository extends JpaRepository<RscIotFGSlobSummary, Long> {
    Long countByRscMtPPheaderId(Long ppHeaderId);

    RscIotFGSlobSummary findByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}