package com.example.validation.repository;

import com.example.validation.domain.RscIotRmSlobSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotRmSlobSummaryRepository extends JpaRepository<RscIotRmSlobSummary, Long> {
    RscIotRmSlobSummary findByRscMtPPheaderId(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderId(Long rscMtPPheaderId);
}
