package com.example.validation.repository;

import com.example.validation.domain.RscIotPMLatestPurchasePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public interface RscIotPMLatestPurchasePlanRepository extends JpaRepository<RscIotPMLatestPurchasePlan, Long> {
    List<RscIotPMLatestPurchasePlan> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotPMLatestPurchasePlan findByRscMtItemId(Long mtItemId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    RscIotPMLatestPurchasePlan findAllByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId);

    List<RscIotPMLatestPurchasePlan> findAllByRscMtPPheaderIdAndIdIn(Long ppHeaderId, List<Long> purchasePlanIdList);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}