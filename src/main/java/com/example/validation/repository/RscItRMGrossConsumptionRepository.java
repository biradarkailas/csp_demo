package com.example.validation.repository;

import com.example.validation.domain.RscItRMGrossConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItRMGrossConsumptionRepository extends JpaRepository<RscItRMGrossConsumption, Long> {
    List<RscItRMGrossConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscItRMGrossConsumption> findAllByRscMtPPheaderIdAndRscMtItemRscDtItemCategoryDescription(Long ppHeaderId, String categoryType);

    long countByRscMtPPheaderId(Long ppHeaderId);

    RscItRMGrossConsumption findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}