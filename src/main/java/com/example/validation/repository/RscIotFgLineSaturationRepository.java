package com.example.validation.repository;

import com.example.validation.domain.RscIotFgLineSaturation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotFgLineSaturationRepository extends JpaRepository<RscIotFgLineSaturation, Long> {
    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFgLineSaturation> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotFgLineSaturation findByRscMtPPheaderIdAndRscDtLinesId(Long ppHeaderId, Long rscDtLineId);

    List<RscIotFgLineSaturation> findAllByRscDtLinesIdAndRscMtPPheaderId(Long rscDtLineId, Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
    List<RscIotFgLineSaturation> findAllByRscMtPPheaderRscDtTagName(String tagName);

}
