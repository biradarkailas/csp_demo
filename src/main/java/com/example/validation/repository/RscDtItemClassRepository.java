package com.example.validation.repository;

import com.example.validation.domain.RscDtItemClass;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtItemClassRepository extends JpaRepository<RscDtItemClass,Long> {
}
