package com.example.validation.repository;

import com.example.validation.domain.RscIotPmOtifSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIotPmOtifSummaryRepository extends JpaRepository<RscIotPmOtifSummary, Long> {
    long countByReceiptHeaderDate(LocalDate receiptHeaderDate);

    List<RscIotPmOtifSummary> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotPmOtifSummary> findByRscMtPPheaderIdAndAvgScore(Long ppHeaderId, Integer avgScore);

    RscIotPmOtifSummary findByRscMtPPheaderIdAndRscDtSupplierId(Long ppHeaderId, Long supplierId);

    List<RscIotPmOtifSummary> findByRscDtSupplierId(Long supplierId);

    List<RscIotPmOtifSummary> findByRscMtPPheaderIdAndReceiptHeaderDateAndAvgScore(Long ppHeaderId, LocalDate receiptHeaderDate, Integer avgScore);

    List<RscIotPmOtifSummary> findAllByRscMtPPheaderIdAndReceiptHeaderDate(Long ppHeaderId, LocalDate receiptHeaderDate);

    RscIotPmOtifSummary findByRscMtPPheaderIdAndRscDtSupplierIdAndReceiptHeaderDate(Long ppHeaderId, Long supplierId, LocalDate receiptHeaderDate);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotPmOtifSummary> findAllByReceiptHeaderDate(LocalDate receiptHeaderDate);

    List<RscIotPmOtifSummary> findByReceiptHeaderDateAndAvgScore(LocalDate receiptHeaderDate, Integer avgScore);

    RscIotPmOtifSummary findByRscDtSupplierIdAndReceiptHeaderDate(Long supplierId, LocalDate receiptHeaderDate);


}
