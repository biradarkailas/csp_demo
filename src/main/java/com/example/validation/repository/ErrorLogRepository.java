package com.example.validation.repository;

import com.example.validation.domain.ErrorLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ErrorLogRepository extends JpaRepository<ErrorLog, Long> {

    @Procedure(procedureName = "export_error_log")
    String exportErrorLogData(@Param("file_path") String filePath);

    @Procedure(procedureName = "prc_sanitize_data")
    String sanitization(@Param("table_name") String tableName);

    /*
    Validation Procedures
     */
    @Procedure(procedureName = "prc_validate_mps_data")
    String mpsValidation();

    @Procedure(procedureName = "prc_validate_working_days_data")
    String workingDaysValidation();

    @Procedure(procedureName = "prc_validate_bom_data")
    String bomValidation();

    @Procedure(procedureName = "prc_validate_item_data")
    String itemValidation();

    @Procedure(procedureName = "prc_validate_mould_master_data")
    String mouldMasterValidation();

    @Procedure(procedureName = "prc_validate_pm_material_master_data")
    String pmMaterialMasterValidation();

    @Procedure(procedureName = "prc_validate_receipts_till_date_data")
    String receiptsTillDateValidation();

    @Procedure(procedureName = "prc_validate_rm_material_master_data")
    String rmMaterialMasterValidation();

    @Procedure(procedureName = "prc_validate_open_po_data")
    String openPoValidation();

    @Procedure(procedureName = "prc_validate_in_std_data")
    String inStdValidation();

    @Procedure(procedureName = "prc_validate_mid_night_stock_data")
    String midNightStockValidation();

    @Procedure(procedureName = "prc_validate_us_listgen_data")
    String usListGenValidation();

    @Procedure(procedureName = "prc_validate_technical_size_data")
    String technicalSizeValidation();

    @Procedure(procedureName = "prc_validate_lines_data")
    String linesValidation();

    @Procedure(procedureName = "prc_validate_Skids_data")
    String skidsValidation();

    @Procedure(procedureName = "prc_validate_std_price_data")
    String stdPriceValidation();

    @Procedure(procedureName = "prc_validate_other_location_stock")
    String otherLocationStockValidation();

    @Procedure(procedureName = "prc_validate_physical_rejection")
    String physicalRejectionValidation();

    @Procedure(procedureName = "prc_validate_item_2")
    String item2Validation();

    @Procedure(procedureName = "prc_validate_forecast_mps_data")
    String forecastMpsValidation();

    @Procedure(procedureName = "prc_validate_MAP_data")
    String mapValidation();

    @Procedure(procedureName = "prc_validate_OTIF_PM_RTD")
    String pmOtifValidation();

    @Procedure(procedureName = "prc_validate_item_2_data")
    String item2DataValidation();

    /*
    Resource Procedures
     */
    @Procedure(procedureName = "prc_post_MPS")
    String srcPostMps();

    @Procedure(procedureName = "prc_post_Working_Days")
    String srcPostWorkingDays();

    @Procedure(procedureName = "prc_post_BOM")
    String srcPostBOM();

    @Procedure(procedureName = "prc_post_item_data")
    String srcPostItemData();

    @Procedure(procedureName = "prc_post_Std_Price_data")
    String srcPostStdPriceData();

    @Procedure(procedureName = "prc_post_pm_material_wrapper")
    String srcPostPMMaterial();

    @Procedure(procedureName = "prc_post_mould_data")
    String srcPostMouldData();

    @Procedure(procedureName = "prc_post_item_technical_size")
    String srcPostItemTechnicalSize();

    @Procedure(procedureName = "prc_post_rm_material_wrapper")
    String srcPostRMMaterial();

    @Procedure(procedureName = "prc_post_mid_night_stock_wrapper")
    String srcPostMidNightStock();

    @Procedure(procedureName = "prc_post_receipt_wrapper")
    String srcPostReceipt();

    @Procedure(procedureName = " prc_post_open_po_wrapper")
    String srcPostOpenPOData();

    @Procedure(procedureName = "prc_post_mes_rej_instdk")
    String srcPostMESRejInStd();

    @Procedure(procedureName = "prc_post_rm_list_gen")
    String srcPostRMListGen();

    @Procedure(procedureName = "prc_post_forecast_mps")
    String srcPostForecastMPS();

    @Procedure(procedureName = "prc_post_lines")
    String srcPostLinesData();

    @Procedure(procedureName = "prc_post_skids")
    String srcPostSkidsData();

    @Procedure(procedureName = "prc_post_Other_location_stock")
    String srcPostOtherLocationStock();

    @Procedure(procedureName = "prc_post_physical_rejection")
    String srcPostPhysicalRejection();

    @Procedure(procedureName = "prc_post_item_2")
    String srcPostItem2();

    @Procedure(procedureName = "prc_post_MAP_data")
    String srcPostMAP();

    @Procedure(procedureName = "prc_post_OTIF_PMRTD")
    String srcPostPMOtif();

    @Procedure(procedureName = "prc_post_item_2")
    String srcPostItem2Data();

    @Procedure(procedureName = "prc_truncate_data")
    String truncateTable(@Param("table_name") String tableName);
}