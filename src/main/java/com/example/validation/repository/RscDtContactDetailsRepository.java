package com.example.validation.repository;

import com.example.validation.domain.RscDtContactDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtContactDetailsRepository extends JpaRepository<RscDtContactDetails,Long> {
}
