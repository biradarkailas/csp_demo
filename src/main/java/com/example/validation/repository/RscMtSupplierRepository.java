package com.example.validation.repository;

import com.example.validation.domain.RscMtSupplier;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscMtSupplierRepository extends JpaRepository<RscMtSupplier, Long>{
}
