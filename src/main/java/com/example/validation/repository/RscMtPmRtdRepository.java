package com.example.validation.repository;

import com.example.validation.domain.RscMtPmRtd;
import com.example.validation.dto.PmMesStockPaginationDto;
import com.example.validation.dto.PmRtdStockPaginationDto;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtPmRtdRepository extends JpaRepository<RscMtPmRtd, Long> {
    List<RscMtPmRtd> findByRscMtItemId(Long rscMtItemId);

    List<RscMtPmRtd> findAllByReceiptDate(LocalDate receiptDate);

    List<RscMtPmRtd> findAllByRscMtItemId(Long rscMtItemId);

    void deleteByReceiptDateBetween(LocalDate startDate, LocalDate endDate);

    @Query(name = "Pm_Rtd_Stock", nativeQuery = true)
    List<PmRtdStockPaginationDto> findPmRtdStockByMpsDateAndStockType(String mpsDate, String stockType, Pageable var1);

    @Query(name = "Pm_Rtd_Stock", nativeQuery = true)
    List<PmRtdStockPaginationDto> findPmRtdStockByMpsDateAndStockType(String mpsDate, String stockType);

}