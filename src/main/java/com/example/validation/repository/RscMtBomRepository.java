package com.example.validation.repository;

import com.example.validation.domain.RscMtBom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscMtBomRepository extends JpaRepository<RscMtBom, Long> {
    List<RscMtBom> findAllByRscMtItemParentIdAndRscMtPPheaderId(Long parentItemId, Long ppHeaderId);

    List<RscMtBom> findAllByRscMtItemParentIdAndRscMtItemChildRscDtItemCategoryDescriptionAndRscMtPPheaderId(Long parentItemId, String childItemDescription, Long ppHeaderId);

    //RscMtBom findOneByRscMtItemParentId(Long parentItemId);

    List<RscMtBom> findAllByRscMtItemChildIdAndRscMtPPheaderId(Long childItemId, Long ppHeaderId);

    List<RscMtBom> findAllByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}