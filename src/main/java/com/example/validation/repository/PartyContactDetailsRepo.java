package com.example.validation.repository;

import com.example.validation.domain.PartyContactDetails;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartyContactDetailsRepo extends JpaRepository<PartyContactDetails, Long> {
    PartyContactDetails findByEmailIdOne(String emailIdOne);
}
