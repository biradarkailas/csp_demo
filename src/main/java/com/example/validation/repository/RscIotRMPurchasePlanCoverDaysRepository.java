package com.example.validation.repository;

import com.example.validation.domain.RscIotRMPurchasePlanCoverDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotRMPurchasePlanCoverDaysRepository extends JpaRepository<RscIotRMPurchasePlanCoverDays, Long> {
    Long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotRMPurchasePlanCoverDays> findAllByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}