package com.example.validation.repository;

import com.example.validation.domain.RscDtTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtTagRepository extends JpaRepository<RscDtTag,Long> {
        }
