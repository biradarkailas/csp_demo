package com.example.validation.repository;

import com.example.validation.domain.StagingTableDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface StagingTableNameRepository extends JpaRepository<StagingTableDetails, Long> {

    List<StagingTableDetails> findByExecutionSequenceGreaterThanEqual(Integer executionSequence);

    Optional<StagingTableDetails> findByTableName(String tableName);

    List<StagingTableDetails> findByTableNameIn(List<String> tableNames);

    StagingTableDetails findOneByTableName(String tableName);

}