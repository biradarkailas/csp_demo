package com.example.validation.repository;

import com.example.validation.domain.RscDtFolderName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtFolderNameRepo extends JpaRepository<RscDtFolderName, Long> {
}
