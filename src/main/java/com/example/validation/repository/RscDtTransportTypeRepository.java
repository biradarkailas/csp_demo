package com.example.validation.repository;

import com.example.validation.domain.RscDtTransportType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtTransportTypeRepository extends JpaRepository<RscDtTransportType,Long> {
}
