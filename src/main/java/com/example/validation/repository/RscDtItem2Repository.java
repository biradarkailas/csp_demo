package com.example.validation.repository;

import com.example.validation.domain.RscDtItem2;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtItem2Repository extends JpaRepository<RscDtItem2, Long> {
    RscDtItem2 findByBpiCode(String bpiCode);
}