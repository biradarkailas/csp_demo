package com.example.validation.repository;

import com.example.validation.domain.ExcelFiledetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ExcelFileDetailsRepository extends JpaRepository<ExcelFiledetails, Long> {
    List<ExcelFiledetails> findByFileName(String fileName);

    ExcelFiledetails findOneBySheetName(String sheetName);
}
