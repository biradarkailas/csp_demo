package com.example.validation.repository;

import com.example.validation.domain.RscDtCoverDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtCoverDaysRepository extends JpaRepository<RscDtCoverDays,Long> {
}
