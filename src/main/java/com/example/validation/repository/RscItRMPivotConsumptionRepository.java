package com.example.validation.repository;

import com.example.validation.domain.RscItRMPivotConsumption;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RscItRMPivotConsumptionRepository extends JpaRepository<RscItRMPivotConsumption, Long> {

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscItRMPivotConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}