package com.example.validation.repository;

import com.example.validation.domain.RscDtSailingDays;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtSailingDaysRepository extends JpaRepository<RscDtSailingDays,Long> {
}
