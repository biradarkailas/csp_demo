package com.example.validation.repository;

import com.example.validation.domain.RscDtState;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtStateRepository extends JpaRepository<RscDtState,Long> {
}
