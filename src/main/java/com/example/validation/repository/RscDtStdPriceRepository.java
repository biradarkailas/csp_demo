package com.example.validation.repository;

import com.example.validation.domain.RscDtStdPrice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtStdPriceRepository extends JpaRepository<RscDtStdPrice,Long> {
}
