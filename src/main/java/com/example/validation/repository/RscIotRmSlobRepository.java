package com.example.validation.repository;

import com.example.validation.domain.RscIotRmSlob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotRmSlobRepository extends JpaRepository<RscIotRmSlob, Long> {
    List<RscIotRmSlob> findAllByRscMtPPheaderId(Long rscMtPPheaderId);

    List<RscIotRmSlob> findAllByRscMtPPheaderIdAndMapPriceIsNull(Long rscMtPPheaderId);

    List<RscIotRmSlob> findAllByRscMtPPheaderIdAndPriceIsNull(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
