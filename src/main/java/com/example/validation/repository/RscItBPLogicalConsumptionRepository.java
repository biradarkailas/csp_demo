package com.example.validation.repository;

import com.example.validation.domain.RscItBPLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItBPLogicalConsumptionRepository extends JpaRepository<RscItBPLogicalConsumption, Long> {
    List<RscItBPLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemParentId(Long ppHeaderId, Long fgMtItemId);

    List<RscItBPLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscItBPLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemChildId(Long ppHeaderId, Long bpItemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}