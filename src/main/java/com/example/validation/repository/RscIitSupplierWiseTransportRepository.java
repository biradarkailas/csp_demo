package com.example.validation.repository;

import com.example.validation.domain.RscIitSupplierWiseTransport;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIitSupplierWiseTransportRepository extends JpaRepository<RscIitSupplierWiseTransport, Long> {
}
