package com.example.validation.repository;

import com.example.validation.domain.RscIotLatestMouldSaturation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotLatestMouldSaturationRepository extends JpaRepository<RscIotLatestMouldSaturation, Long> {
    List<RscIotLatestMouldSaturation> findAllByRscMtPPheaderId(Long rscMtPpheaderId);

    Long countByRscMtPPheaderId(Long rscMtPpheaderId);

    void deleteAllByRscMtPPheaderId(Long rscMtPpheaderId);

    List<RscIotLatestMouldSaturation> findAllByRscMtPPheaderIdAndRscIotPMLatestPurchasePlanIdIn(Long rscMtPpheaderId, List<Long> purchasePlanIdList);

    List<RscIotLatestMouldSaturation> findAllByRscMtPPheaderIdAndRscDtMouldIdIn(Long rscMtPpheaderId, List<Long> uniqueMouldsIdList);
}
