package com.example.validation.repository;

import com.example.validation.domain.RscMtPMOpenPo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscMtPMOpenPoRepository extends JpaRepository<RscMtPMOpenPo, Long> {
    List<RscMtPMOpenPo> findAllByRscMtPPheaderIdAndRscMtItemIdAndRscMtSupplierId(Long mpsId, Long rscMtItemId, Long rscMtSupplierId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
