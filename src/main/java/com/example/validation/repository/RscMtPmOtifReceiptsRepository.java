package com.example.validation.repository;

import com.example.validation.domain.RscMtPmOtifReceipts;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscMtPmOtifReceiptsRepository extends JpaRepository<RscMtPmOtifReceipts, Long> {
    void deleteAllByRscMtOtifPmReceiptsHeaderIdIn(List<Long> ids);
}