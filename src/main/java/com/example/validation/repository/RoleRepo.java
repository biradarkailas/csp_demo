package com.example.validation.repository;

import com.example.validation.domain.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role, Long> {
    public Role findByName(String name);
}
