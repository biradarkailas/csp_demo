package com.example.validation.repository;

import com.example.validation.domain.RscDtFileName;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtFileNameRepo extends JpaRepository<RscDtFileName, Long> {
}
