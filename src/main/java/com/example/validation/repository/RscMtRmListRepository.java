package com.example.validation.repository;

import com.example.validation.domain.RscMtRmList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtRmListRepository extends JpaRepository<RscMtRmList, Long> {
    List<RscMtRmList> findAllByLotNumberAndRscMtItemId(String lotNumber, Long rscMtItemId);

    void deleteAllByMpsDate(LocalDate mpsDate);
}