package com.example.validation.repository;

import com.example.validation.domain.RscIotPmLatestSlobReason;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotPmLatestSlobReasonRepository extends JpaRepository<RscIotPmLatestSlobReason,Long> {
}
