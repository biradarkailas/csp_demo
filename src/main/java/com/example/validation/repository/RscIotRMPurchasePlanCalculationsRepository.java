package com.example.validation.repository;

import com.example.validation.domain.RscIotRMPurchasePlanCalculations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotRMPurchasePlanCalculationsRepository extends JpaRepository<RscIotRMPurchasePlanCalculations, Long> {
    List<RscIotRMPurchasePlanCalculations> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotRMPurchasePlanCalculations findAllByRscMtItemIdAndRscMtPPheaderId(Long rscMtItemId, Long ppheaderId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    RscIotRMPurchasePlanCalculations findAllByRscMtItemIdAndRscMtPPheaderIdAndRscIitRMItemWiseSupplierDetailsId(Long rscMtItemId, Long ppheaderId, Long supplierDetailsId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}