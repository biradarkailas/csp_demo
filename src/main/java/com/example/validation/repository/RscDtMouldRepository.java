package com.example.validation.repository;

import com.example.validation.domain.RscDtMould;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtMouldRepository extends JpaRepository<RscDtMould,Long> {
}
