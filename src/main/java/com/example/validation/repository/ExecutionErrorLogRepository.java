package com.example.validation.repository;

import com.example.validation.domain.ExecutionErrorLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ExecutionErrorLogRepository extends JpaRepository<ExecutionErrorLog, Long> {
    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}