package com.example.validation.repository;

import com.example.validation.domain.RscItSubBaseGrossConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItSubBaseGrossConsumptionRepository extends JpaRepository<RscItSubBaseGrossConsumption, Long> {
    RscItSubBaseGrossConsumption findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId);

    List<RscItSubBaseGrossConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}