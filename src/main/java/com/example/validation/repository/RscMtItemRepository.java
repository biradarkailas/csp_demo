package com.example.validation.repository;

import com.example.validation.domain.RscMtItem;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscMtItemRepository extends JpaRepository<RscMtItem, Long> {
    List<RscMtItem> findAllByRscDtItemCategoryDescription(String description);

    List<RscMtItem> findAllByRscDtItemCategoryDescriptionAndRscDtItemCodeCodeIsStartingWithOrRscDtItemCodeCodeIsStartingWith(String description, String codeInitial1, String codeInitial2);

    List<RscMtItem> findAllByRscDtItemCategoryDescriptionAndRscDtItemTypeNotNull(String description);

    Page<RscMtItem> findAllByRscDtItemCategoryDescription(String description, Pageable var1);

    Page<RscMtItem> findAllByRscDtItemCategoryDescriptionAndRscDtItemCodeCodeIsStartingWithOrRscDtItemCodeCodeIsStartingWith(String description, String codeInitial1, String codeInitial2,Pageable var1);
}
