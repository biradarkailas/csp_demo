package com.example.validation.repository;

import com.example.validation.domain.RscMtBulkMes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtBulkMesRepository extends JpaRepository<RscMtBulkMes, Long> {
    List<RscMtBulkMes> findAllByStockDate(LocalDate stockDate);

    void deleteAllByStockDate(LocalDate stockDate);
}