package com.example.validation.repository;

import com.example.validation.domain.RscMtOtifPmReceiptsHeader;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtOtifPmReceiptsHeaderRepository extends JpaRepository<RscMtOtifPmReceiptsHeader, Long> {
    List<RscMtOtifPmReceiptsHeader> findAllByOtifDateBetween(LocalDate otifStartDate, LocalDate otifEndDate);

//    RscMtOtifPmReceiptsHeader findByOtifDate(LocalDate otifDate);

    List<RscMtOtifPmReceiptsHeader> findByOtifDate(LocalDate otifDate);

    List<RscMtOtifPmReceiptsHeader> findAllByActive(Boolean active);

    void deleteAllByIdIn(List<Long> ids);
}