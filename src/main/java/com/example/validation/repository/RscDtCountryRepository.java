package com.example.validation.repository;

import com.example.validation.domain.RscDtCountry;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtCountryRepository extends JpaRepository<RscDtCountry,Long> {
}
