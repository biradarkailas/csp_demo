package com.example.validation.repository;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyTypeOrganization;
import com.example.validation.util.enums.PartyOrganizationType;
import com.example.validation.util.enums.ServiceType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PartyTypeOrganizationRepo extends JpaRepository<PartyTypeOrganization, Long> {
    public List<PartyTypeOrganization> findAllByServiceType(ServiceType serviceType);
    public PartyTypeOrganization findByOrganizationTypeAndServiceType(PartyOrganizationType partyOrganizationType, ServiceType serviceType);
    public PartyTypeOrganization findByParty(Party party);
    public List<PartyTypeOrganization> getAllByService(boolean service);
}
