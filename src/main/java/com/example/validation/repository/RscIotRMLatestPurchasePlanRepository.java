package com.example.validation.repository;

import com.example.validation.domain.RscIotRMLatestPurchasePlan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotRMLatestPurchasePlanRepository extends JpaRepository<RscIotRMLatestPurchasePlan, Long> {
    List<RscIotRMLatestPurchasePlan> findByRscMtPPheaderId(Long ppHeaderId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}