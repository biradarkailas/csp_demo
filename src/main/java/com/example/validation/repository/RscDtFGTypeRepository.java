package com.example.validation.repository;

import com.example.validation.domain.RscDtFGType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtFGTypeRepository extends JpaRepository<RscDtFGType, Long> {
    RscDtFGType findByType(String type);
}