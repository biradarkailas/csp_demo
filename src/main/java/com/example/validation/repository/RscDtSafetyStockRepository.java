package com.example.validation.repository;

import com.example.validation.domain.RscDtSafetyStock;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtSafetyStockRepository extends JpaRepository<RscDtSafetyStock,Long> {
}
