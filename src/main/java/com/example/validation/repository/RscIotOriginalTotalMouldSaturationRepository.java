package com.example.validation.repository;

import com.example.validation.domain.RscIotOriginalTotalMouldSaturation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RscIotOriginalTotalMouldSaturationRepository extends JpaRepository<RscIotOriginalTotalMouldSaturation, Long> {
    List<RscIotOriginalTotalMouldSaturation> findAllByRscMtPPheaderId(Long mpsDate);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
