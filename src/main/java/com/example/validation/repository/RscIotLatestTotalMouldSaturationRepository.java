package com.example.validation.repository;

import com.example.validation.domain.RscIotLatestTotalMouldSaturation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RscIotLatestTotalMouldSaturationRepository extends JpaRepository<RscIotLatestTotalMouldSaturation, Long> {
    Long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotLatestTotalMouldSaturation> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotLatestTotalMouldSaturation> findAllByRscMtPPheaderIdAndM1ValueGreaterThanEqual(Long ppHeaderId, Long minValue);

    List<RscIotLatestTotalMouldSaturation> findAllByRscMtPPheaderIdAndM1ValueGreaterThanEqualAndM1ValueLessThanEqual(Long ppHeaderId, Long minValue, Long maxValue);

    List<RscIotLatestTotalMouldSaturation> findAllByRscMtPPheaderIdAndM1ValueLessThan(Long ppHeaderId, Long maxValue);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotLatestTotalMouldSaturation findByRscDtMouldIdAndRscMtPPheaderId(Long mouldId, Long ppHeaderId);

    List<RscIotLatestTotalMouldSaturation> findAllByRscMtPPheaderIdAndRscDtMouldIdIn(Long ppHeaderId, List<Long> uniqueMouldsIdList);

}
