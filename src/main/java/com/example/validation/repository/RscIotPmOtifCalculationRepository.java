package com.example.validation.repository;

import com.example.validation.domain.RscIotPmOtifCalculation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIotPmOtifCalculationRepository extends JpaRepository<RscIotPmOtifCalculation, Long> {
    long countByReceiptHeaderDate(LocalDate otifDate);

    List<RscIotPmOtifCalculation> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotPmOtifCalculation> findAllByRscDtSupplierIdAndRscMtPPheaderId(Long supplierId, Long ppHeaderId);

    List<RscIotPmOtifCalculation> findAllByRscMtPPheaderIdAndReceiptHeaderDate(Long ppHeaderId, LocalDate receiptHeaderDate);

    List<RscIotPmOtifCalculation> findAllByRscDtSupplierIdAndRscMtPPheaderIdAndReceiptHeaderDate(Long supplierId, Long ppHeaderId, LocalDate receiptHeaderDate);

    List<RscIotPmOtifCalculation> findAllByRscDtSupplierIdAndReceiptHeaderDate(Long supplierId, LocalDate receiptHeaderDate);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotPmOtifCalculation> findAllByReceiptHeaderDateBetween(LocalDate startDate, LocalDate endDate);
}
