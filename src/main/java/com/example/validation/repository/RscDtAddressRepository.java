package com.example.validation.repository;

import com.example.validation.domain.RscDtAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtAddressRepository extends JpaRepository<RscDtAddress,Long> {
}
