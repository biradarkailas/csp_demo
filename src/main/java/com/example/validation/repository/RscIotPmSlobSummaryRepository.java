package com.example.validation.repository;

import com.example.validation.domain.RscIotPmSlobSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotPmSlobSummaryRepository extends JpaRepository<RscIotPmSlobSummary, Long> {
    List<RscIotPmSlobSummary> findAllByRscMtPPheaderId(Long rscMtPPheaderId);

    RscIotPmSlobSummary findByRscMtPPheaderId(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderId(Long rscMtPPheaderId);
}
