package com.example.validation.repository;

import com.example.validation.domain.RscDtTestTask;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtTestTaskRepository extends JpaRepository<RscDtTestTask,Long> {
}
