package com.example.validation.repository;

import com.example.validation.domain.ResourceErrorLog;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResourceErrorLogRepository extends JpaRepository<ResourceErrorLog, Long> {
}
