package com.example.validation.repository;

import com.example.validation.domain.PmOtifCumulativeScore;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PmOtifCumulativeScoreRepository extends JpaRepository<PmOtifCumulativeScore, Long> {
    PmOtifCumulativeScore findFirstByRscDtSupplierId(Long id);

    List<PmOtifCumulativeScore> findAllByScore(Double score);
}
