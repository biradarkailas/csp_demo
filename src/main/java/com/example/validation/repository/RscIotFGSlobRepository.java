package com.example.validation.repository;

import com.example.validation.domain.RscIotFGSlob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotFGSlobRepository extends JpaRepository<RscIotFGSlob, Long> {
    Long countByRscMtPPHeaderId(Long ppHeaderId);

    List<RscIotFGSlob> findAllByRscMtPPHeaderId(Long ppHeaderId);

    void deleteAllByRscMtPPHeaderId(Long ppHeaderId);
}