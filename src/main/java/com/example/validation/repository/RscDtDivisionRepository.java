package com.example.validation.repository;

import com.example.validation.domain.RscDtDivision;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtDivisionRepository extends JpaRepository<RscDtDivision,Long> {
}
