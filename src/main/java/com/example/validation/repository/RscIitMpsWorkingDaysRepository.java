package com.example.validation.repository;

import com.example.validation.domain.RscIitMpsWorkingDays;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscIitMpsWorkingDaysRepository extends JpaRepository<RscIitMpsWorkingDays, Long> {
    RscIitMpsWorkingDays findByRscMtPPheaderId(Long mpsId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
