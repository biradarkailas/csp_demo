package com.example.validation.repository;

import com.example.validation.domain.RscIitRmBulkOpRm;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscIitRmBulkOpRmRepository extends JpaRepository<RscIitRmBulkOpRm, Long> {
    List<RscIitRmBulkOpRm> findAllByRscMtRmItemId(Long rscMtItemId);

    List<RscIitRmBulkOpRm> findAllByStockDate(LocalDate stockDate);

    void deleteAllByStockDate(LocalDate stockDate);
}