package com.example.validation.repository;

import com.example.validation.domain.RscItItemStockType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscItItemStockTypeRepository extends JpaRepository<RscItItemStockType, Long> {
}
