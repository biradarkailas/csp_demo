package com.example.validation.repository;

import com.example.validation.domain.RscDtUom;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtUomRepository extends JpaRepository<RscDtUom,Long> {
}
