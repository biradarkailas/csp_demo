package com.example.validation.repository;

import com.example.validation.domain.RscItItemWiseSupplier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItItemWiseSupplierRepository extends JpaRepository<RscItItemWiseSupplier, Long> {

    List<RscItItemWiseSupplier> findAllByRscMtItemId(Long rscMtItemId);

    RscItItemWiseSupplier findByRscMtItemId(Long rscMtItemId);

    List<RscItItemWiseSupplier> findAllByRscMtItemRscDtItemCategoryDescriptionIn(List<String> category);

    @Query(
            value = "SELECT * FROM rsc_it_item_supplier AS a INNER JOIN rsc_mt_item AS b ON a.rsc_mt_item_id = b.id INNER JOIN rsc_dt_item_category AS c ON b.rsc_dt_item_category_id = c.id WHERE c.description = 'Packing Material' GROUP BY a.rsc_mt_supplier_id",
            nativeQuery = true)
    Page<RscItItemWiseSupplier> findRscMtItemRscDtItemCategoryPackingMaterialDescriptionIn(Pageable var1);

    @Query(
            value = "SELECT * FROM rsc_it_item_supplier AS a INNER JOIN rsc_mt_item AS b ON a.rsc_mt_item_id = b.id INNER JOIN rsc_dt_item_category AS c ON b.rsc_dt_item_category_id = c.id WHERE c.description = 'Raw Material' GROUP BY a.rsc_mt_supplier_id",
            nativeQuery = true)
    Page<RscItItemWiseSupplier> findByRscMtItemRscDtItemCategoryRawMaterialDescriptionIn(Pageable var1);

}