package com.example.validation.repository;

import com.example.validation.domain.RscItPMSlob;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItPMSlobRepository extends JpaRepository<RscItPMSlob, Long> {
    List<RscItPMSlob> findAllByRscMtPPheaderId(Long rscMtPPheaderId);

    List<RscItPMSlob> findAllByRscMtPPheaderIdAndMapPriceIsNull(Long rscMtPPheaderId);

    List<RscItPMSlob> findAllByRscMtPPheaderIdAndPriceIsNull(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
