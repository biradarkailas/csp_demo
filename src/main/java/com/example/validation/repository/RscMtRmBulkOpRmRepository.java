package com.example.validation.repository;

import com.example.validation.domain.RscMtRmBulkOpRm;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscMtRmBulkOpRmRepository extends JpaRepository<RscMtRmBulkOpRm,Long> {
}
