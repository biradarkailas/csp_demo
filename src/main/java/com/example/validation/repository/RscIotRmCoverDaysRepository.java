package com.example.validation.repository;

import com.example.validation.domain.RscIotRmCoverDays;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RscIotRmCoverDaysRepository extends JpaRepository<RscIotRmCoverDays, Long> {
    List<RscIotRmCoverDays> findAllByRscMtPPheaderId(Long rscMtPPHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotRmCoverDays> findAllByRscMtPPheaderIdAndRateIsNull(Long rscMtPPheaderId);

    void deleteAllByRscMtPPheaderId(Long rscMtPPHeaderId);
}
