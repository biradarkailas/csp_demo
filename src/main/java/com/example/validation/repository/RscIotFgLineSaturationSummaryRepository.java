package com.example.validation.repository;

import com.example.validation.domain.RscIotFgLineSaturationSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotFgLineSaturationSummaryRepository extends JpaRepository<RscIotFgLineSaturationSummary, Long> {
    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFgLineSaturationSummary> findAllByRscMtPPheaderId(Long ppHeaderId);

    RscIotFgLineSaturationSummary findByRscMtPPheaderIdAndRscDtLinesId(Long ppHeaderId, Long rscDtLinesId);

    List<RscIotFgLineSaturationSummary> findAllByRscMtPPheaderIdAndSaturationStatus(Long ppHeaderId, Enum saturationStatus);

    List<RscIotFgLineSaturationSummary> findAllByRscDtLinesId(Long rscDtLinesId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}
