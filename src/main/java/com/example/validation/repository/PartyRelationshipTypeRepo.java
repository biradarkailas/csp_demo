package com.example.validation.repository;

import com.example.validation.domain.PartyRelationshipType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PartyRelationshipTypeRepo extends JpaRepository<PartyRelationshipType, Long> {
    public PartyRelationshipType findByName(String name);
}
