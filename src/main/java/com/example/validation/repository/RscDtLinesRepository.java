package com.example.validation.repository;

import com.example.validation.domain.RscDtLines;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtLinesRepository extends JpaRepository<RscDtLines,Long> {
}
