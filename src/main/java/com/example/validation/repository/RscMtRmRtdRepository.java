package com.example.validation.repository;

import com.example.validation.domain.RscMtRmRtd;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface RscMtRmRtdRepository extends JpaRepository<RscMtRmRtd, Long> {
    List<RscMtRmRtd> findAllByRscMtItemId(Long rscMtItemId);

    List<RscMtRmRtd> findAllByReceiptDate(LocalDate receiptDate);

    void deleteByReceiptDateBetween(LocalDate startDate, LocalDate endDate);
}