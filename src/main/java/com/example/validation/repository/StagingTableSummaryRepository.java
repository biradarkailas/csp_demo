package com.example.validation.repository;

import com.example.validation.domain.StagingTableSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface StagingTableSummaryRepository extends JpaRepository<StagingTableSummary, Long> {
    List<StagingTableSummary> findByTableNameInAndIsSanitizeAndIsActive(List<String> sanitizeTableNames, boolean isSanitize, boolean isActive);

    List<StagingTableSummary> findByTableNameInAndIsActive(List<String> tableNames, boolean isActive);

    List<StagingTableSummary> findByTableNameInAndIsValidateAndIsActive(List<String> sanitizeTableNames, boolean isValidate, boolean isActive);

    Long countByIsActiveAndIsValidate(boolean isActive, boolean IsValidate);

    List<StagingTableSummary> findByIsValidateAndIsActive(boolean isValidate, boolean isActive);

    Long countByTableNameAndIsActive(String tableName, boolean isActive);

    List<StagingTableSummary> findByIsActive(boolean isActive);

    @Query(value = "select tableName from StagingTableSummary where active=true and validate=true and stgLoaded=true")
    List<String> succeededTableNames();

    List<StagingTableSummary> findByIsActiveAndIsValidateAndIsStgLoaded(Boolean isActive, Boolean isValidate, Boolean isStgLoaded);

    StagingTableSummary findByIsActiveAndTableName(boolean isActive, String tableName);

    void deleteAllByMpsDateBetween(LocalDate startDate, LocalDate endDate);
}