package com.example.validation.repository;

import com.example.validation.domain.RscIotRmCoverDaysSummary;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscIotRmCoverDaysSummaryRepository extends JpaRepository<RscIotRmCoverDaysSummary, Long> {
    RscIotRmCoverDaysSummary findAllByRscMtPPheaderId(Long rscMtPPheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllByRscMtPPheaderId(Long rscMtPPHeaderId);

}
