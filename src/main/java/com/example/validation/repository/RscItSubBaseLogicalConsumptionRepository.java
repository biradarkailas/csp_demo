package com.example.validation.repository;

import com.example.validation.domain.RscItSubBaseLogicalConsumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscItSubBaseLogicalConsumptionRepository extends JpaRepository<RscItSubBaseLogicalConsumption, Long> {

    List<RscItSubBaseLogicalConsumption> findAllByRscMtPPheaderId(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    List<RscItSubBaseLogicalConsumption> findAllByRscMtPPheaderIdAndRscMtItemChildId(Long ppHeaderId, Long subBaseItemId);

    void deleteAllByRscMtPPheaderIdAndRscMtItemParentId(Long ppHeaderId, Long bpParentItemId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}