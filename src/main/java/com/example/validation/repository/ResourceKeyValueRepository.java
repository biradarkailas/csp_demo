package com.example.validation.repository;

import com.example.validation.domain.ResourceKeyValue;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ResourceKeyValueRepository extends JpaRepository<ResourceKeyValue, Long> {

    Optional<ResourceKeyValue> findByResourceKeyAndIsDelete(String resourceKey, boolean isDelete);
}