package com.example.validation.repository;

import com.example.validation.domain.RscDtSupplier;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RscDtSupplierRepository extends JpaRepository<RscDtSupplier,Long> {
}
