package com.example.validation.repository;

import com.example.validation.domain.ValidationTableLoadingDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ValidationTableLoadingDetailsRepository extends JpaRepository<ValidationTableLoadingDetails, Long> {
}
