package com.example.validation.repository;

import com.example.validation.domain.RscDtItemCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscDtItemCategoryRepository extends JpaRepository<RscDtItemCategory,Long> {
}
