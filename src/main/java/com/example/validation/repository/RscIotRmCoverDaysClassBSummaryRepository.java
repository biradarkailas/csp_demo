package com.example.validation.repository;

import com.example.validation.domain.RscIotRmCoverDaysClassBSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotRmCoverDaysClassBSummaryRepository extends JpaRepository<RscIotRmCoverDaysClassBSummary, Long> {
    RscIotRmCoverDaysClassBSummary findByRscMtPPheaderId(Long ppheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderId(Long ppHeaderId);
}
