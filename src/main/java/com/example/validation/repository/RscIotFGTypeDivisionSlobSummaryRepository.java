package com.example.validation.repository;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RscIotFGTypeDivisionSlobSummaryRepository extends JpaRepository<RscIotFGTypeDivisionSlobSummary, Long> {
    List<RscIotFGTypeDivisionSlobSummary> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscIotFGTypeDivisionSlobSummary> findAllByRscMtPPheaderIdAndRscDtFGTypeId(Long ppHeaderId, Long fgTypeId);

    Long countByRscMtPPheaderId(Long ppHeaderId);

    RscIotFGTypeDivisionSlobSummary findAllByRscMtPPheaderIdAndRscDtFGTypeIdAndRscDtDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId);

    void deleteAllByRscMtPPheaderId(Long ppHeaderId);
}