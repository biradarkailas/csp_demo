package com.example.validation.repository;

import com.example.validation.domain.RscIotRmCoverDaysClassCSummary;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RscIotRmCoverDaysClassCSummaryRepository extends JpaRepository<RscIotRmCoverDaysClassCSummary, Long> {

    RscIotRmCoverDaysClassCSummary findByRscMtPPheaderId(Long ppheaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderId(Long ppHeaderId);
}
