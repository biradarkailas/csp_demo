package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iit_supplier_wise_transport")
public class RscIitSupplierWiseTransport extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "item_wise_supplier_id")
    private RscItItemWiseSupplier rscItItemWiseSupplier;

    @ManyToOne
    @JoinColumn(name = "sailing_days_id")
    private RscDtSailingDays rscDtSailingDays;

    @ManyToOne
    @JoinColumn(name = "transport_mode_id")
    private RscDtTransportMode rscDtTransportMode;

    @ManyToOne
    @JoinColumn(name = "transport_type_id")
    private RscDtTransportType rscDtTransportType;

    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
}