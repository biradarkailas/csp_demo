package com.example.validation.domain;

import com.example.validation.util.enums.SlobType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_it_pm_slob")
public class RscItPMSlob extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "rsc_mt_pm_item_id")
    private RscMtItem rscMtItem;
    @ManyToOne
    @JoinColumn(name = "mps_id")
    private RscMtPPheader rscMtPPheader;
    @ManyToOne
    @JoinColumn(name = "rsc_it_item_supplier_id")
    private RscItItemWiseSupplier rscItItemWiseSupplier;
    private Double mapPrice;
    private Double stdPrice;
    private Double price;
    private Double midNightStockQuantity;
    private Double stockQuantity;
    private Double stockValue;
    private Double slobQuantity;
    private Double totalConsumptionQuantity;
    private Double slobValue;
    @Enumerated(EnumType.STRING)
    @Column(name = "slob_type")
    private SlobType slobType;
    @ManyToOne
    @JoinColumn(name = "rsc_dt_reason_id")
    private RscDtReason rscDtReason;
    private String remark;
}
