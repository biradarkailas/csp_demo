package com.example.validation.domain;

import com.example.validation.dto.ReceiptStockPaginationDto;
import com.example.validation.dto.RmBulkOpRmStockPaginationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity

@NamedNativeQuery(name = "Bulk_Op_Rm_Stock",
        query =  "select csp_loreal.vew_item.code as bulkCode,csp_loreal.vew_item.description as bulkDescription,csp_loreal.rm_name.code as rmCode,csp_loreal.rm_name.description as rmDescription,csp_loreal.rsc_mt_bulk_mes.rsc_mt_item_id as bulkItemId,sum(csp_loreal.rsc_mt_bulk_mes.Quantity) as bulkTotalQuantity,csp_loreal.rsc_iit_rm_bulk_op_rm.rm_item_id as rmItemId,csp_loreal.rsc_iit_rm_bulk_op_rm.quantity as bulkWiseRmQuantity from csp_loreal.rsc_mt_bulk_mes,csp_loreal.rsc_iit_rm_bulk_op_rm,csp_loreal.vew_item,csp_loreal.vew_item rm_name where concat(year(csp_loreal.rsc_mt_bulk_mes.stock_date),month(csp_loreal.rsc_mt_bulk_mes.stock_date))=?1 and concat(year(csp_loreal.rsc_iit_rm_bulk_op_rm.stock_date),month(csp_loreal.rsc_iit_rm_bulk_op_rm.stock_date))=?1 and csp_loreal.rsc_iit_rm_bulk_op_rm.rsc_mt_bulk_item_id=csp_loreal.rsc_mt_bulk_mes.rsc_mt_item_id and csp_loreal.vew_item.mt_item_id=rsc_mt_item_id and csp_loreal.rm_name.mt_item_id=csp_loreal.rsc_iit_rm_bulk_op_rm.rm_item_id GROUP BY csp_loreal.rsc_mt_bulk_mes.stock_date,csp_loreal.rsc_mt_bulk_mes.rsc_mt_item_id,csp_loreal.rsc_iit_rm_bulk_op_rm.rm_item_id union select csp_loreal.vew_item.code as bulkCode, csp_loreal.vew_item.description as bulkDescription,csp_loreal.rm_name.code as rmCode,csp_loreal.rm_name.description as rmDescription,csp_loreal.stg_tmp_rsc_mt_bulk_mes.rsc_mt_item_id bulkItemId, sum(csp_loreal.stg_tmp_rsc_mt_bulk_mes.Quantity) as bulkTotalQuantity,csp_loreal.rsc_iit_rm_bulk_op_rm.rm_item_id as rmItemId, csp_loreal.rsc_iit_rm_bulk_op_rm.quantity as bulkWiseRmQuantity from csp_loreal.stg_tmp_rsc_mt_bulk_mes,csp_loreal.rsc_iit_rm_bulk_op_rm,csp_loreal.vew_item,csp_loreal.vew_item rm_name where concat(year(csp_loreal.stg_tmp_rsc_mt_bulk_mes.stock_date),month(csp_loreal.stg_tmp_rsc_mt_bulk_mes.stock_date))=?1 and concat(year(csp_loreal.rsc_iit_rm_bulk_op_rm.stock_date),month(csp_loreal.rsc_iit_rm_bulk_op_rm.stock_date))=?1 and csp_loreal.rsc_iit_rm_bulk_op_rm.rsc_mt_bulk_item_id=csp_loreal.stg_tmp_rsc_mt_bulk_mes.rsc_mt_item_id and csp_loreal.vew_item.mt_item_id=rsc_mt_item_id and csp_loreal.rm_name.mt_item_id=csp_loreal.rsc_iit_rm_bulk_op_rm.rm_item_id and csp_loreal.stg_tmp_rsc_mt_bulk_mes.rsc_mt_item_id not in (select rsc_mt_item_id from csp_loreal.rsc_mt_bulk_mes where concat(year(csp_loreal.rsc_mt_bulk_mes.stock_date),month(csp_loreal.rsc_mt_bulk_mes.stock_date))=?1) and left(rm_name.code,1) <> 'B' GROUP BY csp_loreal.stg_tmp_rsc_mt_bulk_mes.rsc_mt_item_id,csp_loreal.rsc_iit_rm_bulk_op_rm.rm_item_id",
        resultSetMapping = "RmBulkOpRmStockPaginationDto")
@SqlResultSetMapping(name = "RmBulkOpRmStockPaginationDto",
        classes = @ConstructorResult(targetClass = RmBulkOpRmStockPaginationDto.class, columns = {
                @ColumnResult(name = "bulkCode", type = String.class),
                @ColumnResult(name = "bulkDescription", type = String.class),
                @ColumnResult(name = "rmCode", type = String.class),
                @ColumnResult(name = "rmDescription", type = String.class),
                @ColumnResult(name = "bulkItemId", type = Long.class),
                @ColumnResult(name = "rmItemId", type = Long.class),
                @ColumnResult(name = "bulkTotalQuantity", type = Double.class),
                @ColumnResult(name = "bulkWiseRmQuantity", type = Double.class)}))
@Table(name = "rsc_mt_rm_bulk_op_rm")
public class RscMtRmBulkOpRm extends BaseIdEntity {
    private LocalDate date;
    private Double quantity;
    private String lotNumber;
    private String location;
    private String stockStatus;
    private String documentNumber;
    private String country;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;
}

