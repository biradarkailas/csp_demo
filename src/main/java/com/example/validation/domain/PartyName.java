package com.example.validation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "party_name")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartyName extends BaseIdEntity {
    private String salutation;
    private String organizationName;
    private String firstName;
    private String lastName;
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "PARTY_ID")
    private Party party;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;
}
