package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_contact_details")
public class RscDtContactDetails extends BaseIdEntity {
    private String mobileNo1;
    private String mobileNo2;
    private String landlineNo1;
    private String landlineNo2;
    private String fax;
    private String emailId1;
    private String emailId2;
}