package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_item2")
public class RscDtItem2 extends BaseIdEntity {
    private String bpiCode;
    private String baseCode;
}