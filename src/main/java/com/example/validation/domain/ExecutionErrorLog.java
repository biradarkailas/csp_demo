package com.example.validation.domain;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Data
@Table(name = "execution_error_log")
public class ExecutionErrorLog extends BaseIdEntity {
    private String methodName;
    private String errorDescription;
    private LocalDateTime createdDate;
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
}
