package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_tag")
public class RscDtTag extends BaseIdEntity {
    private String name;
    private String description;
    private Boolean isBackup;
}

