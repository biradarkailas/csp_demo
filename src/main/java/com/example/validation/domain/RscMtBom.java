package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_mt_bom")
public class RscMtBom extends BaseIdEntity {
    private int materialBatchNumber;
    private Double linkQuantity;
    private Double wastePercent;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
    private Boolean active;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_parent_id")
    private RscMtItem rscMtItemParent;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_child_id")
    private RscMtItem rscMtItemChild;

    @ManyToOne
    @JoinColumn(name = "mps_id")
    private RscMtPPheader rscMtPPheader;
}