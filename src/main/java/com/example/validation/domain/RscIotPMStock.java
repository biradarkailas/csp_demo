package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_pm_stock")
public class RscIotPMStock extends BaseIdEntity {
    private Double quantity;
    private LocalDate stockDate;
    private LocalDate arrivalDateForPo;

    @ManyToOne
    @JoinColumn(name = "pm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "stock_type_id")
    private RscDtStockType rscDtStockType;
}