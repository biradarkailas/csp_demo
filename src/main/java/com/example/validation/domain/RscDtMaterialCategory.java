package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_material_category")
public class RscDtMaterialCategory extends BaseIdEntity {
    private String name;
}