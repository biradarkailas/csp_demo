package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_rm_stock")
public class RscIotRmStock extends BaseIdEntity {
    private Double quantity;
    private LocalDate stockDate;
    private LocalDate arrivalDateForPo;
    private LocalDate mpsDate;

    @ManyToOne
    @JoinColumn(name = "rm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "stock_type_id")
    private RscDtStockType rscDtStockType;
}
