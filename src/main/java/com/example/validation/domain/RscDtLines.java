package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_lines")
public class RscDtLines extends BaseIdEntity{
    private String name;
    private Double capacityPerShift;
    private Double shifts;
    private Double speed;
    private Double gUpTime;
    private Double modPerLinePerShift;
    private Double hrsPerShift;
}

