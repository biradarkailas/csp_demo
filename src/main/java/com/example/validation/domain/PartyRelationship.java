package com.example.validation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Table(name = "party_relationship")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartyRelationship extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "RELATIONSHIP_TYPE_ID")
    private PartyRelationshipType relationshipType;
    @ManyToOne
    @JoinColumn(name = "FIRST_PARTY_ID")
    private Party firstParty;
    @ManyToOne
    @JoinColumn(name = "SECOND_PARTY_ID")
    private Party secondParty;
    private LocalDate beginDate;
    private LocalDate endDate;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;
}
