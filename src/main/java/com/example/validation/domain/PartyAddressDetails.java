package com.example.validation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "party_address_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartyAddressDetails extends BaseIdEntity {
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "PARTY_ID")
    private Party party;
    @ManyToOne
    @JoinColumn(name = "CITY_ID")
    private City city;
    private String addressType;
    private String addressLineOne;
    private String addressLineTwo;
    private String area;
    private String pinCode;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;
}
