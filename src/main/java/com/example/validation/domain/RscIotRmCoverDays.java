package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_rm_cover_days")
public class RscIotRmCoverDays extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "pm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_it_rm_gross_consumption_id")
    private RscItRMGrossConsumption rscItRMGrossConsumption;
    private Double stdPrice;
    private Double map;
    private Double rate;
    private Double stockQuantity;
    private Double stockValue;
    private Double month1ConsumptionValue;
    private Double month2ConsumptionValue;
    private Double month3ConsumptionValue;
    private Double month4ConsumptionValue;
    private Double month5ConsumptionValue;
    private Double month6ConsumptionValue;
    private Double month7ConsumptionValue;
    private Double month8ConsumptionValue;
    private Double month9ConsumptionValue;
    private Double month10ConsumptionValue;
    private Double month11ConsumptionValue;
    private Double month12ConsumptionValue;
    private Double allMonthsConsumptionValue;
    private Double consumptionPercentage;
    private Double aggregatedPercentage;
    private String className;
}