package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_pm_purchase_plan_calculations")
public class RscIotPMPurchasePlanCalculations extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "rsc_it_item_wise_supplier_id")
    private RscItItemWiseSupplier rscItItemWiseSupplier;

    @ManyToOne
    @JoinColumn(name = "pm_gross_consumption_id")
    private RscItPMGrossConsumption rscItPMGrossConsumption;

    @ManyToOne
    @JoinColumn(name = "pm_item_id")
    private RscMtItem rscMtItem;

    @OneToOne(mappedBy = "pmPurchasePlanCalculations")
    private RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays;

    private int priority;

    @Column(name = "month_1_month_end_stock")
    private Double month1MonthEndStock;

    @Column(name = "month_1_stock_after_consumption")
    private Double month1StockAfterConsumption;

    @Column(name = "month_1_receipt_till_date")
    private Double month1ReceiptTillDate;

    @Column(name = "month_1_closing_stock")
    private Double month1ClosingStock;

    @Column(name = "month_1_suggested_supply")
    private Double month1SuggestedSupply;

    @Column(name = "month_1_on_hand_stock")
    private Double month1OnHandStock;

    @Column(name = "month_1_priority_supply")
    private Double month1PrioritySupply;

    @Column(name = "month_1_final_supply")
    private Double month1FinalSupply;

    @Column(name = "month_2_month_end_stock")
    private Double month2MonthEndStock;

    @Column(name = "month_2_stock_after_consumption")
    private Double month2StockAfterConsumption;

    @Column(name = "month_2_receipt_till_date")
    private Double month2ReceiptTillDate;

    @Column(name = "month_2_closing_stock")
    private Double month2ClosingStock;

    @Column(name = "month_2_final_supply")
    private Double month2FinalSupply;

    @Column(name = "month_3_month_end_stock")
    private Double month3MonthEndStock;

    @Column(name = "month_3_stock_after_consumption")
    private Double month3StockAfterConsumption;

    @Column(name = "month_3_receipt_till_date")
    private Double month3ReceiptTillDate;

    @Column(name = "month_3_closing_stock")
    private Double month3ClosingStock;

    @Column(name = "month_3_final_supply")
    private Double month3FinalSupply;

    @Column(name = "month_4_month_end_stock")
    private Double month4MonthEndStock;

    @Column(name = "month_4_stock_after_consumption")
    private Double month4StockAfterConsumption;

    @Column(name = "month_4_receipt_till_date")
    private Double month4ReceiptTillDate;

    @Column(name = "month_4_closing_stock")
    private Double month4ClosingStock;

    @Column(name = "month_4_final_supply")
    private Double month4FinalSupply;

    @Column(name = "month_5_month_end_stock")
    private Double month5MonthEndStock;

    @Column(name = "month_5_stock_after_consumption")
    private Double month5StockAfterConsumption;

    @Column(name = "month_5_receipt_till_date")
    private Double month5ReceiptTillDate;

    @Column(name = "month_5_closing_stock")
    private Double month5ClosingStock;

    @Column(name = "month_5_final_supply")
    private Double month5FinalSupply;

    @Column(name = "month_6_month_end_stock")
    private Double month6MonthEndStock;

    @Column(name = "month_6_stock_after_consumption")
    private Double month6StockAfterConsumption;

    @Column(name = "month_6_receipt_till_date")
    private Double month6ReceiptTillDate;

    @Column(name = "month_6_closing_stock")
    private Double month6ClosingStock;

    @Column(name = "month_6_final_supply")
    private Double month6FinalSupply;

    @Column(name = "month_7_month_end_stock")
    private Double month7MonthEndStock;

    @Column(name = "month_7_stock_after_consumption")
    private Double month7StockAfterConsumption;

    @Column(name = "month_7_receipt_till_date")
    private Double month7ReceiptTillDate;

    @Column(name = "month_7_closing_stock")
    private Double month7ClosingStock;

    @Column(name = "month_7_final_supply")
    private Double month7FinalSupply;

    @Column(name = "month_8_month_end_stock")
    private Double month8MonthEndStock;

    @Column(name = "month_8_stock_after_consumption")
    private Double month8StockAfterConsumption;

    @Column(name = "month_8_receipt_till_date")
    private Double month8ReceiptTillDate;

    @Column(name = "month_8_closing_stock")
    private Double month8ClosingStock;

    @Column(name = "month_8_final_supply")
    private Double month8FinalSupply;

    @Column(name = "month_9_month_end_stock")
    private Double month9MonthEndStock;

    @Column(name = "month_9_stock_after_consumption")
    private Double month9StockAfterConsumption;

    @Column(name = "month_9_receipt_till_date")
    private Double month9ReceiptTillDate;

    @Column(name = "month_9_closing_stock")
    private Double month9ClosingStock;

    @Column(name = "month_9_final_supply")
    private Double month9FinalSupply;

    @Column(name = "month_10_month_end_stock")
    private Double month10MonthEndStock;

    @Column(name = "month_10_stock_after_consumption")
    private Double month10StockAfterConsumption;

    @Column(name = "month_10_receipt_till_date")
    private Double month10ReceiptTillDate;

    @Column(name = "month_10_closing_stock")
    private Double month10ClosingStock;

    @Column(name = "month_10_final_supply")
    private Double month10FinalSupply;

    @Column(name = "month_11_month_end_stock")
    private Double month11MonthEndStock;

    @Column(name = "month_11_stock_after_consumption")
    private Double month11StockAfterConsumption;

    @Column(name = "month_11_receipt_till_date")
    private Double month11ReceiptTillDate;

    @Column(name = "month_11_closing_stock")
    private Double month11ClosingStock;

    @Column(name = "month_11_final_supply")
    private Double month11FinalSupply;

    @Column(name = "month_12_month_end_stock")
    private Double month12MonthEndStock;

    @Column(name = "month_12_stock_after_consumption")
    private Double month12StockAfterConsumption;

    @Column(name = "month_12_receipt_till_date")
    private Double month12ReceiptTillDate;

    @Column(name = "month_12_closing_stock")
    private Double month12ClosingStock;

    @Column(name = "month_12_final_supply")
    private Double month12FinalSupply;

    @Column(name = "month_1_open_po")
    private Double month1OpenPo;

    @Column(name = "month_2_open_po")
    private Double month2OpenPo;

    @Column(name = "month_3_open_po")
    private Double month3OpenPo;

    @Column(name = "month_4_open_po")
    private Double month4OpenPo;

    @Column(name = "month_5_open_po")
    private Double month5OpenPo;

    @Column(name = "month_6_open_po")
    private Double month6OpenPo;

    @Column(name = "month_7_open_po")
    private Double month7OpenPo;

    @Column(name = "month_8_open_po")
    private Double month8OpenPo;

    @Column(name = "month_9_open_po")
    private Double month9OpenPo;

    @Column(name = "month_10_open_po")
    private Double month10OpenPo;

    @Column(name = "month_11_open_po")
    private Double month11OpenPo;

    @Column(name = "month_12_open_po")
    private Double month12OpenPo;
}