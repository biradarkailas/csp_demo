package com.example.validation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "party_contact_details")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartyContactDetails extends BaseIdEntity {
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "PARTY_ID")
    private Party party;
    private String mobileNoOne;
    private String mobileNoTwo;
    private String landLineOne;
    private String landLineTwo;
    private String faxOne;
    private String faxTwo;
    private String emailIdOne;
    private String emailIdTwo;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;
}
