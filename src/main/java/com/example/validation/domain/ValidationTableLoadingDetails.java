package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "validation_table_loading_details")
public class ValidationTableLoadingDetails extends BaseIdEntity {
    private String inputFileName;
    private String procedureName;
    private boolean isValidated;
    private LocalDateTime startLoadDate;
    private LocalDateTime endLoadDate;
    private int recordCount;
}
