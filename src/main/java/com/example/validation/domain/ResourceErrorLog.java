package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "resource_error_log")
public class ResourceErrorLog extends BaseIdEntity {
    private String procedureName;
    private String fieldName;
    private String errorDescription;
    private LocalDateTime createdDate;
}