package com.example.validation.domain;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_item_code")
public class RscDtItemCode extends BaseIdEntity{
   private String code;
   private String description;
}
