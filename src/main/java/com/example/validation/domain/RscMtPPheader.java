package com.example.validation.domain;

import com.example.validation.util.enums.ModuleName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_ppheader")
@Cache(usage = CacheConcurrencyStrategy.NONSTRICT_READ_WRITE)
public class RscMtPPheader extends BaseIdEntity {
    private String mpsName;
    private LocalDate mpsDate;
    private Boolean active;
    private LocalDate otifDate;

    @CreationTimestamp
    private LocalDateTime createdDate;

    @UpdateTimestamp
    private LocalDateTime modifiedDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "module_name")
    private ModuleName moduleName;

    @ManyToOne
    @JoinColumn(name = "tag_id")
    private RscDtTag rscDtTag;

    @OneToMany(mappedBy = "rscMtPPheader", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<RscMtPPdetails> rscMtPPdetailsList;

    private Boolean isExecutionDone;

    private Boolean isBackUpDone;

}