package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Table(name = "rsc_iit_idp_status")
@Entity
@Data
public class RscIitIdpStatus extends BaseIdEntity {
    private String mpsName;
    private LocalDate mpsDate;
    private Boolean isActive;
    private Boolean isRequired;
    private Boolean uploadStatus;
    private Boolean validationStatus;
    private Boolean resourceStatus;

    @CreationTimestamp
    private LocalDateTime createdDate;

    @UpdateTimestamp
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_file_name_id")
    private RscDtFileName rscDtFileName;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_tag_id")
    private RscDtTag rscDtTag;

    private LocalDate otifDate;
    private Boolean isExecutionDone;
}