package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_pm_otif_receipts")
public class RscMtPmOtifReceipts extends BaseIdEntity {
    private LocalDate receiptDate;
    private Double quantity;
    private String receiptNumber;
    private String orderNumber;
    private String packingSlipNumber;
    private String supplierLotNumber;
    private String lotNumber;
    private String poNumber;
    private LocalDate createdDate;
    private LocalDate modifiedDate;
    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_supplier_id")
    private RscDtSupplier rscDtSupplier;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_otif_pm_receipts_header_id")
    private RscMtOtifPmReceiptsHeader rscMtOtifPmReceiptsHeader;
}
