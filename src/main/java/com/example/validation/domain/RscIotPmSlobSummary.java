package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_pm_slob_summary")
public class RscIotPmSlobSummary extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
    private Double totalStockValue;
    private Double sitValue;
    private Double totalStockSitValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
    private Double stockQualityPercentage;
    private Double stockQuantity;
}
