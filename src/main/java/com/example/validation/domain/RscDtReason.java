package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "rsc_dt_reason")
public class RscDtReason extends BaseIdEntity {
    private String reason;
    private String description;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_reason_type_id")
    private RscDtReasonType rscDtReasonType;
}





