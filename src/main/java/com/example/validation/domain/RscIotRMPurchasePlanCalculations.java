package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_rm_purchase_plan_calculations")
public class RscIotRMPurchasePlanCalculations extends BaseIdEntity {

    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

    @OneToOne
    @JoinColumn(name = "rm_gross_consumption_id")
    private RscItRMGrossConsumption rscItRMGrossConsumption;

    @ManyToOne
    @JoinColumn(name = "item_wise_supplier_details_id")
    private RscIitRMItemWiseSupplierDetails rscIitRMItemWiseSupplierDetails;

    @OneToOne(mappedBy = "rmPurchasePlanCalculations")
    private RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays;

    @ManyToOne
    @JoinColumn(name = "rm_item_id")
    private RscMtItem rscMtItem;

    @Column(name = "month_1_month_end_stock")
    private Double month1OpeningStock;

    @Column(name = "month_1_stock_after_consumption")
    private Double month1StockAfterConsumption;

    @Column(name = "month_1_receipt_till_date")
    private Double month1OpenPoStock;

    @Column(name = "month_1_closing_stock")
    private Double month1ClosingStock;

    @Column(name = "month_1_final_supply")
    private Double month1SuggestedPurchasePlan;

    @Column(name = "month_1_obsolete_stock")
    private Double month1ObsoleteStock;

    @Column(name = "month_2_month_end_stock")
    private Double month2OpeningStock;

    @Column(name = "month_2_stock_after_consumption")
    private Double month2StockAfterConsumption;

    @Column(name = "month_2_receipt_till_date")
    private Double month2OpenPoStock;

    @Column(name = "month_2_closing_stock")
    private Double month2ClosingStock;

    @Column(name = "month_2_final_supply")
    private Double month2SuggestedPurchasePlan;

    @Column(name = "month_2_obsolete_stock")
    private Double month2ObsoleteStock;

    @Column(name = "month_3_month_end_stock")
    private Double month3OpeningStock;

    @Column(name = "month_3_stock_after_consumption")
    private Double month3StockAfterConsumption;

    @Column(name = "month_3_receipt_till_date")
    private Double month3OpenPoStock;

    @Column(name = "month_3_closing_stock")
    private Double month3ClosingStock;

    @Column(name = "month_3_final_supply")
    private Double month3SuggestedPurchasePlan;

    @Column(name = "month_3_obsolete_stock")
    private Double month3ObsoleteStock;

    @Column(name = "month_4_month_end_stock")
    private Double month4OpeningStock;

    @Column(name = "month_4_stock_after_consumption")
    private Double month4StockAfterConsumption;

    @Column(name = "month_4_receipt_till_date")
    private Double month4OpenPoStock;

    @Column(name = "month_4_closing_stock")
    private Double month4ClosingStock;

    @Column(name = "month_4_final_supply")
    private Double month4SuggestedPurchasePlan;

    @Column(name = "month_4_obsolete_stock")
    private Double month4ObsoleteStock;

    @Column(name = "month_5_month_end_stock")
    private Double month5OpeningStock;

    @Column(name = "month_5_stock_after_consumption")
    private Double month5StockAfterConsumption;

    @Column(name = "month_5_receipt_till_date")
    private Double month5OpenPoStock;

    @Column(name = "month_5_closing_stock")
    private Double month5ClosingStock;

    @Column(name = "month_5_final_supply")
    private Double month5SuggestedPurchasePlan;

    @Column(name = "month_5_obsolete_stock")
    private Double month5ObsoleteStock;

    @Column(name = "month_6_month_end_stock")
    private Double month6OpeningStock;

    @Column(name = "month_6_stock_after_consumption")
    private Double month6StockAfterConsumption;

    @Column(name = "month_6_receipt_till_date")
    private Double month6OpenPoStock;

    @Column(name = "month_6_closing_stock")
    private Double month6ClosingStock;

    @Column(name = "month_6_final_supply")
    private Double month6SuggestedPurchasePlan;

    @Column(name = "month_6_obsolete_stock")
    private Double month6ObsoleteStock;

    @Column(name = "month_7_month_end_stock")
    private Double month7OpeningStock;

    @Column(name = "month_7_stock_after_consumption")
    private Double month7StockAfterConsumption;

    @Column(name = "month_7_receipt_till_date")
    private Double month7OpenPoStock;

    @Column(name = "month_7_closing_stock")
    private Double month7ClosingStock;

    @Column(name = "month_7_final_supply")
    private Double month7SuggestedPurchasePlan;

    @Column(name = "month_7_obsolete_stock")
    private Double month7ObsoleteStock;

    @Column(name = "month_8_month_end_stock")
    private Double month8OpeningStock;

    @Column(name = "month_8_stock_after_consumption")
    private Double month8StockAfterConsumption;

    @Column(name = "month_8_receipt_till_date")
    private Double month8OpenPoStock;

    @Column(name = "month_8_closing_stock")
    private Double month8ClosingStock;

    @Column(name = "month_8_final_supply")
    private Double month8SuggestedPurchasePlan;

    @Column(name = "month_8_obsolete_stock")
    private Double month8ObsoleteStock;

    @Column(name = "month_9_month_end_stock")
    private Double month9OpeningStock;

    @Column(name = "month_9_stock_after_consumption")
    private Double month9StockAfterConsumption;

    @Column(name = "month_9_receipt_till_date")
    private Double month9OpenPoStock;

    @Column(name = "month_9_closing_stock")
    private Double month9ClosingStock;

    @Column(name = "month_9_final_supply")
    private Double month9SuggestedPurchasePlan;

    @Column(name = "month_9_obsolete_stock")
    private Double month9ObsoleteStock;

    @Column(name = "month_10_month_end_stock")
    private Double month10OpeningStock;

    @Column(name = "month_10_stock_after_consumption")
    private Double month10StockAfterConsumption;

    @Column(name = "month_10_receipt_till_date")
    private Double month10OpenPoStock;

    @Column(name = "month_10_closing_stock")
    private Double month10ClosingStock;

    @Column(name = "month_10_final_supply")
    private Double month10SuggestedPurchasePlan;

    @Column(name = "month_10_obsolete_stock")
    private Double month10ObsoleteStock;

    @Column(name = "month_11_month_end_stock")
    private Double month11OpeningStock;

    @Column(name = "month_11_stock_after_consumption")
    private Double month11StockAfterConsumption;

    @Column(name = "month_11_receipt_till_date")
    private Double month11OpenPoStock;

    @Column(name = "month_11_closing_stock")
    private Double month11ClosingStock;

    @Column(name = "month_11_final_supply")
    private Double month11SuggestedPurchasePlan;

    @Column(name = "month_11_obsolete_stock")
    private Double month11ObsoleteStock;

    @Column(name = "month_12_month_end_stock")
    private Double month12OpeningStock;

    @Column(name = "month_12_stock_after_consumption")
    private Double month12StockAfterConsumption;

    @Column(name = "month_12_receipt_till_date")
    private Double month12OpenPoStock;

    @Column(name = "month_12_closing_stock")
    private Double month12ClosingStock;

    @Column(name = "month_12_final_supply")
    private Double month12SuggestedPurchasePlan;

    @Column(name = "month_12_obsolete_stock")
    private Double month12ObsoleteStock;
}