package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_state")
public class RscDtState extends BaseIdEntity {
    private String StateName;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_country_id")
    private RscDtCountry rscDtCountry;
}

