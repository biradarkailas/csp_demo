package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_skids")
public class RscDtSkids extends BaseIdEntity {
    private String name;
    private Double shifts;
    private Double batchesPerShift;
    private Double hrsPerShift;
}