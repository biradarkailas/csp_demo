package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_rm_latest_slob_reason")
public class RscIotRmLatestSlobReason extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_reason_id")
    private RscDtReason rscDtReason;
    private String remark;
}

