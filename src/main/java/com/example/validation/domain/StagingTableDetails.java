package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "staging_table_details")
public class StagingTableDetails extends BaseIdEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String csvFileName;
    private String tableName;
    private boolean hasHeader;
    private int executionSequence;
    private String validationProcedureName;
    private int resourceSequence;
    private String resourceProcedureName;
}
