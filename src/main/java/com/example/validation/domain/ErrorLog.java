package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "error_log")
public class ErrorLog extends BaseIdEntity {
    private String fileName;
    private String procedureName;
    private String fieldName;
    private String errorDescription;
    private LocalDateTime createdDate;
}