package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_fg_type_division_slob_summary")
public class RscIotFGTypeDivisionSlobSummary extends BaseIdEntity {
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double stockQualityPercentage;

    @ManyToOne
    @JoinColumn(name = "fg_type_id")
    private RscDtFGType rscDtFGType;

    @ManyToOne
    @JoinColumn(name = "division_id")
    private RscDtDivision rscDtDivision;

    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
    private Double stockQualityValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
}
