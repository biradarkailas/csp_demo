package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_ppdetails")
public class RscMtPPdetails extends BaseIdEntity {

    @Column(name = "m1_idealvalue")
    private Double m1Idealvalue;

    @Column(name = "m1_value")
    private Double m1Value;

    @Column(name = "m2_value")
    private Double m2Value;

    @Column(name = "m3_value")
    private Double m3Value;

    @Column(name = "m4_value")
    private Double m4Value;

    @Column(name = "m5_value")
    private Double m5Value;

    @Column(name = "m6_value")
    private Double m6Value;

    @Column(name = "m7_value")
    private Double m7Value;

    @Column(name = "m8_value")
    private Double m8Value;

    @Column(name = "m9_value")
    private Double m9Value;

    @Column(name = "m10_value")
    private Double m10Value;

    @Column(name = "m11_value")
    private Double m11Value;

    @Column(name = "fla")
    private String fla;

    @Column(name = "m12_value")
    private Double m12Value;
    private Double total;
    private boolean priority;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_ppheader_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_production_type_id")
    private RscDtProductionType rscDtProductionType;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_line_id")
    private RscDtLines rscDtLines;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_skid_id")
    private RscDtSkids rscDtSkids;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_division_id")
    private RscDtDivision rscDtDivision;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_brand_id")
    private RscDtBrand rscDtBrand;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_signature_id")
    private RscDtSignature rscDtSignature;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_material_category_id")
    private RscDtMaterialCategory rscDtMaterialCategory;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_material_sub_category_id")
    private RscDtMaterialSubCategory rscDtMaterialSubCategory;
}