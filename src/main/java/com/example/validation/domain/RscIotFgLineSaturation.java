package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_fg_line_saturation")
public class RscIotFgLineSaturation extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_line_id")
    private RscDtLines rscDtLines;
    private Double m1TotalMpsPlanQty;
    private Double m2TotalMpsPlanQty;
    private Double m3TotalMpsPlanQty;
    private Double m4TotalMpsPlanQty;
    private Double m5TotalMpsPlanQty;
    private Double m6TotalMpsPlanQty;
    private Double m7TotalMpsPlanQty;
    private Double m8TotalMpsPlanQty;
    private Double m9TotalMpsPlanQty;
    private Double m10TotalMpsPlanQty;
    private Double m11TotalMpsPlanQty;
    private Double m12TotalMpsPlanQty;
    private Double month1Capacity;
    private Double month2Capacity;
    private Double month3Capacity;
    private Double month4Capacity;
    private Double month5Capacity;
    private Double month6Capacity;
    private Double month7Capacity;
    private Double month8Capacity;
    private Double month9Capacity;
    private Double month10Capacity;
    private Double month11Capacity;
    private Double month12Capacity;
    private Double m1LineSaturation;
    private Double m2LineSaturation;
    private Double m3LineSaturation;
    private Double m4LineSaturation;
    private Double m5LineSaturation;
    private Double m6LineSaturation;
    private Double m7LineSaturation;
    private Double m8LineSaturation;
    private Double m9LineSaturation;
    private Double m10LineSaturation;
    private Double m11LineSaturation;
    private Double m12LineSaturation;
}