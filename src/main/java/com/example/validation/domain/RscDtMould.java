package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "rsc_dt_mould")
public class RscDtMould extends BaseIdEntity {
    private String mould;
    private String childMould;
    private Double noOfCavities;
    private Double perCavity;
    private Double totalCapacity;
    private boolean active;
}