package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_it_item_mould")
public class RscItItemMould extends BaseIdEntity {
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @OneToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_mould_id")
    private RscDtMould rscDtMould;
}