package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_mps_plan_total_quantity")
public class RscIotMPSPlanTotalQuantity extends BaseIdEntity {
    @Column(name = "m1_total_value")
    private Double m1TotalValue;

    @Column(name = "m2_total_value")
    private Double m2TotalValue;

    @Column(name = "m3_total_value")
    private Double m3TotalValue;

    @Column(name = "m4_total_value")
    private Double m4TotalValue;

    @Column(name = "m5_total_value")
    private Double m5TotalValue;

    @Column(name = "m6_total_value")
    private Double m6TotalValue;

    @Column(name = "m7_total_value")
    private Double m7TotalValue;

    @Column(name = "m8_total_value")
    private Double m8TotalValue;

    @Column(name = "m9_total_value")
    private Double m9TotalValue;

    @Column(name = "m10_total_value")
    private Double m10TotalValue;

    @Column(name = "m11_total_value")
    private Double m11TotalValue;

    @Column(name = "m12_total_value")
    private Double m12TotalValue;

    private String mpsPlanName;
    private int mpsPlanSequence;

    @ManyToOne
    @JoinColumn(name = "mps_id")
    private RscMtPPheader rscMtPPheader;
}