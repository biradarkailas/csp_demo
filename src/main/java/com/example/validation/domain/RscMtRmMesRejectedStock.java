package com.example.validation.domain;

import com.example.validation.dto.StockPaginationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@NamedNativeQuery(
        name = "Mes_Rejected_Stock",
        query =
                "select vew_item.code as itemCode,vew_item.description as itemDescription,csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date as stockDate,rsc_mt_item_id as rscMtItemId,csp_loreal.rsc_iot_rm_stock.quantity as quantity,csp_loreal.rsc_mt_rm_mes_rejected_stock.quantity as lotWiseStock,lot_number as lotNumber,location,stock_status as stockStatus from csp_loreal.rsc_iot_rm_stock, csp_loreal.rsc_mt_rm_mes_rejected_stock,vew_item where concat(year(rsc_mt_rm_mes_rejected_stock.stock_date),month(rsc_mt_rm_mes_rejected_stock.stock_date))=?1 and csp_loreal.rsc_mt_rm_mes_rejected_stock.stock_date=csp_loreal.rsc_iot_rm_stock.stock_date and rsc_mt_item_id=rm_item_id and stock_type_id in(SELECT id FROM csp_loreal.rsc_dt_stock_type where tag=?2) and vew_item.mt_item_id=rsc_mt_item_id",
        resultSetMapping = "StockPaginationDto"
)
@SqlResultSetMapping(
        name = "StockPaginationDto",
        classes = @ConstructorResult(
                targetClass = StockPaginationDto.class,
                columns = {
                        @ColumnResult(name = "itemCode", type = String.class),
                        @ColumnResult(name = "itemDescription", type = String.class),
                        @ColumnResult(name = "stockDate", type = LocalDate.class),
                        @ColumnResult(name = "rscMtItemId", type = Long.class),
                        @ColumnResult(name = "quantity", type = Double.class),
                        @ColumnResult(name = "lotWiseStock", type = Double.class),
                        @ColumnResult(name = "lotNumber", type = String.class),
                        @ColumnResult(name = "location", type = String.class),
                        @ColumnResult(name = "stockStatus", type = String.class)
                }
        )
)
@Data
@Table(name = "rsc_mt_rm_mes_rejected_stock")
public class RscMtRmMesRejectedStock extends BaseIdEntity {
    private LocalDate stockDate;
    private Double quantity;
    private String lotNumber;
    private String location;
    private String stockStatus;
    private String documentNumber;
    private String country;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;
}