package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_pm_open_po")
public class RscMtPMOpenPo extends BaseIdEntity {
    private String poNumber;
    private LocalDate poDate;
    private Double orderedQuantity;
    private Double receivedQuantity;
    private LocalDate deliveryDate;
    private Integer paymentMode;
    private LocalDate dueDate;
    private Double rate;
    private String currency;
    private LocalDate sailingDate;
    private LocalDate landingDate;

    @CreatedDate
    private LocalDateTime createdDate;

    @LastModifiedDate
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_supplier_id")
    private RscMtSupplier rscMtSupplier;

    @ManyToOne
    @JoinColumn(name = "mps_id")
    private RscMtPPheader rscMtPPheader;
}