package com.example.validation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;

/*********************************************
 #######################@@@@@@@@@@@@@@@@@@@@@@@
 **********@author - Rhishikesh Akole**********
 **********date - 2018-12-07T11:42*************
 **********class - Country
#######################@@@@@@@@@@@@@@@@@@@@@@@
 *********************************************/
@Entity
@Table(name = "country")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class Country extends BaseIdEntity {
    private String name;
}
