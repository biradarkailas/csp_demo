package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_rm_slob_summary")
public class RscIotRmSlobSummary extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
    private Double totalStockValue;
    private Double sitValue;
    private Double totalStockSitValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteExpiryValue;
    private Double totalObsoleteNonExpiryValue;
    private Double totalObsoleteItemValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
    private Double stockQualityPercentage;
    private Double stockQualityValue;
    private Double totalMidNightStockValue;
    private Double totalExpiredStockValue;
    private Double totalFinalStockValue;
    private Double m1TotalQuantity;
    private Double m2TotalQuantity;
    private Double m3TotalQuantity;
    private Double m4TotalQuantity;
    private Double m5TotalQuantity;
    private Double m6TotalQuantity;
    private Double m7TotalQuantity;
    private Double m8TotalQuantity;
    private Double m9TotalQuantity;
    private Double m10TotalQuantity;
    private Double m11TotalQuantity;
    private Double m12TotalQuantity;
    private Double totalConsumptionQuantity;
    private Double obsoleteExpiryValuePercentage;
    private Double obsoleteNonExpiryValuePercentage;
    private Double totalSlobQuantityValue;
}