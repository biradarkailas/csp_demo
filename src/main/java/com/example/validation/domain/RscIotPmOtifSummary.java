package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_pm_otif_Summary")
public class RscIotPmOtifSummary extends BaseIdEntity {

    private LocalDate otifSummaryDate;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private RscDtSupplier rscDtSupplier;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_ppheader_id")
    private RscMtPPheader rscMtPPheader;
    private LocalDate receiptHeaderDate;

    private Double avgReceiptPercentage;
    private Integer avgScore;
}
