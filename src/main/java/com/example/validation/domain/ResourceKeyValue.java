package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "resource_key_value")
public class ResourceKeyValue extends BaseIdEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String resourceKey;
    private String resourceValue;
    private Boolean isDelete;
}
