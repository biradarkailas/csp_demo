package com.example.validation.domain;

import lombok.*;

import javax.persistence.*;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_cover_days")
public class RscDtCoverDays extends BaseIdEntity {
    private Double coverDays;
}
