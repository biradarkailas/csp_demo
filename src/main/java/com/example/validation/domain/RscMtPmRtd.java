package com.example.validation.domain;

import com.example.validation.dto.PmRtdStockPaginationDto;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Entity

@NamedNativeQuery(name = "Pm_Rtd_Stock",
        query = "select csp_loreal.vew_item.code as itemCode,csp_loreal.vew_item.description as itemDescription,csp_loreal.rsc_mt_PM_rtd.receipt_date as receiptDate,rsc_mt_item_id as rscMtItemId,csp_loreal.rsc_iot_pm_stock.quantity as totalReceiptStock,csp_loreal.rsc_mt_PM_rtd.quantity as lotWiseReceiptStock,csp_loreal.rsc_mt_PM_rtd.receipt_number as receiptNumber,lot_number as lotNumber,packing_slip_number as poNumber,Supplier_name as supplierName from csp_loreal.rsc_iot_pm_stock,csp_loreal.rsc_mt_PM_rtd,csp_loreal.vew_item,csp_loreal.vew_supplier where concat(year(csp_loreal.rsc_iot_pm_stock.stock_date),month(csp_loreal.rsc_iot_pm_stock.stock_date))=?1 and concat(year(receipt_date),month(receipt_date))=?1 and rsc_mt_item_id=pm_item_id and stock_type_id in(Select id from csp_loreal.rsc_dt_stock_type where tag=?2) and csp_loreal.vew_item.mt_item_id=rsc_mt_item_id and csp_loreal.vew_supplier.rsc_dt_supplier_id=csp_loreal.rsc_mt_PM_rtd.rsc_dt_supplier_id",
        resultSetMapping = "PmRtdStockPaginationDto")
@SqlResultSetMapping(name = "PmRtdStockPaginationDto",
        classes = @ConstructorResult(targetClass = PmRtdStockPaginationDto.class, columns = {
                @ColumnResult(name = "itemCode", type = String.class),
                @ColumnResult(name = "itemDescription", type = String.class),
                @ColumnResult(name = "receiptDate", type = LocalDate.class),
                @ColumnResult(name = "rscMtItemId", type = Long.class),
                @ColumnResult(name = "totalReceiptStock", type = Double.class),
                @ColumnResult(name = "lotWiseReceiptStock", type = Double.class),
                @ColumnResult(name = "lotNumber", type = String.class),
                @ColumnResult(name = "receiptNumber", type = String.class),
                @ColumnResult(name = "supplierName", type = String.class),
                @ColumnResult(name = "poNumber", type = String.class)}))
@Data
@Table(name = "rsc_mt_pm_rtd")
public class RscMtPmRtd extends BaseIdEntity {
    private LocalDate receiptDate;
    private Double quantity;
    private String receiptNumber;
    private String orderNumber;
    private String packingSlipNumber;
    private String supplierLotNumber;
    private String lotNumber;
    private String poNumber;
    private LocalDate createdDate;
    private LocalDate modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_supplier_id")
    private RscDtSupplier rscDtSupplier;
}