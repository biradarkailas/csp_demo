package com.example.validation.domain;

import com.example.validation.util.enums.SlobType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_fg_slob")
public class RscIotFGSlob extends BaseIdEntity {
    private Double mapPrice;
    private Double stockQuantity;
    private Double stockValue;
    private Double slobQuantity;
    private Double slobValue;
    private Double totalPlanQuantity;

    @Enumerated(EnumType.STRING)
    @Column(name = "slob_type")
    private SlobType slobType;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_pm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "mps_id")
    private RscMtPPheader rscMtPPHeader;
}