package com.example.validation.domain;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_division")
public class RscDtDivision extends BaseIdEntity{
    private String aliasName;
    private String fullName;
}
