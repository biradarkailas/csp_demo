package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_original_mould_saturation")
public class RscIotOriginalMouldSaturation extends BaseIdEntity {

    @Column(name = "m1_value")
    private Long m1Value;

    @Column(name = "m2_value")
    private Long m2Value;

    @Column(name = "m3_value")
    private Long m3Value;

    @Column(name = "m4_value")
    private Long m4Value;

    @Column(name = "m5_value")
    private Long m5Value;

    @Column(name = "m6_value")
    private Long m6Value;

    @Column(name = "m7_value")
    private Long m7Value;

    @Column(name = "m8_value")
    private Long m8Value;

    @Column(name = "m9_value")
    private Long m9Value;

    @Column(name = "m10_value")
    private Long m10Value;

    @Column(name = "m11_value")
    private Long m11Value;

    @Column(name = "m12_value")
    private Long m12Value;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_mould_id")
    private RscDtMould rscDtMould;

    @ManyToOne
    @JoinColumn(name = "rsc_iot_pm_original_purchase_plan_id")
    private RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan;

    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

}
