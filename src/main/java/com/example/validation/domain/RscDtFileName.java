package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Table(name = "rsc_dt_file_name")
@Data
@Entity
public class RscDtFileName extends BaseIdEntity {
    private String fileName;
    private String extension;
    private Boolean fcRequired;
    private Boolean vdRequired;
    private Boolean psRequired;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_folder_name_id")
    private RscDtFolderName rscDtFolderName;
}
