package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "pm_otif_cumulative_score")
public class PmOtifCumulativeScore extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private RscDtSupplier rscDtSupplier;
    private Double averagePercentage;
    private Double maxPercentage;
    private Double minPercentage;
    private Double score;
}
