package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_fg_skid_saturation")
public class RscIotFgSkidSaturation extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_skids_id")
    private RscDtSkids rscDtSkids;
    private Double m1TotalRatioOfPlanQtyWithTs;
    private Double m2TotalRatioOfPlanQtyWithTs;
    private Double m3TotalRatioOfPlanQtyWithTs;
    private Double m4TotalRatioOfPlanQtyWithTs;
    private Double m5TotalRatioOfPlanQtyWithTs;
    private Double m6TotalRatioOfPlanQtyWithTs;
    private Double m7TotalRatioOfPlanQtyWithTs;
    private Double m8TotalRatioOfPlanQtyWithTs;
    private Double m9TotalRatioOfPlanQtyWithTs;
    private Double m10TotalRatioOfPlanQtyWithTs;
    private Double m11TotalRatioOfPlanQtyWithTs;
    private Double m12TotalRatioOfPlanQtyWithTs;
    private Double month1Capacity;
    private Double month2Capacity;
    private Double month3Capacity;
    private Double month4Capacity;
    private Double month5Capacity;
    private Double month6Capacity;
    private Double month7Capacity;
    private Double month8Capacity;
    private Double month9Capacity;
    private Double month10Capacity;
    private Double month11Capacity;
    private Double month12Capacity;
    private Double m1SkidSaturation;
    private Double m2SkidSaturation;
    private Double m3SkidSaturation;
    private Double m4SkidSaturation;
    private Double m5SkidSaturation;
    private Double m6SkidSaturation;
    private Double m7SkidSaturation;
    private Double m8SkidSaturation;
    private Double m9SkidSaturation;
    private Double m10SkidSaturation;
    private Double m11SkidSaturation;
    private Double m12SkidSaturation;
}