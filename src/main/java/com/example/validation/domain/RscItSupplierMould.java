package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_it_supplier_mould")
public class RscItSupplierMould extends BaseIdEntity{
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_it_item_mould_id")
    private RscItItemMould rscItItemMould;
}
