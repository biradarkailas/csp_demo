package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_address")
public class RscDtAddress extends BaseIdEntity {
    private String addressLine1;
    private String addressLine2;
    private String area;
    private String pincode;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_city_id")
    private RscDtCity rscDtCity;
}
