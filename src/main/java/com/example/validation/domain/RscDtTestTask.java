package com.example.validation.domain;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name="rsc_dt_test_task")
public class RscDtTestTask extends BaseIdEntity{
    private String testTaskName;
    private String testTableName;
}
