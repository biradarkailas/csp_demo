package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_supplier")
public class RscDtSupplier extends BaseIdEntity {
    @Column(unique = true)
    private String supplierName;
    private String supplierCode;
    private boolean active;
}