package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_rm_list_gen", uniqueConstraints = @UniqueConstraint(columnNames = {"rsc_mt_item_id", "lotNumber"}))
public class RscMtRmList extends BaseIdEntity {
    private Double quantity;
    private String lotNumber;
    private LocalDate expirationDate;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;
    private LocalDate mpsDate;
}
