package com.example.validation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "party_relationship_type")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartyRelationshipType extends BaseIdEntity {
    private String name;
    private String firstRoleName;
    private String secondRoleName;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;
}
