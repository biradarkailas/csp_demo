package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_pm_otif_calculation")
public class RscIotPmOtifCalculation extends BaseIdEntity {

    private LocalDate otifCalcDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_ppheader_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "pm_previous_month_supply_id")
    private RscIotPMLatestPurchasePlan rscIotPMLatestPurchasePlan;

    @ManyToOne
    @JoinColumn(name = "pm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private RscDtSupplier rscDtSupplier;

    private LocalDate receiptHeaderDate;
    private Double totalOrderQty;
    private Double totalReceivedQty;
    private Double balanceQty;
    private Double receiptPercentage;
    private Integer score;
    private LocalDateTime createdDate;
}