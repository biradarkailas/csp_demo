package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name = "rsc_dt_pm_remark")
public class RscDtPMRemark extends BaseIdEntity {
    private String name;
    private String aliasName;
}