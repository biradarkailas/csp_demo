package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "resource_table_upload_summary")
public class ResourceTableUploadSummary extends BaseIdEntity {
    private String tableName;
    private String procedureName;
    private LocalDateTime startLoadDate;
    private LocalDateTime endLoadDate;
    private int recordCount;
    private Boolean isLoadSuccess;
}
