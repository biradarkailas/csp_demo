package com.example.validation.domain;

import lombok.*;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_item_category")
public class RscDtItemCategory extends BaseIdEntity {
    private String category;
    private String description;
}
