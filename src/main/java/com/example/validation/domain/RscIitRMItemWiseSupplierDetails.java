package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iit_rm_item_wise_supplier_details")
public class RscIitRMItemWiseSupplierDetails extends BaseIdEntity {
    private String halalComments;
    private String otherComments;
    private String orderType;

    @OneToOne
    @JoinColumn(name = "item_wise_supplier_id")
    private RscItItemWiseSupplier rscItItemWiseSupplier;

    @OneToOne
    @JoinColumn(name = "supplier_wise_transport_id")
    private RscIitSupplierWiseTransport rscIitSupplierWiseTransport;
}