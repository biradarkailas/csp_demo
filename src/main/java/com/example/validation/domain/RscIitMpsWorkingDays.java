package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iit_mps_working_Days")
public class RscIitMpsWorkingDays extends BaseIdEntity{
    @OneToOne
    @JoinColumn(name = "mps_id")
    private RscMtPPheader rscMtPPheader;
    private LocalDate mpsDate;
    private Double month1Value;
    private Double month2Value;
    private Double month3Value;
    private Double month4Value;
    private Double month5Value;
    private Double month6Value;
    private Double month7Value;
    private Double month8Value;
    private Double month9Value;
    private Double month10Value;
    private Double month11Value;
    private Double month12Value;
}
