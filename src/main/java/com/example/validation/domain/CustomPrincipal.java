package com.example.validation.domain;

import java.io.Serializable;

/*********************************************
 #######################@@@@@@@@@@@@@@@@@@@@@@@
 **********@author - Rhishikesh Akole**********
 @date - 2018-11-22T14:37
 #######################@@@@@@@@@@@@@@@@@@@@@@@
 *********************************************/
public class CustomPrincipal implements Serializable {

    private static final long serialVersionUID = 1L;
    private String username;

    public CustomPrincipal() {

    }

    public CustomPrincipal(String username) {
        this.username = username;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}