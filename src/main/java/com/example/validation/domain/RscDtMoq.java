package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_moq")
public class RscDtMoq extends BaseIdEntity {
    private Double moqValue;
}
