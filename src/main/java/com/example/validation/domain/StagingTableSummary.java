package com.example.validation.domain;


import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "staging_table_summary")
public class StagingTableSummary extends BaseIdEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String tableName;
    private boolean isStgLoaded;
    private boolean isSanitize;
    private boolean isValidate;
    private boolean isActive;
    private boolean isRscLoaded;
    private LocalDateTime startStgLoadDate;
    private LocalDateTime endStgLoadDate;
    private LocalDateTime startRscLoadDate;
    private LocalDateTime endRscLoadDate;
    private LocalDateTime startValidateDate;
    private LocalDateTime endValidateDate;
    private int recordCount;
    private LocalDate mpsDate;
}
