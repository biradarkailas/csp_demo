package com.example.validation.domain;

import com.example.validation.util.enums.PartyType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "party")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Party extends BaseIdEntity implements UserDetails {
    private static final long serialVersionUID = 1L;
    @OneToOne(mappedBy = "party", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    PartyTypeOrganization partyTypeOrganization;

    @OneToOne(mappedBy = "party", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    PartyTypeUser partyTypeUser;

    @Enumerated(EnumType.STRING)
    private PartyType type;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;

 /*   @JsonIgnore
    @ManyToMany
    @JoinTable(name = "shipment_stake_holders",
            joinColumns = @JoinColumn(name = "PARTY_ID"),
            inverseJoinColumns = @JoinColumn(name = "SHIPMENT_ID"))
    @ToString.Exclude
    private List<Shipment> shipment;*/

    @OneToOne(mappedBy = "party", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private PartyName partyName;

    @OneToMany(mappedBy = "party", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private Collection<PartyAddressDetails> partyAddressDetails;

    @OneToOne(mappedBy = "party", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @ToString.Exclude
    private PartyContactDetails partyContactDetails;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "role_party", joinColumns = {
            @JoinColumn(name = "PARTY_ID", referencedColumnName = "id")}, inverseJoinColumns = {
            @JoinColumn(name = "ROLE_ID", referencedColumnName = "id")})
    private List<Role> roles;

    @JsonIgnore
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        Set<GrantedAuthority> authorities = new HashSet<GrantedAuthority>();
        if (roles != null && roles.size() != 0) {
            roles.forEach(r -> {
                authorities.add(new SimpleGrantedAuthority(r.getName()));
                r.getPermissions().forEach(p -> {
                    authorities.add(new SimpleGrantedAuthority(p.getName()));
                });
            });
        }
        return authorities;
    }

    @Override
    public String getPassword() {
        if (partyTypeUser != null) {
            return partyTypeUser.getPassword();
        } else {
            return null;
        }
    }

    @Override
    public String getUsername() {
        if (partyTypeUser != null) {
            return partyTypeUser.getUsername();
        } else {
            return null;
        }
    }

    @Override
    public boolean isAccountNonExpired() {
        if (partyTypeUser != null) {
            return !partyTypeUser.isAccountExpired();
        } else {
            return false;
        }
    }

    @Override
    public boolean isAccountNonLocked() {
        if (partyTypeUser != null) {
            return !partyTypeUser.isAccountLocked();
        } else {
            return false;
        }
    }

    @Override
    public boolean isCredentialsNonExpired() {
        if (partyTypeUser != null) {
            return !partyTypeUser.isCredentialsExpired();
        } else {
            return false;
        }
    }

    @Override
    public boolean isEnabled() {
        if (partyTypeUser != null) {
            return partyTypeUser.isEnabled();
        } else {
            return false;
        }
    }
}
