package com.example.validation.domain;

import com.example.validation.util.enums.SlobType;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_rm_slob")
public class RscIotRmSlob extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_rm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_it_item_supplier_id")
    private RscItItemWiseSupplier rscItItemWiseSupplier;

    @ManyToOne
    @JoinColumn(name = "rsc_it_rm_gross_consumption_id")
    private RscItRMGrossConsumption rscItRMGrossConsumption;

    private Double mapPrice;
    private Double stdPrice;
    private Double price;
    private Double stockQuantity;
    private Double stockValue;
    private Double expiryStockQuantity;
    private Double expirySlobValue;
    private Double totalStockQuantity;
    private Double totalStockValue;
    private Double totalConsumptionQuantity;
    private Double slobQuantity;
    private Double slobValue;
    private Double balanceStock;
    private Double totalSlobValue;
    @Enumerated(EnumType.STRING)
    @Column(name = "slob_type")
    private SlobType slobType;
    private Double obSlobValue;
    private Double slowSlobValue;
    private Double totalObValue;
    @ManyToOne
    @JoinColumn(name = "rsc_dt_reason_id")
    private RscDtReason rscDtReason;
    private String remark;
}