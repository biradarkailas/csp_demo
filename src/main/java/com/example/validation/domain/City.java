package com.example.validation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/*********************************************
 #######################@@@@@@@@@@@@@@@@@@@@@@@
 **********@author - Rhishikesh Akole**********
 **********date - 2018-12-07T11:41*************
 **********class - City
#######################@@@@@@@@@@@@@@@@@@@@@@@
 *********************************************/

@Entity
@Table(name = "city")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class City extends BaseIdEntity {

    private String name;
    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "STATE_ID")
    private State state;
}
