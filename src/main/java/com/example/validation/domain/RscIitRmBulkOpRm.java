package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iit_rm_bulk_op_rm")
public class RscIitRmBulkOpRm extends BaseIdEntity {
    private Double quantity;
    private LocalDate stockDate;
    private String lotNumber;
    @ManyToOne
    @JoinColumn(name = "rsc_mt_bulk_item_id")
    private RscMtItem rscMtBulkItem;

    @ManyToOne
    @JoinColumn(name = "rm_item_id")
    private RscMtItem rscMtRmItem;
}
