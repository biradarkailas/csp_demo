package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_sailing_days")
public class RscDtSailingDays extends BaseIdEntity {

    @Column(unique = true)
    private Double sailingDays;
}
