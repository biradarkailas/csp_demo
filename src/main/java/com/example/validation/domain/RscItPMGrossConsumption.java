package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_it_pm_gross_consumption")
public class RscItPMGrossConsumption extends BaseIdEntity {

    @Column(name = "m1_value")
    private Double m1Value;

    @Column(name = "m2_value")
    private Double m2Value;

    @Column(name = "m3_value")
    private Double m3Value;

    @Column(name = "m4_value")
    private Double m4Value;

    @Column(name = "m5_value")
    private Double m5Value;

    @Column(name = "m6_value")
    private Double m6Value;

    @Column(name = "m7_value")
    private Double m7Value;

    @Column(name = "m8_value")
    private Double m8Value;

    @Column(name = "m9_value")
    private Double m9Value;

    @Column(name = "m10_value")
    private Double m10Value;

    @Column(name = "m11_value")
    private Double m11Value;

    @Column(name = "m12_value")
    private Double m12Value;

    private Double totalValue;
    private int priority;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_ppheader_id")
    private RscMtPPheader rscMtPPheader;

    @ManyToOne
    @JoinColumn(name = "rsc_it_item_supplier_id")
    private RscItItemWiseSupplier rscItItemWiseSupplier;
}