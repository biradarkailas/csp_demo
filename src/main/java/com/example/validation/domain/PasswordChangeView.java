package com.example.validation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class PasswordChangeView {
    Long id;
    String oldPassword;
    String newPassword;
    String status;
    String message;
}
