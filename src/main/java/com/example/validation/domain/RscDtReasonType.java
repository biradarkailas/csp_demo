package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Entity
@EqualsAndHashCode(callSuper = true)
@Data
@Table(name= "rsc_dt_reason_type")
public class RscDtReasonType extends BaseIdEntity {
    private String name;
}
