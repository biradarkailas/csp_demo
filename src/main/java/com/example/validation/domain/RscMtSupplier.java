package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_supplier")
public class RscMtSupplier extends BaseIdEntity{
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_supplier_id")
    private RscDtSupplier rscDtSupplier;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_contact_details_id")
    private RscDtContactDetails rscDtContactDetails;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_address_id")
    private RscDtAddress rscDtAddress;
}
