package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_latest_total_mould_saturation_summary")
public class RscIotLatestTotalMouldSaturationSummary extends BaseIdEntity{
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
    @ManyToOne
    @JoinColumn(name = "latest_total_mould_saturation_id")
    private RscIotLatestTotalMouldSaturation rscIotLatestTotalMouldSaturation;

    private Double threeMonthsAverage;
    private Double sixMonthsAverage;
    private Double twelveMonthsAverage;
}
