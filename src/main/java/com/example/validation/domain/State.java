package com.example.validation.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

/*********************************************
 #######################@@@@@@@@@@@@@@@@@@@@@@@
 **********@author - Rhishikesh Akole**********
 **********date - 2018-12-07T11:44*************
 **********class - State
#######################@@@@@@@@@@@@@@@@@@@@@@@
 *********************************************/

@Entity
@Table(name = "STATE")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder

public class State extends BaseIdEntity {
    private String name;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "COUNTRY_ID")
    private Country country;
}
