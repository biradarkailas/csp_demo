package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_pm_cover_days_summary")
public class RscIotPmCoverDaysSummary extends BaseIdEntity {
    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
    private Double totalStockValue;
    private Double sitValue;
    private Double totalStockSitValue;
    private Double month1ConsumptionTotalValue;
    private Double month2ConsumptionTotalValue;
    private Double month3ConsumptionTotalValue;
    private Double month4ConsumptionTotalValue;
    private Double month5ConsumptionTotalValue;
    private Double month6ConsumptionTotalValue;
    private Double month7ConsumptionTotalValue;
    private Double month8ConsumptionTotalValue;
    private Double month9ConsumptionTotalValue;
    private Double month10ConsumptionTotalValue;
    private Double month11ConsumptionTotalValue;
    private Double month12ConsumptionTotalValue;
    private Integer month1CoverDays;
    private Integer month2CoverDays;
    private Integer month3CoverDays;
    private Integer month4CoverDays;
    private Integer month5CoverDays;
    private Integer month6CoverDays;
    private Integer month7CoverDays;
    private Integer month8CoverDays;
    private Integer month9CoverDays;
    private Integer month10CoverDays;
    private Integer month11CoverDays;
    private Integer month12CoverDays;
}
