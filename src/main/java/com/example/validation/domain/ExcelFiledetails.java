package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.io.Serializable;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "excel_file_details")
public class ExcelFiledetails extends BaseIdEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    private String fileName;
    private String sheetName;
    private int sheetIndex;
}
