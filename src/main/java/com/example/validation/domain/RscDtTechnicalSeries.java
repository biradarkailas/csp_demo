package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_dt_technical_series")
public class RscDtTechnicalSeries extends BaseIdEntity {
    private Double seriesValue;
}
