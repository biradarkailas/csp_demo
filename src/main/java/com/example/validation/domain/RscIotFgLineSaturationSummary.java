package com.example.validation.domain;

import com.example.validation.util.enums.SaturationStatus;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "rsc_iot_fg_line_saturation_summary")
public class RscIotFgLineSaturationSummary extends BaseIdEntity {

    private Double avgSaturationPercentage;

    private LocalDate lineSummaryDate;

    @Enumerated(EnumType.STRING)
    @Column(name = "saturation_status")
    private SaturationStatus saturationStatus;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_line_id")
    private RscDtLines rscDtLines;

    @ManyToOne
    @JoinColumn(name = "mps_header_id")
    private RscMtPPheader rscMtPPheader;
}
