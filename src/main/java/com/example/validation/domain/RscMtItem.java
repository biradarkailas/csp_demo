package com.example.validation.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_mt_item")
public class RscMtItem extends BaseIdEntity {

    @OneToOne
    private RscDtItemCode rscDtItemCode;

    @ManyToOne
    private RscDtItemCategory rscDtItemCategory;

    @ManyToOne
    private RscDtItemType rscDtItemType;

    @ManyToOne
    private RscDtCoverDays rscDtCoverDays;

    @ManyToOne
    private RscDtUom rscDtUom;

    @ManyToOne
    private RscDtDivision rscDtDivision;


    @ManyToOne
    private RscDtSafetyStock rscDtSafetyStock;

    @ManyToOne
    private RscDtItemClass rscDtItemClass;

    @ManyToOne
    private RscDtTechnicalSeries rscDtTechnicalSeries;

    @ManyToOne
    private RscDtMoq rscDtMoq;

    @ManyToOne
    private RscDtMap rscDtMap;

    @ManyToOne
    private RscDtStdPrice rscDtStdPrice;

    private LocalDate launchDate;
    private boolean active;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @OneToOne(mappedBy = "rscMtItem", fetch = FetchType.LAZY)
    @JsonIgnore
    private RscItItemMould rscItItemMould;

  /*  @OneToOne(mappedBy = "rscMtItem", fetch = FetchType.LAZY)
    private RscDtUom rscMtItemChild;*/

}
