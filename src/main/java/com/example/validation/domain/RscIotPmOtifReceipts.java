package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;


@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_iot_pm_otif_receipts")
public class RscIotPmOtifReceipts extends BaseIdEntity {

    @JoinColumn(name = "OTIF_date")
    private LocalDate receiptDate;

    @ManyToOne
    @JoinColumn(name = "pm_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "supplier_id")
    private RscDtSupplier rscDtSupplier;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_otif_pm_receipts_header_id")
    private RscMtOtifPmReceiptsHeader rscMtOtifPmReceiptsHeader;
    private Double quantity;
    @CreationTimestamp
    private LocalDate createdDate;
    @UpdateTimestamp
    private LocalDate modifiedDate;
}
