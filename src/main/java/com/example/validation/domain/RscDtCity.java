package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_city")
public class RscDtCity extends BaseIdEntity {
    private String name;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_state_id")
    private RscDtState rscDtState;
}