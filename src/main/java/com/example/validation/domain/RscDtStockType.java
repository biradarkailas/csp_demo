package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_dt_stock_type")
public class RscDtStockType extends BaseIdEntity {
    private String aliasName;
    private String fullName;
    private String tag;
}