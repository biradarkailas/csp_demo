package com.example.validation.domain;

import com.example.validation.util.enums.PartyOrganizationType;
import com.example.validation.util.enums.ServiceType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "party_type_organization")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PartyTypeOrganization {
    @Id
    @Column(name = "ID", unique = true, nullable = false)
    @GeneratedValue(generator = "gen")
    @GenericGenerator(name = "gen", strategy = "foreign", parameters = @org.hibernate.annotations.Parameter(name = "property", value = "party"))
    Long id;

    @JsonIgnore
    @OneToOne
    @PrimaryKeyJoinColumn
    @ToString.Exclude
    Party party;

    @Enumerated(EnumType.STRING)
    private PartyOrganizationType organizationType;
    @Enumerated(EnumType.STRING)
    private ServiceType serviceType;
    private String vendorCode;
    private Boolean service;
    private String lastReferenceNumber;
    private LocalDateTime createdAt;
    private String createdBy;
    private LocalDateTime updatedAt;
    private String modifiedBy;
}
