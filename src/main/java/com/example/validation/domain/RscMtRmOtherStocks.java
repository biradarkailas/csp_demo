package com.example.validation.domain;

import com.example.validation.dto.RmRejectionStockPaginationDTO;
import com.example.validation.dto.RmTransferStockPaginationDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@NamedNativeQuery(
        name = "Rm_Transfer_Stock",
        query = "select csp_loreal.vew_item.code as itemCode, csp_loreal.vew_item.description as itemDescription,csp_loreal.rsc_mt_rm_other_stocks.stock_date as stockDate ,rsc_mt_item_id as rscMtItemId,csp_loreal.rsc_iot_rm_stock.quantity as quantity,csp_loreal.rsc_mt_rm_other_stocks.quantity as lotWiseStock from csp_loreal.rsc_iot_rm_stock,csp_loreal.rsc_mt_rm_other_stocks,csp_loreal.vew_item where concat(year(csp_loreal.rsc_mt_rm_other_stocks.stock_date),month(csp_loreal.rsc_mt_rm_other_stocks.stock_date))=?1 and csp_loreal.rsc_mt_rm_other_stocks.stock_date=csp_loreal.rsc_iot_rm_stock.stock_date and rsc_mt_item_id=rm_item_id and stock_type_id in(Select id from csp_loreal.rsc_dt_stock_type where tag=?2) and csp_loreal.vew_item.mt_item_id=rsc_mt_item_id",
        resultSetMapping = "RmTransferStockPaginationDTO"
)
@SqlResultSetMapping(
        name = "RmTransferStockPaginationDTO",
        classes = @ConstructorResult(
                targetClass = RmTransferStockPaginationDTO.class,
                columns = {
                        @ColumnResult(name = "itemCode", type = String.class),
                        @ColumnResult(name = "itemDescription", type = String.class),
                        @ColumnResult(name = "stockDate", type = LocalDate.class),
                        @ColumnResult(name = "rscMtItemId", type = Long.class),
                        @ColumnResult(name = "quantity", type = Double.class),
                        @ColumnResult(name = "lotWiseStock", type = Double.class)}
        )
)
@NamedNativeQuery(
        name = "Rm_Rejection_Stock",
        query ="select csp_loreal.vew_item.code as code, csp_loreal.vew_item.description as description,csp_loreal.rsc_mt_rm_other_stocks.stock_date as dateOfStock,rsc_mt_item_id as mtItemId,csp_loreal.rsc_iot_rm_stock.quantity as stockQuantity,csp_loreal.rsc_mt_rm_other_stocks.quantity as lotWiseStockQuantity from csp_loreal.rsc_iot_rm_stock,csp_loreal.rsc_mt_rm_other_stocks,csp_loreal.vew_item   where concat(year(csp_loreal.rsc_mt_rm_other_stocks.stock_date),month(csp_loreal.rsc_mt_rm_other_stocks.stock_date))=?1 and csp_loreal.rsc_mt_rm_other_stocks.stock_date=csp_loreal.rsc_iot_rm_stock.stock_date and rsc_mt_item_id=rm_item_id and stock_type_id in(Select id from csp_loreal.rsc_dt_stock_type where tag=?2) and csp_loreal.vew_item.mt_item_id=rsc_mt_item_id",
        resultSetMapping = "RmRejectionStockPaginationDTO"
)
@SqlResultSetMapping(
        name = "RmRejectionStockPaginationDTO",
        classes = @ConstructorResult(
                targetClass = RmRejectionStockPaginationDTO.class,
                columns = {
                        @ColumnResult(name = "code", type = String.class),
                        @ColumnResult(name = "description", type = String.class),
                        @ColumnResult(name = "dateOfStock", type = LocalDate.class),
                        @ColumnResult(name = "mtItemId", type = Long.class),
                        @ColumnResult(name = "stockQuantity", type = Double.class),
                        @ColumnResult(name = "lotWiseStockQuantity", type = Double.class)}
        )
)
@Data
@Table(name = "rsc_mt_rm_other_stocks")
public class RscMtRmOtherStocks extends BaseIdEntity{
    private Double quantity;
    private LocalDate stockDate;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    @JoinColumn(name = "rsc_mt_item_id")
    private RscMtItem rscMtItem;

    @ManyToOne
    @JoinColumn(name = "rsc_dt_stock_type_id")
    private RscDtStockType rscDtStockType;
}
