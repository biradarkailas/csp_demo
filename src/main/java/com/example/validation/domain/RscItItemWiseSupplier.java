package com.example.validation.domain;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.LocalDate;
import java.time.LocalDateTime;

@EqualsAndHashCode(callSuper = true)
@Entity
@Data
@Table(name = "rsc_it_item_supplier")
public class RscItItemWiseSupplier extends BaseIdEntity {
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
    private Double allocationPercentage;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

    @ManyToOne
    private RscMtItem rscMtItem;

    @ManyToOne
    private RscDtTechnicalSeries rscDtTechnicalSeries;

    @ManyToOne
    private RscDtMoq rscDtMoq;

    @ManyToOne
    private RscMtSupplier rscMtSupplier;

    @ManyToOne
    @JoinColumn(name = "container_type_id")
    private RscDtContainerType rscDtContainerType;
}