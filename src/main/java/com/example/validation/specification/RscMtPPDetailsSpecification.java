package com.example.validation.specification;

import com.example.validation.domain.*;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Root;
import java.util.List;

@Component
public class RscMtPPDetailsSpecification {
    public Specification<RscMtPPdetails> createSpecification(RscMtPPDetailsCriteria criteria) {
        Specification<RscMtPPdetails> specification = Specification.where(null);
        if (criteria != null) {
            if (criteria.getMpsId() != null) {
                specification = specification.and((root, criteriaQuery, criteriaBuilder) ->
                        criteriaBuilder.equal(root.join(RscMtPPdetails_.rscMtPPheader, JoinType.LEFT).get(RscMtPPheader_.id), criteria.getMpsId()));
            }
            if (criteria.getItemIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_MT_ITEM, RscMtItem_.ID, criteria.getItemIds()));
            }
            if (criteria.getDivisionIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_DIVISION, RscDtDivision_.ID, criteria.getDivisionIds()));
            }
            if (criteria.getLineIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_LINES, RscDtLines_.ID, criteria.getLineIds()));
            }
            if (criteria.getSkidIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_SKIDS, RscDtSkids_.ID, criteria.getSkidIds()));
            }
            if (criteria.getCategoryIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_MATERIAL_CATEGORY, RscDtMaterialCategory_.ID, criteria.getCategoryIds()));
            }
            if (criteria.getSubCategoryIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_MATERIAL_SUB_CATEGORY, RscDtMaterialSubCategory_.ID, criteria.getSubCategoryIds()));
            }
            if (criteria.getSignatureIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_SIGNATURE, RscDtSignature_.ID, criteria.getSignatureIds()));
            }
            if (criteria.getBrandIds() != null) {
                specification = specification.and(whereMultipleSameChildFields(RscMtPPdetails_.RSC_DT_BRAND, RscDtBrand_.ID, criteria.getBrandIds()));
            }
        }
        return specification;
    }

    private Specification<RscMtPPdetails> whereMultipleSameChildFields(String childTableName, String variableName, List<Long> ids) {
        Specification<RscMtPPdetails> finalSpecification = null;
        for (Long id : ids) {
            Specification<RscMtPPdetails> internalSpecification = (Root<RscMtPPdetails> root, CriteriaQuery<?> query, CriteriaBuilder cb) ->
                    cb.equal(root.get(childTableName).get(variableName), id);
            finalSpecification = orConditionSpecification(finalSpecification, internalSpecification);
        }
        return finalSpecification;
    }

    private Specification<RscMtPPdetails> andConditionSpecification(Specification<RscMtPPdetails> finalSpecification, Specification<RscMtPPdetails> internalSpecification) {
        if (finalSpecification == null)
            return internalSpecification;
        else
            return finalSpecification.and(internalSpecification);
    }

    private Specification<RscMtPPdetails> orConditionSpecification(Specification<RscMtPPdetails> finalSpecification, Specification<RscMtPPdetails> internalSpecification) {
        if (finalSpecification == null)
            return internalSpecification;
        else
            return finalSpecification.or(internalSpecification);
    }
}
