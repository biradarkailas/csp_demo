package com.example.validation.specification;

import lombok.Data;

import java.util.List;

@Data
public class RscMtPPDetailsCriteria {
    private Long mpsId;
    private List<Long> itemIds;
    private List<Long> divisionIds;
    private List<Long> skidIds;
    private List<Long> lineIds;
    private List<Long> categoryIds;
    private List<Long> subCategoryIds;
    private List<Long> signatureIds;
    private List<Long> brandIds;
}