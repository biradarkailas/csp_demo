package com.example.validation.config.constants;

public final class ApplicationConstants {

    public static final String TRUE = "true";
    public static final String OS = System.getProperty("os.name").contains("Win") ? "WIN" : "LIN";
    public static final String SLASH = OS.equals("WIN") ? "\\" : "/";
    public static final String ERROR_FILE_DIRECTORY_PATH = "data/ErrorData/";
    public static final String ERROR_FILE_NAME = "ErrorLog";
    public static final String CSV_FILE_EXTENSION = ".csv";
    public static final String FILE_NAME_DATE_TIME_FORMATTER = "yyyyMMddHHmmss";
    public static final String COUNT_QUERY = "SELECT COUNT(*) FROM ";
    public static final String PLANS_FIRST_CUT_ZIP_SUB_DIR_PATH = "/plans/first cut/";
    public static final String PLANS_VALIDATED_ZIP_SUB_DIR_PATH = "/plans/validated/";
    public static final String PM_OTIF_ZIP_SUB_DIR_PATH = "/analysis/month/pm/otif/";
    public static final String ALL_MODULES_ZIP_SUB_DIR_PATH = "/analysis/annual/";
    public static final String FORWARD_SLASH = "/";
    public static final int ZERO = 0;
    public static final int ONE = 1;
    public static final int TWO = 2;
    public static final int THREE = 3;
    public static final int FOUR = 4;
    public static final int FIVE = 5;
    public static final int SIX = 6;
    public static final int SEVEN = 7;
    public static final int EIGHT = 8;
    public static final int NINE = 9;
    public static final int TEN = 10;
    public static final int ELEVEN = 11;
    public static final int TWELVE = 12;
    public static final double THIRTY = 30;
    public static final double ONE_YEAR_DAYS_COUNT = 365;
    public static final double DOUBLE_ZERO = 0;
    public static final String DEVIATION_STATUS_UP = "UP";
    public static final String DEVIATION_STATUS_DOWN = "DOWN";
    public static final String DEVIATION_STATUS_NA = "NA";
    public static final String FINAL = "final";
    public static final String PREVIOUS = "previous";
    //// Windows Server Configuration ////
    public static final String PYTHON_WIN = "C:\\Users\\server.admin\\AppData\\Local\\Programs\\Python\\Python39\\python.exe";

    //// Local Configuration ////
    /*public static final String DRIVE_NAME = "D:";
    public static final String PYTHON_WIN = "C:\\Users\\user\\AppData\\Local\\Programs\\Python\\Python39\\python.exe";
    //public static final String PYTHON_WIN = "C:\\Users\\Lenovo\\AppData\\Local\\Programs\\Python\\Python39\\python.exe";
    public static final String SOURCE_PATH = DRIVE_NAME + "/CSP/data";
    public static final String PYTHON_FILE_PATH = DRIVE_NAME + "/CSP/resource/ExcelUtil.py";*/
    public static final String DRIVE_NAME = "E:";
    public static final String BACKUP_PATH = System.getProperty("os.name").contains("Win") ? DRIVE_NAME + "\\CSP/BackUp/" : "/home/ubuntu/CSP/resource/backup/";
    public static final String SOURCE_PATH = DRIVE_NAME + "/CSP/data";
    public static final String PYTHON_FILE_PATH = DRIVE_NAME + "/CSP/resource/ExcelUtil.py";

    //// AWS Linux Configuration ////
    public static final String PYTHON_LIN = "/usr/bin/python3";
    //public static final String SOURCE_PATH = "/home/ubuntu/IDP/resource/data";
    //public static final String PYTHON_FILE_PATH = "/home/ubuntu/IDP/resource/ExcelUtil.py";

    //// Windows Database Backup //////
    public static final String WINDOWS_DB_PATH = "C:\\Program Files\\MariaDB 10.6\\bin\\";
    public static final String DB_USER_NAME = "root";
    public static final String DB_PASSWORD = "root";
    public static final String DB_NAME = "csp_loreal";
    public static final String WINDOWS_DB_BACKUP_PATH = DRIVE_NAME + "\\CSP\\Database\\Backup\\";

    public static final String LINUX_DB_BACKUP_PATH = "/home/ubuntu/sanjali-projects/csp/database/backup/";

    private ApplicationConstants() {
    }
}