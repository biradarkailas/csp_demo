package com.example.validation.config.constants;

public final class MaterialCategoryConstants {
    public static final String FG_MATERIAL = "Finished Good";
    public static final String BULK_MATERIAL = "Bulk Material";
    public static final String RAW_MATERIAL = "Raw Material";
    public static final String PACKING_MATERIAL = "Packing Material";
    public static final String WIP_MATERIAL = "WIP Material";
    public static final String SUB_BASES = "SubBases";
    public static final String MES_STOCK_TYPE = "MES";
    public static final String RTD_STOCK_TYPE = "RTD";
    public static final String RTD_TRANSFER_STOCK_TYPE = "TRANSFER_STOCK";
    public static final String BULK = "Bulk";
    public static final String PM = "PM";
    public static final String RM = "RM";
    public static final String STOCK_VALUE = "Stock Value";
    public static final String MOQ = "Moq Value";
    public static final String TS = "Technical Series";
    public static final String CURRENT_MONTH = "Current Month";
    public static final String NEXT_MONTH = "Next Month";
    public static final String MONTH_END_STOCK = "Month End Stock";
    public static final String RECEIPT_TILL_DATE = "Receipt Till Date";
    public static final String PM_STOCK_DETAIL_FORMULA = "Opening Stock = Midnight Stock + Receipts";
    public static final String RM_STOCK_DETAIL_FORMULA = "Opening Stock = Midnight Stock + Receipts + Bulk OP RM + Transfer Stock - Rejection";
    public static final String STOCK_TYPE_TAG_PM_MES = "PM_MES";
    public static final String STOCK_TYPE_TAG_PM_RTD = "PM_RTD";
    public static final String BULK_OP_RM = "Bulk Op RM";
    public static final String STOCK_TYPE_TAG_RM_MES = "RM_MES";
    public static final String STOCK_TYPE_TAG_RM_RTD = "RM_RTD";
    public static final String STOCK_TYPE_TAG_RM_REJ = "RM_Rej";
    public static final String STOCK_TYPE_TAG_RM_REJECTION = "RM_Rej_Physical";
    public static final String STOCK_TYPE_TAG_RM_BULK_OP_RM = "RM_Bulk_op_rm";
    public static final String STOCK_TYPE_TAG_RM_EXPIRED = "RM_EXPIRED";
    public static final String STOCK_TYPE_TAG_RM_Other = "RM_Other_Stock";
    public static final String STOCK_TYPE_TAG_RM_OPEN_PO = "RM_Open_PO";
    public static final String END_OF_LIFE = "End of Life";
    public static final String LAUNCH = "Launch";
    public static final String NONE = "None";
    public static final String STOCK_TYPE_TAG_RM_OTHER_STOCK = "RM_Other_Stock";
    public static final String OPENING_STOCK = "Opening Stock";
    public static final String SAFETY_STOCK = "Safety Stock";
    public static final String ALLOCATION = "Allocation";
    public static final String PACK_SIZE = "Pack Size";
    public static final String TOTAL_STOCK_SIT_VALUE_FORMULA = "Total Stock Sit Value = Sit + Total Stock Value";
    public static final String TOTAL_SLOW_ITEM_VALUE_FORMULA = "Total Slow Item Value = Total Slow Item Value";
    public static final String SLOW_ITEM_PERCENTAGE_FORMULA = "Slow Item Percentage = (Total Slow Item Value / Total Stock Sit Value) * 100";
    public static final String TOTAL_OBSOLETE_ITEM_VALUE_FORMULA = "Total Obsolete Item Value = Total Obsolete Item Value";
    public static final String OBSOLETE_ITEM_PERCENTAGE_FORMULA = "Obsolete Item Percentage = (Total Obsolete Item Value / Total Stock Sit Value) * 100";
    public static final String TOTAL_STOCK_QUALITY_VALUE_FORMULA = "Total Stock Quality Value = Total Stock Sit Value * Stock Quality Percentage;";
    public static final String STOCK_QUALITY_PERCENTAGE_FORMULA = "Stock Quality Percentage = 100 - (Slow Item Percentage + Obsolete Item Percentage)";
    public static final String FG_MES_TAG = "FG_MES";
    public static final String RM_SLOB_TAG = "RM_SLOB";
    public static final String PM_SLOB_TAG = "PM_SLOB";
    public static final String FG_SLOB_TAG = "FG_SLOB";
    public static final String LINE_SATURATION_TAG = "Line";
    public static final String SKID_SATURATION_TAG = "Skid";

    public static final String PM_BOM_REASON_TAG = "BOM";
    public static final String NONE_TAG = "None";
    public static final String SL_SLOB_TYPE = "SL";
    public static final String OB_SLOB_TYPE = "OB";
    public static final String OTHER_STOCK = "other_stock";
    public static final String REJECTION = "Rejection";
    public static final String YFG_TYPE = "YFG";
    public static final String ZRM_TYPE = "ZRM";
    public static final String STOCK = "stock";
    public static final String SLOB = "slob";
    public static final String SLOW = "slow";
    public static final String OBSOLETE = "obsolete";
    public static final String ALL = "ALL";
    public static final String NORMAL_CONTAINER = "Normal_Container";
    public static final String COVER_DAYS_CLASS_A = "A";
    public static final String COVER_DAYS_CLASS_B = "B";
    public static final String COVER_DAYS_CLASS_C = "C";
    public static final String STOCK_TYPE_TAG_PM_REJECTION = "PM_Rej";
    public static final String STOCK_TYPE_TAG_PM_OPEN_PO = "PM_Open_PO";

    private MaterialCategoryConstants() {
    }
}