package com.example.validation.config.constants;

public class ExcelFileNameConstants {
  public static final String Save_All_Exports_Path =
      System.getProperty("os.name").contains("Win")
          ? ApplicationConstants.DRIVE_NAME + "\\CSP/Reports/"
          : "/home/ubuntu/CSP/resource/tmp/";
  public static final String Export_Download_path =
      System.getProperty("os.name").contains("Win")
          ? ApplicationConstants.DRIVE_NAME + "\\CSP/Reports/"
          : "/home/ubuntu/CSP/resource/tmp/";
  public static final String BACKUP_PATH =
      System.getProperty("os.name").contains("Win")
          ? ApplicationConstants.DRIVE_NAME + "\\CSP/InputData/"
          : "/home/ubuntu/CSP/resource/tmp/InputData/";

  public static final String FG = "FG";
  public static final String PM = "PM";
  public static final String RM = "RM";
  public static final String PMOtif = "PMOtif";
  public static final String FG_Directory = "FG/";
  public static final String PM_Directory = "PM/";
  public static final String PMOtif_Directory = "PMOtif/";
  public static final String RM_Directory = "RM/";
  public static final String Extension = ".xlsx";
  public static final String Attachment_File = "attachment; filename = ";
  public static final String Application_Excel = "application/vnd.ms-excel_export";
  public static final String Application_xlsx = "application/vnd.xlsx";
  public static final String Application_zip = "application/vnd.zip";
  public static final String Analyzer_Zip_Filename = "SelectedExports.zip";
  public static final String[] fileNameList = {"FG", "PM", "RM", "PMOtif"};

  public static final String PM_MPS_PLAN = "MPS ";
  public static final String PM_GROSS_CONSUMPTION = "PM Gross (PM based) Consumption ";
  public static final String PM_LOGICAL_CONSUMPTION = "PM Logical (FG based) Consumption ";
  public static final String PM_MIDNIGHT_STOCK = "PM Midnight 'A+Q' Stock ";
  public static final String PM_MIDNIGHT_Rejection_STOCK = "PM Midnight Rejection Stock ";
  public static final String PM_RECEIPTS_STOCK = "PM Receipts ";
  public static final String PM_PURCHASE_PLAN = "PM Purchase Plan ";
  public static final String PM_MOULD_WISE_MOULD = "PM Mould Wise Mould Saturation ";
  public static final String PM_SUPPLIER_WISE_MOULD = "PM Supplier Wise Mould Saturation ";
  public static final String PM_SLOB = "PM SLOB ";
  public static final String PM_COVER_DAYS = "PM Cover Days ";
  public static final String PM_SLOB_SUMMARY = "PM SLOB Summary ";
  public static final String PM_Purchase_Coverdays = "PM Purchase Cover Days ";
  public static final String PM_Purchase_Stock_Equation = "PM Purchase Stock Equation ";
  public static final String PM_SLOB_COVER_DAYS_SUMMARY = "PM Cover Days Summary ";
  public static final String PM_Master_Data_Mould = "PM Mould Master Data ";
  public static final String PM_Material_Masters = "PM Material Master Data ";
  public static final String PM_Supplier = "PM Supplier Master Data ";
  public static final String PM_Otif_Summary = "PM Otif Summary ";
  public static final String Material_Category = "PM Material Category ";
  public static final String MPS_Triangle_Analysis = "MPS Triangle Analysis ";
  public static final String PM_INVENTORY_QUALITY_INDEX = "PM Inventory Quality Index ";
  public static final String PM_SLOB_MOVEMENT = "PM Slob Movement ";

  public static final String RM_LOGICAL_CONSUMPTION = "RM Logical (Bulk based) Consumption ";
  public static final String RM_GROSS_CONSUMPTION = "RM Gross (RM based) Consumption ";
  public static final String RM_BULK_GROSS_CONSUMPTION = "Bulk Gross (Bulk based) Consumption ";
  public static final String RM_BULK_LOGICAL_CONSUMPTION = "Bulk Logical (FG based) Consumption ";
  public static final String RM_MIDNIGHT_STOCK = "RM Midnight 'A+Q' Stock ";
  public static final String RM_RECEIPTS_STOCK = "RM Receipts ";
  public static final String RM_BULK_OP_RM_STOCK = "RM Bulk OP RM Stock ";
  public static final String RM_TRANSFER_STOCK = "RM Transfer Stock ";
  public static final String RM_REJECTION_STOCK = "RM Rejection Stock ";
  public static final String RM_PURCHASE_PLAN = "RM Purchase Plan ";
  public static final String RM_SLOB = "RM SLOB ";
  public static final String RM_SLOB_COVER_DAYS_SUMMARY = "RM Cover Days Summary ";
  public static final String RM_SLOB_SUMMARY = "RM SLOB Summary ";
  public static final String RM_COVER_DAYS = "RM Cover Days ";
  public static final String RM_Purchase_Coverdays = "RM Purchase Cover Days ";
  public static final String RM_Purchase_Stock_Equation = "RM Purchase Stock Equation ";
  public static final String Rm_Pivot_Consumption = "Rm Pivot (RM based) Consumption ";
  public static final String RM_Material_Masters = "RM Material Master Data ";
  public static final String RM_Bulk_Material_Masters = "RM Bulk Material Master Data ";
  public static final String RM_Base_SubBase_Material_Masters =
      "RM Base SubBase Material Master Data ";
  public static final String RM_Supplier = "RM Supplier Master Data ";
  public static final String RM_INVENTORY_QUALITY_INDEX = "RM Inventory Quality Index ";
  public static final String RM_SLOB_MOVEMENT = "RM Slob Movement ";
  public static final String RM_MIDNIGHT_Rejection_STOCK = "RM Midnight Rejection Stock ";

  public static final String FG_LINE_SATURATION = "FG Line Saturation ";
  public static final String FG_Master_Data_LINE_SATURATION = "FG Line Master Data ";
  public static final String FG_SKID_SATURATION = "FG Skid Saturation ";
  public static final String FG_Master_Data_SkidSaturation = "FG Skid Master Data ";
  public static final String FG_SLOB_SATURATION = "FG SLOB ";
  public static final String FG_SLOB_SUMMARY = "FG SLOB Summary ";
  public static final String FG_SF_WIP_Materials_Masters = "SF WIP Materials Masters Data ";
  public static final String FG_Material_Masters = "FG Material Master Data ";
  public static final String FG_SLOB_MOVEMENT = "FG Slob Movement ";
  public static final String FG_INVENTORY_QUALITY_INDEX = "FG Inventory Quality Index ";

  public static final String EXECUTION_ERROR_PATH =
      System.getProperty("os.name").contains("Win")
          ? ApplicationConstants.DRIVE_NAME + "\\CSP/ExecutionErrorLog/"
          : "/home/ubuntu/CSP/resource/tmp/ERRORS/";
}
