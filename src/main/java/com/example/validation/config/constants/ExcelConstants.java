package com.example.validation.config.constants;

public final class ExcelConstants {
    public static final String CALIBRI = "Calibri";

    private ExcelConstants() {
    }
}
