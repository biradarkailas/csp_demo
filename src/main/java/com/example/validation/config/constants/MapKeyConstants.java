package com.example.validation.config.constants;

public final class MapKeyConstants {
    public static final String Total_Stock_Value = "totalStockValue";
    public static final String Total_Slob_Value = "totalSlobValue";
    public static final String SL_Type_Total_Value = "slTypeTotalValue";
    public static final String OB_Type_Total_Value = "obTypeTotalValue";

    private MapKeyConstants() {
    }
}
