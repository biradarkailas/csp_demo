package com.example.validation.config.constants;

public final class ProcedureNameConstants {

    public static final String EXPORT_ERROR_LOG = "export_error_log";
    public static final String PRC_SANITIZE_DATA = "prc_sanitize_data";
    public static final String PRC_VALIDATE_BOM_DATA = "prc_validate_BOM_data";
    public static final String PRC_VALIDATE_MONTH_END_STOCK_DATA = "prc_validate_month_end_stock_data";
    public static final String PRC_VALIDATE_IN_STD_DATA = "prc_validate_in_std_data";
    public static final String PRC_VALIDATE_ITEM_DATA = "prc_validate_item_data";
    public static final String PRC_VALIDATE_MID_NIGHT_STOCK_DATA = "prc_validate_mid_night_stock_data";
    public static final String PRC_VALIDATE_MPS_DATA = "prc_validate_mps_data";
    public static final String PRC_VALIDATE_OPEN_PO_DATA = "prc_validate_open_po_data";
    public static final String PRC_VALIDATE_PM_MATERIAL_MASTER_DATA = "prc_validate_pm_material_master_data";
    public static final String PRC_VALIDATE_PM_RECEIPTS_TILL_DATE_DATA = "prc_validate_pm_receipts_till_date_data";
    public static final String PRC_VALIDATE_RM_MATERIAL_MASTER_DATA = "prc_validate_rm_material_master_data";
    public static final String PRC_VALIDATE_RM_RECEIPTS_TILL_DATE_DATA = "prc_validate_rm_receipts_till_date_data";
    public static final String PRC_VALIDATE_SS_MASTER_DATA = "prc_validate_ss_master_data";
    public static final String PRC_VALIDATE_STG_MOULD_MASTER_DATA = "prc_validate_stg_mould_master_data";
    public static final String PRC_VALIDATE_TECHNICAL_SIZE_DATA = "prc_validate_technical_size_data";
    private ProcedureNameConstants() {
    }
}