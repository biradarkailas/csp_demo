package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.supply.RmPurchasePlanCalculationDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotRMPurchasePlanCalculationsService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotRMPurchasePlanCalculationsController {
    private final RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotRMPurchasePlanCalculationsController(RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotRMPurchasePlanCalculationsService = rscIotRMPurchasePlanCalculationsService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/rm-purchase-plan-calculations/create/{ppHeaderId}")
    public ExecutionResponseDTO createRMPurchasePlanCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotRMPurchasePlanCalculationsService.createRMPurchasePlanCalculations(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Purchase Plan Calculations", "RM Purchase Plan Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMPurchasePlanCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Purchase Plan Calculations", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-purchase-plan-calculations/{rscMtItemId}/{ppHeaderId}/{supplierDetailsId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RmPurchasePlanCalculationDTO> getRMPurchasePlanCalculations(@PathVariable Long rscMtItemId, @PathVariable Long ppHeaderId, @PathVariable Long supplierDetailsId) {
        return ResponseEntity.ok().body(rscIotRMPurchasePlanCalculationsService.getRMPurchasePlanCalculations(rscMtItemId, ppHeaderId, supplierDetailsId));
    }
}