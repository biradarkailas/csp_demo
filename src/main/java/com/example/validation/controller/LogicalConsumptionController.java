package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.service.LogicalConsumptionService;
import com.example.validation.service.RscItBulkLogicalConsumptionService;
import com.example.validation.service.RscItPMLogicalConsumptionService;
import com.example.validation.service.RscItRMLogicalConsumptionService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class LogicalConsumptionController {
    private final LogicalConsumptionService logicalConsumptionService;
    private final RscItPMLogicalConsumptionService rscItPmLogicalConsumptionService;
    private final RscItRMLogicalConsumptionService rscItRmLogicalConsumptionService;
    private final RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService;

    public LogicalConsumptionController(LogicalConsumptionService logicalConsumptionService, RscItPMLogicalConsumptionService rscItPmLogicalConsumptionService, RscItRMLogicalConsumptionService rscItRmLogicalConsumptionService, RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService) {
        this.logicalConsumptionService = logicalConsumptionService;
        this.rscItRmLogicalConsumptionService = rscItRmLogicalConsumptionService;
        this.rscItPmLogicalConsumptionService = rscItPmLogicalConsumptionService;
        this.rscItBulkLogicalConsumptionService = rscItBulkLogicalConsumptionService;
    }

    @GetMapping("/logical-consumption/create/wip/{ppHeaderId}")
    public ExecutionResponseDTO createWIPLogicalConsumption(@PathVariable Long ppHeaderId) {
        try {
            logicalConsumptionService.createWIPLogicalConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("WIP Logical Consumption", "WIP Logical Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("WIP Logical Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/logical-consumption/create/{ppHeaderId}")
    public ExecutionResponseDTO createLogicalConsumption(@PathVariable Long ppHeaderId) {
        try {
            logicalConsumptionService.createLogicalConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM and Bulk Logical Consumption", "PM and Bulk Logical Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("PM and Bulk Logical Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/logical-consumption/create/rm/{ppHeaderId}")
    public ExecutionResponseDTO createRMLogicalConsumption(@PathVariable Long ppHeaderId) {
        try {
            logicalConsumptionService.createRMLogicalConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Logical Consumption", "RM Logical Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Logical Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/logical-consumption/create/bp/{ppHeaderId}")
    public ExecutionResponseDTO createBPLogicalConsumption(@PathVariable Long ppHeaderId) {
        try {
            logicalConsumptionService.createBPLogicalConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("BP Logical Consumption", "BP Logical Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("BP Logical Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/logical-consumption/create/sub-base/{ppHeaderId}")
    public ExecutionResponseDTO createSubBaseLogicalConsumption(@PathVariable Long ppHeaderId) {
        try {
            logicalConsumptionService.createSubBaseLogicalConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("SubBase Logical Consumption", "SubBase Logical Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("SubBase Logical Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/logical-consumption/pm/{ppHeaderId}/{fgItemId}")
    public ResponseEntity<List<LogicalConsumptionMainDTO>> getAllPMLogicalConsumption(@PathVariable Long ppHeaderId, @PathVariable Long fgItemId) {
        return ResponseEntity.ok().body(rscItPmLogicalConsumptionService.getPMLogicalConsumptionByFG(ppHeaderId, fgItemId));
    }

    @GetMapping("/logical-consumption/bulk/{ppHeaderId}/{fgItemId}")
    public ResponseEntity<List<LogicalConsumptionMainDTO>> getAllBulkLogicalConsumption(@PathVariable Long ppHeaderId, @PathVariable Long fgItemId) {
        return ResponseEntity.ok().body(rscItBulkLogicalConsumptionService.getBulkLogicalConsumptionByFG(ppHeaderId, fgItemId));
    }

    @GetMapping("/logical-consumption/rm/{ppHeaderId}/{bulkItemId}")
    public ResponseEntity<List<LogicalConsumptionMainDTO>> getAllRMLogicalConsumption(@PathVariable Long ppHeaderId, @PathVariable Long bulkItemId) {
        return ResponseEntity.ok().body(rscItRmLogicalConsumptionService.getRMLogicalConsumptionByFG(ppHeaderId, bulkItemId));
    }

    @GetMapping("/logical-consumption/pm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<LogicalConsumptionMainDTO>> getPMLogicalConsumption(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPmLogicalConsumptionService.getPMLogicalConsumption(ppHeaderId));
    }

    @GetMapping("/logical-consumption/bulk/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<LogicalConsumptionMainDTO>> getBulkLogicalConsumption(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItBulkLogicalConsumptionService.getBulkLogicalConsumption(ppHeaderId));
    }

    @GetMapping("/logical-consumption/rm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<LogicalConsumptionMainDTO>> getRMLogicalConsumption(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItRmLogicalConsumptionService.getRMLogicalConsumption(ppHeaderId));
    }

    @DeleteMapping("/logical-consumption/delete/{ppHeaderId}")
    public ResponseEntity<String> deleteLogicalConsumption(@PathVariable Long ppHeaderId) {
        rscItRmLogicalConsumptionService.deleteConsumption(ppHeaderId);
        return ResponseEntity.ok().body("Deleted");
    }
}