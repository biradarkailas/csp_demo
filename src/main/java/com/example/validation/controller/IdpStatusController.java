package com.example.validation.controller;

import com.example.validation.dto.FileUploadStatusDTO;
import com.example.validation.dto.MpsPlanViewDto;
import com.example.validation.dto.RscIitIdpStatusDTO;
import com.example.validation.service.RscIitIdpStatusService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/api")
public class IdpStatusController {

    private RscIitIdpStatusService rscIitIdpStatusService;

    public IdpStatusController(RscIitIdpStatusService rscIitIdpStatusService) {
        this.rscIitIdpStatusService = rscIitIdpStatusService;
    }

    @GetMapping("/mps-plan-name/exist/{name}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Boolean mpsPlanNameExists(@PathVariable String name) {
        return rscIitIdpStatusService.mpsNameExists(name);
    }

    @PostMapping("/rsc-iit-idp-status")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto saveRscIitIdpStatus(@RequestBody MpsPlanViewDto mpsPlanViewDto) {
        return rscIitIdpStatusService.saveAll(mpsPlanViewDto);
    }

    @GetMapping("/current-mps-plan/details")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto getCurrentMpsPlan() {
        return rscIitIdpStatusService.getCurrentMpsPlan();
    }

    @GetMapping("/current-mps-plan/available")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Boolean isPlanAvailable() {
        return rscIitIdpStatusService.isPlanAvailable();
    }

    @PostMapping("/upload-file/{idpStatusId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public FileUploadStatusDTO uploadFile(@RequestParam("file") MultipartFile file, HttpSession session, @PathVariable Long idpStatusId) {
        return rscIitIdpStatusService.uploadFile(file, session, idpStatusId);
    }

    @GetMapping("/current-uploaded-mps-plan/{mpsName}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto uploadedMpsPlan(@PathVariable String mpsName) {
        return rscIitIdpStatusService.uploadedMpsPlan(mpsName);
    }

    @PutMapping("/current-mps-plan/delete/selected-files")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto deleteSelectedFiles(@RequestBody List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        return rscIitIdpStatusService.updateStatusOfFiles(rscIitIdpStatusDTOs);
    }

    @PostMapping("/rsc-iit-idp-status/saveOtifDate")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto saveOtifDateInRscIitIdpStatus(@RequestBody MpsPlanViewDto mpsPlanViewDto) {
        return rscIitIdpStatusService.saveOtifDate(mpsPlanViewDto);
    }

    @PostMapping("/rsc-iit-idp-status/update")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto updateRscIitIdpStatus(@RequestBody MpsPlanViewDto mpsPlanViewDto) {
        return rscIitIdpStatusService.updateRscIitIdpStatus(mpsPlanViewDto);
    }

    @GetMapping("/mps-plan-name/exist/except-active-plan/{name}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Boolean mpsPlanNameExistsForEdit(@PathVariable String name) {
        return rscIitIdpStatusService.mpsNameExistsForEdit(name);
    }

    @PostMapping("/rsc-iit-idp-status/save/execution-status")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public MpsPlanViewDto saveExecutionStatusInRscIitIdpStatus(@RequestBody MpsPlanViewDto mpsPlanViewDto) {
        return rscIitIdpStatusService.saveExecutionStatus(mpsPlanViewDto);
    }
}