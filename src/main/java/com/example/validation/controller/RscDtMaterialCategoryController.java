package com.example.validation.controller;

import com.example.validation.dto.RscDtMaterialCategoryDTO;
import com.example.validation.service.RscDtMaterialCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtMaterialCategoryController {
    private final RscDtMaterialCategoryService rscDtMaterialCategoryService;

    public RscDtMaterialCategoryController(RscDtMaterialCategoryService  rscDtMaterialCategoryService ){
        this.rscDtMaterialCategoryService  = rscDtMaterialCategoryService ;
    }

    @GetMapping("/rsc-dt-material-category")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtMaterialCategoryDTO>> getAllRscDtMaterialCategory() {
        return ResponseEntity.ok().body(rscDtMaterialCategoryService .findAll());
    }
}

