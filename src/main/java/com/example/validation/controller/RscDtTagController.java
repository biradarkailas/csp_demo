package com.example.validation.controller;

import com.example.validation.dto.RscDtTagDTO;
import com.example.validation.service.RscDtTagService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtTagController {
    private final RscDtTagService rscDtTagService;

    public RscDtTagController(RscDtTagService rscDtTagService){
        this.rscDtTagService = rscDtTagService;
    }

    @GetMapping("/rsc-dt-tag")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_FG','role_PM','role_RM')")
    public ResponseEntity<List<RscDtTagDTO>> getAllRscDtTag() {
        return ResponseEntity.ok().body(rscDtTagService.findAll());
    }
}
