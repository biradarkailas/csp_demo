package com.example.validation.controller;

import com.example.validation.service.RsyncService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;


@RestController
@RequestMapping("/api")
public class RsyncController {

    private final Logger log = LoggerFactory.getLogger(RsyncController.class);

    private final RsyncService rsyncService;

    public RsyncController(RsyncService rsyncService) {
        this.rsyncService = rsyncService;
    }

    @GetMapping("/file-validation/{mpsName}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Set<String> rsyncConnectionWithoutRsync(@PathVariable String mpsName) {
        log.debug("Loading...");
        return rsyncService.loadFilesWithoutRsync(mpsName);
    }
}