package com.example.validation.controller;

import com.example.validation.dto.RscDtItemCategoryDTO;
import com.example.validation.service.RscDtItemCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtItemCategoryController {
    private final RscDtItemCategoryService rscDtItemCategoryService;

    public RscDtItemCategoryController(RscDtItemCategoryService rscDtItemCategoryService) {
        this.rscDtItemCategoryService = rscDtItemCategoryService;
    }

    @GetMapping("/rsc-dt-item-category")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtItemCategoryDTO>> getAllRscDtItemCategory() {
        return ResponseEntity.ok().body(rscDtItemCategoryService.findAll());
    }
}
