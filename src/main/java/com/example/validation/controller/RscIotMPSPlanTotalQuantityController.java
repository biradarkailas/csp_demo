package com.example.validation.controller;

import com.example.validation.dto.MPSDeltaAnalysisMainDTO;
import com.example.validation.service.RscIotMPSPlanTotalQuantityService;
import com.example.validation.specification.RscMtPPDetailsCriteria;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RscIotMPSPlanTotalQuantityController {
    private final RscIotMPSPlanTotalQuantityService rscIotMPSPlanTotalQuantityService;

    public RscIotMPSPlanTotalQuantityController(RscIotMPSPlanTotalQuantityService rscIotMPSPlanTotalQuantityService) {
        this.rscIotMPSPlanTotalQuantityService = rscIotMPSPlanTotalQuantityService;
    }

    @GetMapping("/mps-plan-total-quantity/create/{ppHeaderId}")
    public String calculateMPSPlanTotalQuantity(@PathVariable Long ppHeaderId) {
        return "MPS Plan Total Quantity Created";
    }

    @PostMapping("/mps-delta-analysis")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<MPSDeltaAnalysisMainDTO> getFilteredMPSDeltaAnalysis(@RequestBody RscMtPPDetailsCriteria criteria) {
        return ResponseEntity.ok().body(rscIotMPSPlanTotalQuantityService.getFilteredMPSDeltaAnalysis(criteria));
    }
}
