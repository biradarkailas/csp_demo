package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotRmCoverDaysService;
import com.example.validation.util.ExecutionResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotRmCoverDaysController {
    private final RscIotRmCoverDaysService rscIotRmCoverDaysService;
    private final Logger log = LoggerFactory.getLogger(RscIotRmCoverDaysController.class);
    private final ExecutionErrorLogService executionErrorLogService;


    public RscIotRmCoverDaysController(RscIotRmCoverDaysService rscIotRmCoverDaysService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotRmCoverDaysService = rscIotRmCoverDaysService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/rm-cover-days/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createRMCoverDaysCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotRmCoverDaysService.calculateRscIotRmCoverDays(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Cover Days", "RM Cover Days Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMCoverDaysCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Cover Days", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-cover-days-summary/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createRMCoverDaysSummaryCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotRmCoverDaysService.calculateRscIotRmCoverDaysSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Cover Days Summary", "RM Cover Days Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMCoverDaysSummaryCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Cover Days Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-cover-days-summary-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RmCoverDaysSummaryDTO> getRMCoverDaysSummary(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRmCoverDaysService.getRmCoverDaysSummaryDetails(ppHeaderId));
    }

    @GetMapping("/rm-cover-days-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RmCoverDaysDetailsDTO> getRmCoverDaysTotalConsumptionDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRmCoverDaysService.getRmCoverDaysDetails(ppHeaderId));
    }

    @GetMapping("/rm-cover-days-summary-all-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<CoverDaysSummaryDetailsDTO> getRMCoverDaysSummaryDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRmCoverDaysService.getRmCoverDaysSummaryAllDetails(ppHeaderId));
    }

    @GetMapping("/rm-coverDays-nullStdPrice-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RmCoverDaysDetailsDTO> getRmCoverDaysNullStdPriceDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRmCoverDaysService.getNullStdPriceDetails(ppHeaderId));
    }

    @PutMapping("/rm-coverDays-saveAllDetails")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RmCoverDaysDTO>> updateRscIotRMCoverDays(@RequestBody List<RmCoverDaysDTO> rmCoverDaysDTO) throws ResourceNotFoundException {
        log.debug("REST request to update RscIotRMCoverDaysDetails : {}", rmCoverDaysDTO);
        List<RmCoverDaysDTO> result = rscIotRmCoverDaysService.saveAllRmCoverDaysDetails(rmCoverDaysDTO);
        return ResponseEntity.ok().body(result);
    }

    /*@GetMapping("/rm-cover-days-class-wise-summary-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<ClassWiseCoverDaysDetailsDTO> getRMCoverDaysClassWiseSummaryDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRmCoverDaysService.getRmCoverDaysClassWiseSummaryDetails(ppHeaderId));
    }*/
}