package com.example.validation.controller;

import com.example.validation.dto.RscDtSignatureDTO;
import com.example.validation.service.RscDtSignatureService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/api")
public class RscDtSignatureController {
    private final RscDtSignatureService rscDtSignatureService;

    public RscDtSignatureController(RscDtSignatureService rscDtSignatureService) {
        this.rscDtSignatureService = rscDtSignatureService;
    }

    @GetMapping("/rsc-dt-signature")
    public ResponseEntity<List<RscDtSignatureDTO>> getAllRscDtSignature() {
        return ResponseEntity.ok().body(rscDtSignatureService.findAll());
    }
}
