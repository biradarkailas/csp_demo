package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.slob.FGSlobTotalValueDetailDTO;
import com.example.validation.dto.slob.RscIotFGSlobDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotFGSlobService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotFGSlobController {

    private final RscIotFGSlobService rscIotFGSlobService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotFGSlobController(RscIotFGSlobService rscIotFGSlobService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotFGSlobService = rscIotFGSlobService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/slob/create/fg/{ppHeaderId}")
    public ExecutionResponseDTO createFGSlob(@PathVariable Long ppHeaderId) {
        try {
            rscIotFGSlobService.createFGSlob(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Slob", "FG Slob Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFGSlob", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Slob", e.getMessage(), false);
        }
    }

    @GetMapping("/slob/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<List<RscIotFGSlobDTO>> getFGSlob(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFGSlobService.getRscIotFGSlobDTO(ppHeaderId));
    }

    @GetMapping("/slob/total-values/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<FGSlobTotalValueDetailDTO> getFGSlobTotalValueDetail(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFGSlobService.getFGSlobTotalValueDetail(ppHeaderId));
    }
}
