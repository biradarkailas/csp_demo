package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.RscIotLatestTotalMouldSaturationDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotLatestTotalMouldSaturationService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotLatestTotalMouldSaturationController {
    private final RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotLatestTotalMouldSaturationController(RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotLatestTotalMouldSaturationService = rscIotLatestTotalMouldSaturationService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/latest-total-mould-saturation/{ppheaderId}")
    public ExecutionResponseDTO saveOriginalTotalMouldSaturation(@PathVariable Long ppheaderId) {
        try {
            rscIotLatestTotalMouldSaturationService.saveLatestTotalMouldSaturation(ppheaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Latest Total Mould Saturation", "Latest Total Mould Saturation Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("saveOriginalTotalMouldSaturation", e.getMessage(), ppheaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Latest Total Mould Saturation", e.getMessage(), false);
        }
    }

    @GetMapping("/total-mould-saturation/{ppheaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<RscIotLatestTotalMouldSaturationDTO>> getLatestTotalMouldSaturation(@PathVariable Long ppheaderId) {
        return ResponseEntity.ok().body(rscIotLatestTotalMouldSaturationService.findAllByRscMtPPheaderId(ppheaderId));
    }
}
