package com.example.validation.controller;

import com.example.validation.dto.RscDtProductionTypeDTO;
import com.example.validation.service.RscDtProductionTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtProductionTypeController {
    private final RscDtProductionTypeService rscDtProductionTypeService;

    public RscDtProductionTypeController(RscDtProductionTypeService rscDtProductionTypeService){
        this.rscDtProductionTypeService = rscDtProductionTypeService;
    }

    @GetMapping("/rsc-dt-production-type")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtProductionTypeDTO>> getAllRscDtProductionType() {
        return ResponseEntity.ok().body(rscDtProductionTypeService.findAll());
    }
}
