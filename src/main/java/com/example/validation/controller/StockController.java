package com.example.validation.controller;

import com.example.validation.dto.PMStockDTO;
import com.example.validation.dto.PmStockDetailDTO;
import com.example.validation.service.StockService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class StockController {
    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("/stocks/pm/{mpsId}")
    PMStockDTO getPmStockByMpsID(@PathVariable Long mpsId, @PathVariable(required = false) String stockType) {
        return stockService.getPmStockByMpsID(mpsId, stockType);
    }

    @GetMapping("/stocks/pm/mes/{mpsId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    PMStockDTO getPmStockWithMes(@PathVariable Long mpsId) {
        return stockService.getPmStockWithMes(mpsId);
    }

    @GetMapping("/stocks/pm/rtd/{mpsId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    PMStockDTO getPmStockWithRtd(@PathVariable Long mpsId) {
        return stockService.getPmStockWithRtd(mpsId);
    }

    @GetMapping("/stocks/details/pm/{mpsId}/{rscMtItemId}")
    PmStockDetailDTO getPmStockDetailsByMpsID(@PathVariable String mpsId, @PathVariable Long rscMtItemId) {
        return stockService.getPmStockDetailByMpsID(mpsId, rscMtItemId);
    }
    //Get Stock Data With Named Native Query

    @GetMapping("/stocks/pm/mes/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List> getPmStockWithMeWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(stockService.getPmStockWithMesWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stocks/pm/mes/with-rejection/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List> getPmStockWithMesRejectionWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(stockService.getPmStockWithMesRejectionWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stocks/pm/rtd/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List> getPmStockWithRtdWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(stockService.getPmStockWithRtdWithNamedQuery(ppHeaderId));
    }
}
