package com.example.validation.controller;

import com.example.validation.dto.RscDtReasonTypeDTO;
import com.example.validation.service.RscDtReasonTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtReasonTypeController {
    private final RscDtReasonTypeService rscDtReasonTypeService;

    public RscDtReasonTypeController(RscDtReasonTypeService rscDtReasonTypeService){
        this.rscDtReasonTypeService = rscDtReasonTypeService;
    }

    @GetMapping("/rsc-dt-reason-type")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtReasonTypeDTO>> getAllRscDtReasonType() {
        return ResponseEntity.ok().body(rscDtReasonTypeService.findAll());
    }
}
