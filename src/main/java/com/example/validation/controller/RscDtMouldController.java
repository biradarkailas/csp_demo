package com.example.validation.controller;

import com.example.validation.dto.RscDtMouldDTO;
import com.example.validation.service.RscDtMouldService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtMouldController {
    private final RscDtMouldService rscDtMouldService;

    public RscDtMouldController(RscDtMouldService rscDtMouldService) {
        this.rscDtMouldService = rscDtMouldService;
    }

    @GetMapping("/rsc-dt-mould")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Admin')")
    public ResponseEntity<List<RscDtMouldDTO>> getAllRscDtMould() {
        return ResponseEntity.ok().body( rscDtMouldService.findAll() );
    }

    @GetMapping("/rsc-dt-mould-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Admin')")
    public ResponseEntity<List<RscDtMouldDTO>> findMouldListWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( rscDtMouldService.findPmMouldWithPagination( pageNo, pageSize ) );
    }
}
