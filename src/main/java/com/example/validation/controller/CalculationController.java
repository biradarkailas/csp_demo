package com.example.validation.controller;

import com.example.validation.service.CalculationService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class CalculationController {

    private final CalculationService calculationService;

    public CalculationController(CalculationService calculationService) {
        this.calculationService = calculationService;
    }

    @GetMapping("/calculate-all-data/{ppHeaderId}")
    public String createLogicalConsumption(@PathVariable Long ppHeaderId) {
        return calculationService.createAllCalculation(ppHeaderId);
    }
}