package com.example.validation.controller;

import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.service.RscDtSupplierService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtSupplierController {
    private final RscDtSupplierService rscDtSupplierService;

    public RscDtSupplierController(RscDtSupplierService rscDtSupplierService){
        this.rscDtSupplierService = rscDtSupplierService;
    }

    @GetMapping("/rsc-dt-supplier")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtSupplierDTO>> getAllRscDtSupplier() {
        return ResponseEntity.ok().body(rscDtSupplierService.findAll());
    }
}
