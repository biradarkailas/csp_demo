package com.example.validation.controller;

import com.example.validation.dto.RscDtItemCodeDTO;
import com.example.validation.service.RscDtItemCodeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtItemCodeController {
    private final RscDtItemCodeService rscDtItemCodeService;

    public RscDtItemCodeController(RscDtItemCodeService rscDtItemCodeService) {
        this.rscDtItemCodeService = rscDtItemCodeService;
    }

    @GetMapping("/rsc-dt-item-codes")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtItemCodeDTO>> getAllRscDtItemCodes() {
        return ResponseEntity.ok().body(rscDtItemCodeService.findAll());
    }
}