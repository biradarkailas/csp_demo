package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotPmCoverDaysService;
import com.example.validation.util.ExecutionResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotPmCoverDaysController {
    private final RscIotPmCoverDaysService rscIotPmCoverDaysService;
    private final Logger log = LoggerFactory.getLogger(RscIotPmCoverDaysController.class);
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotPmCoverDaysController(RscIotPmCoverDaysService rscIotPmCoverDaysService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotPmCoverDaysService = rscIotPmCoverDaysService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/pm-cover-days/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createPMCoverDaysCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotPmCoverDaysService.calculateRscIotPmCoverDays(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Cover Days", "PM Cover Days Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMCoverDaysCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Cover Days", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-cover-days-summary/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createPMCoverDaysSummaryCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotPmCoverDaysService.calculateRscIotPmCoverDaysSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Cover Days Summary", "PM Cover Days Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMCoverDaysSummaryCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Cover Days Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-cover-days-summary-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PmCoverDaysSummaryDTO> getPMCoverDaysSummary(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmCoverDaysService.getPmCoverDaysSummaryDetails(ppHeaderId));
    }

    @GetMapping("/pm-cover-days-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PmCoverDaysDetailsDTO> getPMCoverDaysDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmCoverDaysService.getPmCoverDaysDetails(ppHeaderId));
    }

    @GetMapping("/pm-cover-days-summary-all-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<CoverDaysSummaryDetailsDTO> getPMCoverDaysSummaryDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmCoverDaysService.getPmCoverDaysSummaryAllDetails(ppHeaderId));
    }

    @GetMapping("/pm-coverDays-nullStdPrice-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PmCoverDaysDetailsDTO> getPmCoverDaysNullStdPriceDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmCoverDaysService.getNullStdPriceDetails(ppHeaderId));
    }

    @PutMapping("/pm-coverDays-saveAllDetails")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<PmCoverDaysDTO>> updateRscIotPMCoverDays(@RequestBody List<PmCoverDaysDTO> pmCoverDaysDTO) throws ResourceNotFoundException {
        log.debug("REST request to update RscIotPMCoverDaysDetails : {}", pmCoverDaysDTO);
        List<PmCoverDaysDTO> result = rscIotPmCoverDaysService.saveAllPmCoverDaysDetails(pmCoverDaysDTO);
        return ResponseEntity.ok().body(result);
    }
}