package com.example.validation.controller;

import com.example.validation.dto.RMStockDTO;
import com.example.validation.dto.RmMesStockPaginationDto;
import com.example.validation.dto.RscStockDetailDTO;
import com.example.validation.service.RmStockService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RmStockController {
    private final RmStockService rmStockService;

    public RmStockController(RmStockService rmStockService) {
        this.rmStockService = rmStockService;
    }

    @GetMapping("/stocks/rm/{ppHeaderId}")
    public RMStockDTO getAllRMStocks(@PathVariable Long ppHeaderId, @PathVariable(required = false) String stockType) {
        return rmStockService.getAllRMStocksDTO(ppHeaderId, stockType);
    }

    @GetMapping("/stocks/rm/details/{ppHeaderId}/{rscMtItemId}")
    public RscStockDetailDTO getAllRMStocksSpecificationData(@PathVariable Long ppHeaderId, @PathVariable Long rscMtItemId) {
        return rmStockService.getAllRMStocksDetailData(ppHeaderId, rscMtItemId);
    }

    @GetMapping("/stock/rm/mes/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public RMStockDTO getRmStockWithMes(@PathVariable Long ppHeaderId) {
        return rmStockService.getRmStockWithMes(ppHeaderId);
    }

    @GetMapping("/stock/rm/rtd/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public RMStockDTO getRmStockWithRtd(@PathVariable Long ppHeaderId) {
        return rmStockService.getRmStockWithRtd(ppHeaderId);
    }

    @GetMapping("/stock/rm/bulkOpRm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public RMStockDTO getRmStockWithBulkOpRm(@PathVariable Long ppHeaderId) {
        return rmStockService.getRmStockWithBulkOpRm(ppHeaderId);
    }

    @GetMapping("/stock/rm/transferStock/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public RMStockDTO getRmStockWithTransferStock(@PathVariable Long ppHeaderId) {
        return rmStockService.getRmStockWithTransferStock(ppHeaderId);
    }

    @GetMapping("/stock/rm/rejection/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public RMStockDTO getRmStockWithRejection(@PathVariable Long ppHeaderId) {
        return rmStockService.getRmStockWithRejection(ppHeaderId);
    }

    //Get Stock Data With Named Native Query
    @GetMapping("/stock/rm/mes/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List> getRmStockWithMesWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getRmStockWithMesWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stock/rm/mes/with-rejection/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List> getRmStockWithMesRejectionWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getRmStockWithMesRejectionWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stock/rm/rtd/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List> getRmStockWithRtdWithPagination(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getRmStockWithRtdWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stock/rm/bulkOpRm/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List> getRmStockWithBulkOpRmWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getRmStockWithBulkOpRmWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stock/rm/transferStock/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List> getRmStockWithTransferStockWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getRmStockWithTransferStockWithNamedQuery(ppHeaderId));
    }

    @GetMapping("/stock/rm/rejection/with-named-query/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List> getRmStockWithRejectionWithNamedQuery(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getRmStockWithRejectionWithNamedQuery(ppHeaderId));
    }


//    Mes New Api with optimization  
    @GetMapping("/rm/mes/stock/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RmMesStockPaginationDto>> getStockWithMes(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rmStockService.getStockWithMes(ppHeaderId));
    }
}
