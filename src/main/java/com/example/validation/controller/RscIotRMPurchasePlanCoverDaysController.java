package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCoverDaysDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotRMPurchasePlanCoverDaysService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotRMPurchasePlanCoverDaysController {

    private final RscIotRMPurchasePlanCoverDaysService rscIotRMPurchasePlanCoverDaysService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotRMPurchasePlanCoverDaysController(RscIotRMPurchasePlanCoverDaysService rscIotRMPurchasePlanCoverDaysService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotRMPurchasePlanCoverDaysService = rscIotRMPurchasePlanCoverDaysService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/purchase-plan/cover-days/rm/create/{ppheaderId}")
    public ExecutionResponseDTO calculatePMPurchasePlanCoverDays(@PathVariable Long ppheaderId) {
        try {
            rscIotRMPurchasePlanCoverDaysService.calculateRMPurchasePlanCoverDays(ppheaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Purchase Plan Cover Days", "RM Purchase Plan Cover Days Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("calculatePMPurchasePlanCoverDays", e.getMessage(), ppheaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Purchase Plan Cover Days", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-purchase-plan-cover-days/{ppheaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public List<RscIotRMPurchasePlanCoverDaysDTO> getAllRMPurchasePlanCoverDays(@PathVariable Long ppheaderId) {
        return rscIotRMPurchasePlanCoverDaysService.getAllRMPurchasePlanCoverDays(ppheaderId);
    }

    @GetMapping("/rm-purchase-plan-cover-days/stock-equation/{ppheaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public List<PurchasePlanCoverDaysEquationDTO> getPurchasePlanCoverDaysEquationDTOs(@PathVariable Long ppheaderId) {
        return rscIotRMPurchasePlanCoverDaysService.getPurchasePlanCoverDaysEquationDTOs(ppheaderId);
    }
}