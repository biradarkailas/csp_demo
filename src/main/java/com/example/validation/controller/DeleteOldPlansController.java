package com.example.validation.controller;

import com.example.validation.service.DeleteOldPlansService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;

@RestController
@RequestMapping("/api")
public class DeleteOldPlansController {

    private final Logger log = LoggerFactory.getLogger(DeleteOldPlansController.class);

    private final DeleteOldPlansService deleteOldPlansService;

    public DeleteOldPlansController(DeleteOldPlansService deleteOldPlansService) {
        this.deleteOldPlansService = deleteOldPlansService;
    }

    @GetMapping("/delete-data/{mpsDate}")
    public String deleteAll(@PathVariable String mpsDate) {
        deleteOldPlansService.deleteAllOldPlans(LocalDate.parse(mpsDate));
        return "All Data Deleted";
    }
}
