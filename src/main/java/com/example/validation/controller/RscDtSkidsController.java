package com.example.validation.controller;


import com.example.validation.dto.RscDtSkidsDTO;
import com.example.validation.service.RscDtSkidsService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtSkidsController {
    private final RscDtSkidsService rscDtSkidsService;

    public RscDtSkidsController(RscDtSkidsService rscDtSkidsService) {
        this.rscDtSkidsService = rscDtSkidsService;
    }

    @GetMapping("/rsc-dt-skids")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_FG')")
    public ResponseEntity<List<RscDtSkidsDTO>> getAllRscDtSkids() {
        return ResponseEntity.ok().body( rscDtSkidsService.findAll() );
    }

    @GetMapping("/rsc-dt-skids-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_FG')")
    public ResponseEntity<List<RscDtSkidsDTO>> findFgSkidsListWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( rscDtSkidsService.findFgSkidsWithPagination( pageNo, pageSize ) );
    }
}

