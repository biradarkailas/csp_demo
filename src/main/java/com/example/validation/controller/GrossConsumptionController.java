package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.service.GrossConsumptionService;
import com.example.validation.service.RscItBulkGrossConsumptionService;
import com.example.validation.service.RscItPMGrossConsumptionService;
import com.example.validation.service.RscItRMGrossConsumptionService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GrossConsumptionController {
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final GrossConsumptionService grossConsumptionService;

    public GrossConsumptionController(RscItPMGrossConsumptionService rscItPMGrossConsumptionService, GrossConsumptionService grossConsumptionService, RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService) {
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.grossConsumptionService = grossConsumptionService;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
    }

    @GetMapping("/gross-consumption/create/wip/{ppHeaderId}")
    public ExecutionResponseDTO createWIPGrossConsumption(@PathVariable Long ppHeaderId) {
        try {
            grossConsumptionService.createWIPGrossConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("WIP Gross Consumption", "WIP Gross Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("WIP Gross Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/gross-consumption/create/{ppHeaderId}")
    public ExecutionResponseDTO createGrossConsumption(@PathVariable Long ppHeaderId) {
        try {
            grossConsumptionService.createGrossConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM and Bulk Gross Consumption", "PM and Bulk Gross Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("PM and Bulk Gross Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/gross-consumption/create/bp/{ppHeaderId}")
    public ExecutionResponseDTO createBPGrossConsumption(@PathVariable Long ppHeaderId) {
        try {
            grossConsumptionService.createBPGrossConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("BP Gross Consumption", "BP Gross Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("BP Gross Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/gross-consumption/create/sub-base/{ppHeaderId}")
    public ExecutionResponseDTO createSubBaseGrossConsumption(@PathVariable Long ppHeaderId) {
        try {
            grossConsumptionService.createSubBaseGrossConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("SubBase Gross Consumption", "SubBase Gross Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("SubBase Gross Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/gross-consumption/create/rm/{ppHeaderId}")
    public ExecutionResponseDTO createRMGrossConsumption(@PathVariable Long ppHeaderId) {
        try {
            grossConsumptionService.createRMGrossConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Gross Consumption", "RM Gross Consumption Created Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Gross Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/gross-consumption/pm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public GrossConsumptionMainDTO getAllPMGrossConsumption(@PathVariable Long ppHeaderId) {
        return rscItPMGrossConsumptionService.getAllPMGrossConsumption(ppHeaderId);
    }

    @GetMapping("/gross-consumption/rm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public GrossConsumptionMainDTO getAllRMGrossConsumption(@PathVariable Long ppHeaderId) {
        return rscItRMGrossConsumptionService.getAllRMGrossConsumption(ppHeaderId);
    }

    @GetMapping("/gross-consumption/bulk/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public GrossConsumptionMainDTO getAllBulkGrossConsumption(@PathVariable Long ppHeaderId) {
        return rscItBulkGrossConsumptionService.getAllBulkGrossConsumption(ppHeaderId);
    }
}