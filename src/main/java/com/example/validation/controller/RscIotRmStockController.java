package com.example.validation.controller;

import com.example.validation.service.RscIotRmStockService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotRmStockController {
    private RscIotRmStockService rscIotRmStockService;

    public RscIotRmStockController(RscIotRmStockService rscIotRmStockService) {
        this.rscIotRmStockService = rscIotRmStockService;
    }
}