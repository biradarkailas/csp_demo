package com.example.validation.controller;

import com.example.validation.dto.RscDtCoverDaysDTO;
import com.example.validation.service.RscDtCoverDaysService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtCoverDaysController {
    private final RscDtCoverDaysService rscDtCoverDaysService;

    public RscDtCoverDaysController(RscDtCoverDaysService rscDtCoverDaysService){
        this.rscDtCoverDaysService = rscDtCoverDaysService;
    }

    @GetMapping("/rsc-dt-cover-days")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtCoverDaysDTO>> getAllRscDtCoverDays() {
        return ResponseEntity.ok().body(rscDtCoverDaysService.findAll());
    }
}
