package com.example.validation.controller;

import com.example.validation.dto.OriginalPurchasePlanDTO;
import com.example.validation.dto.SupplierWiseOriginalMouldSaturationDTO;
import com.example.validation.service.RscIotOriginalMouldSaturationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotOriginalMouldSaturationController {
    private final RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService;

    public RscIotOriginalMouldSaturationController(RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService) {
        this.rscIotOriginalMouldSaturationService = rscIotOriginalMouldSaturationService;
    }

    @GetMapping("/original-mould-saturation/{ppHeaderId}")
    public String saveMouldSaturation(@PathVariable Long ppHeaderId) {
        return rscIotOriginalMouldSaturationService.saveMouldSaturation(ppHeaderId);
    }

    @GetMapping("/get-rsc-iot-original-mould-saturation-calculations")
    public ResponseEntity<List<OriginalPurchasePlanDTO>> getAllMouldSaturationCalculations() {
        return ResponseEntity.ok().body(rscIotOriginalMouldSaturationService.findAll());
    }
}
