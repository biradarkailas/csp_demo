package com.example.validation.controller;

import com.example.validation.service.RscItPMGrossConsumptionService;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscItPMGrossConsumptionController {
    private RscItPMGrossConsumptionService rscItPMGrossConsumptionService;

    public RscItPMGrossConsumptionController(RscItPMGrossConsumptionService rscItPMGrossConsumptionService) {
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
    }
}