package com.example.validation.controller;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.NodeDTO;
import com.example.validation.service.RscMtBomService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscMtBomController {
    private final RscMtBomService rscMtBomService;

    public RscMtBomController(RscMtBomService rscMtBomService) {
        this.rscMtBomService = rscMtBomService;
    }

    @GetMapping("/bom/fg-relation/pm/{ppHeaderId}/{fgItemId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<NodeDTO> getPMFGRelation(@PathVariable Long ppHeaderId, @PathVariable Long fgItemId) {
        return ResponseEntity.ok().body(rscMtBomService.getFGRelation(fgItemId, MaterialCategoryConstants.PM, ppHeaderId));
    }

    @GetMapping("/bom/fg-relation/rm/{ppHeaderId}/{bulkItemId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<NodeDTO> getRMBulkRelation(@PathVariable Long ppHeaderId, @PathVariable Long bulkItemId) {
        return ResponseEntity.ok().body(rscMtBomService.getFGRelation(bulkItemId, MaterialCategoryConstants.RM, ppHeaderId));
    }

    @GetMapping("/bom/fg-relation/bulk/{ppHeaderId}/{fgItemId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<NodeDTO> getBulkFGRelation(@PathVariable Long ppHeaderId, @PathVariable Long fgItemId) {
        return ResponseEntity.ok().body(rscMtBomService.getFGRelation(fgItemId, MaterialCategoryConstants.BULK, ppHeaderId));
    }
}