package com.example.validation.controller;

import com.example.validation.dto.RscDtFGTypeDTO;
import com.example.validation.service.RscDtFGTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtFGTypeController {

    private final RscDtFGTypeService rscDtFGTypeService;

    public RscDtFGTypeController(RscDtFGTypeService rscDtFGTypeService) {
        this.rscDtFGTypeService = rscDtFGTypeService;
    }

    @GetMapping("/rsc-dt-fg-type")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<List<RscDtFGTypeDTO>> findAll() {
        return ResponseEntity.ok().body(rscDtFGTypeService.findAll());
    }
}
