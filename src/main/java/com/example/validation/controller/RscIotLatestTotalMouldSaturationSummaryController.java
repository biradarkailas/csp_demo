package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.MouldSaturationDashboardDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotLatestTotalMouldSaturationSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RscIotLatestTotalMouldSaturationSummaryController {
    private final RscIotLatestTotalMouldSaturationSummaryService rscIotLatestTotalMouldSaturationSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotLatestTotalMouldSaturationSummaryController(RscIotLatestTotalMouldSaturationSummaryService rscIotLatestTotalMouldSaturationSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotLatestTotalMouldSaturationSummaryService = rscIotLatestTotalMouldSaturationSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/latest-total-mould-saturation-summary/create/{ppHeaderId}")
    public ExecutionResponseDTO createMouldSaturationSummary(@PathVariable Long ppHeaderId) {
        try {
            rscIotLatestTotalMouldSaturationSummaryService.createMouldSaturationSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Latest Total Mould Saturation Summary", "Latest Total Mould Saturation Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createMouldSaturationSummary", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Latest Total Mould Saturation Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/mould-saturation/dashboard/range-wise-mould-details/{ppheaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<MouldSaturationDashboardDTO> getMouldSaturationDashboardDtoDetails(@PathVariable Long ppheaderId, @RequestParam(required = false) Long monthValue) {
        return ResponseEntity.ok().body(rscIotLatestTotalMouldSaturationSummaryService.getMouldSaturationDashboardDtoDetails(ppheaderId, monthValue));
    }
}
