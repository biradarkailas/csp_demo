package com.example.validation.controller;

import com.example.validation.dto.ItemCodeDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.service.RscItBulkGrossConsumptionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscItBulkGrossConsumptionController {
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;

    public RscItBulkGrossConsumptionController(RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService) {
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
    }

    @GetMapping("/bulk-list/{ppHeaderId}")
    public ResponseEntity<ItemCodeDTO> getFGItemCodeDTO(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItBulkGrossConsumptionService.getBulkItemCodeDTO(ppHeaderId));
    }

    @GetMapping("/gross-consumption/by-bulk/{ppHeaderId}/{bulkItemId}")
    public ResponseEntity<RscItGrossConsumptionDTO> getFGItemCodeDTO(@PathVariable Long ppHeaderId, @PathVariable Long bulkItemId) {
        return ResponseEntity.ok().body(rscItBulkGrossConsumptionService.findByRscMtPPheaderIdAndBulkItemId(ppHeaderId, bulkItemId));
    }
}