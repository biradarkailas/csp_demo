package com.example.validation.controller;

import com.example.validation.dto.RscDtItemTypeDTO;
import com.example.validation.service.MaterialCategoryMasterService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MaterialCategoryMasterController {
    private final MaterialCategoryMasterService materialCategoryMasterService;

    public MaterialCategoryMasterController(MaterialCategoryMasterService materialCategoryMasterService) {
        this.materialCategoryMasterService = materialCategoryMasterService;
    }

    @GetMapping("/material-Category-masters/packing-material-list")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public List<RscDtItemTypeDTO> getMaterialCategoryListOfPackingMaterial() {
        return materialCategoryMasterService.getMaterialCategoryListOfPackingMaterial();
    }

    @GetMapping("/material-Category-masters/packing-material-list/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List> getMaterialCategoryListOfPackingMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body(materialCategoryMasterService.getMaterialCategoryListOfPackingMaterialWithPagination(pageNo, pageSize));
    }
}
