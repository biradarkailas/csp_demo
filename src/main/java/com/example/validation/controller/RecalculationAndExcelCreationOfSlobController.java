package com.example.validation.controller;

import com.example.validation.dto.PmSlobDTO;
import com.example.validation.dto.RmSlobDTO;
import com.example.validation.service.RecalculationAndExcelCreationOnUpdateService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RecalculationAndExcelCreationOfSlobController {
    private final RecalculationAndExcelCreationOnUpdateService recalculationAndExcelCreationOnUpdateService;
    private final Logger log = LoggerFactory.getLogger(RecalculationAndExcelCreationOfSlobController.class);

    public RecalculationAndExcelCreationOfSlobController(RecalculationAndExcelCreationOnUpdateService recalculationAndExcelCreationOnUpdateService) {
        this.recalculationAndExcelCreationOnUpdateService = recalculationAndExcelCreationOnUpdateService;
    }

    @PutMapping("/rm-slob-saveAllDetails/{sitValue}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RmSlobDTO>> updateRscIotRMSlob(@RequestBody List<RmSlobDTO> rmSlobDTO, @PathVariable Double sitValue) throws ResourceNotFoundException {
        log.debug("REST request to update RscItPMSlobDetails : {}", rmSlobDTO);
        List<RmSlobDTO> result = recalculationAndExcelCreationOnUpdateService.saveAllRmSlobDetails(rmSlobDTO, sitValue);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/rm-slob-save-reasons/{sitValue}/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RmSlobDTO>> updateRscItRMSlobList(@RequestBody List<RmSlobDTO> rmSlobDTO, @PathVariable Double sitValue, @PathVariable Long ppHeaderId) throws ResourceNotFoundException {
        log.debug("REST request to update RscIotRMSlobList : {}", rmSlobDTO);
        List<RmSlobDTO> result = recalculationAndExcelCreationOnUpdateService.saveAllRmSlobs(rmSlobDTO, sitValue, ppHeaderId);
        return ResponseEntity.ok().body(result);
    }


    @PutMapping("/pm-slob-saveAllDetails/{sitValue}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<PmSlobDTO>> updateRscIotPMSlob(@RequestBody List<PmSlobDTO> pmSlobDTO, @PathVariable Double sitValue) throws ResourceNotFoundException {
        log.debug("REST request to update RscItPMSlobDetails : {}", pmSlobDTO);
        List<PmSlobDTO> result = recalculationAndExcelCreationOnUpdateService.saveAllPmSlobDetails(pmSlobDTO,sitValue);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/pm-slob-save-reasons/{sitValue}/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<PmSlobDTO>> updateRscItPMSlobList(@RequestBody List<PmSlobDTO> pmSlobDTO, @PathVariable Double sitValue, @PathVariable Long ppHeaderId) throws ResourceNotFoundException {
        log.debug("REST request to update RscIotPMSlobList : {}", pmSlobDTO);
        List<PmSlobDTO> result = recalculationAndExcelCreationOnUpdateService.saveAllPmSlobs(pmSlobDTO, sitValue, ppHeaderId);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/rm-cover-days/excel/export/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public String getRmCoverDaysAndSlobExports(@PathVariable Long ppHeaderId) {
        return recalculationAndExcelCreationOnUpdateService.createRmExcelExportOnUpdate(ppHeaderId);
    }

    @GetMapping("/pm-cover-days/excel/export/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public String getPmCoverDaysAndSlobExports(@PathVariable Long ppHeaderId) {
        return recalculationAndExcelCreationOnUpdateService.createPmExcelExportOnUpdate(ppHeaderId);
    }
}
