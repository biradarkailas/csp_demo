package com.example.validation.controller;

import com.example.validation.dto.RscDtTransportTypeDTO;
import com.example.validation.service.RscDtTransportTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtTransportTypeController {
    private final RscDtTransportTypeService rscDtTransportTypeService;

    public RscDtTransportTypeController(RscDtTransportTypeService rscDtTransportTypeService){
        this.rscDtTransportTypeService = rscDtTransportTypeService;
    }

    @GetMapping("/rsc-dt-transport-type")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtTransportTypeDTO>> getAllRscDtTransportType() {
        return ResponseEntity.ok().body(rscDtTransportTypeService.findAll());
    }
}
