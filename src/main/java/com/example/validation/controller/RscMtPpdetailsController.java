package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.RscMtPpdetailsService;
import com.example.validation.specification.RscMtPPDetailsCriteria;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscMtPpdetailsController {
    private final RscMtPpdetailsService rscMtPpdetailsService;

    public RscMtPpdetailsController(RscMtPpdetailsService rscMtPpdetailsService) {
        this.rscMtPpdetailsService = rscMtPpdetailsService;
    }

    @GetMapping("/mps-plan/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<MpsPlanMainDTO> getProductionPlan(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getProductionPlan(ppHeaderId));
    }
    @GetMapping("/mps-plan/latest")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<MpsPlanMainDTO> getLatestProductionPlan() {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getLatestProductionPlan());
    }

    @GetMapping("/mps/fg-list/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<ItemCodeDTO> getFGItemCodeDTO(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getFGItemCodeDTO(ppHeaderId));
    }

    @GetMapping("/mps-plan/by-fg/{ppHeaderId}/{fgItemId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<ItemDetailDTO> getMPSPlanByFG(@PathVariable Long ppHeaderId, @PathVariable Long fgItemId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getMPSPlanByFG(ppHeaderId, fgItemId));
    }

    @GetMapping("/mps-plan/by-fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_RM')")
    public ResponseEntity<List<ItemDetailDTO>> getMPSPlanByFG1(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getCodeMPSPlanByFG(ppHeaderId));
    }

    @GetMapping("/mps-plan-summary/by-brand/{ppHeaderId}")
    public ResponseEntity<MonthValuesDTO> getPlanSummaryByBrand(@PathVariable Long ppHeaderId, @RequestParam(required = false) Long divisionId, @RequestParam(required = false) Long signatureId, @RequestParam(required = false) Long brandId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getMPSPlanSummaryByDivision(ppHeaderId, divisionId, signatureId, brandId));
    }

    @GetMapping("/mps-plan-summary/by-category/{ppHeaderId}")
    public ResponseEntity<MonthValuesDTO> getPlanSummaryByCategory(@PathVariable Long ppHeaderId, @RequestParam(required = false) Long categoryId, @RequestParam(required = false) Long subCategoryId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getMPSPlanSummaryByCategory(ppHeaderId, categoryId, subCategoryId));
    }

    @GetMapping("/mps-plan-summary/by-lines-and-skids/{ppHeaderId}")
    public ResponseEntity<MonthValuesDTO> getPlanSummaryByLinesAndSkids(@PathVariable Long ppHeaderId, @RequestParam(required = false) Long lineId, @RequestParam(required = false) Long skidId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getMPSPlanSummaryByLinesAndSkids(ppHeaderId, lineId, skidId));
    }

    @GetMapping("/rsc-mt-ppdetails/{ppHeaderId}")
    public ResponseEntity<List<RscMtPPdetailsDTO>> getAllPPdetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.findAllPPdetailsByPPheader(ppHeaderId));
    }

    @GetMapping("/mps-plan-summary/{ppHeaderId}")
    public ResponseEntity<MpsPlanSummaryDTO> getPPdetailsListForMpsPlanSummary(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.getPPdetailsListForMpsPlanSummary(ppHeaderId));
    }

    @GetMapping("/rsc-mt-ppdetails/by-filter")
    public ResponseEntity<List<RscMtPPdetailsDTO>> getAllPPdetails(@RequestBody RscMtPPDetailsCriteria criteria) {
        return ResponseEntity.ok().body(rscMtPpdetailsService.findAllByCriteria(criteria));
    }
}