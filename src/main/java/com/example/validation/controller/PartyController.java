package com.example.validation.controller;

import com.example.validation.domain.*;
import com.example.validation.repository.PartyNameRepo;
import com.example.validation.service.impl.*;
import com.example.validation.util.enums.ServiceType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Locale;

@RestController
public class PartyController {
    @Autowired
    PartyContactDetailsService partyContactDetailsService;

    @Autowired
    PartyAddressDetailsService partyAddressDetailsService;

    @Autowired
    PartyNameService partyNameService;

    @Autowired
    PartyRelationshipService partyRelationshipService;

    @Autowired
    CustomPartyService customPartyService;

    @Autowired
    PartyTypeUserService partyTypeUserService;

    @Autowired
    PartyService partyService;

    @Autowired
    PartyRelationshipTypeService partyRelationshipTypeService;

    @Autowired
    PartyNameRepo partyNameRepo;

    @PostMapping("/party/user")
    public Party savePartyWithTypeUser(@Valid @RequestBody Party party) {
        return customPartyService.addPartyWithTypeUser(party);
    }

    @PutMapping("/party/user")
    public Party updatePartyWithTypeUser(@Valid @RequestBody Party party) {
        return customPartyService.editPartyWithTypeUser(party);
    }

    @GetMapping("/party/user")
    public List<Party> getPartyTypeUsers(@RequestParam Long organizationId) {
        return customPartyService.getPartyTypeUsers(organizationId);
    }

    @GetMapping("/party/user/{id}")
    public CustomPartyService.PartyView getPartyTypeUser(@PathVariable Long id) {
        return customPartyService.getPartyView(id);
    }

    @GetMapping("/orgnization-detail/username")
    public CustomPartyService.CustomOrgnizationDetail getOrganizationNameOfFFByUsername(@RequestParam String username) {
        return customPartyService.getOrgnizationDetailByUsername(username);
    }

    @GetMapping("/all-users-contact/username")
    public List<CustomPartyService.UsersEmail> getUserContactOfSameOrganization(@RequestParam String username) {
        return customPartyService.getUserContactOfSameOrganization(username);
    }
    @GetMapping("/all-party-enable-users")
    public List<PartyTypeUser> getPartyTypeUserByAccEnable(@RequestParam Boolean enable) {
        return partyTypeUserService.findAllByAccountEnable(enable);
    }

    @GetMapping("/party/username")
    public Party getPartyByUsername(@RequestParam String username) {
        PartyTypeUser partyTypeUser = partyTypeUserService.findByUsername(username);
            return partyService.findByPartyTypeUser(partyTypeUser);
    }

    @GetMapping("/party/{id}")
    public Party getPartyNameFromParty(@PathVariable Long id) {
        return partyService.findById(id).orElse(null);
    }

    @PostMapping("/party/organization")
    public Party savePartyWithTypeOrganization(@Valid @RequestBody Party party) {
        return customPartyService.addPartyWithTypeOrganization(party);
    }

    @PutMapping("/party/organization")
    public Party updatePartyWithTypeOrganization(@Valid @RequestBody Party party) {
        return customPartyService.editPartyWithTypeOrganization(party);
    }

    @GetMapping("/party/organization")
    public List<CustomPartyService.PartyView> getPartyTypeOrganizations() {
        return customPartyService.getPartyTypeOrganizations();
    }

    @GetMapping("/party/organization/{id}")
    public CustomPartyService.PartyView getPartyTypeOrganization(@PathVariable Long id) {
        return customPartyService.getPartyView(id);
    }

    @GetMapping("/party/organization/NoService")
    public List<CustomPartyService.PartyView> getPartyTypeOrganizationsWithNoService() {
        return customPartyService.getPartyTypeOrganizationsWithNoService();
    }

    @GetMapping("/party/organization/ff")
    public List<Party> getPartyTypeOrganizationsWithServiceTypeFF() {
        return customPartyService.getPartyWithServiceType(ServiceType.FF);
    }

    @GetMapping("/party/organization/sup")
    public List<Party> getPartyTypeOrganizationsWithServiceTypeSUP() {
        return customPartyService.getPartyWithServiceType(ServiceType.SUP);
    }

    @GetMapping("/party/organization/cha")
    public List<Party> getPartyTypeOrganizationsWithServiceTypeCHA() {
        return customPartyService.getPartyWithServiceType(ServiceType.CHA);
    }

    @GetMapping("/party/organization/cbw")
    public List<Party> getPartyTypeOrganizationsWithServiceTypeCBW() {
        return customPartyService.getPartyWithServiceType(ServiceType.CBW);
    }

    @GetMapping("/party/organization/lp")
    public List<Party> getPartyTypeOrganizationsWithServiceTypeLP() {
        return customPartyService.getPartyWithServiceType(ServiceType.LP);
    }

    @PostMapping("/partyRelationship")
    public PartyRelationship savePartyRelationship(@Valid @RequestBody PartyRelationship partyRelationship) {
        return customPartyService.savePartyRelationship(partyRelationship);
    }

    @PutMapping("/partyRelationship")
    public PartyRelationship updatePartyRelationship(@Valid @RequestBody PartyRelationship partyRelationship) {
        return partyRelationshipService.save(partyRelationship);
    }

    @GetMapping("/partyTypeUserByUsername")
    public PartyTypeUser getPartyTypeUser(@RequestParam String username) {
        return partyTypeUserService.findByUsername(username);
    }

    @GetMapping("/orgnization-by-service")
    public List getPartyTypeUser() {
        return customPartyService.getOrgnizationNameByService();
    }

    @PostMapping("/changeUserPassword")
    public PasswordChangeView changeUserPassword(@Valid @RequestBody PasswordChangeView view) {
        return customPartyService.changePassword(view);
    }

    @PostMapping("/forgetUsernamePassword")
    public ForgetUsernamePassword forgetUsernamePassword(@Valid @RequestBody ForgetUsernamePassword view) {
        return customPartyService.recoverForgetUsernamePassword(view);
    }

    @GetMapping("/forgetUsernamePassword/{partyId}")
    public ForgetUsernamePassword forgetUsernamePassword(@PathVariable Long partyId) {
        ForgetUsernamePassword view = new ForgetUsernamePassword();
        Party party = partyService.findById(partyId).orElse(null);
        return customPartyService.foregetPasswordWithParty(party, view);
    }

    @PostMapping("/user/savePassword")
    public PasswordChangeView changePassword(Locale locale, @Valid @RequestBody PasswordChangeView passwordChangeView) {
//        PartyTypeUser user = (PartyTypeUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
//        customPartyService.changeUserPassword(user, passwordChangeView.getOldPassword());
//        return passwordChangeView;
        return customPartyService.changePassword(passwordChangeView);
    }

    @GetMapping("/shipment/users-contacts/organization-name")
    public List<CustomPartyService.UsersEmail> getContactsOfUser(@RequestParam String organizationName) {
        return customPartyService.getAllUserContactByOrganizationName(organizationName);
    }
}
