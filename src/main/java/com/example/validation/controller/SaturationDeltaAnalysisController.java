package com.example.validation.controller;

import com.example.validation.dto.FgSaturationDeltaAnalysisDTO;
import com.example.validation.service.SaturationDeltaAnalysisService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class SaturationDeltaAnalysisController {
    private final SaturationDeltaAnalysisService saturationDeltaAnalysisService;

    public SaturationDeltaAnalysisController(SaturationDeltaAnalysisService saturationDeltaAnalysisService) {
        this.saturationDeltaAnalysisService = saturationDeltaAnalysisService;
    }

    @GetMapping("/line-saturation-delta-analysis/{rscDtLinesId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<FgSaturationDeltaAnalysisDTO> getLineSaturationDeltaAnalysis(@PathVariable Long rscDtLinesId) {
        return ResponseEntity.ok().body(saturationDeltaAnalysisService.getFgLineSaturationDeltaAnalysis(rscDtLinesId));
    }

    @GetMapping("/skid-saturation-delta-analysis/{rscDtSkidId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<FgSaturationDeltaAnalysisDTO> getSkidSaturationDeltaAnalysis(@PathVariable Long rscDtSkidId) {
        return ResponseEntity.ok().body(saturationDeltaAnalysisService.getFgSkidSaturationDeltaAnalysis(rscDtSkidId));
    }
}
