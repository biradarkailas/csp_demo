package com.example.validation.controller;

import com.example.validation.dto.RscDtItemClassDTO;
import com.example.validation.service.RscDtItemClassService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtItemClassController {
    private final RscDtItemClassService rscDtItemClassService;

    public RscDtItemClassController(RscDtItemClassService rscDtItemClassService){
        this.rscDtItemClassService = rscDtItemClassService;
    }

    @GetMapping("/rsc-dt-item-class")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtItemClassDTO>> getAllRscDtItemClass() {
        return ResponseEntity.ok().body(rscDtItemClassService.findAll());
    }
}
