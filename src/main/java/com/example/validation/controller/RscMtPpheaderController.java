package com.example.validation.controller;

import com.example.validation.dto.DeltaColumnNameDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.MonthNameDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.MonthService;
import com.example.validation.service.RscMtPpheaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Set;

@RestController
@RequestMapping("/api")
public class RscMtPpheaderController {

    private final Logger log = LoggerFactory.getLogger(RscMtPpheaderController.class);

    private final RscMtPpheaderService rscMtPpheaderService;

    private final MonthService monthService;

    public RscMtPpheaderController(RscMtPpheaderService rscMtPpheaderService, MonthService monthService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.monthService = monthService;
    }

    @GetMapping("/mps-plan-header")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<List<RscMtPPheaderDTO>> getAllActivePPheader() {
        return ResponseEntity.ok().body(rscMtPpheaderService.findAllPPheaderByActive());
    }

    @GetMapping("/month-names/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<MonthDetailDTO> getAllMonthName(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(monthService.getMonthNameDetail(ppHeaderId));
    }

    @GetMapping("/mps-plan-header/mps-name/{mpsName}/mps-date/{mpsDate}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<RscMtPPheaderDTO> getPPheaderDTO(@PathVariable String mpsName, @PathVariable String mpsDate) {
        return ResponseEntity.ok().body(rscMtPpheaderService.findOneByMpsNameMpsDate(mpsName, LocalDate.parse(mpsDate)));
    }

    @PutMapping("/mps-plan-header")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<List<RscMtPPheaderDTO>> updatePPheaderDetails(@RequestBody List<RscMtPPheaderDTO> rscMtPPheaderDTOS) throws ResourceNotFoundException {
        List<RscMtPPheaderDTO> result = rscMtPpheaderService.saveAll(rscMtPPheaderDTOS);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/month-names/delta-analysis")
    @PreAuthorize("hasAnyAuthority('role_Analyst','role_FG','role_PM','role_RM')")
    public ResponseEntity<DeltaColumnNameDTO> getAllMonthName() {
        return ResponseEntity.ok().body(monthService.getDeltaAnalysisMonthNames());
    }

    @PutMapping("/mps-plan-header/update")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<RscMtPPheaderDTO> updatePPheaderDetails(@RequestBody RscMtPPheaderDTO rscMtPPheaderDTO) {
        RscMtPPheaderDTO result = rscMtPpheaderService.savePPheader(rscMtPPheaderDTO);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/all-mps-plan/{currentDate}")
    public ResponseEntity<List<RscMtPPheaderDTO>> getRscPPheaderDTOs(@PathVariable String currentDate) {
        return ResponseEntity.ok().body(rscMtPpheaderService.findAllByCurrentDate(LocalDate.parse(currentDate)));
    }

    @GetMapping("/mps-plan-header/update/mps-name/{mpsName}/mps-date/{mpsDate}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<RscMtPPheaderDTO> updateRscMtPPHeaderDTO(@PathVariable String mpsName, @PathVariable String mpsDate) {
        return ResponseEntity.ok().body(rscMtPpheaderService.updateRscMtPPHeaderDTO(mpsName, LocalDate.parse(mpsDate)));
    }

    @GetMapping("/current-mps-plan/file-upload-page/status")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Boolean checkRequiredFilesUploadStatus() {
        return rscMtPpheaderService.checkRequiredFilesUploadStatus();
    }

    @GetMapping("/current-month-mps-plan-header")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<List<RscMtPPheaderDTO>> getAllActivePPheaderOfCurrentMonth() {
        return ResponseEntity.ok().body(rscMtPpheaderService.findAllCurrentMonthsPPHeaderByActive());
    }

    @GetMapping("/all-mps-plan-header")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<List<RscMtPPheaderDTO>> getAllPPheader() {
        return ResponseEntity.ok().body(rscMtPpheaderService.findAllPPheader());
    }

    @GetMapping("/all-month-year")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<Set<String>> getAllMonthYear() {
        return ResponseEntity.ok().body(rscMtPpheaderService.getAllMonthYear());
    }

    @GetMapping("/all-month-year/backup")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<Set<MonthNameDTO>> getAllMonthYearList() {
        return ResponseEntity.ok().body(rscMtPpheaderService.getAllMonthYearList());
    }

    @GetMapping("/update-validated-plans/to-Pseudo/{mpsName}/mps-date/{mpsDate}/tag-name/{tagName}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<List<RscMtPPheaderDTO>> updateMultipleValidatedPlansToPseudo(@PathVariable String mpsName, @PathVariable String mpsDate, @PathVariable String tagName) {
        return ResponseEntity.ok().body(rscMtPpheaderService.updateMultipleValidatedPlansToPseudo(mpsName, LocalDate.parse(mpsDate), tagName));
    }

    @GetMapping("/get-all-mps-plan/without-backup")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<List<RscMtPPheaderDTO>> getAllMpsPlansWithBackupStatusFalse() {
        return ResponseEntity.ok().body(rscMtPpheaderService.getAllMpsPlansWithBackupStatusFalse());
    }

    @PutMapping("/update-mps-details/plan-management/{tagChangeStatus}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public ResponseEntity<RscMtPPheaderDTO> updateMpsDetailsPlanManagement(@PathVariable Boolean tagChangeStatus, @RequestBody RscMtPPheaderDTO rscMtPPheaderDTOs) throws ResourceNotFoundException {
        return ResponseEntity.ok().body(rscMtPpheaderService.updateMpsDetailsPlanManagement(tagChangeStatus, rscMtPPheaderDTOs));
    }

    @GetMapping("/mps-plan-name/exist/except-active-mps-plan/{name}/{id}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Boolean mpsPlanNameExistsForPlanManagement(@PathVariable String name, @PathVariable Long id) {
        return rscMtPpheaderService.mpsPlanNameExistsForPlanManagement(name, id);
    }

    @GetMapping("/mps-plan-header/with-latest-backup-date")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<RscMtPPheaderDTO> getPPheaderWithLatestBackupDate() {
        return ResponseEntity.ok().body(rscMtPpheaderService.getPPheaderWithLatestBackupDate());
    }
}