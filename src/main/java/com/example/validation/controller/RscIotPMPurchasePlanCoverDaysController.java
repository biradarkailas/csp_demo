package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanCoverDaysDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotPMPurchasePlanCoverDaysService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotPMPurchasePlanCoverDaysController {

    private final RscIotPMPurchasePlanCoverDaysService rscIotPMPurchasePlanCoverDaysService;
    private final ExecutionErrorLogService executionErrorLogService;


    public RscIotPMPurchasePlanCoverDaysController(RscIotPMPurchasePlanCoverDaysService rscIotPMPurchasePlanCoverDaysService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotPMPurchasePlanCoverDaysService = rscIotPMPurchasePlanCoverDaysService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/purchase-plan/cover-days/pm/create/{ppheaderId}")
    public ExecutionResponseDTO calculatePMPurchasePlanCoverDays(@PathVariable Long ppheaderId) {
        try {
            rscIotPMPurchasePlanCoverDaysService.calculatePMPurchasePlanCoverDays(ppheaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Purchase Plan Cover Days", "PM Purchase Plan Cover Days Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("calculatePMPurchasePlanCoverDays", e.getMessage(), ppheaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Purchase Plan Cover Days", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-purchase-plan-cover-days/{ppheaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public List<RscIotPMPurchasePlanCoverDaysDTO> getAllPMPurchasePlanCoverDays(@PathVariable Long ppheaderId) {
        return rscIotPMPurchasePlanCoverDaysService.getAllPMPurchasePlanCoverDays(ppheaderId);
    }

    @GetMapping("/pm-purchase-plan-cover-days/stock-equation/{ppheaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public List<PurchasePlanCoverDaysEquationDTO> getPurchasePlanCoverDaysEquationDTOs(@PathVariable Long ppheaderId) {
        return rscIotPMPurchasePlanCoverDaysService.getPurchasePlanCoverDaysEquationDTOs(ppheaderId);
    }
}