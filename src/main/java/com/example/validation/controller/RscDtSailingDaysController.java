package com.example.validation.controller;

import com.example.validation.dto.RscDtSailingDaysDTO;
import com.example.validation.service.RscDtSailingDaysService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtSailingDaysController {
    private final RscDtSailingDaysService rscDtSailingDaysService;

    public RscDtSailingDaysController(RscDtSailingDaysService rscDtSailingDaysService) {
        this.rscDtSailingDaysService = rscDtSailingDaysService;
    }

    @GetMapping("/rsc-dt-sailing-days")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtSailingDaysDTO>> getAllRscDtSailingDays() {
        return ResponseEntity.ok().body(rscDtSailingDaysService.findAll());
    }
}
