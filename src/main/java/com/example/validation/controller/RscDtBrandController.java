package com.example.validation.controller;

import com.example.validation.dto.RscDtBrandDTO;
import com.example.validation.service.RscDtBrandService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtBrandController {
    private final RscDtBrandService rscDtBrandService;

    public RscDtBrandController(RscDtBrandService rscDtBrandService) {
        this.rscDtBrandService = rscDtBrandService;
    }

    @GetMapping("/rsc-dt-brand")
    public ResponseEntity<List<RscDtBrandDTO>> getAllRscDtBrand() {
        return ResponseEntity.ok().body(rscDtBrandService.findAll());
    }
}
