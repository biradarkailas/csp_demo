package com.example.validation.controller;

import com.example.validation.dto.RscDtStockTypeDTO;
import com.example.validation.service.RscDtStockTypeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtStockTypeController {
    private final RscDtStockTypeService rscDtStockTypeService;

    public RscDtStockTypeController(RscDtStockTypeService rscDtStockTypeService){
        this.rscDtStockTypeService = rscDtStockTypeService;
    }

    @GetMapping("/rsc-dt-stock-type")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtStockTypeDTO>> getAllRscDtStockType() {
        return ResponseEntity.ok().body(rscDtStockTypeService.findAll());
    }
}
