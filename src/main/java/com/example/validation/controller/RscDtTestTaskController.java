package com.example.validation.controller;

import com.example.validation.dto.RscDtTestTaskDTO;
import com.example.validation.service.RscDtTestTaskService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtTestTaskController {
    private final RscDtTestTaskService rscDtTestTaskService;

    public RscDtTestTaskController(RscDtTestTaskService rscDtTestTaskService){
        this.rscDtTestTaskService = rscDtTestTaskService;
    }

    @GetMapping("/rsc-dt-test-task")
    public ResponseEntity<List<RscDtTestTaskDTO>> getAllRscDtTestTask(){
        return ResponseEntity.ok().body(rscDtTestTaskService.getAllRscDtTestTask());
    }
}
