package com.example.validation.controller;

import com.example.validation.service.AllExcelExportService;
import com.example.validation.service.AllPMExcelExportService;
import com.example.validation.service.AllRMExcelExportService;
import com.example.validation.service.MPSTriangleAnalysisExcelExportService;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLConnection;

@RestController
@RequestMapping("/api")
public class AllExcelExportController {
    private final AllExcelExportService allExcelExportService;
    private final AllPMExcelExportService allPMExcelExportService;
    private final AllRMExcelExportService allRMExcelExportService;
    private final MPSTriangleAnalysisExcelExportService mpsTriangleAnalysisExcelExportService;

    public AllExcelExportController(AllExcelExportService allExcelExportService, AllPMExcelExportService allPMExcelExportService, AllRMExcelExportService allRMExcelExportService, MPSTriangleAnalysisExcelExportService mpsTriangleAnalysisExcelExportService) {
        this.allExcelExportService = allExcelExportService;
        this.allPMExcelExportService = allPMExcelExportService;
        this.allRMExcelExportService = allRMExcelExportService;
        this.mpsTriangleAnalysisExcelExportService = mpsTriangleAnalysisExcelExportService;
    }

    @GetMapping("/all/excel/export/mpsAndFg/{ppHeaderId}")
    public String getAllExports(@PathVariable Long ppHeaderId) {
        return allExcelExportService.createAllMpsFgExports( ppHeaderId );
    }

    @GetMapping("/all/excel/export/pm/{ppHeaderId}")
    public String getAllPMExports(@PathVariable Long ppHeaderId) {
        return allPMExcelExportService.createAllPMExports( ppHeaderId );
    }

    @GetMapping("/all/excel/export/rm/{ppHeaderId}")
    public String getAllRMExports(@PathVariable Long ppHeaderId) {
        return allRMExcelExportService.createAllRMExports( ppHeaderId );
    }

    @GetMapping("/all/excel/export/mpsTriangleAnalysisExcel/{ppHeaderId}")
    public String getAllMPSTriangleAnalysisExcelExports(@PathVariable Long ppHeaderId) {
        return mpsTriangleAnalysisExcelExportService.createAllMPSTriangleAnalysisExcelExports( ppHeaderId );
    }

    @GetMapping("/download/{ppHeaderId}")
    public void downloadFile(HttpServletResponse response, @PathVariable Long ppHeaderId) throws IOException {
        File file = new File( "D:/PM Logical Consumption 2019-05-01.xlsx" );
        if (file.exists()) {
            String mimeType = URLConnection.guessContentTypeFromName( file.getName() );
            if (mimeType == null) {
                mimeType = "application/octet-stream";
            }
            response.setContentType( mimeType );
            response.setHeader( "Content-Disposition", String.format( "inline; filename=\"" + file.getName() + "\"" ) );
            response.setContentLength( (int) file.length() );
            InputStream inputStream = new BufferedInputStream( new FileInputStream( file ) );
            FileCopyUtils.copy( inputStream, response.getOutputStream() );
        }
    }
}