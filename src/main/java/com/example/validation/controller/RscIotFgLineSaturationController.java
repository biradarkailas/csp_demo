package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotFgLineSaturationService;
import com.example.validation.service.RscIotFgLineSaturationSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotFgLineSaturationController {
    private final RscIotFgLineSaturationService rscIotFgLineSaturationService;
    private final RscIotFgLineSaturationSummaryService rscIotFgLineSaturationSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotFgLineSaturationController(RscIotFgLineSaturationService rscIotFgLineSaturationService, RscIotFgLineSaturationSummaryService rscIotFgLineSaturationSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotFgLineSaturationService = rscIotFgLineSaturationService;
        this.rscIotFgLineSaturationSummaryService = rscIotFgLineSaturationSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/fg-line-saturation/create/{ppHeaderId}")
    public ExecutionResponseDTO createFgLineSaturation(@PathVariable Long ppHeaderId) {
        try {
            rscIotFgLineSaturationService.createFgLineSaturation(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Line Saturation", "FG Line Saturation Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFgLineSaturation", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Line Saturation", e.getMessage(), false);
        }
    }

    @GetMapping("/fg-line-saturation/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<FgLineSaturationDetailsDTO> getFgLineSaturationDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgLineSaturationService.getAllFgLineSaturation(ppHeaderId));
    }

    @GetMapping("/fg-line-saturation/all-lines/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<LinesDetailsDTO> getAllLinesDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgLineSaturationService.getAllFgLines(ppHeaderId));
    }

    @GetMapping("/fg-line-saturation/lines-details/{ppHeaderId}/{rscDtLineId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<FgLineDetailsDTO> getLinesDetails(@PathVariable Long ppHeaderId, @PathVariable Long rscDtLineId) {
        return ResponseEntity.ok().body(rscIotFgLineSaturationService.getFgLinesDetails(ppHeaderId, rscDtLineId));
    }

    @GetMapping("/fg-line-saturation-summary/create/{ppHeaderId}")
    public ExecutionResponseDTO createFgLineSaturationSummary(@PathVariable Long ppHeaderId) {
        try {
            rscIotFgLineSaturationSummaryService.createFgLineSaturationSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Line Saturation Summary", "FG Line Saturation Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFgLineSaturationSummary", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Line Saturation Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/fg-line-saturation-summary/range-wise-lines-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<FgLineSaturationLinesDetailsDTO> getFgLineSaturationsDetailsCalculations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgLineSaturationSummaryService.getFgLineSaturationsLines(ppHeaderId));
    }

    @GetMapping("/fg-line-saturation-summary/lines-list/{ppHeaderId}")
    public ResponseEntity<List<RscIotFgLineSaturationSummaryDTO>> getFgLineSaturations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgLineSaturationSummaryService.getFgLineSaturationsList(ppHeaderId));
    }

    @GetMapping("/fg-line-saturation-summary/lines-details/{ppHeaderId}/{rscDtLinesId}")
    public ResponseEntity<FgLineSaturationLineWiseDetailsDTO> getFgLineSaturationsDetailsByLineId(@PathVariable Long ppHeaderId, @PathVariable Long rscDtLinesId) {
        return ResponseEntity.ok().body(rscIotFgLineSaturationSummaryService.getSingleLinesDetails(ppHeaderId, rscDtLinesId));
    }

    @GetMapping("/all-rsc-dt-lines/present-in-saturation")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<List<LinesDTO>> getAllRscDtLines() {
        return ResponseEntity.ok().body(rscIotFgLineSaturationService.getAllLines());
    }
}
