package com.example.validation.controller;

import com.example.validation.dto.MaterialMasterDTO;
import com.example.validation.service.MaterialMastersService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MaterialMasterController {
    private final MaterialMastersService materialMastersService;

    public MaterialMasterController(MaterialMastersService materialMastersService) {
        this.materialMastersService = materialMastersService;
    }

    @GetMapping("/material-masters/packing-material-list")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public List<MaterialMasterDTO> getMaterialMastersListOfPackingMaterial() {
        return materialMastersService.getMaterialMastersListOfPackingMaterial();
    }

    @GetMapping("/material-masters/packing-material-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<MaterialMasterDTO>> findPackingMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( materialMastersService.findMaterialMastersListOfPackingMaterialWithPagination( pageNo, pageSize ) );
    }

    @GetMapping("/material-masters/raw-material-list")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public List<MaterialMasterDTO> getMaterialMastersListOfRawMaterial() {
        return materialMastersService.getMaterialMastersListOfRawMaterial();
    }

    @GetMapping("/material-masters/raw-material-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<MaterialMasterDTO>> findRawMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( materialMastersService.findMaterialMastersListOfRawMaterialWithPagination( pageNo, pageSize ) );
    }

    @GetMapping("/material-masters/fg-material-list")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public List<MaterialMasterDTO> getMaterialMastersListOfFgMaterial() {
        return materialMastersService.getMaterialMastersListOfFgMaterial();
    }

    @GetMapping("/material-masters/fg-material-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<List<MaterialMasterDTO>> findFgMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( materialMastersService.findMaterialMastersListOfFgMaterialWithPagination( pageNo, pageSize ) );
    }

    @GetMapping("/material-masters/bulk-material-list")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public List<MaterialMasterDTO> getMaterialMastersListOfBulkMaterial() {
        return materialMastersService.getMaterialMastersListOfBulkMaterial();
    }

    @GetMapping("/material-masters/bulk-material-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<MaterialMasterDTO>> findBulkMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( materialMastersService.findMaterialMastersListOfBulkMaterialWithPagination( pageNo, pageSize ) );
    }

    @GetMapping("/material-masters/subBases-material-list")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public List<MaterialMasterDTO> getMaterialMastersListOfSubBasesMaterial() {
        return materialMastersService.getMaterialMastersListOfSubBasesMaterial();
    }

    @GetMapping("/material-masters/subBases-material-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<MaterialMasterDTO>> findSubBasesMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( materialMastersService.findMaterialMastersListOfSubBasesMaterialWithPagination( pageNo, pageSize ) );
    }

    @GetMapping("/material-masters/wip-material-list")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public List<MaterialMasterDTO> getMaterialMastersListOfWipMaterial() {
        return materialMastersService.getMaterialMastersListOfWipMaterial();
    }

    @GetMapping("/material-masters/wip-material-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<List<MaterialMasterDTO>> findWipMaterialWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( materialMastersService.findMaterialMastersListOfWipMaterialWithPagination( pageNo, pageSize ) );
    }
}
