package com.example.validation.controller;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.AllBackUpExportFileListsDTO;
import com.example.validation.dto.AnalyzerAllExportReportsDTO;
import com.example.validation.dto.AnalyzerExportFileDTO;
import com.example.validation.service.AnalyzerAllExportReportsService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class AnalyzerAllExportReportsController {
    private final AnalyzerAllExportReportsService analyzerAllExportReportsService;

    public AnalyzerAllExportReportsController(AnalyzerAllExportReportsService analyzerAllExportReportsService) {
        this.analyzerAllExportReportsService = analyzerAllExportReportsService;
    }

    @GetMapping("/all/excel/export/filenames/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<AnalyzerAllExportReportsDTO> getAllExportFileNames(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(analyzerAllExportReportsService.getAllExportFileNames(ppHeaderId));
    }

    @GetMapping("/download/files/{ppHeaderId}/{fgCheckedList}/{pmCheckedList}/{pmOtifCheckedList}/{rmCheckedList}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<InputStreamResource> downloadfile(@PathVariable Long ppHeaderId, @PathVariable String fgCheckedList, @PathVariable String pmCheckedList, @PathVariable String pmOtifCheckedList, @PathVariable String rmCheckedList) {
        AnalyzerExportFileDTO file = analyzerAllExportReportsService.downloadfile(ppHeaderId, fgCheckedList, pmCheckedList, pmOtifCheckedList, rmCheckedList);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + file.getExcelExportFiles().getFileName()).contentType(MediaType.parseMediaType(file.getMediaType())).body(new InputStreamResource(file.getExcelExportFiles().getFile()));
    }

    @GetMapping("/all/backup/export/filenames/{monthYearValue}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<AllBackUpExportFileListsDTO> getAllBackUpExportFileNames(@PathVariable String monthYearValue) {
        return ResponseEntity.ok().body(analyzerAllExportReportsService.getAllBackUpExportFileNames(monthYearValue));
    }

    @GetMapping("/download/backup-files/zip/{monthYearValue}/{firstCutCheckedList}/{validatedCheckedList}/{pmOtifCheckedList}/{allModuleCheckedList}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst')")
    public ResponseEntity<InputStreamResource> downloadBackupZipFiles(@PathVariable String monthYearValue, @PathVariable String firstCutCheckedList, @PathVariable String validatedCheckedList, @PathVariable String pmOtifCheckedList, @PathVariable String allModuleCheckedList) {
        AnalyzerExportFileDTO file = analyzerAllExportReportsService.downloadBackupZipFiles(monthYearValue, firstCutCheckedList, validatedCheckedList, pmOtifCheckedList, allModuleCheckedList);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + file.getExcelExportFiles().getFileName()).contentType(MediaType.parseMediaType(file.getMediaType())).body(new InputStreamResource(file.getExcelExportFiles().getFile()));
    }
}
