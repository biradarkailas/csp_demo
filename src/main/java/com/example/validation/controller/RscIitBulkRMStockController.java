package com.example.validation.controller;

import com.example.validation.dto.RscIitBulkRMStockDTO;
import com.example.validation.service.RscIitBulkRMStockService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIitBulkRMStockController {
    private final RscIitBulkRMStockService rscIitBulkRMStockService;

    public RscIitBulkRMStockController(RscIitBulkRMStockService rscIitBulkRMStockService) {
        this.rscIitBulkRMStockService = rscIitBulkRMStockService;
    }

    @GetMapping("/bulk-rm-stock/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RscIitBulkRMStockDTO>> getAllRscIitBulkRMStockDTO(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body( rscIitBulkRMStockService.getAllRscIitBulkRMStockDTOs( ppHeaderId ) );
    }
}