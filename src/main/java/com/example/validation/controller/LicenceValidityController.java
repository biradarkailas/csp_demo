package com.example.validation.controller;

import com.example.validation.service.impl.CustomLicenceValidityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;

@RestController
public class LicenceValidityController {
    @Autowired
    CustomLicenceValidityService customLicenceValidityService;

    @PostMapping("/upload-licence-file")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
    public Integer uploadLicenceFile(@RequestParam("file") MultipartFile files, HttpSession session) {
        return customLicenceValidityService.uploadLicence(files, session);
    }

    @GetMapping("/check-valid-licence")
    @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM','role_Analyst','role_Admin')")
    public Integer checkValidLicence(@RequestParam StringBuilder subString) {
        return customLicenceValidityService.hasValidLicence(subString);
    }
}
