package com.example.validation.controller;

import com.example.validation.dto.RscDtUomDTO;
import com.example.validation.service.RscDtUomService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtUomController {
    private final RscDtUomService rscDtUomService;

    public RscDtUomController(RscDtUomService rscDtUomService){
        this.rscDtUomService = rscDtUomService;
    }

    @GetMapping("/rsc-dt-uom")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtUomDTO>> getAllRscDtUom() {
        return ResponseEntity.ok().body(rscDtUomService.findAll());
    }
}
