package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.supply.RMPurchasePlanMainDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotRMPurchasePlanService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotRMPurchasePlanController {
    private final RscIotRMPurchasePlanService rscIotRMPurchasePlanService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotRMPurchasePlanController(RscIotRMPurchasePlanService rscIotRMPurchasePlanService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotRMPurchasePlanService = rscIotRMPurchasePlanService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/rm-purchase-plan/create/{ppHeaderId}")
    public ExecutionResponseDTO createRMSupply(@PathVariable Long ppHeaderId) {
        try {
            rscIotRMPurchasePlanService.createRMPurchasePlan(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Purchase Plan", "RM Purchase Plan Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMSupply", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Purchase Plan", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-purchase-plan/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RMPurchasePlanMainDTO> getRMPurchasePlan(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMPurchasePlanService.getRMPurchasePlan(ppHeaderId));
    }
}