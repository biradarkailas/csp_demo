package com.example.validation.controller;

import com.example.validation.dto.ErrorLogResponseDTO;
import com.example.validation.dto.ExecutionResponseMainDTO;
import com.example.validation.service.ErrorLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class ErrorLogController {

  private final Logger log = LoggerFactory.getLogger(ErrorLogController.class);

  private final ErrorLogService errorLogService;

  public ErrorLogController(ErrorLogService errorLogService) {
    this.errorLogService = errorLogService;
  }

  @GetMapping("/error-log")
  @PreAuthorize("hasAnyAuthority('role_FG','role_PM','role_RM')")
  public List<ErrorLogResponseDTO> rsyncConnection() {
    return errorLogService.findAllErrorLogResponseDTO();
  }

  @PostMapping("/create-error-excel")
  public String createExecutionResponseFile(
      @RequestBody ExecutionResponseMainDTO executionResponseMainDTO) {
    errorLogService.createExecutionResponseFile(executionResponseMainDTO);
    return "Execution Error Excel File Created";
  }
}
