package com.example.validation.controller;

import com.example.validation.dto.ClassWiseCoverDaysDetailsDTO;
import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotRmCoverDaysClassSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotRmCoverDaysClassSummaryController {
    private final RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotRmCoverDaysClassSummaryController(RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotRmCoverDaysClassSummaryService = rscIotRmCoverDaysClassSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/rm-cover-days-class-summary/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createRMCoverDaysClassSummaryCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotRmCoverDaysClassSummaryService.calculateRmCoverDaysAllClassSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Cover Days Class Summary", "RM Cover Days Class Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMCoverDaysClassSummaryCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Cover Days Class Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-cover-days-class-wise-summary-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<ClassWiseCoverDaysDetailsDTO> getRMCoverDaysClassWiseSummaryDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRmCoverDaysClassSummaryService.getRmCoverDaysClassWiseSummaryDetails(ppHeaderId));
    }
}
