package com.example.validation.controller;

import com.example.validation.dto.ConsumptionPivotMainDTO;
import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.FGDetailsDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscItRMPivotConsumptionService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscItRMPivotConsumptionController {
    private final RscItRMPivotConsumptionService rscItRMPivotConsumptionService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscItRMPivotConsumptionController(RscItRMPivotConsumptionService rscItRMPivotConsumptionService, ExecutionErrorLogService executionErrorLogService) {
        this.rscItRMPivotConsumptionService = rscItRMPivotConsumptionService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/rm-pivot-consumption/create/{ppHeaderId}")
    public ExecutionResponseDTO createRMPivotConsumption(@PathVariable Long ppHeaderId) {
        try {
            rscItRMPivotConsumptionService.createRMPivotConsumption(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Pivot Consumption", "RM Pivot Consumption Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMPivotConsumption", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Pivot Consumption", e.getMessage(), false);
        }
    }

    @GetMapping("/consumption/pivot/rm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<ConsumptionPivotMainDTO> getRMPivotConsumption(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItRMPivotConsumptionService.getRMPivotConsumption(ppHeaderId));
    }

    @GetMapping("/consumption/pivot/rm/{ppHeaderId}/{itemId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<FGDetailsDTO>> getFGDetails(@PathVariable Long ppHeaderId, @PathVariable Long itemId) {
        return ResponseEntity.ok().body(rscItRMPivotConsumptionService.getFGDetails(ppHeaderId, itemId));
    }
}