package com.example.validation.controller;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.service.ExcelExportService;
import com.example.validation.util.DatabaseUtil;
import com.example.validation.util.ZipDirectoryUtil;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;

@RestController
@RequestMapping("/api")
public class ExcelExportController {

    private final ExcelExportService excelExportService;
    private final ZipDirectoryUtil zipDirectoryUtil;

    public ExcelExportController(ExcelExportService excelExportService, ZipDirectoryUtil zipDirectoryUtil) {
        this.excelExportService = excelExportService;
        this.zipDirectoryUtil = zipDirectoryUtil;
    }

    @GetMapping("/fg/export/{ppHeaderId}/{fileName}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<InputStreamResource> downloadfgExportFile(@PathVariable Long ppHeaderId, @PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadfgExport(ppHeaderId, fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    @GetMapping("/pm/export/{ppHeaderId}/{fileName}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<InputStreamResource> downloadPmExportFile(@PathVariable Long ppHeaderId, @PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadPmExport(ppHeaderId, fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    @GetMapping("/pmOtif/export/{ppHeaderId}/{fileName}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<InputStreamResource> downloadPmOtifExportFile(@PathVariable Long ppHeaderId, @PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadPmOtifExport(ppHeaderId, fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    @GetMapping("/rm/export/{ppHeaderId}/{fileName}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<InputStreamResource> downloadRmExportFile(@PathVariable Long ppHeaderId, @PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadRmExport(ppHeaderId, fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    //Annual excel download
    @GetMapping("/rmAnnualExport/export/{fileName}")
    /*  @PreAuthorize("hasAnyAuthority('role_RM')")*/ public ResponseEntity<InputStreamResource> downloadRmAnnualExport(@PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadRmAnnualExport(fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    @GetMapping("/pmAnnualExport/export/{fileName}")
    /* @PreAuthorize("hasAnyAuthority('role_PM')")*/ public ResponseEntity<InputStreamResource> downloadPmAnnualExport(@PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadPmAnnualExport(fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    @GetMapping("/fgAnnualExport/export/{fileName}")
    /* @PreAuthorize("hasAnyAuthority('role_FG')")*/ public ResponseEntity<InputStreamResource> downloadFgAnnualExport(@PathVariable String fileName) {
        ExcelExportFiles excelExportFiles = excelExportService.downloadFgAnnualExport(fileName);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, ExcelFileNameConstants.Attachment_File + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType(ExcelFileNameConstants.Application_Excel)).body(new InputStreamResource(excelExportFiles.getFile()));
    }

    @GetMapping("/all/zip")
    public String createZip() {
        try {
            zipDirectoryUtil.createZip();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "ZIP created";
    }

    @GetMapping("/db-backup")
    public void dbBackup() {
        DatabaseUtil.backup();
    }
}
