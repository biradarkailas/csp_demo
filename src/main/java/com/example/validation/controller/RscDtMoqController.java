package com.example.validation.controller;

import com.example.validation.dto.RscDtMoqDTO;
import com.example.validation.service.RscDtMoqService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtMoqController {
    private final RscDtMoqService rscDtMoqService;

    public RscDtMoqController(RscDtMoqService rscDtMoqService){
        this.rscDtMoqService = rscDtMoqService;
    }

    @GetMapping("/rsc-dt-moq")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtMoqDTO>> getAllRscDtMoq() {
        return ResponseEntity.ok().body(rscDtMoqService.findAll());
    }
}
