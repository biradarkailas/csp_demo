package com.example.validation.controller;

import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.service.SupplierMastersService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class SupplierMastersController {
    private final SupplierMastersService supplierMastersService;

    public SupplierMastersController(SupplierMastersService supplierMastersService) {
        this.supplierMastersService = supplierMastersService;
    }

    @GetMapping("/suppliers-masters/packing-materials-suppliers-list")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public List<RscDtSupplierDTO> getSuppliersListOfPackingMaterial() {
        return supplierMastersService.getSuppliersListOfPackingMaterial();
    }

    @GetMapping("/suppliers-masters/packing-materials-suppliers-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<RscDtSupplierDTO>> findPackingMaterialsSuppliersListWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( supplierMastersService.findSuppliersListOfPackingMaterialWithPagination( pageNo, pageSize ) );
    }

    @GetMapping("/suppliers-masters/raw-materials-suppliers-list")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public List<RscDtSupplierDTO> getSuppliersListOfRawMaterial() {
        return supplierMastersService.getSuppliersListOfRawMaterial();
    }

    @GetMapping("/suppliers-masters/raw-materials-suppliers-list-pagination/{pageNo}/{pageSize}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RscDtSupplierDTO>> findRawMaterialsSuppliersListWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( supplierMastersService.findSuppliersListOfRawMaterialWithPagination( pageNo, pageSize ) );
    }

}
