package com.example.validation.controller;

import com.example.validation.dto.RscDtReasonDTO;
import com.example.validation.service.RscDtReasonService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtReasonController {
    private final RscDtReasonService rscDtReasonService;

    public RscDtReasonController(RscDtReasonService rscDtReasonService) {
        this.rscDtReasonService = rscDtReasonService;
    }

    @GetMapping("/rsc-dt-reason")
    @PreAuthorize("hasAnyAuthority('role_Admin','role_PM','role_RM')")
    public ResponseEntity<List<RscDtReasonDTO>> getAllRscDtReason() {
        return ResponseEntity.ok().body(rscDtReasonService.findAll());
    }

    @PutMapping("/rsc-dt-reason")
    public ResponseEntity<List<RscDtReasonDTO>> updateRscIotPMLatestPurchasePlan(@RequestBody List<RscDtReasonDTO> rscDtReasonDTOs) {
        List<RscDtReasonDTO> result = rscDtReasonService.save(rscDtReasonDTOs);
        return ResponseEntity.ok().body(result);
    }
}
