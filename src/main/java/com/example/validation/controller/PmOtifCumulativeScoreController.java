package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.OtifCumulativeScoreAnalysisDTO;
import com.example.validation.dto.OtifCumulativeSuppliersDetailsDTO;
import com.example.validation.service.PmOtifCumulativeScoreService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class PmOtifCumulativeScoreController {
    private final PmOtifCumulativeScoreService pmOtifCumulativeScoreService;

    public PmOtifCumulativeScoreController(PmOtifCumulativeScoreService pmOtifCumulativeScoreService) {
        this.pmOtifCumulativeScoreService = pmOtifCumulativeScoreService;
    }

    @GetMapping("/otif-cumulative/create")
    public ExecutionResponseDTO calculateOtifCumulativeScore() {
        try {
            pmOtifCumulativeScoreService.calculateOtifCumulativeScore();
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Otif Cumulative Score", "PM Otif Cumulative Score Calculated Successfully", true);
        } catch (Exception e) {
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Otif Cumulative Score", e.getMessage(), false);
        }
    }

    @GetMapping("/otif-cumulative/score-wise/suppliersList")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public OtifCumulativeSuppliersDetailsDTO getSuppliersListByScore() {
        return pmOtifCumulativeScoreService.getSuppliersListByScore();
    }

    @GetMapping("/otif-cumulative-score/suppliers-details/{supplierId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public OtifCumulativeScoreAnalysisDTO getSuppliersDetails(@PathVariable Long supplierId) {
        return pmOtifCumulativeScoreService.getSuppliersDetails(supplierId);
    }


}
