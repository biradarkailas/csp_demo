package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotRMSlobService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotRmSlobController {
    private final RscIotRMSlobService rscIotRMSlobService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotRmSlobController(RscIotRMSlobService rscIotRMSlobService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/rm-slob/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createRMSlobCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotRMSlobService.calculateRscItRMSlob(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Slob", "RM Slob Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMSlobCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Slob", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-slob-summary/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createRMSlobSummaryCalculations(@PathVariable Long ppHeaderId, @RequestParam(required = false) Double sitValue) {
        try {
            rscIotRMSlobService.calculateRscItRMSlobSummary(ppHeaderId, sitValue);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Slob Summary", "RM Slob Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createRMSlobSummaryCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("RM Slob Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/rm-slob/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RMSlobDetailsDTO> getRMSlobCalculations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getAllRscIotRmSlob(ppHeaderId));
    }

    @GetMapping("/rm-slob/total-consumption/{ppHeaderId}/{rscMtItemId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<SlobTotalConsumptionDetailsDTO> getTotalConsumptionDetails(@PathVariable Long ppHeaderId, @PathVariable Long rscMtItemId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getTotalConsumption(ppHeaderId, rscMtItemId));
    }

    @GetMapping("/rm-slob-summary-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<SlobSummaryDetailsDTO> getRmSlobSummaryDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getRmSlobSummaryDetails(ppHeaderId));
    }

    @GetMapping("/rm-slob-nullMapPrice-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<RMSlobDetailsDTO> getRmSlobNullMapDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getNullMapPriceDetails(ppHeaderId));
    }

    @GetMapping("/rm-slob/reasons/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RscDtReasonDTO>> getAllRmSlobsReasons(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getAllRmSlobsReasons(ppHeaderId));
    }

    @GetMapping("/slob-summary/rm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<SlobSummaryDTO> getRmSlobSummarysDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getRmSlobSummary(ppHeaderId));
    }

    @GetMapping("/slob-movement/rm")
    @PreAuthorize("hasAnyAuthority('role_RM','role_Analyst')")
    public ResponseEntity<SlobMovementDetailsDTO> getRmSlobMovementDetails() {
        return ResponseEntity.ok().body(rscIotRMSlobService.getRmSlobMovement());
    }

    @GetMapping("/slob-inventory-quality-index/rm")
    @PreAuthorize("hasAnyAuthority('role_RM','role_Analyst')")
    public ResponseEntity<SlobInventoryQualityIndexDetailsDTO> getRmSlobInventoryQualityIndexDetails() {
        return ResponseEntity.ok().body(rscIotRMSlobService.getRmSlobInventoryQualityIndex());
    }

    @GetMapping("/slob-summary/dashboard/rm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM','role_Analyst')")
    public ResponseEntity<SlobSummaryDashboardDTO> getRmSlobSummaryDashboardDTO(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotRMSlobService.getRmSlobSummaryDashboardDTO(ppHeaderId));
    }

    /*@GetMapping("/rm-slob/export/{ppHeaderId}")
    public ResponseEntity<InputStreamResource> getRmSlobExcelExport(@PathVariable Long ppHeaderId) {
        ExcelExportFiles excelExportFiles = rscIotRMSlobService.rmSlobExporter(ppHeaderId);
        return ResponseEntity.ok().header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=" + excelExportFiles.getFileName()).contentType(MediaType.parseMediaType("application/vnd.ms-excel_export")).body(new InputStreamResource(excelExportFiles.getFile()));
    }*/
}