package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.supply.PmSupplyCalculationsDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotPmPurchasePlanCalculationsService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotPmSupplyCalculationsController {
    private final RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotPmSupplyCalculationsController(RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotPmPurchasePlanCalculationsService = rscIotPmPurchasePlanCalculationsService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/pm-purchase-plan-calculations/create/{ppHeaderId}")
    public ExecutionResponseDTO createPMSupplyCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscIotPmPurchasePlanCalculationsService.createPMSupplyCalculations(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Purchase Plan Calculations", "PM Purchase Plan Calculations Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMSupplyCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Purchase Plan Calculations", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-supply-calculations/{rscMtItemId}/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PmSupplyCalculationsDTO> getPMSupplyCalculations(@PathVariable Long rscMtItemId, @PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmPurchasePlanCalculationsService.getPMSupplyCalculations(rscMtItemId, ppHeaderId));
    }
}