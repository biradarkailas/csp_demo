package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotFgSkidSaturationService;
import com.example.validation.service.RscIotFgSkidSaturationSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotFgSkidSaturationController {
    private final RscIotFgSkidSaturationService rscIotFgSkidSaturationService;
    private final RscIotFgSkidSaturationSummaryService rscIotFgSkidSaturationSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotFgSkidSaturationController(RscIotFgSkidSaturationService rscIotFgSkidSaturationService, RscIotFgSkidSaturationSummaryService rscIotFgSkidSaturationSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotFgSkidSaturationService = rscIotFgSkidSaturationService;
        this.rscIotFgSkidSaturationSummaryService = rscIotFgSkidSaturationSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/fg-skid-saturation/create/{ppHeaderId}")
    public ExecutionResponseDTO createFgSkidSaturation(@PathVariable Long ppHeaderId) {
        try {
            rscIotFgSkidSaturationService.createFgSkidSaturation(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Skid Saturation", "FG Skid Saturation Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFgSkidSaturation", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Skid Saturation", e.getMessage(), false);
        }
    }

    @GetMapping("/fg-skid-saturation/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public ResponseEntity<FgSkidSaturationDetailsDTO> getFgSkidSaturationDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationService.getAllFgSkidSaturation(ppHeaderId));
    }

    @GetMapping("/fg-skid-saturation/all-skids/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<SkidsDetailsDTO> getAllFgSkidsDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationService.getAllFgSkids(ppHeaderId));
    }

    @GetMapping("/fg-skid-saturation/skids-details/{ppHeaderId}/{rscDtSkidId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<FgSkidDetailsDTO> getSkidsDetails(@PathVariable Long ppHeaderId, @PathVariable Long rscDtSkidId) {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationService.getFgSkidDetails(ppHeaderId, rscDtSkidId));
    }

    @GetMapping("/fg-skid-saturation-summary/create/{ppHeaderId}")
    public ExecutionResponseDTO createFgSkidSaturationSummary(@PathVariable Long ppHeaderId) {
        try {
            rscIotFgSkidSaturationSummaryService.createFgSkidSaturationSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Skid Saturation Summary", "FG Skid Saturation Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFgSkidSaturationSummary", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Skid Saturation Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/fg-skid-saturation-summary/range-wise-skids-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<FgSkidSaturationSkidsDetailsDTO> getFgSkidSaturationsDetailsCalculations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationSummaryService.getFgSkidSaturationsSkids(ppHeaderId));
    }

    @GetMapping("/fg-skid-saturation-summary/skids-list/{ppHeaderId}")
    public ResponseEntity<List<RscIotFgSkidSaturationSummaryDTO>> getFgSkidSaturations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationSummaryService.getFgSkidSaturationsList(ppHeaderId));
    }

    @GetMapping("/fg-skid-saturation-summary/skids-details/{ppHeaderId}/{rscDtSkidsId}")
    public ResponseEntity<FgSkidSaturationSkidWiseDetailsDTO> getFgSkidSaturationsDetailsBySkidId(@PathVariable Long ppHeaderId, @PathVariable Long rscDtSkidsId) {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationSummaryService.getSingleSkidsDetails(ppHeaderId, rscDtSkidsId));
    }

    @GetMapping("/all-rsc-dt-skid/present-in-saturation")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<List<SkidsDTO>> getAllRscDtLines() {
        return ResponseEntity.ok().body(rscIotFgSkidSaturationService.getAllSkids());
    }
}
