package com.example.validation.controller;

import com.example.validation.dto.RscDtDivisionDTO;
import com.example.validation.service.RscDtDivisionService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtDivisionController {
    private final RscDtDivisionService rscDtDivisionService;

    public RscDtDivisionController(RscDtDivisionService rscDtDivisionService) {
        this.rscDtDivisionService = rscDtDivisionService;
    }

    @GetMapping("/rsc-dt-division")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtDivisionDTO>> getAllRscDtDivision() {
        return ResponseEntity.ok().body(rscDtDivisionService.findAll());
    }
}
