package com.example.validation.controller;

import com.example.validation.dto.RscDtMaterialSubCategoryDTO;
import com.example.validation.service.RscDtMaterialSubCategoryService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
@RestController
@RequestMapping("/api")
public class RscDtMaterialSubCategoryController {
    private final RscDtMaterialSubCategoryService rscDtMaterialSubCategoryService;

    public RscDtMaterialSubCategoryController(RscDtMaterialSubCategoryService rscDtMaterialSubCategoryService) {
        this.rscDtMaterialSubCategoryService = rscDtMaterialSubCategoryService;
    }

    @GetMapping("/rsc-dt-material-sub-category")
    public ResponseEntity<List<RscDtMaterialSubCategoryDTO>> getAllRscDtMaterialCategory() {
        return ResponseEntity.ok().body(rscDtMaterialSubCategoryService .findAll());
    }
}
