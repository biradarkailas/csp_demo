package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotPmOtifCalculationService;
import com.example.validation.service.RscIotPmOtifSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotPmOtifCalculationController {
    private final RscIotPmOtifCalculationService rscIotPmOtifCalculationService;
    private final RscIotPmOtifSummaryService rscIotPmOtifSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotPmOtifCalculationController(RscIotPmOtifCalculationService rscIotPmOtifCalculationService, RscIotPmOtifSummaryService rscIotPmOtifSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotPmOtifCalculationService = rscIotPmOtifCalculationService;
        this.rscIotPmOtifSummaryService = rscIotPmOtifSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/pm-otif-calculation/calculate/{otifDate}")
    public ExecutionResponseDTO createPMOtifCalculations(@PathVariable String otifDate) {
        try {
            rscIotPmOtifCalculationService.calculateRscIotPmOtifCalculation(otifDate);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Otif Calculation", "PM Otif Calculation Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMOtifCalculations", e.getMessage(), rscIotPmOtifCalculationService.getPpheaderIdByDate(otifDate));
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Otif Calculation", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-otif-summary/calculate/{otifDate}")
    public ExecutionResponseDTO createPmOtifSummaryCalculations(@PathVariable String otifDate) {
        try {
            rscIotPmOtifSummaryService.calculateRscIotPmOtifSummary(otifDate);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Otif Summary Calculation", "PM Otif Summary Calculation Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPmOtifSummaryCalculations", e.getMessage(), rscIotPmOtifCalculationService.getPpheaderIdByDate(otifDate));
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Otif Summary Calculation", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-otif-calculation/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<OtifCalculationDetailsDTO> getPMOtifCalculations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmOtifSummaryService.getAllRscItPmOtifCalculation(ppHeaderId));
    }

    @GetMapping("/pm-otif-calculation/range-wise-suppliers-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<OtifCalculationSuppliersDetailsDTO> getPMOtifSuppliersDetailsCalculations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmOtifSummaryService.getPmOtifCalculationSuppliers(ppHeaderId));
    }

    @GetMapping("/pm-otif-calculation/suppliers-list/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<List<RscIotPmOtifSummaryDTO>> getPMOtifSuppliers(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPmOtifSummaryService.getPmOtifCalculationSuppliersList(ppHeaderId));
    }

    @GetMapping("/pm-otif/supplier-details/{ppHeaderId}/{rscDtSupplierId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<PmOtifCalculationSupplierWiseListDTO> getPMOtifSuppliersDetailsById(@PathVariable Long ppHeaderId, @PathVariable Long rscDtSupplierId) {
        return ResponseEntity.ok().body(rscIotPmOtifSummaryService.getSingleSuppliersDetails(ppHeaderId, rscDtSupplierId));
    }

    @GetMapping("/pm-otif-date-list/current-months/{mpsName}/mpsDate/{mpsDate}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_RM','role_FG')")
    public ResponseEntity<List<LocalDate>> getPMOtifDateListOfCurrentMonth(@PathVariable String mpsName, @PathVariable String mpsDate) {
        return ResponseEntity.ok().body(rscIotPmOtifCalculationService.getPMOtifDateListOfCurrentMonth(mpsName, mpsDate));
    }
}
