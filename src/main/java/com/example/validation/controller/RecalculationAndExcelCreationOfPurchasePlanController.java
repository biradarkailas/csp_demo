package com.example.validation.controller;

import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.dto.supply.RMPurchasePlanDTO;
import com.example.validation.service.RecalculationAndExcelCreationOfPurchasePlanService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RecalculationAndExcelCreationOfPurchasePlanController {
    private final RecalculationAndExcelCreationOfPurchasePlanService recalculationAndExcelCreationOfPurchasePlanService;
    private final Logger log = LoggerFactory.getLogger(RscIotPMPurchasePlanController.class);

    public RecalculationAndExcelCreationOfPurchasePlanController(RecalculationAndExcelCreationOfPurchasePlanService recalculationAndExcelCreationOfPurchasePlanService) {
        this.recalculationAndExcelCreationOfPurchasePlanService = recalculationAndExcelCreationOfPurchasePlanService;
    }

    @PutMapping("/pm-latest-purchase-plans")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<PurchasePlanDTO>> updateRscIotPMLatestPurchasePlan(@RequestBody List<PurchasePlanDTO> purchasePlanDTOs) throws ResourceNotFoundException {
        log.debug("REST request to update RscIotPMLatestPurchasePlans : {}", purchasePlanDTOs);
        List<PurchasePlanDTO> result = recalculationAndExcelCreationOfPurchasePlanService.saveAll(purchasePlanDTOs);
        return ResponseEntity.ok().body(result);
    }

    @PutMapping("/rm-latest-purchase-plans")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public ResponseEntity<List<RMPurchasePlanDTO>> updateRscIotRMLatestPurchasePlan(@RequestBody List<RMPurchasePlanDTO> rmPurchasePlanDTOs) {
        log.debug("REST request to update RscIotRMLatestPurchasePlans : {}", rmPurchasePlanDTOs);
        List<RMPurchasePlanDTO> result = recalculationAndExcelCreationOfPurchasePlanService.saveAllRmPurchasePlan(rmPurchasePlanDTOs);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/pm-latest-purchase-plans/excel/export/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public String getPmPurchasePlanExports(@PathVariable Long ppHeaderId) {
        return recalculationAndExcelCreationOfPurchasePlanService.createPmPurchasePlanExportOnUpdate(ppHeaderId);
    }

    @GetMapping("/rm-latest-purchase-plans/excel/export/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_RM')")
    public String getRmPurchasePlanExports(@PathVariable Long ppHeaderId) {
        return recalculationAndExcelCreationOfPurchasePlanService.createRmPurchasePlanExportOnUpdate(ppHeaderId);
    }
}
