package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotLatestMouldSaturationService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotLatestMouldSaturationController {

    private final RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotLatestMouldSaturationController(RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotLatestMouldSaturationService = rscIotLatestMouldSaturationService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/latest-mould-saturation/{ppHeaderId}")
    public ExecutionResponseDTO saveMouldSaturation(@PathVariable Long ppHeaderId) {
        try {
            rscIotLatestMouldSaturationService.saveMouldSaturation(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Latest Mould Saturation", "Latest Mould Saturation Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("saveMouldSaturation", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Latest Mould Saturation", e.getMessage(), false);
        }
    }

    @GetMapping("/rsc-iot-latest-mould-saturation-calculations")
    public ResponseEntity<List<LatestPurchasePlanDTO>> getAllMouldSaturationCalculations() {
        return ResponseEntity.ok().body(rscIotLatestMouldSaturationService.findAll());
    }

    @GetMapping("/latest-mould-saturation/supplier-wise/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<SupplierWiseLatestMouldSaturationDTO>> getSupplierWiseOriginalMouldSaturation(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotLatestMouldSaturationService.getSupplierWiseMouldSaturation(ppHeaderId));
    }

    @GetMapping("/latest-mould-saturation/dashboard/suppliers-list/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<List<RscDtSupplierDTO>> getSuppliersOfOriginalMouldSaturation(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotLatestMouldSaturationService.getSuppliersListOfMouldSaturation(ppHeaderId));
    }

    @GetMapping("/latest-mould-saturation-list/dashboard/{ppHeaderId}/{supplierId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<List<RscIotLatestMouldSaturationDTO>> getOriginalMouldSaturationListBySupplier(@PathVariable Long ppHeaderId, @PathVariable Long supplierId) {
        return ResponseEntity.ok().body(rscIotLatestMouldSaturationService.getMouldSaturationListBySupplier(ppHeaderId, supplierId));
    }
}
