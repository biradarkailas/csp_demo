package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.slob.DivisionFGSlobSummaryDashboardDTO;
import com.example.validation.dto.slob.MaterialWiseFGSlobSummaryDashboardDTO;
import com.example.validation.dto.slob.TypeWiseFGSlobDeviationDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotFGTypeSlobSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotFGTypeSlobSummaryController {

    private final RscIotFGTypeSlobSummaryService rscIotFGTypeSlobSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotFGTypeSlobSummaryController(RscIotFGTypeSlobSummaryService rscIotFGTypeSlobSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotFGTypeSlobSummaryService = rscIotFGTypeSlobSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/slob-summary/fg/type-wise/create/{ppHeaderId}")
    public ExecutionResponseDTO createTypeWiseFGSlobSummary(@PathVariable Long ppHeaderId) {
        try {
            rscIotFGTypeSlobSummaryService.createTypeWiseFGSlobSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Type wise FG Slob Summary", "Type wise FG Slob Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createTypeWiseFGSlobSummary", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("Type wise FG Slob Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/slob-deviation/division-wise/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public List<TypeWiseFGSlobDeviationDTO> getDivisionWiseFGSlobDeviation(@PathVariable Long ppHeaderId) {
        return rscIotFGTypeSlobSummaryService.getDivisionWiseFGSlobDeviation(ppHeaderId);
    }

    @GetMapping("/slob-summary/type-wise/dashboard/fg/{ppHeaderId}")
    public MaterialWiseFGSlobSummaryDashboardDTO getMaterialWiseFGSlobSummaryDashboardDTO(@PathVariable Long ppHeaderId) {
        return rscIotFGTypeSlobSummaryService.getMaterialWiseFGSlobSummaryDashboardDTO(ppHeaderId);
    }

    @GetMapping("/slob-summary/division-wise/dashboard/fg/{ppHeaderId}/{fgTypeId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public DivisionFGSlobSummaryDashboardDTO getDivisionFGSlobSummaryDashboardDTO(@PathVariable Long ppHeaderId, @PathVariable Long fgTypeId) {
        return rscIotFGTypeSlobSummaryService.getDivisionFGSlobSummaryDashboardDTO(ppHeaderId, fgTypeId);
    }
}