package com.example.validation.controller;

import com.example.validation.dto.RscDtMapDTO;
import com.example.validation.service.RscDtMapService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtMapController {
    private final RscDtMapService rscDtMapService;

    public RscDtMapController(RscDtMapService rscDtMapService) {
        this.rscDtMapService = rscDtMapService;
    }

    @GetMapping("/rsc-dt-map")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtMapDTO>> getAllRscDtMap() {
        return ResponseEntity.ok().body(rscDtMapService.findAll());
    }
}
