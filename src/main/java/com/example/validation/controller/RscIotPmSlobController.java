package com.example.validation.controller;

import com.example.validation.dto.*;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscItPMSlobService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotPmSlobController {
    private final RscItPMSlobService rscItPMSlobService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotPmSlobController(RscItPMSlobService rscItPMSlobService, ExecutionErrorLogService executionErrorLogService) {
        this.rscItPMSlobService = rscItPMSlobService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/pm-slob/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createPMSlobCalculations(@PathVariable Long ppHeaderId) {
        try {
            rscItPMSlobService.calculateRscItPMSlob(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Slob", "PM Slob Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMSlobCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Slob", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-slob-summary/calculate/{ppHeaderId}")
    public ExecutionResponseDTO createPMSlobSummaryCalculations(@PathVariable Long ppHeaderId, @RequestParam(required = false) Double sitValue) {
        try {
            rscItPMSlobService.calculateRscItPMSlobSummary(ppHeaderId, sitValue);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Slob Summary", "PM Slob Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMSlobSummaryCalculations", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Slob Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-slob/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PMSlobDetailsDTO> getPMSlobCalculations(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getAllRscItPmSlob(ppHeaderId));
    }

    @GetMapping("/pm-slob-nullMapPrice-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PMSlobDetailsDTO> getPmSlobNullMapDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getNullMapPriceDetails(ppHeaderId));
    }

    @GetMapping("/pm-slob/total-consumption/{ppHeaderId}/{rscMtItemId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<SlobTotalConsumptionDetailsDTO> getTotalConsumptionDetails(@PathVariable Long ppHeaderId, @PathVariable Long rscMtItemId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getTotalConsumption(ppHeaderId, rscMtItemId));
    }

    @GetMapping("/pm-slob-summary-details/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<SlobSummaryDetailsDTO> getPmSlobSummaryDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getPmSlobSummaryDetails(ppHeaderId));
    }

   /* @PutMapping("/pm-slob-saveAllDetails")
    public ResponseEntity<List<PmSlobDTO>> updateRscIotPMSlob(@RequestBody List<PmSlobDTO> pmSlobDTO) throws ResourceNotFoundException {
        log.debug("REST request to update RscItPMSlobDetails : {}", pmSlobDTO);
        List<PmSlobDTO> result = rscItPMSlobService.saveAllPmSlobDetails(pmSlobDTO);
        return ResponseEntity.ok().body(result);
    }*/

    @GetMapping("/pm-slob/reasons/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<RscDtReasonDTO>> getAllPmSlobsReasons(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getAllPmSlobsReasons(ppHeaderId));
    }

   /* @PutMapping("/pm-slob-save-reasons/{sitValue}/{ppHeaderId}")
    public ResponseEntity<List<PmSlobDTO>> updateRscItPMSlobList(@RequestBody List<PmSlobDTO> pmSlobDTO, @PathVariable Double sitValue, @PathVariable Long ppHeaderId) throws ResourceNotFoundException {
        log.debug("REST request to update RscIotPMSlobList : {}", pmSlobDTO);
        List<PmSlobDTO> result = rscItPMSlobService.saveAllPmSlobs(pmSlobDTO, sitValue, ppHeaderId);
        return ResponseEntity.ok().body(result);
    }*/

    @GetMapping("/slob-summary/pm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<SlobSummaryDTO> getPmSlobSummarysDetails(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getPmSlobSummary(ppHeaderId));
    }

    @GetMapping("/slob-movement/pm")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<SlobMovementDetailsDTO> getPmSlobMovementDetails() {
        return ResponseEntity.ok().body(rscItPMSlobService.getPmSlobMovement());
    }

    @GetMapping("/slob-inventory-quality-index/pm")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<SlobInventoryQualityIndexDetailsDTO> getPmSlobInventoryQualityIndexDetails() {
        return ResponseEntity.ok().body(rscItPMSlobService.getPmSlobInventoryQualityIndex());
    }

    @GetMapping("/slob-summary/dashboard/pm/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM','role_Analyst')")
    public ResponseEntity<SlobSummaryDashboardDTO> getPmSlobSummaryDashboardDTO(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscItPMSlobService.getPmSlobSummaryDashboardDTO(ppHeaderId));
    }
}