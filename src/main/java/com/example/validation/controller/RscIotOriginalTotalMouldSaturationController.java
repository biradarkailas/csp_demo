package com.example.validation.controller;

import com.example.validation.dto.RscIotOriginalTotalMouldSaturationDTO;
import com.example.validation.service.RscIotOriginalTotalMouldSaturationService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscIotOriginalTotalMouldSaturationController {
    private final RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService;

    public RscIotOriginalTotalMouldSaturationController(RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService) {
        this.rscIotOriginalTotalMouldSaturationService = rscIotOriginalTotalMouldSaturationService;
    }

    @GetMapping("/rsc-iot-original-total-mould-saturation/{ppheaderId}")
    public String saveOriginalTotalMouldSaturation(@PathVariable Long ppheaderId) {
        return rscIotOriginalTotalMouldSaturationService.saveOriginalTotalMouldSaturation(ppheaderId);
    }

    @GetMapping("/rsc-iot-all-original-total-mould-saturation")
    public ResponseEntity<List<RscIotOriginalTotalMouldSaturationDTO>> getAllOriginalTotalMouldSaturation() {
        return ResponseEntity.ok().body(rscIotOriginalTotalMouldSaturationService.findAll());
    }
}
