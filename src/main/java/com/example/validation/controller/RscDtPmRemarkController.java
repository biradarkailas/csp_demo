package com.example.validation.controller;

import com.example.validation.domain.RscDtPMRemark;
import com.example.validation.dto.RscDtPMRemarkDTO;
import com.example.validation.service.RscDtPMRemarkService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtPmRemarkController {
    private final RscDtPMRemarkService rscDtPMRemarkService;


    public RscDtPmRemarkController(RscDtPMRemarkService rscDtPMRemarkService) {
        this.rscDtPMRemarkService = rscDtPMRemarkService;
    }

    @GetMapping("/remark/pm")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<List<RscDtPMRemarkDTO>> getPmRemarks() {
        return ResponseEntity.ok().body(rscDtPMRemarkService.findAll());
    }

    @PostMapping("/remark/pm/new")
    public RscDtPMRemarkDTO saveNewRemark(@RequestBody RscDtPMRemark rscDtPMRemark) {
        return rscDtPMRemarkService.save(rscDtPMRemark);
    }

    @PutMapping("/remark/pm/update")
    public RscDtPMRemarkDTO saveRscIitIdpStatus(@RequestBody RscDtPMRemark rscDtPMRemark) {
        return rscDtPMRemarkService.save(rscDtPMRemark);
    }
}
