package com.example.validation.controller;

import com.example.validation.dto.SlobInventoryQualityIndexDetailsDTO;
import com.example.validation.dto.SlobMovementDetailsDTO;
import com.example.validation.service.FgSlobMovementAndInventoryIndexService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class FgSlobMovementAndInventoryIndexController {
    private final FgSlobMovementAndInventoryIndexService fgSlobMovementAndInventoryIndexService;

    public FgSlobMovementAndInventoryIndexController(FgSlobMovementAndInventoryIndexService fgSlobMovementAndInventoryIndexService) {
        this.fgSlobMovementAndInventoryIndexService = fgSlobMovementAndInventoryIndexService;
    }

    @GetMapping("/slob-movement/fg")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<SlobMovementDetailsDTO> getFgSlobMovementDetails() {
        return ResponseEntity.ok().body(fgSlobMovementAndInventoryIndexService.getFgSlobMovement());
    }

    @GetMapping("/slob-inventory-quality-index/fg")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public ResponseEntity<SlobInventoryQualityIndexDetailsDTO> getFgSlobInventoryQualityIndexDetails() {
        return ResponseEntity.ok().body(fgSlobMovementAndInventoryIndexService.getFgSlobInventoryQualityIndex());
    }
}
