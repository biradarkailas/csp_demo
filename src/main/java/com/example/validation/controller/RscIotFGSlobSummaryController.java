package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.FGSlobSummaryWithFgTypeAndDivisionDTO;
import com.example.validation.dto.slob.DivisionFGSlobSummaryIQDashboardDTO;
import com.example.validation.dto.slob.FGSlobDeviationMainDTO;
import com.example.validation.dto.slob.FGSlobSummaryDTO;
import com.example.validation.dto.slob.FGSlobSummaryDashboardDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotFGSlobSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api")
public class RscIotFGSlobSummaryController {
    private final RscIotFGSlobSummaryService rscIotFGSlobSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotFGSlobSummaryController(RscIotFGSlobSummaryService rscIotFGSlobSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotFGSlobSummaryService = rscIotFGSlobSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/slob-summary/fg/create/{ppHeaderId}")
    public ExecutionResponseDTO createFGSlobSummary(@PathVariable Long ppHeaderId) {
        try {
            rscIotFGSlobSummaryService.createFGSlobSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Slob Summary", "FG Slob Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFGSlobSummary", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG Slob Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/slob-summary/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public FGSlobSummaryDTO getFGSlobSummary(@PathVariable Long ppHeaderId) {
        return rscIotFGSlobSummaryService.getFGSlobSummaryDTO(ppHeaderId);
    }

    @GetMapping("/slob-deviation/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public FGSlobDeviationMainDTO getFGSlobDeviation(@PathVariable Long ppHeaderId) {
        return rscIotFGSlobSummaryService.getFGTotalSlobDeviation(ppHeaderId);
    }

    @GetMapping("/slob-summary/dashboard/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public FGSlobSummaryDashboardDTO getFGSlobSummaryDashboardDTO(@PathVariable Long ppHeaderId) {
        return rscIotFGSlobSummaryService.getFGSlobSummaryDashboardDTO(ppHeaderId);
    }

    @GetMapping("/slob-summary/division-wise/iq-dashboard/fg/{ppHeaderId}/{fgTypeId}")
    @PreAuthorize("hasAnyAuthority('role_FG')")
    public DivisionFGSlobSummaryIQDashboardDTO getDivisionFGSlobSummaryIQDashboardDTO(@PathVariable Long ppHeaderId, @PathVariable Long fgTypeId) {
        return rscIotFGSlobSummaryService.getDivisionFGSlobSummaryIQDashboardDTO(ppHeaderId, fgTypeId);
    }

    @GetMapping("/fg-slob-summary/division-wise/dashboard/{ppHeaderId}/{fgTypeId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public FGSlobSummaryWithFgTypeAndDivisionDTO getFGSlobSummaryDashboardDTO(@PathVariable Long ppHeaderId, @PathVariable Long fgTypeId, @RequestParam(required = false) Long divisionId) {
        return rscIotFGSlobSummaryService.getFGSlobSummaryDashboardDTO(ppHeaderId, fgTypeId, divisionId);
    }
}