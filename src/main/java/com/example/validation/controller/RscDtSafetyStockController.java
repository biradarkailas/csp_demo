package com.example.validation.controller;

import com.example.validation.dto.RscDtSafetyStockDTO;
import com.example.validation.service.RscDtSafetyStockService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtSafetyStockController {
    private final RscDtSafetyStockService rscDtSafetyStockService;

    public RscDtSafetyStockController(RscDtSafetyStockService rscDtSafetyStockService){
        this.rscDtSafetyStockService = rscDtSafetyStockService;
    }

    @GetMapping("/rsc-dt-safety-stock")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtSafetyStockDTO>> getAllRscDtSafetyStock() {
        return ResponseEntity.ok().body(rscDtSafetyStockService.findAll());
    }
}
