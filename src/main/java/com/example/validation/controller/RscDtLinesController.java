package com.example.validation.controller;

import com.example.validation.dto.RscDtLinesDTO;
import com.example.validation.service.RscDtLinesService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtLinesController {
    private final RscDtLinesService rscDtLinesService;

    public RscDtLinesController(RscDtLinesService rscDtLinesService) {
        this.rscDtLinesService = rscDtLinesService;
    }

    @GetMapping("/rsc-dt-lines")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Admin')")
    public ResponseEntity<List<RscDtLinesDTO>> getAllRscDtLines() {
        return ResponseEntity.ok().body( rscDtLinesService.findAll() );
    }

    @GetMapping("/rsc-dt-lines-list-pagination/{pageNo}/{pageSize}")
    public ResponseEntity<List<RscDtLinesDTO>> findFgLinesListWithPagination(@PathVariable Integer pageNo, @PathVariable Integer pageSize) {
        return ResponseEntity.ok().body( rscDtLinesService.findFgLinesWithPagination( pageNo, pageSize ) );
    }
}
