package com.example.validation.controller;

import com.example.validation.dto.RscDtStdPriceDTO;
import com.example.validation.service.RscDtStdPriceService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtStdPriceController {

    private final RscDtStdPriceService rscDtStdPriceService;

    public RscDtStdPriceController(RscDtStdPriceService rscDtStdPriceService){this.rscDtStdPriceService = rscDtStdPriceService;
    }

    @GetMapping("/rsc-dt-std-price")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtStdPriceDTO>> getAllRscDtStdPrice() {
        return ResponseEntity.ok().body(rscDtStdPriceService.findAll());
    }
}


