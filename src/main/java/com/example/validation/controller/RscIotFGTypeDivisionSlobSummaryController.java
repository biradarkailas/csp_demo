package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.FgTypesAndDivisionsDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotFGTypeDivisionSlobSummaryService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotFGTypeDivisionSlobSummaryController {

    private final RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotFGTypeDivisionSlobSummaryController(RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotFGTypeDivisionSlobSummaryService = rscIotFGTypeDivisionSlobSummaryService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/slob-summary/fg/division-wise/create/{ppHeaderId}")
    public ExecutionResponseDTO createFGTypeDivisionSlobSummary(@PathVariable Long ppHeaderId) {
        try {
            rscIotFGTypeDivisionSlobSummaryService.createFGTypeDivisionSlobSummary(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG division wise Slob Summary", "FG division wise Slob Summary Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createFGTypeDivisionSlobSummary", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("FG division wise Slob Summary", e.getMessage(), false);
        }
    }

    @GetMapping("/slob-summary/fg-types-and-division/fg/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_FG','role_Analyst')")
    public FgTypesAndDivisionsDTO getFgTypesAndDivisions(@PathVariable Long ppHeaderId) {
        return rscIotFGTypeDivisionSlobSummaryService.getFgTypesAndDivisionsData(ppHeaderId);
    }
}

