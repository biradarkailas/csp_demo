package com.example.validation.controller;

import com.example.validation.dto.RscDtTechnicalSeriesDTO;
import com.example.validation.service.RscDtTechnicalSeriesService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtTechnicalSeriesController {
    private final RscDtTechnicalSeriesService rscDtTechnicalSeriesService;

    public RscDtTechnicalSeriesController(RscDtTechnicalSeriesService rscDtTechnicalSeriesService){
        this.rscDtTechnicalSeriesService = rscDtTechnicalSeriesService;
    }

    @GetMapping("/rsc-dt-technical-series")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtTechnicalSeriesDTO>> getAllRscDtTechnicalSeries() {
        return ResponseEntity.ok().body(rscDtTechnicalSeriesService.findAll());
    }
}
