package com.example.validation.controller;

import com.example.validation.dto.RscDtTransportModeDTO;
import com.example.validation.service.RscDtTransportModeService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class RscDtTransportModeController {
    private final RscDtTransportModeService rscDtTransportModeService;

    public RscDtTransportModeController(RscDtTransportModeService rscDtTransportModeService){
        this.rscDtTransportModeService = rscDtTransportModeService;
    }

    @GetMapping("/rsc-dt-transport-mode")
    @PreAuthorize("hasAnyAuthority('role_Admin')")
    public ResponseEntity<List<RscDtTransportModeDTO>> getAllRscDtTransportMode() {
        return ResponseEntity.ok().body(rscDtTransportModeService.findAll());
    }
}
