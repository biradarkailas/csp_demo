package com.example.validation.controller;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.dto.supply.PurchasePlanMainDTO;
import com.example.validation.service.ExecutionErrorLogService;
import com.example.validation.service.RscIotPMPurchasePlanService;
import com.example.validation.util.ExecutionResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class RscIotPMPurchasePlanController {
    private final RscIotPMPurchasePlanService rscIotPMPurchasePlanService;
    private final ExecutionErrorLogService executionErrorLogService;

    public RscIotPMPurchasePlanController(RscIotPMPurchasePlanService rscIotPMPurchasePlanService, ExecutionErrorLogService executionErrorLogService) {
        this.rscIotPMPurchasePlanService = rscIotPMPurchasePlanService;
        this.executionErrorLogService = executionErrorLogService;
    }

    @GetMapping("/pm-purchase-plan/create/{ppHeaderId}")
    public ExecutionResponseDTO createPMSupply(@PathVariable Long ppHeaderId) {
        try {
            rscIotPMPurchasePlanService.createPMPurchasePlan(ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Purchase Plan", "PM Purchase Plan Created Successfully", true);
        } catch (Exception e) {
            executionErrorLogService.saveExecutionError("createPMSupply", e.getMessage(), ppHeaderId);
            return ExecutionResponseUtil.getExecutionResponseDTO("PM Purchase Plan", e.getMessage(), false);
        }
    }

    @GetMapping("/pm-original-purchase-plan/{ppHeaderId}")
    public ResponseEntity<PurchasePlanMainDTO> getPMOriginalPurchasePlan(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPMPurchasePlanService.getPMOriginalPurchasePlan(ppHeaderId));
    }

    @GetMapping("/pm-latest-purchase-plan/{ppHeaderId}")
    @PreAuthorize("hasAnyAuthority('role_PM')")
    public ResponseEntity<PurchasePlanMainDTO> getPMLatestPurchasePlan(@PathVariable Long ppHeaderId) {
        return ResponseEntity.ok().body(rscIotPMPurchasePlanService.getPMLatestPurchasePlan(ppHeaderId));
    }
}