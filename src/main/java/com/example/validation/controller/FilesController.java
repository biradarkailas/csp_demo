package com.example.validation.controller;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.dto.FileNameDTO;
import com.example.validation.dto.FileUploadStatusDTO;
import com.example.validation.service.FileService;
import com.example.validation.util.FileOperation;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.util.List;

@RestController
@RequestMapping("/api")
public class FilesController {
    private final FileService fileService;

    public FilesController(FileService fileService) {
        this.fileService = fileService;
    }

    @GetMapping("/all-files/{tagName}")
    public List<FileNameDTO> getAllFiles(@PathVariable String tagName) {
        return fileService.findAllFiles(tagName);
    }

    @GetMapping("/delete-all-source")
    public Integer deleteAllFromSource() {
        FileOperation.deleteNonEmptyDir(new File(ApplicationConstants.SOURCE_PATH), true);
        return 1;
    }
}
