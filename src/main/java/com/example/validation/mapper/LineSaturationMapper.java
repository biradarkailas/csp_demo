package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgLineSaturation;
import com.example.validation.dto.LinesDTO;
import com.example.validation.dto.RscDtLinesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtLinesMapper.class})
public interface LineSaturationMapper extends EntityMapper<LinesDTO, RscIotFgLineSaturation> {

    @Mapping(source = "rscDtLines.id", target = "id")
    @Mapping(source = "rscDtLines.name", target = "lineName")
    LinesDTO toDto(RscIotFgLineSaturation rscIotFgLineSaturation);

    default RscIotFgLineSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgLineSaturation rscIotFgLineSaturation = new RscIotFgLineSaturation();
        rscIotFgLineSaturation.setId(id);
        return rscIotFgLineSaturation;
    }

}
