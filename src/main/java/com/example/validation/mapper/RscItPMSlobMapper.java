package com.example.validation.mapper;

import com.example.validation.domain.RscItPMSlob;
import com.example.validation.dto.RscItPMSlobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtReasonMapper.class, RscDtItemCodeMapper.class, RscItItemWiseSupplierMapper.class})
public interface RscItPMSlobMapper extends EntityMapper<RscItPMSlobDTO, RscItPMSlob> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "rscDtItemCode")
    @Mapping(source = "rscDtReason.id", target = "rscDtReasonId")
    @Mapping(source = "rscDtReason.reason", target = "rscDtReasonName")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    RscItPMSlobDTO toDto(RscItPMSlob rscItPMSlob);

    @Mapping(source = "rscDtReasonId", target = "rscDtReason")
    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    RscItPMSlob toEntity(RscItPMSlobDTO rscItPMSlobDTO);

    default RscItPMSlob fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItPMSlob rscItPMSlob = new RscItPMSlob();
        rscItPMSlob.setId(id);
        return rscItPMSlob;
    }
}
