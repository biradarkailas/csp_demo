package com.example.validation.mapper;

import com.example.validation.domain.RscItRMPivotConsumption;
import com.example.validation.dto.ConsumptionItemDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface ConsumptionItemDetailsMapper extends EntityMapper<ConsumptionItemDetailsDTO, RscItRMPivotConsumption> {

    @Mapping(source = "rscMtItemParent.id", target = "parentItemId")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.code", target = "parentItemCode")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.description", target = "parentItemDescription")
    @Mapping(source = "rscMtItemChild.id", target = "childItemId")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.code", target = "childItemCode")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.description", target = "childItemDescription")
    ConsumptionItemDetailsDTO toDto(RscItRMPivotConsumption rscItRMPivotConsumption);

    default RscItRMPivotConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItRMPivotConsumption rscItRMPivotConsumption = new RscItRMPivotConsumption();
        rscItRMPivotConsumption.setId(id);
        return rscItRMPivotConsumption;
    }
}