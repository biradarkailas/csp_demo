package com.example.validation.mapper;

import com.example.validation.domain.RscIitRMItemWiseSupplierDetails;
import com.example.validation.dto.RscIitRMItemWiseSupplierDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscItItemWiseSupplierMapper.class, RscIitSupplierWiseTransportMapper.class, RscDtTechnicalSeriesMapper.class, RscDtMoqMapper.class, RscMtItemMapper.class, RscDtSafetyStockMapper.class})
public interface RscIitRMItemWiseSupplierDetailsMapper extends EntityMapper<RscIitRMItemWiseSupplierDetailsDTO, RscIitRMItemWiseSupplierDetails> {

    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscItItemWiseSupplier.rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    @Mapping(source = "rscItItemWiseSupplier.rscDtMoq.moqValue", target = "moqValue")
    @Mapping(source = "rscItItemWiseSupplier.rscDtTechnicalSeries.seriesValue", target = "packSize")
    @Mapping(source = "rscItItemWiseSupplier.allocationPercentage", target = "allocation")
    @Mapping(source = "rscIitSupplierWiseTransport.id", target = "rscIitSupplierWiseTransportId")
    @Mapping(source = "rscItItemWiseSupplier.applicableFrom", target = "applicableFrom")
    @Mapping(source = "rscItItemWiseSupplier.applicableTo", target = "applicableTo")
    RscIitRMItemWiseSupplierDetailsDTO toDto(RscIitRMItemWiseSupplierDetails rscIitRMItemWiseSupplierDetails);

    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscIitSupplierWiseTransportId", target = "rscIitSupplierWiseTransport")
    RscIitRMItemWiseSupplierDetails toEntity(RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO);

    default RscIitRMItemWiseSupplierDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitRMItemWiseSupplierDetails rscIitRMItemWiseSupplierDetails = new RscIitRMItemWiseSupplierDetails();
        rscIitRMItemWiseSupplierDetails.setId(id);
        return rscIitRMItemWiseSupplierDetails;
    }
}