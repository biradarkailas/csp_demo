package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import com.example.validation.dto.slob.DivisionWiseFGSlobSummaryIQDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtDivisionMapper.class})
public interface DivisionWiseFGSlobSummaryIQDashboardMapper extends EntityMapper<DivisionWiseFGSlobSummaryIQDashboardDTO, RscIotFGTypeDivisionSlobSummary> {

    @Mapping(source = "rscDtDivision.id", target = "typeId")
    @Mapping(source = "rscDtDivision.aliasName", target = "typeName")
    @Mapping(source = "totalSlobValue", target = "slobValues.slobValue")
    @Mapping(source = "totalStockValue", target = "slobValues.stockValue")
    @Mapping(source = "stockQualityPercentage", target = "iqValue")
    DivisionWiseFGSlobSummaryIQDashboardDTO toDto(RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary);

    default RscIotFGTypeDivisionSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary = new RscIotFGTypeDivisionSlobSummary();
        rscIotFGTypeDivisionSlobSummary.setId(id);
        return rscIotFGTypeDivisionSlobSummary;
    }
}