package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import com.example.validation.dto.slob.RscIotFGTypeSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtFGTypeMapper.class})
public interface RscIotFGTypeSlobSummaryMapper extends EntityMapper<RscIotFGTypeSlobSummaryDTO, RscIotFGTypeSlobSummary> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtFGType.id", target = "fgTypeId")
    @Mapping(source = "rscDtFGType.type", target = "fgType")
    RscIotFGTypeSlobSummaryDTO toDto(RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "fgTypeId", target = "rscDtFGType")
    RscIotFGTypeSlobSummary toEntity(RscIotFGTypeSlobSummaryDTO rscIotFGTypeSlobSummaryDTO);

    default RscIotFGTypeSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary = new RscIotFGTypeSlobSummary();
        rscIotFGTypeSlobSummary.setId(id);
        return rscIotFGTypeSlobSummary;
    }
}