package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestMouldSaturation;
import com.example.validation.domain.RscIotLatestMouldSaturation;
import com.example.validation.dto.LatestPurchasePlanDTO;
import com.example.validation.dto.OriginalPurchasePlanDTO;
import com.example.validation.dto.RscIotLatestMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class, RscMtPPheaderMapper.class})
public interface RscIotLatestMouldSaturationMapper extends EntityMapper<LatestPurchasePlanDTO, RscIotLatestMouldSaturation> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtMould.mould", target = "mould")
    @Mapping(source = "rscDtMould.id", target = "mouldId")
    LatestPurchasePlanDTO toDto(RscIotLatestMouldSaturation rscIotLatestMouldSaturation);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader.id")
    @Mapping(source = "id", target = "rscIotPMLatestPurchasePlan.id")
    @Mapping(source = "mouldId", target = "rscDtMould.id")
    RscIotLatestMouldSaturation toEntity(LatestPurchasePlanDTO latestPurchasePlanDTO);

    default RscIotLatestMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestMouldSaturation rscIotLatestMouldSaturation = new RscIotLatestMouldSaturation();
        rscIotLatestMouldSaturation.setId(id);
        return rscIotLatestMouldSaturation;
    }
}
