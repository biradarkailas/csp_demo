package com.example.validation.mapper;

import com.example.validation.domain.RscDtStockType;
import com.example.validation.dto.RscDtStockTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtStockTypeMapper extends EntityMapper<RscDtStockTypeDTO, RscDtStockType> {
    RscDtStockTypeDTO toDto(RscDtStockType rscDtStockType);

    RscDtStockType toEntity(RscDtStockTypeDTO rscDtStockTypeDTO);

    default RscDtStockType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtStockType rscDtStockType = new RscDtStockType();
        rscDtStockType.setId(id);
        return rscDtStockType;
    }
}
