package com.example.validation.mapper;

import com.example.validation.domain.RscDtMaterialCategory;
import com.example.validation.dto.RscDtMaterialCategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtMaterialCategoryMapper extends EntityMapper<RscDtMaterialCategoryDTO, RscDtMaterialCategory> {

    RscDtMaterialCategoryDTO toDto(RscDtMaterialCategory rscDtMaterialCategory);

    RscDtMaterialCategory toEntity(RscDtMaterialCategoryDTO rscDtMaterialCategoryDTO);

    default RscDtMaterialCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtMaterialCategory rscDtMaterialCategory = new RscDtMaterialCategory();
        rscDtMaterialCategory.setId(id);
        return rscDtMaterialCategory;
    }
}

