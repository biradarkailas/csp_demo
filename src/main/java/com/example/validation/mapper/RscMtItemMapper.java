package com.example.validation.mapper;

import com.example.validation.domain.RscMtItem;
import com.example.validation.dto.RscMtItemDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtItemCodeMapper.class, RscDtItemCategoryMapper.class,
        RscDtItemTypeMapper.class, RscDtCoverDaysMapper.class, RscDtUomMapper.class, RscDtDivisionMapper.class,
        RscDtSafetyStockMapper.class, RscDtItemClassMapper.class, RscDtMapMapper.class, RscDtStdPriceMapper.class, RscDtTechnicalSeriesMapper.class, RscDtMoqMapper.class})
public interface RscMtItemMapper extends EntityMapper<RscMtItemDTO, RscMtItem> {

    @Mapping(source = "rscDtItemCode.id", target = "rscDtItemCodeId")
    @Mapping(source = "rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtItemCategory.id", target = "rscDtItemCategoryId")
    @Mapping(source = "rscDtItemCategory.category", target = "itemCategoryName")
    @Mapping(source = "rscDtItemCategory.description", target = "itemCategoryDescription")
    @Mapping(source = "rscDtItemType.id", target = "rscDtItemTypeId")
    @Mapping(source = "rscDtItemType.name", target = "itemTypeName")
    @Mapping(source = "rscDtCoverDays.id", target = "rscDtCoverDaysId")
    @Mapping(source = "rscDtCoverDays.coverDays", target = "coverDays")
    @Mapping(source = "rscDtUom.id", target = "rscDtUomId")
    @Mapping(source = "rscDtUom.aliasName", target = "uomName")
    @Mapping(source = "rscDtDivision.id", target = "rscDtDivisionId")
    @Mapping(source = "rscDtDivision.aliasName", target = "divisionName")
    @Mapping(source = "rscDtSafetyStock.id", target = "rscDtSafetyStockId")
    @Mapping(source = "rscDtSafetyStock.stockValue", target = "stockValue")
    @Mapping(source = "rscDtItemClass.id", target = "rscDtItemClassId")
    @Mapping(source = "rscDtItemClass.aliasName", target = "itemClassName")
    @Mapping(source = "rscDtTechnicalSeries.id", target = "rscDtTechnicalSeriesId")
    @Mapping(source = "rscDtTechnicalSeries.seriesValue", target = "seriesValue")
    @Mapping(source = "rscDtMoq.id", target = "rscDtMoqId")
    @Mapping(source = "rscDtMoq.moqValue", target = "moqValue")
    @Mapping(source = "rscDtMap.id", target = "rscDtMapId")
    @Mapping(source = "rscDtMap.mapPrice", target = "mapPrice")
    @Mapping(source = "rscDtStdPrice.stdPrice", target = "stdPrice")
    RscMtItemDTO toDto(RscMtItem rscMtItem);

    RscMtItem toEntity(RscMtItemDTO rscMtItemDTO);

    default RscMtItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtItem rscMtItem = new RscMtItem();
        rscMtItem.setId(id);
        return rscMtItem;
    }
}
