package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmCoverDaysSummary;
import com.example.validation.dto.PmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface PmCoverDaysSummaryMapper extends EntityMapper<PmCoverDaysSummaryDTO, RscIotPmCoverDaysSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    PmCoverDaysSummaryDTO toDto(RscIotPmCoverDaysSummary rscIotPmCoverDaysSummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader.id")
    RscIotPmCoverDaysSummary toEntity(PmCoverDaysSummaryDTO pmCoverDaysSummaryDTO);

    default RscIotPmCoverDaysSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmCoverDaysSummary rscIotPmCoverDaysSummary = new RscIotPmCoverDaysSummary();
        rscIotPmCoverDaysSummary.setId(id);
        return rscIotPmCoverDaysSummary;
    }
}
