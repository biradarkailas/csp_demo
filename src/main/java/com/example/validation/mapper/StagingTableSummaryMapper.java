package com.example.validation.mapper;

import com.example.validation.domain.StagingTableSummary;
import com.example.validation.dto.StagingTableSummaryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface StagingTableSummaryMapper extends EntityMapper<StagingTableSummaryDTO, StagingTableSummary> {
    StagingTableSummaryDTO toDto(StagingTableSummary stagingTableSummary);

    StagingTableSummary toEntity(StagingTableSummaryDTO stagingTableSummaryDTO);

    default StagingTableSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        StagingTableSummary stagingTableSummary = new StagingTableSummary();
        stagingTableSummary.setId(id);
        return stagingTableSummary;
    }
}
