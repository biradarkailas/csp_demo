package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmRtd;
import com.example.validation.dto.IndividualValuesRtdDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPmRtdMapper.class})
public interface IndividualValuesPmRtdMapper extends EntityMapper<IndividualValuesRtdDTO, RscMtPmRtd> {
    @Mapping(source = "quantity", target = "value")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    IndividualValuesRtdDTO toDto(RscMtPmRtd rscMtPmRtd);
}
