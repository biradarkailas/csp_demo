package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmCoverDays;
import com.example.validation.dto.RscIotPmCoverDaysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscItPMGrossConsumptionMapper.class, RscItItemWiseSupplierMapper.class})
public interface RscIotPmCoverDaysMapper extends EntityMapper<RscIotPmCoverDaysDTO, RscIotPmCoverDays> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItPMGrossConsumption.id", target = "rscItPMGrossConsumptionId")
    RscIotPmCoverDaysDTO toDto(RscIotPmCoverDays rscIotPmCoverDays);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscItPMGrossConsumptionId", target = "rscItPMGrossConsumption")
    RscIotPmCoverDays toEntity(RscIotPmCoverDaysDTO rscIotPmCoverDaysDTO);

    default RscIotPmCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmCoverDays rscIotPmCoverDays = new RscIotPmCoverDays();
        rscIotPmCoverDays.setId(id);
        return rscIotPmCoverDays;
    }
}
