package com.example.validation.mapper;

import com.example.validation.domain.RscIitRmBulkOpRm;
import com.example.validation.dto.RscIitRmBulkOpRmDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class})
public interface RscIitRmBulkOpRmMapper extends EntityMapper<RscIitRmBulkOpRmDTO, RscIitRmBulkOpRm>{
    @Mapping(source = "rscMtRmItem.id", target = "rscMtRmItemId")
    @Mapping(source = "rscMtBulkItem.id", target = "rscMtBulkItemId")
    RscIitRmBulkOpRmDTO toDto(RscIitRmBulkOpRm rscIitRmBulkOpRm);

    @Mapping(source = "rscMtRmItemId", target = "rscMtRmItem")
    @Mapping(source = "rscMtBulkItemId", target = "rscMtBulkItem")
    RscIitRmBulkOpRm toEntity(RscIitRmBulkOpRmDTO rscIitRmBulkOpRmDTO);

    default RscIitRmBulkOpRm fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitRmBulkOpRm rscIitRmBulkOpRm = new RscIitRmBulkOpRm();
        rscIitRmBulkOpRm.setId(id);
        return rscIitRmBulkOpRm;
    }
}
