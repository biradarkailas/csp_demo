package com.example.validation.mapper;

import com.example.validation.domain.RscDtFolderName;
import com.example.validation.dto.RscDtFolderNameDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtFolderNameMapper extends EntityMapper<RscDtFolderNameDTO, RscDtFolderName> {
    RscDtFolderNameDTO toDto(RscDtFolderName RscDtFolderName);

    RscDtFolderName toEntity(RscDtFolderNameDTO rscDtFolderNameDTO);

    default RscDtFolderName fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtFolderName rscDtFolderName = new RscDtFolderName();
        rscDtFolderName.setId(id);
        return rscDtFolderName;
    }
}
