package com.example.validation.mapper;

import com.example.validation.domain.RscItRMPivotConsumption;
import com.example.validation.dto.RscItRMPivotConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscItRMPivotConsumptionMapper extends EntityMapper<RscItRMPivotConsumptionDTO, RscItRMPivotConsumption> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtItemParent.id", target = "rscMtItemParentId")
    @Mapping(source = "rscMtItemChild.id", target = "rscMtItemChildId")
    RscItRMPivotConsumptionDTO toDto(RscItRMPivotConsumption rscItRMPivotConsumption);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemParentId", target = "rscMtItemParent")
    @Mapping(source = "rscMtItemChildId", target = "rscMtItemChild")
    RscItRMPivotConsumption toEntity(RscItRMPivotConsumptionDTO rscItRMPivotConsumptionDTO);

    default RscItRMPivotConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItRMPivotConsumption rscItRMPivotConsumption = new RscItRMPivotConsumption();
        rscItRMPivotConsumption.setId(id);
        return rscItRMPivotConsumption;
    }
}