package com.example.validation.mapper;

import com.example.validation.domain.RscDtMap;
import com.example.validation.dto.RscDtMapDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtMapMapper extends EntityMapper<RscDtMapDTO, RscDtMap> {
    RscDtMapDTO toDto(RscDtMap rscDtMap);

    RscDtMap toEntity(RscDtMapDTO rscDtMapDTO);

    default RscDtMap fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtMap rscDtMap = new RscDtMap();
        rscDtMap.setId(id);
        return rscDtMap;
    }
}
