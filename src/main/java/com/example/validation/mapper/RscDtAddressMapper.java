package com.example.validation.mapper;

import com.example.validation.domain.RscDtAddress;
import com.example.validation.dto.RscDtAddressDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtAddressMapper extends EntityMapper<RscDtAddressDTO, RscDtAddress> {
    RscDtAddressDTO toDto(RscDtAddress rscDtAddress);

    RscDtAddress toEntity(RscDtAddressDTO rscDtAddressDTO);

    default RscDtAddress fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtAddress rscDtAddress = new RscDtAddress();
        rscDtAddress.setId(id);
        return rscDtAddress;
    }
}
