package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgSkidSaturation;
import com.example.validation.dto.RscIotFgSkidSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtSkidsMapper.class})
public interface RscIotFgSkidSaturationMapper extends EntityMapper<RscIotFgSkidSaturationDTO, RscIotFgSkidSaturation> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtSkids.id", target = "rscDtSkidsId")
    RscIotFgSkidSaturationDTO toDto(RscIotFgSkidSaturation rscIotFgSkidSaturation);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtSkidsId", target = "rscDtSkids")
    RscIotFgSkidSaturation toEntity(RscIotFgSkidSaturationDTO rscIotFgSkidSaturationDTO);

    default RscIotFgSkidSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgSkidSaturation rscIotFgSkidSaturation = new RscIotFgSkidSaturation();
        rscIotFgSkidSaturation.setId(id);
        return rscIotFgSkidSaturation;
    }
}
