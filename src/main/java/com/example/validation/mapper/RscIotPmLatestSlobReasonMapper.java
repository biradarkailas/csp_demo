package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmLatestSlobReason;
import com.example.validation.dto.RscIotPmLatestSlobReasonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtReasonMapper.class})
public interface RscIotPmLatestSlobReasonMapper extends EntityMapper<RscIotPmLatestSlobReasonDTO, RscIotPmLatestSlobReason> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscDtReason.id", target = "rscDtReasonId")
    RscIotPmLatestSlobReasonDTO toDto(RscIotPmLatestSlobReason rscIotPmLatestSlobReason);

    @Mapping(source = "rscDtReasonId", target = "rscDtReason")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotPmLatestSlobReason toEntity(RscIotPmLatestSlobReasonDTO rscIotPmLatestSlobReasonDTO);

    default RscIotPmLatestSlobReason fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmLatestSlobReason rscIotPmLatestSlobReason = new RscIotPmLatestSlobReason();
        rscIotPmLatestSlobReason.setId(id);
        return rscIotPmLatestSlobReason;
    }
}