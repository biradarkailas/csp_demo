package com.example.validation.mapper;

import com.example.validation.domain.RscDtFileName;
import com.example.validation.dto.RscDtFileNameDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFolderNameMapper.class})
public interface RscDtFileNameMapper extends EntityMapper<RscDtFileNameDTO, RscDtFileName> {
    @Mapping(source = "rscDtFolderName.id", target = "folderId")
    @Mapping(source = "rscDtFolderName.folderName", target = "folderName")
    RscDtFileNameDTO toDto(RscDtFileName rscDtFileName);

    RscDtFileName toEntity(RscDtFileNameDTO rscDtFileNameDTO);

    default RscDtFileName fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtFileName rscDtFileName = new RscDtFileName();
        rscDtFileName.setId(id);
        return rscDtFileName;
    }
}
