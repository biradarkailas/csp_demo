package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmOpenPo;
import com.example.validation.dto.RscMtRmOpenPoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscMtSupplierMapper.class})
public interface RscMtRmOpenPoMapper extends EntityMapper<RscMtRmOpenPoDTO, RscMtRmOpenPo> {

    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtSupplier.id", target = "rscMtSupplierId")
    RscMtRmOpenPoDTO toDto(RscMtRmOpenPo rscMtRmOpenPo);

    @Mapping(source = "rscMtSupplierId", target = "rscMtSupplier")
    @Mapping(source = "mpsId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscMtRmOpenPo toEntity(RscMtRmOpenPoDTO rscMtRmOpenPoDTO);

    default RscMtRmOpenPo fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmOpenPo rscMtRmOpenPo = new RscMtRmOpenPo();
        rscMtRmOpenPo.setId(id);
        return rscMtRmOpenPo;
    }
}
