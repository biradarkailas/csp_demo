package com.example.validation.mapper;

import com.example.validation.domain.RscDtPMRemark;
import com.example.validation.dto.RscDtPMRemarkDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtPMRemarkMapper extends EntityMapper<RscDtPMRemarkDTO, RscDtPMRemark> {

    RscDtPMRemarkDTO toDto(RscDtPMRemark rscDtPMRemark);

    RscDtPMRemark toEntity(RscDtPMRemarkDTO rscDtPMRemarkDTO);

    default RscDtPMRemark fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtPMRemark rscDtPMRemark = new RscDtPMRemark();
        rscDtPMRemark.setId(id);
        return rscDtPMRemark;
    }
}