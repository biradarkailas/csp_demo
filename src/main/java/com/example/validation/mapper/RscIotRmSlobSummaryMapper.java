package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmSlobSummary;
import com.example.validation.dto.RscIotRmSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmSlobSummaryMapper extends EntityMapper<RscIotRmSlobSummaryDTO, RscIotRmSlobSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    RscIotRmSlobSummaryDTO toDto(RscIotRmSlobSummary rscIotRmSlobSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    RscIotRmSlobSummary toEntity(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO);

    default RscIotRmSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmSlobSummary rscIotRmSlobSummary = new RscIotRmSlobSummary();
        rscIotRmSlobSummary.setId(id);
        return rscIotRmSlobSummary;
    }
}