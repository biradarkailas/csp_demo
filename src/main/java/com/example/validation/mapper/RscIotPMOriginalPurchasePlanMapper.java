package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMOriginalPurchasePlan;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class, RscItItemWiseSupplierMapper.class, RscDtPMRemarkMapper.class})
public interface RscIotPMOriginalPurchasePlanMapper extends EntityMapper<RscIotPMPurchasePlanDTO, RscIotPMOriginalPurchasePlan> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscDtPMRemark.id", target = "rscDtPMRemarkId")
    RscIotPMPurchasePlanDTO toDto(RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan);

    @Mapping(source = "rscDtPMRemarkId", target = "rscDtPMRemark")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotPMOriginalPurchasePlan toEntity(RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO);

    default RscIotPMOriginalPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan = new RscIotPMOriginalPurchasePlan();
        rscIotPMOriginalPurchasePlan.setId(id);
        return rscIotPMOriginalPurchasePlan;
    }
}