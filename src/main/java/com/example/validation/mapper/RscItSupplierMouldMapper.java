package com.example.validation.mapper;

import com.example.validation.domain.RscItSupplierMould;
import com.example.validation.dto.RscItSupplierMouldDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscItSupplierMouldMapper extends EntityMapper<RscItSupplierMouldDTO, RscItSupplierMould>{

    RscItSupplierMouldDTO toDto(RscItSupplierMould rscItSupplierMould);

    RscItSupplierMould toEntity(RscItSupplierMouldDTO rscItSupplierMouldDTO);

    default RscItSupplierMould fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItSupplierMould rscItSupplierMould= new RscItSupplierMould();
        rscItSupplierMould.setId(id);
        return rscItSupplierMould;
    }
}
