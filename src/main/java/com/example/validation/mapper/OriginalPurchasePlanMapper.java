package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMOriginalPurchasePlan;
import com.example.validation.dto.OriginalPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class,RscItItemMouldMapper.class, RscDtMouldMapper.class})
public interface OriginalPurchasePlanMapper extends EntityMapper<OriginalPurchasePlanDTO, RscIotPMOriginalPurchasePlan> {
    @Mapping(source = "rscMtItem.rscItItemMould.rscDtMould.totalCapacity", target = "capacity")
    @Mapping(source = "rscMtItem.rscItItemMould.rscDtMould.id", target = "mouldId")
    @Mapping(source = "rscMtItem.rscItItemMould.rscDtMould.mould", target = "mould")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    OriginalPurchasePlanDTO toDto(RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan);

    default RscIotPMOriginalPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan = new RscIotPMOriginalPurchasePlan();
        rscIotPMOriginalPurchasePlan.setId(id);
        return rscIotPMOriginalPurchasePlan;
    }

}
