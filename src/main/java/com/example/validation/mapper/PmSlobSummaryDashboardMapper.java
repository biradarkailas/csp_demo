package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmSlobSummary;
import com.example.validation.dto.SlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface PmSlobSummaryDashboardMapper extends EntityMapper<SlobSummaryDashboardDTO, RscIotPmSlobSummary> {
    @Mapping(source = "totalObsoleteItemValue", target = "obValue")
    @Mapping(source = "totalSlowItemValue", target = "slValue")
    @Mapping(source = "stockQuantity", target = "inventoryQualityValue")
    @Mapping(source = "obsoleteItemPercentage", target = "obPercentage")
    @Mapping(source = "slowItemPercentage", target = "slPercentage")
    @Mapping(source = "stockQualityPercentage", target = "iqPercentage")
    SlobSummaryDashboardDTO toDto(RscIotPmSlobSummary rscIotPmSlobSummary);
}