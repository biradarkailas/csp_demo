package com.example.validation.mapper;

import com.example.validation.domain.ResourceTableUploadSummary;
import com.example.validation.dto.ResourceTableUploadSummaryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ResourceTableUploadSummaryMapper extends EntityMapper<ResourceTableUploadSummaryDTO, ResourceTableUploadSummary> {
    ResourceTableUploadSummaryDTO toDto(ResourceTableUploadSummary resourceTableUploadSummary);

    ResourceTableUploadSummary toEntity(ResourceTableUploadSummaryDTO resourceTableUploadSummaryDTO);

    default ResourceTableUploadSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        ResourceTableUploadSummary resourceTableUploadSummary = new ResourceTableUploadSummary();
        resourceTableUploadSummary.setId( id );
        return resourceTableUploadSummary;
    }
}

