package com.example.validation.mapper;


import com.example.validation.domain.RscDtSkids;
import com.example.validation.dto.RscDtSkidsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtSkidsMapper extends EntityMapper<RscDtSkidsDTO, RscDtSkids> {

    RscDtSkidsDTO toDto(RscDtSkids rscDtSkids);

    RscDtSkids toEntity(RscDtSkidsDTO rscDtSkidsDTO);

    default RscDtSkids fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtSkids rscDtSkids = new RscDtSkids();
        rscDtSkids.setId(id);
        return rscDtSkids;
    }
}