package com.example.validation.mapper;

import com.example.validation.domain.RscMtSupplier;
import com.example.validation.dto.RscMtSupplierDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtSupplierMapper extends EntityMapper<RscMtSupplierDTO, RscMtSupplier>{

    RscMtSupplierDTO toDto(RscMtSupplier rscMtSupplier);

    RscMtSupplier toEntity(RscMtSupplierDTO rscMtSupplierDTO);

    default RscMtSupplier fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtSupplier rscMtSupplier= new RscMtSupplier();
        rscMtSupplier.setId(id);
        return rscMtSupplier;
    }
}
