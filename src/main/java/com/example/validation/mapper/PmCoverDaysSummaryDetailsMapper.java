package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmCoverDaysSummary;
import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface PmCoverDaysSummaryDetailsMapper extends EntityMapper<CoverDaysSummaryDetailsDTO, RscIotPmCoverDaysSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "month1ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month1ConsumptionTotalValue")
    @Mapping(source = "month2ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month2ConsumptionTotalValue")
    @Mapping(source = "month3ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month3ConsumptionTotalValue")
    @Mapping(source = "month4ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month4ConsumptionTotalValue")
    @Mapping(source = "month5ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month5ConsumptionTotalValue")
    @Mapping(source = "month6ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month6ConsumptionTotalValue")
    @Mapping(source = "month7ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month7ConsumptionTotalValue")
    @Mapping(source = "month8ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month8ConsumptionTotalValue")
    @Mapping(source = "month9ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month9ConsumptionTotalValue")
    @Mapping(source = "month10ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month10ConsumptionTotalValue")
    @Mapping(source = "month11ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month11ConsumptionTotalValue")
    @Mapping(source = "month12ConsumptionTotalValue", target = "coverDaysConsumptionDTO.month12ConsumptionTotalValue")
    @Mapping(source = "month1CoverDays", target = "coverDaysDTO.month1CoverDays")
    @Mapping(source = "month2CoverDays", target = "coverDaysDTO.month2CoverDays")
    @Mapping(source = "month3CoverDays", target = "coverDaysDTO.month3CoverDays")
    @Mapping(source = "month4CoverDays", target = "coverDaysDTO.month4CoverDays")
    @Mapping(source = "month5CoverDays", target = "coverDaysDTO.month5CoverDays")
    @Mapping(source = "month6CoverDays", target = "coverDaysDTO.month6CoverDays")
    @Mapping(source = "month7CoverDays", target = "coverDaysDTO.month7CoverDays")
    @Mapping(source = "month8CoverDays", target = "coverDaysDTO.month8CoverDays")
    @Mapping(source = "month9CoverDays", target = "coverDaysDTO.month9CoverDays")
    @Mapping(source = "month10CoverDays", target = "coverDaysDTO.month10CoverDays")
    @Mapping(source = "month11CoverDays", target = "coverDaysDTO.month11CoverDays")
    @Mapping(source = "month12CoverDays", target = "coverDaysDTO.month12CoverDays")
    CoverDaysSummaryDetailsDTO toDto(RscIotPmCoverDaysSummary rscIotPmCoverDaysSummary);

    default RscIotPmCoverDaysSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmCoverDaysSummary rscIotPmCoverDaysSummary = new RscIotPmCoverDaysSummary();
        rscIotPmCoverDaysSummary.setId(id);
        return rscIotPmCoverDaysSummary;
    }
}
