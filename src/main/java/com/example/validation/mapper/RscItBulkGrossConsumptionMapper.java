package com.example.validation.mapper;

import com.example.validation.domain.RscItBulkGrossConsumption;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscItBulkGrossConsumptionMapper extends EntityMapper<RscItGrossConsumptionDTO, RscItBulkGrossConsumption> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    RscItGrossConsumptionDTO toDto(RscItBulkGrossConsumption rscItBulkGrossConsumption);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscItBulkGrossConsumption toEntity(RscItGrossConsumptionDTO rscItGrossConsumptionDTO);

    default RscItBulkGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItBulkGrossConsumption rscItBulkGrossConsumption = new RscItBulkGrossConsumption();
        rscItBulkGrossConsumption.setId(id);
        return rscItBulkGrossConsumption;
    }
}