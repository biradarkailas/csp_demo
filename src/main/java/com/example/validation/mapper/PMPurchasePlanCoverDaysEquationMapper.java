package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMPurchasePlanCoverDays;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscIotPMPurchasePlanCalculationsMapper.class})
public interface PMPurchasePlanCoverDaysEquationMapper extends EntityMapper<PurchasePlanCoverDaysEquationDTO, RscIotPMPurchasePlanCoverDays> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "m1Value", target = "m1StockEquationDTO.coverDays")
    @Mapping(source = "m2Value", target = "m2StockEquationDTO.coverDays")
    @Mapping(source = "m3Value", target = "m3StockEquationDTO.coverDays")
    @Mapping(source = "m4Value", target = "m4StockEquationDTO.coverDays")
    @Mapping(source = "m5Value", target = "m5StockEquationDTO.coverDays")
    @Mapping(source = "m6Value", target = "m6StockEquationDTO.coverDays")
    @Mapping(source = "m7Value", target = "m7StockEquationDTO.coverDays")
    @Mapping(source = "m8Value", target = "m8StockEquationDTO.coverDays")
    @Mapping(source = "m9Value", target = "m9StockEquationDTO.coverDays")
    @Mapping(source = "m10Value", target = "m10StockEquationDTO.coverDays")
    @Mapping(source = "m11Value", target = "m11StockEquationDTO.coverDays")
    @Mapping(source = "m12Value", target = "m12StockEquationDTO.coverDays")
    PurchasePlanCoverDaysEquationDTO toDto(RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays);

    @AfterMapping
    default void updatePMPurchasePlanCoverDaysEquationDTO(@MappingTarget PurchasePlanCoverDaysEquationDTO purchasePlanCoverDaysEquationDTO, RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays) {
        setPMPurchasePlanCoverDaysEquationDTO(purchasePlanCoverDaysEquationDTO, rscIotPMPurchasePlanCoverDays);
    }

    default void setPMPurchasePlanCoverDaysEquationDTO(@MappingTarget PurchasePlanCoverDaysEquationDTO purchasePlanCoverDaysEquationDTO, RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays) {
        purchasePlanCoverDaysEquationDTO.getM1StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth1MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM1StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth1FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM2StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth2MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM2StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth2FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM3StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth3MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM3StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth3FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM4StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth4MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM4StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth4FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM5StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth5MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM5StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth5FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM6StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth6MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM6StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth6FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM7StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth7MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM7StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth7FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM8StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth8MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM8StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth8FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM9StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth9MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM9StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth9FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM10StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth10MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM10StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth10FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM11StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth11MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM11StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth11FinalSupply());
        purchasePlanCoverDaysEquationDTO.getM12StockEquationDTO().setStock(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth12MonthEndStock());
        purchasePlanCoverDaysEquationDTO.getM12StockEquationDTO().setOrder(rscIotPMPurchasePlanCoverDays.getPmPurchasePlanCalculations().getMonth12FinalSupply());
    }

    default RscIotPMPurchasePlanCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays = new RscIotPMPurchasePlanCoverDays();
        rscIotPMPurchasePlanCoverDays.setId(id);
        return rscIotPMPurchasePlanCoverDays;
    }
}