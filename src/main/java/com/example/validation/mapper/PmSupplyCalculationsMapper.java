package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMPurchasePlanCalculations;
import com.example.validation.dto.supply.PmSupplyCalculationsDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtSupplierMapper.class, RscDtSupplierMapper.class, RscMtItemMapper.class, RscDtTechnicalSeriesMapper.class,
        RscDtMoqMapper.class, RscDtSafetyStockMapper.class, RscDtCoverDaysMapper.class, RscDtItemCodeMapper.class, RscItPMGrossConsumptionMapper.class})
public interface PmSupplyCalculationsMapper extends EntityMapper<PmSupplyCalculationsDTO, RscIotPMPurchasePlanCalculations> {

    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "materialDescription")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "materialCode")
    @Mapping(source = "rscMtItem.rscDtCoverDays.coverDays", target = "coverDays")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    @Mapping(source = "rscItItemWiseSupplier.rscDtMoq.moqValue", target = "moq")
    @Mapping(source = "rscItItemWiseSupplier.rscDtTechnicalSeries.seriesValue", target = "techSeries")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "month1MonthEndStock", target = "month1SupplyDTO.monthEndStock")
    @Mapping(source = "month1StockAfterConsumption", target = "month1SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month1ReceiptTillDate", target = "month1SupplyDTO.receiptTillDate")
    @Mapping(source = "month1ClosingStock", target = "month1SupplyDTO.closingStock")
    @Mapping(source = "month1SuggestedSupply", target = "month1SupplyDTO.suggestedSupply")
    @Mapping(source = "month1OnHandStock", target = "month1SupplyDTO.onHandStock")
    @Mapping(source = "month1PrioritySupply", target = "month1SupplyDTO.prioritySupply")
    @Mapping(source = "month1FinalSupply", target = "month1SupplyDTO.finalSupply")
    @Mapping(source = "month2MonthEndStock", target = "month2SupplyDTO.monthEndStock")
    @Mapping(source = "month2StockAfterConsumption", target = "month2SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month2ReceiptTillDate", target = "month2SupplyDTO.receiptTillDate")
    @Mapping(source = "month2ClosingStock", target = "month2SupplyDTO.closingStock")
    @Mapping(source = "month2FinalSupply", target = "month2SupplyDTO.finalSupply")
    @Mapping(source = "month3MonthEndStock", target = "month3SupplyDTO.monthEndStock")
    @Mapping(source = "month3StockAfterConsumption", target = "month3SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month3ReceiptTillDate", target = "month3SupplyDTO.receiptTillDate")
    @Mapping(source = "month3ClosingStock", target = "month3SupplyDTO.closingStock")
    @Mapping(source = "month3FinalSupply", target = "month3SupplyDTO.finalSupply")
    @Mapping(source = "month4MonthEndStock", target = "month4SupplyDTO.monthEndStock")
    @Mapping(source = "month4StockAfterConsumption", target = "month4SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month4ReceiptTillDate", target = "month4SupplyDTO.receiptTillDate")
    @Mapping(source = "month4ClosingStock", target = "month4SupplyDTO.closingStock")
    @Mapping(source = "month4FinalSupply", target = "month4SupplyDTO.finalSupply")
    @Mapping(source = "month5MonthEndStock", target = "month5SupplyDTO.monthEndStock")
    @Mapping(source = "month5StockAfterConsumption", target = "month5SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month5ReceiptTillDate", target = "month5SupplyDTO.receiptTillDate")
    @Mapping(source = "month5ClosingStock", target = "month5SupplyDTO.closingStock")
    @Mapping(source = "month5FinalSupply", target = "month5SupplyDTO.finalSupply")
    @Mapping(source = "month6MonthEndStock", target = "month6SupplyDTO.monthEndStock")
    @Mapping(source = "month6StockAfterConsumption", target = "month6SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month6ReceiptTillDate", target = "month6SupplyDTO.receiptTillDate")
    @Mapping(source = "month6ClosingStock", target = "month6SupplyDTO.closingStock")
    @Mapping(source = "month6FinalSupply", target = "month6SupplyDTO.finalSupply")
    @Mapping(source = "month7MonthEndStock", target = "month7SupplyDTO.monthEndStock")
    @Mapping(source = "month7StockAfterConsumption", target = "month7SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month7ReceiptTillDate", target = "month7SupplyDTO.receiptTillDate")
    @Mapping(source = "month7ClosingStock", target = "month7SupplyDTO.closingStock")
    @Mapping(source = "month7FinalSupply", target = "month7SupplyDTO.finalSupply")
    @Mapping(source = "month8MonthEndStock", target = "month8SupplyDTO.monthEndStock")
    @Mapping(source = "month8StockAfterConsumption", target = "month8SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month8ReceiptTillDate", target = "month8SupplyDTO.receiptTillDate")
    @Mapping(source = "month8ClosingStock", target = "month8SupplyDTO.closingStock")
    @Mapping(source = "month8FinalSupply", target = "month8SupplyDTO.finalSupply")
    @Mapping(source = "month9MonthEndStock", target = "month9SupplyDTO.monthEndStock")
    @Mapping(source = "month9StockAfterConsumption", target = "month9SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month9ReceiptTillDate", target = "month9SupplyDTO.receiptTillDate")
    @Mapping(source = "month9ClosingStock", target = "month9SupplyDTO.closingStock")
    @Mapping(source = "month9FinalSupply", target = "month9SupplyDTO.finalSupply")
    @Mapping(source = "month10MonthEndStock", target = "month10SupplyDTO.monthEndStock")
    @Mapping(source = "month10StockAfterConsumption", target = "month10SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month10ReceiptTillDate", target = "month10SupplyDTO.receiptTillDate")
    @Mapping(source = "month10ClosingStock", target = "month10SupplyDTO.closingStock")
    @Mapping(source = "month10FinalSupply", target = "month10SupplyDTO.finalSupply")
    @Mapping(source = "month11MonthEndStock", target = "month11SupplyDTO.monthEndStock")
    @Mapping(source = "month11StockAfterConsumption", target = "month11SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month11ReceiptTillDate", target = "month11SupplyDTO.receiptTillDate")
    @Mapping(source = "month11ClosingStock", target = "month11SupplyDTO.closingStock")
    @Mapping(source = "month11FinalSupply", target = "month11SupplyDTO.finalSupply")
    @Mapping(source = "month12MonthEndStock", target = "month12SupplyDTO.monthEndStock")
    @Mapping(source = "month12StockAfterConsumption", target = "month12SupplyDTO.stockAfterConsumption")
    @Mapping(source = "month12ReceiptTillDate", target = "month12SupplyDTO.receiptTillDate")
    @Mapping(source = "month12ClosingStock", target = "month12SupplyDTO.closingStock")
    @Mapping(source = "month12FinalSupply", target = "month12SupplyDTO.finalSupply")
    PmSupplyCalculationsDTO toDto(RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations);

    @AfterMapping
    default void updatePMSupplyCalculationsDTO(@MappingTarget PmSupplyCalculationsDTO pmSupplyCalculationsDTO, RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations) {
        setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO);
        setPMGrossConsumptionCurrentMonth(pmSupplyCalculationsDTO, rscIotPMPurchasePlanCalculations);
        setCoverDays(pmSupplyCalculationsDTO, rscIotPMPurchasePlanCalculations);
    }

    default void setPMGrossConsumptionCurrentMonth(@MappingTarget PmSupplyCalculationsDTO pmSupplyCalculationsDTO, RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations) {
        pmSupplyCalculationsDTO.getMonth1SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM1Value());
        pmSupplyCalculationsDTO.getMonth2SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM2Value());
        pmSupplyCalculationsDTO.getMonth3SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM3Value());
        pmSupplyCalculationsDTO.getMonth4SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM4Value());
        pmSupplyCalculationsDTO.getMonth5SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM5Value());
        pmSupplyCalculationsDTO.getMonth6SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM6Value());
        pmSupplyCalculationsDTO.getMonth7SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM7Value());
        pmSupplyCalculationsDTO.getMonth8SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM8Value());
        pmSupplyCalculationsDTO.getMonth9SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM9Value());
        pmSupplyCalculationsDTO.getMonth10SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM10Value());
        pmSupplyCalculationsDTO.getMonth11SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM11Value());
        pmSupplyCalculationsDTO.getMonth12SupplyDTO().setConsumptionAtCurrentMonth(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM12Value());
    }

    default void setCoverDays(@MappingTarget PmSupplyCalculationsDTO pmSupplyCalculationsDTO, RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations) {
        pmSupplyCalculationsDTO.getMonth1SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM1Value());
        pmSupplyCalculationsDTO.getMonth2SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM2Value());
        pmSupplyCalculationsDTO.getMonth3SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM3Value());
        pmSupplyCalculationsDTO.getMonth4SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM4Value());
        pmSupplyCalculationsDTO.getMonth5SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM5Value());
        pmSupplyCalculationsDTO.getMonth6SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM6Value());
        pmSupplyCalculationsDTO.getMonth7SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM7Value());
        pmSupplyCalculationsDTO.getMonth8SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM8Value());
        pmSupplyCalculationsDTO.getMonth9SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM9Value());
        pmSupplyCalculationsDTO.getMonth10SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM10Value());
        pmSupplyCalculationsDTO.getMonth11SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM11Value());
        pmSupplyCalculationsDTO.getMonth12SupplyDTO().setCoverDays(rscIotPMPurchasePlanCalculations.getRscIotPMPurchasePlanCoverDays().getM12Value());
    }

    default void setFinalSupplySuggestedCurrentMonth(@MappingTarget PmSupplyCalculationsDTO pmSupplyCalculationsDTO) {
        if (pmSupplyCalculationsDTO.getCoverDays() == 30) {
            pmSupplyCalculationsDTO.getMonth1SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth1SupplyDTO().getPrioritySupply());
            pmSupplyCalculationsDTO.getMonth2SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth1SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth3SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth2SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth4SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth3SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth5SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth4SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth6SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth5SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth7SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth6SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth8SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth7SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth9SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth8SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth10SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth9SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth11SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth10SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth12SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth11SupplyDTO().getFinalSupply());
        } else {
            pmSupplyCalculationsDTO.getMonth1SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth1SupplyDTO().getSuggestedSupply());
            pmSupplyCalculationsDTO.getMonth2SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth2SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth3SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth3SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth4SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth4SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth5SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth5SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth6SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth6SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth7SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth7SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth8SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth8SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth9SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth9SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth10SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth10SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth11SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth11SupplyDTO().getFinalSupply());
            pmSupplyCalculationsDTO.getMonth12SupplyDTO().setFinalSupplySuggestedCurrentMonth(pmSupplyCalculationsDTO.getMonth12SupplyDTO().getFinalSupply());
        }
    }

    default RscIotPMPurchasePlanCalculations fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations = new RscIotPMPurchasePlanCalculations();
        rscIotPMPurchasePlanCalculations.setId(id);
        return rscIotPMPurchasePlanCalculations;
    }
}