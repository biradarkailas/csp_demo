package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import com.example.validation.dto.slob.TypeWiseFGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFGTypeMapper.class})
public interface TypeWiseFGSlobSummaryDashboardDTOMapper extends EntityMapper<TypeWiseFGSlobSummaryDashboardDTO, RscIotFGTypeSlobSummary> {

    @Mapping(source = "rscDtFGType.id", target = "fgTypeId")
    @Mapping(source = "rscDtFGType.type", target = "fgType")
    @Mapping(source = "totalSlobValue", target = "slobValue")
    @Mapping(source = "totalSlowItemValue", target = "slValue")
    @Mapping(source = "totalObsoleteItemValue", target = "obValue")
    TypeWiseFGSlobSummaryDashboardDTO toDto(RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary);

    default RscIotFGTypeSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary = new RscIotFGTypeSlobSummary();
        rscIotFGTypeSlobSummary.setId(id);
        return rscIotFGTypeSlobSummary;
    }
}