package com.example.validation.mapper;

import com.example.validation.domain.RscDtDivision;
import com.example.validation.dto.RscDtDivisionDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtDivisionMapper extends EntityMapper<RscDtDivisionDTO, RscDtDivision> {

    RscDtDivisionDTO toDto(RscDtDivision rscDtDivision);

    RscDtDivision toEntity(RscDtDivisionDTO rscDtDivisionDTO);

    default RscDtDivision fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtDivision rscDtDivision = new RscDtDivision();
        rscDtDivision.setId(id);
        return rscDtDivision;
    }
}
