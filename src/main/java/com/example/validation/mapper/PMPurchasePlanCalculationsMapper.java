package com.example.validation.mapper;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.domain.RscIotPMPurchasePlanCalculations;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscItItemWiseSupplierMapper.class, RscMtSupplierMapper.class})
public interface PMPurchasePlanCalculationsMapper extends EntityMapper<RscIotPMPurchasePlanDTO, RscIotPMPurchasePlanCalculations> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.launchDate", target = "launchDate")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscMtItem.rscDtCoverDays.coverDays", target = "coverDays")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.applicableFrom", target = "applicableFrom")
    @Mapping(source = "rscItItemWiseSupplier.applicableTo", target = "applicableTo")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.id", target = "rscMtSupplierId")
    @Mapping(source = "month1PrioritySupply", target = "m1Value")
    @Mapping(source = "month1FinalSupply", target = "m2Value")
    @Mapping(source = "month2FinalSupply", target = "m3Value")
    @Mapping(source = "month3FinalSupply", target = "m4Value")
    @Mapping(source = "month4FinalSupply", target = "m5Value")
    @Mapping(source = "month5FinalSupply", target = "m6Value")
    @Mapping(source = "month6FinalSupply", target = "m7Value")
    @Mapping(source = "month7FinalSupply", target = "m8Value")
    @Mapping(source = "month8FinalSupply", target = "m9Value")
    @Mapping(source = "month9FinalSupply", target = "m10Value")
    @Mapping(source = "month10FinalSupply", target = "m11Value")
    @Mapping(source = "month11FinalSupply", target = "m12Value")
    @Mapping(source = "month1ReceiptTillDate", target = "rtdValue")
    RscIotPMPurchasePlanDTO toDto(RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations);

    @AfterMapping
    default void updateRscIotPMPurchasePlanDTO(@MappingTarget RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO, RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations) {
        if (!rscIotPMPurchasePlanDTO.getCoverDays().equals(ApplicationConstants.THIRTY)) {
            rscIotPMPurchasePlanDTO.setM1Value(rscIotPMPurchasePlanCalculations.getMonth1FinalSupply());
            rscIotPMPurchasePlanDTO.setM2Value(rscIotPMPurchasePlanCalculations.getMonth2FinalSupply());
            rscIotPMPurchasePlanDTO.setM3Value(rscIotPMPurchasePlanCalculations.getMonth3FinalSupply());
            rscIotPMPurchasePlanDTO.setM4Value(rscIotPMPurchasePlanCalculations.getMonth4FinalSupply());
            rscIotPMPurchasePlanDTO.setM5Value(rscIotPMPurchasePlanCalculations.getMonth5FinalSupply());
            rscIotPMPurchasePlanDTO.setM6Value(rscIotPMPurchasePlanCalculations.getMonth6FinalSupply());
            rscIotPMPurchasePlanDTO.setM7Value(rscIotPMPurchasePlanCalculations.getMonth7FinalSupply());
            rscIotPMPurchasePlanDTO.setM8Value(rscIotPMPurchasePlanCalculations.getMonth8FinalSupply());
            rscIotPMPurchasePlanDTO.setM9Value(rscIotPMPurchasePlanCalculations.getMonth9FinalSupply());
            rscIotPMPurchasePlanDTO.setM10Value(rscIotPMPurchasePlanCalculations.getMonth10FinalSupply());
            rscIotPMPurchasePlanDTO.setM11Value(rscIotPMPurchasePlanCalculations.getMonth11FinalSupply());
            rscIotPMPurchasePlanDTO.setM12Value(rscIotPMPurchasePlanCalculations.getMonth12FinalSupply());
        }
    }

    default RscIotPMPurchasePlanCalculations fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations = new RscIotPMPurchasePlanCalculations();
        rscIotPMPurchasePlanCalculations.setId(id);
        return rscIotPMPurchasePlanCalculations;
    }
}