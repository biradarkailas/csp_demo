package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMOriginalPurchasePlan;
import com.example.validation.dto.supply.OriginalRMPurchasePlanDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class})
public interface OriginalRMPurchasePlanMapper extends EntityMapper<OriginalRMPurchasePlanDTO, RscIotRMOriginalPurchasePlan> {

    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    @Mapping(source = "rscMtItem.id", target = "materialId")
    @Mapping(source = "m1Value", target = "month1.value")
    @Mapping(source = "m2Value", target = "month2.value")
    @Mapping(source = "m3Value", target = "month3.value")
    @Mapping(source = "m4Value", target = "month4.value")
    @Mapping(source = "m5Value", target = "month5.value")
    @Mapping(source = "m6Value", target = "month6.value")
    @Mapping(source = "m7Value", target = "month7.value")
    @Mapping(source = "m8Value", target = "month8.value")
    @Mapping(source = "m9Value", target = "month9.value")
    @Mapping(source = "m10Value", target = "month10.value")
    @Mapping(source = "m11Value", target = "month11.value")
    @Mapping(source = "m12Value", target = "month12.value")
    @Mapping(target = "totalValue", ignore = true)
    OriginalRMPurchasePlanDTO toDto(RscIotRMOriginalPurchasePlan rscIotRMOriginalPurchasePlan);

    @AfterMapping
    default void calculateTotalValue(@MappingTarget OriginalRMPurchasePlanDTO originalRMPurchasePlanDTO) {
        originalRMPurchasePlanDTO.setTotalValue(getTotalValue(originalRMPurchasePlanDTO));
    }

    private Double getTotalValue(OriginalRMPurchasePlanDTO originalRMPurchasePlanDTO) {
        return originalRMPurchasePlanDTO.getMonth1().getValue()
                + originalRMPurchasePlanDTO.getMonth2().getValue()
                + originalRMPurchasePlanDTO.getMonth3().getValue()
                + originalRMPurchasePlanDTO.getMonth4().getValue()
                + originalRMPurchasePlanDTO.getMonth5().getValue()
                + originalRMPurchasePlanDTO.getMonth6().getValue()
                + originalRMPurchasePlanDTO.getMonth7().getValue()
                + originalRMPurchasePlanDTO.getMonth8().getValue()
                + originalRMPurchasePlanDTO.getMonth9().getValue()
                + originalRMPurchasePlanDTO.getMonth10().getValue()
                + originalRMPurchasePlanDTO.getMonth11().getValue()
                + originalRMPurchasePlanDTO.getMonth12().getValue();
    }

    default RscIotRMOriginalPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMOriginalPurchasePlan rscIotRMOriginalPurchasePlan = new RscIotRMOriginalPurchasePlan();
        rscIotRMOriginalPurchasePlan.setId(id);
        return rscIotRMOriginalPurchasePlan;
    }
}
