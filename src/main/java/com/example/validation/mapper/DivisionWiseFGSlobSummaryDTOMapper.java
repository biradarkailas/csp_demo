package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import com.example.validation.dto.slob.DivisionWiseFGSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtDivisionMapper.class})
public interface DivisionWiseFGSlobSummaryDTOMapper extends EntityMapper<DivisionWiseFGSlobSummaryDTO, RscIotFGTypeDivisionSlobSummary> {

    @Mapping(source = "rscDtDivision.id", target = "rscDtDivisionId")
    @Mapping(source = "rscDtDivision.aliasName", target = "divisionName")
    @Mapping(source = "totalStockValue", target = "stockValue")
    @Mapping(source = "totalSlobValue", target = "slobValue")
    @Mapping(source = "totalSlowItemValue", target = "slValue")
    @Mapping(source = "totalObsoleteItemValue", target = "obValue")
    DivisionWiseFGSlobSummaryDTO toDto(RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary);

    default RscIotFGTypeDivisionSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary = new RscIotFGTypeDivisionSlobSummary();
        rscIotFGTypeDivisionSlobSummary.setId(id);
        return rscIotFGTypeDivisionSlobSummary;
    }
}