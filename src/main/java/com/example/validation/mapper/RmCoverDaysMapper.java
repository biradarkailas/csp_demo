package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDays;
import com.example.validation.dto.RmCoverDaysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class, RscDtItemTypeMapper.class, RscItRMGrossConsumptionMapper.class})
public interface RmCoverDaysMapper extends EntityMapper<RmCoverDaysDTO, RscIotRmCoverDays> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "rscMtItem.rscDtItemType.name", target = "category")
    @Mapping(source = "rscItRMGrossConsumption.m1Value", target = "coverDaysMonthDetailsDTO.month1ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m2Value", target = "coverDaysMonthDetailsDTO.month2ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m3Value", target = "coverDaysMonthDetailsDTO.month3ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m4Value", target = "coverDaysMonthDetailsDTO.month4ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m5Value", target = "coverDaysMonthDetailsDTO.month5ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m6Value", target = "coverDaysMonthDetailsDTO.month6ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m7Value", target = "coverDaysMonthDetailsDTO.month7ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m8Value", target = "coverDaysMonthDetailsDTO.month8ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m9Value", target = "coverDaysMonthDetailsDTO.month9ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m10Value", target = "coverDaysMonthDetailsDTO.month10ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m11Value", target = "coverDaysMonthDetailsDTO.month11ConsumptionQuantity")
    @Mapping(source = "rscItRMGrossConsumption.m12Value", target = "coverDaysMonthDetailsDTO.month12ConsumptionQuantity")
    @Mapping(source = "month1ConsumptionValue", target = "coverDaysMonthDetailsDTO.month1Value")
    @Mapping(source = "month2ConsumptionValue", target = "coverDaysMonthDetailsDTO.month2Value")
    @Mapping(source = "month3ConsumptionValue", target = "coverDaysMonthDetailsDTO.month3Value")
    @Mapping(source = "month4ConsumptionValue", target = "coverDaysMonthDetailsDTO.month4Value")
    @Mapping(source = "month5ConsumptionValue", target = "coverDaysMonthDetailsDTO.month5Value")
    @Mapping(source = "month6ConsumptionValue", target = "coverDaysMonthDetailsDTO.month6Value")
    @Mapping(source = "month7ConsumptionValue", target = "coverDaysMonthDetailsDTO.month7Value")
    @Mapping(source = "month8ConsumptionValue", target = "coverDaysMonthDetailsDTO.month8Value")
    @Mapping(source = "month9ConsumptionValue", target = "coverDaysMonthDetailsDTO.month9Value")
    @Mapping(source = "month10ConsumptionValue", target = "coverDaysMonthDetailsDTO.month10Value")
    @Mapping(source = "month11ConsumptionValue", target = "coverDaysMonthDetailsDTO.month11Value")
    @Mapping(source = "month12ConsumptionValue", target = "coverDaysMonthDetailsDTO.month12Value")
    RmCoverDaysDTO toDto(RscIotRmCoverDays rscIotRmCoverDays);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscItRMGrossConsumptionId", target = "rscItRMGrossConsumption")
    @Mapping(source = "coverDaysMonthDetailsDTO.month1Value", target = "month1ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month2Value", target = "month2ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month3Value", target = "month3ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month4Value", target = "month4ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month5Value", target = "month5ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month6Value", target = "month6ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month7Value", target = "month7ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month8Value", target = "month8ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month9Value", target = "month9ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month10Value", target = "month10ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month11Value", target = "month11ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month12Value", target = "month12ConsumptionValue")
    RscIotRmCoverDays toEntity(RmCoverDaysDTO rmCoverDaysDTO);

    default RscIotRmCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDays rscIotRmCoverDays = new RscIotRmCoverDays();
        rscIotRmCoverDays.setId(id);
        return rscIotRmCoverDays;
    }
}
