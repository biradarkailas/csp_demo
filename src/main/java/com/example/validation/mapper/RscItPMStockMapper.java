package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMStock;
import com.example.validation.dto.RscItPMStockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtStockTypeMapper.class, RscDtMapMapper.class})
public interface RscItPMStockMapper extends EntityMapper<RscItPMStockDTO, RscIotPMStock> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtStockType.id", target = "rscDtStockTypeId")
    @Mapping(source = "rscDtStockType.aliasName", target = "stockTypeAlias")
    @Mapping(source = "rscDtStockType.tag", target = "stockTypeTag")
    @Mapping(source = "rscMtItem.rscDtMap.mapPrice", target = "mapPrice")
    @Mapping(source = "rscMtItem.rscDtStdPrice.stdPrice", target = "stdPrice")
    RscItPMStockDTO toDto(RscIotPMStock rscIotPMStock);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscDtStockTypeId", target = "rscDtStockType")
    RscIotPMStock toEntity(RscItPMStockDTO rscItPMStockDTO);

    default RscIotPMStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMStock rscIotPMStock = new RscIotPMStock();
        rscIotPMStock.setId(id);
        return rscIotPMStock;
    }
}