package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmBulkOpRm;
import com.example.validation.dto.RscMtRmBulkOpRmDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtStockTypeMapper.class})
public interface RscMtRmBulkOpRmMapper extends EntityMapper<RscMtRmBulkOpRmDTO, RscMtRmBulkOpRm> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    RscMtRmBulkOpRmDTO toDto(RscMtRmBulkOpRm rscMtRmBulkOpRm);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscMtRmBulkOpRm toEntity(RscMtRmBulkOpRmDTO rscMtRmBulkOpRmDTO);

    default RscMtRmBulkOpRm fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmBulkOpRm rscMtRmBulkOpRm = new RscMtRmBulkOpRm();
        rscMtRmBulkOpRm.setId(id);
        return rscMtRmBulkOpRm;
    }
}