package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmOtherStocks;
import com.example.validation.dto.IndividualStockTypeValueDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface IndividualOtherStockMapper extends EntityMapper<IndividualStockTypeValueDTO, RscMtRmOtherStocks> {

    IndividualStockTypeValueDTO toDto(RscMtRmOtherStocks rscMtRmOtherStocks);
}
