package com.example.validation.mapper;

import com.example.validation.domain.RscDtReason;
import com.example.validation.dto.RscDtReasonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtReasonTypeMapper.class})
public interface RscDtReasonMapper extends EntityMapper<RscDtReasonDTO, RscDtReason> {

    @Mapping(source = "rscDtReasonType.id", target = "rscDtReasonTypeId")
    @Mapping(source = "rscDtReasonType.name", target = "reasonType")
    RscDtReasonDTO toDto(RscDtReason rscDtReason);

    @Mapping(source = "rscDtReasonTypeId", target = "rscDtReasonType")
    RscDtReason toEntity(RscDtReasonDTO rscDtReasonDTO);

    default RscDtReason fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtReason rscDtReason = new RscDtReason();
        rscDtReason.setId(id);
        return rscDtReason;
    }
}

