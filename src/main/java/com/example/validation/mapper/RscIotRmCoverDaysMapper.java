package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDays;
import com.example.validation.dto.RscIotRmCoverDaysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscItRMGrossConsumptionMapper.class, RscItItemWiseSupplierMapper.class})
public interface RscIotRmCoverDaysMapper extends EntityMapper<RscIotRmCoverDaysDTO, RscIotRmCoverDays> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    RscIotRmCoverDaysDTO toDto(RscIotRmCoverDays rscIotRmCoverDays);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscItRMGrossConsumptionId", target = "rscItRMGrossConsumption")
    RscIotRmCoverDays toEntity(RscIotRmCoverDaysDTO rscIotRmCoverDaysDTO);

    default RscIotRmCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDays rscIotRmCoverDays = new RscIotRmCoverDays();
        rscIotRmCoverDays.setId(id);
        return rscIotRmCoverDays;
    }
}
