package com.example.validation.mapper;

import com.example.validation.domain.RscItItemWiseSupplier;
import com.example.validation.dto.RscItItemWiseSupplierDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtItemCodeMapper.class, RscDtTechnicalSeriesMapper.class, RscDtMoqMapper.class, RscMtItemMapper.class, RscMtSupplierMapper.class, RscDtContainerTypeMapper.class})
public interface RscItItemWiseSupplierMapper extends EntityMapper<RscItItemWiseSupplierDTO, RscItItemWiseSupplier> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtTechnicalSeries.id", target = "rscDtTechnicalSeriesId")
    @Mapping(source = "rscDtTechnicalSeries.seriesValue", target = "seriesValue")
    @Mapping(source = "rscDtMoq.id", target = "rscDtMoqId")
    @Mapping(source = "rscDtMoq.moqValue", target = "moqValue")
    @Mapping(source = "rscMtSupplier.id", target = "rscMtSupplierId")
    @Mapping(source = "rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscMtSupplier.rscDtSupplier.supplierCode", target = "supplierCode")
    @Mapping(source = "rscDtContainerType.id", target = "rscDtContainerTypeId")
    @Mapping(source = "rscDtContainerType.typeName", target = "containerType")
    RscItItemWiseSupplierDTO toDto(RscItItemWiseSupplier rscItItemWiseSupplier);

    RscItItemWiseSupplier toEntity(RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO);

    default RscItItemWiseSupplier fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItItemWiseSupplier rscItItemWiseSupplier = new RscItItemWiseSupplier();
        rscItItemWiseSupplier.setId(id);
        return rscItItemWiseSupplier;
    }
}
