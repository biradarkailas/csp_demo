package com.example.validation.mapper;

import com.example.validation.domain.RscIitRmBulkOpRm;
import com.example.validation.dto.RscIitBulkRMStockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface RscIitBulkRmStockMapper extends EntityMapper<RscIitBulkRMStockDTO, RscIitRmBulkOpRm> {
    @Mapping(source = "quantity", target = "rmQuantity")
    @Mapping(source = "rscMtBulkItem.id", target = "bulkItemId")
    @Mapping(source = "rscMtBulkItem.rscDtItemCode.code", target = "bulkItemCode")
    @Mapping(source = "rscMtBulkItem.rscDtItemCode.description", target = "bulkItemDescription")
    @Mapping(source = "rscMtRmItem.id", target = "rmItemId")
    @Mapping(source = "rscMtRmItem.rscDtItemCode.code", target = "rmItemCode")
    @Mapping(source = "rscMtRmItem.rscDtItemCode.description", target = "rmItemDescription")
    RscIitBulkRMStockDTO toDto(RscIitRmBulkOpRm rscIitRmBulkOpRm);

    default RscIitRmBulkOpRm fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitRmBulkOpRm rscIitRmBulkOpRm = new RscIitRmBulkOpRm();
        rscIitRmBulkOpRm.setId(id);
        return rscIitRmBulkOpRm;
    }
}
