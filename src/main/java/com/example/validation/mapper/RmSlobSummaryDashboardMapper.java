package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmSlobSummary;
import com.example.validation.dto.SlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RmSlobSummaryDashboardMapper extends EntityMapper<SlobSummaryDashboardDTO, RscIotRmSlobSummary> {
    @Mapping(source = "totalObsoleteItemValue", target = "obValue")
    @Mapping(source = "totalSlowItemValue", target = "slValue")
    @Mapping(source = "stockQualityValue", target = "inventoryQualityValue")
    @Mapping(source = "obsoleteItemPercentage", target = "obPercentage")
    @Mapping(source = "slowItemPercentage", target = "slPercentage")
    @Mapping(source = "stockQualityPercentage", target = "iqPercentage")
    SlobSummaryDashboardDTO toDto(RscIotRmSlobSummary rscIotRmSlobSummary);
}
