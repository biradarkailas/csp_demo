package com.example.validation.mapper;

import com.example.validation.domain.ResourceKeyValue;
import com.example.validation.dto.ResourceKeyValueDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ResourceKeyValueMapper extends EntityMapper<ResourceKeyValueDTO, ResourceKeyValue> {
    ResourceKeyValueDTO toDto(ResourceKeyValue resourceKeyValue);
}