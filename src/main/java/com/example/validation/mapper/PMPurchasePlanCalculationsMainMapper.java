package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMPurchasePlanCalculations;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscItPMGrossConsumptionMapper.class, RscMtItemMapper.class, RscDtCoverDaysMapper.class})
public interface PMPurchasePlanCalculationsMainMapper extends EntityMapper<PurchasePlanCalculationsMainDTO, RscIotPMPurchasePlanCalculations> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtCoverDays.coverDays", target = "coverDays")
    @Mapping(source = "month1MonthEndStock", target = "month1SupplyDTO.monthEndStock")
    @Mapping(source = "month1FinalSupply", target = "month1SupplyDTO.finalSupply")
    @Mapping(source = "month1PrioritySupply", target = "month1SupplyDTO.priority")
    @Mapping(source = "month2MonthEndStock", target = "month2SupplyDTO.monthEndStock")
    @Mapping(source = "month2FinalSupply", target = "month2SupplyDTO.finalSupply")
    @Mapping(source = "month3MonthEndStock", target = "month3SupplyDTO.monthEndStock")
    @Mapping(source = "month3FinalSupply", target = "month3SupplyDTO.finalSupply")
    @Mapping(source = "month4MonthEndStock", target = "month4SupplyDTO.monthEndStock")
    @Mapping(source = "month4FinalSupply", target = "month4SupplyDTO.finalSupply")
    @Mapping(source = "month5MonthEndStock", target = "month5SupplyDTO.monthEndStock")
    @Mapping(source = "month5FinalSupply", target = "month5SupplyDTO.finalSupply")
    @Mapping(source = "month6MonthEndStock", target = "month6SupplyDTO.monthEndStock")
    @Mapping(source = "month6FinalSupply", target = "month6SupplyDTO.finalSupply")
    @Mapping(source = "month7MonthEndStock", target = "month7SupplyDTO.monthEndStock")
    @Mapping(source = "month7FinalSupply", target = "month7SupplyDTO.finalSupply")
    @Mapping(source = "month8MonthEndStock", target = "month8SupplyDTO.monthEndStock")
    @Mapping(source = "month8FinalSupply", target = "month8SupplyDTO.finalSupply")
    @Mapping(source = "month9MonthEndStock", target = "month9SupplyDTO.monthEndStock")
    @Mapping(source = "month9FinalSupply", target = "month9SupplyDTO.finalSupply")
    @Mapping(source = "month10MonthEndStock", target = "month10SupplyDTO.monthEndStock")
    @Mapping(source = "month10FinalSupply", target = "month10SupplyDTO.finalSupply")
    @Mapping(source = "month11MonthEndStock", target = "month11SupplyDTO.monthEndStock")
    @Mapping(source = "month11FinalSupply", target = "month11SupplyDTO.finalSupply")
    @Mapping(source = "month12MonthEndStock", target = "month12SupplyDTO.monthEndStock")
    @Mapping(source = "month12FinalSupply", target = "month12SupplyDTO.finalSupply")
    PurchasePlanCalculationsMainDTO toDto(RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations);

    @AfterMapping
    default void updatePurchasePlanCalculationsMainDTO(@MappingTarget PurchasePlanCalculationsMainDTO purchasePlanCalculationsMainDTO, RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations) {
        updateRMPurchasePlanCalculationsMainDTO(purchasePlanCalculationsMainDTO, rscIotPMPurchasePlanCalculations);
        setFinalSupplySuggestedCurrentMonth(purchasePlanCalculationsMainDTO);
    }

    default void updateRMPurchasePlanCalculationsMainDTO(@MappingTarget PurchasePlanCalculationsMainDTO purchasePlanCalculationsMainDTO, RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations) {
        purchasePlanCalculationsMainDTO.getMonth1SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM1Value());
        purchasePlanCalculationsMainDTO.getMonth2SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM2Value());
        purchasePlanCalculationsMainDTO.getMonth3SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM3Value());
        purchasePlanCalculationsMainDTO.getMonth4SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM4Value());
        purchasePlanCalculationsMainDTO.getMonth5SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM5Value());
        purchasePlanCalculationsMainDTO.getMonth6SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM6Value());
        purchasePlanCalculationsMainDTO.getMonth7SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM7Value());
        purchasePlanCalculationsMainDTO.getMonth8SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM8Value());
        purchasePlanCalculationsMainDTO.getMonth9SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM9Value());
        purchasePlanCalculationsMainDTO.getMonth10SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM10Value());
        purchasePlanCalculationsMainDTO.getMonth11SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM11Value());
        purchasePlanCalculationsMainDTO.getMonth12SupplyDTO().setConsumption(rscIotPMPurchasePlanCalculations.getRscItPMGrossConsumption().getM12Value());
    }

    default void setFinalSupplySuggestedCurrentMonth(@MappingTarget PurchasePlanCalculationsMainDTO purchasePlanCalculationsMainDTO) {
        if (purchasePlanCalculationsMainDTO.getCoverDays() == 30) {
            purchasePlanCalculationsMainDTO.getMonth12SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth11SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth11SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth10SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth10SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth9SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth9SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth8SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth8SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth7SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth7SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth6SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth6SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth5SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth5SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth4SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth4SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth3SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth3SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth2SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth2SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth1SupplyDTO().getFinalSupply());
            purchasePlanCalculationsMainDTO.getMonth1SupplyDTO().setFinalSupply(purchasePlanCalculationsMainDTO.getMonth1SupplyDTO().getPriority());
        }
    }

    default RscIotPMPurchasePlanCalculations fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMPurchasePlanCalculations rscIotPMPurchasePlanCalculations = new RscIotPMPurchasePlanCalculations();
        rscIotPMPurchasePlanCalculations.setId(id);
        return rscIotPMPurchasePlanCalculations;
    }
}