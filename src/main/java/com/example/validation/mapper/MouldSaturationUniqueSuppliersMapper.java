package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestMouldSaturation;
import com.example.validation.dto.RscDtSupplierDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class, RscIotPMLatestPurchasePlanMapper.class, RscItItemWiseSupplierMapper.class, RscMtSupplierMapper.class, RscDtSupplierMapper.class})
public interface MouldSaturationUniqueSuppliersMapper extends EntityMapper<RscDtSupplierDTO, RscIotLatestMouldSaturation> {
    @Mapping(source = "rscIotPMLatestPurchasePlan.rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierCode", target = "supplierCode")
    @Mapping(source = "rscIotPMLatestPurchasePlan.rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscIotPMLatestPurchasePlan.rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.id", target = "id")
    RscDtSupplierDTO toDto(RscIotLatestMouldSaturation rscIotLatestMouldSaturation);

    default RscIotLatestMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestMouldSaturation rscIotLatestMouldSaturation = new RscIotLatestMouldSaturation();
        rscIotLatestMouldSaturation.setId(id);
        return rscIotLatestMouldSaturation;
    }
}
