package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysClassASummary;
import com.example.validation.dto.RscIotRmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysClassASummaryMapper extends EntityMapper<RscIotRmCoverDaysSummaryDTO, RscIotRmCoverDaysClassASummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    RscIotRmCoverDaysSummaryDTO toDto(RscIotRmCoverDaysClassASummary rscIotRmCoverDaysClassASummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    RscIotRmCoverDaysClassASummary toEntity(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO);

    default RscIotRmCoverDaysClassASummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysClassASummary rscIotRmCoverDaysClassASummary = new RscIotRmCoverDaysClassASummary();
        rscIotRmCoverDaysClassASummary.setId(id);
        return rscIotRmCoverDaysClassASummary;
    }
}