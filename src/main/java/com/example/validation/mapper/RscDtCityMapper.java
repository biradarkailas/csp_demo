package com.example.validation.mapper;

import com.example.validation.domain.RscDtCity;
import com.example.validation.dto.RscDtCityDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtCityMapper extends EntityMapper<RscDtCityDTO, RscDtCity> {
    RscDtCityDTO toDto(RscDtCity rscDtCity);

    RscDtCity toEntity(RscDtCityDTO rscDtCityDTO);

    default RscDtCity fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtCity rscDtCity = new RscDtCity();
        rscDtCity.setId(id);
        return rscDtCity;
    }

}
