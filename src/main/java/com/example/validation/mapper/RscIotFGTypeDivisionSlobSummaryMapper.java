package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import com.example.validation.dto.slob.RscIotFGTypeDivisionSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtDivisionMapper.class, RscDtFGTypeMapper.class})
public interface RscIotFGTypeDivisionSlobSummaryMapper extends EntityMapper<RscIotFGTypeDivisionSlobSummaryDTO, RscIotFGTypeDivisionSlobSummary> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtDivision.id", target = "rscDtDivisionId")
    @Mapping(source = "rscDtDivision.aliasName", target = "divisionName")
    @Mapping(source = "rscDtFGType.id", target = "fgTypeId")
    @Mapping(source = "rscDtFGType.type", target = "fgType")
    RscIotFGTypeDivisionSlobSummaryDTO toDto(RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary);

    @Mapping(source = "fgTypeId", target = "rscDtFGType")
    @Mapping(source = "rscDtDivisionId", target = "rscDtDivision")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    RscIotFGTypeDivisionSlobSummary toEntity(RscIotFGTypeDivisionSlobSummaryDTO rscIotFGTypeDivisionSlobSummaryDTO);

    default RscIotFGTypeDivisionSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary = new RscIotFGTypeDivisionSlobSummary();
        rscIotFGTypeDivisionSlobSummary.setId(id);
        return rscIotFGTypeDivisionSlobSummary;
    }
}