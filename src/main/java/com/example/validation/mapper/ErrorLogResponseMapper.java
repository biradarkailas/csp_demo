package com.example.validation.mapper;

import com.example.validation.domain.ErrorLog;
import com.example.validation.dto.ErrorLogResponseDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ErrorLogResponseMapper extends EntityMapper<ErrorLogResponseDTO, ErrorLog> {
    ErrorLogResponseDTO toDto(ErrorLog errorLog);
}
