package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGStock;
import com.example.validation.dto.RscIotFGStockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtStockTypeMapper.class, RscDtMapMapper.class, RscDtItemCategoryMapper.class, RscDtItemCodeMapper.class})
public interface RscIotFGStockMapper extends EntityMapper<RscIotFGStockDTO, RscIotFGStock> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscDtStockType.id", target = "rscDtStockTypeId")
    @Mapping(source = "rscDtStockType.aliasName", target = "stockTypeAlias")
    @Mapping(source = "rscDtStockType.tag", target = "stockTypeTag")
    @Mapping(source = "rscMtItem.rscDtMap.mapPrice", target = "mapPrice")
    @Mapping(source = "rscMtItem.rscDtItemCategory.description", target = "itemCategoryDescription")
    RscIotFGStockDTO toDto(RscIotFGStock rscIotFGStock);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscDtStockTypeId", target = "rscDtStockType")
    RscIotFGStock toEntity(RscIotFGStockDTO rscIotFGStockDTO);

    default RscIotFGStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGStock rscIotFGStock = new RscIotFGStock();
        rscIotFGStock.setId(id);
        return rscIotFGStock;
    }
}