package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmSlob;
import com.example.validation.dto.RmSlobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class,RscDtReasonMapper.class, RscItItemWiseSupplierMapper.class, RscMtSupplierMapper.class,RscItRMGrossConsumptionMapper.class , RscDtSupplierMapper.class})
public interface RmSlobMapper extends EntityMapper<RmSlobDTO, RscIotRmSlob> {
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplier")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "stockQuantity", target = "midNightStock")
    @Mapping(source = "expiryStockQuantity", target = "expiredStock")
    @Mapping(source = "totalStockQuantity", target = "finalStock")
    @Mapping(source = "mapPrice", target = "map")
    @Mapping(source = "expirySlobValue", target = "obsoleteExpiryValue")
    @Mapping(source = "obSlobValue", target = "obsoleteNonExpiryValue")
    @Mapping(source = "slowSlobValue", target = "slowMovingValue")
    @Mapping(source = "rscDtReason.id", target = "reasonId")
    @Mapping(source = "rscDtReason.reason", target = "reason")
    @Mapping(source = "rscItRMGrossConsumption.m1Value", target = "monthValuesDTO.month1Value")
    @Mapping(source = "rscItRMGrossConsumption.m2Value", target = "monthValuesDTO.month2Value")
    @Mapping(source = "rscItRMGrossConsumption.m3Value", target = "monthValuesDTO.month3Value")
    @Mapping(source = "rscItRMGrossConsumption.m4Value", target = "monthValuesDTO.month4Value")
    @Mapping(source = "rscItRMGrossConsumption.m5Value", target = "monthValuesDTO.month5Value")
    @Mapping(source = "rscItRMGrossConsumption.m6Value", target = "monthValuesDTO.month6Value")
    @Mapping(source = "rscItRMGrossConsumption.m7Value", target = "monthValuesDTO.month7Value")
    @Mapping(source = "rscItRMGrossConsumption.m8Value", target = "monthValuesDTO.month8Value")
    @Mapping(source = "rscItRMGrossConsumption.m9Value", target = "monthValuesDTO.month9Value")
    @Mapping(source = "rscItRMGrossConsumption.m10Value", target = "monthValuesDTO.month10Value")
    @Mapping(source = "rscItRMGrossConsumption.m11Value", target = "monthValuesDTO.month11Value")
    @Mapping(source = "rscItRMGrossConsumption.m12Value", target = "monthValuesDTO.month12Value")

    RmSlobDTO toDto(RscIotRmSlob rscIotRmSlob);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "finalStock", target = "totalStockQuantity")
    @Mapping(source = "midNightStock", target = "stockQuantity")
    @Mapping(source = "expiredStock", target = "expiryStockQuantity")
    @Mapping(source = "map", target = "mapPrice")
    @Mapping(source = "obsoleteExpiryValue", target = "expirySlobValue")
    @Mapping(source = "obsoleteNonExpiryValue", target = "obSlobValue")
    @Mapping(source = "slowMovingValue", target = "slowSlobValue")
    @Mapping(source = "reasonId", target = "rscDtReason")
    @Mapping(source = "rscItRMGrossConsumptionId", target = "rscItRMGrossConsumption")
    RscIotRmSlob toEntity(RmSlobDTO rmSlobDTO);

    default RscIotRmSlob fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmSlob rscIotRmSlob = new RscIotRmSlob();
        rscIotRmSlob.setId(id);
        return rscIotRmSlob;
    }
}
