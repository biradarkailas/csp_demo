package com.example.validation.mapper;

import com.example.validation.domain.RscDtSafetyStock;
import com.example.validation.dto.RscDtSafetyStockDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtSafetyStockMapper extends EntityMapper<RscDtSafetyStockDTO, RscDtSafetyStock> {

    RscDtSafetyStockDTO toDto(RscDtSafetyStock rscDtSafetyStock);

    RscDtSafetyStock toEntity(RscDtSafetyStockDTO rscDtSafetyStockDTO);

    default RscDtSafetyStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtSafetyStock rscDtSafetyStock = new RscDtSafetyStock();
        rscDtSafetyStock.setId(id);
        return rscDtSafetyStock;
    }
}