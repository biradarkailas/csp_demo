package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGSlobSummary;
import com.example.validation.dto.slob.RscIotFGSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotFGSlobSummaryMapper extends EntityMapper<RscIotFGSlobSummaryDTO, RscIotFGSlobSummary> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    RscIotFGSlobSummaryDTO toDto(RscIotFGSlobSummary rscIotFGSlobSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    RscIotFGSlobSummary toEntity(RscIotFGSlobSummaryDTO rscIotFGSlobSummaryDTO);

    default RscIotFGSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGSlobSummary rscIotFGSlobSummary = new RscIotFGSlobSummary();
        rscIotFGSlobSummary.setId(id);
        return rscIotFGSlobSummary;
    }
}