package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmList;
import com.example.validation.dto.RscMtRmListDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtRmListMapper extends EntityMapper<RscMtRmListDTO, RscMtRmList> {

    RscMtRmListDTO toDto(RscMtRmList rscMtRmList);

    RscMtRmList toEntity(RscMtRmListDTO rscMtRmListDTO);

    default RscMtRmList fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmList rscMtRmList = new RscMtRmList();
        rscMtRmList.setId(id);
        return rscMtRmList;
    }
}
