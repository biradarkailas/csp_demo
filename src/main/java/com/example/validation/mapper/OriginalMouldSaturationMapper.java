package com.example.validation.mapper;

import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.dto.LatestPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class, RscMtPPheaderMapper.class, RscIotPMOriginalPurchasePlanMapper.class, RscIotPMLatestPurchasePlanMapper.class})
public interface OriginalMouldSaturationMapper extends EntityMapper<LatestPurchasePlanDTO, RscIotOriginalMouldSaturation> {
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "id", target = "rscIotPMOriginalPurchasePlan")
    @Mapping(source = "mouldId", target = "rscDtMould")
    RscIotOriginalMouldSaturation toEntity(LatestPurchasePlanDTO latestPurchasePlanDTO);

    default RscIotOriginalMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotOriginalMouldSaturation rscIotOriginalMouldSaturation = new RscIotOriginalMouldSaturation();
        rscIotOriginalMouldSaturation.setId(id);
        return rscIotOriginalMouldSaturation;
    }
}
