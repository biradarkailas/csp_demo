package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysClassCSummary;
import com.example.validation.dto.RscIotRmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysClassCSummaryMapper extends EntityMapper<RscIotRmCoverDaysSummaryDTO, RscIotRmCoverDaysClassCSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    RscIotRmCoverDaysSummaryDTO toDto(RscIotRmCoverDaysClassCSummary rscIotRmCoverDaysClassCSummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    RscIotRmCoverDaysClassCSummary toEntity(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO);

    default RscIotRmCoverDaysClassCSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysClassCSummary rscIotRmCoverDaysClassCSummary = new RscIotRmCoverDaysClassCSummary();
        rscIotRmCoverDaysClassCSummary.setId(id);
        return rscIotRmCoverDaysClassCSummary;
    }
}