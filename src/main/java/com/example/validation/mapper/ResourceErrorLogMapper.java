package com.example.validation.mapper;

import com.example.validation.domain.ResourceErrorLog;
import com.example.validation.dto.ResourceErrorLogDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ResourceErrorLogMapper extends EntityMapper<ResourceErrorLogDTO, ResourceErrorLog> {
    ResourceErrorLogDTO toDto(ResourceErrorLog resourceErrorLog);

    ResourceErrorLog toEntity(ResourceErrorLogDTO resourceErrorLogDTO);

    default ResourceErrorLog fromId(Long id) {
        if (id == null) {
            return null;
        }
        ResourceErrorLog resourceErrorLog = new ResourceErrorLog();
        resourceErrorLog.setId( id );
        return resourceErrorLog;
    }
}
