package com.example.validation.mapper;

import com.example.validation.domain.ExecutionErrorLog;
import com.example.validation.dto.ExecutionErrorLogDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface ExecutionErrorLogMapper extends EntityMapper<ExecutionErrorLogDTO, ExecutionErrorLog> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    ExecutionErrorLogDTO toDto(ExecutionErrorLog executionErrorLog);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    ExecutionErrorLog toEntity(ExecutionErrorLogDTO executionErrorLogDTO);

    default ExecutionErrorLog fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExecutionErrorLog executionErrorLog = new ExecutionErrorLog();
        executionErrorLog.setId(id);
        return executionErrorLog;
    }
}
