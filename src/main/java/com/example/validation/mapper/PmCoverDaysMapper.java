package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmCoverDays;
import com.example.validation.dto.PmCoverDaysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscItRMGrossConsumptionMapper.class,RscMtPPheaderMapper.class,RscItPMGrossConsumptionMapper.class, RscDtItemCodeMapper.class, RscItItemWiseSupplierMapper.class, RscMtSupplierMapper.class, RscDtSupplierMapper.class})
public interface PmCoverDaysMapper extends EntityMapper<PmCoverDaysDTO, RscIotPmCoverDays> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItPMGrossConsumption.id", target = "rscItPMGrossConsumptionId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplier")
    @Mapping(source = "rscMtItem.rscDtItemType.name", target = "category")
    @Mapping(source = "stdPrice", target = "stdPrice")
    @Mapping(source = "rscItPMGrossConsumption.m1Value", target = "coverDaysMonthDetailsDTO.month1ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m2Value", target = "coverDaysMonthDetailsDTO.month2ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m3Value", target = "coverDaysMonthDetailsDTO.month3ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m4Value", target = "coverDaysMonthDetailsDTO.month4ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m5Value", target = "coverDaysMonthDetailsDTO.month5ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m6Value", target = "coverDaysMonthDetailsDTO.month6ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m7Value", target = "coverDaysMonthDetailsDTO.month7ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m8Value", target = "coverDaysMonthDetailsDTO.month8ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m9Value", target = "coverDaysMonthDetailsDTO.month9ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m10Value", target = "coverDaysMonthDetailsDTO.month10ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m11Value", target = "coverDaysMonthDetailsDTO.month11ConsumptionQuantity")
    @Mapping(source = "rscItPMGrossConsumption.m12Value", target = "coverDaysMonthDetailsDTO.month12ConsumptionQuantity")
    @Mapping(source = "month1ConsumptionValue", target = "coverDaysMonthDetailsDTO.month1Value")
    @Mapping(source = "month2ConsumptionValue", target = "coverDaysMonthDetailsDTO.month2Value")
    @Mapping(source = "month3ConsumptionValue", target = "coverDaysMonthDetailsDTO.month3Value")
    @Mapping(source = "month4ConsumptionValue", target = "coverDaysMonthDetailsDTO.month4Value")
    @Mapping(source = "month5ConsumptionValue", target = "coverDaysMonthDetailsDTO.month5Value")
    @Mapping(source = "month6ConsumptionValue", target = "coverDaysMonthDetailsDTO.month6Value")
    @Mapping(source = "month7ConsumptionValue", target = "coverDaysMonthDetailsDTO.month7Value")
    @Mapping(source = "month8ConsumptionValue", target = "coverDaysMonthDetailsDTO.month8Value")
    @Mapping(source = "month9ConsumptionValue", target = "coverDaysMonthDetailsDTO.month9Value")
    @Mapping(source = "month10ConsumptionValue", target = "coverDaysMonthDetailsDTO.month10Value")
    @Mapping(source = "month11ConsumptionValue", target = "coverDaysMonthDetailsDTO.month11Value")
    @Mapping(source = "month12ConsumptionValue", target = "coverDaysMonthDetailsDTO.month12Value")
    PmCoverDaysDTO toDto(RscIotPmCoverDays rscIotPmCoverDays);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscItPMGrossConsumptionId", target = "rscItPMGrossConsumption")
    @Mapping(source = "coverDaysMonthDetailsDTO.month1Value", target = "month1ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month2Value", target = "month2ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month3Value", target = "month3ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month4Value", target = "month4ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month5Value", target = "month5ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month6Value", target = "month6ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month7Value", target = "month7ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month8Value", target = "month8ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month9Value", target = "month9ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month10Value", target = "month10ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month11Value", target = "month11ConsumptionValue")
    @Mapping(source = "coverDaysMonthDetailsDTO.month12Value", target = "month12ConsumptionValue")
    RscIotPmCoverDays toEntity(PmCoverDaysDTO pmCoverDaysDTO);

    default RscIotPmCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmCoverDays rscIotPmCoverDays = new RscIotPmCoverDays();
        rscIotPmCoverDays.setId(id);
        return rscIotPmCoverDays;
    }
}
