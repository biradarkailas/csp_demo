package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmOtifCalculation;
import com.example.validation.dto.RscIotPmOtifCalculationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscIotPMLatestPurchasePlanMapper.class,RscDtItemCodeMapper.class, RscMtPPheaderMapper.class,RscDtSupplierMapper.class})
public interface RscIotPmOtifCalculationMapper extends EntityMapper<RscIotPmOtifCalculationDTO, RscIotPmOtifCalculation> {

    @Mapping(source = "rscIotPMLatestPurchasePlan.id", target = "rscIotPMLatestPurchasePlanId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscIotPMLatestPurchasePlan.m1Value", target = "prevMonthOrderQty")
    @Mapping(source = "rscIotPMLatestPurchasePlan.m2Value", target = "currentMonthOrderQty")
    RscIotPmOtifCalculationDTO toDto(RscIotPmOtifCalculation rscIotPmOtifCalculation);

    @Mapping(source = "rscIotPMLatestPurchasePlanId", target = "rscIotPMLatestPurchasePlan")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtSupplierId", target = "rscDtSupplier")
    RscIotPmOtifCalculation toEntity(RscIotPmOtifCalculationDTO rscIotPmOtifCalculationDTO);

    default RscIotPmOtifCalculation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmOtifCalculation rscIotPmOtifCalculation = new RscIotPmOtifCalculation();
        rscIotPmOtifCalculation.setId(id);
        return rscIotPmOtifCalculation;
    }
}