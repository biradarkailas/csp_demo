package com.example.validation.mapper;

import com.example.validation.domain.RscMtBulkMes;
import com.example.validation.dto.RscMtBulkMesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class})
public interface RscMtBulkMesMapper extends EntityMapper<RscMtBulkMesDTO, RscMtBulkMes> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    RscMtBulkMesDTO toDto(RscMtBulkMes rscMtBulkMes);

    RscMtBulkMes toEntity(RscMtBulkMesDTO rscMtBulkMesDTO);

    default RscMtBulkMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtBulkMes rscMtBulkMes = new RscMtBulkMes();
        rscMtBulkMes.setId( id );
        return rscMtBulkMes;
    }
}