package com.example.validation.mapper;

import com.example.validation.domain.RscDtMould;
import com.example.validation.dto.RscDtMouldDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtMouldMapper extends EntityMapper<RscDtMouldDTO, RscDtMould> {
    RscDtMouldDTO toDto(RscDtMould rscDtMould);

    RscDtMould toEntity(RscDtMouldDTO rscDtMouldDTO);

    default RscDtMould fromId(Long id) {
        if(id == null) {
            return null;
        }
        RscDtMould rscDtMould=new RscDtMould();
        rscDtMould.setId(id);
        return rscDtMould;
    }
}
