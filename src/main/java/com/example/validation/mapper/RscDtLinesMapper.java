package com.example.validation.mapper;

import com.example.validation.domain.RscDtLines;
import com.example.validation.dto.RscDtLinesDTO;
import org.mapstruct.Mapper;

import java.util.Optional;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtLinesMapper extends EntityMapper<RscDtLinesDTO, RscDtLines> {

    RscDtLinesDTO toDto(RscDtLines rscDtLines);

    RscDtLines toEntity(RscDtLinesDTO rscDtLinesDTO);

    default RscDtLines fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtLines rscDtLines = new RscDtLines();
        rscDtLines.setId(id);
        return rscDtLines;
    }
}
