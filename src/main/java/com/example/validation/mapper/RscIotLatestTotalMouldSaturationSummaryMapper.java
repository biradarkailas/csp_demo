package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestTotalMouldSaturationSummary;
import com.example.validation.dto.RscIotLatestTotalMouldSaturationSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class,RscIotLatestTotalMouldSaturationMapper.class})
public interface RscIotLatestTotalMouldSaturationSummaryMapper extends EntityMapper<RscIotLatestTotalMouldSaturationSummaryDTO, RscIotLatestTotalMouldSaturationSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscIotLatestTotalMouldSaturation.id", target = "rscIotLatestTotalMouldSaturationId")
    RscIotLatestTotalMouldSaturationSummaryDTO toDto(RscIotLatestTotalMouldSaturationSummary rscIotLatestTotalMouldSaturationSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscIotLatestTotalMouldSaturationId", target = "rscIotLatestTotalMouldSaturation")
    RscIotLatestTotalMouldSaturationSummary toEntity(RscIotLatestTotalMouldSaturationSummaryDTO rscIotLatestTotalMouldSaturationSummaryDTO);

    default RscIotLatestTotalMouldSaturationSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestTotalMouldSaturationSummary rscIotLatestTotalMouldSaturationSummary = new RscIotLatestTotalMouldSaturationSummary();
        rscIotLatestTotalMouldSaturationSummary.setId(id);
        return rscIotLatestTotalMouldSaturationSummary;
    }
}
