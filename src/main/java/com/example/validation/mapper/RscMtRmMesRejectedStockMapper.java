package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmMesRejectedStock;
import com.example.validation.dto.supply.RscMtRmMesRejectedStockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class})
public interface RscMtRmMesRejectedStockMapper extends EntityMapper<RscMtRmMesRejectedStockDTO, RscMtRmMesRejectedStock> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    RscMtRmMesRejectedStockDTO toDto(RscMtRmMesRejectedStock rscMtRmMesRejectedStock);

    RscMtRmMesRejectedStock toEntity(RscMtRmMesRejectedStockDTO rscMtRmMesRejectedStockDTO);

    default RscMtRmMesRejectedStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmMesRejectedStock rscMtRmMesRejectedStock = new RscMtRmMesRejectedStock();
        rscMtRmMesRejectedStock.setId( id );
        return rscMtRmMesRejectedStock;
    }
}
