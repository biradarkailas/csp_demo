package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmLatestSlobReason;
import com.example.validation.dto.RscIotRmLatestSlobReasonDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtReasonMapper.class})
public interface RscIotRmLatestSlobReasonMapper extends EntityMapper<RscIotRmLatestSlobReasonDTO, RscIotRmLatestSlobReason> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscDtReason.id", target = "rscDtReasonId")
    RscIotRmLatestSlobReasonDTO toDto(RscIotRmLatestSlobReason rscIotRmLatestSlobReason);

    @Mapping(source = "rscDtReasonId", target = "rscDtReason")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotRmLatestSlobReason toEntity(RscIotRmLatestSlobReasonDTO rscIotRmLatestSlobReasonDTO);

    default RscIotRmLatestSlobReason fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmLatestSlobReason rscIotRmLatestSlobReason = new RscIotRmLatestSlobReason();
        rscIotRmLatestSlobReason.setId(id);
        return rscIotRmLatestSlobReason;
    }
}
