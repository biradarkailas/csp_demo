package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgLineSaturation;
import com.example.validation.dto.FgLineSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtLinesMapper.class})
public interface FgLineSaturationMapper extends EntityMapper<FgLineSaturationDTO, RscIotFgLineSaturation> {
    @Mapping(source = "id", target = "rscIotFgLineSaturationDTOS.id")
    @Mapping(source = "rscMtPPheader.id", target = "rscIotFgLineSaturationDTOS.rscMtPPheaderId")
    @Mapping(source = "rscDtLines.id", target = "rscIotFgLineSaturationDTOS.rscDtLinesId")
    @Mapping(source = "rscDtLines.name", target = "lineName")
    @Mapping(source = "m1TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m1TotalMpsPlanQty")
    @Mapping(source = "m2TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m2TotalMpsPlanQty")
    @Mapping(source = "m3TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m3TotalMpsPlanQty")
    @Mapping(source = "m4TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m4TotalMpsPlanQty")
    @Mapping(source = "m5TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m5TotalMpsPlanQty")
    @Mapping(source = "m6TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m6TotalMpsPlanQty")
    @Mapping(source = "m7TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m7TotalMpsPlanQty")
    @Mapping(source = "m8TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m8TotalMpsPlanQty")
    @Mapping(source = "m9TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m9TotalMpsPlanQty")
    @Mapping(source = "m10TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m10TotalMpsPlanQty")
    @Mapping(source = "m11TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m11TotalMpsPlanQty")
    @Mapping(source = "m12TotalMpsPlanQty", target = "rscIotFgLineSaturationDTOS.m12TotalMpsPlanQty")
    @Mapping(source = "month1Capacity", target = "rscIotFgLineSaturationDTOS.month1Capacity")
    @Mapping(source = "month2Capacity", target = "rscIotFgLineSaturationDTOS.month2Capacity")
    @Mapping(source = "month3Capacity", target = "rscIotFgLineSaturationDTOS.month3Capacity")
    @Mapping(source = "month4Capacity", target = "rscIotFgLineSaturationDTOS.month4Capacity")
    @Mapping(source = "month5Capacity", target = "rscIotFgLineSaturationDTOS.month5Capacity")
    @Mapping(source = "month6Capacity", target = "rscIotFgLineSaturationDTOS.month6Capacity")
    @Mapping(source = "month7Capacity", target = "rscIotFgLineSaturationDTOS.month7Capacity")
    @Mapping(source = "month8Capacity", target = "rscIotFgLineSaturationDTOS.month8Capacity")
    @Mapping(source = "month9Capacity", target = "rscIotFgLineSaturationDTOS.month9Capacity")
    @Mapping(source = "month10Capacity", target = "rscIotFgLineSaturationDTOS.month10Capacity")
    @Mapping(source = "month11Capacity", target = "rscIotFgLineSaturationDTOS.month11Capacity")
    @Mapping(source = "month12Capacity", target = "rscIotFgLineSaturationDTOS.month12Capacity")
    @Mapping(source = "m1LineSaturation", target = "rscIotFgLineSaturationDTOS.m1LineSaturation")
    @Mapping(source = "m2LineSaturation", target = "rscIotFgLineSaturationDTOS.m2LineSaturation")
    @Mapping(source = "m3LineSaturation", target = "rscIotFgLineSaturationDTOS.m3LineSaturation")
    @Mapping(source = "m4LineSaturation", target = "rscIotFgLineSaturationDTOS.m4LineSaturation")
    @Mapping(source = "m5LineSaturation", target = "rscIotFgLineSaturationDTOS.m5LineSaturation")
    @Mapping(source = "m6LineSaturation", target = "rscIotFgLineSaturationDTOS.m6LineSaturation")
    @Mapping(source = "m7LineSaturation", target = "rscIotFgLineSaturationDTOS.m7LineSaturation")
    @Mapping(source = "m8LineSaturation", target = "rscIotFgLineSaturationDTOS.m8LineSaturation")
    @Mapping(source = "m9LineSaturation", target = "rscIotFgLineSaturationDTOS.m9LineSaturation")
    @Mapping(source = "m10LineSaturation", target = "rscIotFgLineSaturationDTOS.m10LineSaturation")
    @Mapping(source = "m11LineSaturation", target = "rscIotFgLineSaturationDTOS.m11LineSaturation")
    @Mapping(source = "m12LineSaturation", target = "rscIotFgLineSaturationDTOS.m12LineSaturation")
    FgLineSaturationDTO toDto(RscIotFgLineSaturation rscIotFgLineSaturation);

    default RscIotFgLineSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgLineSaturation rscIotFgLineSaturation = new RscIotFgLineSaturation();
        rscIotFgLineSaturation.setId(id);
        return rscIotFgLineSaturation;
    }
}
