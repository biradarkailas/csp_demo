package com.example.validation.mapper;

import com.example.validation.domain.RscDtMaterialSubCategory;
import com.example.validation.dto.RscDtMaterialSubCategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtMaterialSubCategoryMapper extends EntityMapper<RscDtMaterialSubCategoryDTO, RscDtMaterialSubCategory> {

    RscDtMaterialSubCategoryDTO toDto(RscDtMaterialSubCategory rscDtMaterialSubCategory);

    RscDtMaterialSubCategory toEntity(RscDtMaterialSubCategoryDTO rscDtMaterialSubCategoryDTO);

    default RscDtMaterialSubCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtMaterialSubCategory rscDtMaterialSubCategory = new RscDtMaterialSubCategory();
        rscDtMaterialSubCategory.setId(id);
        return rscDtMaterialSubCategory;
    }
}