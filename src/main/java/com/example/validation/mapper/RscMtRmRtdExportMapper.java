package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmRtd;
import com.example.validation.dto.RscMtRmRtdExportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscDtSupplierMapper.class, RscMtPPheaderMapper.class})
public interface RscMtRmRtdExportMapper extends EntityMapper<RscMtRmRtdExportDTO, RscMtRmRtd> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    RscMtRmRtdExportDTO toDto(RscMtRmRtd rscMtRmRtd);

    RscMtRmRtd toEntity(RscMtRmRtdExportDTO rscMtRmRtdExportDTO);

    default RscMtRmRtd fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmRtd rscMtRmRtd = new RscMtRmRtd();
        rscMtRmRtd.setId(id);
        return rscMtRmRtd;
    }
}
