package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmOtifSummary;
import com.example.validation.dto.RscIotPmOtifSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtSupplierMapper.class})
public interface RscIotPmOtifSummaryMapper extends EntityMapper<RscIotPmOtifSummaryDTO, RscIotPmOtifSummary> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscDtSupplier.supplierCode", target = "supplierCode")
    RscIotPmOtifSummaryDTO toDto(RscIotPmOtifSummary rscIotPmOtifSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtSupplierId", target = "rscDtSupplier")
    RscIotPmOtifSummary toEntity(RscIotPmOtifSummaryDTO rscIotPmOtifSummaryDTO);

    default RscIotPmOtifSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmOtifSummary rscIotPmOtifSummary = new RscIotPmOtifSummary();
        rscIotPmOtifSummary.setId(id);
        return rscIotPmOtifSummary;
    }
}
