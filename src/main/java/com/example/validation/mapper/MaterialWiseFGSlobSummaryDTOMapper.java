package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import com.example.validation.dto.slob.TypeWiseFGSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFGTypeMapper.class})
public interface MaterialWiseFGSlobSummaryDTOMapper extends EntityMapper<TypeWiseFGSlobSummaryDTO, RscIotFGTypeSlobSummary> {

    @Mapping(source = "rscDtFGType.id", target = "fgTypeId")
    @Mapping(source = "rscDtFGType.type", target = "fgType")
    @Mapping(source = "totalStockValue", target = "totalTypeStockValue")
    @Mapping(source = "totalSlobValue", target = "totalTypeSlobValue")
    @Mapping(source = "totalSlowItemValue", target = "totalTypeSLValue")
    @Mapping(source = "totalObsoleteItemValue", target = "totalTypeOBValue")
    @Mapping(source = "stockQualityPercentage", target = "materialIQ")
    TypeWiseFGSlobSummaryDTO toDto(RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary);

    default RscIotFGTypeSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary = new RscIotFGTypeSlobSummary();
        rscIotFGTypeSlobSummary.setId(id);
        return rscIotFGTypeSlobSummary;
    }
}