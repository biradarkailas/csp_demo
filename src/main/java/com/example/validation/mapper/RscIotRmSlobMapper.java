package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmSlob;
import com.example.validation.dto.RscIotRmSlobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtReasonMapper.class, RscItRMGrossConsumptionMapper.class, RscDtItemCodeMapper.class, RscItItemWiseSupplierMapper.class})
public interface RscIotRmSlobMapper extends EntityMapper<RscIotRmSlobDTO, RscIotRmSlob> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscDtReason.id", target = "rscDtReasonId")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    RscIotRmSlobDTO toDto(RscIotRmSlob rscItPMSlob);

    @Mapping(source = "rscDtReasonId", target = "rscDtReason")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscItRMGrossConsumptionId", target = "rscItRMGrossConsumption")
    RscIotRmSlob toEntity(RscIotRmSlobDTO rscIotRmSlobDTO);

    default RscIotRmSlob fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmSlob rscIotRmSlob = new RscIotRmSlob();
        rscIotRmSlob.setId(id);
        return rscIotRmSlob;
    }
}
