package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgLineSaturationSummary;
import com.example.validation.dto.FgLineSaturationLinesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtLinesMapper.class})
public interface FgLineSaturationLinesMapper extends EntityMapper<FgLineSaturationLinesDTO, RscIotFgLineSaturationSummary>{
    @Mapping(source = "rscDtLines.id", target = "rscDtLinesId")
    @Mapping(source = "rscDtLines.name", target = "lineName")
    FgLineSaturationLinesDTO toDto(RscIotFgLineSaturationSummary rscIotFgLineSaturationSummary);

    default RscIotFgLineSaturationSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgLineSaturationSummary rscIotFgLineSaturationSummary = new RscIotFgLineSaturationSummary();
        rscIotFgLineSaturationSummary.setId(id);
        return rscIotFgLineSaturationSummary;
    }
}
