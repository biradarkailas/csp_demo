package com.example.validation.mapper;

import com.example.validation.domain.RscMtPPheader;
import com.example.validation.dto.RscMtPPheaderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtTagMapper.class})
public interface RscMtPPheaderMapper extends EntityMapper<RscMtPPheaderDTO, RscMtPPheader> {

    @Mapping(source = "rscDtTag.isBackup", target = "isMpsBackup")
    @Mapping(source = "rscDtTag.name", target = "tagName")
    @Mapping(source = "rscDtTag.id", target = "tagId")
    @Mapping(source = "rscDtTag.description", target = "tagDescription")
    RscMtPPheaderDTO toDto(RscMtPPheader rscMtPPheader);

    @Mapping(source = "tagId", target = "rscDtTag")
    RscMtPPheader toEntity(RscMtPPheaderDTO rscMtPPheaderDTO);

    default RscMtPPheader fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPPheader rscMtPPheader = new RscMtPPheader();
        rscMtPPheader.setId(id);
        return rscMtPPheader;
    }
}
