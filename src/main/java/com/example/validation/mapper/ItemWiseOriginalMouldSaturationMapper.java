package com.example.validation.mapper;

import com.example.validation.domain.RscDtItemCode;
import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.dto.ItemWiseOriginalMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscItItemWiseSupplierMapper.class, RscMtItemMapper.class, RscDtItemCode.class})
public interface ItemWiseOriginalMouldSaturationMapper extends EntityMapper<ItemWiseOriginalMouldSaturationDTO, RscIotOriginalMouldSaturation> {

    @Mapping(source = "rscIotPMOriginalPurchasePlan.rscItItemWiseSupplier.rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscIotPMOriginalPurchasePlan.rscItItemWiseSupplier.rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "m1Value", target = "month1.value")
    @Mapping(source = "m2Value", target = "month2.value")
    @Mapping(source = "m3Value", target = "month3.value")
    @Mapping(source = "m4Value", target = "month4.value")
    @Mapping(source = "m5Value", target = "month5.value")
    @Mapping(source = "m6Value", target = "month6.value")
    @Mapping(source = "m7Value", target = "month7.value")
    @Mapping(source = "m8Value", target = "month8.value")
    @Mapping(source = "m9Value", target = "month9.value")
    @Mapping(source = "m10Value", target = "month10.value")
    @Mapping(source = "m11Value", target = "month11.value")
    @Mapping(source = "m12Value", target = "month12.value")
    ItemWiseOriginalMouldSaturationDTO toDto(RscIotOriginalMouldSaturation rscIotOriginalMouldSaturation);

}
