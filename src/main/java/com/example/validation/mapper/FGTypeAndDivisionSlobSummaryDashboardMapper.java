package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import com.example.validation.dto.slob.FGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface FGTypeAndDivisionSlobSummaryDashboardMapper extends EntityMapper<FGSlobSummaryDashboardDTO, RscIotFGTypeDivisionSlobSummary> {
    @Mapping(source = "totalObsoleteItemValue", target = "totalOBValue")
    @Mapping(source = "totalSlowItemValue", target = "totalSLValue")
    @Mapping(source = "stockQualityValue", target = "iqValue")
    @Mapping(source = "obsoleteItemPercentage", target = "obPercentage")
    @Mapping(source = "slowItemPercentage", target = "slPercentage")
    @Mapping(source = "stockQualityPercentage", target = "iqPercentage")
    FGSlobSummaryDashboardDTO toDto(RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary);

    default RscIotFGTypeDivisionSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary = new RscIotFGTypeDivisionSlobSummary();
        rscIotFGTypeDivisionSlobSummary.setId(id);
        return rscIotFGTypeDivisionSlobSummary;
    }
}