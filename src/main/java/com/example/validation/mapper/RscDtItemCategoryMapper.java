package com.example.validation.mapper;


import com.example.validation.domain.RscDtItemCategory;
import com.example.validation.dto.RscDtItemCategoryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtItemCategoryMapper extends EntityMapper<RscDtItemCategoryDTO, RscDtItemCategory> {

    RscDtItemCategoryDTO toDto(RscDtItemCategory rscDtItemCategory);


    RscDtItemCategory toEntity(RscDtItemCategoryDTO rscDtItemCategoryDTO);

    default RscDtItemCategory fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtItemCategory rscDtItemCategory = new RscDtItemCategory();
        rscDtItemCategory.setId(id);
        return rscDtItemCategory;
    }
}
