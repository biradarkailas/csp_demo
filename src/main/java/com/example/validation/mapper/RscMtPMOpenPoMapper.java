package com.example.validation.mapper;

import com.example.validation.domain.RscMtPMOpenPo;
import com.example.validation.domain.RscMtRmOpenPo;
import com.example.validation.dto.RscMtPMOpenPoDTO;
import com.example.validation.dto.RscMtRmOpenPoDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscMtSupplierMapper.class})
public interface RscMtPMOpenPoMapper extends EntityMapper<RscMtPMOpenPoDTO, RscMtPMOpenPo> {

    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtSupplier.id", target = "rscMtSupplierId")
    RscMtPMOpenPoDTO toDto(RscMtPMOpenPo rscMtPMOpenPo);

    @Mapping(source = "rscMtSupplierId", target = "rscMtSupplier")
    @Mapping(source = "mpsId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscMtPMOpenPo toEntity(RscMtPMOpenPoDTO rscMtPMOpenPoDTO);

    default RscMtPMOpenPo fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPMOpenPo rscMtPMOpenPo = new RscMtPMOpenPo();
        rscMtPMOpenPo.setId(id);
        return rscMtPMOpenPo;
    }
}