package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmOtifReceipts;
import com.example.validation.dto.RscIotPmOtifReceiptsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtSupplierMapper.class,RscMtOtifPmReceiptsHeaderMapper.class})
public interface RscIotPmOtifReceiptsMapper extends EntityMapper<RscIotPmOtifReceiptsDTO, RscIotPmOtifReceipts> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscMtOtifPmReceiptsHeader.id", target = "rscMtOtifPmReceiptsHeaderId")

    RscIotPmOtifReceiptsDTO toDto(RscIotPmOtifReceipts rscIotPmOtifReceipts);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscDtSupplierId", target = "rscDtSupplier")
    @Mapping(source = "rscMtOtifPmReceiptsHeaderId", target = "rscMtOtifPmReceiptsHeader")
    RscIotPmOtifReceipts toEntity(RscIotPmOtifReceiptsDTO rscIotPmOtifReceiptsDTO);

    default RscIotPmOtifReceipts fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmOtifReceipts rscIotPmOtifReceipts = new RscIotPmOtifReceipts();
        rscIotPmOtifReceipts.setId(id);
        return rscIotPmOtifReceipts;
    }
}