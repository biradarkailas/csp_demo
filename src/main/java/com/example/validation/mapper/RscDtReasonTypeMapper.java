package com.example.validation.mapper;

import com.example.validation.domain.RscDtReasonType;
import com.example.validation.dto.RscDtReasonTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtReasonTypeMapper extends EntityMapper<RscDtReasonTypeDTO, RscDtReasonType> {
    RscDtReasonTypeDTO toDto(RscDtReasonType rscDtReasonType);

    RscDtReasonType toEntity(RscDtReasonTypeDTO rscDtReasonTypeDTO);

    default RscDtReasonType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtReasonType rscDtReasonType = new RscDtReasonType();
        rscDtReasonType.setId(id);
        return rscDtReasonType;
    }
}
