package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmMes;
import com.example.validation.dto.RscMtRmMesDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtRmMesMapper extends EntityMapper<RscMtRmMesDTO, RscMtRmMes>{

    RscMtRmMesDTO toDto(RscMtRmMes rscMtRmMes);

    RscMtRmMes toEntity(RscMtRmMesDTO rscMtRmMesDTO);

    default RscMtRmMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmMes rscMtRmMes= new RscMtRmMes();
        rscMtRmMes.setId(id);
        return rscMtRmMes;
    }
}