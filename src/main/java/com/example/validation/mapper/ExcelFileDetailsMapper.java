package com.example.validation.mapper;

import com.example.validation.domain.ExcelFiledetails;
import com.example.validation.dto.ExcelFiledetailsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ExcelFileDetailsMapper extends EntityMapper<ExcelFiledetailsDTO, ExcelFiledetails> {
    ExcelFiledetailsDTO toDto(ExcelFiledetails excelFiledetails);

    ExcelFiledetails toEntity(ExcelFiledetailsDTO excelFiledetailsDTO);

    default ExcelFiledetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        ExcelFiledetails excelFiledetails = new ExcelFiledetails();
        excelFiledetails.setId(id);
        return excelFiledetails;
    }
}
