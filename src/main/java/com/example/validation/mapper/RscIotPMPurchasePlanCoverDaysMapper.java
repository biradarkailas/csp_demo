package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMPurchasePlanCoverDays;
import com.example.validation.dto.supply.RscIotPMPurchasePlanCoverDaysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class, RscIotPMPurchasePlanCalculationsMapper.class})
public interface RscIotPMPurchasePlanCoverDaysMapper extends EntityMapper<RscIotPMPurchasePlanCoverDaysDTO, RscIotPMPurchasePlanCoverDays> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "pmPurchasePlanCalculations.id", target = "pmPurchasePlanCalculationsId")
    RscIotPMPurchasePlanCoverDaysDTO toDto(RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays);

    @Mapping(source = "pmPurchasePlanCalculationsId", target = "pmPurchasePlanCalculations")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotPMPurchasePlanCoverDays toEntity(RscIotPMPurchasePlanCoverDaysDTO rscIotPMPurchasePlanCoverDaysDTO);

    default RscIotPMPurchasePlanCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMPurchasePlanCoverDays rscIotPMPurchasePlanCoverDays = new RscIotPMPurchasePlanCoverDays();
        rscIotPMPurchasePlanCoverDays.setId(id);
        return rscIotPMPurchasePlanCoverDays;
    }
}