package com.example.validation.mapper;

import com.example.validation.domain.RscItRMLogicalConsumption;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscMtBomMapper.class})
public interface RscItRMLogicalConsumptionMapper extends EntityMapper<RscItLogicalConsumptionDTO, RscItRMLogicalConsumption> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItemParent.id", target = "rscMtItemMpsParentId")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.code", target = "rscMtItemMpsParentCode")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.description", target = "rscMItemMpsParentCodeDescription")
    @Mapping(source = "rscMtItemParent.id", target = "rscMtItemParentId")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.code", target = "rscMtItemParentCode")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.description", target = "rscMtItemParentCodeDescription")
    @Mapping(source = "rscMtItemChild.id", target = "rscMtItemChildId")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.code", target = "rscMtItemChildCode")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.description", target = "mtItemChildCodeDescription")
    @Mapping(source = "rscMtBom.id", target = "rscMtBomId")
    @Mapping(source = "rscMtBom.applicableFrom", target = "applicableFrom")
    @Mapping(source = "rscMtBom.applicableTo", target = "applicableTo")
    RscItLogicalConsumptionDTO toDto(RscItRMLogicalConsumption rscItRMLogicalConsumption);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemMpsParentId", target = "rscMtItemMpsParent")
    @Mapping(source = "rscMtItemParentId", target = "rscMtItemParent")
    @Mapping(source = "rscMtItemChildId", target = "rscMtItemChild")
    @Mapping(source = "rscMtBomId", target = "rscMtBom")
    RscItRMLogicalConsumption toEntity(RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO);

    default RscItRMLogicalConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItRMLogicalConsumption rscItRMLogicalConsumption = new RscItRMLogicalConsumption();
        rscItRMLogicalConsumption.setId(id);
        return rscItRMLogicalConsumption;
    }
}