package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmOtifReceipts;
import com.example.validation.dto.RscMtPmOtifReceiptsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtSupplierMapper.class, RscMtOtifPmReceiptsHeaderMapper.class})
public interface RscMtPmOtifReceiptsMapper extends EntityMapper<RscMtPmOtifReceiptsDTO, RscMtPmOtifReceipts> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscMtOtifPmReceiptsHeader.id", target = "rscMtOtifPmReceiptsHeaderId")
    RscMtPmOtifReceiptsDTO toDto(RscMtPmOtifReceipts rscMtPmOtifReceipts);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscDtSupplierId", target = "rscDtSupplier")
    @Mapping(source = "rscMtOtifPmReceiptsHeaderId", target = "rscMtOtifPmReceiptsHeader")
    RscMtPmOtifReceipts toEntity(RscMtPmOtifReceiptsDTO rscMtPmOtifReceiptsDTO);

    default RscMtPmOtifReceipts fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPmOtifReceipts rscMtPmOtifReceipts = new RscMtPmOtifReceipts();
        rscMtPmOtifReceipts.setId( id );
        return rscMtPmOtifReceipts;
    }
}
