package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGSlobSummary;
import com.example.validation.dto.slob.FGSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface FGSlobSummaryMapper extends EntityMapper<FGSlobSummaryDTO, RscIotFGSlobSummary> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtPPheader.mpsName", target = "mpsName")
    @Mapping(source = "slobItemPercentage", target = "slobIQ")
    FGSlobSummaryDTO toDto(RscIotFGSlobSummary rscIotFGSlobSummary);

    default RscIotFGSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGSlobSummary rscIotFGSlobSummary = new RscIotFGSlobSummary();
        rscIotFGSlobSummary.setId(id);
        return rscIotFGSlobSummary;
    }
}