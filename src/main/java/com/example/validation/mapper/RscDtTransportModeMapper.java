package com.example.validation.mapper;

import com.example.validation.domain.RscDtTransportMode;
import com.example.validation.dto.RscDtTransportModeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtTransportModeMapper extends EntityMapper<RscDtTransportModeDTO, RscDtTransportMode> {
    RscDtTransportModeDTO toDto(RscDtTransportMode rscDtTransportMode);

    RscDtTransportMode toEntity(RscDtTransportModeDTO rscDtTransportModeDTO);

    default RscDtTransportMode fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtTransportMode rscDtTransportMode = new RscDtTransportMode();
        rscDtTransportMode.setId(id);
        return rscDtTransportMode;
    }
}
