package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmOtifSummary;
import com.example.validation.dto.RscIotPmOtifCalculationDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class,RscDtSupplierMapper.class})
public interface PmOtifSummaryMapper extends EntityMapper<RscIotPmOtifCalculationDetailsDTO, RscIotPmOtifSummary> {
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscDtSupplier.supplierCode", target = "supplierCode")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "avgReceiptPercentage", target = "receiptPercentage")
    @Mapping(source = "avgScore", target = "score")
    RscIotPmOtifCalculationDetailsDTO toDto(RscIotPmOtifSummary rscIotPmOtifSummary);

    default RscIotPmOtifSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmOtifSummary rscIotPmOtifSummary = new RscIotPmOtifSummary();
        rscIotPmOtifSummary.setId(id);
        return rscIotPmOtifSummary;
    }
}