package com.example.validation.mapper;

import com.example.validation.domain.RscDtStdPrice;
import com.example.validation.dto.RscDtStdPriceDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtStdPriceMapper extends EntityMapper<RscDtStdPriceDTO, RscDtStdPrice> {
    RscDtStdPriceDTO toDto(RscDtStdPrice rscDtStdPrice);

    RscDtStdPrice toEntity(RscDtStdPriceDTO rscDtStdPriceDTO);

    default RscDtStdPrice fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtStdPrice rscDtStdPrice = new RscDtStdPrice();
        rscDtStdPrice.setId(id);
        return rscDtStdPrice;
    }
}
