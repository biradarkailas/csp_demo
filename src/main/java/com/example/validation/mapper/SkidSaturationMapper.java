package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgSkidSaturation;
import com.example.validation.dto.SkidsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtSkidsMapper.class})
public interface SkidSaturationMapper extends EntityMapper<SkidsDTO, RscIotFgSkidSaturation> {

    @Mapping(source = "rscDtSkids.id", target = "id")
    @Mapping(source = "rscDtSkids.name", target = "skidName")
    SkidsDTO toDto(RscIotFgSkidSaturation rscIotFgSkidSaturation);

    default RscIotFgSkidSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgSkidSaturation rscIotFgSkidSaturation = new RscIotFgSkidSaturation();
        rscIotFgSkidSaturation.setId(id);
        return rscIotFgSkidSaturation;
    }

}
