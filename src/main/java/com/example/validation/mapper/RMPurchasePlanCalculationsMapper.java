package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMPurchasePlanCalculations;
import com.example.validation.dto.supply.RscIotRMPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscIitRMItemWiseSupplierDetailsMapper.class, RscItRMGrossConsumptionMapper.class, RscMtSupplierMapper.class})
public interface RMPurchasePlanCalculationsMapper extends EntityMapper<RscIotRMPurchasePlanDTO, RscIotRMPurchasePlanCalculations> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.id", target = "rscIitRMItemWiseSupplierDetailsId")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscMtSupplier.id", target = "rscMtSupplierId")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscDtContainerType.typeName", target = "containerType")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    @Mapping(source = "month1SuggestedPurchasePlan", target = "m1Value")
    @Mapping(source = "month2SuggestedPurchasePlan", target = "m2Value")
    @Mapping(source = "month3SuggestedPurchasePlan", target = "m3Value")
    @Mapping(source = "month4SuggestedPurchasePlan", target = "m4Value")
    @Mapping(source = "month5SuggestedPurchasePlan", target = "m5Value")
    @Mapping(source = "month6SuggestedPurchasePlan", target = "m6Value")
    @Mapping(source = "month7SuggestedPurchasePlan", target = "m7Value")
    @Mapping(source = "month8SuggestedPurchasePlan", target = "m8Value")
    @Mapping(source = "month9SuggestedPurchasePlan", target = "m9Value")
    @Mapping(source = "month10SuggestedPurchasePlan", target = "m10Value")
    @Mapping(source = "month11SuggestedPurchasePlan", target = "m11Value")
    @Mapping(source = "month12SuggestedPurchasePlan", target = "m12Value")
    RscIotRMPurchasePlanDTO toDto(RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations);

    default RscIotRMPurchasePlanCalculations fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations = new RscIotRMPurchasePlanCalculations();
        rscIotRMPurchasePlanCalculations.setId(id);
        return rscIotRMPurchasePlanCalculations;
    }
}