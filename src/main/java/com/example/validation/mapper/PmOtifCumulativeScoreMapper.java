package com.example.validation.mapper;

import com.example.validation.domain.PmOtifCumulativeScore;
import com.example.validation.dto.PmOtifCumulativeScoreDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtSupplierMapper.class})
public interface PmOtifCumulativeScoreMapper extends EntityMapper<PmOtifCumulativeScoreDTO, PmOtifCumulativeScore> {
    @Mapping(source = "rscDtSupplier.id", target = "supplierId")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    PmOtifCumulativeScoreDTO toDto(PmOtifCumulativeScore pmOtifCumulativeScore);

    @Mapping(source = "supplierId", target = "rscDtSupplier")
    PmOtifCumulativeScore toEntity(PmOtifCumulativeScoreDTO pmOtifCumulativeScoreDTO);

    default PmOtifCumulativeScore fromId(Long id) {
        if (id == null) {
            return null;
        }
        PmOtifCumulativeScore pmOtifCumulativeScore = new PmOtifCumulativeScore();
        pmOtifCumulativeScore.setId(id);
        return pmOtifCumulativeScore;
    }
}
