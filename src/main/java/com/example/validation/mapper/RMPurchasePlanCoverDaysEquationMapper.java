package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMPurchasePlanCoverDays;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscIotRMPurchasePlanCalculationsMapper.class})
public interface RMPurchasePlanCoverDaysEquationMapper extends EntityMapper<PurchasePlanCoverDaysEquationDTO, RscIotRMPurchasePlanCoverDays> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "m1Value", target = "m1StockEquationDTO.coverDays")
    @Mapping(source = "m2Value", target = "m2StockEquationDTO.coverDays")
    @Mapping(source = "m3Value", target = "m3StockEquationDTO.coverDays")
    @Mapping(source = "m4Value", target = "m4StockEquationDTO.coverDays")
    @Mapping(source = "m5Value", target = "m5StockEquationDTO.coverDays")
    @Mapping(source = "m6Value", target = "m6StockEquationDTO.coverDays")
    @Mapping(source = "m7Value", target = "m7StockEquationDTO.coverDays")
    @Mapping(source = "m8Value", target = "m8StockEquationDTO.coverDays")
    @Mapping(source = "m9Value", target = "m9StockEquationDTO.coverDays")
    @Mapping(source = "m10Value", target = "m10StockEquationDTO.coverDays")
    @Mapping(source = "m11Value", target = "m11StockEquationDTO.coverDays")
    @Mapping(source = "m12Value", target = "m12StockEquationDTO.coverDays")
    PurchasePlanCoverDaysEquationDTO toDto(RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays);

    @AfterMapping
    default void updateRMPurchasePlanCoverDaysEquationDTO(@MappingTarget PurchasePlanCoverDaysEquationDTO purchasePlanCoverDaysEquationDTO, RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays) {
        setRMPurchasePlanCoverDaysEquationDTO(purchasePlanCoverDaysEquationDTO, rscIotRMPurchasePlanCoverDays);
    }

    default void setRMPurchasePlanCoverDaysEquationDTO(@MappingTarget PurchasePlanCoverDaysEquationDTO purchasePlanCoverDaysEquationDTO, RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays) {
        purchasePlanCoverDaysEquationDTO.getM1StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth1OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM1StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth1SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM2StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth2OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM2StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth2SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM3StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth3OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM3StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth3SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM4StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth4OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM4StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth4SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM5StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth5OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM5StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth5SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM6StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth6OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM6StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth6SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM7StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth7OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM7StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth7SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM8StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth8OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM8StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth8SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM9StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth9OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM9StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth9SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM10StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth10OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM10StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth10SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM11StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth11OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM11StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth11SuggestedPurchasePlan());
        purchasePlanCoverDaysEquationDTO.getM12StockEquationDTO().setStock(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth12OpeningStock());
        purchasePlanCoverDaysEquationDTO.getM12StockEquationDTO().setOrder(rscIotRMPurchasePlanCoverDays.getRmPurchasePlanCalculations().getMonth12SuggestedPurchasePlan());
    }

    default RscIotRMPurchasePlanCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays = new RscIotRMPurchasePlanCoverDays();
        rscIotRMPurchasePlanCoverDays.setId(id);
        return rscIotRMPurchasePlanCoverDays;
    }
}