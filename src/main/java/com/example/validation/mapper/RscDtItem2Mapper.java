package com.example.validation.mapper;

import com.example.validation.domain.RscDtCity;
import com.example.validation.domain.RscDtItem2;
import com.example.validation.dto.RscDtCityDTO;
import com.example.validation.dto.RscDtItem2DTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtItem2Mapper extends EntityMapper<RscDtItem2DTO, RscDtItem2> {
    RscDtItem2DTO toDto(RscDtItem2 rscDtItem2);

    RscDtItem2 toEntity(RscDtItem2DTO rscDtItem2DTO);

    default RscDtItem2 fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtItem2 rscDtItem2 = new RscDtItem2();
        rscDtItem2.setId(id);
        return rscDtItem2;
    }

}
