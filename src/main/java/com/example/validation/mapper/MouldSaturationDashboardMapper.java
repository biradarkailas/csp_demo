package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestTotalMouldSaturation;
import com.example.validation.dto.MouldSaturationDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class})
public interface MouldSaturationDashboardMapper extends EntityMapper<MouldSaturationDetailsDTO, RscIotLatestTotalMouldSaturation> {
    @Mapping(source = "rscDtMould.id", target = "rscDtMouldId")
    @Mapping(source = "rscDtMould.mould", target = "mouldName")
    @Mapping(source = "m1Value", target = "avgSaturationPercentage")
    MouldSaturationDetailsDTO toDto(RscIotLatestTotalMouldSaturation rscIotLatestTotalMouldSaturation);

    default RscIotLatestTotalMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestTotalMouldSaturation rscIotLatestTotalMouldSaturation = new RscIotLatestTotalMouldSaturation();
        rscIotLatestTotalMouldSaturation.setId(id);
        return rscIotLatestTotalMouldSaturation;
    }
}
