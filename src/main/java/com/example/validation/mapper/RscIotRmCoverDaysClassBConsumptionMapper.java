package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysClassBSummary;
import com.example.validation.dto.ClassWiseCoverDaysDetailsDTO;
import com.example.validation.dto.ClassWiseDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysClassBConsumptionMapper extends EntityMapper<ClassWiseDetailsDTO, RscIotRmCoverDaysClassBSummary> {
      @Mapping(source = "totalCoverDaysWithoutSit", target = "totalCoverDays")
    ClassWiseDetailsDTO toDto(RscIotRmCoverDaysClassBSummary rscIotRmCoverDaysClassBSummary);

    default RscIotRmCoverDaysClassBSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysClassBSummary rscIotRmCoverDaysClassBSummary = new RscIotRmCoverDaysClassBSummary();
        rscIotRmCoverDaysClassBSummary.setId(id);
        return rscIotRmCoverDaysClassBSummary;
    }

}
