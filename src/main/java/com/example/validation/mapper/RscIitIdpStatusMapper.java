package com.example.validation.mapper;

import com.example.validation.domain.RscIitIdpStatus;
import com.example.validation.dto.RscIitIdpStatusDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFileNameMapper.class, RscDtFolderNameMapper.class, RscDtTagMapper.class})
public interface RscIitIdpStatusMapper extends EntityMapper<RscIitIdpStatusDTO, RscIitIdpStatus> {

    @Mapping(source = "rscDtTag.id", target = "rscDtTagId")
    @Mapping(source = "rscDtTag.name", target = "tagName")
    @Mapping(source = "rscDtTag.description", target = "tagDescription")
    @Mapping(source = "rscDtFileName.id", target = "fileNameId")
    @Mapping(source = "rscDtFileName.fileName", target = "fileName")
    @Mapping(source = "rscDtFileName.extension", target = "extension")
    @Mapping(source = "rscDtFileName.rscDtFolderName.folderName", target = "folderName")
    @Mapping(source = "rscDtFileName.rscDtFolderName.id", target = "folderNameId")
    RscIitIdpStatusDTO toDto(RscIitIdpStatus rscIitIdpStatus);

    @Mapping(target = "rscDtTag", source = "rscDtTagId")
    @Mapping(target = "rscDtFileName", source = "fileNameId")
    RscIitIdpStatus toEntity(RscIitIdpStatusDTO rscIitIdpStatusDTO);

    default RscIitIdpStatus fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitIdpStatus rscIitIdpStatus = new RscIitIdpStatus();
        rscIitIdpStatus.setId(id);
        return rscIitIdpStatus;
    }
}
