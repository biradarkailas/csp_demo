package com.example.validation.mapper;

import com.example.validation.domain.RscIitSupplierWiseTransport;
import com.example.validation.dto.RscIitSupplierWiseTransportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtSailingDaysMapper.class, RscItItemWiseSupplierMapper.class, RscDtTransportModeMapper.class, RscDtTransportTypeMapper.class})
public interface RscIitSupplierWiseTransportMapper extends EntityMapper<RscIitSupplierWiseTransportDTO, RscIitSupplierWiseTransport> {

    @Mapping(source = "rscDtSailingDays.id", target = "rscDtSailingDaysId")
    @Mapping(source = "rscDtSailingDays.sailingDays", target = "sailingDays")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscDtTransportMode.id", target = "rscDtTransportModeId")
    @Mapping(source = "rscDtTransportMode.code", target = "rscDtTransportModeCode")
    @Mapping(source = "rscDtTransportType.id", target = "rscDtTransportTypeId")
    @Mapping(source = "rscDtTransportType.fullName", target = "transportTypeFullName")
    RscIitSupplierWiseTransportDTO toDto(RscIitSupplierWiseTransport rscIitSupplierWiseTransport);

    @Mapping(source = "rscDtSailingDaysId", target = "rscDtSailingDays")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscDtTransportModeId", target = "rscDtTransportMode")
    @Mapping(source = "rscDtTransportTypeId", target = "rscDtTransportType")
    RscIitSupplierWiseTransport toEntity(RscIitSupplierWiseTransportDTO rscIitRMItemWiseSupplierDetailsDTO);

    default RscIitSupplierWiseTransport fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitSupplierWiseTransport rscIitSupplierWiseTransport = new RscIitSupplierWiseTransport();
        rscIitSupplierWiseTransport.setId(id);
        return rscIitSupplierWiseTransport;
    }
}