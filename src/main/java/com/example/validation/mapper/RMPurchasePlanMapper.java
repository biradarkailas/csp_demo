package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMLatestPurchasePlan;
import com.example.validation.dto.supply.RMPurchasePlanDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscIitRMItemWiseSupplierDetailsMapper.class, RscItRMGrossConsumptionMapper.class, RscItItemWiseSupplierMapper.class,
        RscMtSupplierMapper.class, RscDtSupplierMapper.class, RscIitSupplierWiseTransportMapper.class, RscDtTransportTypeMapper.class, RscDtTechnicalSeriesMapper.class, RscDtMoqMapper.class})
public interface RMPurchasePlanMapper extends EntityMapper<RMPurchasePlanDTO, RscIotRMLatestPurchasePlan> {

    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    @Mapping(source = "rscMtItem.id", target = "materialId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "materialCode")
    @Mapping(source = "description", target = "materialDescription")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.id", target = "rscIitRMItemWiseSupplierDetailsId")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierCode", target = "supplierCode")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscIitSupplierWiseTransport.rscDtTransportType.aliasName", target = "category")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscDtTechnicalSeries.seriesValue", target = "packSize")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscDtMoq.moqValue", target = "moqValue")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.halalComments", target = "halalComments")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.otherComments", target = "otherComments")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.orderType", target = "orderType")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    @Mapping(source = "m1Value", target = "supplyMonthValues.month1.value")
    @Mapping(source = "m2Value", target = "supplyMonthValues.month2.value")
    @Mapping(source = "m3Value", target = "supplyMonthValues.month3.value")
    @Mapping(source = "m4Value", target = "supplyMonthValues.month4.value")
    @Mapping(source = "m5Value", target = "supplyMonthValues.month5.value")
    @Mapping(source = "m6Value", target = "supplyMonthValues.month6.value")
    @Mapping(source = "m7Value", target = "supplyMonthValues.month7.value")
    @Mapping(source = "m8Value", target = "supplyMonthValues.month8.value")
    @Mapping(source = "m9Value", target = "supplyMonthValues.month9.value")
    @Mapping(source = "m10Value", target = "supplyMonthValues.month10.value")
    @Mapping(source = "m11Value", target = "supplyMonthValues.month11.value")
    @Mapping(source = "m12Value", target = "supplyMonthValues.month12.value")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscDtContainerType.typeName", target = "containerType")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.allocationPercentage", target = "allocation")
    @Mapping(target = "totalValue", ignore = true)
    RMPurchasePlanDTO toDto(RscIotRMLatestPurchasePlan rscIotRMLatestPurchasePlan);

    @Mapping(source = "rscItRMGrossConsumptionId", target = "rscItRMGrossConsumption")
    @Mapping(source = "rscIitRMItemWiseSupplierDetailsId", target = "rscIitRMItemWiseSupplierDetails")
    @Mapping(source = "mpsId", target = "rscMtPPheader")
    @Mapping(source = "materialId", target = "rscMtItem")
    @Mapping(source = "supplyMonthValues.month1.value", target = "m1Value")
    @Mapping(source = "supplyMonthValues.month2.value", target = "m2Value")
    @Mapping(source = "supplyMonthValues.month3.value", target = "m3Value")
    @Mapping(source = "supplyMonthValues.month4.value", target = "m4Value")
    @Mapping(source = "supplyMonthValues.month5.value", target = "m5Value")
    @Mapping(source = "supplyMonthValues.month6.value", target = "m6Value")
    @Mapping(source = "supplyMonthValues.month7.value", target = "m7Value")
    @Mapping(source = "supplyMonthValues.month8.value", target = "m8Value")
    @Mapping(source = "supplyMonthValues.month9.value", target = "m9Value")
    @Mapping(source = "supplyMonthValues.month10.value", target = "m10Value")
    @Mapping(source = "supplyMonthValues.month11.value", target = "m11Value")
    @Mapping(source = "supplyMonthValues.month12.value", target = "m12Value")
    RscIotRMLatestPurchasePlan toEntity(RMPurchasePlanDTO rmPurchasePlanDTO);

    @AfterMapping
    default void calculateTotalValue(@MappingTarget RMPurchasePlanDTO rmPurchasePlanDTO) {
        rmPurchasePlanDTO.setTotalValue(getTotalValue(rmPurchasePlanDTO));
        rmPurchasePlanDTO.setAllocation(rmPurchasePlanDTO.getAllocation() * 100);
    }

    private Double getTotalValue(RMPurchasePlanDTO rmPurchasePlanDTO) {
        return rmPurchasePlanDTO.getSupplyMonthValues().getMonth1().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth2().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth3().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth4().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth5().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth6().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth7().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth8().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth9().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth10().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth11().getValue()
                + rmPurchasePlanDTO.getSupplyMonthValues().getMonth12().getValue();
    }

    default RscIotRMLatestPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMLatestPurchasePlan rscIotRMLatestPurchasePlan = new RscIotRMLatestPurchasePlan();
        rscIotRMLatestPurchasePlan.setId(id);
        return rscIotRMLatestPurchasePlan;
    }
}