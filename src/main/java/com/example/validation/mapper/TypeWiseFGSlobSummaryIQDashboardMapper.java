package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import com.example.validation.dto.slob.DivisionWiseFGSlobSummaryIQDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFGTypeMapper.class})
public interface TypeWiseFGSlobSummaryIQDashboardMapper extends EntityMapper<DivisionWiseFGSlobSummaryIQDashboardDTO, RscIotFGTypeSlobSummary> {

    @Mapping(source = "rscDtFGType.id", target = "typeId")
    @Mapping(source = "rscDtFGType.type", target = "typeName")
    @Mapping(source = "totalSlobValue", target = "slobValues.slobValue")
    @Mapping(source = "totalStockValue", target = "slobValues.stockValue")
    @Mapping(source = "stockQualityPercentage", target = "iqValue")
    DivisionWiseFGSlobSummaryIQDashboardDTO toDto(RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary);

    default RscIotFGTypeSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary = new RscIotFGTypeSlobSummary();
        rscIotFGTypeSlobSummary.setId(id);
        return rscIotFGTypeSlobSummary;
    }
}