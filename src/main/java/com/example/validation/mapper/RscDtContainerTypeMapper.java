package com.example.validation.mapper;


import com.example.validation.domain.RscDtContainerType;
import com.example.validation.dto.RscDtContainerTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtContainerTypeMapper extends EntityMapper<RscDtContainerTypeDTO, RscDtContainerType> {

    RscDtContainerTypeDTO toDto(RscDtContainerType rscDtContainerType);

    RscDtContainerType toEntity(RscDtContainerTypeDTO rscDtContainerTypeDTO);

    default RscDtContainerType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtContainerType rscDtContainerType = new RscDtContainerType();
        rscDtContainerType.setId(id);
        return rscDtContainerType;
    }
}
