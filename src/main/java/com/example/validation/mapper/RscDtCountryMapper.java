package com.example.validation.mapper;

import com.example.validation.domain.RscDtCountry;
import com.example.validation.dto.RscDtCountryDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtCountryMapper extends EntityMapper<RscDtCountryDTO, RscDtCountry> {
    RscDtCountryDTO toDto(RscDtCountry rscDtCountry);

    RscDtCountry toEntity(RscDtCountryDTO rscDtCountryDTO);

    default RscDtCountry fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtCountry rscDtCountry = new RscDtCountry();
        rscDtCountry.setId(id);
        return rscDtCountry;
    }
}
