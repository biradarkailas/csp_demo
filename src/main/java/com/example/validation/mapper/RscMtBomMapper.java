package com.example.validation.mapper;


import com.example.validation.domain.RscMtBom;
import com.example.validation.dto.RscMtBomDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscDtItemCategoryMapper.class, RscMtPPheaderMapper.class})
public interface RscMtBomMapper extends EntityMapper<RscMtBomDTO, RscMtBom> {

    @Mapping(source = "rscMtItemParent.id", target = "rscMtItemParentId")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.code", target = "parentItemCode")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.description", target = "parentItemDescription")
    @Mapping(source = "rscMtItemParent.rscDtItemCategory.description", target = "parentItemCategory")
    @Mapping(source = "rscMtItemChild.id", target = "rscMtItemChildId")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.code", target = "childItemCode")
    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    RscMtBomDTO toDto(RscMtBom rscMtBom);

    RscMtBom toEntity(RscMtBomDTO rscMtBomDTO);

    default RscMtBom fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtBom rscMtBom = new RscMtBom();
        rscMtBom.setId(id);
        return rscMtBom;
    }
}