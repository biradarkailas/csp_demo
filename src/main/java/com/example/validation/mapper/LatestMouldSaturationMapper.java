package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestMouldSaturation;
import com.example.validation.dto.RscIotLatestMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class, RscMtPPheaderMapper.class, RscIotPMLatestPurchasePlanMapper.class})
public interface LatestMouldSaturationMapper extends EntityMapper<RscIotLatestMouldSaturationDTO, RscIotLatestMouldSaturation> {
    @Mapping(source = "rscDtMould.id", target = "mould")
    @Mapping(source = "rscDtMould.mould", target = "mouldName")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscIotPMLatestPurchasePlan.id", target = "rscIotPMLatestPurchasePlanId")
    RscIotLatestMouldSaturationDTO toDto(RscIotLatestMouldSaturation rscIotLatestMouldSaturation);

    @Mapping(source = "mould", target = "rscDtMould")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader.id")
    @Mapping(source = "rscIotPMLatestPurchasePlanId", target = "rscIotPMLatestPurchasePlan.id")
    RscIotLatestMouldSaturation toEntity(RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturationDTO);

    default RscIotLatestMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestMouldSaturation rscIotLatestMouldSaturation = new RscIotLatestMouldSaturation();
        rscIotLatestMouldSaturation.setId(id);
        return rscIotLatestMouldSaturation;
    }
}
