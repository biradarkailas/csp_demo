package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmMes;
import com.example.validation.dto.IndividualValuesMesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPmMesMapper.class})
public interface IndividualValuesPmMesMapper extends EntityMapper<IndividualValuesMesDTO, RscMtPmMes> {
    @Mapping(source = "quantity", target = "value")
    IndividualValuesMesDTO toDto(RscMtPmMes rscMtPmMes);
}
