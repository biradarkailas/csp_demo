package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMStock;
import com.example.validation.dto.PmStockItemCodeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscDtSafetyStockMapper.class})
public interface PmStockItemCodeMapper extends EntityMapper<PmStockItemCodeDTO, RscIotPMStock> {
    @Mapping(source = "rscMtItem.id", target = "id")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    PmStockItemCodeDTO toDto(RscIotPMStock rscIotPMStock);

    default RscIotPMStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMStock rscIotPMStock = new RscIotPMStock();
        rscIotPMStock.setId(id);
        return rscIotPMStock;
    }
}
