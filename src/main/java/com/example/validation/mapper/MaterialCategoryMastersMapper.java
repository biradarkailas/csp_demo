package com.example.validation.mapper;

import com.example.validation.domain.RscMtItem;
import com.example.validation.dto.RscDtItemTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtItemTypeMapper.class})
public interface MaterialCategoryMastersMapper extends EntityMapper<RscDtItemTypeDTO, RscMtItem> {
    @Mapping(source = "rscDtItemType.id", target = "id")
    @Mapping(source = "rscDtItemType.name", target = "itemTypeDescription")
    RscDtItemTypeDTO toDto(RscMtItem rscMtItem);

    default RscMtItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtItem rscMtItem = new RscMtItem();
        rscMtItem.setId(id);
        return rscMtItem;
    }
}
