package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMLatestPurchasePlan;
import com.example.validation.dto.supply.RscIotRMPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class, RscIitRMItemWiseSupplierDetailsMapper.class, RscItRMGrossConsumptionMapper.class})
public interface RscIotRMLatestPurchasePlanMapper extends EntityMapper<RscIotRMPurchasePlanDTO, RscIotRMLatestPurchasePlan> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.id", target = "rscIitRMItemWiseSupplierDetailsId")
    @Mapping(source = "rscItRMGrossConsumption.id", target = "rscItRMGrossConsumptionId")
    RscIotRMPurchasePlanDTO toDto(RscIotRMLatestPurchasePlan rscIotRMLatestPurchasePlan);

    @Mapping(source = "rscItRMGrossConsumptionId", target = "rscItRMGrossConsumption")
    @Mapping(source = "rscIitRMItemWiseSupplierDetailsId", target = "rscIitRMItemWiseSupplierDetails")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotRMLatestPurchasePlan toEntity(RscIotRMPurchasePlanDTO rscIotRMPurchasePlanDTO);

    default RscIotRMLatestPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMLatestPurchasePlan rscIotRMLatestPurchasePlan = new RscIotRMLatestPurchasePlan();
        rscIotRMLatestPurchasePlan.setId(id);
        return rscIotRMLatestPurchasePlan;
    }
}