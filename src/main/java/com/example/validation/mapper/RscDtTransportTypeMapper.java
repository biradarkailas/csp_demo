package com.example.validation.mapper;

import com.example.validation.domain.RscDtTransportType;
import com.example.validation.dto.RscDtTransportTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtTransportTypeMapper extends EntityMapper<RscDtTransportTypeDTO, RscDtTransportType> {
    RscDtTransportTypeDTO toDto(RscDtTransportType rscDtTransportType);

    RscDtTransportType toEntity(RscDtTransportTypeDTO rscDtTransportTypeDTO);

    default RscDtTransportType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtTransportType rscDtTransportType = new RscDtTransportType();
        rscDtTransportType.setId(id);
        return rscDtTransportType;
    }

}
