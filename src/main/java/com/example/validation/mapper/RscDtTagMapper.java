package com.example.validation.mapper;

import com.example.validation.domain.RscDtTag;
import com.example.validation.dto.RscDtTagDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtTagMapper extends EntityMapper<RscDtTagDTO, RscDtTag> {
    RscDtTagDTO toDto(RscDtTag rscDtTag);

    RscDtTag toEntity(RscDtTagDTO rscDtTagDTO);

    default RscDtTag fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtTag rscDtTag = new RscDtTag();
        rscDtTag.setId(id);
        return rscDtTag;
    }
}
