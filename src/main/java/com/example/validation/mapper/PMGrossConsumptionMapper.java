package com.example.validation.mapper;

import com.example.validation.domain.RscItPMGrossConsumption;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscItItemWiseSupplierMapper.class, RscDtSupplierMapper.class})
public interface PMGrossConsumptionMapper extends EntityMapper<GrossConsumptionDTO, RscItPMGrossConsumption> {

    @Mapping(source = "rscMtItem.id", target = "materialId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "materialCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "materialDescription")
    @Mapping(source = "m1Value", target = "grossConsumptionMonthValues.month1.value")
    @Mapping(source = "m2Value", target = "grossConsumptionMonthValues.month2.value")
    @Mapping(source = "m3Value", target = "grossConsumptionMonthValues.month3.value")
    @Mapping(source = "m4Value", target = "grossConsumptionMonthValues.month4.value")
    @Mapping(source = "m5Value", target = "grossConsumptionMonthValues.month5.value")
    @Mapping(source = "m6Value", target = "grossConsumptionMonthValues.month6.value")
    @Mapping(source = "m7Value", target = "grossConsumptionMonthValues.month7.value")
    @Mapping(source = "m8Value", target = "grossConsumptionMonthValues.month8.value")
    @Mapping(source = "m9Value", target = "grossConsumptionMonthValues.month9.value")
    @Mapping(source = "m10Value", target = "grossConsumptionMonthValues.month10.value")
    @Mapping(source = "m11Value", target = "grossConsumptionMonthValues.month11.value")
    @Mapping(source = "m12Value", target = "grossConsumptionMonthValues.month12.value")
    @Mapping(source = "rscMtItem.rscDtItemType.name", target = "category")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "itemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierCode", target = "supplierCode")
    GrossConsumptionDTO toDto(RscItPMGrossConsumption rscItPmGrossConsumption);

    default RscItPMGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItPMGrossConsumption rscItPmGrossConsumption = new RscItPMGrossConsumption();
        rscItPmGrossConsumption.setId(id);
        return rscItPmGrossConsumption;
    }
}