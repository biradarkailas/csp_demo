package com.example.validation.mapper;

import com.example.validation.domain.RscMtPPdetails;
import com.example.validation.dto.RscMtPPdetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class,
        RscDtProductionTypeMapper.class, RscDtLinesMapper.class, RscDtSkidsMapper.class,
        RscDtItemCodeMapper.class, RscDtDivisionMapper.class, RscDtBrandMapper.class, RscDtSignatureMapper.class, RscDtMaterialCategoryMapper.class, RscDtMaterialSubCategoryMapper.class})
public interface RscMtPPdetailsMapper extends EntityMapper<RscMtPPdetailsDTO, RscMtPPdetails> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsName", target = "mpsName")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtProductionType.id", target = "rscDtProductionTypeId")
    @Mapping(source = "rscDtProductionType.name", target = "productionType")
    @Mapping(source = "rscDtLines.id", target = "rscDtLinesId")
    @Mapping(source = "rscDtLines.name", target = "lineName")
    @Mapping(source = "rscDtSkids.id", target = "rscDtSkidsId")
    @Mapping(source = "rscDtSkids.name", target = "skidName")
    @Mapping(source = "rscDtDivision.id", target = "rscDtDivisionId")
    @Mapping(source = "rscDtDivision.aliasName", target = "divisionName")
    @Mapping(source = "rscDtSignature.id", target = "rscDtSignatureId")
    @Mapping(source = "rscDtSignature.name", target = "signatureName")
    @Mapping(source = "rscDtBrand.id", target = "rscDtBrandId")
    @Mapping(source = "rscDtBrand.name", target = "brandName")
    @Mapping(source = "rscDtMaterialCategory.id", target = "rscDtMaterialCategoryId")
    @Mapping(source = "rscDtMaterialCategory.name", target = "materialCategoryName")
    @Mapping(source = "rscDtMaterialSubCategory.id", target = "rscDtMaterialSubCategoryId")
    @Mapping(source = "rscDtMaterialSubCategory.name", target = "materialSubCategoryName")
    RscMtPPdetailsDTO toDto(RscMtPPdetails rscMtPPdetails);

    RscMtPPdetails toEntity(RscMtPPdetailsDTO rscMtPPdetailsDTO);

    default RscMtPPdetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPPdetails rscMtPPdetails = new RscMtPPdetails();
        rscMtPPdetails.setId(id);
        return rscMtPPdetails;
    }
}