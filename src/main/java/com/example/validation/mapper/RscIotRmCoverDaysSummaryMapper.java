package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysSummary;
import com.example.validation.dto.RscIotRmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysSummaryMapper extends EntityMapper<RscIotRmCoverDaysSummaryDTO, RscIotRmCoverDaysSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    RscIotRmCoverDaysSummaryDTO toDto(RscIotRmCoverDaysSummary rscIotRmCoverDaysSummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    RscIotRmCoverDaysSummary toEntity(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO);

    default RscIotRmCoverDaysSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysSummary rscIotRmCoverDaysSummary = new RscIotRmCoverDaysSummary();
        rscIotRmCoverDaysSummary.setId(id);
        return rscIotRmCoverDaysSummary;
    }
}
