package com.example.validation.mapper;

import com.example.validation.domain.RscIotOriginalTotalMouldSaturation;
import com.example.validation.dto.RscIotLatestTotalMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class, RscMtPPheaderMapper.class, RscIotPMLatestPurchasePlanMapper.class})
public interface OriginalTotalMouldSaturationMapper extends EntityMapper<RscIotLatestTotalMouldSaturationDTO, RscIotOriginalTotalMouldSaturation> {
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtMouldId", target = "rscDtMould")
    RscIotOriginalTotalMouldSaturation toEntity(RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO);

    default RscIotOriginalTotalMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotOriginalTotalMouldSaturation rscIotOriginalTotalMouldSaturation = new RscIotOriginalTotalMouldSaturation();
        rscIotOriginalTotalMouldSaturation.setId(id);
        return rscIotOriginalTotalMouldSaturation;
    }
}
