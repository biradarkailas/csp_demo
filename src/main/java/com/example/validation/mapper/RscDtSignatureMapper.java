package com.example.validation.mapper;

import com.example.validation.domain.RscDtSignature;
import com.example.validation.dto.RscDtSignatureDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})

public interface RscDtSignatureMapper extends EntityMapper<RscDtSignatureDTO, RscDtSignature> {

    RscDtSignatureDTO toDto(RscDtSignature rscDtSignature);

    RscDtSignature toEntity(RscDtSignatureDTO rscDtSignatureDTO);

    default RscDtSignature fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtSignature rscDtSignature = new RscDtSignature();
        rscDtSignature.setId(id);
        return rscDtSignature;
    }
}
