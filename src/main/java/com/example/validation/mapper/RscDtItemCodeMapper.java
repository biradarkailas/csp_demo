package com.example.validation.mapper;

import com.example.validation.domain.RscDtItemCode;
import com.example.validation.dto.RscDtItemCodeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtItemCodeMapper extends EntityMapper<RscDtItemCodeDTO, RscDtItemCode> {
    RscDtItemCodeDTO toDto(RscDtItemCode rscDtItemCode);

    RscDtItemCode toEntity(RscDtItemCodeDTO rscDtItemCodeDTO);

    default RscDtItemCode fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtItemCode rscDtItemCode = new RscDtItemCode();
        rscDtItemCode.setId(id);
        return rscDtItemCode;
    }
}
