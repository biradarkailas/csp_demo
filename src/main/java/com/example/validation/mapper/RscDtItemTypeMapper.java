package com.example.validation.mapper;


import com.example.validation.domain.RscDtItemType;
import com.example.validation.dto.RscDtItemTypeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtItemTypeMapper extends EntityMapper<RscDtItemTypeDTO, RscDtItemType> {
    @Mapping(source = "name", target = "itemTypeDescription")
    RscDtItemTypeDTO toDto(RscDtItemType rscDtItemType);

    RscDtItemType toEntity(RscDtItemTypeDTO rscDtItemTypeDTO);

    default RscDtItemType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtItemType rscDtItemType = new RscDtItemType();
        rscDtItemType.setId(id);
        return rscDtItemType;
    }
}
