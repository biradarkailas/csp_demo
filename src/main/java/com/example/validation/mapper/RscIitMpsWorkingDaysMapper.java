package com.example.validation.mapper;

import com.example.validation.domain.RscIitMpsWorkingDays;
import com.example.validation.dto.RscIitMpsWorkingDaysDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscIitMpsWorkingDaysMapper extends EntityMapper<RscIitMpsWorkingDaysDTO, RscIitMpsWorkingDays> {
    RscIitMpsWorkingDaysDTO toDto(RscIitMpsWorkingDays rscIitMpsWorkingDays);

    RscIitMpsWorkingDays toEntity(RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO);

    default RscIitMpsWorkingDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitMpsWorkingDays rscIitMpsWorkingDays = new RscIitMpsWorkingDays();
        rscIitMpsWorkingDays.setId(id);
        return rscIitMpsWorkingDays;
    }
}
