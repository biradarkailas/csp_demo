package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestTotalMouldSaturation;
import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.domain.RscIotOriginalTotalMouldSaturation;
import com.example.validation.dto.RscIotLatestTotalMouldSaturationDTO;
import com.example.validation.dto.RscIotOriginalTotalMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class,RscDtMouldMapper.class})
public interface RscIotLatestTotalMouldSaturationMapper extends EntityMapper<RscIotLatestTotalMouldSaturationDTO, RscIotLatestTotalMouldSaturation> {

    @Mapping(source = "rscDtMould.mould", target = "mould")
    @Mapping(source = "rscDtMould.totalCapacity", target = "capacity")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtMould.id", target = "rscDtMouldId")
    RscIotLatestTotalMouldSaturationDTO toDto(RscIotLatestTotalMouldSaturation rscIotLatestTotalMouldSaturation);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtMouldId", target = "rscDtMould")
    RscIotLatestTotalMouldSaturation toEntity(RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO);

    default RscIotLatestTotalMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestTotalMouldSaturation rscIotLatestTotalMouldSaturation = new RscIotLatestTotalMouldSaturation();
        rscIotLatestTotalMouldSaturation.setId(id);
        return rscIotLatestTotalMouldSaturation;
    }
}
