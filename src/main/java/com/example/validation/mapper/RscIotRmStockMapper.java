package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmStock;
import com.example.validation.dto.RscIotRmStockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtStockTypeMapper.class, RscDtMapMapper.class, RscDtItemCodeMapper.class})
public interface RscIotRmStockMapper extends EntityMapper<RscIotRmStockDTO, RscIotRmStock> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscDtStockType.id", target = "rscDtStockTypeId")
    @Mapping(source = "rscDtStockType.tag", target = "stockTypeTag")
    @Mapping(source = "rscDtStockType.aliasName", target = "stockTypeAliasName")
    @Mapping(source = "rscMtItem.rscDtMap.mapPrice", target = "mapPrice")
    @Mapping(source = "rscMtItem.rscDtStdPrice.stdPrice", target = "stdPrice")
    RscIotRmStockDTO toDto(RscIotRmStock rscIotRmStock);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscDtStockTypeId", target = "rscDtStockType")
    RscIotRmStock toEntity(RscIotRmStockDTO rscIotRmStockDTO);

    default RscIotRmStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmStock rscIotRmStock = new RscIotRmStock();
        rscIotRmStock.setId(id);
        return rscIotRmStock;
    }
}