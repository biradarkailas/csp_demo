package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMPurchasePlanCalculations;
import com.example.validation.dto.supply.RmPurchasePlanCalculationDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscIitRMItemWiseSupplierDetailsMapper.class, RscItItemWiseSupplierMapper.class,
        RscMtSupplierMapper.class, RscDtSafetyStockMapper.class, RscDtCoverDaysMapper.class, RscDtMoqMapper.class, RscDtTechnicalSeriesMapper.class, RscItRMGrossConsumptionMapper.class})
public interface RmPurchasePlanCalculationMapper extends EntityMapper<RmPurchasePlanCalculationDTO, RscIotRMPurchasePlanCalculations> {

    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "materialCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "materialDescription")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    @Mapping(source = "rscMtItem.rscDtCoverDays.coverDays", target = "coverDays")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscDtMoq.moqValue", target = "moq")
    @Mapping(source = "rscIitRMItemWiseSupplierDetails.rscItItemWiseSupplier.rscDtTechnicalSeries.seriesValue", target = "packSize")
    @Mapping(source = "month1OpeningStock", target = "month1PurchasePlanDTO.openingStock")
    @Mapping(source = "month1StockAfterConsumption", target = "month1PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month1OpenPoStock", target = "month1PurchasePlanDTO.openPoStock")
    @Mapping(source = "month1ClosingStock", target = "month1PurchasePlanDTO.closingStock")
    @Mapping(source = "month1SuggestedPurchasePlan", target = "month1PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month1ObsoleteStock", target = "month1PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month2OpeningStock", target = "month2PurchasePlanDTO.openingStock")
    @Mapping(source = "month2StockAfterConsumption", target = "month2PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month2OpenPoStock", target = "month2PurchasePlanDTO.openPoStock")
    @Mapping(source = "month2ClosingStock", target = "month2PurchasePlanDTO.closingStock")
    @Mapping(source = "month2SuggestedPurchasePlan", target = "month2PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month2ObsoleteStock", target = "month2PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month3OpeningStock", target = "month3PurchasePlanDTO.openingStock")
    @Mapping(source = "month3StockAfterConsumption", target = "month3PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month3OpenPoStock", target = "month3PurchasePlanDTO.openPoStock")
    @Mapping(source = "month3ClosingStock", target = "month3PurchasePlanDTO.closingStock")
    @Mapping(source = "month3SuggestedPurchasePlan", target = "month3PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month3ObsoleteStock", target = "month3PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month4OpeningStock", target = "month4PurchasePlanDTO.openingStock")
    @Mapping(source = "month4StockAfterConsumption", target = "month4PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month4OpenPoStock", target = "month4PurchasePlanDTO.openPoStock")
    @Mapping(source = "month4ClosingStock", target = "month4PurchasePlanDTO.closingStock")
    @Mapping(source = "month4SuggestedPurchasePlan", target = "month4PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month4ObsoleteStock", target = "month4PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month5OpeningStock", target = "month5PurchasePlanDTO.openingStock")
    @Mapping(source = "month5StockAfterConsumption", target = "month5PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month5OpenPoStock", target = "month5PurchasePlanDTO.openPoStock")
    @Mapping(source = "month5ClosingStock", target = "month5PurchasePlanDTO.closingStock")
    @Mapping(source = "month5SuggestedPurchasePlan", target = "month5PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month5ObsoleteStock", target = "month5PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month6OpeningStock", target = "month6PurchasePlanDTO.openingStock")
    @Mapping(source = "month6StockAfterConsumption", target = "month6PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month6OpenPoStock", target = "month6PurchasePlanDTO.openPoStock")
    @Mapping(source = "month6ClosingStock", target = "month6PurchasePlanDTO.closingStock")
    @Mapping(source = "month6SuggestedPurchasePlan", target = "month6PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month6ObsoleteStock", target = "month6PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month7OpeningStock", target = "month7PurchasePlanDTO.openingStock")
    @Mapping(source = "month7StockAfterConsumption", target = "month7PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month7OpenPoStock", target = "month7PurchasePlanDTO.openPoStock")
    @Mapping(source = "month7ClosingStock", target = "month7PurchasePlanDTO.closingStock")
    @Mapping(source = "month7SuggestedPurchasePlan", target = "month7PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month7ObsoleteStock", target = "month7PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month8OpeningStock", target = "month8PurchasePlanDTO.openingStock")
    @Mapping(source = "month8StockAfterConsumption", target = "month8PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month8OpenPoStock", target = "month8PurchasePlanDTO.openPoStock")
    @Mapping(source = "month8ClosingStock", target = "month8PurchasePlanDTO.closingStock")
    @Mapping(source = "month8SuggestedPurchasePlan", target = "month8PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month8ObsoleteStock", target = "month8PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month9OpeningStock", target = "month9PurchasePlanDTO.openingStock")
    @Mapping(source = "month9StockAfterConsumption", target = "month9PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month9OpenPoStock", target = "month9PurchasePlanDTO.openPoStock")
    @Mapping(source = "month9ClosingStock", target = "month9PurchasePlanDTO.closingStock")
    @Mapping(source = "month9SuggestedPurchasePlan", target = "month9PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month9ObsoleteStock", target = "month9PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month10OpeningStock", target = "month10PurchasePlanDTO.openingStock")
    @Mapping(source = "month10StockAfterConsumption", target = "month10PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month10OpenPoStock", target = "month10PurchasePlanDTO.openPoStock")
    @Mapping(source = "month10ClosingStock", target = "month10PurchasePlanDTO.closingStock")
    @Mapping(source = "month10SuggestedPurchasePlan", target = "month10PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month10ObsoleteStock", target = "month10PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month11OpeningStock", target = "month11PurchasePlanDTO.openingStock")
    @Mapping(source = "month11StockAfterConsumption", target = "month11PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month11OpenPoStock", target = "month11PurchasePlanDTO.openPoStock")
    @Mapping(source = "month11ClosingStock", target = "month11PurchasePlanDTO.closingStock")
    @Mapping(source = "month11SuggestedPurchasePlan", target = "month11PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month11ObsoleteStock", target = "month11PurchasePlanDTO.obsoleteStock")
    @Mapping(source = "month12OpeningStock", target = "month12PurchasePlanDTO.openingStock")
    @Mapping(source = "month12StockAfterConsumption", target = "month12PurchasePlanDTO.stockAfterConsumption")
    @Mapping(source = "month12OpenPoStock", target = "month12PurchasePlanDTO.openPoStock")
    @Mapping(source = "month12ClosingStock", target = "month12PurchasePlanDTO.closingStock")
    @Mapping(source = "month12SuggestedPurchasePlan", target = "month12PurchasePlanDTO.suggestedPurchasePlan")
    @Mapping(source = "month12ObsoleteStock", target = "month12PurchasePlanDTO.obsoleteStock")
    RmPurchasePlanCalculationDTO toDto(RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations);

    @AfterMapping
    default void updateRMPurchasePlanCalculationDTO(@MappingTarget RmPurchasePlanCalculationDTO rmPurchasePlanCalculationDTO, RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations) {
        setRMGrossConsumptionCurrentMonth(rmPurchasePlanCalculationDTO, rscIotRMPurchasePlanCalculations);
        setCoverDays(rmPurchasePlanCalculationDTO, rscIotRMPurchasePlanCalculations);
    }

    default void setRMGrossConsumptionCurrentMonth(@MappingTarget RmPurchasePlanCalculationDTO rmPurchasePlanCalculationDTO, RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations) {
        rmPurchasePlanCalculationDTO.getMonth1PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM1Value());
        rmPurchasePlanCalculationDTO.getMonth2PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM2Value());
        rmPurchasePlanCalculationDTO.getMonth3PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM3Value());
        rmPurchasePlanCalculationDTO.getMonth4PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM4Value());
        rmPurchasePlanCalculationDTO.getMonth5PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM5Value());
        rmPurchasePlanCalculationDTO.getMonth6PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM6Value());
        rmPurchasePlanCalculationDTO.getMonth7PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM7Value());
        rmPurchasePlanCalculationDTO.getMonth8PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM8Value());
        rmPurchasePlanCalculationDTO.getMonth9PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM9Value());
        rmPurchasePlanCalculationDTO.getMonth10PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM10Value());
        rmPurchasePlanCalculationDTO.getMonth11PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM11Value());
        rmPurchasePlanCalculationDTO.getMonth12PurchasePlanDTO().setConsumptionAtCurrentMonth(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM12Value());
    }

    default void setCoverDays(@MappingTarget RmPurchasePlanCalculationDTO rmSupplyCalculationsDTO, RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations) {
        rmSupplyCalculationsDTO.getMonth1PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM1Value());
        rmSupplyCalculationsDTO.getMonth2PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM2Value());
        rmSupplyCalculationsDTO.getMonth3PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM3Value());
        rmSupplyCalculationsDTO.getMonth4PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM4Value());
        rmSupplyCalculationsDTO.getMonth5PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM5Value());
        rmSupplyCalculationsDTO.getMonth6PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM6Value());
        rmSupplyCalculationsDTO.getMonth7PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM7Value());
        rmSupplyCalculationsDTO.getMonth8PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM8Value());
        rmSupplyCalculationsDTO.getMonth9PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM9Value());
        rmSupplyCalculationsDTO.getMonth10PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM10Value());
        rmSupplyCalculationsDTO.getMonth11PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM11Value());
        rmSupplyCalculationsDTO.getMonth12PurchasePlanDTO().setCoverDays(rscIotRMPurchasePlanCalculations.getRscIotRMPurchasePlanCoverDays().getM12Value());
    }

    default RscIotRMPurchasePlanCalculations fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations = new RscIotRMPurchasePlanCalculations();
        rscIotRMPurchasePlanCalculations.setId(id);
        return rscIotRMPurchasePlanCalculations;
    }
}