package com.example.validation.mapper;

import com.example.validation.domain.RscIotMPSPlanTotalQuantity;
import com.example.validation.dto.RscIotMPSPlanTotalQuantityDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotMPSPlanTotalQuantityMapper extends EntityMapper<RscIotMPSPlanTotalQuantityDTO, RscIotMPSPlanTotalQuantity> {

    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtPPheader.mpsName", target = "mpsName")
    RscIotMPSPlanTotalQuantityDTO toDto(RscIotMPSPlanTotalQuantity rscIotMPSPlanTotalQuantity);

    @Mapping(source = "mpsId", target = "rscMtPPheader")
    RscIotMPSPlanTotalQuantity toEntity(RscIotMPSPlanTotalQuantityDTO rscIotMPSPlanTotalQuantityDTO);

    default RscIotMPSPlanTotalQuantity fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotMPSPlanTotalQuantity rscIotMPSPlanTotalQuantity = new RscIotMPSPlanTotalQuantity();
        rscIotMPSPlanTotalQuantity.setId(id);
        return rscIotMPSPlanTotalQuantity;
    }
}