package com.example.validation.mapper;


import com.example.validation.domain.RscDtTechnicalSeries;
import com.example.validation.dto.RscDtTechnicalSeriesDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtTechnicalSeriesMapper extends EntityMapper<RscDtTechnicalSeriesDTO, RscDtTechnicalSeries> {

    RscDtTechnicalSeriesDTO toDto(RscDtTechnicalSeries rscDtTechnicalSeries);

    RscDtTechnicalSeries toEntity(RscDtTechnicalSeriesDTO rscDtTechnicalSeriesDTO);

    default RscDtTechnicalSeries fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtTechnicalSeries rscDtTechnicalSeries = new RscDtTechnicalSeries();
        rscDtTechnicalSeries.setId(id);
        return rscDtTechnicalSeries;
    }
}