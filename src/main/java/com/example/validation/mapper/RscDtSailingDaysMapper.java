package com.example.validation.mapper;

import com.example.validation.domain.RscDtSailingDays;
import com.example.validation.dto.RscDtSailingDaysDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtSailingDaysMapper extends EntityMapper<RscDtSailingDaysDTO, RscDtSailingDays> {
    RscDtSailingDaysDTO toDto(RscDtSailingDays rscDtSailingDays);

    RscDtSailingDays toEntity(RscDtSailingDaysDTO rscDtSailingDaysDTO);

    default RscDtSailingDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtSailingDays rscDtSailingDays = new RscDtSailingDays();
        rscDtSailingDays.setId(id);
        return rscDtSailingDays;
    }
}
