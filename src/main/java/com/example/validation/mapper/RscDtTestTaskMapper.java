package com.example.validation.mapper;

import com.example.validation.domain.RscDtTestTask;
import com.example.validation.dto.RscDtTestTaskDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtTestTaskMapper extends EntityMapper<RscDtTestTaskDTO,RscDtTestTask>{
    RscDtTestTaskDTO toDto(RscDtTestTask rscDtTestTask);

    RscDtTestTask toEntity(RscDtTestTaskDTO rscDtTestTaskDTO);

    default RscDtTestTask fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtTestTask rscDtTestTask = new RscDtTestTask();
        rscDtTestTask.setId(id);
        return rscDtTestTask;
    }
}
