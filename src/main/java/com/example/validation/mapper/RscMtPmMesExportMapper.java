package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmMes;
import com.example.validation.dto.RscMtPmMesExportDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscMtPmMesExportMapper extends EntityMapper<RscMtPmMesExportDto, RscMtPmMes>{

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    RscMtPmMesExportDto toDto(RscMtPmMes rscMtPmMes);

    RscMtPmMes toEntity(RscMtPmMesExportDto rscMtPmMesExportDto);

    default RscMtPmMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPmMes rscMtPmMes = new RscMtPmMes();
        rscMtPmMes.setId(id);
        return rscMtPmMes;
    }
}
