package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgSkidSaturation;
import com.example.validation.dto.FgSkidSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtSkidsMapper.class})
public interface FgSkidSaturationMapper extends EntityMapper<FgSkidSaturationDTO, RscIotFgSkidSaturation> {
    @Mapping(source = "id", target = "rscIotFgSkidSaturationDTO.id")
    @Mapping(source = "rscMtPPheader.id", target = "rscIotFgSkidSaturationDTO.rscMtPPheaderId")
    @Mapping(source = "rscDtSkids.id", target = "rscIotFgSkidSaturationDTO.rscDtSkidsId")
    @Mapping(source = "rscDtSkids.name", target = "skidName")
    @Mapping(source = "m1TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m1TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m2TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m2TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m3TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m3TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m4TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m4TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m5TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m5TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m6TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m6TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m7TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m7TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m8TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m8TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m9TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m9TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m10TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m10TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m11TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m11TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "m12TotalRatioOfPlanQtyWithTs", target = "rscIotFgSkidSaturationDTO.m12TotalRatioOfPlanQtyWithTs")
    @Mapping(source = "month1Capacity", target = "rscIotFgSkidSaturationDTO.month1Capacity")
    @Mapping(source = "month2Capacity", target = "rscIotFgSkidSaturationDTO.month2Capacity")
    @Mapping(source = "month3Capacity", target = "rscIotFgSkidSaturationDTO.month3Capacity")
    @Mapping(source = "month4Capacity", target = "rscIotFgSkidSaturationDTO.month4Capacity")
    @Mapping(source = "month5Capacity", target = "rscIotFgSkidSaturationDTO.month5Capacity")
    @Mapping(source = "month6Capacity", target = "rscIotFgSkidSaturationDTO.month6Capacity")
    @Mapping(source = "month7Capacity", target = "rscIotFgSkidSaturationDTO.month7Capacity")
    @Mapping(source = "month8Capacity", target = "rscIotFgSkidSaturationDTO.month8Capacity")
    @Mapping(source = "month9Capacity", target = "rscIotFgSkidSaturationDTO.month9Capacity")
    @Mapping(source = "month10Capacity", target = "rscIotFgSkidSaturationDTO.month10Capacity")
    @Mapping(source = "month11Capacity", target = "rscIotFgSkidSaturationDTO.month11Capacity")
    @Mapping(source = "month12Capacity", target = "rscIotFgSkidSaturationDTO.month12Capacity")
    @Mapping(source = "m1SkidSaturation", target = "rscIotFgSkidSaturationDTO.m1SkidSaturation")
    @Mapping(source = "m2SkidSaturation", target = "rscIotFgSkidSaturationDTO.m2SkidSaturation")
    @Mapping(source = "m3SkidSaturation", target = "rscIotFgSkidSaturationDTO.m3SkidSaturation")
    @Mapping(source = "m4SkidSaturation", target = "rscIotFgSkidSaturationDTO.m4SkidSaturation")
    @Mapping(source = "m5SkidSaturation", target = "rscIotFgSkidSaturationDTO.m5SkidSaturation")
    @Mapping(source = "m6SkidSaturation", target = "rscIotFgSkidSaturationDTO.m6SkidSaturation")
    @Mapping(source = "m7SkidSaturation", target = "rscIotFgSkidSaturationDTO.m7SkidSaturation")
    @Mapping(source = "m8SkidSaturation", target = "rscIotFgSkidSaturationDTO.m8SkidSaturation")
    @Mapping(source = "m9SkidSaturation", target = "rscIotFgSkidSaturationDTO.m9SkidSaturation")
    @Mapping(source = "m10SkidSaturation", target = "rscIotFgSkidSaturationDTO.m10SkidSaturation")
    @Mapping(source = "m11SkidSaturation", target = "rscIotFgSkidSaturationDTO.m11SkidSaturation")
    @Mapping(source = "m12SkidSaturation", target = "rscIotFgSkidSaturationDTO.m12SkidSaturation")
    FgSkidSaturationDTO toDto(RscIotFgSkidSaturation rscIotFgSkidSaturation);

    default RscIotFgSkidSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgSkidSaturation rscIotFgSkidSaturation = new RscIotFgSkidSaturation();
        rscIotFgSkidSaturation.setId(id);
        return rscIotFgSkidSaturation;
    }
}
