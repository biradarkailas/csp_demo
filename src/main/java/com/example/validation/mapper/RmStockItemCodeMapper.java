package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmStock;
import com.example.validation.dto.RmStockItemCodeDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscDtSafetyStockMapper.class})
public interface RmStockItemCodeMapper extends EntityMapper<RmStockItemCodeDTO, RscIotRmStock> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    RmStockItemCodeDTO toDto(RscIotRmStock rscIotRmStock);

    default RscIotRmStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmStock rscIotRmStock = new RscIotRmStock();
        rscIotRmStock.setId(id);
        return rscIotRmStock;
    }
}
