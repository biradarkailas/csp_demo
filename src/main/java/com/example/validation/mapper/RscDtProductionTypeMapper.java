package com.example.validation.mapper;

import com.example.validation.domain.RscDtProductionType;
import com.example.validation.dto.RscDtProductionTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtProductionTypeMapper extends EntityMapper<RscDtProductionTypeDTO, RscDtProductionType> {

    RscDtProductionTypeDTO toDto(RscDtProductionType rscDtProductionType);

    RscDtProductionType toEntity(RscDtProductionTypeDTO rscDtProductionTypeDTO);

    default RscDtProductionType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtProductionType rscDtProductionType = new RscDtProductionType();
        rscDtProductionType.setId(id);
        return rscDtProductionType;
    }
}
