package com.example.validation.mapper;

import com.example.validation.domain.RscItBulkGrossConsumption;
import com.example.validation.dto.ItemCodeDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface BulkItemCodedetailsMapper extends EntityMapper<ItemCodeDetailDTO, RscItBulkGrossConsumption> {

    @Mapping(source = "rscMtItem.id", target = "itemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    ItemCodeDetailDTO toDto(RscItBulkGrossConsumption rscItBulkGrossConsumption);

    default RscItBulkGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItBulkGrossConsumption rscItBulkGrossConsumption = new RscItBulkGrossConsumption();
        rscItBulkGrossConsumption.setId(id);
        return rscItBulkGrossConsumption;
    }
}