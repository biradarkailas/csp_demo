package com.example.validation.mapper;

import com.example.validation.domain.RscMtPPdetails;
import com.example.validation.dto.ItemCodeDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface FgItemCodedetailsMapper extends EntityMapper<ItemCodeDetailDTO, RscMtPPdetails> {

    @Mapping(source = "rscMtItem.id", target = "itemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    ItemCodeDetailDTO toDto(RscMtPPdetails rscMtPPdetails);

    default RscMtPPdetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPPdetails rscMtPPdetails = new RscMtPPdetails();
        rscMtPPdetails.setId(id);
        return rscMtPPdetails;
    }
}