package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmRtd;
import com.example.validation.dto.RscMtRmRtdDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtRmRtdMapper extends EntityMapper<RscMtRmRtdDto, RscMtRmRtd> {

    RscMtRmRtdDto toDto(RscMtRmRtd rscMtRmRtd);

    RscMtRmRtd toEntity(RscMtRmRtdDto rscMtRmRtdDto);

    default RscMtRmRtd fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmRtd rscMtRmRtd= new RscMtRmRtd();
        rscMtRmRtd.setId(id);
        return rscMtRmRtd;
    }
}
