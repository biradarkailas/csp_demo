package com.example.validation.mapper;

import com.example.validation.domain.RscDtFGType;
import com.example.validation.dto.RscDtFGTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtFGTypeMapper extends EntityMapper<RscDtFGTypeDTO, RscDtFGType> {
    RscDtFGTypeDTO toDto(RscDtFGType rscDtFGType);

    RscDtFGType toEntity(RscDtFGTypeDTO rscDtFGTypeDTO);

    default RscDtFGType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtFGType rscDtFGType = new RscDtFGType();
        rscDtFGType.setId(id);
        return rscDtFGType;
    }
}
