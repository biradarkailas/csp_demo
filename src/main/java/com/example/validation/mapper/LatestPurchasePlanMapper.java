package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMLatestPurchasePlan;
import com.example.validation.dto.LatestPurchasePlanDTO;
import com.example.validation.dto.OriginalPurchasePlanDTO;
import com.example.validation.dto.RscIotLatestMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscItItemMouldMapper.class, RscDtMouldMapper.class})
public interface LatestPurchasePlanMapper extends EntityMapper<LatestPurchasePlanDTO, RscIotPMLatestPurchasePlan> {
    @Mapping(source = "rscMtItem.rscItItemMould.rscDtMould.totalCapacity", target = "capacity")
    @Mapping(source = "rscMtItem.rscItItemMould.rscDtMould.id", target = "mouldId")
    @Mapping(source = "rscMtItem.rscItItemMould.rscDtMould.mould", target = "mould")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    LatestPurchasePlanDTO toDto(RscIotPMLatestPurchasePlan rscIotPMLatestPurchasePlan);

    default RscIotPMLatestPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMLatestPurchasePlan rscIotPMOriginalPurchasePlan = new RscIotPMLatestPurchasePlan();
        rscIotPMOriginalPurchasePlan.setId(id);
        return rscIotPMOriginalPurchasePlan;
    }
}
