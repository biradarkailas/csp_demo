package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGSlobSummary;
import com.example.validation.dto.slob.FGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface FGSlobSummaryDashboardMapper extends EntityMapper<FGSlobSummaryDashboardDTO, RscIotFGSlobSummary> {

    @Mapping(source = "totalStockValue", target = "totalStockValue")
    @Mapping(source = "totalSlobValue", target = "totalSlobValue")
    @Mapping(source = "totalSlowItemValue", target = "totalSLValue")
    @Mapping(source = "totalObsoleteItemValue", target = "totalOBValue")
    @Mapping(source = "stockQualityPercentage", target = "iqValue")
    FGSlobSummaryDashboardDTO toDto(RscIotFGSlobSummary rscIotFGSlobSummary);

    default RscIotFGSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGSlobSummary rscIotFGSlobSummary = new RscIotFGSlobSummary();
        rscIotFGSlobSummary.setId(id);
        return rscIotFGSlobSummary;
    }
}