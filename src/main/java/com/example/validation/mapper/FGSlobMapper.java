package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGSlob;
import com.example.validation.dto.slob.FGSlobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtDivisionMapper.class, RscDtItemCategoryMapper.class})
public interface FGSlobMapper extends EntityMapper<FGSlobDTO, RscIotFGSlob> {

    @Mapping(source = "rscMtPPHeader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtDivision.id", target = "itemDivisionId")
    @Mapping(source = "rscMtItem.rscDtItemCategory.description", target = "itemCategory")
    FGSlobDTO toDto(RscIotFGSlob rscIotFGSlob);

    default RscIotFGSlob fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGSlob rscIotFGSlob = new RscIotFGSlob();
        rscIotFGSlob.setId(id);
        return rscIotFGSlob;
    }
}