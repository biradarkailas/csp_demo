package com.example.validation.mapper;

import com.example.validation.domain.RscItItemStockType;
import com.example.validation.dto.RscItItemStockTypeDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscItItemStockTypeMapper extends EntityMapper<RscItItemStockTypeDTO, RscItItemStockType>{

    RscItItemStockTypeDTO toDto(RscItItemStockType rscItItemStockType);

    RscItItemStockType toEntity(RscItItemStockTypeDTO rscItItemStockTypeDTO);

    default RscItItemStockType fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItItemStockType rscItItemStockType= new RscItItemStockType();
        rscItItemStockType.setId(id);
        return rscItItemStockType;
    }
}
