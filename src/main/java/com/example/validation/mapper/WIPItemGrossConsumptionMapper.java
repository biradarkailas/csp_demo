package com.example.validation.mapper;

import com.example.validation.domain.RscItWIPGrossConsumption;
import com.example.validation.dto.ItemDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface WIPItemGrossConsumptionMapper extends EntityMapper<ItemDetailDTO, RscItWIPGrossConsumption> {

    @Mapping(source = "rscMtItem.id", target = "id")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "totalValue", target = "total")
    @Mapping(source = "m1Value", target = "monthValues.month1.value")
    @Mapping(source = "m2Value", target = "monthValues.month2.value")
    @Mapping(source = "m3Value", target = "monthValues.month3.value")
    @Mapping(source = "m4Value", target = "monthValues.month4.value")
    @Mapping(source = "m5Value", target = "monthValues.month5.value")
    @Mapping(source = "m6Value", target = "monthValues.month6.value")
    @Mapping(source = "m7Value", target = "monthValues.month7.value")
    @Mapping(source = "m8Value", target = "monthValues.month8.value")
    @Mapping(source = "m9Value", target = "monthValues.month9.value")
    @Mapping(source = "m10Value", target = "monthValues.month10.value")
    @Mapping(source = "m11Value", target = "monthValues.month11.value")
    @Mapping(source = "m12Value", target = "monthValues.month12.value")
    ItemDetailDTO toDto(RscItWIPGrossConsumption rscItWIPGrossConsumption);

    default RscItWIPGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItWIPGrossConsumption rscItWIPGrossConsumption = new RscItWIPGrossConsumption();
        rscItWIPGrossConsumption.setId(id);
        return rscItWIPGrossConsumption;
    }
}