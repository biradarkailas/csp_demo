package com.example.validation.mapper;

import com.example.validation.domain.RscItBPUniqueLogicalConsumption;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtBomMapper.class})
public interface BPLogicalConsumptionMapper extends EntityMapper<LogicalConsumptionMainDTO, RscItBPUniqueLogicalConsumption> {

    @Mapping(source = "rscMtItemParent.id", target = "parentId")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.code", target = "parentCode")
    @Mapping(source = "rscMtItemParent.rscDtItemCode.description", target = "parentDescription")
    @Mapping(source = "rscMtItemChild.id", target = "childId")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.code", target = "childCode")
    @Mapping(source = "rscMtItemChild.rscDtItemCode.description", target = "childDescription")
    @Mapping(source = "rscMtBom.applicableFrom", target = "applicableFrom")
    @Mapping(source = "rscMtBom.applicableTo", target = "applicableTo")
    @Mapping(source = "rscMtBom.linkQuantity", target = "linkQuantity")
    @Mapping(source = "rscMtBom.wastePercent", target = "wastePercent")
    @Mapping(source = "m1Value", target = "logicalConsumptionMonthValues.month1.value")
    @Mapping(source = "m2Value", target = "logicalConsumptionMonthValues.month2.value")
    @Mapping(source = "m3Value", target = "logicalConsumptionMonthValues.month3.value")
    @Mapping(source = "m4Value", target = "logicalConsumptionMonthValues.month4.value")
    @Mapping(source = "m5Value", target = "logicalConsumptionMonthValues.month5.value")
    @Mapping(source = "m6Value", target = "logicalConsumptionMonthValues.month6.value")
    @Mapping(source = "m7Value", target = "logicalConsumptionMonthValues.month7.value")
    @Mapping(source = "m8Value", target = "logicalConsumptionMonthValues.month8.value")
    @Mapping(source = "m9Value", target = "logicalConsumptionMonthValues.month9.value")
    @Mapping(source = "m10Value", target = "logicalConsumptionMonthValues.month10.value")
    @Mapping(source = "m11Value", target = "logicalConsumptionMonthValues.month11.value")
    @Mapping(source = "m12Value", target = "logicalConsumptionMonthValues.month12.value")
    LogicalConsumptionMainDTO toDto(RscItBPUniqueLogicalConsumption rscItBPLogicalConsumption);

    default RscItBPUniqueLogicalConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItBPUniqueLogicalConsumption rscItBPLogicalConsumption = new RscItBPUniqueLogicalConsumption();
        rscItBPLogicalConsumption.setId(id);
        return rscItBPLogicalConsumption;
    }
}
