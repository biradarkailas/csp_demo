package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgLineSaturation;
import com.example.validation.dto.RscIotFgLineSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class,RscDtLinesMapper.class})
public interface RscIotFgLineSaturationMapper extends EntityMapper<RscIotFgLineSaturationDTO, RscIotFgLineSaturation> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtLines.id", target = "rscDtLinesId")
    RscIotFgLineSaturationDTO toDto(RscIotFgLineSaturation rscIotFgLineSaturation);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtLinesId", target = "rscDtLines")
    RscIotFgLineSaturation toEntity(RscIotFgLineSaturationDTO rscIotFgLineSaturationDTO);

    default RscIotFgLineSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgLineSaturation rscIotFgLineSaturation = new RscIotFgLineSaturation();
        rscIotFgLineSaturation.setId(id);
        return rscIotFgLineSaturation;
    }
}
