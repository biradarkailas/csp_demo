package com.example.validation.mapper;

import com.example.validation.domain.RscItWIPGrossConsumption;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscItWIPGrossConsumptionMapper extends EntityMapper<RscItGrossConsumptionDTO, RscItWIPGrossConsumption> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    RscItGrossConsumptionDTO toDto(RscItWIPGrossConsumption rscItWIPGrossConsumption);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscItWIPGrossConsumption toEntity(RscItGrossConsumptionDTO rscItGrossConsumptionDTO);

    default RscItWIPGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItWIPGrossConsumption rscItWIPGrossConsumption = new RscItWIPGrossConsumption();
        rscItWIPGrossConsumption.setId(id);
        return rscItWIPGrossConsumption;
    }
}