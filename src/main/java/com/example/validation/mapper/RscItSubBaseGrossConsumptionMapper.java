package com.example.validation.mapper;

import com.example.validation.domain.RscItSubBaseGrossConsumption;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscItSubBaseGrossConsumptionMapper extends EntityMapper<RscItGrossConsumptionDTO, RscItSubBaseGrossConsumption> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    RscItGrossConsumptionDTO toDto(RscItSubBaseGrossConsumption rscItSubBaseGrossConsumption);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscItSubBaseGrossConsumption toEntity(RscItGrossConsumptionDTO rscItGrossConsumptionDTO);

    default RscItSubBaseGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItSubBaseGrossConsumption rscItSubBaseGrossConsumption = new RscItSubBaseGrossConsumption();
        rscItSubBaseGrossConsumption.setId(id);
        return rscItSubBaseGrossConsumption;
    }
}