package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysSummary;
import com.example.validation.dto.RmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
@Mapper(componentModel = "spring", uses = {})
public interface RmCoverDaysSummaryMapper extends EntityMapper<RmCoverDaysSummaryDTO, RscIotRmCoverDaysSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    RmCoverDaysSummaryDTO toDto(RscIotRmCoverDaysSummary rscIotRmCoverDaysSummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader.id")
    RscIotRmCoverDaysSummary toEntity(RmCoverDaysSummaryDTO rmCoverDaysSummaryDTO);

    default RscIotRmCoverDaysSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysSummary rscIotRmCoverDaysSummary = new RscIotRmCoverDaysSummary();
        rscIotRmCoverDaysSummary.setId(id);
        return rscIotRmCoverDaysSummary;
    }
}
