package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysClassBSummary;
import com.example.validation.dto.RscIotRmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysClassBSummaryMapper extends EntityMapper<RscIotRmCoverDaysSummaryDTO, RscIotRmCoverDaysClassBSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    RscIotRmCoverDaysSummaryDTO toDto(RscIotRmCoverDaysClassBSummary rscIotRmCoverDaysClassBSummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    RscIotRmCoverDaysClassBSummary toEntity(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO);

    default RscIotRmCoverDaysClassBSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysClassBSummary rscIotRmCoverDaysClassBSummary = new RscIotRmCoverDaysClassBSummary();
        rscIotRmCoverDaysClassBSummary.setId(id);
        return rscIotRmCoverDaysClassBSummary;
    }
}