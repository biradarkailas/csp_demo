package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmOtifSummary;
import com.example.validation.dto.RscDtSupplierDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtSupplierMapper.class})
public interface RscDtSupplierMapperBasedOnOtifSummaryMapper extends EntityMapper<RscDtSupplierDTO, RscIotPmOtifSummary> {
    @Mapping(source = "rscDtSupplier.id", target = "id")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscDtSupplier.supplierCode", target = "supplierCode")
    RscDtSupplierDTO toDto(RscIotPmOtifSummary rscIotPmOtifSummary);

    default RscIotPmOtifSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmOtifSummary rscIotPmOtifSummary = new RscIotPmOtifSummary();
        rscIotPmOtifSummary.setId(id);
        return rscIotPmOtifSummary;
    }

}
