package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMOriginalPurchasePlan;
import com.example.validation.dto.RscIotOriginalMonthValueslDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {})
public interface RscIotOriginalMonthValuesMapper extends EntityMapper<RscIotOriginalMonthValueslDTO, RscIotPMOriginalPurchasePlan> {

    @Mapping(source = "m1Value", target = "month1.value")
    @Mapping(source = "m2Value", target = "month2.value")
    @Mapping(source = "m3Value", target = "month3.value")
    @Mapping(source = "m4Value", target = "month4.value")
    @Mapping(source = "m5Value", target = "month5.value")
    @Mapping(source = "m6Value", target = "month6.value")
    @Mapping(source = "m7Value", target = "month7.value")
    @Mapping(source = "m8Value", target = "month8.value")
    @Mapping(source = "m9Value", target = "month9.value")
    @Mapping(source = "m10Value", target = "month10.value")
    @Mapping(source = "m11Value", target = "month11.value")
    @Mapping(source = "m12Value", target = "month12.value")
    @Mapping(target = "totalValue", ignore = true)
    RscIotOriginalMonthValueslDTO toDto(RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan);

    @AfterMapping
    default void calculateTotalValue(@MappingTarget RscIotOriginalMonthValueslDTO purchasePlanDTO) {
        purchasePlanDTO.setTotalValue(getTotalValue(purchasePlanDTO));
    }

    private Double getTotalValue(RscIotOriginalMonthValueslDTO purchasePlanDTO) {
        return purchasePlanDTO.getMonth1().getValue()
                + purchasePlanDTO.getMonth2().getValue()
                + purchasePlanDTO.getMonth3().getValue()
                + purchasePlanDTO.getMonth4().getValue()
                + purchasePlanDTO.getMonth5().getValue()
                + purchasePlanDTO.getMonth6().getValue()
                + purchasePlanDTO.getMonth7().getValue()
                + purchasePlanDTO.getMonth8().getValue()
                + purchasePlanDTO.getMonth9().getValue()
                + purchasePlanDTO.getMonth10().getValue()
                + purchasePlanDTO.getMonth11().getValue()
                + purchasePlanDTO.getMonth12().getValue();
    }

    default RscIotPMOriginalPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMOriginalPurchasePlan rscIitRmBulkOpRm = new RscIotPMOriginalPurchasePlan();
        rscIitRmBulkOpRm.setId(id);
        return rscIitRmBulkOpRm;
    }
}
