package com.example.validation.mapper;

import com.example.validation.domain.RscDtContactDetails;
import com.example.validation.dto.RscDtContactDetailsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtContactDetailsMapper extends EntityMapper<RscDtContactDetailsDTO, RscDtContactDetails> {
    RscDtContactDetailsDTO toDto(RscDtContactDetails rscDtContactDetails);

    RscDtContactDetails toEntity(RscDtContactDetailsDTO rscDtContactDetailsDTO);

    default RscDtContactDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtContactDetails rscDtContactDetails = new RscDtContactDetails();
        rscDtContactDetails.setId(id);
        return rscDtContactDetails;
    }
}
