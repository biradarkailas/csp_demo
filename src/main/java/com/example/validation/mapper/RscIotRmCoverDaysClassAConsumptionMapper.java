package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysClassASummary;
import com.example.validation.dto.ClassWiseDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysClassAConsumptionMapper extends EntityMapper<ClassWiseDetailsDTO, RscIotRmCoverDaysClassASummary> {
      @Mapping(source = "totalCoverDaysWithoutSit", target = "totalCoverDays")
    ClassWiseDetailsDTO toDto(RscIotRmCoverDaysClassASummary rscIotRmCoverDaysClassASummary);

    default RscIotRmCoverDaysClassASummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysClassASummary rscIotRmCoverDaysClassASummary = new RscIotRmCoverDaysClassASummary();
        rscIotRmCoverDaysClassASummary.setId(id);
        return rscIotRmCoverDaysClassASummary;
    }

}
