package com.example.validation.mapper;

import com.example.validation.domain.RscIotLatestTotalMouldSaturationSummary;
import com.example.validation.dto.MouldSaturationDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscIotLatestTotalMouldSaturationMapper.class, RscDtMouldMapper.class})
public interface MouldSaturationSummaryForThreeMonthsMapper extends EntityMapper<MouldSaturationDetailsDTO, RscIotLatestTotalMouldSaturationSummary> {
    @Mapping(source = "rscIotLatestTotalMouldSaturation.rscDtMould.id", target = "rscDtMouldId")
    @Mapping(source = "rscIotLatestTotalMouldSaturation.rscDtMould.mould", target = "mouldName")
    @Mapping(source = "threeMonthsAverage", target = "avgSaturationPercentage")
    MouldSaturationDetailsDTO toDto(RscIotLatestTotalMouldSaturationSummary rscIotLatestTotalMouldSaturationSummary);

    default RscIotLatestTotalMouldSaturationSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotLatestTotalMouldSaturationSummary rscIotLatestTotalMouldSaturationSummary = new RscIotLatestTotalMouldSaturationSummary();
        rscIotLatestTotalMouldSaturationSummary.setId(id);
        return rscIotLatestTotalMouldSaturationSummary;
    }
}
