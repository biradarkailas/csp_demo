package com.example.validation.mapper;

import com.example.validation.domain.RscIitRmBulkOpRm;
import com.example.validation.dto.BulkOpRmExportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface BulkOpRmExportMapper extends EntityMapper<BulkOpRmExportDTO, RscIitRmBulkOpRm> {

    @Mapping(source = "rscMtRmItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtRmItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtRmItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscMtRmItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    BulkOpRmExportDTO toDto(RscIitRmBulkOpRm rscIitRmBulkOpRm);

    @Mapping(source = "rscMtItemId", target = "rscMtRmItem")
    RscIitRmBulkOpRm toEntity(BulkOpRmExportDTO bulkOpRmExportDTO);

    default RscIitRmBulkOpRm fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitRmBulkOpRm rscIitRmBulkOpRm = new RscIitRmBulkOpRm();
        rscIitRmBulkOpRm.setId(id);
        return rscIitRmBulkOpRm;
    }
}
