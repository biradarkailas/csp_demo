package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGSlob;
import com.example.validation.dto.slob.RscIotFGSlobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface RscIotFGSlobMapper extends EntityMapper<RscIotFGSlobDTO, RscIotFGSlob> {

    @Mapping(source = "rscMtPPHeader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtPPHeader.mpsName", target = "mpsName")
    @Mapping(source = "rscMtPPHeader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    RscIotFGSlobDTO toDto(RscIotFGSlob rscIotFGSlob);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPHeader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotFGSlob toEntity(RscIotFGSlobDTO rscIotFGSlobDTO);

    default RscIotFGSlob fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGSlob rscIotFGSlob = new RscIotFGSlob();
        rscIotFGSlob.setId(id);
        return rscIotFGSlob;
    }
}