package com.example.validation.mapper;

import com.example.validation.domain.RscItItemWiseSupplier;
import com.example.validation.dto.RscDtSupplierDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
@Mapper(componentModel = "spring", uses = {  RscMtSupplierMapper.class, RscDtSupplierMapper.class})
public interface SupplierMaterialsMapper extends EntityMapper<RscDtSupplierDTO, RscItItemWiseSupplier> {
    @Mapping(source = "rscMtSupplier.rscDtSupplier.id", target = "id")
    @Mapping(source = "rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscMtSupplier.rscDtSupplier.supplierCode", target = "supplierCode")
    RscDtSupplierDTO toDto(RscItItemWiseSupplier rscItItemWiseSupplier);

    default RscItItemWiseSupplier fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItItemWiseSupplier rscItItemWiseSupplier = new RscItItemWiseSupplier();
        rscItItemWiseSupplier.setId(id);
        return rscItItemWiseSupplier;
    }
}
