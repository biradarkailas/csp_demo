package com.example.validation.mapper;

import com.example.validation.domain.RscDtFileName;
import com.example.validation.dto.FileNameDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFolderNameMapper.class})
public interface VDPlanFileNameMapper extends EntityMapper<FileNameDTO, RscDtFileName> {
    @Mapping(source = "rscDtFolderName.id", target = "folderId")
    @Mapping(source = "rscDtFolderName.folderName", target = "folderName")
    @Mapping(source = "vdRequired", target = "isRequired")
    FileNameDTO toDto(RscDtFileName rscDtFileName);

    default RscDtFileName fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtFileName rscDtFileName = new RscDtFileName();
        rscDtFileName.setId(id);
        return rscDtFileName;
    }
}
