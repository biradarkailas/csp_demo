package com.example.validation.mapper;

import com.example.validation.domain.RscMtOtifPmReceiptsHeader;
import com.example.validation.dto.RscMtOtifPmReceiptsHeaderDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtOtifPmReceiptsHeaderMapper extends EntityMapper<RscMtOtifPmReceiptsHeaderDTO, RscMtOtifPmReceiptsHeader> {
    RscMtOtifPmReceiptsHeaderDTO toDto(RscMtOtifPmReceiptsHeader rscMtOtifPmReceiptsHeader);

    RscMtOtifPmReceiptsHeader toEntity(RscMtOtifPmReceiptsHeaderDTO rscMtOtifPmReceiptsHeaderDTO);

    default RscMtOtifPmReceiptsHeader fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtOtifPmReceiptsHeader rscMtOtifPmReceiptsHeader = new RscMtOtifPmReceiptsHeader();
        rscMtOtifPmReceiptsHeader.setId(id);
        return rscMtOtifPmReceiptsHeader;
    }
}
