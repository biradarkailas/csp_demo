package com.example.validation.mapper;

import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.domain.RscIotOriginalTotalMouldSaturation;
import com.example.validation.dto.RscIotOriginalMouldSaturationDTO;
import com.example.validation.dto.RscIotOriginalTotalMouldSaturationDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtMouldMapper.class,RscMtPPheaderMapper.class,RscDtMouldMapper.class})
public interface RscIotOriginalTotalMouldSaturationMapper extends EntityMapper<RscIotOriginalTotalMouldSaturationDTO, RscIotOriginalTotalMouldSaturation> {

    @Mapping(source = "rscDtMould.mould", target = "mould")
    @Mapping(source = "rscDtMould.totalCapacity", target = "capacity")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtMould.id", target = "rscDtMouldId")
    RscIotOriginalTotalMouldSaturationDTO toDto(RscIotOriginalTotalMouldSaturation rscIotOriginalTotalMouldSaturation);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtMouldId", target = "rscDtMould")
    RscIotOriginalTotalMouldSaturation toEntity(RscIotOriginalTotalMouldSaturationDTO rscIotOriginalTotalMouldSaturationDTO);

    default RscIotOriginalMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotOriginalMouldSaturation rscIotOriginalMouldSaturation = new RscIotOriginalMouldSaturation();
        rscIotOriginalMouldSaturation.setId(id);
        return rscIotOriginalMouldSaturation;
    }
}
