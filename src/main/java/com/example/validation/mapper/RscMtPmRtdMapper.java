package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmRtd;
import com.example.validation.dto.RscMtPmRtdDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscDtSupplierMapper.class, RscMtPPheaderMapper.class})
public interface RscMtPmRtdMapper extends EntityMapper<RscMtPmRtdDTO, RscMtPmRtd> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    RscMtPmRtdDTO toDto(RscMtPmRtd rscMtPmRtd);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscDtSupplierId", target = "rscDtSupplier")
    RscMtPmRtd toEntity(RscMtPmRtdDTO rscMtPmRtdDTO);

    default RscMtPmRtd fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPmRtd rscMtPmRtd = new RscMtPmRtd();
        rscMtPmRtd.setId(id);
        return rscMtPmRtd;
    }
}
