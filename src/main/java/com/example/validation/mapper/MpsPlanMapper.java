package com.example.validation.mapper;

import com.example.validation.domain.RscMtPPdetails;
import com.example.validation.dto.ItemDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtProductionTypeMapper.class,
        RscDtLinesMapper.class, RscDtSkidsMapper.class, RscDtItemCodeMapper.class, RscDtDivisionMapper.class,RscDtBrandMapper.class})
public interface MpsPlanMapper extends EntityMapper<ItemDetailDTO, RscMtPPdetails> {

    @Mapping(source = "rscMtItem.id", target = "id")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "rscDtProductionType.id", target = "otherInfo.productionTypeId")
    @Mapping(source = "rscDtProductionType.name", target = "productionType")
    @Mapping(source = "rscDtLines.id", target = "otherInfo.linesId")
    @Mapping(source = "rscDtLines.name", target = "lineName")
    @Mapping(source = "rscDtSkids.id", target = "otherInfo.skidsId")
    @Mapping(source = "rscDtSkids.name", target = "skidName")
    @Mapping(source = "rscDtDivision.id", target = "otherInfo.divisionId")
    @Mapping(source = "rscDtDivision.aliasName", target = "divisionName")
    @Mapping(source = "rscDtBrand.id", target = "otherInfo.brandId")
    @Mapping(source = "rscDtBrand.name", target = "brandName")
    @Mapping(source = "rscDtSignature.id", target = "otherInfo.signatureId")
    @Mapping(source = "rscDtSignature.name", target = "signatureName")
    @Mapping(source = "rscDtMaterialCategory.id", target = "otherInfo.materialCategoryId")
    @Mapping(source = "rscDtMaterialCategory.name", target = "materialCategoryName")
    @Mapping(source = "rscDtMaterialSubCategory.id", target = "otherInfo.materialSubCategoryId")
    @Mapping(source = "rscDtMaterialSubCategory.name", target = "materialSubCategoryName")
    @Mapping(source = "m1Value", target = "monthValues.month1.value")
    @Mapping(source = "m2Value", target = "monthValues.month2.value")
    @Mapping(source = "m3Value", target = "monthValues.month3.value")
    @Mapping(source = "m4Value", target = "monthValues.month4.value")
    @Mapping(source = "m5Value", target = "monthValues.month5.value")
    @Mapping(source = "m6Value", target = "monthValues.month6.value")
    @Mapping(source = "m7Value", target = "monthValues.month7.value")
    @Mapping(source = "m8Value", target = "monthValues.month8.value")
    @Mapping(source = "m9Value", target = "monthValues.month9.value")
    @Mapping(source = "m10Value", target = "monthValues.month10.value")
    @Mapping(source = "m11Value", target = "monthValues.month11.value")
    @Mapping(source = "m12Value", target = "monthValues.month12.value")
    ItemDetailDTO toDto(RscMtPPdetails rscMtPPdetails);

    default RscMtPPdetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPPdetails rscMtPPdetails = new RscMtPPdetails();
        rscMtPPdetails.setId(id);
        return rscMtPPdetails;
    }
}