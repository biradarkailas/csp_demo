package com.example.validation.mapper;

import com.example.validation.domain.ValidationTableLoadingDetails;
import com.example.validation.dto.ValidationTableLoadingDetailsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface ValidationTableLoadingDetailsMapper extends EntityMapper<ValidationTableLoadingDetailsDTO, ValidationTableLoadingDetails> {
    ValidationTableLoadingDetailsDTO toDto(ValidationTableLoadingDetails validationTableLoadingDetails);

    ValidationTableLoadingDetails toEntity(ValidationTableLoadingDetailsDTO validationTableLoadingDetailsDTO);

    default ValidationTableLoadingDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        ValidationTableLoadingDetails validationTableLoadingDetails = new ValidationTableLoadingDetails();
        validationTableLoadingDetails.setId( id );
        return validationTableLoadingDetails;
    }
}

