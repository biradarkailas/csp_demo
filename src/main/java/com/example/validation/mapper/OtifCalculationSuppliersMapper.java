package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmOtifSummary;
import com.example.validation.dto.OtifCalculationSuppliersDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtSupplierMapper.class})
public interface OtifCalculationSuppliersMapper extends EntityMapper<OtifCalculationSuppliersDTO, RscIotPmOtifSummary> {
    @Mapping(source = "rscDtSupplier.id", target = "rscDtSupplierId")
    @Mapping(source = "rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "avgReceiptPercentage", target = "receiptPercentage")
    OtifCalculationSuppliersDTO toDto(RscIotPmOtifSummary rscIotPmOtifSummary);

    default RscIotPmOtifSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmOtifSummary rscIotPmOtifSummary = new RscIotPmOtifSummary();
        rscIotPmOtifSummary.setId(id);
        return rscIotPmOtifSummary;
    }
}
