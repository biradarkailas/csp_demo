package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmSlobSummary;
import com.example.validation.dto.RscIotPmSlobSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotPmSlobSummaryMapper extends EntityMapper<RscIotPmSlobSummaryDTO, RscIotPmSlobSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    RscIotPmSlobSummaryDTO toDto(RscIotPmSlobSummary rscIotPmSlobSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    RscIotPmSlobSummary toEntity(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO);

    default RscIotPmSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmSlobSummary rscIotPmSlobSummary = new RscIotPmSlobSummary();
        rscIotPmSlobSummary.setId(id);
        return rscIotPmSlobSummary;
    }
}
