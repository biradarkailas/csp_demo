package com.example.validation.mapper;

        import com.example.validation.domain.RscDtState;
        import com.example.validation.dto.RscDtStateDTO;
        import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtStateMapper extends EntityMapper<RscDtStateDTO, RscDtState>{
    RscDtStateDTO toDto(RscDtState rscDtState);

    RscDtState toEntity(RscDtStateDTO rscDtStateDTO);

    default RscDtState fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtState rscDtState = new RscDtState();
        rscDtState.setId(id);
        return rscDtState;
    }

}
