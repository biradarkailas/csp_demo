package com.example.validation.mapper;

import com.example.validation.domain.RscDtSupplier;
import com.example.validation.dto.RscDtSupplierDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtSupplierMapper extends EntityMapper<RscDtSupplierDTO, RscDtSupplier> {
    RscDtSupplierDTO toDto(RscDtSupplier rscDtSupplier);

    RscDtSupplier toEntity(RscDtSupplierDTO rscDtSupplierDTO);

    default RscDtSupplier fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtSupplier rscDtSupplier = new RscDtSupplier();
        rscDtSupplier.setId(id);
        return rscDtSupplier;
    }
}
