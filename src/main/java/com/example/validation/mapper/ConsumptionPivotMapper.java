package com.example.validation.mapper;

import com.example.validation.domain.RscItRMGrossConsumption;
import com.example.validation.dto.ConsumptionPivotDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface ConsumptionPivotMapper extends EntityMapper<ConsumptionPivotDTO, RscItRMGrossConsumption> {

    @Mapping(source = "rscMtItem.id", target = "childItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "childItemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "childItemDescription")
    @Mapping(source = "m1Value", target = "m1TotalValue")
    @Mapping(source = "m2Value", target = "m2TotalValue")
    @Mapping(source = "m3Value", target = "m3TotalValue")
    @Mapping(source = "m4Value", target = "m4TotalValue")
    @Mapping(source = "m5Value", target = "m5TotalValue")
    @Mapping(source = "m6Value", target = "m6TotalValue")
    @Mapping(source = "m7Value", target = "m7TotalValue")
    @Mapping(source = "m8Value", target = "m8TotalValue")
    @Mapping(source = "m9Value", target = "m9TotalValue")
    @Mapping(source = "m10Value", target = "m10TotalValue")
    @Mapping(source = "m11Value", target = "m11TotalValue")
    @Mapping(source = "m12Value", target = "m12TotalValue")
    ConsumptionPivotDTO toDto(RscItRMGrossConsumption rscItRMGrossConsumption);

    default RscItRMGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItRMGrossConsumption rscItRMGrossConsumption = new RscItRMGrossConsumption();
        rscItRMGrossConsumption.setId(id);
        return rscItRMGrossConsumption;
    }
}