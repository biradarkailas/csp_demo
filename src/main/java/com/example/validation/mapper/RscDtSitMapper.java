package com.example.validation.mapper;

import com.example.validation.domain.RscDtSit;
import com.example.validation.dto.RscDtSitDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtSitMapper extends EntityMapper<RscDtSitDTO, RscDtSit> {
    RscDtSitDTO toDto(RscDtSit rscDtSit);

    RscDtSit toEntity(RscDtSitDTO rscDtSitDTO);

    default RscDtSit fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtSit rscDtSit = new RscDtSit();
        rscDtSit.setId(id);
        return rscDtSit;
    }
}
