package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmMes;
import com.example.validation.dto.RscMtPmMesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscMtPmMesMapper extends EntityMapper<RscMtPmMesDTO, RscMtPmMes> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    RscMtPmMesDTO toDto(RscMtPmMes rscMtPmMes);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscMtPmMes toEntity(RscMtPmMesDTO rscMtPmMesDTO);

    default RscMtPmMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPmMes rscMtPmMes = new RscMtPmMes();
        rscMtPmMes.setId(id);
        return rscMtPmMes;
    }
}