package com.example.validation.mapper;

import com.example.validation.domain.RscItItemMould;
import com.example.validation.dto.RscItItemMouldDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RscItItemMouldMapper extends EntityMapper<RscItItemMouldDTO, RscItItemMould> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscDtMould.id", target = "rscDtMouldId")
    RscItItemMouldDTO toDto(RscItItemMould rscItItemMould);

    RscItItemMould toEntity(RscItItemMouldDTO rscItItemMouldDTO);

    default RscItItemMould fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItItemMould rscItItemMould = new RscItItemMould();
        rscItItemMould.setId(id);
        return rscItItemMould;
    }
}
