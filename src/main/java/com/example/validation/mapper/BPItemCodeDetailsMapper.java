package com.example.validation.mapper;

import com.example.validation.domain.RscItBPGrossConsumption;
import com.example.validation.dto.ItemCodeDetailDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class})
public interface BPItemCodeDetailsMapper extends EntityMapper<ItemCodeDetailDTO, RscItBPGrossConsumption> {

    @Mapping(source = "rscMtItem.id", target = "itemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    ItemCodeDetailDTO toDto(RscItBPGrossConsumption rscItBPGrossConsumption);

    default RscItBPGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItBPGrossConsumption rscItBPGrossConsumption = new RscItBPGrossConsumption();
        rscItBPGrossConsumption.setId(id);
        return rscItBPGrossConsumption;
    }
}