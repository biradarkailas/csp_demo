package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmMes;
import com.example.validation.domain.RscMtRmOtherStocks;
import com.example.validation.dto.RscMtRmMesDTO;
import com.example.validation.dto.RscMtRmOtherStocksDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtRmOtherStocksMapper extends EntityMapper<RscMtRmOtherStocksDTO, RscMtRmOtherStocks>{

    RscMtRmOtherStocksDTO toDto(RscMtRmOtherStocks rscMtRmOtherStocks);

    RscMtRmOtherStocks toEntity(RscMtRmOtherStocksDTO rscMtRmOtherStocksDTO);

    default RscMtRmOtherStocks fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmOtherStocks rscMtRmOtherStocks= new RscMtRmOtherStocks();
        rscMtRmOtherStocks.setId(id);
        return rscMtRmOtherStocks;
    }
}
