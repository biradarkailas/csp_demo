package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import com.example.validation.dto.FgTypesDTO;
import com.example.validation.dto.RscDtDivisionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtDivisionMapper.class})
public interface FgTypeDivisionMapper extends EntityMapper<FgTypesDTO, RscIotFGTypeDivisionSlobSummary> {
    @Mapping(source = "rscDtDivision.id", target = "id")
    @Mapping(source = "rscDtDivision.aliasName", target = "name")
    FgTypesDTO toDto(RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary);

    default RscIotFGTypeDivisionSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary = new RscIotFGTypeDivisionSlobSummary();
        rscIotFGTypeDivisionSlobSummary.setId(id);
        return rscIotFGTypeDivisionSlobSummary;
    }
}
