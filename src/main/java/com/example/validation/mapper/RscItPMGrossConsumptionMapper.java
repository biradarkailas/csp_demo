package com.example.validation.mapper;

import com.example.validation.domain.RscItPMGrossConsumption;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscItItemWiseSupplierMapper.class, RscMtSupplierMapper.class, RscDtSupplierMapper.class})
public interface RscItPMGrossConsumptionMapper extends EntityMapper<RscItGrossConsumptionDTO, RscItPMGrossConsumption> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "itemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    RscItGrossConsumptionDTO toDto(RscItPMGrossConsumption rscItPmGrossConsumption);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "itemWiseSupplierId", target = "rscItItemWiseSupplier")
    RscItPMGrossConsumption toEntity(RscItGrossConsumptionDTO rscItGrossConsumptionDTO);

    default RscItPMGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItPMGrossConsumption rscItPmGrossConsumption = new RscItPMGrossConsumption();
        rscItPmGrossConsumption.setId(id);
        return rscItPmGrossConsumption;
    }
}