package com.example.validation.mapper;

import com.example.validation.domain.RscIotPmCoverDaysSummary;
import com.example.validation.dto.RscIotPmCoverDaysSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscItPMGrossConsumptionMapper.class})
public interface RscIotPmCoverDaysSummaryMapper extends EntityMapper<RscIotPmCoverDaysSummaryDTO, RscIotPmCoverDaysSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    RscIotPmCoverDaysSummaryDTO toDto(RscIotPmCoverDaysSummary rscIotPmCoverDaysSummary);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    RscIotPmCoverDaysSummary toEntity(RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO);

    default RscIotPmCoverDaysSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPmCoverDaysSummary rscIotPmCoverDaysSummary = new RscIotPmCoverDaysSummary();
        rscIotPmCoverDaysSummary.setId(id);
        return rscIotPmCoverDaysSummary;
    }
}

