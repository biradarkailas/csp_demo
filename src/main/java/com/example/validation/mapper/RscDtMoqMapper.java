package com.example.validation.mapper;

import com.example.validation.domain.RscDtMoq;
import com.example.validation.dto.RscDtMoqDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtMoqMapper extends EntityMapper<RscDtMoqDTO, RscDtMoq> {
    RscDtMoqDTO toDto(RscDtMoq rscDtMoq);

    RscDtMoq toEntity(RscDtMoqDTO rscDtMoqDTO);

    default RscDtMoq fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtMoq rscDtMoq = new RscDtMoq();
        rscDtMoq.setId(id);
        return rscDtMoq;
    }
}
