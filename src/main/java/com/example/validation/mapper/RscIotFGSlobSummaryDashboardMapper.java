package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGSlobSummary;
import com.example.validation.dto.slob.FGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RscIotFGSlobSummaryDashboardMapper extends EntityMapper<FGSlobSummaryDashboardDTO, RscIotFGSlobSummary> {
    @Mapping(source = "totalObsoleteItemValue", target = "totalOBValue")
    @Mapping(source = "totalSlowItemValue", target = "totalSLValue")
    @Mapping(source = "stockQualityValue", target = "iqValue")
    @Mapping(source = "obsoleteItemPercentage", target = "obPercentage")
    @Mapping(source = "slowItemPercentage", target = "slPercentage")
    @Mapping(source = "stockQualityPercentage", target = "iqPercentage")
    FGSlobSummaryDashboardDTO toDto(RscIotFGSlobSummary rscIotFGSlobSummary);

    default RscIotFGSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGSlobSummary rscIotFGSlobSummary = new RscIotFGSlobSummary();
        rscIotFGSlobSummary.setId(id);
        return rscIotFGSlobSummary;
    }
}
