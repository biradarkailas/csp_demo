package com.example.validation.mapper;

import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.dto.OriginalPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtMouldMapper.class, RscIotPMOriginalPurchasePlanMapper.class})
public interface RscIotOriginalMouldSaturationMapper extends EntityMapper<OriginalPurchasePlanDTO, RscIotOriginalMouldSaturation> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtMould.mould", target = "mould")
    @Mapping(source = "rscDtMould.id", target = "mouldId")
    OriginalPurchasePlanDTO toDto(RscIotOriginalMouldSaturation rscIotOriginalMouldSaturation);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "id", target = "rscIotPMOriginalPurchasePlan")
    @Mapping(source = "mouldId", target = "rscDtMould")
    RscIotOriginalMouldSaturation toEntity(OriginalPurchasePlanDTO originalPurchasePlanDTO);

    default RscIotOriginalMouldSaturation fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotOriginalMouldSaturation rscIotOriginalMouldSaturation = new RscIotOriginalMouldSaturation();
        rscIotOriginalMouldSaturation.setId(id);
        return rscIotOriginalMouldSaturation;
    }
}
