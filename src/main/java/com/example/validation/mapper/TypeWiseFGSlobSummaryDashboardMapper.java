package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import com.example.validation.dto.slob.DivisionWiseFGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtFGTypeMapper.class})
public interface TypeWiseFGSlobSummaryDashboardMapper extends EntityMapper<DivisionWiseFGSlobSummaryDashboardDTO, RscIotFGTypeSlobSummary> {

    @Mapping(source = "rscDtFGType.type", target = "type")
    @Mapping(source = "totalSlobValue", target = "slobValue")
    @Mapping(source = "totalSlowItemValue", target = "slValue")
    @Mapping(source = "totalObsoleteItemValue", target = "obValue")
    DivisionWiseFGSlobSummaryDashboardDTO toDto(RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary);

    default RscIotFGTypeSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary = new RscIotFGTypeSlobSummary();
        rscIotFGTypeSlobSummary.setId(id);
        return rscIotFGTypeSlobSummary;
    }
}