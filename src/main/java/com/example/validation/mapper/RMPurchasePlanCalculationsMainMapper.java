package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMPurchasePlanCalculations;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscItRMGrossConsumptionMapper.class})
public interface RMPurchasePlanCalculationsMainMapper extends EntityMapper<PurchasePlanCalculationsMainDTO, RscIotRMPurchasePlanCalculations> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "month1OpeningStock", target = "month1SupplyDTO.monthEndStock")
    @Mapping(source = "month1SuggestedPurchasePlan", target = "month1SupplyDTO.finalSupply")
    @Mapping(source = "month2OpeningStock", target = "month2SupplyDTO.monthEndStock")
    @Mapping(source = "month2SuggestedPurchasePlan", target = "month2SupplyDTO.finalSupply")
    @Mapping(source = "month3OpeningStock", target = "month3SupplyDTO.monthEndStock")
    @Mapping(source = "month3SuggestedPurchasePlan", target = "month3SupplyDTO.finalSupply")
    @Mapping(source = "month4OpeningStock", target = "month4SupplyDTO.monthEndStock")
    @Mapping(source = "month4SuggestedPurchasePlan", target = "month4SupplyDTO.finalSupply")
    @Mapping(source = "month5OpeningStock", target = "month5SupplyDTO.monthEndStock")
    @Mapping(source = "month5SuggestedPurchasePlan", target = "month5SupplyDTO.finalSupply")
    @Mapping(source = "month6OpeningStock", target = "month6SupplyDTO.monthEndStock")
    @Mapping(source = "month6SuggestedPurchasePlan", target = "month6SupplyDTO.finalSupply")
    @Mapping(source = "month7OpeningStock", target = "month7SupplyDTO.monthEndStock")
    @Mapping(source = "month7SuggestedPurchasePlan", target = "month7SupplyDTO.finalSupply")
    @Mapping(source = "month8OpeningStock", target = "month8SupplyDTO.monthEndStock")
    @Mapping(source = "month8SuggestedPurchasePlan", target = "month8SupplyDTO.finalSupply")
    @Mapping(source = "month9OpeningStock", target = "month9SupplyDTO.monthEndStock")
    @Mapping(source = "month9SuggestedPurchasePlan", target = "month9SupplyDTO.finalSupply")
    @Mapping(source = "month10OpeningStock", target = "month10SupplyDTO.monthEndStock")
    @Mapping(source = "month10SuggestedPurchasePlan", target = "month10SupplyDTO.finalSupply")
    @Mapping(source = "month11OpeningStock", target = "month11SupplyDTO.monthEndStock")
    @Mapping(source = "month11SuggestedPurchasePlan", target = "month11SupplyDTO.finalSupply")
    @Mapping(source = "month12OpeningStock", target = "month12SupplyDTO.monthEndStock")
    @Mapping(source = "month12SuggestedPurchasePlan", target = "month12SupplyDTO.finalSupply")
    PurchasePlanCalculationsMainDTO toDto(RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations);

    @AfterMapping
    default void updateRMPurchasePlanCalculationsMainDTO(@MappingTarget PurchasePlanCalculationsMainDTO purchasePlanCalculationsMainDTO, RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations) {
        purchasePlanCalculationsMainDTO.getMonth1SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM1Value());
        purchasePlanCalculationsMainDTO.getMonth2SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM2Value());
        purchasePlanCalculationsMainDTO.getMonth3SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM3Value());
        purchasePlanCalculationsMainDTO.getMonth4SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM4Value());
        purchasePlanCalculationsMainDTO.getMonth5SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM5Value());
        purchasePlanCalculationsMainDTO.getMonth6SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM6Value());
        purchasePlanCalculationsMainDTO.getMonth7SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM7Value());
        purchasePlanCalculationsMainDTO.getMonth8SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM8Value());
        purchasePlanCalculationsMainDTO.getMonth9SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM9Value());
        purchasePlanCalculationsMainDTO.getMonth10SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM10Value());
        purchasePlanCalculationsMainDTO.getMonth11SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM11Value());
        purchasePlanCalculationsMainDTO.getMonth12SupplyDTO().setConsumption(rscIotRMPurchasePlanCalculations.getRscItRMGrossConsumption().getM12Value());
    }

    default RscIotRMPurchasePlanCalculations fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMPurchasePlanCalculations rscIotRMPurchasePlanCalculations = new RscIotRMPurchasePlanCalculations();
        rscIotRMPurchasePlanCalculations.setId(id);
        return rscIotRMPurchasePlanCalculations;
    }
}