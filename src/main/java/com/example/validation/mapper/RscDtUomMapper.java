package com.example.validation.mapper;


import com.example.validation.domain.RscDtUom;
import com.example.validation.dto.RscDtUomDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtUomMapper extends EntityMapper<RscDtUomDTO, RscDtUom> {

    RscDtUomDTO toDto(RscDtUom rscDtUom);

    RscDtUom toEntity(RscDtUomDTO rscDtUomDTO);

    default RscDtUom fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtUom rscDtUom = new RscDtUom();
        rscDtUom.setId(id);
        return rscDtUom;
    }
}