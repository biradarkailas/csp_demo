package com.example.validation.mapper;

import com.example.validation.domain.RscIotRMPurchasePlanCoverDays;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCoverDaysDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class, RscDtItemCodeMapper.class, RscIotRMPurchasePlanCalculationsMapper.class})
public interface RscIotRMPurchasePlanCoverDaysMapper extends EntityMapper<RscIotRMPurchasePlanCoverDaysDTO, RscIotRMPurchasePlanCoverDays> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rmPurchasePlanCalculations.id", target = "rmPurchasePlanCalculationsId")
    RscIotRMPurchasePlanCoverDaysDTO toDto(RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays);

    @Mapping(source = "rmPurchasePlanCalculationsId", target = "rmPurchasePlanCalculations")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotRMPurchasePlanCoverDays toEntity(RscIotRMPurchasePlanCoverDaysDTO rscIotRMPurchasePlanCoverDaysDTO);

    default RscIotRMPurchasePlanCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRMPurchasePlanCoverDays rscIotRMPurchasePlanCoverDays = new RscIotRMPurchasePlanCoverDays();
        rscIotRMPurchasePlanCoverDays.setId(id);
        return rscIotRMPurchasePlanCoverDays;
    }
}