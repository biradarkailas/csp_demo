package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMLatestPurchasePlan;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscMtItemMapper.class,RscDtSupplierMapper.class, RscMtSupplierMapper.class,RscDtItemCodeMapper.class, RscItItemWiseSupplierMapper.class, RscDtPMRemarkMapper.class})
public interface RscIotPMLatestPurchasePlanMapper extends EntityMapper<RscIotPMPurchasePlanDTO, RscIotPMLatestPurchasePlan> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscMtPPheader.mpsDate", target = "mpsDate")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscDtPMRemark.id", target = "rscDtPMRemarkId")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.id", target = "rscDtSupplierId")
    RscIotPMPurchasePlanDTO toDto(RscIotPMLatestPurchasePlan rscIotPMLatestPurchasePlan);

    @Mapping(source = "rscDtPMRemarkId", target = "rscDtPMRemark")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIotPMLatestPurchasePlan toEntity(RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO);

    default RscIotPMLatestPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMLatestPurchasePlan rscIotPMLatestPurchasePlan = new RscIotPMLatestPurchasePlan();
        rscIotPMLatestPurchasePlan.setId(id);
        return rscIotPMLatestPurchasePlan;
    }
}
