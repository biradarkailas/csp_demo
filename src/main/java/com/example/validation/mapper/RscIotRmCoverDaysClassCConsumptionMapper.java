package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysClassCSummary;
import com.example.validation.dto.ClassWiseDetailsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysClassCConsumptionMapper extends EntityMapper<ClassWiseDetailsDTO, RscIotRmCoverDaysClassCSummary> {
      @Mapping(source = "totalCoverDaysWithoutSit", target = "totalCoverDays")
    ClassWiseDetailsDTO toDto(RscIotRmCoverDaysClassCSummary rscIotRmCoverDaysClassCSummary);

    default RscIotRmCoverDaysClassCSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysClassCSummary rscIotRmCoverDaysClassCSummary = new RscIotRmCoverDaysClassCSummary();
        rscIotRmCoverDaysClassCSummary.setId(id);
        return rscIotRmCoverDaysClassCSummary;
    }
}