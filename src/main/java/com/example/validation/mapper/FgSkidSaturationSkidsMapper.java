package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgSkidSaturationSummary;
import com.example.validation.dto.FgSkidSaturationSkidsDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtSkidsMapper.class})
public interface FgSkidSaturationSkidsMapper extends EntityMapper<FgSkidSaturationSkidsDTO, RscIotFgSkidSaturationSummary> {
    @Mapping(source = "rscDtSkids.id", target = "rscDtSkidsId")
    @Mapping(source = "rscDtSkids.name", target = "skidName")
    FgSkidSaturationSkidsDTO toDto(RscIotFgSkidSaturationSummary rscIotFgSkidSaturationSummary);

    default RscIotFgSkidSaturationSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgSkidSaturationSummary rscIotFgSkidSaturationSummary = new RscIotFgSkidSaturationSummary();
        rscIotFgSkidSaturationSummary.setId(id);
        return rscIotFgSkidSaturationSummary;
    }
}
