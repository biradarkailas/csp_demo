package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgLineSaturationSummary;
import com.example.validation.dto.RscIotFgLineSaturationSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtLinesMapper.class})
public interface RscIotFgLineSaturationSummaryMapper extends EntityMapper<RscIotFgLineSaturationSummaryDTO, RscIotFgLineSaturationSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtLines.id", target = "rscDtLinesId")
    @Mapping(source = "rscDtLines.name", target = "lineName")
    RscIotFgLineSaturationSummaryDTO toDto(RscIotFgLineSaturationSummary rscIotFgLineSaturationSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtLinesId", target = "rscDtLines")
    RscIotFgLineSaturationSummary toEntity(RscIotFgLineSaturationSummaryDTO rscIotFgLineSaturationSummaryDTO);

    default RscIotFgLineSaturationSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgLineSaturationSummary rscIotFgLineSaturationSummary = new RscIotFgLineSaturationSummary();
        rscIotFgLineSaturationSummary.setId(id);
        return rscIotFgLineSaturationSummary;
    }
}