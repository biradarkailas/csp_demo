package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmMes;
import com.example.validation.dto.RmMesStockPaginationDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RmMesStockDtoMapper extends EntityMapper<RmMesStockPaginationDto, RscMtRmMes> {
    @Mapping(source = "quantity", target = "lotWiseStock")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    RmMesStockPaginationDto toDto(RscMtRmMes rscMtRmMes);

    default RscMtRmMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmMes rscMtRmMes = new RscMtRmMes();
        rscMtRmMes.setId(id);
        return rscMtRmMes;
    }
}
