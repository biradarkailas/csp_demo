package com.example.validation.mapper;

import com.example.validation.domain.RscIotRmCoverDaysSummary;
import com.example.validation.dto.CoverDaysConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class})
public interface RscIotRmCoverDaysConsumptionMapper extends EntityMapper<CoverDaysConsumptionDTO, RscIotRmCoverDaysSummary> {
    CoverDaysConsumptionDTO toDto(RscIotRmCoverDaysSummary rscIotRmCoverDaysSummary);

    default RscIotRmCoverDaysSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotRmCoverDaysSummary rscIotRmCoverDaysSummary = new RscIotRmCoverDaysSummary();
        rscIotRmCoverDaysSummary.setId(id);
        return rscIotRmCoverDaysSummary;
    }
}
