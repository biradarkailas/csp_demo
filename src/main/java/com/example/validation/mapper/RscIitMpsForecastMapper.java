package com.example.validation.mapper;

import com.example.validation.domain.RscIitMpsForecast;
import com.example.validation.dto.RscIitMpsForecastDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscMtPPheaderMapper.class})
public interface RscIitMpsForecastMapper extends EntityMapper<RscIitMpsForecastDTO, RscIitMpsForecast> {

    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPHeaderId")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    RscIitMpsForecastDTO toDto(RscIitMpsForecast rscIitMpsForecast);

    @Mapping(source = "rscMtPPHeaderId", target = "rscMtPPheader")
    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    RscIitMpsForecast toEntity(RscIitMpsForecastDTO rscIitMpsForecastDTO);

    default RscIitMpsForecast fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIitMpsForecast rscIitMpsForecast = new RscIitMpsForecast();
        rscIitMpsForecast.setId(id);
        return rscIitMpsForecast;
    }
}
