package com.example.validation.mapper;

import com.example.validation.domain.RscDtCoverDays;
import com.example.validation.dto.RscDtCoverDaysDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtCoverDaysMapper extends EntityMapper<RscDtCoverDaysDTO, RscDtCoverDays> {

    RscDtCoverDaysDTO toDto(RscDtCoverDays rscDtCoverDays);

    RscDtCoverDays toEntity(RscDtCoverDaysDTO rscDtCoverDaysDTO);

    default RscDtCoverDays fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtCoverDays rscDtCoverDays = new RscDtCoverDays();
        rscDtCoverDays.setId(id);
        return rscDtCoverDays;
    }
}
