package com.example.validation.mapper;

import com.example.validation.domain.RscItPMSlob;
import com.example.validation.dto.PmSlobDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class,RscMtItemMapper.class, RscDtReasonMapper.class, RscDtItemCodeMapper.class, RscItItemWiseSupplierMapper.class, RscMtSupplierMapper.class, RscDtSupplierMapper.class})
public interface PmSlobMapper extends EntityMapper<PmSlobDTO, RscItPMSlob> {
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "description")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplier")
    @Mapping(source = "rscMtItem.rscDtItemType.name", target = "category")
    @Mapping(source = "stockQuantity", target = "stock")
    @Mapping(source = "stockValue", target = "value")
    @Mapping(source = "totalConsumptionQuantity", target = "totalStock")
    @Mapping(source = "mapPrice", target = "map")
    @Mapping(source = "rscDtReason.id", target = "reasonId")
    @Mapping(source = "rscDtReason.reason", target = "reason")
    PmSlobDTO toDto(RscItPMSlob rscItPMSlob);

    @Mapping(source = "rscMtItemId", target = "rscMtItem")
    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscItItemWiseSupplierId", target = "rscItItemWiseSupplier")
    @Mapping(source = "totalStock", target = "totalConsumptionQuantity")
    @Mapping(source = "stock", target = "stockQuantity")
    @Mapping(source = "value", target = "stockValue")
    @Mapping(source = "map", target = "mapPrice")
    @Mapping(source = "reasonId", target = "rscDtReason")
    @Mapping(source = "remark", target = "remark")
    RscItPMSlob toEntity(PmSlobDTO pmSlobDTO);

    default RscItPMSlob fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItPMSlob rscItPMSlob = new RscItPMSlob();
        rscItPMSlob.setId(id);
        return rscItPMSlob;
    }
}