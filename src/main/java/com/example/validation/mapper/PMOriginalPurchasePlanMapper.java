package com.example.validation.mapper;

import com.example.validation.domain.RscIotPMOriginalPurchasePlan;
import com.example.validation.dto.supply.PurchasePlanDTO;
import org.mapstruct.AfterMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingTarget;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class, RscItItemWiseSupplierMapper.class, RscDtMoqMapper.class, RscDtTechnicalSeriesMapper.class, RscMtSupplierMapper.class, RscDtSupplierMapper.class, RscDtPMRemarkMapper.class})
public interface PMOriginalPurchasePlanMapper extends EntityMapper<PurchasePlanDTO, RscIotPMOriginalPurchasePlan> {

    @Mapping(source = "rscMtPPheader.id", target = "mpsId")
    @Mapping(source = "rscMtItem.id", target = "materialId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "materialCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "materialDescription")
    @Mapping(source = "rscDtPMRemark.id", target = "rscDtPMRemarkId")
    @Mapping(source = "rscDtPMRemark.name", target = "remarkName")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "stockValue")
    @Mapping(source = "rscItItemWiseSupplier.id", target = "rscItItemWiseSupplierId")
    @Mapping(source = "rscItItemWiseSupplier.rscDtMoq.moqValue", target = "moqValue")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierName", target = "supplierName")
    @Mapping(source = "rscItItemWiseSupplier.rscMtSupplier.rscDtSupplier.supplierCode", target = "supplierCode")
    @Mapping(source = "m1Value", target = "supplyMonthValues.month1.value")
    @Mapping(source = "m2Value", target = "supplyMonthValues.month2.value")
    @Mapping(source = "m3Value", target = "supplyMonthValues.month3.value")
    @Mapping(source = "m4Value", target = "supplyMonthValues.month4.value")
    @Mapping(source = "m5Value", target = "supplyMonthValues.month5.value")
    @Mapping(source = "m6Value", target = "supplyMonthValues.month6.value")
    @Mapping(source = "m7Value", target = "supplyMonthValues.month7.value")
    @Mapping(source = "m8Value", target = "supplyMonthValues.month8.value")
    @Mapping(source = "m9Value", target = "supplyMonthValues.month9.value")
    @Mapping(source = "m10Value", target = "supplyMonthValues.month10.value")
    @Mapping(source = "m11Value", target = "supplyMonthValues.month11.value")
    @Mapping(source = "m12Value", target = "supplyMonthValues.month12.value")
    @Mapping(target = "totalValue", ignore = true)
    PurchasePlanDTO toDto(RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan);

    @AfterMapping
    default void calculateTotalValue(@MappingTarget PurchasePlanDTO purchasePlanDTO) {
        purchasePlanDTO.setTotalValue(getTotalValue(purchasePlanDTO));
    }

    private Double getTotalValue(PurchasePlanDTO purchasePlanDTO) {
        return purchasePlanDTO.getSupplyMonthValues().getMonth1().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth2().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth3().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth4().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth5().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth6().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth7().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth8().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth9().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth10().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth11().getValue()
                + purchasePlanDTO.getSupplyMonthValues().getMonth12().getValue();
    }

    default RscIotPMOriginalPurchasePlan fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotPMOriginalPurchasePlan rscIotPMOriginalPurchasePlan = new RscIotPMOriginalPurchasePlan();
        rscIotPMOriginalPurchasePlan.setId(id);
        return rscIotPMOriginalPurchasePlan;
    }
}