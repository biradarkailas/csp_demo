package com.example.validation.mapper;

import com.example.validation.domain.RscIotFgSkidSaturationSummary;
import com.example.validation.dto.RscIotFgLineSaturationSummaryDTO;
import com.example.validation.dto.RscIotFgSkidSaturationSummaryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtPPheaderMapper.class, RscDtSkidsMapper.class})
public interface RscIotFgSkidSaturationSummaryMapper extends EntityMapper<RscIotFgSkidSaturationSummaryDTO, RscIotFgSkidSaturationSummary> {
    @Mapping(source = "rscMtPPheader.id", target = "rscMtPPheaderId")
    @Mapping(source = "rscDtSkids.id", target = "rscDtSkidsId")
    @Mapping(source = "rscDtSkids.name", target = "skidName")
    RscIotFgSkidSaturationSummaryDTO toDto(RscIotFgSkidSaturationSummary rscIotFgSkidSaturationSummary);

    @Mapping(source = "rscMtPPheaderId", target = "rscMtPPheader")
    @Mapping(source = "rscDtSkidsId", target = "rscDtSkids")
    RscIotFgSkidSaturationSummary toEntity(RscIotFgSkidSaturationSummaryDTO rscIotFgSkidSaturationSummaryDTO);

    default RscIotFgSkidSaturationSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFgSkidSaturationSummary rscIotFgSkidSaturationSummary = new RscIotFgSkidSaturationSummary();
        rscIotFgSkidSaturationSummary.setId(id);
        return rscIotFgSkidSaturationSummary;
    }
}