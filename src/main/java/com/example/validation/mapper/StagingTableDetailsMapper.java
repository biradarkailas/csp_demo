package com.example.validation.mapper;

import com.example.validation.domain.StagingTableDetails;
import com.example.validation.dto.StagingTableDetailsDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface StagingTableDetailsMapper extends EntityMapper<StagingTableDetailsDTO, StagingTableDetails> {

    StagingTableDetailsDTO toDto(StagingTableDetails stagingTableDetails);

    StagingTableDetails toEntity(StagingTableDetailsDTO stagingTableDetailsDTO);

    default StagingTableDetails fromId(Long id) {
        if (id == null) {
            return null;
        }
        StagingTableDetails stagingTableDetails = new StagingTableDetails();
        stagingTableDetails.setId(id);
        return stagingTableDetails;
    }
}