package com.example.validation.mapper;

import com.example.validation.domain.RscDtBrand;
import com.example.validation.dto.RscDtBrandDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtBrandMapper extends EntityMapper<RscDtBrandDTO, RscDtBrand> {

    RscDtBrandDTO toDto(RscDtBrand rscDtBrand);

    RscDtBrand toEntity(RscDtBrandDTO rscDtBrandDTO);

    default RscDtBrand fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtBrand rscDtBrand = new RscDtBrand();
        rscDtBrand.setId(id);
        return rscDtBrand;
    }
}
