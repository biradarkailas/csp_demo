package com.example.validation.mapper;

import com.example.validation.domain.RscDtItemClass;
import com.example.validation.dto.RscDtItemClassDTO;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", uses = {})
public interface RscDtItemClassMapper extends EntityMapper<RscDtItemClassDTO, RscDtItemClass> {

    RscDtItemClassDTO toDto(RscDtItemClass rscDtItemClass);

    RscDtItemClass toEntity(RscDtItemClassDTO rscDtItemClassDTO);

    default RscDtItemClass fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscDtItemClass rscDtItemClass = new RscDtItemClass();
        rscDtItemClass.setId(id);
        return rscDtItemClass;
    }
}
