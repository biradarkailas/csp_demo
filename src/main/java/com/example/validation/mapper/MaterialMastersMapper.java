package com.example.validation.mapper;

import com.example.validation.domain.RscMtItem;
import com.example.validation.dto.MaterialMasterDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtItemCodeMapper.class,
        RscDtSafetyStockMapper.class, RscDtMapMapper.class, RscDtStdPriceMapper.class, RscDtTechnicalSeriesMapper.class, RscDtMoqMapper.class})
public interface MaterialMastersMapper extends EntityMapper<MaterialMasterDTO, RscMtItem> {
    @Mapping(source = "id", target = "rscMtItemId")
    @Mapping(source = "rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscDtMoq.id", target = "rscDtMoqId")
    @Mapping(source = "rscDtMoq.moqValue", target = "moqValue")
    @Mapping(source = "rscDtTechnicalSeries.id", target = "rscDtTechnicalSeriesId")
    @Mapping(source = "rscDtTechnicalSeries.seriesValue", target = "seriesValue")
    @Mapping(source = "rscDtSafetyStock.id", target = "rscDtSafetyStockId")
    @Mapping(source = "rscDtSafetyStock.stockValue", target = "stockValue")
    @Mapping(source = "rscDtMap.mapPrice", target = "mapPrice")
    @Mapping(source = "rscDtStdPrice.stdPrice", target = "stdPrice")
    MaterialMasterDTO toDto(RscMtItem rscMtItem);

    default RscMtItem fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtItem rscMtItem = new RscMtItem();
        rscMtItem.setId(id);
        return rscMtItem;
    }
}
