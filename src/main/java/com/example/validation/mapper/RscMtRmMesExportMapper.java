package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmMes;
import com.example.validation.dto.RscMtRmMesExportDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface RscMtRmMesExportMapper extends EntityMapper<RscMtRmMesExportDTO, RscMtRmMes> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "itemCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "itemDescription")
    @Mapping(source = "rscMtItem.rscDtSafetyStock.stockValue", target = "safetyStock")
    RscMtRmMesExportDTO toDto(RscMtRmMes rscMtRmMes);

    RscMtRmMes toEntity(RscMtRmMesExportDTO rscMtRmMesExportDto);

    default RscMtRmMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtRmMes rscMtRmMes = new RscMtRmMes();
        rscMtRmMes.setId(id);
        return rscMtRmMes;
    }
}
