package com.example.validation.mapper;

import com.example.validation.domain.RscItBPGrossConsumption;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class, RscDtItemCodeMapper.class, RscMtPPheaderMapper.class})
public interface BPGrossConsumptionMapper extends EntityMapper<GrossConsumptionDTO, RscItBPGrossConsumption> {

    @Mapping(source = "rscMtItem.id", target = "materialId")
    @Mapping(source = "rscMtItem.rscDtItemCode.code", target = "materialCode")
    @Mapping(source = "rscMtItem.rscDtItemCode.description", target = "materialDescription")
    @Mapping(source = "m1Value", target = "grossConsumptionMonthValues.month1.value")
    @Mapping(source = "m2Value", target = "grossConsumptionMonthValues.month2.value")
    @Mapping(source = "m3Value", target = "grossConsumptionMonthValues.month3.value")
    @Mapping(source = "m4Value", target = "grossConsumptionMonthValues.month4.value")
    @Mapping(source = "m5Value", target = "grossConsumptionMonthValues.month5.value")
    @Mapping(source = "m6Value", target = "grossConsumptionMonthValues.month6.value")
    @Mapping(source = "m7Value", target = "grossConsumptionMonthValues.month7.value")
    @Mapping(source = "m8Value", target = "grossConsumptionMonthValues.month8.value")
    @Mapping(source = "m9Value", target = "grossConsumptionMonthValues.month9.value")
    @Mapping(source = "m10Value", target = "grossConsumptionMonthValues.month10.value")
    @Mapping(source = "m11Value", target = "grossConsumptionMonthValues.month11.value")
    @Mapping(source = "m12Value", target = "grossConsumptionMonthValues.month12.value")
    GrossConsumptionDTO toDto(RscItBPGrossConsumption rscItBPGrossConsumption);

    default RscItBPGrossConsumption fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscItBPGrossConsumption rscItBPGrossConsumption = new RscItBPGrossConsumption();
        rscItBPGrossConsumption.setId(id);
        return rscItBPGrossConsumption;
    }
}