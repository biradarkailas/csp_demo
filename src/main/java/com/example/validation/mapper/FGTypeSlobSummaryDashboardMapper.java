package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeSlobSummary;
import com.example.validation.dto.slob.FGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface FGTypeSlobSummaryDashboardMapper extends EntityMapper<FGSlobSummaryDashboardDTO, RscIotFGTypeSlobSummary> {
    @Mapping(source = "totalObsoleteItemValue", target = "totalOBValue")
    @Mapping(source = "totalSlowItemValue", target = "totalSLValue")
    @Mapping(source = "stockQualityValue", target = "iqValue")
    @Mapping(source = "obsoleteItemPercentage", target = "obPercentage")
    @Mapping(source = "slowItemPercentage", target = "slPercentage")
    @Mapping(source = "stockQualityPercentage", target = "iqPercentage")
    FGSlobSummaryDashboardDTO toDto(RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary);

    default RscIotFGTypeSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeSlobSummary rscIotFGTypeSlobSummary = new RscIotFGTypeSlobSummary();
        rscIotFGTypeSlobSummary.setId(id);
        return rscIotFGTypeSlobSummary;
    }
}
