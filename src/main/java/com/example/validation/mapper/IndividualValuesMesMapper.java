package com.example.validation.mapper;

import com.example.validation.domain.RscMtRmMes;
import com.example.validation.dto.IndividualValuesMesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtRmMesMapper.class})
public interface IndividualValuesMesMapper extends EntityMapper<IndividualValuesMesDTO, RscMtRmMes> {
    @Mapping(source = "quantity", target = "value")
    IndividualValuesMesDTO toDto(RscMtRmMes rscMtRmMes);
}
