package com.example.validation.mapper;

import com.example.validation.domain.RscIotFGTypeDivisionSlobSummary;
import com.example.validation.dto.slob.DivisionWiseFGSlobSummaryDashboardDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscDtDivisionMapper.class})
public interface DivisionWiseFGSlobSummaryDashboardDTOMapper extends EntityMapper<DivisionWiseFGSlobSummaryDashboardDTO, RscIotFGTypeDivisionSlobSummary> {

    @Mapping(source = "rscDtDivision.aliasName", target = "type")
    @Mapping(source = "totalSlobValue", target = "slobValue")
    @Mapping(source = "totalSlowItemValue", target = "slValue")
    @Mapping(source = "totalObsoleteItemValue", target = "obValue")
    DivisionWiseFGSlobSummaryDashboardDTO toDto(RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary);

    default RscIotFGTypeDivisionSlobSummary fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscIotFGTypeDivisionSlobSummary rscIotFGTypeDivisionSlobSummary = new RscIotFGTypeDivisionSlobSummary();
        rscIotFGTypeDivisionSlobSummary.setId(id);
        return rscIotFGTypeDivisionSlobSummary;
    }
}