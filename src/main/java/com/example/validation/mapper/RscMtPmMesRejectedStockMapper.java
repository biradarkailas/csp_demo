package com.example.validation.mapper;

import com.example.validation.domain.RscMtPmMesRejectedStock;
import com.example.validation.dto.RscMtPmMesRejectedStockDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {})
public interface RscMtPmMesRejectedStockMapper extends EntityMapper<RscMtPmMesRejectedStockDTO, RscMtPmMesRejectedStock> {
    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    RscMtPmMesRejectedStockDTO toDto(RscMtPmMesRejectedStock rscMtPmMesRejectedStock);

    RscMtPmMesRejectedStock toEntity(RscMtPmMesRejectedStockDTO rscMtPmMesRejectedStockDTO);

    default RscMtPmMesRejectedStock fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtPmMesRejectedStock rscMtPmMesRejectedStock = new RscMtPmMesRejectedStock();
        rscMtPmMesRejectedStock.setId( id );
        return rscMtPmMesRejectedStock;
    }
}
