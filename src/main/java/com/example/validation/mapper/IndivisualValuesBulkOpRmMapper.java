package com.example.validation.mapper;

import com.example.validation.domain.RscIitRmBulkOpRm;
import com.example.validation.dto.IndividualValuesBulkOpRmDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscIotRmStockMapper.class})
public interface IndivisualValuesBulkOpRmMapper extends EntityMapper<IndividualValuesBulkOpRmDTO, RscIitRmBulkOpRm> {
    @Mapping(source = "quantity", target = "value")
    @Mapping(source = "rscMtBulkItem.rscDtItemCode.code", target = "code")
    @Mapping(source = "rscMtBulkItem.rscDtItemCode.description", target = "description")
    IndividualValuesBulkOpRmDTO toDto(RscIitRmBulkOpRm rscIitRmBulkOpRm);
}
