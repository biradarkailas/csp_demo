package com.example.validation.mapper;

import com.example.validation.domain.RscMtFgMes;
import com.example.validation.dto.RscMtFgMesDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring", uses = {RscMtItemMapper.class})
public interface RscMtFgMesMapper extends EntityMapper<RscMtFgMesDTO, RscMtFgMes> {

    @Mapping(source = "rscMtItem.id", target = "rscMtItemId")
    RscMtFgMesDTO toDto(RscMtFgMes rscMtFgMes);

    RscMtFgMes toEntity(RscMtFgMesDTO rscMtFgMesDTO);

    default RscMtFgMes fromId(Long id) {
        if (id == null) {
            return null;
        }
        RscMtFgMes rscMtFgMes = new RscMtFgMes();
        rscMtFgMes.setId( id );
        return rscMtFgMes;
    }
}