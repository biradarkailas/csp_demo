package com.example.validation.service;

import com.example.validation.dto.RscIitRMItemWiseSupplierDetailsDTO;

public interface RscIitRMItemWiseSupplierDetailsService {
    RscIitRMItemWiseSupplierDetailsDTO findByItemWiseSupplierId(Long itemWiseSupplierId);
}