package com.example.validation.service;

import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;

import java.util.List;

public interface RscItWIPLogicalConsumptionService {
    void saveAll(List<RscItLogicalConsumptionDTO> wipRscItLogicalConsumptionDTOS, Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getWIPLogicalConsumptionByBulk(Long ppHeaderId, Long fgItemId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId);

    Long count(Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getWIPLogicalConsumption(Long ppHeaderId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderIdAndChildItemId(Long ppHeaderId, Long wipItemId);

    void deleteAll(Long ppHeaderId);
}