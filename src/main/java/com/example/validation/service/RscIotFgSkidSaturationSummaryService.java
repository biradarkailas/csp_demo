package com.example.validation.service;

import com.example.validation.dto.FgSkidSaturationSkidWiseDetailsDTO;
import com.example.validation.dto.FgSkidSaturationSkidsDetailsDTO;
import com.example.validation.dto.RscIotFgSkidSaturationSummaryDTO;

import java.util.List;

public interface RscIotFgSkidSaturationSummaryService {
    void createFgSkidSaturationSummary(Long ppHeaderId);

    List<RscIotFgSkidSaturationSummaryDTO> getFgSkidSaturationsList(Long ppHeaderId);

    FgSkidSaturationSkidWiseDetailsDTO getSingleSkidsDetails(Long ppHeaderId, Long rscDtSkidsId);

    FgSkidSaturationSkidsDetailsDTO getFgSkidSaturationsSkids(Long ppHeaderId);

    List<RscIotFgSkidSaturationSummaryDTO> getSkidsList(Long rscDtSkidsId);

    void deleteAll(Long ppHeaderId);
}
