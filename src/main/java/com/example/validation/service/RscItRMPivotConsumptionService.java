package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscItRMPivotConsumptionService {
    void createRMPivotConsumption(Long ppHeaderId);

    ConsumptionPivotMainDTO getRMPivotConsumption(Long ppHeaderId);

    ExcelExportFiles getRmPivotConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    List<FGDetailsDTO> getFGDetails(Long ppHeaderId, Long itemId);

    void deleteAll(Long ppHeaderId);
}