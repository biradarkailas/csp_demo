package com.example.validation.service;

import com.example.validation.dto.RscDtTransportModeDTO;

import java.util.List;

public interface RscDtTransportModeService {
    List<RscDtTransportModeDTO> findAll();
}
