package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.FGSlobSummaryWithFgTypeAndDivisionDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.slob.*;
import org.springframework.stereotype.Service;

@Service
public interface RscIotFGSlobSummaryService {
    void createFGSlobSummary(Long ppHeaderId);

    FGSlobSummaryDTO getFGSlobSummaryDTO(Long ppHeaderId);

    FGSlobDeviationMainDTO getFGTotalSlobDeviation(Long ppHeaderId);

    FGSlobSummaryDashboardDTO getFGSlobSummaryDashboardDTO(Long ppHeaderId);

    DivisionFGSlobSummaryIQDashboardDTO getDivisionFGSlobSummaryIQDashboardDTO(Long ppHeaderId, Long fgTypeId);

    ExcelExportFiles exportFgSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO);

    RscIotFGSlobSummaryDTO findByRscMtPPheaderId(Long ppHeaderId);

    FGSlobSummaryDashboardDTO findSlobSummaryByRscMtPPheaderId(Long ppHeaderId);

    FGSlobSummaryWithFgTypeAndDivisionDTO getFGSlobSummaryDashboardDTO(Long ppHeaderId, Long fgTypeId, Long divisionId);

    void deleteAll(Long ppHeaderId);
}