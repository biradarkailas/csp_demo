package com.example.validation.service;

import com.example.validation.dto.MouldSaturationDashboardDTO;
import com.example.validation.dto.supply.PurchasePlanDTO;

import java.util.List;

public interface RscIotLatestTotalMouldSaturationSummaryService {
    void createMouldSaturationSummary(Long ppHeaderId);

    MouldSaturationDashboardDTO getMouldSaturationDashboardDtoDetails(Long ppHeaderId, Long monthValue);

    void updateMouldSaturationSummaryForUpdatedPurchasePlan(List<PurchasePlanDTO> purchasePlanDTOList, Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}
