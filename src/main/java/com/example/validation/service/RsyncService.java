package com.example.validation.service;

import java.util.Set;

public interface RsyncService {
    Set<String> loadFilesWithoutRsync(String mpsName);
}
