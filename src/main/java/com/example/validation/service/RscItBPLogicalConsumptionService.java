package com.example.validation.service;

import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;

import java.util.List;

public interface RscItBPLogicalConsumptionService {
    void saveAll(List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOS, Long ppHeaderId);

    void saveAllUnique(List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOS, Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getBPLogicalConsumptionByBulk(Long ppHeaderId, Long bulkItemId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId);

    Long count(Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getBPLogicalConsumption(Long ppHeaderId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderIdAndChildItemId(Long ppHeaderId, Long bpItemId);

    void deleteAll(Long ppHeaderId);
}