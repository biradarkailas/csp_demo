package com.example.validation.service;

import com.example.validation.domain.RscDtPMRemark;
import com.example.validation.dto.RscDtPMRemarkDTO;

import java.util.List;

public interface RscDtPMRemarkService {
    Long findRscDtPMRemarkIdByAliasName(String aliasName);

    List<RscDtPMRemarkDTO> findAll();

    RscDtPMRemarkDTO save(RscDtPMRemark rscDtPMRemark);
}