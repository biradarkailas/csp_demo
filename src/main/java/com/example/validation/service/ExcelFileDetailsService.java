package com.example.validation.service;

import com.example.validation.dto.ExcelFiledetailsDTO;

import java.util.List;

public interface ExcelFileDetailsService {
    List<String> getSheetNamesByFileName(String fileName);

    ExcelFiledetailsDTO findOneBySheetName(String sheetName);

    List<ExcelFiledetailsDTO> getExcelFiledetailsDTOs(String fileName);
}