package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.slob.FGSlobDTO;
import com.example.validation.dto.slob.FGSlobTotalValueDetailDTO;
import com.example.validation.dto.slob.RscIotFGSlobDTO;

import java.util.List;

public interface RscIotFGSlobService {
    void createFGSlob(Long ppHeaderId);

    List<RscIotFGSlobDTO> getRscIotFGSlobDTO(Long ppHeaderId);

    FGSlobTotalValueDetailDTO getFGSlobTotalValueDetail(Long ppHeaderId);

    ExcelExportFiles fgSlobExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<FGSlobDTO> getAllFGSlobDTO(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}