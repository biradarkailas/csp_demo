package com.example.validation.service;

import com.example.validation.dto.OtifCumulativeScoreAnalysisDTO;
import com.example.validation.dto.OtifCumulativeSuppliersDetailsDTO;

public interface PmOtifCumulativeScoreService {
    void calculateOtifCumulativeScore();

    OtifCumulativeSuppliersDetailsDTO getSuppliersListByScore();

    OtifCumulativeScoreAnalysisDTO getSuppliersDetails(Long supplierId);
}
