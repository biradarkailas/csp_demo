package com.example.validation.service;

import java.util.Set;

public interface DataLoadService {
    String loadData(Set<String> files, String mpsName);
}