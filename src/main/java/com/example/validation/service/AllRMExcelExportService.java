package com.example.validation.service;

public interface AllRMExcelExportService {
    String createAllRMExports(Long ppHeaderId);

    void createDirectory(String year, String month, String date, String name);
}
