package com.example.validation.service;

public interface ResourceKeyValueService {
    String getResourceValueByKey(String key);
}