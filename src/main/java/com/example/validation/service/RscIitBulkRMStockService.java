package com.example.validation.service;

import com.example.validation.dto.RscIitBulkRMStockDTO;

import java.util.List;

public interface RscIitBulkRMStockService {
    List<RscIitBulkRMStockDTO> getAllRscIitBulkRMStockDTOs(Long ppHeaderId);
}