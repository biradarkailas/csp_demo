package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;

import java.util.List;
import java.util.Map;
import java.util.Set;

public interface RscItPMLogicalConsumptionService {
    List<LogicalConsumptionMainDTO> getPMLogicalConsumptionByFG(Long ppHeaderId, Long fgMtItemId);

    void saveAll(List<RscItLogicalConsumptionDTO> pmRscItLogicalConsumptionDTOS, Long ppHeaderId);

    Set<Long> getDistinctPMMtItemIds();

    List<RscItLogicalConsumptionDTO> findAll();

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

    ExcelExportFiles getPmLogicalConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, Map<Long, ItemDetailDTO> mpsPlanDetailMap);

    Long count(Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getPMLogicalConsumption(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}