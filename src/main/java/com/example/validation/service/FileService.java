package com.example.validation.service;

import com.example.validation.dto.FileNameDTO;

import java.util.List;

public interface FileService {
    List<FileNameDTO> findAllFiles(String tagName);
}