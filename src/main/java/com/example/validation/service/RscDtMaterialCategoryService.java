package com.example.validation.service;

import com.example.validation.dto.RscDtMaterialCategoryDTO;

import java.util.List;

public interface RscDtMaterialCategoryService {
    List<RscDtMaterialCategoryDTO> findAll();
}
