package com.example.validation.service;

import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;

import java.util.List;

public interface RscItWIPGrossConsumptionService {
    void saveAll(List<RscItGrossConsumptionDTO> wipRscItGrossConsumptionDTOs, Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<ItemDetailDTO> findByAllRscMtPPheaderId(Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllGrossConsumptionDTOByRscMtPPheaderId(Long ppHeaderId);

    Long count(Long ppHeaderId);

    ItemDetailDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long wipItemId);

    void deleteAll(Long ppHeaderId);
}