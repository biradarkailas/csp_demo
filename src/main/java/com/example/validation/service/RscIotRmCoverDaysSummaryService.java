package com.example.validation.service;

import com.example.validation.dto.CoverDaysConsumptionDTO;
import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import com.example.validation.dto.RmCoverDaysSummaryDTO;
import com.example.validation.dto.RscIotRmCoverDaysSummaryDTO;

public interface RscIotRmCoverDaysSummaryService {
    void saveCoverDaysSummary(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO);

    RmCoverDaysSummaryDTO getRmCoverDaysSummaryDetailsofMonths(Long ppHeaderId);

    RscIotRmCoverDaysSummaryDTO getRmCoverDaysSummary(Long ppHeaderId);

    CoverDaysSummaryDetailsDTO getRmCoverDaysSummaryAllDetails(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllById(Long ppHeaderId);

    CoverDaysConsumptionDTO getTotalCoverDaysConsumption(Long ppHeaderId);
}
