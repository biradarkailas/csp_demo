package com.example.validation.service;

import com.example.validation.dto.RscDtSupplierDTO;

import java.util.List;

public interface RscDtSupplierService {
    List<RscDtSupplierDTO> findAll();
}
