package com.example.validation.service;

import com.example.validation.dto.FgLineSaturationLineWiseDetailsDTO;
import com.example.validation.dto.FgLineSaturationLinesDetailsDTO;
import com.example.validation.dto.RscIotFgLineSaturationSummaryDTO;

import java.util.List;

public interface RscIotFgLineSaturationSummaryService {
    void createFgLineSaturationSummary(Long ppHeaderId);

    List<RscIotFgLineSaturationSummaryDTO> getFgLineSaturationsList(Long ppHeaderId);

    FgLineSaturationLineWiseDetailsDTO getSingleLinesDetails(Long ppHeaderId, Long rscDtLinesId);

    FgLineSaturationLinesDetailsDTO getFgLineSaturationsLines(Long ppHeaderId);

    List<RscIotFgLineSaturationSummaryDTO> getLinesList(Long rscDtLinesId);

    void deleteAll(Long ppHeaderId);
}
