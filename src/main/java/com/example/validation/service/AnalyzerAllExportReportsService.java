package com.example.validation.service;

import com.example.validation.dto.AllBackUpExportFileListsDTO;
import com.example.validation.dto.AnalyzerAllExportReportsDTO;
import com.example.validation.dto.AnalyzerExportFileDTO;

public interface AnalyzerAllExportReportsService {

    AnalyzerAllExportReportsDTO getAllExportFileNames(Long ppHeaderId);

    AnalyzerExportFileDTO downloadfile(Long ppHeaderId, String fgCheckedList, String pmCheckedList, String pmOtifCheckedList, String rmCheckedList);

    AllBackUpExportFileListsDTO getAllBackUpExportFileNames(String monthYearValue);

    AnalyzerExportFileDTO downloadBackupZipFiles(String monthYearValue, String firstCutCheckedList, String validatedCheckedList, String pmOtifCheckedList, String allModuleCheckedList);
}
