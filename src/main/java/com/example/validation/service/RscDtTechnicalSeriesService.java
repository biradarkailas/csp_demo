package com.example.validation.service;

import com.example.validation.dto.RscDtTechnicalSeriesDTO;

import java.util.List;

public interface RscDtTechnicalSeriesService {
    List<RscDtTechnicalSeriesDTO> findAll();
}
