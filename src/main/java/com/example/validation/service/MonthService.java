package com.example.validation.service;

import com.example.validation.dto.DeltaColumnNameDTO;
import com.example.validation.dto.MonthDetailDTO;

import java.time.LocalDate;
import java.util.List;

public interface MonthService {
    MonthDetailDTO getMonthNameDetail(Long ppHeaderId);

    DeltaColumnNameDTO getDeltaAnalysisMonthNames();

    MonthDetailDTO getMonthDetails(LocalDate date);
}