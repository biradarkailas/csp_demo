package com.example.validation.service;

import com.example.validation.dto.RscDtTestTaskDTO;

import java.util.List;

public interface RscDtTestTaskService {
     List<RscDtTestTaskDTO> getAllRscDtTestTask();
}
