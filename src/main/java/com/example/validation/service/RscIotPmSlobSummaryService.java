package com.example.validation.service;

import com.example.validation.dto.RscIotPmSlobSummaryDTO;
import com.example.validation.dto.SlobSummaryDashboardDTO;

import java.util.List;

public interface RscIotPmSlobSummaryService {
    List<RscIotPmSlobSummaryDTO> findAll(Long ppHeaderId);

    void save(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO);

    RscIotPmSlobSummaryDTO findByRscMtPPheaderId(Long rscMtPPHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteById(Long ppHeaderId);

    SlobSummaryDashboardDTO getPmSlobSummaryDashboard(Long ppHeaderId);
}
