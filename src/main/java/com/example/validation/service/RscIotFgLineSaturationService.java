package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotFgLineSaturationService {
    void createFgLineSaturation(Long ppHeaderId);

    void saveAll(List<RscIotFgLineSaturationDTO> rscIotFgLineSaturationDTOList);

    ExcelExportFiles fgLineExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    FgLineSaturationDetailsDTO getAllFgLineSaturation(Long ppHeaderId);

    LinesDetailsDTO getAllFgLines(Long ppHeaderId);

    FgLineDetailsDTO getFgLinesDetails(Long ppHeaderId, Long rscDtLineId);

    List<RscIotFgLineSaturationDTO> findAll(Long ppHeaderId);

    List<RscIotFgLineSaturationDTO> findAllByLinesIdAndRscMtPPheaderId(Long rscDtLineId, Long ppHeaderId);

    void deleteAll(Long ppHeaderId);

    List<LinesDTO> getAllLines();
}
