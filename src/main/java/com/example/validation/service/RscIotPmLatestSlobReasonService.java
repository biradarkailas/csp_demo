package com.example.validation.service;

import com.example.validation.dto.RscIotPmLatestSlobReasonDTO;

import java.util.List;

public interface RscIotPmLatestSlobReasonService {
    List<RscIotPmLatestSlobReasonDTO> findAll();

    void saveAll(List<RscIotPmLatestSlobReasonDTO> pmLatestSlobReasonDTOS);

    void saveOne(RscIotPmLatestSlobReasonDTO pmLatestSlobReasonDTO);
}
