package com.example.validation.service;

public interface LogicalConsumptionService {
    void createWIPLogicalConsumption(Long ppHeaderId);

    void createLogicalConsumption(Long ppHeaderId);

    void createRMLogicalConsumption(Long ppHeaderId);

    void createBPLogicalConsumption(Long ppHeaderId);

    void createSubBaseLogicalConsumption(Long ppHeaderId);
}
