package com.example.validation.service;

import com.example.validation.dto.LatestPurchasePlanDTO;
import com.example.validation.dto.OriginalPurchasePlanDTO;

import java.util.List;

public interface RscIotOriginalMouldSaturationService {
    String saveMouldSaturation(Long ppHeaderId);

    List<OriginalPurchasePlanDTO> findAll();

    void saveAll(List<OriginalPurchasePlanDTO> originalPurchasePlanDTOList);

    void save(OriginalPurchasePlanDTO originalPurchasePlanDTO);

    void saveAllOriginalMouldSaturation(List<LatestPurchasePlanDTO> latestPurchasePlanDTOList);

//    List<RscIotPMOriginalPurchasePlan> rscIotPMOriginalPurchasePlanListByPpHeaderId(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}
