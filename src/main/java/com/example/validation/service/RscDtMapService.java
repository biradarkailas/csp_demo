package com.example.validation.service;

import com.example.validation.dto.RscDtMapDTO;

import java.util.List;

public interface RscDtMapService {
    List<RscDtMapDTO> findAll();
}
