package com.example.validation.service;

import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import com.example.validation.dto.PmCoverDaysSummaryDTO;
import com.example.validation.dto.RscIotPmCoverDaysSummaryDTO;

import java.util.List;

public interface RscIotPmCoverDaysSummaryService {
    void saveCoverDaysSummary(RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO);

    PmCoverDaysSummaryDTO getPmCoverDaysSummaryDetails(Long ppHeaderId);

    CoverDaysSummaryDetailsDTO getPmCoverDaysSummaryAllDetails(Long ppHeaderId);

    RscIotPmCoverDaysSummaryDTO getPmCoverDaysSummary(Long ppHeaderId);

    List<RscIotPmCoverDaysSummaryDTO> getAllPmCoverDays(Long ppHeaderId);

    long countByRscMtPPheaderId(Long ppHeaderId);

    void deleteAllById(Long ppHeaderId);
}
