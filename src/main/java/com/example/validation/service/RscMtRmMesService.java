package com.example.validation.service;

import com.example.validation.dto.IndividualValuesMesDTO;
import com.example.validation.dto.RmMesStockPaginationDto;
import com.example.validation.dto.RscMtRmMesDTO;
import com.example.validation.dto.RscMtRmMesExportDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtRmMesService {
    List<RscMtRmMesDTO> findAllByRscMtItemId(Long rscMtItemId);

    List<IndividualValuesMesDTO> findAllIndividualValueDTORscMtItemId(Long rscMtItemId);

    List<RscMtRmMesExportDTO> findAll();

    void deleteAll(LocalDate stockDate);

    //    Mes New Api with optimization
    List<RmMesStockPaginationDto> findAllByStockDate(LocalDate stockDate);
}
