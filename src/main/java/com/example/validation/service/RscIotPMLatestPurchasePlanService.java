package com.example.validation.service;

import com.example.validation.dto.LatestPurchasePlanDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;

import java.util.List;

public interface RscIotPMLatestPurchasePlanService {

    List<RscIotPMPurchasePlanDTO> getPurchasePlanById(Long rscMtPPheaderId);

    List<LatestPurchasePlanDTO> getPurchasePlanListForMouldCalculation(Long rscMtPPHeaderId);

    List<LatestPurchasePlanDTO> getPurchasePlanListByIdList(List<Long> purchasePlanIdList, Long rscMtPpHeaderId);

    void deleteAll(Long ppHeaderId);
}
