package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotFgSkidSaturationService {
    void createFgSkidSaturation(Long ppHeaderId);

    void saveAll(List<RscIotFgSkidSaturationDTO> rscIotFgSkidSaturationDTOList);

    ExcelExportFiles fgSkidExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    FgSkidSaturationDetailsDTO getAllFgSkidSaturation(Long ppHeaderId);

    SkidsDetailsDTO getAllFgSkids(Long ppHeaderId);

    FgSkidDetailsDTO getFgSkidDetails(Long ppHeaderId, Long rscDtSkidId);

    List<RscIotFgSkidSaturationDTO> findAll(Long ppHeaderId);

    List<RscIotFgSkidSaturationDTO> findAllBySkidsIdAndRscMtPPheaderId(Long rscDtSkidId, Long ppHeaderId);

    void deleteAll(Long ppHeaderId);

    List<SkidsDTO> getAllSkids();
}
