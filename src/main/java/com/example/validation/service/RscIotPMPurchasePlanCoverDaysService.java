package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanCoverDaysDTO;

import java.util.List;

public interface RscIotPMPurchasePlanCoverDaysService {
    void calculatePMPurchasePlanCoverDays(Long ppHeaderId);

    List<RscIotPMPurchasePlanCoverDaysDTO> getAllPMPurchasePlanCoverDays(Long ppHeaderId);

    List<PurchasePlanCoverDaysEquationDTO> getPurchasePlanCoverDaysEquationDTOs(Long ppHeaderId);

    ExcelExportFiles pmPurchaseCoverdaysExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    ExcelExportFiles pmPurchaseStockEquationExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void deleteAll(Long ppHeaderId);
}
