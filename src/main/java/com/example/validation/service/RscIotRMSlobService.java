package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotRMSlobService {
    void calculateRscItRMSlob(Long rscMtPPHeaderId);

    void saveAllSlobs(List<RscIotRmSlobDTO> rscItRMSlobDtoList);

    void calculateRscItRMSlobSummary(Long rscMtPPHeaderId, Double sitValue);

    void save(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO);

    RMSlobDetailsDTO getAllRscIotRmSlob(Long rscMtPPHeaderId);

    List<RscIotRmSlobDTO> findAllRMSlobsByRscMtPpHeaderId(Long id);

    SlobTotalConsumptionDetailsDTO getTotalConsumption(Long ppHeaderId, Long rscMtItemId);

    SlobSummaryDetailsDTO getRmSlobSummaryDetails(Long ppHeaderId);

    RMSlobDetailsDTO getNullMapPriceDetails(Long rscMtPPHeaderId);

//    List<RmSlobDTO> saveAllRmSlobDetails(List<RmSlobDTO> rmSlobDTO);

    ExcelExportFiles rmSlobExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    ExcelExportFiles exportRmSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<RscDtReasonDTO> getAllRmSlobsReasons(Long rscMtPPHeaderId);

//    List<RmSlobDTO> saveAllRmSlobs(List<RmSlobDTO> rmSlobDTO, Double sitValue, Long ppHeaderId);

    SlobSummaryDTO getRmSlobSummary(Long ppHeaderId);

    SlobMovementDetailsDTO getRmSlobMovement();

    SlobInventoryQualityIndexDetailsDTO getRmSlobInventoryQualityIndex();

    SlobSummaryDashboardDTO getRmSlobSummaryDashboardDTO(Long ppHeaderId);

    ExcelExportFiles exportRmSlobMovementExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportRmSlobInventoryQualityIndexExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    void deleteAll(Long ppHeaderId);
}
