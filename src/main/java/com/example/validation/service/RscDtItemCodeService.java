package com.example.validation.service;

import com.example.validation.dto.RscDtItemCodeDTO;

import java.util.List;

public interface RscDtItemCodeService {
    List<RscDtItemCodeDTO> findAll();
}
