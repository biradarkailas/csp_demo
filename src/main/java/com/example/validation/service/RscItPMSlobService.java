package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscItPMSlobService {
    List<RscItPMSlobDTO> getRscItPMSlobByRscMTPPHeader(Long rscMtPpHeaderId);

    void saveAll(List<RscItPMSlobDTO> rscItPMSlobDtoList);

    void calculateRscItPMSlob(Long rscMtPPHeaderId);

    PMSlobDetailsDTO getAllRscItPmSlob(Long rscMtPPHeaderId);

    PMSlobDetailsDTO getNullMapPriceDetails(Long rscMtPPHeaderId);

//    List<PmSlobDTO> saveAllPmSlobDetails(List<PmSlobDTO> PmSlobDTO);

    void calculateRscItPMSlobSummary(Long rscMtPPHeaderId, Double sitValue);

    SlobTotalConsumptionDetailsDTO getTotalConsumption(Long ppHeaderId, Long rscMtItemId);

    SlobSummaryDetailsDTO getPmSlobSummaryDetails(Long ppHeaderId);

    ExcelExportFiles exportPmSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<RscDtReasonDTO> getAllPmSlobsReasons(Long rscMtPPHeaderId);

//    List<PmSlobDTO> saveAllPmSlobs(List<PmSlobDTO> PmSlobDTO, Double sitValue, Long ppHeaderId);

    ExcelExportFiles exportPmSlobExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    SlobSummaryDTO getPmSlobSummary(Long ppHeaderId);

    SlobMovementDetailsDTO getPmSlobMovement();

    SlobInventoryQualityIndexDetailsDTO getPmSlobInventoryQualityIndex();

    SlobSummaryDashboardDTO getPmSlobSummaryDashboardDTO(Long ppHeaderId);

    ExcelExportFiles pmCoverDaysSummaryExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    ExcelExportFiles exportPmSlobMovementExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportPmSlobInventoryQualityIndexExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    void deleteAll(Long ppHeaderId);
}
