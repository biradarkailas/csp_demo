package com.example.validation.service;

import com.example.validation.dto.RscDtMoqDTO;

import java.util.List;

public interface RscDtMoqService {
    List<RscDtMoqDTO> findAll();
}
