package com.example.validation.service;

import com.example.validation.dto.RmStockItemCodeDTO;
import com.example.validation.dto.RscIotRmStockDTO;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;

public interface RscIotRmStockService {
    List<RscIotRmStockDTO> findByRscMtItemIdAndMPSDate(Long id, LocalDate date);

    List<RscIotRmStockDTO> findByRscMtItemId(Long rscMtItemId, LocalDate mpsDate);

    List<RscIotRmStockDTO> findAll();

    List<RmStockItemCodeDTO> findAllDistinctByRscMtItem();

    List<RscIotRmStockDTO> findAllRmStocksByStockTypeTag(List<String> stockTypeList);

    void deleteAll(LocalDate stockDate);

    List findDataByMpsDateAndStockType(LocalDate mpsDate, String stockType);

    List findDataByMpsDateAndStockType(LocalDate mpsDate, String stockType, Integer pageNo, Integer pageSize);

    //    Mes New Api with optimization
    HashMap<Long, RscIotRmStockDTO> findAllByStockDateAndStockType(LocalDate stockDate, String stockType);
}