package com.example.validation.service;

import com.example.validation.dto.*;
import com.example.validation.specification.RscMtPPDetailsCriteria;

import java.util.List;

public interface RscMtPpdetailsService {

    MpsPlanMainDTO getProductionPlan(Long ppHeaderId);

    MpsPlanMainDTO getLatestProductionPlan();

    List<RscMtPPdetailsDTO> findAllPPdetailsByPPheader(Long ppheaderId);

    List<RscMtPPdetailsDTO> findAllPPdetailsByPPheaderAndNoCoPackingItem(Long ppheaderId);

    ItemCodeDTO getFGItemCodeDTO(Long ppHeaderId);

    ItemDetailDTO getMPSPlanByFG(Long ppHeaderId, Long fgItemId);

    RscMtPPdetailsDTO findOneRscMtPPdetailsDTO(Long ppHeaderId, Long fgItemId);

    List<ItemDetailDTO> getItemDetailDTOList(Long ppheaderId);

    ExcelExportFiles mpsPlanToExcel(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, MpsPlanMainDTO mpsPlanMainDTO);

    MpsPlanMainDTO getMPSPlanMainDTO(RscMtPPheaderDTO rscMtPpHeaderDTO);

    List<Long> findUniqueRscDtLines(Long ppHeaderId);

    List<Long> findUniqueRscDtSkids(Long ppHeaderId);

    List<RscMtPPdetailsDTO> findAllRscMtPPdetailsDTO(Long ppHeaderId, List<Long> fgItemIds);

    List<RscMtPPdetailsDTO> findAllByLinesIdAndRscMtPPheaderId(Long rscDtLinesId, Long ppHeaderId);

    List<RscMtPPdetailsDTO> findAllBySkidsIdAndRscMtPPheaderId(Long rscDtSkidsId, Long ppHeaderId);

    MonthValuesDTO getMPSPlanSummaryByDivision(Long ppHeaderId, Long divisionId, Long signatureId, Long brandId);

    MonthValuesDTO getMPSPlanSummaryByCategory(Long ppHeaderId, Long categoryId, Long subCategoryId);

    MonthValuesDTO getMPSPlanSummaryByLinesAndSkids(Long ppHeaderId, Long rscDtLinesId, Long rscDtSkidsId);

    MpsPlanSummaryDTO getPPdetailsListForMpsPlanSummary(Long ppHeaderId);

    List<RscMtPPdetailsDTO> findAllByCriteria(RscMtPPDetailsCriteria criteria);

    List<ItemDetailDTO> getCodeMPSPlanByFG(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}
