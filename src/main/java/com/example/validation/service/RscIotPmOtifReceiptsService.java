package com.example.validation.service;

import com.example.validation.dto.RscIotPmOtifReceiptsDTO;

import java.util.List;

public interface RscIotPmOtifReceiptsService {
    List<RscIotPmOtifReceiptsDTO> getAllRscIotPmOtifReceipts();

    List<RscIotPmOtifReceiptsDTO> getRscIotPmOtifReceiptsByItemAndReceiptHeaderId(Long rscMtItemId, Long latestReceiptHeaderId);

    RscIotPmOtifReceiptsDTO getRscIotPmOtifReceiptsByItemAndReceiptHeaderIdAndSupplierId(Long rscMtItemId, Long latestReceiptHeaderId, Long supplierId);

    void deleteAll(List<Long> ids);
}