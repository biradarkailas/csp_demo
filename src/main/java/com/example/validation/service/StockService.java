package com.example.validation.service;

import com.example.validation.domain.RscMtPmMes;
import com.example.validation.domain.RscMtPmRtd;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.PMStockDTO;
import com.example.validation.dto.PmStockDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface StockService {
    PMStockDTO getPmStockByMpsID(Long mpsId, String stockType);

    PmStockDetailDTO getPmStockDetailByMpsID(String mpsId, Long rscMtItemId);

    ExcelExportFiles exportMesStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportMesStockRejection(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportRtdStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<RscMtPmRtd> pmRtdByRscMtItemId(Long rscMtItemId);

    List<RscMtPmMes> pmMesByRscMtItemId(Long rscMtItemId);

    PMStockDTO getPmStockWithMes(Long mpsId);

    PMStockDTO getPmStockWithRtd(Long mpsId);

//    with-named-query
    List getPmStockWithMesWithNamedQuery(Long ppHeaderId);

    List getPmStockWithMesRejectionWithNamedQuery(Long ppHeaderId);

    List getPmStockWithRtdWithNamedQuery(Long ppHeaderId);

}
