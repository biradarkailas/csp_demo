package com.example.validation.service;

import com.example.validation.dto.*;
import com.example.validation.dto.supply.PurchasePlanDTO;

import java.util.List;

public interface RscIotLatestMouldSaturationService {
    List<LatestPurchasePlanDTO> findAll();

    void saveMouldSaturation(Long ppHeaderId);

    /*
        List<RscIotPMLatestPurchasePlan> rscIotPMLatestPurchasePlanListByPpHeaderId(Long ppHeaderId);
    */
    List<RscIotLatestMouldSaturationDTO> getMouldSaturationListByPpHeaderId(Long ppHeaderId);

    void saveAll(List<LatestPurchasePlanDTO> latestPurchasePlanDTOList);

    List<SupplierWiseLatestMouldSaturationDTO> getSupplierWiseMouldSaturation(Long ppHeaderId);

    ExcelExportFiles getSupplierWiseMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    List<RscDtSupplierDTO> getSuppliersListOfMouldSaturation(Long ppHeaderId);

    List<RscIotLatestMouldSaturationDTO> getMouldSaturationListBySupplier(Long ppHeaderId, Long supplierId);

    void deleteAllByPpHeaderId(Long ppHeaderId);

    void updateMouldSaturationForUpdatedPurchasePlan(List<PurchasePlanDTO> purchasePlanDTOList, Long ppHeaderId);

    List<RscIotLatestMouldSaturationDTO> getMouldSaturationListByPpHeaderIdAndMouldsList(Long ppHeaderId, List<Long> uniqueMouldsIdList);

}
