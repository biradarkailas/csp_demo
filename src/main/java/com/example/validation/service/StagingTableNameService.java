package com.example.validation.service;

import com.example.validation.dto.StagingTableDetailsDTO;

import java.util.List;

public interface StagingTableNameService {

    List<StagingTableDetailsDTO> findAll();

    List<StagingTableDetailsDTO> findAllByTableNamesAndValidationSequenceAsc(List<String> tableNames);

    List<StagingTableDetailsDTO> findAllByTableNamesAndResourceSequenceAsc(List<String> tableNames);

    List<String> findAllTableNameByExecutionSequence(Integer executionSequence);

    StagingTableDetailsDTO findOneByTableName(String tableName);
}
