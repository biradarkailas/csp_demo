package com.example.validation.service;

import com.example.validation.dto.RscDtItemTypeDTO;

import java.util.List;

public interface RscDtItemTypeService {
    List<RscDtItemTypeDTO> findAllWithPagination(Integer pageNo, Integer pageSize);

}
