package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;

public interface ExcelExportService {
    ExcelExportFiles downloadfgExport(Long ppHeaderId, String fileName);

    ExcelExportFiles downloadPmExport(Long ppHeaderId, String fileName);

    ExcelExportFiles downloadRmExport(Long ppHeaderId, String fileName);

    ExcelExportFiles downloadPmOtifExport(Long ppHeaderId, String fileName);

    ExcelExportFiles downloadFgAnnualExport( String fileName);

    ExcelExportFiles downloadPmAnnualExport( String fileName);

    ExcelExportFiles downloadRmAnnualExport(String fileName);
}
