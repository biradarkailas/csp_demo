package com.example.validation.service;

import com.example.validation.dto.supply.RscMtRmMesRejectedStockDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtRmMesRejectedStockService {
    List<RscMtRmMesRejectedStockDTO> findAll();

    void deleteAll(LocalDate stockDate);
}
