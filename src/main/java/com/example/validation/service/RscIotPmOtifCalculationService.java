package com.example.validation.service;

import com.example.validation.dto.RscIotPmOtifCalculationDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscIotPmOtifCalculationService {

    void calculateRscIotPmOtifCalculation(String otifDate);

    void saveAll(List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList);

    List<RscIotPmOtifCalculationDTO> findAllByIdAndReceiptOtifDate(Long rscMtPPHeaderId, LocalDate otifDate);

    List<RscIotPmOtifCalculationDTO> findAllBySupplierIdAndRscMtPPheaderId(Long supplierId, Long rscMtPPHeaderId);

    List<RscIotPmOtifCalculationDTO> findAllBySupplierIdAndRscMtPPheaderIdAndOtifReceiptDate(Long supplierId, Long rscMtPPHeaderId, LocalDate otifDate);

    Long getPpheaderIdByDate(String otifDate);

    void deleteAll(Long ppHeaderId);

    List<LocalDate> getPMOtifDateListOfCurrentMonth(String mpsName, String mpsDate);

    List<RscIotPmOtifCalculationDTO> findAllBySupplierIdAndOtifReceiptDate(Long supplierId, LocalDate otifDate);


}
