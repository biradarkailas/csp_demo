package com.example.validation.service;

import com.example.validation.dto.RscIotLatestTotalMouldSaturationDTO;
import com.example.validation.dto.RscIotOriginalTotalMouldSaturationDTO;

import java.util.List;

public interface RscIotOriginalTotalMouldSaturationService {
    String saveOriginalTotalMouldSaturation(Long ppheaderId);

    void save(RscIotOriginalTotalMouldSaturationDTO rscIotOriginalTotalMouldSaturationDTO);

    List<RscIotOriginalTotalMouldSaturationDTO> findAll();

    void saveAllOriginalTotalMouldSaturation(List<RscIotLatestTotalMouldSaturationDTO> latestTotalMouldSaturationDTOList);

    void deleteAll(Long ppHeaderId);
}
