package com.example.validation.service;

import com.example.validation.dto.ErrorLogResponseDTO;
import com.example.validation.dto.ExecutionResponseMainDTO;

import java.util.List;

public interface ErrorLogService {

  List<ErrorLogResponseDTO> findAll();

  void deleteAll();

  boolean dataSanitization(String tableName);

  boolean exportErrorLogData(String filePath);

  void truncateTable(String tableName);

  List<ErrorLogResponseDTO> findAllErrorLogResponseDTO();

  void createExecutionResponseFile(ExecutionResponseMainDTO executionResponseMainDTO);
}
