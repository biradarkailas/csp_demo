package com.example.validation.service;

import com.example.validation.dto.RscMtPmOtifReceiptsDTO;

import java.util.List;

public interface RscMtPmOtifReceiptsService {
    List<RscMtPmOtifReceiptsDTO> findAll();

    void deleteAll(List<Long> ids);
}
