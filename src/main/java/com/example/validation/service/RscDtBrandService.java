package com.example.validation.service;

import com.example.validation.dto.RscDtBrandDTO;

import java.util.List;

public interface RscDtBrandService {
    List<RscDtBrandDTO> findAll();

}
