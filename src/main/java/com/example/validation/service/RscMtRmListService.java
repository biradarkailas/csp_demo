package com.example.validation.service;

import com.example.validation.dto.RscMtRmListDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtRmListService {
    List<RscMtRmListDTO> findAllByLotNumberAndRscMtItemId(String lotNumber, Long rscMtItemId);

    void deleteAll(LocalDate mpsDate);
}
