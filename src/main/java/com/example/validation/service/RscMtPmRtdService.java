package com.example.validation.service;

import com.example.validation.dto.IndividualValuesRtdDTO;
import com.example.validation.dto.RscMtPmRtdDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtPmRtdService {
    List<RscMtPmRtdDTO> findAll();

    List<RscMtPmRtdDTO> findByReceiptDate(LocalDate receiptDate);

    List<IndividualValuesRtdDTO> findAllIndividualValueDTOByRscMtItemId(Long rscMtItemId);

    void deleteAll(LocalDate stockDate);
}
