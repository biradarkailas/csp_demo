package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotPmOtifSummaryService {
    void calculateRscIotPmOtifSummary(String otifDate);

    void saveAll(List<RscIotPmOtifSummaryDTO> rscIotPmOtifCalculationDTOList);

    OtifCalculationDetailsDTO getAllRscItPmOtifCalculation(Long rscMtPPHeaderId);

    OtifCalculationSuppliersDetailsDTO getPmOtifCalculationSuppliers(Long ppHeaderId);

    List<RscIotPmOtifSummaryDTO> getPmOtifCalculationSuppliersList(Long ppHeaderId);

    PmOtifCalculationSupplierWiseListDTO getSingleSuppliersDetails(Long ppHeaderId, Long rscDtSupplierId);

    List<RscDtSupplierDTO> getAllUniqueSuppliersList();

    List<RscIotPmOtifSummaryDTO> getSuppliersList(Long rscDtSupplierId);

    ExcelExportFiles pmOtifSummaryExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void deleteAll(Long ppHeaderId);
}
