package com.example.validation.service;

import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.RmPurchasePlanCalculationDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanDTO;

import java.util.List;

public interface RscIotRMPurchasePlanCalculationsService {
    Boolean createRMPurchasePlanCalculations(Long ppHeaderId);

    List<RscIotRMPurchasePlanDTO> findAllByPPHeaderId(Long ppHeaderId);

    RmPurchasePlanCalculationDTO getRMPurchasePlanCalculations(Long rscMtItemId, Long ppHeaderId, Long supplierDetailsId);

    List<PurchasePlanCalculationsMainDTO> findAll(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);

}