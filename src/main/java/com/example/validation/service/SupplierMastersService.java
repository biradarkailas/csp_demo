package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface SupplierMastersService {
    List<RscDtSupplierDTO> getSuppliersListOfPackingMaterial();

    List<RscDtSupplierDTO> getSuppliersListOfRawMaterial();

    ExcelExportFiles pmSuppliersMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles rmSuppliersMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<RscDtSupplierDTO> findSuppliersListOfPackingMaterialWithPagination(Integer pageNo, Integer pageSize);

    List<RscDtSupplierDTO> findSuppliersListOfRawMaterialWithPagination(Integer pageNo, Integer pageSize);
}

