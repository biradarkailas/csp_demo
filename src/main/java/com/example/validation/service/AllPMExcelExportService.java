package com.example.validation.service;

public interface AllPMExcelExportService {
        String createAllPMExports(Long ppHeaderId);

        void createDirectory(String year, String month, String date, String name);
    }
