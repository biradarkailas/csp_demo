package com.example.validation.service;

import com.example.validation.dto.NodeDTO;
import com.example.validation.dto.RscMtBomDTO;

import java.util.List;

public interface RscMtBomService {
    List<RscMtBomDTO> findAllByRscMtItemParentId(Long parentItemId, Long ppHeaderId);

    List<RscMtBomDTO> findAllByParentItemIdAndChildItemCategoryDescription(Long parentItemId, String itemDescription, Long ppHeaderId);

    NodeDTO getFGRelation(Long fgItemId, String itemType, Long ppHeaderId);

    List<Long> findFGItemIdByRscMtItemChildId(Long parentItemId, Long ppHeaderId);

    List<RscMtBomDTO> findAll(Long ppHeaderId);

    List<RscMtBomDTO> findAllByRscMtItemChildId(Long childItemId, Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}