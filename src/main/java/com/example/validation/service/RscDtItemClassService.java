package com.example.validation.service;

import com.example.validation.dto.RscDtItemClassDTO;

import java.util.List;

public interface RscDtItemClassService {
    List<RscDtItemClassDTO> findAll();
}
