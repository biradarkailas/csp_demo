package com.example.validation.service;

import com.example.validation.dto.RscDtSailingDaysDTO;

import java.util.List;

public interface RscDtSailingDaysService {
    List<RscDtSailingDaysDTO> findAll();
}
