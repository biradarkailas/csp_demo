package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotRmCoverDaysService {
    void calculateRscIotRmCoverDays(Long rscMtPPHeaderId);

    void calculateRscIotRmCoverDaysSummary(Long ppHeaderId);

    RmCoverDaysSummaryDTO getRmCoverDaysSummaryDetails(Long ppHeaderId);

    RmCoverDaysDetailsDTO getRmCoverDaysDetails(Long ppHeaderId);

    CoverDaysSummaryDetailsDTO getRmCoverDaysSummaryAllDetails(Long ppHeaderId);

    ExcelExportFiles exportRmCoverDaysExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    RmCoverDaysDetailsDTO getNullStdPriceDetails(Long rscMtPPHeaderId);

    List<RmCoverDaysDTO> saveAllRmCoverDaysDetails(List<RmCoverDaysDTO> pmCoverDaysDTO);

//    ExcelExportFiles rmCoverDaysSummaryExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void deleteById(Long ppHeaderId);

     List<RscIotRmCoverDaysDTO> getRscItRmCoverDaysList(Long rscMtPPHeaderId);
//    ClassWiseCoverDaysDetailsDTO getRmCoverDaysClassWiseSummaryDetails(Long ppHeaderId);
}
