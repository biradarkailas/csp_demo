package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.SlobTotalConsumptionDetailsDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;

import java.util.List;

public interface RscItPMGrossConsumptionService {
    GrossConsumptionMainDTO getAllPMGrossConsumption(Long ppHeaderId);

    void saveAll(List<RscItGrossConsumptionDTO> pmRscItGrossConsumptionDTOs, Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllPMGrossConsumptionByRscMtPPheaderId(Long ppHeaderId);

    RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId);

    ExcelExportFiles getPmGrossConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    SlobTotalConsumptionDetailsDTO getPmSlobTotalConsumptionDetailsDTO(Long ppHeaderId, Long rscMtItemId);

    Long count(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}