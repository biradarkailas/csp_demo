package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.SlobInventoryQualityIndexDetailsDTO;
import com.example.validation.dto.SlobMovementDetailsDTO;

public interface FgSlobMovementAndInventoryIndexService {
    SlobMovementDetailsDTO getFgSlobMovement();

    ExcelExportFiles exportFgSlobMovementExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportFgSlobInventoryQualityIndexExporter(RscMtPPheaderDTO rscMtPPheaderDTO);

    SlobInventoryQualityIndexDetailsDTO getFgSlobInventoryQualityIndex();

}
