package com.example.validation.service;

import com.example.validation.dto.IndividualValuesMesDTO;
import com.example.validation.dto.RscMtPmMesDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtPmMesService {
    List<RscMtPmMesDTO> findAll();

    List<RscMtPmMesDTO> findAllByStockDate(LocalDate mpsDate);

    List<IndividualValuesMesDTO> findAllIndividualValueDTOByRscMtItemId(Long rscMtItemId);

    void deleteAll(LocalDate stockDate);
}
