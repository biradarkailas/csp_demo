package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotRmCoverDaysClassSummaryService {
//    void calculateRmCoverDaysAllClassSummary(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList, Long rscMtPPHeaderId);

    ClassWiseCoverDaysDetailsDTO getAllClassConsumptionByPpHeaderId(Long rscMtPPHeaderId);

    void deleteClassSummaryById(Long rscMtPPHeaderId);

    void calculateRmCoverDaysAllClassSummary( Long rscMtPPHeaderId);

    ClassWiseCoverDaysDetailsDTO getRmCoverDaysClassWiseSummaryDetails(Long ppHeaderId);

    ExcelExportFiles rmCoverDaysSummaryExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);
}
