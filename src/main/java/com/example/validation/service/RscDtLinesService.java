package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtLinesDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface RscDtLinesService {
    List<RscDtLinesDTO> findAll();

    RscDtLinesDTO findById(Long rscDtLinesId);

    List<RscDtLinesDTO> findFgLinesWithPagination(Integer pageNo, Integer pageSize);

    ExcelExportFiles fgMasterDatalineExport(RscMtPPheaderDTO rscMtPPheaderDTO);
}
