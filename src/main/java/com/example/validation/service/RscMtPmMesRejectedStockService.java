package com.example.validation.service;

import com.example.validation.dto.RscMtPmMesRejectedStockDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtPmMesRejectedStockService {
    List<RscMtPmMesRejectedStockDTO> findAll();

    void deleteAll(LocalDate stockDate);
}
