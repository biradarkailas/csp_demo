package com.example.validation.service;

import com.example.validation.dto.RscDtReasonTypeDTO;

import java.util.List;

public interface RscDtReasonTypeService {
    List<RscDtReasonTypeDTO> findAll();
}
