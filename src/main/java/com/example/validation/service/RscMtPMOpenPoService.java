package com.example.validation.service;

import com.example.validation.dto.RscMtPMOpenPoDTO;

import java.util.List;

public interface RscMtPMOpenPoService {
    List<RscMtPMOpenPoDTO> findAllByRscMtItemIdAndMtSupplierId(Long mpsId, Long rscMtItemId, Long rscMtSupplierId);

    void deleteAll(Long ppHeaderId);
}