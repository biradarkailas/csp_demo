package com.example.validation.service;

import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.dto.RscItItemWiseSupplierDTO;

import java.util.List;

public interface RscItItemWiseSupplierService {
    List<RscItItemWiseSupplierDTO> findByRscMtItemId(Long rscMtItemId, Long ppHeaderId);

    RscItItemWiseSupplierDTO findOneByRscMtItemIdAndAllocation(Long rscMtItemId);

    List<RscDtSupplierDTO> getSuppliersListByCategory(List<String> category);

}