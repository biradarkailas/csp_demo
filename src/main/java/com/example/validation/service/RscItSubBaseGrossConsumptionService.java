package com.example.validation.service;

import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;

import java.util.List;

public interface RscItSubBaseGrossConsumptionService {
    void saveAll(List<RscItGrossConsumptionDTO> subBaseRscItGrossConsumptionDTOs, Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId);

    RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId);

    Long count(Long ppHeaderId);

    List<Long> findAllItemIdByRscMtPPHeaderId(Long ppHeaderId);

    List<GrossConsumptionDTO> getAllSubBaseGrossConsumption(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}