package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.ItemCodeDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;

import java.util.List;

public interface RscItBulkGrossConsumptionService {
    void saveAll(List<RscItGrossConsumptionDTO> bulkRscItGrossConsumptionDTOs, Long ppHeaderId);

    GrossConsumptionMainDTO getAllBulkGrossConsumption(Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

    ItemCodeDTO getBulkItemCodeDTO(Long ppHeaderId);

    RscItGrossConsumptionDTO findByRscMtPPheaderIdAndBulkItemId(Long ppHeaderId, Long bulkItemId);

    List<RscItGrossConsumptionDTO> findAllGrossConsumptionDTOByRscMtPPheaderId(Long ppHeaderId);

    ExcelExportFiles exportBulkGross(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    Long count(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}