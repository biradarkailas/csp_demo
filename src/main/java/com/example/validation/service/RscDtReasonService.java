package com.example.validation.service;

import com.example.validation.dto.RscDtReasonDTO;

import java.util.List;

public interface RscDtReasonService {
    List<RscDtReasonDTO> findAll();

    List<RscDtReasonDTO> save(List<RscDtReasonDTO> rscDtReasonDTOs);
}
