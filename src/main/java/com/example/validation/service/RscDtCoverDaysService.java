package com.example.validation.service;

import com.example.validation.dto.RscDtCoverDaysDTO;

import java.util.List;

public interface RscDtCoverDaysService {
    List<RscDtCoverDaysDTO> findAll();
}
