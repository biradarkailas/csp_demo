package com.example.validation.service;

public interface AllExcelExportService {
    String createAllMpsFgExports(Long ppHeaderId);

    void createDirectory(String year, String month, String date, String name);
}
