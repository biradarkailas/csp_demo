package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RscIotPmCoverDaysService {
    void calculateRscIotPmCoverDays(Long rscMtPPHeaderId);

    void saveAll(List<RscIotPmCoverDaysDTO> RscIotPmCoverDaysDtoList);

    void calculateRscIotPmCoverDaysSummary(Long ppHeaderId);

    PmCoverDaysSummaryDTO getPmCoverDaysSummaryDetails(Long ppHeaderId);

    PmCoverDaysDetailsDTO getPmCoverDaysDetails(Long ppHeaderId);

    CoverDaysSummaryDetailsDTO getPmCoverDaysSummaryAllDetails(Long ppHeaderId);

    List<RscIotPmCoverDaysSummaryDTO> findAllByCoverDaysSummaryDTOppHeaderId(Long ppHeaderId);

    ExcelExportFiles exportPmSlobExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    PmCoverDaysDetailsDTO getNullStdPriceDetails(Long rscMtPPHeaderId);

    List<PmCoverDaysDTO> saveAllPmCoverDaysDetails(List<PmCoverDaysDTO> pmCoverDaysDTO);

    void deleteById(Long ppHeaderId);

}
