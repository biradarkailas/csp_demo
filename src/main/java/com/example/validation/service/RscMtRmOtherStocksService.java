package com.example.validation.service;

import com.example.validation.dto.IndividualStockTypeValueDTO;
import com.example.validation.dto.RscMtRmOtherStocksDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtRmOtherStocksService {
    List<RscMtRmOtherStocksDTO> findAllByRscMtItemId(Long rscMtItemId);

    List<IndividualStockTypeValueDTO> findAllByRscDtStockTypeIdAndRscMtItemId(Long stockTypeId, Long rscMtItemId);

    void deleteAll(LocalDate stockDate);
}