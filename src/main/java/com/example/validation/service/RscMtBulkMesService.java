package com.example.validation.service;

import com.example.validation.dto.RscMtBulkMesDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtBulkMesService {
    List<RscMtBulkMesDTO> findAll();

    void deleteAll(LocalDate stockDate);
}
