package com.example.validation.service;

import com.example.validation.dto.PmSlobDTO;
import com.example.validation.dto.RmSlobDTO;

import java.util.List;

public interface RecalculationAndExcelCreationOnUpdateService {
    List<RmSlobDTO> saveAllRmSlobs(List<RmSlobDTO> rmSlobDTO, Double sitValue, Long ppHeaderId);

    List<RmSlobDTO> saveAllRmSlobDetails(List<RmSlobDTO> rmSlobDTO, Double sitValue);

    List<PmSlobDTO> saveAllPmSlobs(List<PmSlobDTO> PmSlobDTO, Double sitValue, Long ppHeaderId);

    List<PmSlobDTO> saveAllPmSlobDetails(List<PmSlobDTO> PmSlobDTO, Double sitValue);

    String createRmExcelExportOnUpdate(Long ppHeaderId);

    String createPmExcelExportOnUpdate(Long ppHeaderId);
}
