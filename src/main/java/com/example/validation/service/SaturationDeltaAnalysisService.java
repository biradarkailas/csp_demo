package com.example.validation.service;

import com.example.validation.dto.FgSaturationDeltaAnalysisDTO;

public interface SaturationDeltaAnalysisService {
    FgSaturationDeltaAnalysisDTO getFgLineSaturationDeltaAnalysis(Long rscDtLinesId);

    FgSaturationDeltaAnalysisDTO getFgSkidSaturationDeltaAnalysis(Long rscDtSkidsId);
}
