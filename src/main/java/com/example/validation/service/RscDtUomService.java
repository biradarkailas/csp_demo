package com.example.validation.service;

import com.example.validation.dto.RscDtUomDTO;

import java.util.List;

public interface RscDtUomService {
    List<RscDtUomDTO> findAll();
}
