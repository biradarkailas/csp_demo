package com.example.validation.service;

import com.example.validation.domain.RscDtStockType;
import com.example.validation.dto.RscDtStockTypeDTO;

import java.util.List;

public interface RscDtStockTypeService {
    List<RscDtStockTypeDTO> findAll();

    RscDtStockType findOneByTag(String aliasName);
}
