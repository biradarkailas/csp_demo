package com.example.validation.service;

import com.example.validation.dto.MaterialMasterDTO;
import com.example.validation.dto.RscDtItemTypeDTO;
import com.example.validation.dto.RscMtItemDTO;

import java.util.List;

public interface RscMtItemService {
    RscMtItemDTO findById(Long id);

    List<MaterialMasterDTO> getMaterialMastersListByCategory(String materialCategory);

    List<RscDtItemTypeDTO> getMaterialCategoryMastersListByCategory(String materialCategory);
}