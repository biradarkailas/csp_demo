package com.example.validation.service;

import com.example.validation.dto.ResourceTableUploadSummaryDTO;

import java.util.List;

public interface ResourceTableUploadSummaryService {
    List<ResourceTableUploadSummaryDTO> findAll();

    void deleteAll();
}