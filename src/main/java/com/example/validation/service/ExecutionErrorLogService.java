package com.example.validation.service;

public interface ExecutionErrorLogService {
    void saveExecutionError(String procedureName, String errorDescription, Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}