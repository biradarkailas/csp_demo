package com.example.validation.service;

import com.example.validation.dto.RscDtSignatureDTO;

import java.util.List;

public interface RscDtSignatureService {
    List<RscDtSignatureDTO> findAll();

}
