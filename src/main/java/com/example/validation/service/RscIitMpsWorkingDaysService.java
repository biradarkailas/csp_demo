package com.example.validation.service;

import com.example.validation.dto.RscIitMpsWorkingDaysDTO;

public interface RscIitMpsWorkingDaysService {
    RscIitMpsWorkingDaysDTO findByMpsId(Long mpsId);

    void deleteAll(Long ppHeaderId);
}