package com.example.validation.service;

import com.example.validation.dto.RscMtRmOpenPoDTO;

import java.util.List;

public interface RscMtRmOpenPoService {
    List<RscMtRmOpenPoDTO> findAllByRscMtItemIdAndMtSupplierId(Long mpsId, Long rscMtItemId, Long rscMtSupplierId);

    void deleteAll(Long ppHeaderId);
}
