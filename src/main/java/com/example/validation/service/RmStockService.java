package com.example.validation.service;

import com.example.validation.dto.*;

import java.util.List;

public interface RmStockService {

    RMStockDTO getAllRMStocksDTO(Long ppHeaderId, String stockType);

    RscStockDetailDTO getAllRMStocksDetailData(Long ppHeaderId, Long RscMtItemId);

    RMStockDTO getRmStockWithMes(Long ppHeaderId);

    RMStockDTO getRmStockWithRtd(Long ppHeaderId);

    RMStockDTO getRmStockWithBulkOpRm(Long ppHeaderId);

    RMStockDTO getRmStockWithTransferStock(Long ppHeaderId);

    RMStockDTO getRmStockWithRejection(Long ppHeaderId);

    ExcelExportFiles exportRMStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportRtdStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportTransferStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportRejectionStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    //   Named Native Query
    List getRmStockWithMesWithNamedQuery(Long ppHeaderId);

    List getRmStockWithMesRejectionWithNamedQuery(Long ppHeaderId);

    List getRmStockWithRtdWithNamedQuery(Long ppHeaderId);

    List getRmStockWithBulkOpRmWithNamedQuery(Long ppHeaderId);

    List getRmStockWithTransferStockWithNamedQuery(Long ppHeaderId);

    List getRmStockWithRejectionWithNamedQuery(Long ppHeaderId);

    ExcelExportFiles exportRmMesStockRejection(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles exportBulkOpRmStock(RscMtPPheaderDTO rscMtPPheaderDTO);

    //    Mes New Api with optimization
    List<RmMesStockPaginationDto> getStockWithMes(Long ppHeaderId);
}
