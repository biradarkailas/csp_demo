package com.example.validation.service;

import com.example.validation.dto.MonthNameDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public interface RscMtPpheaderService {

    List<RscMtPPheaderDTO> findAll();

    List<RscMtPPheaderDTO> findAllPPheaderByActive();

    RscMtPPheaderDTO findLastPPheader();

    Optional<RscMtPPheaderDTO> findOne(Long id);

    Optional<RscMtPPheaderDTO> findByMpsDate(LocalDate mpsDate);

    RscMtPPheaderDTO findOneByMpsNameMpsDate(String mpsName, LocalDate mpsDate);

    List<RscMtPPheaderDTO> saveAll(List<RscMtPPheaderDTO> rscMtPPheaderDTOS);

    Long getPreviousMonthMpsId(Long currentMonthId);

    List<RscMtPPheaderDTO> findAllPPheaderListSortedBasedOnMpsDate();

    List<RscMtPPheaderDTO> findAllPPheaderListSortedBasedOnMpsDateandMpsValidated();

    Long getNextMonthMpsId(Long currentMonthMpsId);

    Boolean isMpsValidated(Long mpsId);

    Optional<RscMtPPheaderDTO> findValidatedMpsPlanByOtifDate(LocalDate otifDate);

    List<RscMtPPheaderDTO> findAllByActiveAndIsValidated();

    RscMtPPheaderDTO savePPheader(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<RscMtPPheaderDTO> findAllByCurrentDate(LocalDate currentDate);

    RscMtPPheaderDTO updateRscMtPPHeaderDTO(String mpsName, LocalDate mpsDate);

    Boolean checkRequiredFilesUploadStatus();

    List<RscMtPPheaderDTO> findAllCurrentMonthsPPHeaderByActive();

    List<RscMtPPheaderDTO> findAllPPheader();

    Set<String> getAllMonthYear();

    Set<MonthNameDTO> getAllMonthYearList();

    void deleteById(Long ppHeaderId);

    List<RscMtPPheaderDTO> getBeforeOneYearPlans(LocalDate mpsDate);

    List<RscMtPPheaderDTO> findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();

    List<RscMtPPheaderDTO> findAllPreviousMonthsPPHeaderByActive();

    List<RscMtPPheaderDTO> updateMultipleValidatedPlansToPseudo(String mpsName, LocalDate mpsDate, String tagName);

    Long getPreviousMonthsFirstCutMpsId(Long currentMonthId);

    List<RscMtPPheaderDTO> getAllMpsPlansWithBackupStatusFalse();

    RscMtPPheaderDTO updateMpsDetailsPlanManagement(Boolean tagChangeStatus, RscMtPPheaderDTO rscMtPPheaderDTO);

    Boolean mpsPlanNameExistsForPlanManagement(String name, Long id);

    RscMtPPheaderDTO getPPheaderWithLatestBackupDate();

    void updateIsBackupDone();

    RscMtPPheaderDTO getValidatedPlan();
}