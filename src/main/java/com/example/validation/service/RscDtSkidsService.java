package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtSkidsDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface RscDtSkidsService {
    List<RscDtSkidsDTO> findAll();

    List<RscDtSkidsDTO> findFgSkidsWithPagination(Integer pageNo, Integer pageSize);

    RscDtSkidsDTO findById(Long rscDtLinesId);

    ExcelExportFiles fgMasterDataSkidExport(RscMtPPheaderDTO rscMtPPheaderDTO);
}
