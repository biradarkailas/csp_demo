package com.example.validation.service;

public interface MPSTriangleAnalysisExcelExportService {
    String createAllMPSTriangleAnalysisExcelExports(Long ppHeaderId);

    void createDirectory(String year, String month, String date, String name);
}
