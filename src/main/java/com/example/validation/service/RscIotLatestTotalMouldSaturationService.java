package com.example.validation.service;

import com.example.validation.dto.*;
import com.example.validation.dto.supply.PurchasePlanDTO;

import java.util.List;

public interface RscIotLatestTotalMouldSaturationService {

    ExcelExportFiles mouldWiseSaturationToExcel(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void saveLatestTotalMouldSaturation(Long ppheaderId);

    List<RscIotLatestTotalMouldSaturationDTO> findAll();

    List<RscIotLatestTotalMouldSaturationDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

//    Long count(Long ppHeaderId);

    List<MouldSaturationDetailsDTO> getMouldSaturationWithLowStatusForCurrentMonth(Long ppHeaderId);

    List<MouldSaturationDetailsDTO> getMouldSaturationWithMediumStatusForCurrentMonth(Long ppHeaderId);

    List<MouldSaturationDetailsDTO> getMouldSaturationWithHighStatusForCurrentMonth(Long ppHeaderId);

    void deleteAllByPpHeaderId(Long ppHeaderId);

    void updateTotalMouldSaturationForUpdatedPurchasePlan(List<PurchasePlanDTO> purchasePlanDTOList, Long ppHeaderId);

    List<Long> getAllUniqueMouldsList(List<PurchasePlanDTO> purchasePlanDTOList);

    List<RscIotLatestTotalMouldSaturationDTO> findAllByRscMtPpHeaderIdAndMouldsList(Long ppHeaderId, List<Long> uniqueMouldsIdList);
}
