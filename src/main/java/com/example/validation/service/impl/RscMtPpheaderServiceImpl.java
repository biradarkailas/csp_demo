package com.example.validation.service.impl;

import com.example.validation.domain.RscMtPPheader;
import com.example.validation.dto.MonthNameDTO;
import com.example.validation.dto.RscDtTagDTO;
import com.example.validation.dto.RscIitIdpStatusDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.RscMtPPheaderMapper;
import com.example.validation.repository.RscMtPpheaderRepository;
import com.example.validation.service.RscDtTagService;
import com.example.validation.service.RscIitIdpStatusService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.DatabaseUtil;
import com.example.validation.util.DateUtils;
import com.example.validation.util.enums.ModuleName;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscMtPpheaderServiceImpl implements RscMtPpheaderService {
    private final RscMtPpheaderRepository rscMtPpheaderRepository;
    private final RscMtPPheaderMapper rscMtPPheaderMapper;
    private final RscIitIdpStatusService rscIitIdpStatusService;
    private final RscDtTagService rscDtTagService;

    public RscMtPpheaderServiceImpl(RscMtPpheaderRepository rscMtPpheaderRepository, RscMtPPheaderMapper rscMtPPheaderMapper, RscIitIdpStatusService rscIitIdpStatusService, RscDtTagService rscDtTagService) {
        this.rscMtPpheaderRepository = rscMtPpheaderRepository;
        this.rscMtPPheaderMapper = rscMtPPheaderMapper;
        this.rscIitIdpStatusService = rscIitIdpStatusService;
        this.rscDtTagService = rscDtTagService;
    }

    @Override
    public List<RscMtPPheaderDTO> findAll() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAll());
    }

    @Override
    public List<RscMtPPheaderDTO> findAllPPheaderByActive() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActive(true));
    }

    @Override
    public RscMtPPheaderDTO findLastPPheader() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findFirstByOrderByIdDesc());
    }

    @Override
    public Optional<RscMtPPheaderDTO> findOne(Long id) {
        return rscMtPpheaderRepository.findById(id).map(rscMtPPheaderMapper::toDto);
    }

    @Override
    public Optional<RscMtPPheaderDTO> findByMpsDate(LocalDate mpsDate) {
        return rscMtPpheaderRepository.findByMpsDate(mpsDate).map(rscMtPPheaderMapper::toDto);
    }

    @Override
    public RscMtPPheaderDTO findOneByMpsNameMpsDate(String mpsName, LocalDate mpsDate) {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findByMpsNameAndMpsDate(mpsName, mpsDate));
    }

    @Override
    public List<RscMtPPheaderDTO> saveAll(List<RscMtPPheaderDTO> rscMtPPheaderDTOs) {
        List<RscMtPPheader> rscMtPPheaderList = rscMtPpheaderRepository.saveAll(rscMtPPheaderMapper.toEntity(rscMtPPheaderDTOs));
        return rscMtPPheaderMapper.toDto(rscMtPPheaderList);
    }

    @Override
    public Long getPreviousMonthMpsId(Long currentMonthMpsId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTOOptional = rscMtPpheaderRepository.findById(currentMonthMpsId).map(rscMtPPheaderMapper::toDto);
        Optional<RscMtPPheaderDTO> optionalRscMtPPheaderDTO = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("VD")).stream().filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getYear() == (rscMtPPheaderDTOOptional.get().getMpsDate().minusMonths(1).getYear())).filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().equals(rscMtPPheaderDTOOptional.get().getMpsDate().minusMonths(1).getMonth())).findFirst();
        if (optionalRscMtPPheaderDTO.isPresent()) return optionalRscMtPPheaderDTO.get().getId();
        return null;
    }

    @Override
    public Long getNextMonthMpsId(Long currentMonthMpsId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTOOptional = rscMtPpheaderRepository.findById(currentMonthMpsId).map(rscMtPPheaderMapper::toDto);
        Optional<RscMtPPheaderDTO> optionalRscMtPPheaderDTO = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("VD")).stream().filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getYear() == (rscMtPPheaderDTOOptional.get().getMpsDate().plusMonths(1).getYear())).filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().equals(rscMtPPheaderDTOOptional.get().getMpsDate().plusMonths(1).getMonth())).findFirst();
        if (optionalRscMtPPheaderDTO.isPresent()) return optionalRscMtPPheaderDTO.get().getId();
        return null;
    }

    @Override
    public List<RscMtPPheaderDTO> findAllPPheaderListSortedBasedOnMpsDate() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAll()).stream().sorted(Comparator.comparing(RscMtPPheaderDTO::getMpsDate)).collect(Collectors.toList());
    }

    @Override
    public Boolean isMpsValidated(Long mpsId) {
        Optional<RscMtPPheader> rscMtPPheader = rscMtPpheaderRepository.findById(mpsId);
        if (rscMtPPheader.isPresent()) {
            return rscMtPPheaderMapper.toDto(rscMtPPheader.get()).getTagName().equals("VD");
        }
        return false;
    }

    @Override
    public List<RscMtPPheaderDTO> findAllPPheaderListSortedBasedOnMpsDateandMpsValidated() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("VD")).stream().sorted(Comparator.comparing(RscMtPPheaderDTO::getMpsDate)).collect(Collectors.toList());
    }

    @Override
    public List<RscMtPPheaderDTO> findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("FC")).stream().sorted(Comparator.comparing(RscMtPPheaderDTO::getMpsDate)).collect(Collectors.toList());
    }

    @Override
    public Optional<RscMtPPheaderDTO> findValidatedMpsPlanByOtifDate(LocalDate otifDate) {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("VD")).stream().
                filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().name().matches(otifDate.getMonth().name())).sorted(Comparator.comparing(RscMtPPheaderDTO::getId).reversed()).findFirst();
    }

    @Override
    public List<RscMtPPheaderDTO> findAllByActiveAndIsValidated() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActiveAndRscDtTagName(true, "VD")).stream().sorted(Comparator.comparing(RscMtPPheaderDTO::getMpsDate)).collect(Collectors.toList());
    }

    @Override
    public RscMtPPheaderDTO savePPheader(RscMtPPheaderDTO rscMtPPheaderDTO) {
        RscMtPPheaderDTO rscMtPPheaderDTOObject = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.save(rscMtPPheaderMapper.toEntity(rscMtPPheaderDTO)));
        if (rscMtPPheaderDTOObject.getIsExecutionDone()) {
            System.out.println("rscMtPPheaderDTOObject" + rscMtPPheaderDTOObject);
            DatabaseUtil.backup();
        }
        return rscMtPPheaderDTOObject;
    }

    @Override
    public List<RscMtPPheaderDTO> findAllByCurrentDate(LocalDate currentDate) {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActive(true)).stream().filter(rscMtPPheaderDTO -> DateUtils.isEqualMonthAndYear(rscMtPPheaderDTO.getMpsDate(), currentDate)).collect(Collectors.toList());
    }

    @Override
    public RscMtPPheaderDTO updateRscMtPPHeaderDTO(String mpsName, LocalDate mpsDate) {
        RscMtPPheader rscMtPPheader = rscMtPpheaderRepository.findByMpsNameAndMpsDate(mpsName, mpsDate);
        if (Optional.ofNullable(rscMtPPheader).isPresent()) {
            rscMtPPheader.setModuleName(ModuleName.ALL);
            return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.save(rscMtPPheader));
        }
        return null;
    }

    @Override
    public Boolean checkRequiredFilesUploadStatus() {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusService.findAllRscIitIdpStatusByActive();
        if (Optional.ofNullable(rscIitIdpStatusDTOs).isPresent() && rscIitIdpStatusDTOs.size() > 0) {
            if (DateUtils.isEqualMonthAndYear(java.time.LocalDate.now(), rscIitIdpStatusDTOs.get(0).getMpsDate())) {
                return getStatusByCheckingRequiredFilesStatus(rscIitIdpStatusDTOs);
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    private Boolean getStatusByCheckingRequiredFilesStatus(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        boolean status = true;
        RscMtPPheaderDTO rscMtPPheaderDTO = findOneByMpsNameMpsDate(rscIitIdpStatusDTOs.get(0).getMpsName(), rscIitIdpStatusDTOs.get(0).getMpsDate());
        for (RscIitIdpStatusDTO rscIitIdpStatus : rscIitIdpStatusDTOs)
            if (rscIitIdpStatus.getIsRequired()) {
                if (!(rscIitIdpStatus.getUploadStatus() && rscIitIdpStatus.getResourceStatus() && rscIitIdpStatus.getValidationStatus())) {
                    status = false;
                    break;
                }
            }
        if ((status == true) && Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            if (Optional.ofNullable(rscMtPPheaderDTO.getIsExecutionDone()).isPresent() && rscMtPPheaderDTO.getIsExecutionDone()) {
                if (rscMtPPheaderDTO.getIsExecutionDone()) {
                    status = false;
                } else {
                    status = true;
                }
            }
        }
        return status;
    }

    @Override
    public List<RscMtPPheaderDTO> findAllCurrentMonthsPPHeaderByActive() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActive(true)).stream().
                filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().name().matches(java.time.LocalDate.now().getMonth().name())).sorted(Comparator.comparing(RscMtPPheaderDTO::getId)).collect(Collectors.toList());
    }

    @Override
    public List<RscMtPPheaderDTO> findAllPPheader() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAll());
    }

    @Override
    public Set<String> getAllMonthYear() {
        List<RscMtPPheaderDTO> rscMtPPHeaderDTOs = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActive(true));
        Set<String> monthYear = new HashSet<>();
        for (RscMtPPheaderDTO rscMtPPheaderDTO : rscMtPPHeaderDTOs) {
            monthYear.add(DateUtils.getCurrentMonthName(rscMtPPheaderDTO.getMpsDate()));
        }
        return monthYear;
    }

    @Override
    public Set<MonthNameDTO> getAllMonthYearList() {
        LinkedHashSet<MonthNameDTO> monthYear = new LinkedHashSet<>();
        List<RscMtPPheaderDTO> rscMtPPHeaderDTOs = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActiveAndIsBackUpDone(true, true));
        rscMtPPHeaderDTOs.sort(Comparator.comparing(RscMtPPheaderDTO::getMpsDate).reversed());
        for (RscMtPPheaderDTO rscMtPPheaderDTO : rscMtPPHeaderDTOs) {
            MonthNameDTO monthNameDTO = new MonthNameDTO();
            monthNameDTO.setMonthName(DateUtils.getCurrentMonthName(rscMtPPheaderDTO.getMpsDate()));
            monthNameDTO.setMonthNameAlias(DateUtils.getCurrentMonthYearName(rscMtPPheaderDTO.getMpsDate()));
            monthYear.add(monthNameDTO);
        }
        return monthYear;
    }

    @Override
    public List<RscMtPPheaderDTO> getBeforeOneYearPlans(LocalDate mpsDate) {
        LocalDate beforeOneYearDate = mpsDate.minusYears(1);
        List<RscMtPPheaderDTO> rscMtPPHeaderDTOs = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAll());
        return rscMtPPHeaderDTOs.stream().filter(rscMtPPHeaderDTO -> rscMtPPHeaderDTO.getMpsDate().getYear() == beforeOneYearDate.getYear()).filter(rscMtPPHeaderDTO -> rscMtPPHeaderDTO.getMpsDate().getMonth().equals(beforeOneYearDate.getMonth())).collect(Collectors.toList());
    }

    @Override
    public void deleteById(Long id) {
        rscMtPpheaderRepository.deleteById(id);
    }

    @Override
    public List<RscMtPPheaderDTO> findAllPreviousMonthsPPHeaderByActive() {
        List<String> tagNames = new ArrayList<>();
        tagNames.add("FC");
        tagNames.add("VD");
        List<RscMtPPheaderDTO> rscMtPPheaderDTOs = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActiveAndIsBackUpDone(true, false)).stream().filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().name().equals(java.time.LocalDate.now().minusMonths(1).getMonth().name())).collect(Collectors.toList());
        //updateIsBackupDone(rscMtPPheaderDTOs);
        return rscMtPPheaderDTOs.stream().filter(rscMtPPheaderDTO -> tagNames.contains(rscMtPPheaderDTO.getTagName())).sorted(Comparator.comparing(RscMtPPheaderDTO::getId)).collect(Collectors.toList());
    }

    @Override
    public List<RscMtPPheaderDTO> updateMultipleValidatedPlansToPseudo(String mpsName, LocalDate mpsDate, String tagName) {
        if (!(tagName.equals("PFC") || tagName.equals("PVD"))) {
            RscMtPPheaderDTO rscMtPPHeaderDTOs = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findByMpsNameAndMpsDate(mpsName, mpsDate));
            if (Optional.ofNullable(rscMtPPHeaderDTOs).isPresent()) {
                List<RscMtPPheaderDTO> rscMtPPheaderDTOList = getAllPlansListByMpsDateAndTag(mpsName, mpsDate, tagName);
                if (Optional.ofNullable(rscMtPPheaderDTOList).isPresent()) {
                    if (tagName.equals("VD")) {
                        updatePpHeaderTagToPseudo(rscMtPPheaderDTOList, "PVD");
                    } else {
                        updatePpHeaderTagToPseudo(rscMtPPheaderDTOList, "PFC");
                    }
                }
                return rscMtPPheaderDTOList;
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private List<RscMtPPheaderDTO> getAllPlansListByMpsDateAndTag(String mpsName, LocalDate mpsDate, String tagName) {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName(tagName)).stream().
                filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().name().matches(mpsDate.getMonth().name()) && !rscMtPPheaderDTO.getMpsName().matches(mpsName)).collect(Collectors.toList());
    }

    private List<RscMtPPheaderDTO> updatePpHeaderTagToPseudo(List<RscMtPPheaderDTO> rscMtPPheaderDTOList, String tagName) {
        List<RscDtTagDTO> rscDtTagDTOList = rscDtTagService.findAll().stream().filter(rscDtTagDTO -> rscDtTagDTO.getName().equals(tagName)).collect(Collectors.toList());
        rscMtPPheaderDTOList.forEach(rscMtPPheaderDTO -> rscMtPPheaderDTO.setTagId(rscDtTagDTOList.get(0).getId().toString()));
        saveAll(rscMtPPheaderDTOList);
        return rscMtPPheaderDTOList;
    }

    @Override
    public Long getPreviousMonthsFirstCutMpsId(Long currentMonthMpsId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTOOptional = rscMtPpheaderRepository.findById(currentMonthMpsId).map(rscMtPPheaderMapper::toDto);
        Optional<RscMtPPheaderDTO> optionalRscMtPPheaderDTO = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("FC")).stream().filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getYear() == (rscMtPPheaderDTOOptional.get().getMpsDate().minusMonths(1).getYear())).filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().equals(rscMtPPheaderDTOOptional.get().getMpsDate().minusMonths(1).getMonth())).findFirst();
        if (optionalRscMtPPheaderDTO.isPresent()) return optionalRscMtPPheaderDTO.get().getId();
        return null;
    }

    @Override
    public List<RscMtPPheaderDTO> getAllMpsPlansWithBackupStatusFalse() {
        return rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActiveAndIsBackUpDoneIsNullOrIsBackUpDoneEquals(true, false));
    }

    @Override
    public void updateIsBackupDone() {
        List<RscMtPPheaderDTO> rscMtPPHeaderDTOs = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActiveAndIsBackUpDone(true, false)).stream().filter(rscMtPPheaderDTO -> rscMtPPheaderDTO.getMpsDate().getMonth().name().equals(java.time.LocalDate.now().minusMonths(1).getMonth().name())).collect(Collectors.toList());
        if (!rscMtPPHeaderDTOs.isEmpty()) {
            for (RscMtPPheaderDTO rscMtPPheaderDTO : rscMtPPHeaderDTOs) {
                rscMtPPheaderDTO.setIsBackUpDone(true);
            }
            rscMtPpheaderRepository.saveAll(rscMtPPheaderMapper.toEntity(rscMtPPHeaderDTOs));
        }
    }

    @Override
    public RscMtPPheaderDTO updateMpsDetailsPlanManagement(Boolean tagChangeStatus, RscMtPPheaderDTO rscMtPPheaderDTOs) {
        rscMtPpheaderRepository.save(rscMtPPheaderMapper.toEntity(rscMtPPheaderDTOs));
        if (tagChangeStatus == false) {
            return rscMtPPheaderDTOs;
        } else {
            return updatePpheaderWhenTagChanged(rscMtPPheaderDTOs);
        }
    }

    private RscMtPPheaderDTO updatePpheaderWhenTagChanged(RscMtPPheaderDTO rscMtPPheaderDTOs) {
        List<RscMtPPheaderDTO> rscMtPPheaderDTOList = getAllPlansListByMpsDateAndTag(rscMtPPheaderDTOs.getMpsName(), rscMtPPheaderDTOs.getMpsDate(), rscMtPPheaderDTOs.getTagName());
        if (rscMtPPheaderDTOs.getTagName().equals("VD")) {
            rscMtPPheaderDTOList = updatePpHeaderTagToPseudo(rscMtPPheaderDTOList, "PVD");
        } else {
            rscMtPPheaderDTOList = updatePpHeaderTagToPseudo(rscMtPPheaderDTOList, "PFC");
        }
        return rscMtPPheaderDTOs;
    }

    @Override
    public Boolean mpsPlanNameExistsForPlanManagement(String name, Long id) {
        Boolean status = false;
        List<RscMtPPheaderDTO> rscMtPPheaderList = rscMtPPheaderMapper.toDto((rscMtPpheaderRepository.findByMpsNameAndIdIsNotAndIsBackUpDoneIsNullOrIsBackUpDoneEquals(name, id, false)));
        for (int i = 0; i < rscMtPPheaderList.size(); i++) {
            if (rscMtPPheaderList.get(i).getMpsName().compareToIgnoreCase(name) == 0) {
                status = true;
                break;
            }
        }
        return status;
    }

    @Override
    public RscMtPPheaderDTO getPPheaderWithLatestBackupDate() {
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByActiveAndIsBackUpDone(true, true)).stream().max(Comparator.comparing(RscMtPPheaderDTO::getMpsDate)).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            rscMtPPheaderDTO.setMpsDate(getNextMonthsFirstDate(rscMtPPheaderDTO.getMpsDate()));
        }
        return rscMtPPheaderDTO;
    }

    private LocalDate getNextMonthsFirstDate(LocalDate mpsDate) {
        return mpsDate.plusMonths(1).with(TemporalAdjusters.firstDayOfMonth());
    }

    /**
     * This method is defined for to return latest validated plan.
     *
     * @return RscMtPPheaderDTO
     * @author kailas biradar
     * @date 2022/04/19
     */
    @Override
    public RscMtPPheaderDTO getValidatedPlan() {
        List<RscMtPPheaderDTO> validatedPlans = rscMtPPheaderMapper.toDto(rscMtPpheaderRepository.findAllByRscDtTagName("VD"));
        Collections.sort(validatedPlans, Comparator.comparingLong(RscMtPPheaderDTO::getId).reversed());
        return validatedPlans.stream().findFirst().orElse(null);
    }
}