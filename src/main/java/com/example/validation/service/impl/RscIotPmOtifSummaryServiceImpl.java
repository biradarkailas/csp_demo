package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.domain.RscIotPmOtifSummary;
import com.example.validation.dto.*;
import com.example.validation.mapper.OtifCalculationSuppliersMapper;
import com.example.validation.mapper.PmOtifSummaryMapper;
import com.example.validation.mapper.RscDtSupplierMapperBasedOnOtifSummaryMapper;
import com.example.validation.mapper.RscIotPmOtifSummaryMapper;
import com.example.validation.repository.RscIotPmOtifSummaryRepository;
import com.example.validation.service.RscIotPmOtifCalculationService;
import com.example.validation.service.RscIotPmOtifSummaryService;
import com.example.validation.service.RscMtOtifPmReceiptsHeaderService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.ListUtils;
import com.example.validation.util.MathUtils;
import com.example.validation.util.enums.SupplierWiseReceiptRange;
import com.example.validation.util.excel_export.PmOtifSummaryExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotPmOtifSummaryServiceImpl implements RscIotPmOtifSummaryService {
    private final RscIotPmOtifSummaryRepository rscIotPmOtifSummaryRepository;
    private final RscIotPmOtifSummaryMapper rscIotPmOtifSummaryMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotPmOtifCalculationService rscIotPmOtifCalculationService;
    private final PmOtifSummaryMapper pmOtifSummaryMapper;
    private final OtifCalculationSuppliersMapper otifCalculationSuppliersMapper;
    private final RscDtSupplierMapperBasedOnOtifSummaryMapper rscDtSupplierMapperBasedOnOtifSummaryMapper;
    private final RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService;

    public RscIotPmOtifSummaryServiceImpl(RscIotPmOtifSummaryRepository rscIotPmOtifSummaryRepository, RscIotPmOtifSummaryMapper rscIotPmOtifSummaryMapper, RscMtPpheaderService rscMtPpheaderService, RscIotPmOtifCalculationService rscIotPmOtifCalculationService, PmOtifSummaryMapper pmOtifSummaryMapper, OtifCalculationSuppliersMapper otifCalculationSuppliersMapper, RscDtSupplierMapperBasedOnOtifSummaryMapper rscDtSupplierMapperBasedOnOtifSummaryMapper, RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService) {
        this.rscIotPmOtifSummaryRepository = rscIotPmOtifSummaryRepository;
        this.rscIotPmOtifSummaryMapper = rscIotPmOtifSummaryMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotPmOtifCalculationService = rscIotPmOtifCalculationService;
        this.pmOtifSummaryMapper = pmOtifSummaryMapper;
        this.otifCalculationSuppliersMapper = otifCalculationSuppliersMapper;
        this.rscDtSupplierMapperBasedOnOtifSummaryMapper = rscDtSupplierMapperBasedOnOtifSummaryMapper;
        this.rscMtOtifPmReceiptsHeaderService = rscMtOtifPmReceiptsHeaderService;
    }

    @Override
    public void calculateRscIotPmOtifSummary(String otifDate) {
        if (!otifDate.equals("null")) {
            RscMtOtifPmReceiptsHeaderDTO rscMtOtifPmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.findByOtifDate(LocalDate.parse(otifDate));
            if (Optional.ofNullable(rscMtOtifPmReceiptsHeaderDTO).isPresent()) {
                Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findValidatedMpsPlanByOtifDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate());
                if (pPheaderDTO.isPresent()) {
                    if (rscIotPmOtifSummaryRepository.countByReceiptHeaderDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate()) == 0) {
                        List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList = rscIotPmOtifCalculationService.findAllByIdAndReceiptOtifDate(pPheaderDTO.get().getId(), rscMtOtifPmReceiptsHeaderDTO.getOtifDate());
                        if (Optional.ofNullable(rscIotPmOtifCalculationDTOList).isPresent()) {
                            List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList = getPmOtifSummaryList(rscIotPmOtifCalculationDTOList, pPheaderDTO.get().getId(), rscMtOtifPmReceiptsHeaderDTO);
                            saveAll(pmOtifSummaryDTOList);
                        }
                    }
                }
            }
        }

    }

    @Override
    public void saveAll(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList) {
        rscIotPmOtifSummaryRepository.saveAll(rscIotPmOtifSummaryMapper.toEntity(pmOtifSummaryDTOList));
    }

    private List<RscIotPmOtifSummaryDTO> getPmOtifSummaryList(List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList, Long ppHeaderId, RscMtOtifPmReceiptsHeaderDTO rscMtOtifPmReceiptsHeaderDTO) {
        List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList = new ArrayList<>();
        List<Long> uniqueSuppilersList = getUniqueSuppliersList(rscIotPmOtifCalculationDTOList);
        if (Optional.ofNullable(uniqueSuppilersList).isPresent()) {
            uniqueSuppilersList.forEach(uniqueSuppilerId -> {
                RscIotPmOtifSummaryDTO rscIotPmOtifSummaryDTO = new RscIotPmOtifSummaryDTO();
                List<RscIotPmOtifCalculationDTO> pmOtifCalculationDTOList = getOtifCalculationDTOListWithUniqueSupplierId(rscIotPmOtifCalculationDTOList, uniqueSuppilerId);
                rscIotPmOtifSummaryDTO.setRscDtSupplierId(uniqueSuppilerId);
                rscIotPmOtifSummaryDTO.setRscMtPPheaderId(ppHeaderId);
                rscIotPmOtifSummaryDTO.setReceiptHeaderDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate());
                rscIotPmOtifSummaryDTO.setOtifSummaryDate(LocalDate.now());
                rscIotPmOtifSummaryDTO.setAvgReceiptPercentage(calculateAvgReceiptPercentage(pmOtifCalculationDTOList));
                if (rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE4.getNumVal()) {
                    rscIotPmOtifSummaryDTO.setAvgScore(4);
                } else if (rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE3.getNumVal() && rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() < SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE3.getNumVal()) {
                    rscIotPmOtifSummaryDTO.setAvgScore(3);
                } else if (rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE2.getNumVal() && rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() < SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE2.getNumVal()) {
                    rscIotPmOtifSummaryDTO.setAvgScore(2);
                } else if (rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE1.getNumVal() && rscIotPmOtifSummaryDTO.getAvgReceiptPercentage() < SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE2.getNumVal()) {
                    rscIotPmOtifSummaryDTO.setAvgScore(1);
                } else {
                    rscIotPmOtifSummaryDTO.setAvgScore(0);
                }
                pmOtifSummaryDTOList.add(rscIotPmOtifSummaryDTO);
            });
        }
        return pmOtifSummaryDTOList;
    }

    private Double calculateAvgReceiptPercentage(List<RscIotPmOtifCalculationDTO> pmOtifCalculationDTOList) {
        return DoubleUtils.round(MathUtils.divide(MathUtils.multiply(pmOtifCalculationDTOList.stream().filter(s -> s.getReceiptPercentage() >= 100).count(), 100), pmOtifCalculationDTOList.size()));
    }

    private List<Long> getUniqueSuppliersList(List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList) {
        return rscIotPmOtifCalculationDTOList.stream().
                map(RscIotPmOtifCalculationDTO::getRscDtSupplierId).distinct().collect(Collectors.toList());
    }

    private List<RscIotPmOtifCalculationDTO> getOtifCalculationDTOListWithUniqueSupplierId(List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList, Long uniqueSuppilerId) {
        return rscIotPmOtifCalculationDTOList.stream().filter(rscIotPmOtifCalculationDTO -> rscIotPmOtifCalculationDTO.getRscDtSupplierId().equals(uniqueSuppilerId)).collect(Collectors.toList());
    }

    @Override
    public OtifCalculationDetailsDTO getAllRscItPmOtifCalculation(Long rscMtPPHeaderId) {
        OtifCalculationDetailsDTO otifCalculationDetailsDTO = new OtifCalculationDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        RscMtOtifPmReceiptsHeaderDTO pmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.getLatestReceiptHeaderId(rscMtPPheaderDTO.get().getMpsDate());
        if (rscMtPPheaderDTO.isPresent() && Optional.ofNullable(pmReceiptsHeaderDTO).isPresent()) {
            otifCalculationDetailsDTO.setMpsId(rscMtPPheaderDTO.get().getId());
            otifCalculationDetailsDTO.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
            otifCalculationDetailsDTO.setOtifDate(pmReceiptsHeaderDTO.getOtifDate());
            otifCalculationDetailsDTO.setMpsName(rscMtPPheaderDTO.get().getMpsName());
            otifCalculationDetailsDTO.setCurrentMonth(DateUtils.getNextMonthName(rscMtPPheaderDTO.get().getMpsDate()));
            otifCalculationDetailsDTO.setPreviousMonth(DateUtils.getMonthValue(rscMtPPheaderDTO.get().getMpsDate()));
            otifCalculationDetailsDTO.setOtifCalculationDetailsDTO(getOtifCalculationDetails(pmReceiptsHeaderDTO.getOtifDate()));
        }
        return otifCalculationDetailsDTO;
    }


    public ExcelExportFiles pmOtifSummaryExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent() && Optional.ofNullable( rscMtPPheaderDTO.getOtifDate() ).isPresent()) {
            OtifCalculationDetailsDTO otifCalculationDetailsDTO = getAllRscItPmOtifCalculation( rscMtPPheaderDTO.getId() );
            excelExportFiles.setFile( PmOtifSummaryExporter.pmOtifSummaryToExcel( otifCalculationDetailsDTO, monthDetailDTO, rscMtPPheaderDTO.getOtifDate() ) );
            excelExportFiles.setFileName( ExcelFileNameConstants.PM_Otif_Summary + rscMtPPheaderDTO.getOtifDate() + ExcelFileNameConstants.Extension );
        }
        return excelExportFiles;
    }

    private List<RscIotPmOtifCalculationDetailsDTO> getOtifCalculationDetails(LocalDate otifHeaderDate) {
        List<RscIotPmOtifCalculationDetailsDTO> otifCalculationDetailsDTOList = pmOtifSummaryMapper.toDto(rscIotPmOtifSummaryRepository.findAllByReceiptHeaderDate(otifHeaderDate).stream().sorted(Comparator.comparing(RscIotPmOtifSummary::getAvgReceiptPercentage).reversed()).collect(Collectors.toList()));
        otifCalculationDetailsDTOList.forEach(rscIotPmOtifCalculationDetailsDTO -> {
            List<RscIotPmOtifCalculationDTO> pmOtifCalculationDTOList = rscIotPmOtifCalculationService.findAllBySupplierIdAndOtifReceiptDate(rscIotPmOtifCalculationDetailsDTO.getRscDtSupplierId(), otifHeaderDate);
            rscIotPmOtifCalculationDetailsDTO.setPmOtifCalculationDetailsDTO(getOtifCalculationList(pmOtifCalculationDTOList));
        });
        return otifCalculationDetailsDTOList;
    }

    private List<PmOtifCalculationDetailsDTO> getOtifCalculationList(List<RscIotPmOtifCalculationDTO> pmOtifCalculationDTOList) {
        List<PmOtifCalculationDetailsDTO> pmOtifCalculationDetailsDTO = new ArrayList<>();
        pmOtifCalculationDTOList.forEach(rscIotPmOtifCalculationDTO -> {
            PmOtifCalculationDetailsDTO calculationDetailsDTO = new PmOtifCalculationDetailsDTO();
            calculationDetailsDTO.setRscMtItemId(rscIotPmOtifCalculationDTO.getRscMtItemId());
            calculationDetailsDTO.setItemCode(rscIotPmOtifCalculationDTO.getItemCode());
            calculationDetailsDTO.setItemDescription(rscIotPmOtifCalculationDTO.getItemDescription());
            calculationDetailsDTO.setCurrentMonthOrderQty(rscIotPmOtifCalculationDTO.getCurrentMonthOrderQty());
            calculationDetailsDTO.setPrevMonthOrderQty(rscIotPmOtifCalculationDTO.getPrevMonthOrderQty());
            calculationDetailsDTO.setTotalOrderQty(rscIotPmOtifCalculationDTO.getTotalOrderQty());
            calculationDetailsDTO.setTotalReceivedQty(rscIotPmOtifCalculationDTO.getTotalReceivedQty());
            calculationDetailsDTO.setBalanceQty(rscIotPmOtifCalculationDTO.getBalanceQty());
            calculationDetailsDTO.setReceiptPercentage(rscIotPmOtifCalculationDTO.getReceiptPercentage());
            calculationDetailsDTO.setScore(rscIotPmOtifCalculationDTO.getScore());
            pmOtifCalculationDetailsDTO.add(calculationDetailsDTO);
        });
        return pmOtifCalculationDetailsDTO;
    }

    @Override
    public OtifCalculationSuppliersDetailsDTO getPmOtifCalculationSuppliers(Long ppHeaderId) {
        OtifCalculationSuppliersDetailsDTO suppliersDetailsDTO = new OtifCalculationSuppliersDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if ((rscMtPPheaderDTO).isPresent()) {
            RscMtOtifPmReceiptsHeaderDTO pmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.getLatestReceiptHeaderId(rscMtPPheaderDTO.get().getMpsDate());
            if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && Optional.ofNullable(pmReceiptsHeaderDTO).isPresent()) {
                suppliersDetailsDTO.setMpsId(rscMtPPheaderDTO.get().getId());
                suppliersDetailsDTO.setMpsName(rscMtPPheaderDTO.get().getMpsName());
                suppliersDetailsDTO.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
                suppliersDetailsDTO.setScore4ReceiptRangeSuppliersDTOList(getReceiptRangeSuppliersByReceiptHeaderDateScore(pmReceiptsHeaderDTO.getOtifDate(), 4));
                suppliersDetailsDTO.setScore3ReceiptRangeSuppliersDTOList(getReceiptRangeSuppliersByReceiptHeaderDateScore(pmReceiptsHeaderDTO.getOtifDate(), 3));
                suppliersDetailsDTO.setScore2ReceiptRangeSuppliersDTOList(getReceiptRangeSuppliersByReceiptHeaderDateScore(pmReceiptsHeaderDTO.getOtifDate(), 2));
                suppliersDetailsDTO.setScore1ReceiptRangeSuppliersDTOList(getReceiptRangeSuppliersByReceiptHeaderDateScore(pmReceiptsHeaderDTO.getOtifDate(), 1));
                suppliersDetailsDTO.setScore0ReceiptRangeSuppliersDTOList(getReceiptRangeSuppliersByReceiptHeaderDateScore(pmReceiptsHeaderDTO.getOtifDate(), 0));

                suppliersDetailsDTO.setScore4ReceiptRangeListCount(suppliersDetailsDTO.getScore4ReceiptRangeSuppliersDTOList().size());
                suppliersDetailsDTO.setScore3ReceiptRangeListCount(suppliersDetailsDTO.getScore3ReceiptRangeSuppliersDTOList().size());
                suppliersDetailsDTO.setScore2ReceiptRangeListCount(suppliersDetailsDTO.getScore2ReceiptRangeSuppliersDTOList().size());
                suppliersDetailsDTO.setScore1ReceiptRangeListCount(suppliersDetailsDTO.getScore1ReceiptRangeSuppliersDTOList().size());
                suppliersDetailsDTO.setScore0ReceiptRangeListCount(suppliersDetailsDTO.getScore0ReceiptRangeSuppliersDTOList().size());

                suppliersDetailsDTO.setTotalCount(getTotalReceiptCount(suppliersDetailsDTO.getScore4ReceiptRangeListCount(), suppliersDetailsDTO.getScore3ReceiptRangeListCount(), suppliersDetailsDTO.getScore2ReceiptRangeListCount(), suppliersDetailsDTO.getScore1ReceiptRangeListCount(), suppliersDetailsDTO.getScore0ReceiptRangeListCount()));
                suppliersDetailsDTO.setReceiptRangeForScore4(">= " + SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE4.getNumVal() + "%");
                suppliersDetailsDTO.setReceiptRangeForScore3(">= " + SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE3.getNumVal() + "% - <" + SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE3.getNumVal() + "%");
                suppliersDetailsDTO.setReceiptRangeForScore2(">= " + SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE2.getNumVal() + "% - <" + SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE2.getNumVal() + "%");
                suppliersDetailsDTO.setReceiptRangeForScore1(">= " + SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE1.getNumVal() + "% - <" + SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE1.getNumVal() + "%");
                suppliersDetailsDTO.setReceiptRangeForScore0("< " + SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE0.getNumVal() + "%");

            }
        }

        return suppliersDetailsDTO;
    }

    private Integer getTotalReceiptCount(Integer score4ReceiptCount, Integer score3ReceiptCount, Integer score2ReceiptCount, Integer score1ReceiptCount, Integer score0ReceiptCount) {
        return score4ReceiptCount + score3ReceiptCount + score2ReceiptCount + score1ReceiptCount + score0ReceiptCount;
    }

    private List<OtifCalculationSuppliersDTO> getReceiptRangeSuppliersByScore(Long ppHeaderId, LocalDate otifDate, Integer avgScore) {
        return otifCalculationSuppliersMapper.toDto(rscIotPmOtifSummaryRepository.findByRscMtPPheaderIdAndReceiptHeaderDateAndAvgScore(ppHeaderId, otifDate, avgScore).stream().sorted(Comparator.comparing(RscIotPmOtifSummary::getAvgReceiptPercentage).reversed()).collect(Collectors.toList()));
    }

    private List<OtifCalculationSuppliersDTO> getReceiptRangeSuppliersByReceiptHeaderDateScore(LocalDate otifDate, Integer avgScore) {
        return otifCalculationSuppliersMapper.toDto(rscIotPmOtifSummaryRepository.findByReceiptHeaderDateAndAvgScore(otifDate, avgScore).stream().sorted(Comparator.comparing(RscIotPmOtifSummary::getAvgReceiptPercentage).reversed()).collect(Collectors.toList()));
    }

    @Override
    public List<RscIotPmOtifSummaryDTO> getPmOtifCalculationSuppliersList(Long ppHeaderId) {
        List<RscIotPmOtifSummaryDTO> rscIotPmOtifSummaryDTO = new ArrayList<>();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        RscMtOtifPmReceiptsHeaderDTO pmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.getLatestReceiptHeaderId(rscMtPPheaderDTO.getMpsDate());
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && Optional.ofNullable(pmReceiptsHeaderDTO).isPresent()) {
            rscIotPmOtifSummaryDTO.addAll(rscIotPmOtifSummaryMapper.toDto(rscIotPmOtifSummaryRepository.findAllByReceiptHeaderDate(pmReceiptsHeaderDTO.getOtifDate()).stream().sorted(Comparator.comparing(RscIotPmOtifSummary::getAvgReceiptPercentage).reversed()).collect(Collectors.toList())));
        }
        return rscIotPmOtifSummaryDTO;
    }

    @Override
    public PmOtifCalculationSupplierWiseListDTO getSingleSuppliersDetails(Long ppHeaderId, Long rscDtSupplierId) {
        PmOtifCalculationSupplierWiseListDTO pmOtifCalculationSupplierWiseListDTO = new PmOtifCalculationSupplierWiseListDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        RscMtOtifPmReceiptsHeaderDTO pmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.getLatestReceiptHeaderId(rscMtPPheaderDTO.getMpsDate());
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && Optional.ofNullable(pmReceiptsHeaderDTO).isPresent()) {
            RscIotPmOtifSummaryDTO rscIotPmOtifSummaryDTO = rscIotPmOtifSummaryMapper.toDto(rscIotPmOtifSummaryRepository.findByRscDtSupplierIdAndReceiptHeaderDate(rscDtSupplierId, pmReceiptsHeaderDTO.getOtifDate()));
            if (Optional.ofNullable(rscIotPmOtifSummaryDTO).isPresent()) {
                pmOtifCalculationSupplierWiseListDTO.setRscDtSupplierId(rscIotPmOtifSummaryDTO.getRscDtSupplierId());
                pmOtifCalculationSupplierWiseListDTO.setSupplierCode(rscIotPmOtifSummaryDTO.getSupplierCode());
                pmOtifCalculationSupplierWiseListDTO.setSupplierName(rscIotPmOtifSummaryDTO.getSupplierName());
                pmOtifCalculationSupplierWiseListDTO.setAvgReceiptPercentage(rscIotPmOtifSummaryDTO.getAvgReceiptPercentage());
                pmOtifCalculationSupplierWiseListDTO.setAvgScore(rscIotPmOtifSummaryDTO.getAvgScore());
            }
            pmOtifCalculationSupplierWiseListDTO.setRscIotPmOtifCalculationDTOList(rscIotPmOtifCalculationService.findAllBySupplierIdAndOtifReceiptDate(rscDtSupplierId, pmReceiptsHeaderDTO.getOtifDate()));
        }
        return pmOtifCalculationSupplierWiseListDTO;
    }

    @Override
    public List<RscDtSupplierDTO> getAllUniqueSuppliersList() {
        return rscDtSupplierMapperBasedOnOtifSummaryMapper.toDto(rscIotPmOtifSummaryRepository.findAll().stream().filter(ListUtils.distinctByKey(rscIotPmOtifSummary -> rscIotPmOtifSummary.getRscDtSupplier().getId())).collect(Collectors.toList()));
    }

    @Override
    public List<RscIotPmOtifSummaryDTO> getSuppliersList(Long rscDtSupplierId) {
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllPPheaderListSortedBasedOnMpsDateandMpsValidated();
        List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList = rscIotPmOtifSummaryMapper.toDto(rscIotPmOtifSummaryRepository.findByRscDtSupplierId(rscDtSupplierId));
        List<RscIotPmOtifSummaryDTO> summaryDTOList = new ArrayList<>();
        if (Optional.ofNullable(pmOtifSummaryDTOList).isPresent()) {
            pPheaderDTOList.forEach(rscMtPPheaderDTO -> {
                List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOS = pmOtifSummaryDTOList.stream().filter(rscIotPmOtifSummaryDTO -> rscIotPmOtifSummaryDTO.getReceiptHeaderDate().getMonth().name().matches(rscMtPPheaderDTO.getMpsDate().getMonth().name())).collect(Collectors.toList());
                if (pmOtifSummaryDTOS.size() > 0) {
                    summaryDTOList.add(pmOtifSummaryDTOS.get(pmOtifSummaryDTOS.size() - 1));
                }
            });
        }
        return summaryDTOList;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotPmOtifSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}