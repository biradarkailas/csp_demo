package com.example.validation.service.impl;

import com.example.validation.dto.RscMtRmOpenPoDTO;
import com.example.validation.mapper.RscMtRmOpenPoMapper;
import com.example.validation.repository.RscMtRmOpenPoRepository;
import com.example.validation.service.RscMtRmOpenPoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscMtRmOpenPoServiceImpl implements RscMtRmOpenPoService {
    private final RscMtRmOpenPoRepository rscMtRmOpenPoRepository;
    private final RscMtRmOpenPoMapper rscMtRmOpenPoMapper;

    public RscMtRmOpenPoServiceImpl(RscMtRmOpenPoRepository rscMtRmOpenPoRepository, RscMtRmOpenPoMapper rscMtRmOpenPoMapper) {
        this.rscMtRmOpenPoRepository = rscMtRmOpenPoRepository;
        this.rscMtRmOpenPoMapper = rscMtRmOpenPoMapper;
    }

    @Override
    public List<RscMtRmOpenPoDTO> findAllByRscMtItemIdAndMtSupplierId(Long mpsId, Long rscMtItemId, Long rscMtSupplierId) {
        return rscMtRmOpenPoMapper.toDto(rscMtRmOpenPoRepository.findAllByRscMtPPheaderIdAndRscMtItemIdAndRscMtSupplierId(mpsId, rscMtItemId, rscMtSupplierId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscMtRmOpenPoRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}