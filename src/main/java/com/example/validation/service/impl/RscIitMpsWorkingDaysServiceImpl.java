package com.example.validation.service.impl;

import com.example.validation.dto.RscIitMpsWorkingDaysDTO;
import com.example.validation.mapper.RscIitMpsWorkingDaysMapper;
import com.example.validation.repository.RscIitMpsWorkingDaysRepository;
import com.example.validation.service.RscIitMpsWorkingDaysService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RscIitMpsWorkingDaysServiceImpl implements RscIitMpsWorkingDaysService {
    private final RscIitMpsWorkingDaysRepository rscIitMpsWorkingDaysRepository;
    private final RscIitMpsWorkingDaysMapper rscIitMpsWorkingDaysMapper;

    public RscIitMpsWorkingDaysServiceImpl(RscIitMpsWorkingDaysRepository rscIitMpsWorkingDaysRepository, RscIitMpsWorkingDaysMapper rscIitMpsWorkingDaysMapper) {
        this.rscIitMpsWorkingDaysRepository = rscIitMpsWorkingDaysRepository;
        this.rscIitMpsWorkingDaysMapper = rscIitMpsWorkingDaysMapper;
    }

    @Override
    public RscIitMpsWorkingDaysDTO findByMpsId(Long mpsId) {
        return rscIitMpsWorkingDaysMapper.toDto(rscIitMpsWorkingDaysRepository.findByRscMtPPheaderId(mpsId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIitMpsWorkingDaysRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}