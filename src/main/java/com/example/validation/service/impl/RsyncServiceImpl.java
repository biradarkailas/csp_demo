package com.example.validation.service.impl;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.service.DataLoadService;
import com.example.validation.service.RsyncService;
import com.example.validation.util.FileOperation;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

@Service
public class RsyncServiceImpl implements RsyncService {
    private final DataLoadService dataLoadService;

    public RsyncServiceImpl(DataLoadService dataLoadService) {
        this.dataLoadService = dataLoadService;
    }

    public Set<String> loadFilesWithoutRsync(String mpsName) {
        Set<String> files = new HashSet<>();
        String[] fileArray = FileOperation.getFilesInDirectory(new File(ApplicationConstants.SOURCE_PATH));
        if (fileArray != null)
            for (String s : fileArray) {
                if (new File(ApplicationConstants.SOURCE_PATH + ApplicationConstants.FORWARD_SLASH + s).isFile())
                    files.add(s);
            }
        dataLoadService.loadData(files, mpsName);
        return files;
    }
}
