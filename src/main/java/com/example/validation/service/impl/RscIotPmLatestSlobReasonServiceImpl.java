package com.example.validation.service.impl;

import com.example.validation.dto.RscIotPmLatestSlobReasonDTO;
import com.example.validation.mapper.RscIotPmLatestSlobReasonMapper;
import com.example.validation.repository.RscIotPmLatestSlobReasonRepository;
import com.example.validation.service.RscIotPmLatestSlobReasonService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscIotPmLatestSlobReasonServiceImpl implements RscIotPmLatestSlobReasonService {
    private final RscIotPmLatestSlobReasonRepository rscIotPmLatestSlobReasonRepository;
    private final RscIotPmLatestSlobReasonMapper rscIotPmLatestSlobReasonMapper;

    public RscIotPmLatestSlobReasonServiceImpl(RscIotPmLatestSlobReasonRepository rscIotPmLatestSlobReasonRepository, RscIotPmLatestSlobReasonMapper rscIotPmLatestSlobReasonMapper) {
        this.rscIotPmLatestSlobReasonRepository = rscIotPmLatestSlobReasonRepository;
        this.rscIotPmLatestSlobReasonMapper = rscIotPmLatestSlobReasonMapper;
    }

    @Override
    public List<RscIotPmLatestSlobReasonDTO> findAll() {
        return rscIotPmLatestSlobReasonMapper.toDto(rscIotPmLatestSlobReasonRepository.findAll());
    }

    @Override
    public void saveAll(List<RscIotPmLatestSlobReasonDTO> pmLatestSlobReasonDTOS) {
        rscIotPmLatestSlobReasonRepository.saveAll(rscIotPmLatestSlobReasonMapper.toEntity(pmLatestSlobReasonDTOS));
    }

    @Override
    public void saveOne(RscIotPmLatestSlobReasonDTO pmLatestSlobReasonDTO) {
       /* if (pmLatestSlobReasonDTO.getId() == null) {
        } else {*/
            rscIotPmLatestSlobReasonRepository.save(rscIotPmLatestSlobReasonMapper.toEntity(pmLatestSlobReasonDTO));
//        }
    }
}
