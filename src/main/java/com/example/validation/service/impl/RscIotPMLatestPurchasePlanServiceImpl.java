package com.example.validation.service.impl;

import com.example.validation.dto.LatestPurchasePlanDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import com.example.validation.mapper.LatestPurchasePlanMapper;
import com.example.validation.mapper.RscIotPMLatestPurchasePlanMapper;
import com.example.validation.repository.RscIotPMLatestPurchasePlanRepository;
import com.example.validation.service.RscIotPMLatestPurchasePlanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscIotPMLatestPurchasePlanServiceImpl implements RscIotPMLatestPurchasePlanService {
    private final RscIotPMLatestPurchasePlanRepository rscIotPMLatestPurchasePlanRepository;
    private final RscIotPMLatestPurchasePlanMapper rscIotPMLatestPurchasePlanMapper;
    private final LatestPurchasePlanMapper latestPurchasePlanMapper;

    public RscIotPMLatestPurchasePlanServiceImpl(RscIotPMLatestPurchasePlanRepository rscIotPMLatestPurchasePlanRepository, RscIotPMLatestPurchasePlanMapper rscIotPMLatestPurchasePlanMapper, LatestPurchasePlanMapper latestPurchasePlanMapper) {
        this.rscIotPMLatestPurchasePlanRepository = rscIotPMLatestPurchasePlanRepository;
        this.rscIotPMLatestPurchasePlanMapper = rscIotPMLatestPurchasePlanMapper;
        this.latestPurchasePlanMapper = latestPurchasePlanMapper;
    }

    @Override
    public List<RscIotPMPurchasePlanDTO> getPurchasePlanById(Long rscMtPPheaderId) {
        return rscIotPMLatestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlanRepository.findAllByRscMtPPheaderId(rscMtPPheaderId));
    }

    @Override
    public List<LatestPurchasePlanDTO> getPurchasePlanListForMouldCalculation(Long rscMtPPHeaderId) {
        return latestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlanRepository.findAllByRscMtPPheaderId(rscMtPPHeaderId));
    }

    @Override
    public List<LatestPurchasePlanDTO> getPurchasePlanListByIdList(List<Long> purchasePlanIdList, Long rscMtPpHeaderId) {
        return latestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlanRepository.findAllByRscMtPPheaderIdAndIdIn(rscMtPpHeaderId, purchasePlanIdList));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotPMLatestPurchasePlanRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
