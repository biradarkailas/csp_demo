package com.example.validation.service.impl;

import com.example.validation.domain.PartyContactDetails;
import com.example.validation.repository.PartyContactDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartyContactDetailsService {
    @Autowired
    PartyContactDetailsRepo partyContactDetailsRepo;

    public PartyContactDetails save(PartyContactDetails partyContactDetails) {
        return partyContactDetailsRepo.save(partyContactDetails);
    }

    public PartyContactDetails findPartyContactDetailsByEmailIdOne(String emailOne){
        return partyContactDetailsRepo.findByEmailIdOne(emailOne);
    }
}
