package com.example.validation.service.impl;

import com.example.validation.dto.ExcelFiledetailsDTO;
import com.example.validation.mapper.ExcelFileDetailsMapper;
import com.example.validation.repository.ExcelFileDetailsRepository;
import com.example.validation.service.ExcelFileDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class ExcelFileDetailsServiceImpl implements ExcelFileDetailsService {
    private final ExcelFileDetailsRepository excelFileDetailsRepository;
    private final ExcelFileDetailsMapper excelFileDetailsMapper;

    public ExcelFileDetailsServiceImpl(ExcelFileDetailsRepository excelFileDetailsRepository, ExcelFileDetailsMapper excelFileDetailsMapper) {
        this.excelFileDetailsRepository = excelFileDetailsRepository;
        this.excelFileDetailsMapper = excelFileDetailsMapper;
    }

    @Override
    public List<String> getSheetNamesByFileName(String fileName) {
        Optional<List<ExcelFiledetailsDTO>> excelFileName = Optional.ofNullable(excelFileDetailsMapper.toDto(excelFileDetailsRepository.findByFileName(fileName)));
        return excelFileName.map(excelFileDetailsDTOS -> excelFileDetailsDTOS.stream().map(ExcelFiledetailsDTO::getSheetName).collect(Collectors.toList())).orElse(null);
    }

    @Override
    public ExcelFiledetailsDTO findOneBySheetName(String sheetName) {
        return excelFileDetailsMapper.toDto(excelFileDetailsRepository.findOneBySheetName(sheetName));
    }

    @Override
    public List<ExcelFiledetailsDTO> getExcelFiledetailsDTOs(String fileName) {
        return excelFileDetailsMapper.toDto(excelFileDetailsRepository.findByFileName(fileName));
    }
}