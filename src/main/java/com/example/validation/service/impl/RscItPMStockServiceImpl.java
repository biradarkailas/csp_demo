package com.example.validation.service.impl;

import com.example.validation.dto.PmStockItemCodeDTO;
import com.example.validation.dto.RscItPMStockDTO;
import com.example.validation.mapper.PmStockItemCodeMapper;
import com.example.validation.mapper.RscItPMStockMapper;
import com.example.validation.repository.RscIotPMStockRepository;
import com.example.validation.service.RscItPMStockService;
import com.example.validation.util.DateUtils;
import com.example.validation.util.ListUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscItPMStockServiceImpl implements RscItPMStockService {
    private final RscIotPMStockRepository rscIotPMStockRepository;
    private final RscItPMStockMapper rscItPMStockMapper;
    private final PmStockItemCodeMapper pmStockItemCodeMapper;

    public RscItPMStockServiceImpl(RscIotPMStockRepository rscIotPMStockRepository, RscItPMStockMapper rscItPMStockMapper, PmStockItemCodeMapper pmStockItemCodeMapper) {
        this.rscIotPMStockRepository = rscIotPMStockRepository;
        this.rscItPMStockMapper = rscItPMStockMapper;
        this.pmStockItemCodeMapper = pmStockItemCodeMapper;
    }

    @Override
    public List<RscItPMStockDTO> findByRscMtItemId(Long rscMtItemId, LocalDate mpsDate) {
        return getMpsDateWisePMStockDTOs(mpsDate, rscItPMStockMapper.toDto(rscIotPMStockRepository.findAllByRscMtItemId(rscMtItemId)));
    }

    private List<RscItPMStockDTO> getMpsDateWisePMStockDTOs(LocalDate mpsDate, List<RscItPMStockDTO> rscItPMStockDTOs) {
        return rscItPMStockDTOs.stream()
                .filter(rscItPMStockDTO -> DateUtils.isEqualMonthAndYear(rscItPMStockDTO.getStockDate(), mpsDate))
                .collect(Collectors.toList());
    }

    @Override
    public List<RscItPMStockDTO> findAllRscItPmStock() {
        return rscItPMStockMapper.toDto(rscIotPMStockRepository.findAll());
    }

    @Override
    public List<RscItPMStockDTO> findAllRscItPmStockByStockDate(LocalDate mpsDate) {
        return rscItPMStockMapper.toDto(rscIotPMStockRepository.findAllRscItPmStockByStockDate(mpsDate));
    }

    @Override
    public List<RscItPMStockDTO> findAll() {
        return rscItPMStockMapper.toDto(rscIotPMStockRepository.findAll());
    }

    @Override
    public List<PmStockItemCodeDTO> findAllDistinctByRscMtItem() {
        return pmStockItemCodeMapper.toDto(rscIotPMStockRepository.findAll()).stream()
                .filter(ListUtils.distinctByKey(pmStockItemCodeDTO -> pmStockItemCodeDTO.getId()))
                .collect(Collectors.toList());
    }

    @Override
    public List<RscItPMStockDTO> findAllRscItPmStockByStockTypeTag(List<String> stockTypeList) {
        return rscItPMStockMapper.toDto(rscIotPMStockRepository.findAllByRscDtStockTypeTagIn(stockTypeList));
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscIotPMStockRepository.deleteAllByStockDate(stockDate);
    }
}