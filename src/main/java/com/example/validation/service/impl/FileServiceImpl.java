package com.example.validation.service.impl;

import com.example.validation.dto.FileNameDTO;
import com.example.validation.mapper.FCPlanFileNameMapper;
import com.example.validation.mapper.PSPlanFileNameMapper;
import com.example.validation.mapper.VDPlanFileNameMapper;
import com.example.validation.repository.RscDtFileNameRepo;
import com.example.validation.service.FileService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileServiceImpl implements FileService {
    private final RscDtFileNameRepo rscDtFileNameRepo;
    private final VDPlanFileNameMapper vdPlanFileNameMapper;
    private final FCPlanFileNameMapper fcPlanFileNameMapper;
    private final PSPlanFileNameMapper psPlanFileNameMapper;


    public FileServiceImpl(RscDtFileNameRepo rscDtFileNameRepo, VDPlanFileNameMapper vdPlanFileNameMapper, FCPlanFileNameMapper fcPlanFileNameMapper, PSPlanFileNameMapper psPlanFileNameMapper) {
        this.rscDtFileNameRepo = rscDtFileNameRepo;
        this.vdPlanFileNameMapper = vdPlanFileNameMapper;
        this.fcPlanFileNameMapper = fcPlanFileNameMapper;
        this.psPlanFileNameMapper = psPlanFileNameMapper;
    }

    @Override
    public List<FileNameDTO> findAllFiles(String tagName) {
        switch (tagName) {
            case "FC":
                return fcPlanFileNameMapper.toDto(rscDtFileNameRepo.findAll());
            case "VD":
                return vdPlanFileNameMapper.toDto(rscDtFileNameRepo.findAll());
            case "PFC":
                return fcPlanFileNameMapper.toDto(rscDtFileNameRepo.findAll());
            case "PVD":
                return vdPlanFileNameMapper.toDto(rscDtFileNameRepo.findAll());
        }
        return null;
    }
}