package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.mapper.RMLogicalConsumptionMapper;
import com.example.validation.mapper.RscItRMLogicalConsumptionMapper;
import com.example.validation.mapper.RscItRMUniqueLogicalConsumptionMapper;
import com.example.validation.repository.RscItRMLogicalConsumptionRepository;
import com.example.validation.repository.RscItRMUniqueLogicalConsumptionRepository;
import com.example.validation.service.*;
import com.example.validation.util.excel_export.RmBulkLogicalConsumptionExporter;
import com.example.validation.util.excel_export.RmLogicalConsumptionExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RscItRMLogicalConsumptionServiceImpl implements RscItRMLogicalConsumptionService {
    private final RscItRMLogicalConsumptionRepository rscItRmLogicalConsumptionRepository;
    private final RscItRMLogicalConsumptionMapper rscItRMLogicalConsumptionMapper;
    private final RMLogicalConsumptionMapper rmLogicalConsumptionMapper;
    private final RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService;
    private final RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final RscItRMUniqueLogicalConsumptionRepository rscItRMUniqueLogicalConsumptionRepository;
    private final RscItRMUniqueLogicalConsumptionMapper rscItRMUniqueLogicalConsumptionMapper;
    private final RscMtPpdetailsService rscMtPpdetailsService;

    public RscItRMLogicalConsumptionServiceImpl(RscItRMLogicalConsumptionRepository rscItRmLogicalConsumptionRepository, RscItRMLogicalConsumptionMapper rscItRMLogicalConsumptionMapper, RMLogicalConsumptionMapper rmLogicalConsumptionMapper, RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService, RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService, RscItRMUniqueLogicalConsumptionRepository rscItRMUniqueLogicalConsumptionRepository, RscItRMUniqueLogicalConsumptionMapper rscItRMUniqueLogicalConsumptionMapper, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService, RscMtPpdetailsService rscMtPpdetailsService) {
        this.rscItRmLogicalConsumptionRepository = rscItRmLogicalConsumptionRepository;
        this.rscItRMLogicalConsumptionMapper = rscItRMLogicalConsumptionMapper;
        this.rmLogicalConsumptionMapper = rmLogicalConsumptionMapper;
        this.rscItBPLogicalConsumptionService = rscItBPLogicalConsumptionService;
        this.rscItBulkLogicalConsumptionService = rscItBulkLogicalConsumptionService;
        this.rscItRMUniqueLogicalConsumptionRepository = rscItRMUniqueLogicalConsumptionRepository;
        this.rscItRMUniqueLogicalConsumptionMapper = rscItRMUniqueLogicalConsumptionMapper;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
    }

    @Override
    public List<LogicalConsumptionMainDTO> getRMLogicalConsumptionByFG(Long ppHeaderId, Long bulkItemId) {
        List<LogicalConsumptionMainDTO> result = new ArrayList<>();
        result.addAll(rmLogicalConsumptionMapper.toDto(rscItRMUniqueLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(ppHeaderId, bulkItemId)));
        result.addAll(rscItBPLogicalConsumptionService.getBPLogicalConsumptionByBulk(ppHeaderId, bulkItemId));
        return result;
    }

    @Override
    public void saveAll(List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOs, Long ppHeaderId) {
        if (rscItRmLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            rscItRmLogicalConsumptionRepository.saveAll(rscItRMLogicalConsumptionMapper.toEntity(getSortedRscItLogicalConsumptionDTOs(rmRscItLogicalConsumptionDTOs)));
        }
    }

    @Override
    public void saveAllUnique(List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOs, Long ppHeaderId) {
        if (rscItRMUniqueLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            rscItRMUniqueLogicalConsumptionRepository.saveAll(rscItRMUniqueLogicalConsumptionMapper.toEntity(getSortedRscItLogicalConsumptionDTOs(rmRscItLogicalConsumptionDTOs)));
        }
    }

    private List<RscItLogicalConsumptionDTO> getSortedRscItLogicalConsumptionDTOs(List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOs) {
        List<RscItLogicalConsumptionDTO> numericItemCodeDTOs = new ArrayList<>();
        List<RscItLogicalConsumptionDTO> alphaNumericItemCodeDTOs = new ArrayList<>();
        for (RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO : rmRscItLogicalConsumptionDTOs) {
            if (isValid(rscItLogicalConsumptionDTO)) {
                numericItemCodeDTOs.add(rscItLogicalConsumptionDTO);
            } else {
                alphaNumericItemCodeDTOs.add(rscItLogicalConsumptionDTO);
            }
        }
        numericItemCodeDTOs.sort(Comparator.comparingLong(obj -> Long.parseLong(obj.getRscMtItemChildCode())));
        alphaNumericItemCodeDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
        numericItemCodeDTOs.addAll(alphaNumericItemCodeDTOs);
        numericItemCodeDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
        return numericItemCodeDTOs;
    }

    private Boolean isValid(RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO) {
        try {
            Long.parseLong(rscItLogicalConsumptionDTO.getRscMtItemChildCode());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItRmLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllLogicalConsumptionDTOsByItemCategory(String itemDescription) {
        return rscItRMLogicalConsumptionMapper.toDto(rscItRmLogicalConsumptionRepository.findAllByRscMtItemChildRscDtItemCategoryDescription(itemDescription));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAll() {
        return rscItRMLogicalConsumptionMapper.toDto(rscItRmLogicalConsumptionRepository.findAll());
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscItRMLogicalConsumptionMapper.toDto(rscItRmLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public ExcelExportFiles getRmLogicalConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO,  Map<Long, GrossConsumptionDTO>  grossConsumptionMap) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmLogicalConsumptionExporter.logicalConsumptionsToExcel(getRMLogicalConsumption(rscMtPPheaderDTO.getId()),grossConsumptionMap, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_LOGICAL_CONSUMPTION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles getBulkLogicalConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, Map<Long, ItemDetailDTO> mpsPlanDetailMap) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmBulkLogicalConsumptionExporter.bulklogicalConsumptionsToExcel(rscItBulkLogicalConsumptionService.getBulkLogicalConsumption(rscMtPPheaderDTO.getId()), mpsPlanDetailMap, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_BULK_LOGICAL_CONSUMPTION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<LogicalConsumptionMainDTO> getRMLogicalConsumption(Long ppHeaderId) {
        return rmLogicalConsumptionMapper.toDto(rscItRMUniqueLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void deleteConsumption(Long ppHeaderId) {
        rscItRMUniqueLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
        rscItRmLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}