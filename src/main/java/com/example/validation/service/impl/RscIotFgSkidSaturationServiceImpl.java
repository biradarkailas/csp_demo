package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.mapper.FgSkidSaturationMapper;
import com.example.validation.mapper.RscIotFgSkidSaturationMapper;
import com.example.validation.mapper.SkidSaturationMapper;
import com.example.validation.repository.RscIotFgSkidSaturationRepository;
import com.example.validation.service.*;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.excel_export.FgSkidSaturationExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotFgSkidSaturationServiceImpl implements RscIotFgSkidSaturationService {
    private final RscIotFgSkidSaturationRepository rscIotFgSkidSaturationRepository;
    private final RscIotFgSkidSaturationMapper rscIotFgSkidSaturationMapper;
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService;
    private final RscMtItemService rscMtItemService;
    private final RscDtSkidsService rscDtSkidsService;
    private final FgSkidSaturationMapper fgSkidSaturationMapper;
    private final SkidSaturationMapper skidSaturationMapper;

    public RscIotFgSkidSaturationServiceImpl(RscIotFgSkidSaturationRepository rscIotFgSkidSaturationRepository, RscIotFgSkidSaturationMapper rscIotFgSkidSaturationMapper, RscMtPpdetailsService rscMtPpdetailsService, RscMtPpheaderService rscMtPpheaderService, RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService, RscMtItemService rscMtItemService, RscDtSkidsService rscDtSkidsService, FgSkidSaturationMapper fgSkidSaturationMapper, SkidSaturationMapper skidSaturationMapper) {
        this.rscIotFgSkidSaturationRepository = rscIotFgSkidSaturationRepository;
        this.rscIotFgSkidSaturationMapper = rscIotFgSkidSaturationMapper;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIitMpsWorkingDaysService = rscIitMpsWorkingDaysService;
        this.rscMtItemService = rscMtItemService;
        this.rscDtSkidsService = rscDtSkidsService;
        this.fgSkidSaturationMapper = fgSkidSaturationMapper;
        this.skidSaturationMapper = skidSaturationMapper;
    }

    @Override
    public void createFgSkidSaturation(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            if (rscIotFgSkidSaturationRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
                List<RscMtPPdetailsDTO> rscMtPPdetailsDTO = rscMtPpdetailsService.findAllPPdetailsByPPheader(ppHeaderId);
                if (Optional.ofNullable(rscMtPPdetailsDTO).isPresent()) {
                    List<RscIotFgSkidSaturationDTO> rscIotFgSkidSaturationDTOS = new ArrayList<>();
                    Double[] monthsTotalRatioOfPlanQtyWithTs = new Double[12];
                    List<Long> rscDtSkidsList = rscMtPpdetailsService.findUniqueRscDtSkids(ppHeaderId);
                    RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO = rscIitMpsWorkingDaysService.findByMpsId(pPheaderDTO.get().getId());
                    rscDtSkidsList.forEach(rscDtSkids -> {
                        for (int i = 0; i < monthsTotalRatioOfPlanQtyWithTs.length; i++) {
                            monthsTotalRatioOfPlanQtyWithTs[i] = 0.0;
                        }
                        List<RscMtPPdetailsDTO> ppDetailsDTOS = rscMtPPdetailsDTO.stream().filter(rscMtPPdetails -> rscMtPPdetails.getRscDtSkidsId().equals(rscDtSkids)).collect(Collectors.toList());
                        Double[] monthsTotalMpsPlanQtyList = getMonthsTotalRatioOfPlanQtyWithTs(monthsTotalRatioOfPlanQtyWithTs, ppDetailsDTOS);
                        RscDtSkidsDTO rscDtSkidsDTO = rscDtSkidsService.findById(rscDtSkids);
                        RscIotFgSkidSaturationDTO rscIotFgLineSaturationDTO = getFgSkidSaturationDetails(monthsTotalMpsPlanQtyList, rscDtSkidsDTO, rscIitMpsWorkingDaysDTO, pPheaderDTO.get().getId());
                        rscIotFgSkidSaturationDTOS.add(rscIotFgLineSaturationDTO);
                    });
                    saveAll(rscIotFgSkidSaturationDTOS);
                }
            }
        }
    }

    @Override
    public void saveAll(List<RscIotFgSkidSaturationDTO> rscIotFgSkidSaturationDTOList) {
        rscIotFgSkidSaturationRepository.saveAll(rscIotFgSkidSaturationMapper.toEntity(rscIotFgSkidSaturationDTOList));
    }

    private RscIotFgSkidSaturationDTO getFgSkidSaturationDetails(Double[] monthsTotalRatioOfPlanQtyWithTs, RscDtSkidsDTO rscDtSkidsDTO, RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO, Long ppHeaderId) {
        RscIotFgSkidSaturationDTO rscIotFgSkidSaturationDTO = new RscIotFgSkidSaturationDTO();
        rscIotFgSkidSaturationDTO.setRscMtPPheaderId(ppHeaderId);
        rscIotFgSkidSaturationDTO.setRscDtSkidsId(rscDtSkidsDTO.getId());
        rscIotFgSkidSaturationDTO.setM1TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[0]));
        rscIotFgSkidSaturationDTO.setM2TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[1]));
        rscIotFgSkidSaturationDTO.setM3TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[2]));
        rscIotFgSkidSaturationDTO.setM4TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[3]));
        rscIotFgSkidSaturationDTO.setM5TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[4]));
        rscIotFgSkidSaturationDTO.setM6TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[5]));
        rscIotFgSkidSaturationDTO.setM7TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[6]));
        rscIotFgSkidSaturationDTO.setM8TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[7]));
        rscIotFgSkidSaturationDTO.setM9TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[8]));
        rscIotFgSkidSaturationDTO.setM10TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[9]));
        rscIotFgSkidSaturationDTO.setM11TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[10]));
        rscIotFgSkidSaturationDTO.setM12TotalRatioOfPlanQtyWithTs(DoubleUtils.getFormattedValue(monthsTotalRatioOfPlanQtyWithTs[11]));
        if (Optional.ofNullable(rscIitMpsWorkingDaysDTO).isPresent()) {
            rscIotFgSkidSaturationDTO.setMonth1Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth1Value()));
            rscIotFgSkidSaturationDTO.setMonth2Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth2Value()));
            rscIotFgSkidSaturationDTO.setMonth3Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth3Value()));
            rscIotFgSkidSaturationDTO.setMonth4Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth4Value()));
            rscIotFgSkidSaturationDTO.setMonth5Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth5Value()));
            rscIotFgSkidSaturationDTO.setMonth6Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth6Value()));
            rscIotFgSkidSaturationDTO.setMonth7Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth7Value()));
            rscIotFgSkidSaturationDTO.setMonth8Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth8Value()));
            rscIotFgSkidSaturationDTO.setMonth9Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth9Value()));
            rscIotFgSkidSaturationDTO.setMonth10Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth10Value()));
            rscIotFgSkidSaturationDTO.setMonth11Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth11Value()));
            rscIotFgSkidSaturationDTO.setMonth12Capacity(getCapacity(rscDtSkidsDTO.getShifts(), rscDtSkidsDTO.getBatchesPerShift(), rscIitMpsWorkingDaysDTO.getMonth12Value()));
            rscIotFgSkidSaturationDTO.setM1SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[0], rscIotFgSkidSaturationDTO.getMonth1Capacity()));
            rscIotFgSkidSaturationDTO.setM2SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[1], rscIotFgSkidSaturationDTO.getMonth2Capacity()));
            rscIotFgSkidSaturationDTO.setM3SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[2], rscIotFgSkidSaturationDTO.getMonth3Capacity()));
            rscIotFgSkidSaturationDTO.setM4SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[3], rscIotFgSkidSaturationDTO.getMonth4Capacity()));
            rscIotFgSkidSaturationDTO.setM5SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[4], rscIotFgSkidSaturationDTO.getMonth5Capacity()));
            rscIotFgSkidSaturationDTO.setM6SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[5], rscIotFgSkidSaturationDTO.getMonth6Capacity()));
            rscIotFgSkidSaturationDTO.setM7SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[6], rscIotFgSkidSaturationDTO.getMonth7Capacity()));
            rscIotFgSkidSaturationDTO.setM8SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[7], rscIotFgSkidSaturationDTO.getMonth8Capacity()));
            rscIotFgSkidSaturationDTO.setM9SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[8], rscIotFgSkidSaturationDTO.getMonth9Capacity()));
            rscIotFgSkidSaturationDTO.setM10SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[9], rscIotFgSkidSaturationDTO.getMonth10Capacity()));
            rscIotFgSkidSaturationDTO.setM11SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[10], rscIotFgSkidSaturationDTO.getMonth11Capacity()));
            rscIotFgSkidSaturationDTO.setM12SkidSaturation(getSkidSaturation(monthsTotalRatioOfPlanQtyWithTs[11], rscIotFgSkidSaturationDTO.getMonth12Capacity()));
        }
        return rscIotFgSkidSaturationDTO;
    }

    private Double getCapacity(Double shifts, Double batchPerShift, Double monthValue) {
        if (Optional.ofNullable(shifts).isPresent() && Optional.ofNullable(batchPerShift).isPresent() && Optional.ofNullable(monthValue).isPresent()) {
            return DoubleUtils.getFormattedValue(shifts * batchPerShift * monthValue);
        } else {
            return 0.0;
        }
    }

    private Double getSkidSaturation(Double monthValue, Double capacity) {
        if (capacity != 0) {
            return DoubleUtils.getFormattedValue((monthValue / capacity) * 100);
        } else {
            return 0.0;
        }
    }

    private Double[] getMonthsTotalRatioOfPlanQtyWithTs(Double[] monthsTotalRatioOfPlanQtyWithTs, List<RscMtPPdetailsDTO> ppdetailsDTOS) {
        for (RscMtPPdetailsDTO pdetailsDTO : ppdetailsDTOS) {
            RscMtItemDTO rscMtItemDTO = rscMtItemService.findById(pdetailsDTO.getRscMtItemId());
            if (rscMtItemDTO.getSeriesValue() != null) {
                monthsTotalRatioOfPlanQtyWithTs[0] += getPlanQtyWithTs(pdetailsDTO.getM1Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[1] += getPlanQtyWithTs(pdetailsDTO.getM2Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[2] += getPlanQtyWithTs(pdetailsDTO.getM3Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[3] += getPlanQtyWithTs(pdetailsDTO.getM4Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[4] += getPlanQtyWithTs(pdetailsDTO.getM5Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[5] += getPlanQtyWithTs(pdetailsDTO.getM6Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[6] += getPlanQtyWithTs(pdetailsDTO.getM7Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[7] += getPlanQtyWithTs(pdetailsDTO.getM8Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[8] += getPlanQtyWithTs(pdetailsDTO.getM9Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[9] += getPlanQtyWithTs(pdetailsDTO.getM10Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[10] += getPlanQtyWithTs(pdetailsDTO.getM11Value(), rscMtItemDTO.getSeriesValue());
                monthsTotalRatioOfPlanQtyWithTs[11] += getPlanQtyWithTs(pdetailsDTO.getM12Value(), rscMtItemDTO.getSeriesValue());
            }
        }
        return monthsTotalRatioOfPlanQtyWithTs;
    }

    private Double getPlanQtyWithTs(Double monthValue, Double seriesValue) {
        if (seriesValue != 0) {
            return (DoubleUtils.getFormattedValue(monthValue / seriesValue));
        } else {
            return 0.0;
        }
    }

    @Override
    public FgSkidSaturationDetailsDTO getAllFgSkidSaturation(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            return getFgSkidSaturationDetailsDTO(pPheaderDTO.get());
        }
        return null;
    }

    private FgSkidSaturationDetailsDTO getFgSkidSaturationDetailsDTO(RscMtPPheaderDTO rscMtPPheaderDTO) {
        FgSkidSaturationDetailsDTO fgSkidSaturationDetailsDTO = new FgSkidSaturationDetailsDTO();
        fgSkidSaturationDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
        fgSkidSaturationDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
        fgSkidSaturationDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
        RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO = rscIitMpsWorkingDaysService.findByMpsId(rscMtPPheaderDTO.getId());
        if (Optional.ofNullable(rscIitMpsWorkingDaysDTO).isPresent()) {
            fgSkidSaturationDetailsDTO.setRscIitMpsWorkingDaysDTO(rscIitMpsWorkingDaysDTO);
        }
        List<FgSkidSaturationDTO> rscIotFgSkidSaturationList = fgSkidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findAllByRscMtPPheaderId(rscMtPPheaderDTO.getId()));
        if (Optional.ofNullable(rscIotFgSkidSaturationList).isPresent()) {
            fgSkidSaturationDetailsDTO.setFgSkidSaturationDTO(rscIotFgSkidSaturationList);
        }
        return fgSkidSaturationDetailsDTO;
    }

    @Override
    public ExcelExportFiles fgSkidExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        FgSkidSaturationDetailsDTO fgLineSaturationDetailsDTO = getFgSkidSaturationDetailsDTO(rscMtPPheaderDTO);
        excelExportFiles.setFile(FgSkidSaturationExporter.skidSaturationToExcel(fgLineSaturationDetailsDTO, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.FG_SKID_SATURATION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public SkidsDetailsDTO getAllFgSkids(Long ppHeaderId) {
        SkidsDetailsDTO skidsDetailsDTO = new SkidsDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            skidsDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
            skidsDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            skidsDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            skidsDetailsDTO.setSkidsDTOList(getAllFgSkidSaturationDTO(rscMtPPheaderDTO.getId()));
        }
        return skidsDetailsDTO;
    }


    private List<SkidsDTO> getAllFgSkidSaturationDTO(Long ppHeaderId) {
        return skidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public FgSkidDetailsDTO getFgSkidDetails(Long ppHeaderId, Long rscDtSkidId) {
        FgSkidDetailsDTO skidDetailsDTO = new FgSkidDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            skidDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
            skidDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            skidDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            skidDetailsDTO.setFgSkidSaturationDTO(fgSkidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findByRscMtPPheaderIdAndRscDtSkidsId(ppHeaderId, rscDtSkidId)));
        }
        return skidDetailsDTO;
    }

    @Override
    public List<RscIotFgSkidSaturationDTO> findAll(Long ppHeaderId) {
        return rscIotFgSkidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscIotFgSkidSaturationDTO> findAllBySkidsIdAndRscMtPPheaderId(Long rscDtSkidId, Long ppHeaderId) {
        return rscIotFgSkidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findAllByRscDtSkidsIdAndRscMtPPheaderId(rscDtSkidId, ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFgSkidSaturationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

  /*  @Override
    public List<SkidsDTO> getAllSkids() {
return null;
    }*/


    @Override
    public List<SkidsDTO> getAllSkids() {
        List<SkidsDTO> skidsDTOList = skidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findAllByRscMtPPheaderRscDtTagName("VD"));
        return getLinesWhoseSaturationPresentForAllMonths(skidsDTOList);
    }

    private List<SkidsDTO> getLinesWhoseSaturationPresentForAllMonths(List<SkidsDTO> UniqueSkidsDTOList) {
        List<SkidsDTO> skidsDTOList = skidSaturationMapper.toDto(rscIotFgSkidSaturationRepository.findAllByRscMtPPheaderRscDtTagName("VD"));
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllByActiveAndIsValidated();
        HashSet<SkidsDTO> skidDTOS = new HashSet<>();
        UniqueSkidsDTOList.forEach(skidsDTO -> {
            if (getCountOfValidatedSaturationsForLine(skidsDTO.getId(), skidsDTOList) == pPheaderDTOList.size()) {
                skidDTOS.add(skidsDTO);
            }
        });
        return new ArrayList<>(skidDTOS);
    }

    private Long getCountOfValidatedSaturationsForLine(Long id, List<SkidsDTO> skidsDTOList) {
        return skidsDTOList.stream().filter(skidsDTO -> skidsDTO.getId().equals(id)).count();
    }
}