package com.example.validation.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.time.LocalDate;
import java.util.Scanner;

@Service
public class CustomLicenceValidityService {
    @Autowired
    FileUploadService fileUploadService;

    public Integer uploadLicence(MultipartFile file, HttpSession session) {
        return fileUploadService.uploadLicence(file, session);
    }

    private File getValidLicenceFile(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            System.out.println(children);
            return new File("LICENCE" +
                    (System.getProperty("os.name").indexOf("Win") >= 0 ? "\\" : "/") + children[0]);
        }
        return null;
    }

    public Integer hasValidLicence(StringBuilder keySb) {
        try {
            LocalDate ld = LocalDate.now();
            keySb.append(':');
            keySb.append(appendDate(ld));
            File myObj;
            Scanner myReader;
            try {
                myObj = getValidLicenceFile(new File("LICENCE"));
                myReader = new Scanner(myObj);
            } catch (Exception ex) {
                return -2;
            }
            int count = -1;
            boolean enableCount = false;
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                if (checkValidKey(data, keySb))
                    enableCount = true;
                if (enableCount)
                    count++;
            }
            myReader.close();
            return count;
        } catch (Exception ex) {
            return -1;
        }
    }

    private boolean checkValidKey(String data, StringBuilder keySb) {
        try {
            MessageDigest md = MessageDigest.getInstance("SHA-256");
            BigInteger number = new BigInteger(1, md.digest(keySb.toString().getBytes(StandardCharsets.UTF_8)));
            StringBuilder hexString = new StringBuilder(number.toString(16));
            while (hexString.length() < 32) {
                hexString.insert(0, '0');
            }
            if (data.startsWith(hexString.toString())){
                return true;
            } else {
                return false;
            }
        } catch (Exception ex){
            return false;
        }
    }

    private StringBuilder appendDate(LocalDate tempDate) {
        StringBuilder sb = new StringBuilder();
        sb.append(tempDate.getYear());
        if (tempDate.getMonthValue() < 10) {
            sb.append(0);
            sb.append(tempDate.getMonthValue());
        } else
            sb.append(tempDate.getMonthValue());
        if (tempDate.getDayOfMonth() < 10) {
            sb.append(0);
            sb.append(tempDate.getDayOfMonth());
        } else
            sb.append(tempDate.getDayOfMonth());
        return sb;
    }
}
