package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.PmSlobMapper;
import com.example.validation.mapper.RscItPMSlobMapper;
import com.example.validation.repository.RscItPMSlobRepository;
import com.example.validation.service.*;
import com.example.validation.util.*;
import com.example.validation.util.enums.SlobType;
import com.example.validation.util.excel_export.*;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscItPMSlobServiceImpl implements RscItPMSlobService {
    private final RscItPMSlobRepository rscItPMSlobRepository;
    private final RscItPMSlobMapper rscItPMSlobMapper;
    private final RscItPMStockService rscItPMStockService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final PmSlobMapper pmSlobMapper;
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final RscItItemWiseSupplierService rscItItemWiseSupplierService;
    private final RscIotPmSlobSummaryService rscIotPmSlobSummaryService;
    private final RscIotPmCoverDaysService rscIotPmCoverDaysService;
    private final RscDtReasonService rscDtReasonService;
    private final RscIotPmLatestSlobReasonService rscIotPmLatestSlobReasonService;
    private final SlobSummaryUtil slobSummaryUtil;
    private final SlobMovementsUtil slobMovementsUtil;
    private final SlobInventoryQualityIndexUtil slobInventoryQualityIndexUtil;

    public RscItPMSlobServiceImpl(RscItPMSlobRepository rscItPMSlobRepository, RscItPMSlobMapper rscItPMSlobMapper, RscItPMStockService rscItPMStockService, RscMtPpheaderService rscMtPpheaderService, PmSlobMapper pmSlobMapper, RscItPMGrossConsumptionService rscItPMGrossConsumptionService, RscItItemWiseSupplierService rscItItemWiseSupplierService, RscIotPmSlobSummaryService rscIotPmSlobSummaryService, @Lazy RscIotPmCoverDaysService rscIotPmCoverDaysService, RscDtReasonService rscDtReasonService, RscIotPmLatestSlobReasonService rscIotPmLatestSlobReasonService, SlobSummaryUtil slobSummaryUtil, SlobMovementsUtil slobMovementsUtil, SlobInventoryQualityIndexUtil slobInventoryQualityIndexUtil) {
        this.rscItPMSlobRepository = rscItPMSlobRepository;
        this.rscItPMSlobMapper = rscItPMSlobMapper;
        this.rscItPMStockService = rscItPMStockService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.pmSlobMapper = pmSlobMapper;
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.rscItItemWiseSupplierService = rscItItemWiseSupplierService;
        this.rscIotPmSlobSummaryService = rscIotPmSlobSummaryService;
        this.rscIotPmCoverDaysService = rscIotPmCoverDaysService;
        this.rscDtReasonService = rscDtReasonService;
        this.rscIotPmLatestSlobReasonService = rscIotPmLatestSlobReasonService;
        this.slobSummaryUtil = slobSummaryUtil;
        this.slobMovementsUtil = slobMovementsUtil;
        this.slobInventoryQualityIndexUtil = slobInventoryQualityIndexUtil;
    }

    @Override
    public void calculateRscItPMSlob(Long rscMtPPHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            if (rscItPMSlobRepository.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
                List<String> stockTypeList = new ArrayList<>();
                stockTypeList.add(MaterialCategoryConstants.STOCK_TYPE_TAG_PM_MES);
                stockTypeList.add(MaterialCategoryConstants.STOCK_TYPE_TAG_PM_REJECTION);
                List<RscItPMStockDTO> rscIotPmStockDtoList = rscItPMStockService.findAllRscItPmStockByStockTypeTag(stockTypeList);
                if (Optional.ofNullable(rscIotPmStockDtoList).isPresent()) {
                    HashMap<Long, RscItPMStockDTO> mesMap = new HashMap<>();
                    HashMap<Long, RscItPMStockDTO> rejectionMap = new HashMap<>();
                    List<RscIotPmLatestSlobReasonDTO> rscIotPmLatestSlobReasonDTOS = rscIotPmLatestSlobReasonService.findAll();
                    Set<Long> uniqueItemId = new HashSet<>();
                    for (RscItPMStockDTO rscItPMStockDTO : rscIotPmStockDtoList) {
                        if (!rscItPMStockDTO.getItemCode().startsWith("SF")) {
                            if (rscItPMStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_PM_MES) && DateUtils.isEqualMonthAndYear(rscItPMStockDTO.getStockDate(), rscMtPPheaderDTO.get().getMpsDate())) {
                                mesMap.put(rscItPMStockDTO.getRscMtItemId(), rscItPMStockDTO);
                            } else if (DateUtils.isEqualMonthAndYear(rscItPMStockDTO.getStockDate(), rscMtPPheaderDTO.get().getMpsDate())) {
                                rejectionMap.put(rscItPMStockDTO.getRscMtItemId(), rscItPMStockDTO);
                            }
                            if (DateUtils.isEqualMonthAndYear(rscItPMStockDTO.getStockDate(), rscMtPPheaderDTO.get().getMpsDate())) {
                                uniqueItemId.add(rscItPMStockDTO.getRscMtItemId());
                            }
                        }
                    }
                    calculateRscItPMSlobData(mesMap, rejectionMap, uniqueItemId, rscMtPPHeaderId, rscIotPmLatestSlobReasonDTOS);
                }
            }
        }
    }

    private void calculateRscItPMSlobData(HashMap<Long, RscItPMStockDTO> mesMap, HashMap<Long, RscItPMStockDTO> rejectionMap, Set<Long> uniqueItemIds, Long ppHeaderId, List<RscIotPmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS) {
        Double price;
        Double stockQuantity;
        Double mesStockQuantity;
        Double rejectionStockQuantity;
        Double slobQuantity;
        List<RscItPMSlobDTO> rscItPMSlobDTOList = new ArrayList<>();
        for (Long uniqueItemId : uniqueItemIds) {
            mesStockQuantity = 0.0;
            rejectionStockQuantity = 0.0;
            slobQuantity = null;
            price = null;
            RscItPMSlobDTO rscItPMSlobDTO = new RscItPMSlobDTO();
            rscItPMSlobDTO.setRscMtPPHeaderId(ppHeaderId);
            rscItPMSlobDTO.setRscMtItemId(uniqueItemId);
            if (Optional.ofNullable(rscIotRmLatestSlobReasonDTOS).isPresent()) {
                rscItPMSlobDTO.setRscDtReasonId(getReasonIdFromSlobReason(uniqueItemId, rscIotRmLatestSlobReasonDTOS));
            }
            if (Optional.ofNullable(mesMap.get(uniqueItemId)).isPresent()) {
                if (mesMap.get(uniqueItemId).getItemCode() != null) {
                    rscItPMSlobDTO.setRscDtItemCode(mesMap.get(uniqueItemId).getItemCode());
                }
                if (mesMap.get(uniqueItemId).getStdPrice() != null) {
                    rscItPMSlobDTO.setStdPrice(mesMap.get(uniqueItemId).getStdPrice());
                    price = mesMap.get(uniqueItemId).getStdPrice();
                }
                if (mesMap.get(uniqueItemId).getMapPrice() != null) {
                    rscItPMSlobDTO.setMapPrice(mesMap.get(uniqueItemId).getMapPrice());
                    price = mesMap.get(uniqueItemId).getMapPrice();
                }
                if (Optional.ofNullable(mesMap.get(uniqueItemId).getQuantity()).isPresent()) {
                    mesStockQuantity = mesMap.get(uniqueItemId).getQuantity();
                    rscItPMSlobDTO.setMidNightStockQuantity(mesStockQuantity);
                }
            }
            if (Optional.ofNullable(rejectionMap.get(uniqueItemId)).isPresent()) {
                if (rejectionMap.get(uniqueItemId).getItemCode() != null) {
                    rscItPMSlobDTO.setRscDtItemCode(rejectionMap.get(uniqueItemId).getItemCode());
                }
                if (Optional.ofNullable(rejectionMap.get(uniqueItemId).getQuantity()).isPresent()) {
                    rejectionStockQuantity = rejectionMap.get(uniqueItemId).getQuantity();
                }
                if (rejectionMap.get(uniqueItemId).getStdPrice() != null) {
                    rscItPMSlobDTO.setStdPrice(rejectionMap.get(uniqueItemId).getStdPrice());
                    price = rejectionMap.get(uniqueItemId).getStdPrice();
                }
                if (rejectionMap.get(uniqueItemId).getMapPrice() != null) {
                    rscItPMSlobDTO.setMapPrice(rejectionMap.get(uniqueItemId).getMapPrice());
                    price = rejectionMap.get(uniqueItemId).getMapPrice();
                }
            }
            rscItPMSlobDTO.setPrice(price);
            stockQuantity = mesStockQuantity + rejectionStockQuantity;
            rscItPMSlobDTO.setStockQuantity(stockQuantity);
            RscItGrossConsumptionDTO pmGrossConsumptionDto = rscItPMGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, uniqueItemId);
            RscItItemWiseSupplierDTO oneByRscMtItemIdAndAllocation = rscItItemWiseSupplierService.findOneByRscMtItemIdAndAllocation(uniqueItemId);
            if (Optional.ofNullable(pmGrossConsumptionDto).isPresent()) {
                if (pmGrossConsumptionDto.getTotalValue() != null) {
                    rscItPMSlobDTO.setTotalConsumptionQuantity(DoubleUtils.getFormattedValue(pmGrossConsumptionDto.getTotalValue()));
                }
                if (Optional.ofNullable(oneByRscMtItemIdAndAllocation).isPresent()) {
                    if (oneByRscMtItemIdAndAllocation.getId() != null) {
                        rscItPMSlobDTO.setRscItItemWiseSupplierId(oneByRscMtItemIdAndAllocation.getId());
                    }
                }
                if ((stockQuantity - pmGrossConsumptionDto.getTotalValue()) > 0) {
                    slobQuantity = stockQuantity - pmGrossConsumptionDto.getTotalValue();
                    rscItPMSlobDTO.setSlobType(SlobType.SL);
                }
            } else {
                slobQuantity = stockQuantity;
                rscItPMSlobDTO.setSlobType(SlobType.OB);
            }
            if (slobQuantity != null) {
                rscItPMSlobDTO.setSlobQuantity(DoubleUtils.round(slobQuantity));
                if (price != null) {
                    rscItPMSlobDTO.setSlobValue(DoubleUtils.round(slobQuantity * price));
                }
            }
            if (price != null) {
                rscItPMSlobDTO.setStockValue(DoubleUtils.round(stockQuantity * price));
            }
            rscItPMSlobDTOList.add(rscItPMSlobDTO);
        }
        saveAll(rscItPMSlobDTOList);
    }

    private Long getReasonIdFromSlobReason(Long rscMtItemId, List<RscIotPmLatestSlobReasonDTO> rscIotPmLatestSlobReasonDTOS) {
        List<RscIotPmLatestSlobReasonDTO> iotPmLatestSlobReasonDTO = rscIotPmLatestSlobReasonDTOS.stream().filter(rscIotPmLatestSlobReasonDTO -> rscIotPmLatestSlobReasonDTO.getRscMtItemId().equals(rscMtItemId)).collect(Collectors.toList());
        if (iotPmLatestSlobReasonDTO.size() == 0) {
            return null;
        } else {
            RscIotPmLatestSlobReasonDTO rscIotPmLatestSlobReasonDTO = iotPmLatestSlobReasonDTO.get(0);
            return rscIotPmLatestSlobReasonDTO.getRscDtReasonId();
        }
    }

    @Override
    public void calculateRscItPMSlobSummary(Long rscMtPPHeaderId, Double sitValue1) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        if (pPheaderDTO.isPresent()) {
            if (rscIotPmSlobSummaryService.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
                RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO = new RscIotPmSlobSummaryDTO();
                List<RscItPMSlobDTO> rscItPMSlobList = getRscItPMSlobList(rscMtPPHeaderId);
                if (Optional.ofNullable(rscItPMSlobList).isPresent()) {
                    Double totalStockValue = 0.0;
                    Double sitValue;
                    if (Optional.ofNullable(sitValue1).isPresent()) {
                        sitValue = sitValue1;
                    } else {
                        sitValue = 0.0;
                    }
                    Double totalStockSitValue;
                    Double totalSlobValue;
                    Double totalSlowItemValue = 0.0;
                    Double totalObsoleteItemValue = 0.0;
                    Double slowItemPercentage;
                    Double obsoleteItemPercentage;
                    Double stockQualityPercentage;
                    Double stockQuantity;
                    Double slobSum = 0.0;
                    for (RscItPMSlobDTO rscItPMSlob : rscItPMSlobList) {
                        if (rscItPMSlob.getStockValue() != null) {
                            totalStockValue += rscItPMSlob.getStockValue();
                        }
                        if (isValidReasonSlob(rscItPMSlob)) {
                              /* if (rscItPMSlob.getStockValue() != null) {
                                totalStockValue += rscItPMSlob.getStockValue();
                            }*/
                            if (rscItPMSlob.getSlobValue() != null) {
                                slobSum += rscItPMSlob.getSlobValue();
                            }
                            if (Optional.ofNullable(rscItPMSlob.getSlobType()).isPresent()) {
                                if (rscItPMSlob.getSlobType().equals(SlobType.SL)) {
                                    if (rscItPMSlob.getSlobValue() != null) {
                                        totalSlowItemValue += rscItPMSlob.getSlobValue();
                                    }
                                } else if (rscItPMSlob.getSlobType().equals(SlobType.OB)) {
                                    if (rscItPMSlob.getSlobValue() != null) {
                                        totalObsoleteItemValue += rscItPMSlob.getSlobValue();
                                    }
                                }
                            }
                        }
                    }
                    rscIotPmSlobSummaryDTO.setRscMtPPheaderId(rscMtPPHeaderId);
                    totalStockSitValue = totalStockValue + sitValue;
                    totalSlobValue = slobSum;
                    slowItemPercentage = (totalSlowItemValue / totalStockSitValue) * 100;
                    obsoleteItemPercentage = (totalObsoleteItemValue / totalStockSitValue) * 100;
                    stockQualityPercentage = 100 - (slowItemPercentage + obsoleteItemPercentage);
                    stockQuantity = (totalStockSitValue * stockQualityPercentage) / 100;
                    rscIotPmSlobSummaryDTO.setTotalStockValue(DoubleUtils.round(totalStockValue));
                    rscIotPmSlobSummaryDTO.setSitValue(DoubleUtils.getFormattedValue(sitValue));
                    rscIotPmSlobSummaryDTO.setTotalStockSitValue(DoubleUtils.round(totalStockSitValue));
                    rscIotPmSlobSummaryDTO.setTotalSlobValue(DoubleUtils.round(totalSlobValue));
                    rscIotPmSlobSummaryDTO.setTotalSlowItemValue(DoubleUtils.round(totalSlowItemValue));
                    rscIotPmSlobSummaryDTO.setTotalObsoleteItemValue(DoubleUtils.round(totalObsoleteItemValue));
                    rscIotPmSlobSummaryDTO.setSlowItemPercentage(DoubleUtils.getFormattedValue(slowItemPercentage));
                    rscIotPmSlobSummaryDTO.setObsoleteItemPercentage(DoubleUtils.getFormattedValue(obsoleteItemPercentage));
                    rscIotPmSlobSummaryDTO.setStockQualityPercentage(DoubleUtils.getFormattedValue(stockQualityPercentage));
                    rscIotPmSlobSummaryDTO.setStockQuantity(DoubleUtils.round(stockQuantity));
                }
                rscIotPmSlobSummaryService.save(rscIotPmSlobSummaryDTO);
            }
        }
    }

    private boolean isValidReasonSlob(RscItPMSlobDTO rscItPMSlob) {
        if (Optional.ofNullable(rscItPMSlob.getRscDtReasonId()).isPresent() && Optional.ofNullable(rscItPMSlob.getRscDtReasonName()).isPresent()) {
            if (rscItPMSlob.getRscDtReasonName().equals(MaterialCategoryConstants.PM_BOM_REASON_TAG)) {
                return false;
            }
        }
        return true;
    }

    private List<RscItPMSlobDTO> getRscItPMSlobList(Long rscMtPPHeaderId) {
        return rscItPMSlobMapper.toDto(rscItPMSlobRepository.findAllByRscMtPPheaderId(rscMtPPHeaderId));
    }

    @Override
    public List<RscItPMSlobDTO> getRscItPMSlobByRscMTPPHeader(Long rscMtPpHeaderId) {
        return rscItPMSlobMapper.toDto(rscItPMSlobRepository.findAllByRscMtPPheaderId(rscMtPpHeaderId));
    }

    @Override
    public void saveAll(List<RscItPMSlobDTO> rscItPMSlobDTOList) {
        rscItPMSlobDTOList.sort(Comparator.comparing(RscItPMSlobDTO::getRscDtItemCode));
        rscItPMSlobRepository.saveAll(rscItPMSlobMapper.toEntity(rscItPMSlobDTOList));
    }

    @Override
    public PMSlobDetailsDTO getAllRscItPmSlob(Long rscMtPPHeaderId) {
        PMSlobDetailsDTO PMSlobDetails = new PMSlobDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            PMSlobDetails.setMpsId(rscMtPPheaderDTO.get().getId());
            PMSlobDetails.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
            PMSlobDetails.setMpsName(rscMtPPheaderDTO.get().getMpsName());
            RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(rscMtPPHeaderId);
            if (Optional.ofNullable(rscIotPmSlobSummaryDTO).isPresent()) {
                PMSlobDetails.setTotalStockValue(rscIotPmSlobSummaryDTO.getTotalStockValue());
                PMSlobDetails.setTotalSlowItemValue(rscIotPmSlobSummaryDTO.getTotalSlowItemValue());
                PMSlobDetails.setTotalObsoleteItemValue(rscIotPmSlobSummaryDTO.getTotalObsoleteItemValue());
                PMSlobDetails.setSitValue(rscIotPmSlobSummaryDTO.getSitValue());
            }
            List<PmSlobDTO> rscItPMSlobList = pmSlobMapper.toDto(rscItPMSlobRepository.findAllByRscMtPPheaderId(rscMtPPHeaderId));
            List<PmSlobDTO> pmSlobsList = new ArrayList<>();
            if (Optional.ofNullable(rscItPMSlobList).isPresent()) {
                pmSlobsList.addAll(rscItPMSlobList);
            }
            PMSlobDetails.setPmSlobs(pmSlobsList);
        }
        return PMSlobDetails;
    }

    @Override
    public PMSlobDetailsDTO getNullMapPriceDetails(Long rscMtPPHeaderId) {
        PMSlobDetailsDTO PMSlobDetails = new PMSlobDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            PMSlobDetails.setMpsId(rscMtPPheaderDTO.get().getId());
            PMSlobDetails.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
            PMSlobDetails.setMpsName(rscMtPPheaderDTO.get().getMpsName());
            PMSlobDetails.setPmSlobs(getPmSlobDetails(rscMtPPHeaderId));
        }
        return PMSlobDetails;
    }

    private List<PmSlobDTO> getPmSlobDetails(Long rscMtPPHeaderId) {
        List<PmSlobDTO> rscItPMSlobList = pmSlobMapper.toDto(rscItPMSlobRepository.findAllByRscMtPPheaderIdAndPriceIsNull(rscMtPPHeaderId));
        List<PmSlobDTO> pmSlobsList = new ArrayList<>();
        if (Optional.ofNullable(rscItPMSlobList).isPresent()) {
            pmSlobsList.addAll(rscItPMSlobList);
        }
        return pmSlobsList;
    }

    @Override
    public SlobTotalConsumptionDetailsDTO getTotalConsumption(Long ppHeaderId, Long rscMtItemId) {
        return rscItPMGrossConsumptionService.getPmSlobTotalConsumptionDetailsDTO(ppHeaderId, rscMtItemId);
    }

    @Override
    public SlobSummaryDetailsDTO getPmSlobSummaryDetails(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return getSlobSummaryDetailsDTO(rscMtPPheaderDTO.get());
        }
        return null;
    }

    private SlobSummaryDetailsDTO getSlobSummaryDetailsDTO(RscMtPPheaderDTO rscMtPPheaderDTO) {
        SlobSummaryDetailsDTO pmSlobSummaryDetails = new SlobSummaryDetailsDTO();
        pmSlobSummaryDetails.setMpsId(rscMtPPheaderDTO.getId());
        pmSlobSummaryDetails.setMpsDate(rscMtPPheaderDTO.getMpsDate());
        pmSlobSummaryDetails.setMpsName(rscMtPPheaderDTO.getMpsName());
        RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
        if (Optional.ofNullable(rscIotPmSlobSummaryDTO).isPresent()) {
            SlobSummaryFormulaDetailsDTO pmSlobSummaryFormulaDetails = new SlobSummaryFormulaDetailsDTO();
            pmSlobSummaryFormulaDetails.setTotalStockSitValueDTO(setTotalStockSitValue(rscIotPmSlobSummaryDTO));
            pmSlobSummaryFormulaDetails.setTotalSlowItemValueDTO(setTotalSlowItemValue(rscIotPmSlobSummaryDTO));
            pmSlobSummaryFormulaDetails.setTotalObsoleteItemValueDTO(setTotalObsoleteItemValue(rscIotPmSlobSummaryDTO));
            pmSlobSummaryFormulaDetails.setTotalStockQualityValueDTO(setTotalStockQualityValue(rscIotPmSlobSummaryDTO));
            pmSlobSummaryDetails.setSlobSummaryFormulaDetailsDTO(pmSlobSummaryFormulaDetails);
        }
        return pmSlobSummaryDetails;
    }

    private TotalStockSitValueDTO setTotalStockSitValue(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO) {
        TotalStockSitValueDTO totalStockSitValueDTO = new TotalStockSitValueDTO();
        totalStockSitValueDTO.setTotalStockSitValueFormula(MaterialCategoryConstants.TOTAL_STOCK_SIT_VALUE_FORMULA);
        totalStockSitValueDTO.setTotalStockSitFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getTotalStockSitValue()) + " = " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getSitValue()) + " + " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalStockValue()));
        totalStockSitValueDTO.setTotalStockSitValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getTotalStockSitValue()));
        return totalStockSitValueDTO;
    }

    private TotalSlowItemValueDTO setTotalSlowItemValue(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO) {
        TotalSlowItemValueDTO totalSlowItemValueDTO = new TotalSlowItemValueDTO();
        totalSlowItemValueDTO.setTotalSlowItemValueFormula(MaterialCategoryConstants.TOTAL_SLOW_ITEM_VALUE_FORMULA);
        totalSlowItemValueDTO.setTotalSlowItemFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getTotalSlowItemValue()) + " = " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalSlowItemValue()));
        totalSlowItemValueDTO.setTotalSlowItemValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getTotalSlowItemValue()));
        totalSlowItemValueDTO.setSlowItemPercentageFormula(MaterialCategoryConstants.SLOW_ITEM_PERCENTAGE_FORMULA);
        totalSlowItemValueDTO.setSlowItemPercentageFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getSlowItemPercentage()) + " = " + " ( " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalSlowItemValue()) + " / " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalStockSitValue()) + " ) " + " * " + 100);
        totalSlowItemValueDTO.setSlowItemPercentage(DoubleUtils.round(rscIotPmSlobSummaryDTO.getSlowItemPercentage()));
        return totalSlowItemValueDTO;
    }

    private TotalObsoleteItemValueDTO setTotalObsoleteItemValue(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO) {
        TotalObsoleteItemValueDTO totalObsoleteItemValueDTO = new TotalObsoleteItemValueDTO();
        totalObsoleteItemValueDTO.setTotalObsoleteItemValueFormula(MaterialCategoryConstants.TOTAL_OBSOLETE_ITEM_VALUE_FORMULA);
        totalObsoleteItemValueDTO.setTotalObsoleteItemFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getTotalObsoleteItemValue()) + " = " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalObsoleteItemValue()));
        totalObsoleteItemValueDTO.setTotalObsoleteItemValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getTotalObsoleteItemValue()));
        totalObsoleteItemValueDTO.setObsoleteItemPercentageFormula(MaterialCategoryConstants.OBSOLETE_ITEM_PERCENTAGE_FORMULA);
        totalObsoleteItemValueDTO.setObsoleteItemPercentageFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getObsoleteItemPercentage()) + " = " + " ( " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalObsoleteItemValue()) + " / " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalStockSitValue()) + " ) " + " * " + 100);
        totalObsoleteItemValueDTO.setObsoleteItemPercentage(DoubleUtils.round(rscIotPmSlobSummaryDTO.getObsoleteItemPercentage()));
        return totalObsoleteItemValueDTO;
    }

    private TotalStockQualityValueDTO setTotalStockQualityValue(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO) {
        TotalStockQualityValueDTO totalStockQualityValueDTO = new TotalStockQualityValueDTO();
        totalStockQualityValueDTO.setTotalStockQualityValueFormula(MaterialCategoryConstants.TOTAL_STOCK_QUALITY_VALUE_FORMULA);
        totalStockQualityValueDTO.setTotalStockQualityFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getStockQuantity()) + " = " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getTotalStockSitValue()) + " * " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getStockQualityPercentage()));
        totalStockQualityValueDTO.setStockQuantity(DoubleUtils.round(rscIotPmSlobSummaryDTO.getStockQuantity()));
        totalStockQualityValueDTO.setStockQualityPercentageFormula(MaterialCategoryConstants.STOCK_QUALITY_PERCENTAGE_FORMULA);
        totalStockQualityValueDTO.setStockQualityPercentageFormulaValue(DoubleUtils.round(rscIotPmSlobSummaryDTO.getStockQualityPercentage()) + " = " + 100 + " - " + " ( " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getSlowItemPercentage()) + " + " + DoubleUtils.getFormattedValue(rscIotPmSlobSummaryDTO.getObsoleteItemPercentage()) + " )");
        totalStockQualityValueDTO.setStockQualityPercentage(DoubleUtils.round(rscIotPmSlobSummaryDTO.getStockQualityPercentage()));
        return totalStockQualityValueDTO;
    }

  /*  @Override
    public List<PmSlobDTO> saveAllPmSlobDetails(List<PmSlobDTO> pmSlobsLists) {
        List<RscItPMSlob> pmSlobList = new ArrayList<>();
        pmSlobsLists.forEach(pmSlob -> {
            Double mapPrice = null;
            if (pmSlob.getMap() != null) {
                mapPrice = pmSlob.getMap();
                pmSlob.setMap(pmSlob.getMap());
                pmSlob.setPrice(pmSlob.getMap());
            }
            if (mapPrice != null) {
                if (pmSlob.getSlobQuantity() != null) {
                    pmSlob.setSlobValue(DoubleUtils.round(pmSlob.getSlobQuantity() * mapPrice));
                }
                pmSlob.setValue(DoubleUtils.round(pmSlob.getStock() * mapPrice));
            }
            pmSlobList.add(pmSlobMapper.toEntity(pmSlob));
        });
        rscItPMSlobRepository.saveAll(pmSlobList);
        return pmSlobsLists;
    }*/

    @Override
    public ExcelExportFiles exportPmSlobExporter(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        PMSlobDetailsDTO pmSlobDetailsDTO = getAllRscItPmSlob(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(PmSlobExporter.pmSlobToExcelList(pmSlobDetailsDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_SLOB + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    public ExcelExportFiles pmCoverDaysSummaryExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobSummaryDetailsDTO pmSlobSummaryDetails = getSlobSummaryDetailsDTO(rscMtPPheaderDTO);
        CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO = rscIotPmCoverDaysService.getPmCoverDaysSummaryAllDetails(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(PmCoverDaysSummaryExporter.pmCoverDaysSummaryToExcel(pmSlobSummaryDetails, coverDaysSummaryDetailsDTO, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_SLOB_COVER_DAYS_SUMMARY + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<RscDtReasonDTO> getAllPmSlobsReasons(Long rscMtPPHeaderId) {
        return rscDtReasonService.findAll().stream().filter(rscDtReasonDTO -> rscDtReasonDTO.getReasonType().equals(MaterialCategoryConstants.PM_SLOB_TAG) || rscDtReasonDTO.getReasonType().equals(MaterialCategoryConstants.NONE_TAG)).collect(Collectors.toList());
    }

    /*@Override
    public List<PmSlobDTO> saveAllPmSlobs(List<PmSlobDTO> pmSlobDTOList, Double sitValue, Long ppHeaderId) {
        List<RscIotPmLatestSlobReasonDTO> rscIotPmLatestSlobReasonDTOS = rscIotPmLatestSlobReasonService.findAll();
        RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(ppHeaderId);
        if (Optional.ofNullable(rscIotPmSlobSummaryDTO).isPresent()) {
            if (!rscIotPmSlobSummaryDTO.getSitValue().equals(sitValue)) {
                rscIotPmSlobSummaryService.deleteById(ppHeaderId);
                calculateRscItPMSlobSummary(ppHeaderId, sitValue);
            }
        }
        if (Optional.ofNullable(pmSlobDTOList).isPresent() && pmSlobDTOList.size() > 0) {
            pmSlobDTOList.forEach(pmSlobDTO -> {
                saveRscIotPmLatestSlob(pmSlobDTO, rscIotPmLatestSlobReasonDTOS);
            });
        }
        List<RscItPMSlob> rscItPMSlobs = rscItPMSlobRepository.saveAll(pmSlobMapper.toEntity(pmSlobDTOList));
        return pmSlobMapper.toDto(rscItPMSlobs);
    }

    private void saveRscIotPmLatestSlob(PmSlobDTO pmSlobDTO, List<RscIotPmLatestSlobReasonDTO> rscIotPmLatestSlobReasonDTOS) {
        List<RscIotPmLatestSlobReasonDTO> iotPmLatestSlobReasonDTO = rscIotPmLatestSlobReasonDTOS.stream().filter(rscIotPmLatestSlobReasonDTO ->
                rscIotPmLatestSlobReasonDTO.getRscMtItemId().equals(pmSlobDTO.getRscMtItemId())).collect(Collectors.toList());
        if (iotPmLatestSlobReasonDTO.size() == 0) {
            RscIotPmLatestSlobReasonDTO rscIotPmLatestSlobReasonDTO = new RscIotPmLatestSlobReasonDTO();
            rscIotPmLatestSlobReasonDTO.setRscMtItemId(pmSlobDTO.getRscMtItemId());
            rscIotPmLatestSlobReasonDTO.setRscDtReasonId(pmSlobDTO.getReasonId());
            rscIotPmLatestSlobReasonDTO.setRemark(pmSlobDTO.getRemark());
            rscIotPmLatestSlobReasonService.saveOne(rscIotPmLatestSlobReasonDTO);
        } else {
            iotPmLatestSlobReasonDTO.forEach(rscIotPmLatestSlobReasonDTO -> {
                RscIotPmLatestSlobReasonDTO pmLatestSlobReasonDTO = new RscIotPmLatestSlobReasonDTO();
                pmLatestSlobReasonDTO.setId(rscIotPmLatestSlobReasonDTO.getId());
                pmLatestSlobReasonDTO.setRscMtItemId(rscIotPmLatestSlobReasonDTO.getRscMtItemId());
                if (pmSlobDTO.getReasonId() != null) {
                    pmLatestSlobReasonDTO.setRscDtReasonId(pmSlobDTO.getReasonId());
                }
                if (pmSlobDTO.getRemark() != null) {
                    pmLatestSlobReasonDTO.setRemark(pmSlobDTO.getRemark());
                }
                rscIotPmLatestSlobReasonService.saveOne(pmLatestSlobReasonDTO);
            });
        }
    }
*/
    @Override
    public SlobSummaryDTO getPmSlobSummary(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return getPMSlobSummaryDTO(rscMtPPheaderDTO.get());
        }
        return null;
    }

    private SlobSummaryDTO getPMSlobSummaryDTO(RscMtPPheaderDTO rscMtPPheaderDTO) {
        SlobSummaryDTO slobSummaryDTO = new SlobSummaryDTO();
        slobSummaryDTO.setMpsId(rscMtPPheaderDTO.getId());
        slobSummaryDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
        slobSummaryDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
        slobSummaryDTO.setCurrentMonthName(slobSummaryUtil.getPreviousMonthName(rscMtPPheaderDTO.getMpsDate()));
        slobSummaryDTO.setPreviousMonthName(slobSummaryUtil.getPreviousSecondMonthName(rscMtPPheaderDTO.getMpsDate()));
        RscIotPmSlobSummaryDTO currentMonthPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
        if (Optional.ofNullable(currentMonthPmSlobSummaryDTO).isPresent()) {
            Long previousMonthMpsId = rscMtPpheaderService.getPreviousMonthsFirstCutMpsId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(previousMonthMpsId).isPresent()) {
                RscIotPmSlobSummaryDTO previousMonthPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(previousMonthMpsId);
                if (Optional.ofNullable(previousMonthPmSlobSummaryDTO).isPresent()) {
                    slobSummaryDTO.setTotalObsoleteItemValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalObsoleteItemValue(), previousMonthPmSlobSummaryDTO.getTotalObsoleteItemValue()));
                    slobSummaryDTO.setTotalSlowItemValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalSlowItemValue(), previousMonthPmSlobSummaryDTO.getTotalSlowItemValue()));
                    slobSummaryDTO.setTotalSlobValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalSlobValue(), previousMonthPmSlobSummaryDTO.getTotalSlobValue()));
                    slobSummaryDTO.setTotalStockValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalStockSitValue(), previousMonthPmSlobSummaryDTO.getTotalStockSitValue()));
                    slobSummaryDTO.setSlowItemPercentageDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getSlowItemPercentage(), previousMonthPmSlobSummaryDTO.getSlowItemPercentage()));
                    slobSummaryDTO.setObsoleteItemPercentageDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getObsoleteItemPercentage(), previousMonthPmSlobSummaryDTO.getObsoleteItemPercentage()));
                    slobSummaryDTO.setStockQualityPercentageDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getStockQualityPercentage(), previousMonthPmSlobSummaryDTO.getStockQualityPercentage()));
                    slobSummaryDTO.setStockQualityValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getStockQuantity(), previousMonthPmSlobSummaryDTO.getStockQuantity()));
                }
            } else {
                slobSummaryDTO.setTotalObsoleteItemValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalObsoleteItemValue(), 0.0));
                slobSummaryDTO.setTotalSlowItemValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalSlowItemValue(), 0.0));
                slobSummaryDTO.setTotalSlobValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalSlobValue(), 0.0));
                slobSummaryDTO.setTotalStockValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getTotalStockSitValue(), 0.0));
                slobSummaryDTO.setSlowItemPercentageDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getSlowItemPercentage(), 0.0));
                slobSummaryDTO.setObsoleteItemPercentageDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getObsoleteItemPercentage(), 0.0));
                slobSummaryDTO.setStockQualityPercentageDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getStockQualityPercentage(), 0.0));
                slobSummaryDTO.setStockQualityValueDetails(slobSummaryUtil.getSlobSummaryValueDetails(currentMonthPmSlobSummaryDTO.getStockQuantity(), 0.0));
            }
        }
        return slobSummaryDTO;
    }

    @Override
    public ExcelExportFiles exportPmSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobSummaryDTO slobSummaryDTO = getPMSlobSummaryDTO(rscMtPPheaderDTO);
        excelExportFiles.setFile(PmSlobSummaryExporter.pmSlobDetailsToExcelList(slobSummaryDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_SLOB_SUMMARY + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public SlobMovementDetailsDTO getPmSlobMovement() {
        SlobMovementDetailsDTO slobMovementDetailsDTO = new SlobMovementDetailsDTO();
        List<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && rscMtPPheaderDTO.size() > 0) {
            slobMovementDetailsDTO.setMpsId(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getId());
            slobMovementDetailsDTO.setMpsDate(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsDate());
            slobMovementDetailsDTO.setMpsName(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsName());
            slobMovementDetailsDTO.setSlobMovementDTOList(slobMovementsUtil.getSlobMovementList("PM_SLOB"));
        }
        return slobMovementDetailsDTO;
    }
    @Override
    public ExcelExportFiles exportPmSlobMovementExporter(RscMtPPheaderDTO rscMtPPheaderDTO ) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobMovementDetailsDTO  slobMovementDetailsDTO  = getPmSlobMovement();
        excelExportFiles.setFile( PmSlobMovementExporter.pmslobMovementToExcelList( slobMovementDetailsDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_SLOB_MOVEMENT + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }
    @Override
    public SlobInventoryQualityIndexDetailsDTO getPmSlobInventoryQualityIndex() {
        SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO = new SlobInventoryQualityIndexDetailsDTO();
        List<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && rscMtPPheaderDTO.size() > 0) {
            slobInventoryQualityIndexDetailsDTO.setMpsId(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getId());
            slobInventoryQualityIndexDetailsDTO.setMpsDate(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsDate());
            slobInventoryQualityIndexDetailsDTO.setMpsName(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsName());
            slobInventoryQualityIndexDetailsDTO.setSlobInventoryQualityIndexDTOList(slobInventoryQualityIndexUtil.getSlobInventoryQualityIndexList("PM_SLOB"));
        }
        return slobInventoryQualityIndexDetailsDTO;
    }
    @Override
    public ExcelExportFiles exportPmSlobInventoryQualityIndexExporter(RscMtPPheaderDTO rscMtPPheaderDTO ) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobInventoryQualityIndexDetailsDTO  slobInventoryQualityIndexDetailsDTO  = getPmSlobInventoryQualityIndex();
        excelExportFiles.setFile( PmSlobInventoryQualityExporter.pmSlobInventoryQualityIndexDetailsDTOToExcelList( slobInventoryQualityIndexDetailsDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_INVENTORY_QUALITY_INDEX + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }
    @Override
    public SlobSummaryDashboardDTO getPmSlobSummaryDashboardDTO(Long ppHeaderId) {
        SlobSummaryDashboardDTO slobSummaryDashboardDTO = new SlobSummaryDashboardDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            slobSummaryDashboardDTO = rscIotPmSlobSummaryService.getPmSlobSummaryDashboard(ppHeaderId);
        }
        return slobSummaryDashboardDTO;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItPMSlobRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}