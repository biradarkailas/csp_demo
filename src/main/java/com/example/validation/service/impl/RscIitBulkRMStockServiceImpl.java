package com.example.validation.service.impl;

import com.example.validation.dto.RscIitBulkRMStockDTO;
import com.example.validation.dto.RscMtBulkMesDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.RscMtBulkMesMapper;
import com.example.validation.repository.RscMtBulkMesRepository;
import com.example.validation.service.RscIitBulkRMStockService;
import com.example.validation.service.RscIitRmBulkOpRmService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class RscIitBulkRMStockServiceImpl implements RscIitBulkRMStockService {
    private final RscIitRmBulkOpRmService rscIitRmBulkOpRmService;
    private final RscMtBulkMesMapper rscMtBulkMesMapper;
    private final RscMtBulkMesRepository rscMtBulkMesRepository;
    private final RscMtPpheaderService rscMtPpheaderService;

    public RscIitBulkRMStockServiceImpl(RscIitRmBulkOpRmService rscIitRmBulkOpRmService, RscMtBulkMesMapper rscMtBulkMesMapper, RscMtBulkMesRepository rscMtBulkMesRepository, RscMtPpheaderService rscMtPpheaderService) {
        this.rscIitRmBulkOpRmService = rscIitRmBulkOpRmService;
        this.rscMtBulkMesMapper = rscMtBulkMesMapper;
        this.rscMtBulkMesRepository = rscMtBulkMesRepository;
        this.rscMtPpheaderService = rscMtPpheaderService;
    }

    @Override
    public List<RscIitBulkRMStockDTO> getAllRscIitBulkRMStockDTOs(Long ppHeaderId) {
        List<RscIitBulkRMStockDTO> rscIitBulkRMStockDTOs = new ArrayList<>();
        RscMtPPheaderDTO rscMtPpheaderDTO = rscMtPpheaderService.findOne( ppHeaderId ).get();
        LocalDate stockDate = rscMtPpheaderDTO.getMpsDate().withDayOfMonth( 1 );
        Map<Long, Double> bulkStockQuantityMap = getBulkWiseQuantityMap( stockDate );
        for (RscIitBulkRMStockDTO rscIitBulkRMStockDTO : rscIitRmBulkOpRmService.findAllByStockDate( stockDate )) {
            rscIitBulkRMStockDTO.setBulkQuantity( bulkStockQuantityMap.get( rscIitBulkRMStockDTO.getBulkItemId() ) );
            rscIitBulkRMStockDTOs.add( rscIitBulkRMStockDTO );
        }
        return rscIitBulkRMStockDTOs;
    }

    private Map<Long, Double> getBulkWiseQuantityMap(LocalDate stockDate) {
        Map<Long, Double> bulkStockQuantityMap = new HashMap<>();
        for (RscMtBulkMesDTO rscMtBulkMesDTO : rscMtBulkMesMapper.toDto( rscMtBulkMesRepository.findAllByStockDate( stockDate ) )) {
            bulkStockQuantityMap.put( rscMtBulkMesDTO.getRscMtItemId(), rscMtBulkMesDTO.getQuantity() );
        }
        return bulkStockQuantityMap;
    }
}