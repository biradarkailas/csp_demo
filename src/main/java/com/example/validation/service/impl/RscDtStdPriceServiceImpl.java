package com.example.validation.service.impl;

import com.example.validation.dto.RscDtStdPriceDTO;
import com.example.validation.mapper.RscDtStdPriceMapper;
import com.example.validation.repository.RscDtStdPriceRepository;
import com.example.validation.service.RscDtStdPriceService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtStdPriceServiceImpl  implements RscDtStdPriceService {
   private final RscDtStdPriceRepository rscDtStdPriceRepository;
   private  final  RscDtStdPriceMapper rscDtStdPriceMapper;

    public RscDtStdPriceServiceImpl(RscDtStdPriceRepository rscDtStdPriceRepository, RscDtStdPriceMapper rscDtStdPriceMapper){
        this.rscDtStdPriceRepository = rscDtStdPriceRepository;
        this.rscDtStdPriceMapper = rscDtStdPriceMapper;
    }

    @Override
    public List<RscDtStdPriceDTO> findAll() {
        return rscDtStdPriceMapper.toDto(rscDtStdPriceRepository.findAll());
    }
}
