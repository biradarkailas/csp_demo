package com.example.validation.service.impl;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscDtFGTypeDTO;
import com.example.validation.dto.slob.*;
import com.example.validation.mapper.*;
import com.example.validation.repository.RscIotFGTypeSlobSummaryRepository;
import com.example.validation.service.RscDtFGTypeService;
import com.example.validation.service.RscIotFGTypeDivisionSlobSummaryService;
import com.example.validation.service.RscIotFGTypeSlobSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.FGSlobSummaryUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RscIotFGTypeSlobSummaryServiceImpl implements RscIotFGTypeSlobSummaryService {
    private final RscIotFGTypeSlobSummaryRepository rscIotFGTypeSlobSummaryRepository;
    private final RscIotFGTypeSlobSummaryMapper rscIotFGTypeSlobSummaryMapper;
    private final RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService;
    private final MaterialWiseFGSlobSummaryDTOMapper materialWiseFGSlobSummaryDTOMapper;
    private final FGSlobSummaryUtil fgSlobSummaryUtil;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final TypeWiseFGSlobSummaryDashboardDTOMapper typeWiseFGSlobSummaryDashboardDTOMapper;
    private final RscDtFGTypeService rscDtFGTypeService;
    private final TypeWiseFGSlobSummaryIQDashboardMapper typeWiseFGSlobSummaryIQDashboardMapper;
    private final TypeWiseFGSlobSummaryDashboardMapper typeWiseFGSlobSummaryDashboardMapper;
    private final FGTypeSlobSummaryDashboardMapper fgTypeSlobSummaryDashboardMapper;

    public RscIotFGTypeSlobSummaryServiceImpl(RscIotFGTypeSlobSummaryRepository rscIotFGTypeSlobSummaryRepository, RscIotFGTypeSlobSummaryMapper rscIotFGTypeSlobSummaryMapper, RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService, MaterialWiseFGSlobSummaryDTOMapper materialWiseFGSlobSummaryDTOMapper, FGSlobSummaryUtil fgSlobSummaryUtil, RscMtPpheaderService rscMtPpheaderService, TypeWiseFGSlobSummaryDashboardDTOMapper typeWiseFGSlobSummaryDashboardDTOMapper, RscDtFGTypeService rscDtFGTypeService, TypeWiseFGSlobSummaryIQDashboardMapper typeWiseFGSlobSummaryIQDashboardMapper, TypeWiseFGSlobSummaryDashboardMapper typeWiseFGSlobSummaryDashboardMapper, FGTypeSlobSummaryDashboardMapper fgTypeSlobSummaryDashboardMapper) {
        this.rscIotFGTypeSlobSummaryRepository = rscIotFGTypeSlobSummaryRepository;
        this.rscIotFGTypeSlobSummaryMapper = rscIotFGTypeSlobSummaryMapper;
        this.rscIotFGTypeDivisionSlobSummaryService = rscIotFGTypeDivisionSlobSummaryService;
        this.materialWiseFGSlobSummaryDTOMapper = materialWiseFGSlobSummaryDTOMapper;
        this.fgSlobSummaryUtil = fgSlobSummaryUtil;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.typeWiseFGSlobSummaryDashboardDTOMapper = typeWiseFGSlobSummaryDashboardDTOMapper;
        this.rscDtFGTypeService = rscDtFGTypeService;
        this.typeWiseFGSlobSummaryIQDashboardMapper = typeWiseFGSlobSummaryIQDashboardMapper;
        this.typeWiseFGSlobSummaryDashboardMapper = typeWiseFGSlobSummaryDashboardMapper;
        this.fgTypeSlobSummaryDashboardMapper = fgTypeSlobSummaryDashboardMapper;
    }

    @Override
    public void createTypeWiseFGSlobSummary(Long ppHeaderId) {
        if (rscIotFGTypeSlobSummaryRepository.countByRscMtPPheaderId(ppHeaderId) == 0)
            rscIotFGTypeSlobSummaryRepository.saveAll(rscIotFGTypeSlobSummaryMapper.toEntity(getRscIotFGDivisionSlobSummaryDTOs(ppHeaderId)));
    }

    private List<RscIotFGTypeSlobSummaryDTO> getRscIotFGDivisionSlobSummaryDTOs(Long ppHeaderId) {
        List<RscIotFGTypeDivisionSlobSummaryDTO> rscIotFGTypeDivisionSlobSummaryDTOs = rscIotFGTypeDivisionSlobSummaryService.findAll(ppHeaderId);
        Map<String, Double> fgTypeWiseSlobMap = new HashMap<>();
        Map<String, Double> sfTypeWiseSlobMap = new HashMap<>();
        if (Optional.ofNullable(rscIotFGTypeDivisionSlobSummaryDTOs).isPresent()) {
            for (RscIotFGTypeDivisionSlobSummaryDTO rscIotFGTypeDivisionSlobSummaryDTO : rscIotFGTypeDivisionSlobSummaryDTOs) {
                if (rscIotFGTypeDivisionSlobSummaryDTO.getFgType().equals(MaterialCategoryConstants.YFG_TYPE)) {
                    fgTypeWiseSlobMap.put(MaterialCategoryConstants.STOCK, fgSlobSummaryUtil.getValidTotalValue(fgTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalStockValue(), MaterialCategoryConstants.STOCK));
                    fgTypeWiseSlobMap.put(MaterialCategoryConstants.SLOB, fgSlobSummaryUtil.getValidTotalValue(fgTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalSlobValue(), MaterialCategoryConstants.SLOB));
                    fgTypeWiseSlobMap.put(MaterialCategoryConstants.SLOW, fgSlobSummaryUtil.getValidTotalValue(fgTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalSlowItemValue(), MaterialCategoryConstants.SLOW));
                    fgTypeWiseSlobMap.put(MaterialCategoryConstants.OBSOLETE, fgSlobSummaryUtil.getValidTotalValue(fgTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalObsoleteItemValue(), MaterialCategoryConstants.OBSOLETE));
                } else {
                    sfTypeWiseSlobMap.put(MaterialCategoryConstants.STOCK, fgSlobSummaryUtil.getValidTotalValue(sfTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalStockValue(), MaterialCategoryConstants.STOCK));
                    sfTypeWiseSlobMap.put(MaterialCategoryConstants.SLOB, fgSlobSummaryUtil.getValidTotalValue(sfTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalSlobValue(), MaterialCategoryConstants.SLOB));
                    sfTypeWiseSlobMap.put(MaterialCategoryConstants.SLOW, fgSlobSummaryUtil.getValidTotalValue(sfTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalSlowItemValue(), MaterialCategoryConstants.SLOW));
                    sfTypeWiseSlobMap.put(MaterialCategoryConstants.OBSOLETE, fgSlobSummaryUtil.getValidTotalValue(sfTypeWiseSlobMap, rscIotFGTypeDivisionSlobSummaryDTO.getTotalObsoleteItemValue(), MaterialCategoryConstants.OBSOLETE));
                }
            }
        }
        return getRscIotFGDivisionSlobSummaryDTOs(fgTypeWiseSlobMap, sfTypeWiseSlobMap, ppHeaderId);
    }

    private List<RscIotFGTypeSlobSummaryDTO> getRscIotFGDivisionSlobSummaryDTOs(Map<String, Double> fgTypeWiseSlobMap, Map<String, Double> sfTypeWiseSlobMap, Long ppHeaderId) {
        List<RscIotFGTypeSlobSummaryDTO> rscIotFGTypeSlobSummaryDTOS = new ArrayList<>();
        if (fgTypeWiseSlobMap.size() > 0)
            rscIotFGTypeSlobSummaryDTOS.add(getRscIotFGDivisionSlobSummaryDTO(fgTypeWiseSlobMap, rscDtFGTypeService.getRscDtFGTypeIdByType(MaterialCategoryConstants.YFG_TYPE), ppHeaderId));
        if (sfTypeWiseSlobMap.size() > 0)
            rscIotFGTypeSlobSummaryDTOS.add(getRscIotFGDivisionSlobSummaryDTO(sfTypeWiseSlobMap, rscDtFGTypeService.getRscDtFGTypeIdByType(MaterialCategoryConstants.ZRM_TYPE), ppHeaderId));
        return rscIotFGTypeSlobSummaryDTOS;
    }

    private RscIotFGTypeSlobSummaryDTO getRscIotFGDivisionSlobSummaryDTO(Map<String, Double> typeWiseSlobMap, Long fgTypeId, Long ppHeaderId) {
        RscIotFGTypeSlobSummaryDTO rscIotFGTypeSlobSummaryDTO = new RscIotFGTypeSlobSummaryDTO();
        rscIotFGTypeSlobSummaryDTO.setFgTypeId(fgTypeId);
        rscIotFGTypeSlobSummaryDTO.setRscMtPPheaderId(ppHeaderId);
        rscIotFGTypeSlobSummaryDTO.setTotalStockValue(typeWiseSlobMap.get(MaterialCategoryConstants.STOCK));
        rscIotFGTypeSlobSummaryDTO.setTotalSlobValue(typeWiseSlobMap.get(MaterialCategoryConstants.SLOB));
        rscIotFGTypeSlobSummaryDTO.setTotalSlowItemValue(typeWiseSlobMap.get(MaterialCategoryConstants.SLOW));
        rscIotFGTypeSlobSummaryDTO.setTotalObsoleteItemValue(typeWiseSlobMap.get(MaterialCategoryConstants.OBSOLETE));
        Double slobPercentageValue = fgSlobSummaryUtil.getPercentageValue(typeWiseSlobMap, MaterialCategoryConstants.SLOB);
        rscIotFGTypeSlobSummaryDTO.setSlobItemPercentage(slobPercentageValue);
        rscIotFGTypeSlobSummaryDTO.setSlowItemPercentage(fgSlobSummaryUtil.getPercentageValue(typeWiseSlobMap, MaterialCategoryConstants.SLOW));
        rscIotFGTypeSlobSummaryDTO.setObsoleteItemPercentage(fgSlobSummaryUtil.getPercentageValue(typeWiseSlobMap, MaterialCategoryConstants.OBSOLETE));
        rscIotFGTypeSlobSummaryDTO.setStockQualityPercentage(fgSlobSummaryUtil.getStockQualityPercentage(slobPercentageValue));
        rscIotFGTypeSlobSummaryDTO.setStockQualityValue(fgSlobSummaryUtil.getStockQualityValue(rscIotFGTypeSlobSummaryDTO.getTotalSlobValue(), rscIotFGTypeSlobSummaryDTO.getStockQualityPercentage()));
        return rscIotFGTypeSlobSummaryDTO;
    }

    @Override
    public List<RscIotFGTypeSlobSummaryDTO> findAll(Long ppHeaderId) {
        return rscIotFGTypeSlobSummaryMapper.toDto(rscIotFGTypeSlobSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<TypeWiseFGSlobSummaryDTO> findAllMaterialWiseFGSlobSummaryDTO(Long ppHeaderId) {
        return materialWiseFGSlobSummaryDTOMapper.toDto(rscIotFGTypeSlobSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<TypeWiseFGSlobDeviationDTO> getDivisionWiseFGSlobDeviation(Long currentMonthppHeaderId) {
        List<TypeWiseFGSlobDeviationDTO> typeWiseFGSlobDeviationDTOs = new ArrayList<>();
        Long previousMonthMpsId = rscMtPpheaderService.getPreviousMonthMpsId(currentMonthppHeaderId);
        List<RscIotFGTypeSlobSummaryDTO> rscIotFGTypeSlobSummaryDTOs = rscIotFGTypeSlobSummaryMapper.toDto(rscIotFGTypeSlobSummaryRepository.findAllByRscMtPPheaderId(currentMonthppHeaderId));
        for (RscIotFGTypeSlobSummaryDTO rscIotFGTypeSlobSummaryDTO : rscIotFGTypeSlobSummaryDTOs) {
            TypeWiseFGSlobDeviationDTO typeWiseFGSlobDeviationDTO = new TypeWiseFGSlobDeviationDTO();
            typeWiseFGSlobDeviationDTO.setFgTypeId(rscIotFGTypeSlobSummaryDTO.getFgTypeId());
            typeWiseFGSlobDeviationDTO.setFgType(rscIotFGTypeSlobSummaryDTO.getFgType());
            typeWiseFGSlobDeviationDTO.setTypeDeviationDTO(getTypeWiseFGSlobDeviationDTO(previousMonthMpsId, rscIotFGTypeSlobSummaryDTO.getTotalSlobValue(), rscIotFGTypeSlobSummaryDTO.getFgTypeId()));
            typeWiseFGSlobDeviationDTO.setDivisionWiseFGSlobSummaryDTOs(getDivisionWiseFGSlobDeviationDTOs(currentMonthppHeaderId, previousMonthMpsId, rscIotFGTypeSlobSummaryDTO.getFgTypeId()));
            typeWiseFGSlobDeviationDTOs.add(typeWiseFGSlobDeviationDTO);
        }
        return typeWiseFGSlobDeviationDTOs;
    }

    private FGSlobDeviationDTO getTypeWiseFGSlobDeviationDTO(Long previousMonthMpsId, Double totalSlobValue, Long fgTypeId) {
        if (Optional.ofNullable(previousMonthMpsId).isPresent()) {
            RscIotFGTypeSlobSummaryDTO previousMonthRscIotFGTypeSlobSummaryDTO = rscIotFGTypeSlobSummaryMapper.toDto(rscIotFGTypeSlobSummaryRepository.findByRscMtPPheaderIdAndRscDtFGTypeId(previousMonthMpsId, fgTypeId));
            if (Optional.ofNullable(previousMonthRscIotFGTypeSlobSummaryDTO).isPresent()) {
                return fgSlobSummaryUtil.getFGSlobDeviationDTO(totalSlobValue, previousMonthRscIotFGTypeSlobSummaryDTO.getTotalSlobValue());
            } else {
                return fgSlobSummaryUtil.getFGSlobDeviationDTO(totalSlobValue, 0.0);
            }
        } else {
            return fgSlobSummaryUtil.getFGSlobDeviationDTO(totalSlobValue, 0.0);
        }
    }

    private List<DivisionWiseFGSlobDeviationDTO> getDivisionWiseFGSlobDeviationDTOs(Long currentMonthppHeaderId, Long previousMonthMpsId, Long fgTypeId) {
        List<DivisionWiseFGSlobDeviationDTO> divisionWiseFGSlobDeviationDTOs = new ArrayList<>();
        List<DivisionWiseFGSlobSummaryDTO> currentMonthDivisionWiseFGSlobSummaryDTOs = rscIotFGTypeDivisionSlobSummaryService.findAllByFGTypeId(currentMonthppHeaderId, fgTypeId);
        for (DivisionWiseFGSlobSummaryDTO divisionWiseFGSlobSummaryDTO : currentMonthDivisionWiseFGSlobSummaryDTOs) {
            DivisionWiseFGSlobDeviationDTO divisionWiseFGSlobDeviationDTO = new DivisionWiseFGSlobDeviationDTO();
            if (Optional.ofNullable(previousMonthMpsId).isPresent()) {
                divisionWiseFGSlobDeviationDTO.setDivisionName(divisionWiseFGSlobSummaryDTO.getDivisionName());
                divisionWiseFGSlobDeviationDTO.setDivisionWiseDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(divisionWiseFGSlobSummaryDTO.getSlobValue(), getPreviousMonthSlobValue(previousMonthMpsId, divisionWiseFGSlobSummaryDTO.getRscDtDivisionId(), fgTypeId)));
            } else {
                divisionWiseFGSlobDeviationDTO.setDivisionName(divisionWiseFGSlobSummaryDTO.getDivisionName());
                divisionWiseFGSlobDeviationDTO.setDivisionWiseDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(divisionWiseFGSlobSummaryDTO.getSlobValue(), 0.0));
            }
            divisionWiseFGSlobDeviationDTOs.add(divisionWiseFGSlobDeviationDTO);
        }
        return divisionWiseFGSlobDeviationDTOs;
    }

    private Double getPreviousMonthSlobValue(Long previousMonthMpsId, Long divisionId, Long fgTypeId) {
        List<DivisionWiseFGSlobSummaryDTO> previousMonthDivisionWiseFGSlobSummaryDTOs = rscIotFGTypeDivisionSlobSummaryService.findAllByFGTypeId(previousMonthMpsId, fgTypeId);
        Optional<DivisionWiseFGSlobSummaryDTO> divisionWiseFGSlobSummaryDTO = previousMonthDivisionWiseFGSlobSummaryDTOs.stream()
                .filter(previousMonthDivisionWiseFGSlobSummaryDTO -> previousMonthDivisionWiseFGSlobSummaryDTO.getRscDtDivisionId().equals(divisionId))
                .findFirst();
        if (divisionWiseFGSlobSummaryDTO.isPresent())
            return divisionWiseFGSlobSummaryDTO.get().getSlobValue();
        return 0.0;
    }

    @Override
    public MaterialWiseFGSlobSummaryDashboardDTO getMaterialWiseFGSlobSummaryDashboardDTO(Long ppHeaderId) {
        MaterialWiseFGSlobSummaryDashboardDTO materialWiseFGSlobSummaryDashboardDTO = new MaterialWiseFGSlobSummaryDashboardDTO();
        List<TypeWiseFGSlobSummaryDashboardDTO> typeWiseFGSlobSummaryDashboardDTOs = typeWiseFGSlobSummaryDashboardDTOMapper.toDto(rscIotFGTypeSlobSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
        materialWiseFGSlobSummaryDashboardDTO.setTypeWiseFGSlobSummaryDashboardDTOs(typeWiseFGSlobSummaryDashboardDTOs);
        materialWiseFGSlobSummaryDashboardDTO.setTotalSlobValue(fgSlobSummaryUtil.getDashboardTotalSlobValue(typeWiseFGSlobSummaryDashboardDTOs));
        materialWiseFGSlobSummaryDashboardDTO.setTotalSLValue(fgSlobSummaryUtil.getDashboardTotalSLValue(typeWiseFGSlobSummaryDashboardDTOs));
        materialWiseFGSlobSummaryDashboardDTO.setTotalOBValue(fgSlobSummaryUtil.getDashboardTotalOBValue(typeWiseFGSlobSummaryDashboardDTOs));
        return materialWiseFGSlobSummaryDashboardDTO;
    }

    @Override
    public List<DivisionWiseFGSlobSummaryIQDashboardDTO> findAllMaterialWiseFGSlobSummaryIQDashboardDTO(Long ppHeaderId) {
        return typeWiseFGSlobSummaryIQDashboardMapper.toDto(rscIotFGTypeSlobSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RscIotFGTypeSlobSummaryDTO findByPPHeaderIdAndTypeId(Long ppHeaderId, Long typeId) {
        return rscIotFGTypeSlobSummaryMapper.toDto(rscIotFGTypeSlobSummaryRepository.findByRscMtPPheaderIdAndRscDtFGTypeId(ppHeaderId, typeId));
    }

    @Override
    public DivisionFGSlobSummaryDashboardDTO getDivisionFGSlobSummaryDashboardDTO(Long ppHeaderId, Long fgTypeId) {
        DivisionFGSlobSummaryDashboardDTO divisionFGSlobSummaryDashboardDTO = new DivisionFGSlobSummaryDashboardDTO();
        RscDtFGTypeDTO rscDtFGTypeDTO = rscDtFGTypeService.findById(fgTypeId);
        if (rscDtFGTypeDTO.getType().equals(MaterialCategoryConstants.ALL)) {
            divisionFGSlobSummaryDashboardDTO.setFgTypeId(rscDtFGTypeDTO.getId());
            divisionFGSlobSummaryDashboardDTO.setFgType(rscDtFGTypeDTO.getType());
            divisionFGSlobSummaryDashboardDTO.setDivisionWiseFGSlobSummaryDashboardDTOs(typeWiseFGSlobSummaryDashboardMapper.toDto(rscIotFGTypeSlobSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        } else {
            divisionFGSlobSummaryDashboardDTO.setFgTypeId(rscDtFGTypeDTO.getId());
            divisionFGSlobSummaryDashboardDTO.setFgType(rscDtFGTypeDTO.getType());
            divisionFGSlobSummaryDashboardDTO.setDivisionWiseFGSlobSummaryDashboardDTOs(rscIotFGTypeDivisionSlobSummaryService.getDivisionFGSlobSummaryDashboardDTO(ppHeaderId, fgTypeId));
        }
        return divisionFGSlobSummaryDashboardDTO;
    }

    @Override
    public FGSlobSummaryDashboardDTO findSlobSummaryByPPHeaderIdAndTypeId(Long ppHeaderId, Long typeId) {
        return fgTypeSlobSummaryDashboardMapper.toDto(rscIotFGTypeSlobSummaryRepository.findByRscMtPPheaderIdAndRscDtFGTypeId(ppHeaderId, typeId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFGTypeSlobSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}