package com.example.validation.service.impl;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.service.AnalyzerAllExportReportsService;
import com.example.validation.service.ExcelExportService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.stereotype.Service;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
public class AnalyzerAllExportReportsServiceImpl implements AnalyzerAllExportReportsService {
    private final RscMtPpheaderService rscMtPpHeaderService;
    private final ExcelExportService excelExportService;

    public AnalyzerAllExportReportsServiceImpl(RscMtPpheaderService rscMtPpHeaderService, ExcelExportService excelExportService) {
        this.rscMtPpHeaderService = rscMtPpHeaderService;
        this.excelExportService = excelExportService;
    }

    @Override
    public AnalyzerAllExportReportsDTO getAllExportFileNames(Long ppHeaderId) {
        AnalyzerAllExportReportsDTO analyzerAllExportReportsDTO = new AnalyzerAllExportReportsDTO();
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne( ppHeaderId );
        List<String> fgExportsFiles = new ArrayList<>();
        List<String> pmExportsFiles = new ArrayList<>();
        List<String> pmOtifExportsFiles = new ArrayList<>();
        List<String> rmExportsFiles = new ArrayList<>();
        List<String> fgAnnualExportsFiles = new ArrayList<>();
        List<String> pmAnnualExportsFiles = new ArrayList<>();
        List<String> rmAnnualExportsFiles = new ArrayList<>();
        for (String list : ExcelFileNameConstants.fileNameList) {
            if (optionalRscMtPpHeaderDTO.isPresent()) {
                File fgFolder = new File( ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Plans" + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + list );
                File fgAnnualFolder = new File( ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Analysis" + "/" + "Annual" + "/" + list );
                File pmFolder = new File( ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Analysis" + "/" + "Month" + "/" + "PM" + "/" + "Otif" );
                if (pmFolder.isDirectory()) {
                    File[] PMOtifFolderList = pmFolder.listFiles();
                    if (Optional.ofNullable( PMOtifFolderList ).isPresent()) {
                        for (File ExportFolderList : PMOtifFolderList) {
                            if (list.equals( ExcelFileNameConstants.PMOtif ) && ExportFolderList.isFile()) {
                                pmOtifExportsFiles.add( ExportFolderList.getName() );
                            }
                        }
                    }
                }
                if (fgFolder.isDirectory()) {
                    File[] fgFolderList = fgFolder.listFiles();
                    if (Optional.ofNullable( fgFolderList ).isPresent()) {
                        for (File ExportFolderList : fgFolderList) {
                            if (list.equals( ExcelFileNameConstants.FG ) && ExportFolderList.isFile()) {
                                fgExportsFiles.add( ExportFolderList.getName() );
                            } else if (list.equals( ExcelFileNameConstants.PM ) && ExportFolderList.isFile()) {
                                pmExportsFiles.add( ExportFolderList.getName() );
                            } else if (list.equals( ExcelFileNameConstants.PMOtif ) && ExportFolderList.isFile()) {
                                pmOtifExportsFiles.add( ExportFolderList.getName() );
                            } else if (list.equals( ExcelFileNameConstants.RM ) && ExportFolderList.isFile()) {
                                rmExportsFiles.add( ExportFolderList.getName() );
                            }
                        }
                    }
                }
                if (fgAnnualFolder.isDirectory()) {
                    File[] fgAnnualFolderList = fgAnnualFolder.listFiles();
                    if (Optional.ofNullable( fgAnnualFolderList ).isPresent()) {
                        for (File ExportFolderList : fgAnnualFolderList) {
                            if (list.equals( ExcelFileNameConstants.FG ) && ExportFolderList.isFile()) {
                                fgAnnualExportsFiles.add( ExportFolderList.getName() );
                            } else if (list.equals( ExcelFileNameConstants.PM ) && ExportFolderList.isFile()) {
                                pmAnnualExportsFiles.add( ExportFolderList.getName() );
                            } else if (list.equals( ExcelFileNameConstants.PMOtif ) && ExportFolderList.isFile()) {
                                pmOtifExportsFiles.add( ExportFolderList.getName() );
                            } else if (list.equals( ExcelFileNameConstants.RM ) && ExportFolderList.isFile()) {
                                rmAnnualExportsFiles.add( ExportFolderList.getName() );
                            }
                        }
                    }
                }
            }
            analyzerAllExportReportsDTO.setFgFileList( fgExportsFiles );
            analyzerAllExportReportsDTO.setPmFileList( pmExportsFiles );
            analyzerAllExportReportsDTO.setPmOtifFileList( pmOtifExportsFiles );
            analyzerAllExportReportsDTO.setRmFileList( rmExportsFiles );
            analyzerAllExportReportsDTO.setFgAnnualFileList( fgAnnualExportsFiles );
            analyzerAllExportReportsDTO.setPmAnnualFileList( pmAnnualExportsFiles );
            analyzerAllExportReportsDTO.setRmAnnualFileList( rmAnnualExportsFiles );
        }
        return analyzerAllExportReportsDTO;
    }

    @Override
    public AnalyzerExportFileDTO downloadfile(Long ppHeaderId, String fgCheckedList, String pmCheckedList, String pmOtifCheckedList, String rmCheckedList) {
        AnalyzerExportFileDTO analyzerExportFileDTO = new AnalyzerExportFileDTO();
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne( ppHeaderId );
        String[] fgFiles = fgCheckedList.split( "," );
        String[] pmFiles = pmCheckedList.split( "," );
        String[] pmOtifFiles = pmOtifCheckedList.split( "," );
        String[] rmFiles = rmCheckedList.split( "," );
        List<String> allFilesList = new ArrayList<>();
        Collections.addAll( allFilesList, fgFiles );
        Collections.addAll( allFilesList, pmFiles );
        Collections.addAll( allFilesList, pmOtifFiles );
        Collections.addAll( allFilesList, rmFiles );
        Long fileCount = allFilesList.stream().filter( x -> !x.contains( "null" ) ).count();
        if (fileCount == 1) {
            if (pmFiles[0].contains( "null" ) && rmFiles[0].contains( "null" ) && pmOtifFiles[0].contains( "null" ))
                analyzerExportFileDTO.setExcelExportFiles( excelExportService.downloadfgExport( ppHeaderId, fgFiles[0].substring( 0, fgFiles[0].length() - 16 ) ) );
            else if (fgFiles[0].contains( "null" ) && rmFiles[0].contains( "null" ) && pmOtifFiles[0].contains( "null" ))
                analyzerExportFileDTO.setExcelExportFiles( excelExportService.downloadPmExport( ppHeaderId, pmFiles[0].substring( 0, pmFiles[0].length() - 16 ) ) );
            else if (fgFiles[0].contains( "null" ) && rmFiles[0].contains( "null" ) && pmFiles[0].contains( "null" ))
                analyzerExportFileDTO.setExcelExportFiles(excelExportService.downloadPmOtifExport(ppHeaderId, pmOtifFiles[0]));
            else if (fgFiles[0].contains( "null" ) && pmFiles[0].contains( "null" ) && pmOtifFiles[0].contains( "null" ))
                analyzerExportFileDTO.setExcelExportFiles( excelExportService.downloadRmExport( ppHeaderId, rmFiles[0].substring( 0, rmFiles[0].length() - 16 ) ) );
            analyzerExportFileDTO.setMediaType( ExcelFileNameConstants.Application_xlsx );
        } else if (fileCount > 1) {
            if (optionalRscMtPpHeaderDTO.isPresent()) {
                File fgFileFolder = new File( ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Plans" + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + ExcelFileNameConstants.FG );
                File pmFileFolder = new File( ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Plans" + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + ExcelFileNameConstants.PM );
                File pmOtifFileFolder = new File(ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Analysis" + "/" + "Month" + "/" + "PM" + "/" + "Otif");
                File rmFileFolder = new File( ExcelFileNameConstants.Export_Download_path + optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() + "/" + "Plans" + "/" + optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + ExcelFileNameConstants.RM );
                File[] allFgFiles = fgFileFolder.listFiles();
                File[] allPmFiles = pmFileFolder.listFiles();
                File[] allPmOtifFiles = pmOtifFileFolder.listFiles();
                File[] allRmFiles = rmFileFolder.listFiles();
                ;
                List<File> fgZipFileList = saveFile( allFgFiles, fgFiles );
                List<File> pmZipFileList = saveFile( allPmFiles, pmFiles );
                List<File> pmOtifZipFileList = saveFile( allPmOtifFiles, pmOtifFiles );
                List<File> rmZipFileList = saveFile( allRmFiles, rmFiles );
                InputStream file = createZip( fgZipFileList, pmZipFileList, pmOtifZipFileList, rmZipFileList );
                excelExportFiles.setFile( file );
                excelExportFiles.setFileName( ExcelFileNameConstants.Analyzer_Zip_Filename );
                analyzerExportFileDTO.setExcelExportFiles( excelExportFiles );
                analyzerExportFileDTO.setMediaType( ExcelFileNameConstants.Application_zip );
            }
        }
        return analyzerExportFileDTO;
    }

    private ByteArrayInputStream createZip(List<File> fgZipFileList, List<File> pmZipFileList, List<File> pmOtifZipFileList, List<File> rmZipFileList) {
        try (ByteArrayOutputStream fout = new ByteArrayOutputStream(); ZipOutputStream out = new ZipOutputStream( fout )) {
            byte fileSize[] = new byte[10240];
            ZipEntry addFgFolder = new ZipEntry( ExcelFileNameConstants.FG_Directory );
            ZipEntry addPmFolder = new ZipEntry( ExcelFileNameConstants.PM_Directory );
            ZipEntry addPmOtifFolder = new ZipEntry( ExcelFileNameConstants.PMOtif_Directory );
            ZipEntry addRmFolder = new ZipEntry( ExcelFileNameConstants.RM_Directory );

            for (File file : fgZipFileList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ExcelFileNameConstants.FG_Directory + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }

            for (File file : pmZipFileList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ExcelFileNameConstants.PM_Directory + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }

            for (File file : pmOtifZipFileList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ExcelFileNameConstants.PMOtif_Directory + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }
            for (File file : rmZipFileList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ExcelFileNameConstants.RM_Directory + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }

            out.close();
            fout.close();
            return new ByteArrayInputStream( fout.toByteArray() );
        } catch (Exception e) {
            throw new RuntimeException( "fail to download Zip file: " + e.getMessage() );
        }
    }

    private ByteArrayInputStream getZipFile(File file, String path) {
        try {
            File f = new File( path + "/" + file.getName() );
            InputStream in = new FileInputStream( f );
            byte[] buff = new byte[10240];
            int bytesRead = 0;
            ByteArrayOutputStream bao = new ByteArrayOutputStream();
            while ((bytesRead = in.read( buff )) != -1) {
                bao.write( buff, 0, bytesRead );
            }
            byte[] data = bao.toByteArray();
            ByteArrayInputStream bin = new ByteArrayInputStream( data );
            return bin;
        } catch (Exception e) {
            throw new RuntimeException( "fail to download Zip file: " + e.getMessage() );
        }
    }

    private ByteArrayInputStream createBackUpZip(List<File> allFirstCutFilesList, List<File> allValidatedFilesList, List<File> pmOtifZipFileList, List<File> allModulesFilesList) {
        try (ByteArrayOutputStream fout = new ByteArrayOutputStream(); ZipOutputStream out = new ZipOutputStream( fout )) {
            byte fileSize[] = new byte[10240];
            for (File file : allFirstCutFilesList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ApplicationConstants.PLANS_FIRST_CUT_ZIP_SUB_DIR_PATH + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }
            for (File file : allValidatedFilesList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ApplicationConstants.PLANS_VALIDATED_ZIP_SUB_DIR_PATH + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }
            for (File file : pmOtifZipFileList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ApplicationConstants.PM_OTIF_ZIP_SUB_DIR_PATH + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }
            for (File file : allModulesFilesList) {
                if (file.exists()) {
                    ZipEntry addFiles = new ZipEntry( ApplicationConstants.ALL_MODULES_ZIP_SUB_DIR_PATH + file.getName() );
                    addFiles.setTime( file.lastModified() );
                    out.putNextEntry( addFiles );
                    InputStream fin = new FileInputStream( file );
                    while (true) {
                        int fileLength = fin.read( fileSize, 0, fileSize.length );
                        if (fileLength <= 0) break;
                        out.write( fileSize, 0, fileLength );
                    }
                    fin.close();
                }
            }
            out.close();
            fout.close();
            return new ByteArrayInputStream( fout.toByteArray() );
        } catch (Exception e) {
            throw new RuntimeException( "fail to download Zip file: " + e.getMessage() );
        }
    }


    private List<File> saveFile(File[] allFiles, String[] filesNames) {
        List<File> fileSet = new ArrayList<>();
        if (Optional.ofNullable( allFiles ).isPresent()) {
            for (String fileName : filesNames) {
                for (File fgfile : allFiles) {
                    if (fgfile.getName().compareTo( fileName ) == 0) {
                        fileSet.add( fgfile );
                    }
                }
            }
        }
        return fileSet;
    }

    @Override
    public AllBackUpExportFileListsDTO getAllBackUpExportFileNames(String monthYearValue) {
        String[] str = monthYearValue.split( "-" );
        AllBackUpExportFileListsDTO allBackUpExportFileListsDTO = new AllBackUpExportFileListsDTO();
        BackupPlanList backupPlanList = new BackupPlanList();
        String rootLocation = ApplicationConstants.BACKUP_PATH + "/" + str[1] + "/" + str[0];
        backupPlanList.setFirstCutFileList( getFirstCutPlansList( rootLocation ) );
        backupPlanList.setValidatedFileList( getValidatedFileList( rootLocation ) );
        allBackUpExportFileListsDTO.setPlanZipList( backupPlanList );
        allBackUpExportFileListsDTO.setPmOtifZipList( getPmOtifZipList( rootLocation ) );
        allBackUpExportFileListsDTO.setAllModuleZipList( getAllModulesZipList( rootLocation ) );
        return allBackUpExportFileListsDTO;
    }

    private List<String> getFirstCutPlansList(String rootLocation) {
        List<String> firstCutFileList = new ArrayList<>();
        File directoryPath = new File( rootLocation + ApplicationConstants.PLANS_FIRST_CUT_ZIP_SUB_DIR_PATH );
        if (directoryPath.exists() && directoryPath.list().length > 0) {
            firstCutFileList.addAll( getZipFiles( Arrays.asList( directoryPath.list() ) ) );
        }
        return firstCutFileList;
    }

    private List<String> getValidatedFileList(String rootLocation) {
        List<String> validatedFileList = new ArrayList<>();
        File directoryPath = new File( rootLocation + ApplicationConstants.PLANS_VALIDATED_ZIP_SUB_DIR_PATH );
        if (directoryPath.exists() && directoryPath.list().length > 0) {
            validatedFileList.addAll( getZipFiles( Arrays.asList( directoryPath.list() ) ) );
        }
        return validatedFileList;
    }

    private List<String> getPmOtifZipList(String rootLocation) {
        List<String> pmOtifFileList = new ArrayList<>();
        File directoryPath = new File( rootLocation + ApplicationConstants.PM_OTIF_ZIP_SUB_DIR_PATH );
        if (directoryPath.exists() && directoryPath.list().length > 0) {
            pmOtifFileList.addAll( getZipFiles( Arrays.asList( directoryPath.list() ) ) );
        }
        return pmOtifFileList;
    }

    private List<String> getAllModulesZipList(String rootLocation) {
        List<String> allModulesZipList = new ArrayList<>();
        File directoryPath = new File( rootLocation + ApplicationConstants.ALL_MODULES_ZIP_SUB_DIR_PATH );
        if (directoryPath.exists() && directoryPath.list().length > 0) {
            allModulesZipList.addAll( getZipFiles( Arrays.asList( directoryPath.list() ) ) );
        }
        return allModulesZipList;
    }

    private List<String> getZipFiles(List<String> stringList) {
        return stringList.stream().filter( s -> s.endsWith( ".zip" ) ).collect( Collectors.toList() );
    }

    @Override
    public AnalyzerExportFileDTO downloadBackupZipFiles(String monthYearValue, String firstCutCheckedList, String validatedCheckedList, String pmOtifCheckedList, String allModuleCheckedList) {
        String[] str = monthYearValue.split( "-" );
        AnalyzerExportFileDTO analyzerExportFileDTO = new AnalyzerExportFileDTO();
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        String[] firstCutFiles = firstCutCheckedList.split( "," );
        String[] validatedFiles = validatedCheckedList.split( "," );
        String[] pmOtifFiles = pmOtifCheckedList.split( "," );
        String[] allModuleFiles = allModuleCheckedList.split( "," );
        List<String> allFilesList = new ArrayList<>();
        Collections.addAll( allFilesList, firstCutFiles );
        Collections.addAll( allFilesList, validatedFiles );
        Collections.addAll( allFilesList, pmOtifFiles );
        Collections.addAll( allFilesList, allModuleFiles );
        Long fileCount = allFilesList.stream().filter( x -> !x.contains( "null" ) ).count();
        File firstCutDirectoryPath = new File( ApplicationConstants.BACKUP_PATH + "/" + str[1] + "/" + str[0] + ApplicationConstants.PLANS_FIRST_CUT_ZIP_SUB_DIR_PATH );
        File validatedDirectoryPath = new File( ApplicationConstants.BACKUP_PATH + "/" + str[1] + "/" + str[0] + ApplicationConstants.PLANS_VALIDATED_ZIP_SUB_DIR_PATH );
        File pmOtifDirectoryPath = new File( ApplicationConstants.BACKUP_PATH + "/" + str[1] + "/" + str[0] + ApplicationConstants.PM_OTIF_ZIP_SUB_DIR_PATH );
        File allModuleDirectoryPath = new File( ApplicationConstants.BACKUP_PATH + "/" + str[1] + "/" + str[0] + ApplicationConstants.ALL_MODULES_ZIP_SUB_DIR_PATH );
        File[] allFirstCutFiles = firstCutDirectoryPath.listFiles();
        File[] allValidatedFiles = validatedDirectoryPath.listFiles();
        File[] allPmOtifFiles = pmOtifDirectoryPath.listFiles();
        File[] allModulesFiles = allModuleDirectoryPath.listFiles();
        if (fileCount == 1) {
            if (firstCutFiles[0].contains( "null" ) && validatedFiles[0].contains( "null" ) && pmOtifFiles[0].contains( "null" )) {
                excelExportFiles.setFile( getZipFile( saveFile( allModulesFiles, allModuleFiles ).get( 0 ), allModuleDirectoryPath.toString() ) );
                excelExportFiles.setFileName( allModuleFiles[0].toString() );
                analyzerExportFileDTO.setExcelExportFiles( excelExportFiles );
            } else if (allModuleFiles[0].contains( "null" ) && validatedFiles[0].contains( "null" ) && pmOtifFiles[0].contains( "null" )) {
                excelExportFiles.setFile( getZipFile( saveFile( allFirstCutFiles, firstCutFiles ).get( 0 ), firstCutDirectoryPath.toString() ) );
                excelExportFiles.setFileName( firstCutFiles[0].toString() );
                analyzerExportFileDTO.setExcelExportFiles( excelExportFiles );
            } else if (allModuleFiles[0].contains( "null" ) && validatedFiles[0].contains( "null" ) && firstCutFiles[0].contains( "null" )) {
                excelExportFiles.setFile( getZipFile( saveFile( allPmOtifFiles, pmOtifFiles ).get( 0 ), pmOtifDirectoryPath.toString() ) );
                excelExportFiles.setFileName( pmOtifFiles[0].toString() );
                analyzerExportFileDTO.setExcelExportFiles( excelExportFiles );
            } else if (allModuleFiles[0].contains( "null" ) && firstCutFiles[0].contains( "null" ) && pmOtifFiles[0].contains( "null" )) {
                excelExportFiles.setFile( getZipFile( saveFile( allValidatedFiles, validatedFiles ).get( 0 ), validatedDirectoryPath.toString() ) );
                excelExportFiles.setFileName( validatedFiles[0].toString() );
                analyzerExportFileDTO.setExcelExportFiles( excelExportFiles );
            }
            analyzerExportFileDTO.setMediaType( ExcelFileNameConstants.Application_zip );
        } else if (fileCount > 1) {
            List<File> allFirstCutFilesList = saveFile( allFirstCutFiles, firstCutFiles );
            List<File> allValidatedFilesList = saveFile( allValidatedFiles, validatedFiles );
            List<File> pmOtifZipFileList = saveFile( allPmOtifFiles, pmOtifFiles );
            List<File> allModulesFilesList = saveFile( allModulesFiles, allModuleFiles );
            InputStream file = createBackUpZip( allFirstCutFilesList, allValidatedFilesList, pmOtifZipFileList, allModulesFilesList );
            excelExportFiles.setFile( file );
            excelExportFiles.setFileName( str[0] + "_" + str[1] + ".zip" );
            analyzerExportFileDTO.setExcelExportFiles( excelExportFiles );
            analyzerExportFileDTO.setMediaType( ExcelFileNameConstants.Application_zip );
        }
        return analyzerExportFileDTO;
    }
}
