package com.example.validation.service.impl;

import com.example.validation.dto.ItemCodeDetailDTO;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.BPGrossConsumptionMapper;
import com.example.validation.mapper.BPItemCodeDetailsMapper;
import com.example.validation.mapper.RscItBPGrossConsumptionMapper;
import com.example.validation.repository.RscItBPGrossConsumptionRepository;
import com.example.validation.service.RscItBPGrossConsumptionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscItBPGrossConsumptionServiceImpl implements RscItBPGrossConsumptionService {
    private final RscItBPGrossConsumptionRepository rscItBPGrossConsumptionRepository;
    private final RscItBPGrossConsumptionMapper rscItBPGrossConsumptionMapper;
    private final BPGrossConsumptionMapper bpGrossConsumptionMapper;
    private final BPItemCodeDetailsMapper bpItemCodedetailsMapper;

    public RscItBPGrossConsumptionServiceImpl(RscItBPGrossConsumptionRepository rscItBPGrossConsumptionRepository, RscItBPGrossConsumptionMapper rscItBPGrossConsumptionMapper, BPGrossConsumptionMapper bpGrossConsumptionMapper, BPItemCodeDetailsMapper bpItemCodedetailsMapper) {
        this.rscItBPGrossConsumptionRepository = rscItBPGrossConsumptionRepository;
        this.rscItBPGrossConsumptionMapper = rscItBPGrossConsumptionMapper;
        this.bpGrossConsumptionMapper = bpGrossConsumptionMapper;
        this.bpItemCodedetailsMapper = bpItemCodedetailsMapper;
    }

    @Override
    public void saveAll(List<RscItGrossConsumptionDTO> bpRscItGrossConsumptionDTOs, Long ppHeaderId) {
        if (rscItBPGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            bpRscItGrossConsumptionDTOs.sort(Comparator.comparing(RscItGrossConsumptionDTO::getItemCode));
            rscItBPGrossConsumptionRepository.saveAll(rscItBPGrossConsumptionMapper.toEntity(bpRscItGrossConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItBPGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<GrossConsumptionDTO> getAllBPGrossConsumption(Long ppHeaderId) {
        return bpGrossConsumptionMapper.toDto(rscItBPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<ItemCodeDetailDTO> getBPItemCodeDetailDTO(Long ppHeaderId) {
        return bpItemCodedetailsMapper.toDto(rscItBPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId) {
        return rscItBPGrossConsumptionMapper.toDto(rscItBPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId) {
        return rscItBPGrossConsumptionMapper.toDto(rscItBPGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, bpItemId));
    }

    @Override
    public List<Long> findAllItemIdByRscMtPPHeaderId(Long ppHeaderId) {
        List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs = rscItBPGrossConsumptionMapper.toDto(rscItBPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
        return rscItGrossConsumptionDTOs.stream().map(RscItGrossConsumptionDTO::getRscMtItemId).collect(Collectors.toList());
    }

    @Override
    public void deleteByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId) {
        rscItBPGrossConsumptionRepository.deleteByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, bpItemId);
    }

    @Override
    public void save(RscItGrossConsumptionDTO bpRscItGrossConsumptionDTO) {
        rscItBPGrossConsumptionRepository.save(rscItBPGrossConsumptionMapper.toEntity(bpRscItGrossConsumptionDTO));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItBPGrossConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}