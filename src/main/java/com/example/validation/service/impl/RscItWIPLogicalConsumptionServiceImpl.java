package com.example.validation.service.impl;

import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.mapper.RscItWIPLogicalConsumptionMapper;
import com.example.validation.mapper.WIPLogicalConsumptionMapper;
import com.example.validation.repository.RscItWIPLogicalConsumptionRepository;
import com.example.validation.service.RscItWIPLogicalConsumptionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class RscItWIPLogicalConsumptionServiceImpl implements RscItWIPLogicalConsumptionService {
    private final RscItWIPLogicalConsumptionRepository rscItWIPLogicalConsumptionRepository;
    private final RscItWIPLogicalConsumptionMapper rscItWIPLogicalConsumptionMapper;
    private final WIPLogicalConsumptionMapper wipLogicalConsumptionMapper;

    public RscItWIPLogicalConsumptionServiceImpl(RscItWIPLogicalConsumptionRepository rscItWIPLogicalConsumptionRepository, RscItWIPLogicalConsumptionMapper rscItWIPLogicalConsumptionMapper, WIPLogicalConsumptionMapper wipLogicalConsumptionMapper) {
        this.rscItWIPLogicalConsumptionRepository = rscItWIPLogicalConsumptionRepository;
        this.rscItWIPLogicalConsumptionMapper = rscItWIPLogicalConsumptionMapper;
        this.wipLogicalConsumptionMapper = wipLogicalConsumptionMapper;
    }

    @Override
    public void saveAll(List<RscItLogicalConsumptionDTO> wipRscItLogicalConsumptionDTOs, Long ppHeaderId) {
        if (rscItWIPLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            wipRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
            wipRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
            rscItWIPLogicalConsumptionRepository.saveAll(rscItWIPLogicalConsumptionMapper.toEntity(wipRscItLogicalConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItWIPLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<LogicalConsumptionMainDTO> getWIPLogicalConsumptionByBulk(Long ppHeaderId, Long wipMtItemId) {
        return wipLogicalConsumptionMapper.toDto(rscItWIPLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemParentId(ppHeaderId, wipMtItemId));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId) {
        return rscItWIPLogicalConsumptionMapper.toDto(rscItWIPLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<LogicalConsumptionMainDTO> getWIPLogicalConsumption(Long ppHeaderId) {
        return wipLogicalConsumptionMapper.toDto(rscItWIPLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderIdAndChildItemId(Long ppHeaderId, Long wipItemId) {
        return rscItWIPLogicalConsumptionMapper.toDto(rscItWIPLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemChildId(ppHeaderId, wipItemId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItWIPLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}