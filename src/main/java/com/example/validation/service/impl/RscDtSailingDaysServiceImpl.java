package com.example.validation.service.impl;

import com.example.validation.dto.RscDtSailingDaysDTO;
import com.example.validation.mapper.RscDtSailingDaysMapper;
import com.example.validation.repository.RscDtSailingDaysRepository;
import com.example.validation.service.RscDtSailingDaysService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtSailingDaysServiceImpl implements RscDtSailingDaysService {
    private final RscDtSailingDaysRepository rscDtSailingDaysRepository;
    private final RscDtSailingDaysMapper rscDtSailingDaysMapper;

    public RscDtSailingDaysServiceImpl(RscDtSailingDaysRepository rscDtSailingDaysRepository, RscDtSailingDaysMapper rscDtSailingDaysMapper) {
        this.rscDtSailingDaysRepository = rscDtSailingDaysRepository;
        this.rscDtSailingDaysMapper = rscDtSailingDaysMapper;
    }

    @Override
    public List<RscDtSailingDaysDTO> findAll() {
        return rscDtSailingDaysMapper.toDto(rscDtSailingDaysRepository.findAll());
    }
}
