package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelFiledetailsDTO;
import com.example.validation.dto.RscIitIdpStatusDTO;
import com.example.validation.dto.StagingTableDetailsDTO;
import com.example.validation.dto.StagingTableSummaryDTO;
import com.example.validation.service.*;
import com.example.validation.util.ExcelFileUtil;
import com.example.validation.util.FileOperation;
import com.example.validation.util.helper.JdbcTemplateHelper;
import com.example.validation.util.helper.PythonProcessHelper;
import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author Kailas Biradar
 */
@Service
public class DataLoadServiceImpl implements DataLoadService {
    private final StagingTableNameService stagingTableNameService;
    private final StagingTableSummaryService stagingTableSummaryService;
    private final JpaProcedureServiceImpl jpaProcedureServiceImpl;
    private final ExcelFileUtil excelFileUtil;
    private final JdbcTemplateHelper jdbcTemplateHelper;
    private final PythonProcessHelper pythonProcessHelper;
    private final ExcelFileDetailsService excelFileDetailsService;
    private final RscIitIdpStatusService rscIitIdpStatusService;
    private final ErrorLogService errorLogService;

    public DataLoadServiceImpl(StagingTableNameService stagingTableNameService, StagingTableSummaryService stagingTableSummaryService, JpaProcedureServiceImpl jpaProcedureServiceImpl, ExcelFileUtil excelFileUtil, JdbcTemplateHelper jdbcTemplateHelper, PythonProcessHelper pythonProcessHelper, ExcelFileDetailsService excelFileDetailsService, RscIitIdpStatusService rscIitIdpStatusService, ErrorLogService errorLogService) {
        this.stagingTableNameService = stagingTableNameService;
        this.stagingTableSummaryService = stagingTableSummaryService;
        this.jpaProcedureServiceImpl = jpaProcedureServiceImpl;
        this.excelFileUtil = excelFileUtil;
        this.jdbcTemplateHelper = jdbcTemplateHelper;
        this.pythonProcessHelper = pythonProcessHelper;
        this.excelFileDetailsService = excelFileDetailsService;
        this.rscIitIdpStatusService = rscIitIdpStatusService;
        this.errorLogService = errorLogService;
    }

    public String loadData(Set<String> files, String mpsName) {
        List<String> csvFiles = new ArrayList<>(excelFileUtil.getCsvFiles(files));
        Optional<List<String>> optionalFileList = Optional.of(excelFileUtil.getExcelFiles(files));
        optionalFileList.ifPresent(fileLists -> csvFiles.addAll(getCsvFilesByExcel(fileLists)));
        if (!csvFiles.isEmpty()) {
            loadCsvFiles(csvFiles, mpsName);
        }
        return "File Loading Started";
    }

    private List<String> getCsvFilesByExcel(List<String> excelFiles) {
        List<String> csvFileNames = new ArrayList<>();
        for (String excelFile : excelFiles) {
            Optional<List<String>> optionalSheetNameList = Optional.ofNullable(excelFileDetailsService.getSheetNamesByFileName(excelFile));
            try {
                if (optionalSheetNameList.isPresent()) {
                    String sheetNames = StringUtils.join(optionalSheetNameList.get(), ", ");
                    csvFileNames.addAll(pythonProcessHelper.excelToCsv(excelFile, sheetNames));
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return csvFileNames;
    }

    private void loadCsvFiles(List<String> csvFiles, String mpsName) {
        List<StagingTableDetailsDTO> stagingTableDetailsListByCsvFiles = stagingTableNameService.findAll().stream()
                .filter(sapTableNameObj -> csvFiles.contains(sapTableNameObj.getCsvFileName()))
                .collect(Collectors.toList());
        errorLogService.deleteAll();
        for (StagingTableDetailsDTO stagingTableDetailsListByCsvFile : stagingTableDetailsListByCsvFiles) {
            loadFileBySapTableName(stagingTableDetailsListByCsvFile);
        }
        FileOperation.moveFiles(excelFileUtil.getSourcePath(), getBackupFilesPath(mpsName));
        Optional<StagingTableDetailsDTO> smallestSequenceTableDetailsDTO = getSmallestSequenceTableName(stagingTableDetailsListByCsvFiles);
        smallestSequenceTableDetailsDTO.ifPresent(stagingTableDetailsDTO -> validateAllTableData(stagingTableDetailsDTO.getExecutionSequence(), mpsName));
        if (stagingTableSummaryService.isValidStagingTableSummaryCount())
            stagingToResourceTableData(mpsName);
    }

    private String getBackupFilesPath(String mpsName) {
        LocalDate mpsDate = rscIitIdpStatusService.getMpsDateByMpsName(mpsName);
        return ExcelFileNameConstants.BACKUP_PATH + String.valueOf(mpsDate.getYear()) + "/" + String.valueOf(mpsDate.getMonth()) + "/" + mpsName;
    }

    private Optional<StagingTableDetailsDTO> getSmallestSequenceTableName(List<StagingTableDetailsDTO> stagingTableDetailsListByCsvFiles) {
        return stagingTableDetailsListByCsvFiles.stream().min(Comparator.comparing(StagingTableDetailsDTO::getExecutionSequence));
    }

    private void loadFileBySapTableName(StagingTableDetailsDTO stagingTableDetails) {
        StagingTableSummaryDTO stagingTableSummaryDTO = stagingTableSummaryService.findByIsActiveAndTableName(stagingTableDetails.getTableName());
        if (Optional.ofNullable(stagingTableSummaryDTO).isPresent()) {
            updateAndSaveStagingTableSummary(stagingTableDetails, stagingTableSummaryDTO);
        } else {
            StagingTableSummaryDTO newStagingTableSummaryDTO = new StagingTableSummaryDTO();
            newStagingTableSummaryDTO.setTableName(stagingTableDetails.getTableName());
            newStagingTableSummaryDTO.setStartStgLoadDate(LocalDateTime.now());
            newStagingTableSummaryDTO.setActive(true);
            newStagingTableSummaryDTO.setMpsDate(LocalDate.now());
            updateAndSaveStagingTableSummary(stagingTableDetails, newStagingTableSummaryDTO);
        }
    }

    private void updateAndSaveStagingTableSummary(StagingTableDetailsDTO stagingTableDetails, StagingTableSummaryDTO stagingTableSummaryDTO) {
        if (excelFileUtil.loadCsvFile(stagingTableDetails)) {
            stagingTableSummaryDTO.setStgLoaded(true);
            stagingTableSummaryDTO.setEndStgLoadDate(LocalDateTime.now());
            stagingTableSummaryDTO.setSanitize(jpaProcedureServiceImpl.dataSanitize(stagingTableDetails.getTableName()));
        } else {
            stagingTableSummaryDTO.setSanitize(false);
            stagingTableSummaryDTO.setStgLoaded(false);
        }
        stagingTableSummaryDTO.setRecordCount(jdbcTemplateHelper.getRecordCountByTableName(stagingTableDetails.getTableName()));
        stagingTableSummaryService.save(stagingTableSummaryDTO);
    }

    /**
     * InActive old records only Active new file table StagingTableSummary
     */
    private void inActiveAllStagingTableSummary() {
        List<StagingTableSummaryDTO> activeStagingTableSummaryDTO = stagingTableSummaryService.findAllByIsActive();
        if (Optional.ofNullable(activeStagingTableSummaryDTO).isPresent()) {
            for (StagingTableSummaryDTO stagingTableSummary : activeStagingTableSummaryDTO) {
                stagingTableSummary.setActive(false);
            }
            stagingTableSummaryService.saveAll(activeStagingTableSummaryDTO);
        }
    }

    private List<String> getTableNames(List<StagingTableDetailsDTO> stagingTableDetailsDTOs) {
        return stagingTableDetailsDTOs.stream().map(StagingTableDetailsDTO::getTableName).collect(Collectors.toList());
    }

    private void validateAllTableData(Integer executionSequence, String mpsName) {
        List<String> allActiveTableNames = stagingTableNameService.findAllTableNameByExecutionSequence(executionSequence);
        if (Optional.ofNullable(allActiveTableNames).isPresent()) {
            Map<String, StagingTableSummaryDTO> stagingTableSummaryDTOMap = getValidateStagingTableSummaryDTOMap(allActiveTableNames);
            if (!stagingTableSummaryDTOMap.isEmpty()) {
                List<StagingTableDetailsDTO> stagingTableDetailsDTOs = stagingTableNameService.findAllByTableNamesAndValidationSequenceAsc(allActiveTableNames);
                for (StagingTableDetailsDTO stagingTableDetailsDTO : stagingTableDetailsDTOs) {
                    System.out.println("Validation Sq No:: " + stagingTableDetailsDTO.getExecutionSequence());
                    if (stagingTableSummaryDTOMap.containsKey(stagingTableDetailsDTO.getTableName())) {
                        StagingTableSummaryDTO stagingTableSummaryDTO = stagingTableSummaryDTOMap.get(stagingTableDetailsDTO.getTableName());
                        updateStagingTableSummary(stagingTableSummaryDTO, mpsName);
                    } else {
                        jpaProcedureServiceImpl.truncateTable(stagingTableDetailsDTO.getTableName());
                        saveStagingTableSummary(stagingTableDetailsDTO);
                    }
                }
            }
        }
    }

    private void saveStagingTableSummary(StagingTableDetailsDTO stagingTableDetailsDTO) {
        if (stagingTableSummaryService.isValidStagingTableSummaryByTableName(stagingTableDetailsDTO.getTableName())) {
            StagingTableSummaryDTO stagingTableSummaryDTO = new StagingTableSummaryDTO();
            stagingTableSummaryDTO.setTableName(stagingTableDetailsDTO.getTableName());
            stagingTableSummaryDTO.setValidate(true);
            stagingTableSummaryDTO.setActive(true);
            stagingTableSummaryDTO.setRecordCount(jdbcTemplateHelper.getRecordCountByTableName(stagingTableDetailsDTO.getTableName()));
            stagingTableSummaryService.save(stagingTableSummaryDTO);
        }
    }

    private void stagingToResourceTableData(String mpsName) {
        List<String> validateTableNames = stagingTableSummaryService.findAllTableNamesByValidate();
        if (Optional.ofNullable(validateTableNames).isPresent()) {
            Map<String, StagingTableSummaryDTO> stagingTableSummaryDTOMap = getResourceStagingTableSummaryDTOMap(validateTableNames);
            if (!stagingTableSummaryDTOMap.isEmpty()) {
                List<StagingTableDetailsDTO> stagingTableDetailsDTOs = stagingTableNameService.findAllByTableNamesAndResourceSequenceAsc(validateTableNames);
                for (StagingTableDetailsDTO stagingTableDetailsDTO : stagingTableDetailsDTOs) {
                    System.out.println("Resource Sq No:: " + stagingTableDetailsDTO.getResourceSequence());
                    if (stagingTableSummaryDTOMap.containsKey(stagingTableDetailsDTO.getTableName())) {
                        stagingToResourceTableDataProcedureCall(stagingTableSummaryDTOMap.get(stagingTableDetailsDTO.getTableName()), mpsName);
                    }
                }
                stagingTableSummaryService.inActiveAllStagingTableSummary();
            }
        }
    }

    private Map<String, StagingTableSummaryDTO> getValidateStagingTableSummaryDTOMap(List<String> activeTableNames) {
        List<StagingTableSummaryDTO> optionalActiveStagingTableSummaryDTO = stagingTableSummaryService.findAllByTableNames(activeTableNames);
        return getStagingTableSummaryDTOMap(optionalActiveStagingTableSummaryDTO);
    }

    private Map<String, StagingTableSummaryDTO> getResourceStagingTableSummaryDTOMap(List<String> validateTableNames) {
        List<StagingTableSummaryDTO> optionalActiveStagingTableSummaryDTO = stagingTableSummaryService.findAllByTableNamesAndIsValidate(validateTableNames);
        return getStagingTableSummaryDTOMap(optionalActiveStagingTableSummaryDTO);
    }

    private Map<String, StagingTableSummaryDTO> getStagingTableSummaryDTOMap(List<StagingTableSummaryDTO> activeStagingTableSummaryDTOs) {
        Map<String, StagingTableSummaryDTO> stagingTableSummaryDTOMap = new HashMap<>();
        if (Optional.ofNullable(activeStagingTableSummaryDTOs).isPresent()) {
            for (StagingTableSummaryDTO stagingTableSummaryDTO : activeStagingTableSummaryDTOs) {
                stagingTableSummaryDTOMap.put(stagingTableSummaryDTO.getTableName(), stagingTableSummaryDTO);
            }
        }
        return stagingTableSummaryDTOMap;
    }

    private void updateStagingTableSummary(StagingTableSummaryDTO stagingTableSummaryDTO, String mpsName) {
        stagingTableSummaryDTO.setStartValidateDate(LocalDateTime.now());
        if (isValidateData(stagingTableSummaryDTO)) {
            updateRscIitIdpStatus(stagingTableSummaryDTO, mpsName, true, true);
            stagingTableSummaryDTO.setValidate(true);
            stagingTableSummaryDTO.setEndValidateDate(LocalDateTime.now());
        } else {
            //TODO Temperary save isValidate true
            stagingTableSummaryDTO.setValidate(false);
            updateRscIitIdpStatus(stagingTableSummaryDTO, mpsName, false, true);
        }
        stagingTableSummaryService.save(stagingTableSummaryDTO);
    }

    private void stagingToResourceTableDataProcedureCall(StagingTableSummaryDTO stagingTableSummaryDTO, String mpsName) {
        stagingTableSummaryDTO.setStartRscLoadDate(LocalDateTime.now());
        if (isStagingToResourceLoadData(stagingTableSummaryDTO)) {
            updateRscIitIdpStatus(stagingTableSummaryDTO, mpsName, true, false);
            stagingTableSummaryDTO.setRscLoaded(true);
            stagingTableSummaryDTO.setEndRscLoadDate(LocalDateTime.now());
        } else {
            stagingTableSummaryDTO.setRscLoaded(false);
            updateRscIitIdpStatus(stagingTableSummaryDTO, mpsName, false, false);
        }
        stagingTableSummaryService.save(stagingTableSummaryDTO);
    }

    private boolean isValidateData(StagingTableSummaryDTO stagingTableSummaryDTO) {
        return jpaProcedureServiceImpl.callProcedureByTableName(stagingTableSummaryDTO.getTableName(), true);
    }

    private boolean isStagingToResourceLoadData(StagingTableSummaryDTO stagingTableSummaryDTO) {
        return jpaProcedureServiceImpl.callProcedureByTableName(stagingTableSummaryDTO.getTableName(), false);
    }

    private void updateRscIitIdpStatus(StagingTableSummaryDTO stagingTableSummaryDTO, String mpsName, Boolean isSuccessProcedure, Boolean validationCall) {
        String fileName = getFileNameByTableName(stagingTableSummaryDTO.getTableName());
        if (Optional.ofNullable(fileName).isPresent()) {
            if (validationCall)
                updateIdpValidationStatus(fileName, mpsName, isSuccessProcedure);
            else
                updateIdpResourceStatus(fileName, mpsName, isSuccessProcedure);
        }
    }

    private String getFileNameByTableName(String tableName) {
        StagingTableDetailsDTO stagingTableDetailsDTO = stagingTableNameService.findOneByTableName(tableName);
        if (Optional.ofNullable(stagingTableDetailsDTO).isPresent()) {
            return getMainFileName(stagingTableDetailsDTO.getCsvFileName());
        }
        return null;
    }

    private String getMainFileName(String csvFileName) {
        ExcelFiledetailsDTO excelFiledetailsDTO = excelFileDetailsService.findOneBySheetName(getSplitString(csvFileName));
        if (Optional.ofNullable(excelFiledetailsDTO).isPresent()) {
            return excelFiledetailsDTO.getFileName();
        }
        return csvFileName;
    }

    private String getSplitString(String csvFileName) {
        return csvFileName.split("[.]")[0];
    }

    private void updateIdpValidationStatus(String fileName, String mpsName, Boolean isValidation) {
        RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusService.findIdpStatusDTOByMpsNameAndFileName(mpsName, fileName);
        if (Optional.ofNullable(rscIitIdpStatusDTO).isPresent()) {
            if (isValidation)
                rscIitIdpStatusDTO.setValidationStatus(true);
            else
                rscIitIdpStatusDTO.setValidationStatus(false);
            rscIitIdpStatusService.save(rscIitIdpStatusDTO);
            rscIitIdpStatusService.updateAllFilesModifiedDate(rscIitIdpStatusDTO.getId());
        }
    }

    private void updateIdpResourceStatus(String fileName, String mpsName, Boolean isResource) {
        RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusService.findIdpStatusDTOByMpsNameAndFileName(mpsName, fileName);
        if (Optional.ofNullable(rscIitIdpStatusDTO).isPresent()) {
            if (isResource)
                rscIitIdpStatusDTO.setResourceStatus(true);
            else
                rscIitIdpStatusDTO.setResourceStatus(false);
            rscIitIdpStatusService.save(rscIitIdpStatusDTO);
            rscIitIdpStatusService.updateAllFilesModifiedDate(rscIitIdpStatusDTO.getId());
        }
    }
}