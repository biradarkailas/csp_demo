package com.example.validation.service.impl;

import com.example.validation.domain.RscDtPMRemark;
import com.example.validation.dto.RscDtPMRemarkDTO;
import com.example.validation.mapper.RscDtPMRemarkMapper;
import com.example.validation.repository.RscDtPMRemarkRepository;
import com.example.validation.service.RscDtPMRemarkService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RscDtPMRemarkServiceImpl implements RscDtPMRemarkService {
    private final RscDtPMRemarkRepository rscDtPMRemarkRepository;
    private final RscDtPMRemarkMapper rscDtPMRemarkMapper;

    public RscDtPMRemarkServiceImpl(RscDtPMRemarkRepository rscDtPMRemarkRepository, RscDtPMRemarkMapper rscDtPMRemarkMapper) {
        this.rscDtPMRemarkRepository = rscDtPMRemarkRepository;
        this.rscDtPMRemarkMapper = rscDtPMRemarkMapper;
    }

    @Override
    public Long findRscDtPMRemarkIdByAliasName(String aliasName) {
        Optional<RscDtPMRemark> rscDtPMRemark = rscDtPMRemarkRepository.findByAliasName(aliasName);
        if (rscDtPMRemark.isPresent()) return rscDtPMRemarkMapper.toDto(rscDtPMRemark.get()).getId();
        return null;
    }

    @Override
    public List<RscDtPMRemarkDTO> findAll() {
        return rscDtPMRemarkMapper.toDto(rscDtPMRemarkRepository.findAll());
    }

    @Override
    public RscDtPMRemarkDTO save(RscDtPMRemark rscDtPMRemarkDTO) {
        return rscDtPMRemarkMapper.toDto(rscDtPMRemarkRepository.save(rscDtPMRemarkDTO));
    }
}