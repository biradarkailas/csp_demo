package com.example.validation.service.impl;

import com.example.validation.dto.ResourceTableUploadSummaryDTO;
import com.example.validation.mapper.ResourceTableUploadSummaryMapper;
import com.example.validation.repository.ResourceTableUploadSummaryRepository;
import com.example.validation.service.ResourceTableUploadSummaryService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@Transactional
public class ResourceTableUploadSummaryServiceImpl implements ResourceTableUploadSummaryService {
    private final ResourceTableUploadSummaryRepository resourceTableUploadSummaryRepository;
    private final ResourceTableUploadSummaryMapper resourceTableUploadSummaryMapper;

    public ResourceTableUploadSummaryServiceImpl(ResourceTableUploadSummaryRepository resourceTableUploadSummaryRepository, ResourceTableUploadSummaryMapper resourceTableUploadSummaryMapper) {
        this.resourceTableUploadSummaryRepository = resourceTableUploadSummaryRepository;
        this.resourceTableUploadSummaryMapper = resourceTableUploadSummaryMapper;
    }

    @Override
    public List<ResourceTableUploadSummaryDTO> findAll() {
        return resourceTableUploadSummaryMapper.toDto(resourceTableUploadSummaryRepository.findAll());
    }

    @Override
    public void deleteAll() {
        resourceTableUploadSummaryRepository.deleteAll();
    }
}