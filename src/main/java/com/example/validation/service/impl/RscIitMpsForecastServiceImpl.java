package com.example.validation.service.impl;

import com.example.validation.dto.RscIitMpsForecastDTO;
import com.example.validation.mapper.RscIitMpsForecastMapper;
import com.example.validation.repository.RscIitMpsForecastRepository;
import com.example.validation.service.RscIitMpsForecastService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscIitMpsForecastServiceImpl implements RscIitMpsForecastService {
    private final RscIitMpsForecastRepository rscIitMpsForecastRepository;
    private final RscIitMpsForecastMapper rscIitMpsForecastMapper;

    public RscIitMpsForecastServiceImpl(RscIitMpsForecastRepository rscIitMpsForecastRepository, RscIitMpsForecastMapper rscIitMpsForecastMapper) {
        this.rscIitMpsForecastRepository = rscIitMpsForecastRepository;
        this.rscIitMpsForecastMapper = rscIitMpsForecastMapper;
    }

    @Override
    public RscIitMpsForecastDTO findByFGItemIdAndPPHeaderId(Long fgItemId, Long ppHeaderId) {
        return rscIitMpsForecastMapper.toDto(rscIitMpsForecastRepository.findByRscMtItemIdAndRscMtPPheaderId(fgItemId, ppHeaderId));
    }

    @Override
    public List<RscIitMpsForecastDTO> findByBpiCodeAndPPHeaderId(String bpiCode, Long ppHeaderId) {
        return rscIitMpsForecastMapper.toDto(rscIitMpsForecastRepository.findByEightDigitCodeAndRscMtPPheaderId(bpiCode, ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIitMpsForecastRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}