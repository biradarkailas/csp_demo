package com.example.validation.service.impl;

import com.example.validation.dto.RscMtOtifPmReceiptsHeaderDTO;
import com.example.validation.mapper.RscMtOtifPmReceiptsHeaderMapper;
import com.example.validation.repository.RscMtOtifPmReceiptsHeaderRepository;
import com.example.validation.service.RscMtOtifPmReceiptsHeaderService;
import com.example.validation.util.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscMtOtifPmReceiptsHeaderServiceImpl implements RscMtOtifPmReceiptsHeaderService {
    private final RscMtOtifPmReceiptsHeaderRepository rscMtOtifPmReceiptsHeaderRepository;
    private final RscMtOtifPmReceiptsHeaderMapper rscMtOtifPmReceiptsHeaderMapper;

    public RscMtOtifPmReceiptsHeaderServiceImpl(RscMtOtifPmReceiptsHeaderRepository rscMtOtifPmReceiptsHeaderRepository, RscMtOtifPmReceiptsHeaderMapper rscMtOtifPmReceiptsHeaderMapper) {
        this.rscMtOtifPmReceiptsHeaderRepository = rscMtOtifPmReceiptsHeaderRepository;
        this.rscMtOtifPmReceiptsHeaderMapper = rscMtOtifPmReceiptsHeaderMapper;
    }

    @Override
    public RscMtOtifPmReceiptsHeaderDTO getLatestReceiptHeaderId(LocalDate mpsDate) {
        List<RscMtOtifPmReceiptsHeaderDTO> rscMtOtifPmReceiptsHeaderDTOS = rscMtOtifPmReceiptsHeaderMapper.toDto(rscMtOtifPmReceiptsHeaderRepository.findAllByOtifDateBetween(getFirstDateOfMonth(mpsDate), DateUtils.getLastDateOfMonth(mpsDate)));
        if (Optional.ofNullable(rscMtOtifPmReceiptsHeaderDTOS).isPresent()) {
            List<RscMtOtifPmReceiptsHeaderDTO> rscMtOtifPmReceiptsHeaderDTOList = rscMtOtifPmReceiptsHeaderDTOS.stream().sorted(Comparator.comparing(RscMtOtifPmReceiptsHeaderDTO::getOtifDate)).collect(Collectors.toList());
            if (rscMtOtifPmReceiptsHeaderDTOList.size() > 0) {
                return rscMtOtifPmReceiptsHeaderDTOS.get(rscMtOtifPmReceiptsHeaderDTOList.size() - 1);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    @Override
    public Optional<RscMtOtifPmReceiptsHeaderDTO> findOne(Long id) {
        return rscMtOtifPmReceiptsHeaderRepository.findById(id).map(rscMtOtifPmReceiptsHeaderMapper::toDto);
    }

    @Override
    public RscMtOtifPmReceiptsHeaderDTO findByOtifDate(LocalDate otifDate) {
        if (rscMtOtifPmReceiptsHeaderRepository.findByOtifDate(otifDate).size() > 0) {
            return rscMtOtifPmReceiptsHeaderMapper.toDto(rscMtOtifPmReceiptsHeaderRepository.findByOtifDate(otifDate).
                    get(rscMtOtifPmReceiptsHeaderRepository.findByOtifDate(otifDate).size() - 1));
        } else {
            return null;
        }
    }

    private LocalDate getFirstDateOfMonth(LocalDate mpsDate) {
        return mpsDate.with(TemporalAdjusters.firstDayOfMonth());
    }

    @Override
    public List<LocalDate> getPreviousMonthAllOtifDate() {
        List<RscMtOtifPmReceiptsHeaderDTO> rscMtOtifPmReceiptsHeaderDTOs = rscMtOtifPmReceiptsHeaderMapper.toDto(rscMtOtifPmReceiptsHeaderRepository.findAllByActive(true));
        if (Optional.ofNullable(rscMtOtifPmReceiptsHeaderDTOs).isPresent()) {
            return rscMtOtifPmReceiptsHeaderDTOs.stream().filter(rscMtOtifPmReceiptsHeaderDTO -> rscMtOtifPmReceiptsHeaderDTO.getOtifDate() != null && rscMtOtifPmReceiptsHeaderDTO.getOtifDate().getMonth().equals(java.time.LocalDate.now().minusMonths(1).getMonth())).map(RscMtOtifPmReceiptsHeaderDTO::getOtifDate).collect(Collectors.toList());
        }
        return null;
    }

    @Override
    public void deleteAll(List<Long> ids) {
        rscMtOtifPmReceiptsHeaderRepository.deleteAllByIdIn(ids);
    }

    @Override
    public List<Long> getAllIds(LocalDate otifDate) {
        if (Optional.ofNullable(otifDate).isPresent()) {
            List<RscMtOtifPmReceiptsHeaderDTO> rscMtOtifPmReceiptsHeaderDTOs = rscMtOtifPmReceiptsHeaderMapper.toDto(rscMtOtifPmReceiptsHeaderRepository.findAllByOtifDateBetween(otifDate.withDayOfMonth(1), otifDate.withDayOfMonth(otifDate.lengthOfMonth())));
            if (Optional.ofNullable(rscMtOtifPmReceiptsHeaderDTOs).isPresent()) {
                return rscMtOtifPmReceiptsHeaderDTOs.stream().map(RscMtOtifPmReceiptsHeaderDTO::getId).collect(Collectors.toList());
            }
        }
        return null;
    }
}