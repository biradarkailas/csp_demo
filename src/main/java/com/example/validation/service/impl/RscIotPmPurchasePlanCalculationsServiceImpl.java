package com.example.validation.service.impl;

import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.supply.PmSupplyCalculationsDTO;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanCalculationsDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import com.example.validation.mapper.PMPurchasePlanCalculationsMainMapper;
import com.example.validation.mapper.PMPurchasePlanCalculationsMapper;
import com.example.validation.mapper.PmSupplyCalculationsMapper;
import com.example.validation.mapper.RscIotPMPurchasePlanCalculationsMapper;
import com.example.validation.repository.RscIotPmSupplyCalculationsRepository;
import com.example.validation.service.RscIotPmPurchasePlanCalculationsService;
import com.example.validation.service.RscItPMGrossConsumptionService;
import com.example.validation.util.supply.PMPurchasePlanUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotPmPurchasePlanCalculationsServiceImpl implements RscIotPmPurchasePlanCalculationsService {
    private final RscIotPmSupplyCalculationsRepository rscIotPmSupplyCalculationsRepository;
    private final RscIotPMPurchasePlanCalculationsMapper rscIotPMPurchasePlanCalculationsMapper;
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final PMPurchasePlanUtil pmPurchasePlanUtil;
    private final PMPurchasePlanCalculationsMapper pmPurchasePlanCalculationsMapper;
    private final PmSupplyCalculationsMapper pmSupplyCalculationsMapper;
    private final PMPurchasePlanCalculationsMainMapper pmPurchasePlanCalculationsMainMapper;

    public RscIotPmPurchasePlanCalculationsServiceImpl(RscIotPmSupplyCalculationsRepository rscIotPmSupplyCalculationsRepository, RscIotPMPurchasePlanCalculationsMapper rscIotPMPurchasePlanCalculationsMapper, PMPurchasePlanUtil pmPurchasePlanUtil,
                                                       RscItPMGrossConsumptionService rscItPMGrossConsumptionService, PMPurchasePlanCalculationsMapper pmPurchasePlanCalculationsMapper,
                                                       PmSupplyCalculationsMapper pmSupplyCalculationsMapper, PMPurchasePlanCalculationsMainMapper pmPurchasePlanCalculationsMainMapper) {
        this.rscIotPmSupplyCalculationsRepository = rscIotPmSupplyCalculationsRepository;
        this.rscIotPMPurchasePlanCalculationsMapper = rscIotPMPurchasePlanCalculationsMapper;
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.pmPurchasePlanUtil = pmPurchasePlanUtil;
        this.pmPurchasePlanCalculationsMapper = pmPurchasePlanCalculationsMapper;
        this.pmSupplyCalculationsMapper = pmSupplyCalculationsMapper;
        this.pmPurchasePlanCalculationsMainMapper = pmPurchasePlanCalculationsMainMapper;
    }

    @Override
    public Boolean createPMSupplyCalculations(Long ppHeaderId) {
        if (rscIotPmSupplyCalculationsRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotPMPurchasePlanCalculationsDTO> rscIotPMPurchasePlanCalculationsDTOS = new ArrayList<>();
            List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs = rscItPMGrossConsumptionService.findAllPMGrossConsumptionByRscMtPPheaderId(ppHeaderId);
            for (RscItGrossConsumptionDTO rscItGrossConsumptionDTO : rscItGrossConsumptionDTOs) {
                if (Optional.ofNullable(rscItGrossConsumptionDTO).isPresent()) {
                    RscIotPMPurchasePlanCalculationsDTO rscIotPMPurchasePlanCalculationsDTO = pmPurchasePlanUtil.getRscIotPmSupplyCalculationsDTO(rscItGrossConsumptionDTO);
                    if (Optional.ofNullable(rscIotPMPurchasePlanCalculationsDTO).isPresent())
                        rscIotPMPurchasePlanCalculationsDTOS.add(rscIotPMPurchasePlanCalculationsDTO);
                }
            }
            rscIotPmSupplyCalculationsRepository.saveAll(rscIotPMPurchasePlanCalculationsMapper.toEntity(rscIotPMPurchasePlanCalculationsDTOS));
        }
        return true;
    }

    @Override
    public List<RscIotPMPurchasePlanDTO> getPMSupplyCalculationsDTOByPPheaderId(Long ppHeaderId) {
        List<RscIotPMPurchasePlanDTO> rscIotPMPurchasePlanDTOS = pmPurchasePlanCalculationsMapper.toDto(rscIotPmSupplyCalculationsRepository.findAllByRscMtPPheaderId(ppHeaderId));
        for (RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO : rscIotPMPurchasePlanDTOS) {
            rscIotPMPurchasePlanDTO.setRscDtPMRemarkId(pmPurchasePlanUtil.getRscDtPMRemarkId(rscIotPMPurchasePlanDTO));
        }
        return rscIotPMPurchasePlanDTOS;
    }

    @Override
    public PmSupplyCalculationsDTO getPMSupplyCalculations(Long rscMtItemId, Long ppHeaderId) {
        return pmSupplyCalculationsMapper.toDto(rscIotPmSupplyCalculationsRepository.findByRscMtItemIdAndRscMtPPheaderId(rscMtItemId, ppHeaderId));
    }

    @Override
    public List<PurchasePlanCalculationsMainDTO> findAll(Long ppHeaderId) {
        return pmPurchasePlanCalculationsMainMapper.toDto(rscIotPmSupplyCalculationsRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotPmSupplyCalculationsRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}