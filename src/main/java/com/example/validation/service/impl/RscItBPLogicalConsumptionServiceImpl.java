package com.example.validation.service.impl;

import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.mapper.BPLogicalConsumptionMapper;
import com.example.validation.mapper.RscItBPLogicalConsumptionMapper;
import com.example.validation.mapper.RscItBPUniqueLogicalConsumptionMapper;
import com.example.validation.repository.RscItBPLogicalConsumptionRepository;
import com.example.validation.repository.RscItBPUniqueLogicalConsumptionRepository;
import com.example.validation.service.RscItBPLogicalConsumptionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class RscItBPLogicalConsumptionServiceImpl implements RscItBPLogicalConsumptionService {
    private final RscItBPLogicalConsumptionRepository rscItBPLogicalConsumptionRepository;
    private final RscItBPLogicalConsumptionMapper rscItBPLogicalConsumptionMapper;
    private final RscItBPUniqueLogicalConsumptionRepository rscItBPUniqueLogicalConsumptionRepository;
    private final RscItBPUniqueLogicalConsumptionMapper rscItBPUniqueLogicalConsumptionMapper;
    private final BPLogicalConsumptionMapper bpLogicalConsumptionMapper;

    public RscItBPLogicalConsumptionServiceImpl(RscItBPLogicalConsumptionRepository rscItBPLogicalConsumptionRepository, RscItBPLogicalConsumptionMapper rscItBPLogicalConsumptionMapper, BPLogicalConsumptionMapper bpLogicalConsumptionMapper, RscItBPUniqueLogicalConsumptionRepository rscItBPUniqueLogicalConsumptionRepository, RscItBPUniqueLogicalConsumptionMapper rscItBPUniqueLogicalConsumptionMapper) {
        this.rscItBPLogicalConsumptionRepository = rscItBPLogicalConsumptionRepository;
        this.rscItBPLogicalConsumptionMapper = rscItBPLogicalConsumptionMapper;
        this.bpLogicalConsumptionMapper = bpLogicalConsumptionMapper;
        this.rscItBPUniqueLogicalConsumptionRepository = rscItBPUniqueLogicalConsumptionRepository;
        this.rscItBPUniqueLogicalConsumptionMapper = rscItBPUniqueLogicalConsumptionMapper;
    }

    @Override
    public void saveAll(List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOs, Long ppHeaderId) {
        if (rscItBPLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            bpRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
            bpRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
            rscItBPLogicalConsumptionRepository.saveAll(rscItBPLogicalConsumptionMapper.toEntity(bpRscItLogicalConsumptionDTOs));
        }
    }

    @Override
    public void saveAllUnique(List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOs, Long ppHeaderId) {
        if (rscItBPUniqueLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            bpRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
            bpRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
            rscItBPUniqueLogicalConsumptionRepository.saveAll(rscItBPUniqueLogicalConsumptionMapper.toEntity(bpRscItLogicalConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItBPLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<LogicalConsumptionMainDTO> getBPLogicalConsumptionByBulk(Long ppHeaderId, Long bpMtItemId) {
        return bpLogicalConsumptionMapper.toDto(rscItBPUniqueLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemParentId(ppHeaderId, bpMtItemId));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId) {
        return rscItBPLogicalConsumptionMapper.toDto(rscItBPLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<LogicalConsumptionMainDTO> getBPLogicalConsumption(Long ppHeaderId) {
        return bpLogicalConsumptionMapper.toDto(rscItBPUniqueLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderIdAndChildItemId(Long ppHeaderId, Long bpItemId) {
        return rscItBPLogicalConsumptionMapper.toDto(rscItBPLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemChildId(ppHeaderId, bpItemId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItBPLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
        rscItBPUniqueLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}