package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.domain.RscMtPPdetails;
import com.example.validation.dto.*;
import com.example.validation.mapper.FgItemCodedetailsMapper;
import com.example.validation.mapper.MpsPlanMapper;
import com.example.validation.mapper.RscMtPPdetailsMapper;
import com.example.validation.repository.RscMtPpdetailsRepository;
import com.example.validation.service.RscItWIPGrossConsumptionService;
import com.example.validation.service.RscMtPpdetailsService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.specification.RscMtPPDetailsCriteria;
import com.example.validation.specification.RscMtPPDetailsSpecification;
import com.example.validation.util.excel_export.PmMpsPlanExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscMtPpdetailsServiceImpl implements RscMtPpdetailsService {
    private final RscMtPpdetailsRepository rscMtPpdetailsRepository;
    private final RscMtPPdetailsMapper rscMtPPdetailsMapper;
    private final MpsPlanMapper mpsPlanMapper;
    private final RscMtPpheaderService rscMtPpHeaderService;
    private final FgItemCodedetailsMapper fgItemCodedetailsMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService;
    private final RscMtPPDetailsSpecification rscMtPPDetailsSpecification;

    public RscMtPpdetailsServiceImpl(RscMtPpdetailsRepository rscMtPpdetailsRepository, RscMtPPdetailsMapper rscMtPPdetailsMapper, MpsPlanMapper mpsPlanMapper, RscMtPpheaderService rscMtPpHeaderService, FgItemCodedetailsMapper fgItemCodedetailsMapper, RscMtPpheaderService rscMtPpheaderService, RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService, RscMtPPDetailsSpecification rscMtPPDetailsSpecification) {
        this.rscMtPpdetailsRepository = rscMtPpdetailsRepository;
        this.rscMtPPdetailsMapper = rscMtPPdetailsMapper;
        this.mpsPlanMapper = mpsPlanMapper;
        this.rscMtPpHeaderService = rscMtPpHeaderService;
        this.fgItemCodedetailsMapper = fgItemCodedetailsMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscItWIPGrossConsumptionService = rscItWIPGrossConsumptionService;
        this.rscMtPPDetailsSpecification = rscMtPPDetailsSpecification;
    }

    @Override
    public MpsPlanMainDTO getProductionPlan(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne(ppHeaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            return getMPSPlanMainDTO(optionalRscMtPpHeaderDTO.get());
        }
        return null;
    }

    @Override
    public MpsPlanMainDTO getLatestProductionPlan() {
        List<RscMtPPheaderDTO> rscMtPPheaderDTOList = rscMtPpHeaderService.findAllPPheaderByActive();
        if (Optional.ofNullable(rscMtPPheaderDTOList).isPresent() && rscMtPPheaderDTOList.size() > 0) {
            return getMPSPlanMainDTO(rscMtPPheaderDTOList.get(rscMtPPheaderDTOList.size() - 1));
        }
        return null;
    }

    @Override
    public MpsPlanMainDTO getMPSPlanMainDTO(RscMtPPheaderDTO rscMtPpHeaderDTO) {
        MpsPlanMainDTO mpsPlanMainDTO = new MpsPlanMainDTO();
        List<ItemDetailDTO> itemDetailDTOs = mpsPlanMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderId(rscMtPpHeaderDTO.getId()));
        mpsPlanMainDTO.setMpsId(rscMtPpHeaderDTO.getId());
        mpsPlanMainDTO.setMpsName(rscMtPpHeaderDTO.getMpsName());
        mpsPlanMainDTO.setMpsDate(rscMtPpHeaderDTO.getMpsDate());
        mpsPlanMainDTO.setBpiCodes(itemDetailDTOs);
        return mpsPlanMainDTO;
    }

    @Override
    public RscMtPPdetailsDTO findOneRscMtPPdetailsDTO(Long ppHeaderId, Long fgItemId) {
        return rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, fgItemId));
    }

    @Override
    public List<RscMtPPdetailsDTO> findAllRscMtPPdetailsDTO(Long ppHeaderId, List<Long> fgItemIds) {
        return rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findByRscMtPPheaderIdAndRscMtItemIdIn(ppHeaderId, fgItemIds));
    }

    @Override
    public ItemDetailDTO getMPSPlanByFG(Long ppHeaderId, Long fgItemId) {
        ItemDetailDTO fgItemDetailDTO = mpsPlanMapper.toDto(rscMtPpdetailsRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, fgItemId));
        ItemDetailDTO wipItemDetailDTO = rscItWIPGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, fgItemId);
        if (Optional.ofNullable(fgItemDetailDTO).isPresent()) return fgItemDetailDTO;
        return wipItemDetailDTO;
    }

    @Override
    public List<ItemDetailDTO> getCodeMPSPlanByFG(Long ppHeaderId) {
        List<ItemDetailDTO> fgItemDetailList = new ArrayList<>();
        List<ItemDetailDTO> fgCodeItemDetailDTO = mpsPlanMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderId(ppHeaderId));
        List<ItemDetailDTO> wipCodeItemDetailDTO = rscItWIPGrossConsumptionService.findByAllRscMtPPheaderId(ppHeaderId);
        fgItemDetailList.addAll(fgCodeItemDetailDTO);
        fgItemDetailList.addAll(wipCodeItemDetailDTO);
        return fgItemDetailList;
    }

    @Override
    public List<RscMtPPdetailsDTO> findAllPPdetailsByPPheader(Long ppHeaderId) {
        return rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscMtPPdetailsDTO> findAllPPdetailsByPPheaderAndNoCoPackingItem(Long ppHeaderId) {
        List<RscMtPPdetailsDTO> rscMtPPdetailsDTOs = rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderId(ppHeaderId));
        return rscMtPPdetailsDTOs.stream().filter(rscMtPPdetailsDTO -> !rscMtPPdetailsDTO.getLineName().equals("COPACKING")).collect(Collectors.toList());
    }

    @Override
    public ItemCodeDTO getFGItemCodeDTO(Long ppHeaderId) {
        ItemCodeDTO itemCodeDTO = new ItemCodeDTO();
        List<RscMtPPdetails> rscMtPPdetailsList = rscMtPpdetailsRepository.findAllByRscMtPPheaderId(ppHeaderId);
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne(ppHeaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            itemCodeDTO.setMpsId(optionalRscMtPpHeaderDTO.get().getId());
            itemCodeDTO.setMpsName(optionalRscMtPpHeaderDTO.get().getMpsName());
            itemCodeDTO.setMpsDate(optionalRscMtPpHeaderDTO.get().getMpsDate());
            itemCodeDTO.setItemCodes(fgItemCodedetailsMapper.toDto(rscMtPPdetailsList));
        }
        return itemCodeDTO;
    }

    @Override
    public List<ItemDetailDTO> getItemDetailDTOList(Long ppheaderId) {
        return mpsPlanMapper.toDto(rscMtPpdetailsRepository.findAll());
    }

    @Override
    public List<Long> findUniqueRscDtLines(Long ppHeaderId) {
        return rscMtPpdetailsRepository.findAllUniqueRscDtLines(ppHeaderId);
    }

    @Override
    public List<Long> findUniqueRscDtSkids(Long ppHeaderId) {
        return rscMtPpdetailsRepository.findAllUniqueRscDtSkids(ppHeaderId);
    }

    @Override
    public ExcelExportFiles mpsPlanToExcel(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, MpsPlanMainDTO mpsPlanMainDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmMpsPlanExporter.mpsPlanToExcel(mpsPlanMainDTO, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_MPS_PLAN + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<RscMtPPdetailsDTO> findAllByLinesIdAndRscMtPPheaderId(Long rscDtLinesId, Long ppHeaderId) {
        return rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscDtLinesIdAndRscMtPPheaderId(rscDtLinesId, ppHeaderId));
    }

    @Override
    public List<RscMtPPdetailsDTO> findAllBySkidsIdAndRscMtPPheaderId(Long rscDtSkidsId, Long ppHeaderId) {
        return rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscDtSkidsIdAndRscMtPPheaderId(rscDtSkidsId, ppHeaderId));
    }

    @Override
    public MonthValuesDTO getMPSPlanSummaryByDivision(Long ppHeaderId, Long divisionId, Long signatureId, Long brandId) {
        MonthValuesDTO rscMtPPdetailsDTOS = new MonthValuesDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            rscMtPPdetailsDTOS = (getPpdeatilsList(ppHeaderId, divisionId, signatureId, brandId));
        }
        return rscMtPPdetailsDTOS;
    }

    private MonthValuesDTO getPpdeatilsList(Long ppHeaderId, Long divisionId, Long signatureId, Long brandId) {
        if (Optional.ofNullable(divisionId).isPresent() && Optional.ofNullable(signatureId).isPresent() && Optional.ofNullable(brandId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtDivisionIdAndRscDtSignatureIdAndRscDtBrandId(ppHeaderId, divisionId, signatureId, brandId)));
        } else if (Optional.ofNullable(divisionId).isPresent() && Optional.ofNullable(signatureId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtDivisionIdAndRscDtSignatureId(ppHeaderId, divisionId, signatureId)));
        } else if (Optional.ofNullable(divisionId).isPresent() && Optional.ofNullable(brandId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtDivisionIdAndRscDtBrandId(ppHeaderId, divisionId, brandId)));
        } else if (Optional.ofNullable(signatureId).isPresent() && Optional.ofNullable(brandId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtSignatureIdAndRscDtBrandId(ppHeaderId, signatureId, brandId)));
        } else if (Optional.ofNullable(divisionId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtDivisionId(ppHeaderId, divisionId)));
        } else if (Optional.ofNullable(signatureId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtSignatureId(ppHeaderId, signatureId)));
        } else if (Optional.ofNullable(brandId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtBrandId(ppHeaderId, brandId)));
        } else {
            return new MonthValuesDTO();
        }
    }

    private MonthValuesDTO getAverageOfPpdetails(List<RscMtPPdetailsDTO> pPdetailsDTOList) {
        MonthValuesDTO valuesDTO = new MonthValuesDTO();
        valuesDTO.setMonth1Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM1Value() != null).mapToDouble(RscMtPPdetailsDTO::getM1Value).sum());
        valuesDTO.setMonth2Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM2Value() != null).mapToDouble(RscMtPPdetailsDTO::getM2Value).sum());
        valuesDTO.setMonth3Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM3Value() != null).mapToDouble(RscMtPPdetailsDTO::getM3Value).sum());
        valuesDTO.setMonth4Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM4Value() != null).mapToDouble(RscMtPPdetailsDTO::getM4Value).sum());
        valuesDTO.setMonth5Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM5Value() != null).mapToDouble(RscMtPPdetailsDTO::getM5Value).sum());
        valuesDTO.setMonth6Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM6Value() != null).mapToDouble(RscMtPPdetailsDTO::getM6Value).sum());
        valuesDTO.setMonth7Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM7Value() != null).mapToDouble(RscMtPPdetailsDTO::getM7Value).sum());
        valuesDTO.setMonth8Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM8Value() != null).mapToDouble(RscMtPPdetailsDTO::getM8Value).sum());
        valuesDTO.setMonth9Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM9Value() != null).mapToDouble(RscMtPPdetailsDTO::getM9Value).sum());
        valuesDTO.setMonth10Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM10Value() != null).mapToDouble(RscMtPPdetailsDTO::getM10Value).sum());
        valuesDTO.setMonth11Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM11Value() != null).mapToDouble(RscMtPPdetailsDTO::getM11Value).sum());
        valuesDTO.setMonth12Value(pPdetailsDTOList.stream().filter(rscMtPPdetailsDTO -> rscMtPPdetailsDTO.getM12Value() != null).mapToDouble(RscMtPPdetailsDTO::getM12Value).sum());
        return valuesDTO;
    }

    @Override
    public MonthValuesDTO getMPSPlanSummaryByCategory(Long ppHeaderId, Long categoryId, Long subCategoryId) {
        MonthValuesDTO monthValuesDTO = new MonthValuesDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            monthValuesDTO = getPpdeatilsListByCategory(ppHeaderId, categoryId, subCategoryId);
        }
        return monthValuesDTO;
    }

    private MonthValuesDTO getPpdeatilsListByCategory(Long ppHeaderId, Long categoryId, Long subCategoryId) {
        if (Optional.ofNullable(categoryId).isPresent() && Optional.ofNullable(subCategoryId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtMaterialCategoryIdAndRscDtMaterialSubCategoryId(ppHeaderId, categoryId, subCategoryId)));
        } else if (Optional.ofNullable(categoryId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtMaterialCategoryId(ppHeaderId, categoryId)));
        } else if (Optional.ofNullable(subCategoryId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtMaterialSubCategoryId(ppHeaderId, subCategoryId)));
        } else {
            return new MonthValuesDTO();
        }
    }

    @Override
    public MonthValuesDTO getMPSPlanSummaryByLinesAndSkids(Long ppHeaderId, Long rscDtLinesId, Long rscDtSkidsId) {
        MonthValuesDTO monthValuesDTO = new MonthValuesDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            monthValuesDTO = getPpdeatilsListByLinesAndSkids(ppHeaderId, rscDtLinesId, rscDtSkidsId);
        }
        return monthValuesDTO;
    }

    private MonthValuesDTO getPpdeatilsListByLinesAndSkids(Long ppHeaderId, Long rscDtLinesId, Long rscDtSkidsId) {
        if (Optional.ofNullable(rscDtLinesId).isPresent() && Optional.ofNullable(rscDtSkidsId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtLinesIdAndRscDtSkidsId(ppHeaderId, rscDtLinesId, rscDtSkidsId)));
        } else if (Optional.ofNullable(rscDtLinesId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtLinesId(ppHeaderId, rscDtLinesId)));
        } else if (Optional.ofNullable(rscDtSkidsId).isPresent()) {
            return getAverageOfPpdetails(rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderIdAndRscDtSkidsId(ppHeaderId, rscDtSkidsId)));
        } else {
            return new MonthValuesDTO();
        }
    }

    @Override
    public MpsPlanSummaryDTO getPPdetailsListForMpsPlanSummary(Long ppHeaderId) {
        MpsPlanSummaryDTO mpsPlanSummaryDTO = new MpsPlanSummaryDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            List<RscMtPPdetailsDTO> pPdetailsList = rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAllByRscMtPPheaderId(ppHeaderId));
            if (Optional.ofNullable(pPdetailsList).isPresent()) {
                mpsPlanSummaryDTO.setPpHeaderId(rscMtPPheaderDTO.getId());
                mpsPlanSummaryDTO.setDivisionList(getUniqueDivisionsList(pPdetailsList));
                mpsPlanSummaryDTO.setSignatureList(getUniqueSignaturesList(pPdetailsList));
                mpsPlanSummaryDTO.setBrandList(getUniqueBrandsList(pPdetailsList));
                mpsPlanSummaryDTO.setCategory1List(getUniqueCategory1List(pPdetailsList));
                mpsPlanSummaryDTO.setCategory2List(getUniqueCategory2List(pPdetailsList));
                mpsPlanSummaryDTO.setLinesList(getUniqueLinesList(pPdetailsList));
            }
        }
        return mpsPlanSummaryDTO;
    }

    private List getUniqueDivisionsList(List<RscMtPPdetailsDTO> pPdetailsList) {
        return pPdetailsList.stream().map(RscMtPPdetailsDTO::getDivisionName).distinct().collect(Collectors.toList());
    }

    private List getUniqueSignaturesList(List<RscMtPPdetailsDTO> pPdetailsList) {
        return pPdetailsList.stream().map(RscMtPPdetailsDTO::getSignatureName).distinct().collect(Collectors.toList());
    }

    private List getUniqueBrandsList(List<RscMtPPdetailsDTO> pPdetailsList) {
        return pPdetailsList.stream().map(RscMtPPdetailsDTO::getBrandName).distinct().collect(Collectors.toList());
    }

    private List getUniqueCategory1List(List<RscMtPPdetailsDTO> pPdetailsList) {
        return pPdetailsList.stream().map(RscMtPPdetailsDTO::getMaterialCategoryName).distinct().collect(Collectors.toList());
    }

    private List getUniqueCategory2List(List<RscMtPPdetailsDTO> pPdetailsList) {
        return pPdetailsList.stream().map(RscMtPPdetailsDTO::getMaterialSubCategoryName).distinct().collect(Collectors.toList());
    }

    private List getUniqueLinesList(List<RscMtPPdetailsDTO> pPdetailsList) {
        return pPdetailsList.stream().map(RscMtPPdetailsDTO::getLineName).distinct().collect(Collectors.toList());
    }

    @Override
    public List<RscMtPPdetailsDTO> findAllByCriteria(RscMtPPDetailsCriteria criteria) {
        return rscMtPPdetailsMapper.toDto(rscMtPpdetailsRepository.findAll(rscMtPPDetailsSpecification.createSpecification(criteria)));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscMtPpdetailsRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}