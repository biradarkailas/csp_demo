package com.example.validation.service.impl;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.service.EmailService;
import com.example.validation.service.ErrorLogService;
import com.example.validation.util.FileOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;

@Service
public class EmailServiceImpl implements EmailService {

    private final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);
    private final JavaMailSender emailSender;
    private final ErrorLogService errorLogService;

    public EmailServiceImpl(JavaMailSender emailSender, ErrorLogService errorLogService) {
        this.emailSender = emailSender;
        this.errorLogService = errorLogService;
    }

    public boolean exportErrorLogData() throws IOException {
        String fileName = FileOperation.createFileNameWithTimestamp(ApplicationConstants.ERROR_FILE_NAME, ApplicationConstants.CSV_FILE_EXTENSION);
        String filePath = ApplicationConstants.ERROR_FILE_DIRECTORY_PATH + "/" + fileName;
        FileOperation.createDirectoryIfNotExits(ApplicationConstants.ERROR_FILE_DIRECTORY_PATH);
        if (errorLogService.exportErrorLogData(filePath)) {
            sendEmail(filePath, fileName);
            return true;
        } else {
            return false;
        }
    }

    private void sendEmail(String filePath, String fileName) {
        try {
            MimeMessage message = emailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message, true);
            //helper.setTo(ApplicationConstants.EMAIL_TO);
            //helper.setSubject(ApplicationConstants.EMAIL_SUBJECT);
            //helper.setText(ApplicationConstants.EMAIL_BODY);
            FileSystemResource file = new FileSystemResource(new File(filePath));
            helper.addAttachment(fileName, file);
            emailSender.send(message);
        } catch (MessagingException me) {
            log.debug("Exception::", me);
        }
    }
}