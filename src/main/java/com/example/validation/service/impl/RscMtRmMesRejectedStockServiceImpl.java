package com.example.validation.service.impl;

import com.example.validation.dto.supply.RscMtRmMesRejectedStockDTO;
import com.example.validation.mapper.RscMtRmMesRejectedStockMapper;
import com.example.validation.repository.RscMtRmMesRejectedStockRepository;
import com.example.validation.service.RscMtRmMesRejectedStockService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtRmMesRejectedStockServiceImpl implements RscMtRmMesRejectedStockService {
    private final RscMtRmMesRejectedStockRepository rscMtRmMesRejectedStockRepository;
    private final RscMtRmMesRejectedStockMapper rscMtRmMesRejectedStockMapper;

    public RscMtRmMesRejectedStockServiceImpl(RscMtRmMesRejectedStockRepository rscMtRmMesRejectedStockRepository, RscMtRmMesRejectedStockMapper rscMtRmMesRejectedStockMapper) {
        this.rscMtRmMesRejectedStockRepository = rscMtRmMesRejectedStockRepository;
        this.rscMtRmMesRejectedStockMapper = rscMtRmMesRejectedStockMapper;
    }

    @Override
    public List<RscMtRmMesRejectedStockDTO> findAll() {
        return rscMtRmMesRejectedStockMapper.toDto(rscMtRmMesRejectedStockRepository.findAll());
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtRmMesRejectedStockRepository.deleteAllByStockDate(stockDate);
    }
}