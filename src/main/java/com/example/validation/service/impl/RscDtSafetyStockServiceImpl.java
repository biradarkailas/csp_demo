package com.example.validation.service.impl;

import com.example.validation.dto.RscDtSafetyStockDTO;
import com.example.validation.mapper.RscDtSafetyStockMapper;
import com.example.validation.repository.RscDtSafetyStockRepository;
import com.example.validation.service.RscDtSafetyStockService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtSafetyStockServiceImpl implements RscDtSafetyStockService {
    private final RscDtSafetyStockRepository rscDtSafetyStockRepository;
    private final RscDtSafetyStockMapper rscDtSafetyStockMapper;

    public RscDtSafetyStockServiceImpl(RscDtSafetyStockRepository rscDtSafetyStockRepository, RscDtSafetyStockMapper rscDtSafetyStockMapper){
        this.rscDtSafetyStockRepository = rscDtSafetyStockRepository;
        this.rscDtSafetyStockMapper = rscDtSafetyStockMapper;
    }

    @Override
    public List<RscDtSafetyStockDTO> findAll() {
        return rscDtSafetyStockMapper.toDto(rscDtSafetyStockRepository.findAll());
    }
}
