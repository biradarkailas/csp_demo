package com.example.validation.service.impl;

import com.example.validation.dto.RscDtItemTypeDTO;
import com.example.validation.mapper.RscDtItemTypeMapper;
import com.example.validation.repository.RscDtItemTypeRepository;
import com.example.validation.service.RscDtItemTypeService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RscDtItemTypeServiceImpl implements RscDtItemTypeService {
    private final RscDtItemTypeRepository rscDtItemTypeRepository;
    private final RscDtItemTypeMapper rscDtItemTypeMapper;

    public RscDtItemTypeServiceImpl(RscDtItemTypeRepository rscDtItemTypeRepository, RscDtItemTypeMapper rscDtItemTypeMapper) {
        this.rscDtItemTypeRepository = rscDtItemTypeRepository;
        this.rscDtItemTypeMapper = rscDtItemTypeMapper;
    }

    @Override
    public List<RscDtItemTypeDTO> findAllWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of(pageNo, pageSize);
        Page<RscDtItemTypeDTO> RscDtItemTypeList = rscDtItemTypeRepository.findAll(paging).map(rscDtItemTypeMapper::toDto);
        if (RscDtItemTypeList.hasContent()) {
            return RscDtItemTypeList.getContent();
        } else return new ArrayList<>();
    }
}
