package com.example.validation.service.impl;

import com.example.validation.dto.RscDtTransportModeDTO;
import com.example.validation.mapper.RscDtTransportModeMapper;
import com.example.validation.repository.RscDtTransportModeRepository;
import com.example.validation.service.RscDtTransportModeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtTransportModeServiceImpl implements RscDtTransportModeService {
    private final RscDtTransportModeRepository rscDtTransportModeRepository;
    private final RscDtTransportModeMapper rscDtTransportModeMapper;

    public RscDtTransportModeServiceImpl(RscDtTransportModeRepository rscDtTransportModeRepository, RscDtTransportModeMapper rscDtTransportModeMapper){
        this.rscDtTransportModeRepository = rscDtTransportModeRepository;
        this.rscDtTransportModeMapper = rscDtTransportModeMapper;
    }

    @Override
    public List<RscDtTransportModeDTO> findAll() {
        return rscDtTransportModeMapper.toDto(rscDtTransportModeRepository.findAll());
    }
}
