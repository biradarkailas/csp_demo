package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ErrorLogResponseDTO;
import com.example.validation.dto.ExecutionResponseMainDTO;
import com.example.validation.mapper.ErrorLogResponseMapper;
import com.example.validation.repository.ErrorLogRepository;
import com.example.validation.service.*;
import com.example.validation.util.StringUtil;
import com.example.validation.util.excel_export.ExecutionResponseExporter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class ErrorLogServiceImpl implements ErrorLogService {
  private final Logger log = LoggerFactory.getLogger(ErrorLogServiceImpl.class);
  private final ErrorLogRepository errorLogRepository;
  private final ErrorLogResponseMapper errorLogResponseMapper;
  private final ResourceErrorLogService resourceErrorLogService;
  private final ResourceTableUploadSummaryService resourceTableUploadSummaryService;
  private final ValidationTableLoadingDetailsService validationTableLoadingDetailsService;
  private final StagingTableSummaryService stagingTableSummaryService;

  public ErrorLogServiceImpl(
      ErrorLogRepository errorLogRepository,
      ErrorLogResponseMapper errorLogResponseMapper,
      ResourceErrorLogService resourceErrorLogService,
      ResourceTableUploadSummaryService resourceTableUploadSummaryService,
      ValidationTableLoadingDetailsService validationTableLoadingDetailsService,
      StagingTableSummaryService stagingTableSummaryService) {
    this.errorLogRepository = errorLogRepository;
    this.errorLogResponseMapper = errorLogResponseMapper;
    this.resourceErrorLogService = resourceErrorLogService;
    this.resourceTableUploadSummaryService = resourceTableUploadSummaryService;
    this.validationTableLoadingDetailsService = validationTableLoadingDetailsService;
    this.stagingTableSummaryService = stagingTableSummaryService;
  }

  @Override
  public List<ErrorLogResponseDTO> findAll() {
    log.debug("Request to get all ErrorLog");
    return errorLogResponseMapper.toDto(errorLogRepository.findAll());
  }

  @Override
  public void deleteAll() {
    log.debug("Request to delete all Log Data");
    errorLogRepository.deleteAll();
    resourceErrorLogService.deleteAll();
    resourceTableUploadSummaryService.deleteAll();
    validationTableLoadingDetailsService.deleteAll();
    stagingTableSummaryService.deleteAll(LocalDate.now().minusMonths(1));
  }

  @Override
  public boolean dataSanitization(String tableName) {
    try {
      return StringUtil.isEqual(errorLogRepository.sanitization(tableName));
    } catch (Exception e) {
      log.info("Error Log Export SQL Exception");
    }
    return false;
  }

  @Override
  public boolean exportErrorLogData(String filePath) {
    try {
      return StringUtil.isEqual(errorLogRepository.exportErrorLogData(filePath));
    } catch (Exception e) {
      log.info("Error Log Export SQL Exception");
    }
    return false;
  }

  @Override
  public void truncateTable(String tableName) {
    try {
      System.out.println("Table Truncated ::" + tableName);
      errorLogRepository.truncateTable(tableName);
    } catch (Exception e) {
      log.info("Table Truncate SQL Exception");
    }
  }

  @Override
  public List<ErrorLogResponseDTO> findAllErrorLogResponseDTO() {
    List<ErrorLogResponseDTO> result = new ArrayList<>();
    List<ErrorLogResponseDTO> errorLogResponseDTOs =
        errorLogResponseMapper.toDto(errorLogRepository.findAll());
    for (ErrorLogResponseDTO errorLogResponseDTO : errorLogResponseDTOs) {
      String errorDescription = errorLogResponseDTO.getErrorDescription();
      if (errorDescription.startsWith("Warning")) {
        errorLogResponseDTO.setErrorDescription(errorDescription.substring(10));
        errorLogResponseDTO.setErrorType("Warning");
      } else {
        errorLogResponseDTO.setErrorType("Error");
      }
      result.add(errorLogResponseDTO);
    }
    result.sort(Comparator.comparing(ErrorLogResponseDTO::getErrorType));
    return result;
  }

  @Override
  public void createExecutionResponseFile(ExecutionResponseMainDTO executionResponseMainDTO) {
    String mpsDate = executionResponseMainDTO.getMpsDate().toString();
    createExcelFile(
        mpsDate,
        executionResponseMainDTO.getMpsName(),
        ExecutionResponseExporter.createExecutionResponseExcel(
            mpsDate, executionResponseMainDTO.getExecutionResponseDTOs()));
  }

  private void createExcelFile(String mpsDate, String mpsName, InputStream inputStream) {
    try {
      File file =
          new File(
              ExcelFileNameConstants.EXECUTION_ERROR_PATH
                  + mpsDate
                  + "/"
                  + mpsName
                  + "_ErrorLog"
                  + ExcelFileNameConstants.Extension);
      file.getParentFile().mkdirs();
      FileOutputStream fileOut = new FileOutputStream(file);
      fileOut.write(inputStream.readAllBytes());
    } catch (IOException e) {
      throw new RuntimeException("Fail to create to Excel file: " + e.getMessage());
    }
  }
}
