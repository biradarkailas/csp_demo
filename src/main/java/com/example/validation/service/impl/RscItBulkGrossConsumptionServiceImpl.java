package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.BulkGrossConsumptionMapper;
import com.example.validation.mapper.BulkItemCodedetailsMapper;
import com.example.validation.mapper.RscItBulkGrossConsumptionMapper;
import com.example.validation.repository.RscItBulkGrossConsumptionRepository;
import com.example.validation.service.RscItBPGrossConsumptionService;
import com.example.validation.service.RscItBulkGrossConsumptionService;
import com.example.validation.service.RscItSubBaseGrossConsumptionService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.excel_export.RmBulkGrossConsumptionExporter;
import com.example.validation.util.consumption.ConsumptionUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscItBulkGrossConsumptionServiceImpl implements RscItBulkGrossConsumptionService {
    private final RscItBulkGrossConsumptionRepository rscItBulkGrossConsumptionRepository;
    private final RscItBulkGrossConsumptionMapper rscItBulkGrossConsumptionMapper;
    private final BulkGrossConsumptionMapper bulkGrossConsumptionMapper;
    private final BulkItemCodedetailsMapper bulkItemCodedetailsMapper;
    private final ConsumptionUtil consumptionUtil;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscMtPpheaderService rscMtPpHeaderService;
    private final RscItBPGrossConsumptionService rscItBPGrossConsumptionService;
    private final RscItSubBaseGrossConsumptionService rscItSubBaseGrossConsumptionService;

    public RscItBulkGrossConsumptionServiceImpl(RscItBulkGrossConsumptionRepository rscItBulkGrossConsumptionRepository, RscItBulkGrossConsumptionMapper rscItBulkGrossConsumptionMapper, ConsumptionUtil consumptionUtil, RscMtPpheaderService rscMtPpheaderService, BulkGrossConsumptionMapper bulkGrossConsumptionMapper, RscMtPpheaderService rscMtPpHeaderService, BulkItemCodedetailsMapper bulkItemCodedetailsMapper, RscItBPGrossConsumptionService rscItBPGrossConsumptionService, RscItSubBaseGrossConsumptionService rscItSubBaseGrossConsumptionService) {
        this.rscItBulkGrossConsumptionRepository = rscItBulkGrossConsumptionRepository;
        this.rscItBulkGrossConsumptionMapper = rscItBulkGrossConsumptionMapper;
        this.consumptionUtil = consumptionUtil;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.bulkGrossConsumptionMapper = bulkGrossConsumptionMapper;
        this.rscMtPpHeaderService = rscMtPpHeaderService;
        this.bulkItemCodedetailsMapper = bulkItemCodedetailsMapper;
        this.rscItBPGrossConsumptionService = rscItBPGrossConsumptionService;
        this.rscItSubBaseGrossConsumptionService = rscItSubBaseGrossConsumptionService;
    }

    @Override
    public void saveAll(List<RscItGrossConsumptionDTO> bulkRscItGrossConsumptionDTOs, Long ppHeaderId) {
        if (rscItBulkGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            bulkRscItGrossConsumptionDTOs.sort(Comparator.comparing(RscItGrossConsumptionDTO::getItemCode));
            rscItBulkGrossConsumptionRepository.saveAll(rscItBulkGrossConsumptionMapper.toEntity(bulkRscItGrossConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItBulkGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscItBulkGrossConsumptionMapper.toDto(rscItBulkGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllGrossConsumptionDTOByRscMtPPheaderId(Long ppHeaderId) {
        List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs = new ArrayList<>();
        rscItGrossConsumptionDTOs.addAll(rscItBulkGrossConsumptionMapper.toDto(rscItBulkGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        rscItGrossConsumptionDTOs.addAll(rscItBPGrossConsumptionService.findAllByRscMtPPHeaderId(ppHeaderId));
        rscItGrossConsumptionDTOs.addAll(rscItSubBaseGrossConsumptionService.findAllByRscMtPPHeaderId(ppHeaderId));
        return rscItGrossConsumptionDTOs;
    }

    @Override
    public GrossConsumptionMainDTO getAllBulkGrossConsumption(Long ppHeaderId) {
        List<GrossConsumptionDTO> grossConsumptionDTOs = new ArrayList<>();
        Optional<RscMtPPheaderDTO> optionalRscMtPPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        grossConsumptionDTOs.addAll(bulkGrossConsumptionMapper.toDto(rscItBulkGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        grossConsumptionDTOs.addAll(rscItBPGrossConsumptionService.getAllBPGrossConsumption(ppHeaderId));
        grossConsumptionDTOs.addAll(rscItSubBaseGrossConsumptionService.getAllSubBaseGrossConsumption(ppHeaderId));
        return consumptionUtil.getGrossConsumptionMainDTO(optionalRscMtPPHeaderDTO.get(), grossConsumptionDTOs);
    }

    @Override
    public ItemCodeDTO getBulkItemCodeDTO(Long ppHeaderId) {
        ItemCodeDTO itemCodeDTO = new ItemCodeDTO();
        List<ItemCodeDetailDTO> itemCodeDetails = new ArrayList<>();
        List<ItemCodeDetailDTO> bulkItemCodeDetailDTOs = bulkItemCodedetailsMapper.toDto(rscItBulkGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne(ppHeaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            RscMtPPheaderDTO rscMtPPheaderDTO = optionalRscMtPpHeaderDTO.get();
            itemCodeDTO.setMpsId(rscMtPPheaderDTO.getId());
            itemCodeDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            itemCodeDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            itemCodeDetails.addAll(bulkItemCodeDetailDTOs);
            itemCodeDetails.addAll(rscItBPGrossConsumptionService.getBPItemCodeDetailDTO(ppHeaderId));
            itemCodeDTO.setItemCodes(itemCodeDetails);
        }
        return itemCodeDTO;
    }

    @Override
    public RscItGrossConsumptionDTO findByRscMtPPheaderIdAndBulkItemId(Long ppHeaderId, Long itemId) {
        RscItGrossConsumptionDTO bpRscItGrossConsumptionDTO = rscItBPGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, itemId);
        RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO = rscItBulkGrossConsumptionMapper.toDto(rscItBulkGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, itemId));
        if (Optional.ofNullable(bpRscItGrossConsumptionDTO).isPresent())
            return bpRscItGrossConsumptionDTO;
        return bulkRscItGrossConsumptionDTO;
    }

    @Override
    public ExcelExportFiles exportBulkGross(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmBulkGrossConsumptionExporter.rmBulkGrossToExcel(getAllBulkGrossConsumption(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_BULK_GROSS_CONSUMPTION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItBulkGrossConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}