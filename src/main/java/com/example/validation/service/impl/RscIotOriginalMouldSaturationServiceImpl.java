package com.example.validation.service.impl;

import com.example.validation.domain.RscIotOriginalMouldSaturation;
import com.example.validation.dto.LatestPurchasePlanDTO;
import com.example.validation.dto.OriginalPurchasePlanDTO;
import com.example.validation.mapper.OriginalMouldSaturationMapper;
import com.example.validation.mapper.RscIotOriginalMouldSaturationMapper;
import com.example.validation.repository.RscIotOriginalMouldSaturationRepository;
import com.example.validation.service.RscIotOriginalMouldSaturationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

@Service
@Transactional
public class RscIotOriginalMouldSaturationServiceImpl implements RscIotOriginalMouldSaturationService {

    private final RscIotOriginalMouldSaturationRepository rscIotOriginalMouldSaturationRepository;
    private final RscIotOriginalMouldSaturationMapper rscIotOriginalMouldSaturationMapper;
    private final OriginalMouldSaturationMapper originalMouldSaturationMapper;

    public RscIotOriginalMouldSaturationServiceImpl(RscIotOriginalMouldSaturationRepository rscIotOriginalMouldSaturationRepository, RscIotOriginalMouldSaturationMapper rscIotOriginalMouldSaturationMapper, OriginalMouldSaturationMapper originalMouldSaturationMapper) {
        this.rscIotOriginalMouldSaturationRepository = rscIotOriginalMouldSaturationRepository;
        this.rscIotOriginalMouldSaturationMapper = rscIotOriginalMouldSaturationMapper;
        this.originalMouldSaturationMapper = originalMouldSaturationMapper;
    }

    private static <T> Predicate<T> distinctBySupplierId(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public String saveMouldSaturation(Long ppHeaderId) {
       /* List<OriginalPurchasePlanDTO> originalPurchasePlanDTOList = new ArrayList<>();
        List<OriginalPurchasePlanDTO> originalPurchasePlanDataList = originalPurchasePlanMapper.toDto(this.rscIotPMOriginalPurchasePlanListByPpHeaderId(ppHeaderId));
        RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay = rscIitMpsWorkingDaysService.findByMpsId(ppHeaderId);
        for (OriginalPurchasePlanDTO originalPurchasePlanDTO : originalPurchasePlanDataList) {
            if (rscIitMpsWorkingDay!=null && originalPurchasePlanDTO!=null && originalPurchasePlanDTO.getMouldId() != null) {
                originalPurchasePlanDTO.setM1Value(MouldSaturationUtil.getMonth1MouldSaturation(originalPurchasePlanDTO.getM1Value(), originalPurchasePlanDTO.getM2Value(), rscIitMpsWorkingDay.getMonth1Value(), originalPurchasePlanDTO.getRtdValue(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM2Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM3Value(), rscIitMpsWorkingDay.getMonth2Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM3Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM4Value(), rscIitMpsWorkingDay.getMonth3Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM4Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM5Value(), rscIitMpsWorkingDay.getMonth4Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM5Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM6Value(), rscIitMpsWorkingDay.getMonth5Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM6Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM7Value(), rscIitMpsWorkingDay.getMonth6Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM7Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM8Value(), rscIitMpsWorkingDay.getMonth7Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM8Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM9Value(), rscIitMpsWorkingDay.getMonth8Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM9Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM10Value(), rscIitMpsWorkingDay.getMonth9Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM10Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM11Value(), rscIitMpsWorkingDay.getMonth10Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM11Value(MouldSaturationUtil.getMonthWiseMouldSaturation(originalPurchasePlanDTO.getM12Value(), rscIitMpsWorkingDay.getMonth11Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTO.setM12Value(MouldSaturationUtil.getMonthWiseMouldSaturation(0.0, rscIitMpsWorkingDay.getMonth12Value(), originalPurchasePlanDTO.getCapacity()));
                originalPurchasePlanDTOList.add(originalPurchasePlanDTO);
            }
        }
        saveAll(originalPurchasePlanDTOList);*/
        return "Original Mould Saturation Saved Successfully";
    }

    /*@Override
    public List<RscIotPMOriginalPurchasePlan> rscIotPMOriginalPurchasePlanListByPpHeaderId(Long ppHeaderId) {
        return rscIotPMOriginalPurchasePlanRepository.findAllByRscMtPPheaderId(ppHeaderId);
    }*/

    @Override
    public void saveAllOriginalMouldSaturation(List<LatestPurchasePlanDTO> latestPurchasePlanDTOList) {
        rscIotOriginalMouldSaturationRepository.saveAll(originalMouldSaturationMapper.toEntity(latestPurchasePlanDTOList));
    }

    @Override
    public void save(OriginalPurchasePlanDTO originalPurchasePlanDTO) {
        rscIotOriginalMouldSaturationRepository.save(rscIotOriginalMouldSaturationMapper.toEntity(originalPurchasePlanDTO));
    }

    @Override
    public void saveAll(List<OriginalPurchasePlanDTO> originalPurchasePlanDTOList) {
        rscIotOriginalMouldSaturationRepository.saveAll(rscIotOriginalMouldSaturationMapper.toEntity(originalPurchasePlanDTOList));
    }

    @Override
    public List<OriginalPurchasePlanDTO> findAll() {
        List<RscIotOriginalMouldSaturation> rscIotOriginalMouldSaturationList = rscIotOriginalMouldSaturationRepository.findAll();
        return rscIotOriginalMouldSaturationMapper.toDto(rscIotOriginalMouldSaturationList);
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotOriginalMouldSaturationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
