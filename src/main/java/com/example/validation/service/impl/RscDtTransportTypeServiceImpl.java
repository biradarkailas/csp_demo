package com.example.validation.service.impl;

import com.example.validation.dto.RscDtTransportTypeDTO;
import com.example.validation.mapper.RscDtTransportTypeMapper;
import com.example.validation.repository.RscDtTransportTypeRepository;
import com.example.validation.service.RscDtTransportTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtTransportTypeServiceImpl implements RscDtTransportTypeService {
    private final RscDtTransportTypeRepository rscDtTransportTypeRepository;
    private final RscDtTransportTypeMapper rscDtTransportTypeMapper;

    public RscDtTransportTypeServiceImpl(RscDtTransportTypeRepository rscDtTransportTypeRepository, RscDtTransportTypeMapper rscDtTransportTypeMapper){
        this.rscDtTransportTypeRepository = rscDtTransportTypeRepository;
        this.rscDtTransportTypeMapper = rscDtTransportTypeMapper;
    }

    @Override
    public List<RscDtTransportTypeDTO> findAll() {
        return rscDtTransportTypeMapper.toDto(rscDtTransportTypeRepository.findAll());
    }
}
