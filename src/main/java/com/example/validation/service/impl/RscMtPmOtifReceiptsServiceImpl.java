package com.example.validation.service.impl;

import com.example.validation.dto.RscMtPmOtifReceiptsDTO;
import com.example.validation.mapper.RscMtPmOtifReceiptsMapper;
import com.example.validation.repository.RscMtPmOtifReceiptsRepository;
import com.example.validation.service.RscMtPmOtifReceiptsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscMtPmOtifReceiptsServiceImpl implements RscMtPmOtifReceiptsService {
    private final RscMtPmOtifReceiptsRepository rscMtPmOtifReceiptsRepository;
    private final RscMtPmOtifReceiptsMapper rscMtPmOtifReceiptsMapper;

    public RscMtPmOtifReceiptsServiceImpl(RscMtPmOtifReceiptsRepository rscMtPmOtifReceiptsRepository, RscMtPmOtifReceiptsMapper rscMtPmOtifReceiptsMapper) {
        this.rscMtPmOtifReceiptsRepository = rscMtPmOtifReceiptsRepository;
        this.rscMtPmOtifReceiptsMapper = rscMtPmOtifReceiptsMapper;
    }

    @Override
    public List<RscMtPmOtifReceiptsDTO> findAll() {
        return rscMtPmOtifReceiptsMapper.toDto(rscMtPmOtifReceiptsRepository.findAll());
    }

    @Override
    public void deleteAll(List<Long> ids) {
        rscMtPmOtifReceiptsRepository.deleteAllByRscMtOtifPmReceiptsHeaderIdIn(ids);
    }
}