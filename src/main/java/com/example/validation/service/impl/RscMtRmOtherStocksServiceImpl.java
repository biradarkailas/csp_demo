package com.example.validation.service.impl;

import com.example.validation.dto.IndividualStockTypeValueDTO;
import com.example.validation.dto.RscMtRmOtherStocksDTO;
import com.example.validation.mapper.IndividualOtherStockMapper;
import com.example.validation.mapper.RscMtRmOtherStocksMapper;
import com.example.validation.repository.RscMtRmOtherStocksRepository;
import com.example.validation.service.RscMtRmOtherStocksService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtRmOtherStocksServiceImpl implements RscMtRmOtherStocksService {
    private final RscMtRmOtherStocksRepository rscMtRmOtherStocksRepository;
    private final RscMtRmOtherStocksMapper rscMtRmOtherStocksMapper;
    private final IndividualOtherStockMapper individualOtherStockMapper;

    public RscMtRmOtherStocksServiceImpl(RscMtRmOtherStocksRepository rscMtRmOtherStocksRepository, RscMtRmOtherStocksMapper rscMtRmOtherStocksMapper, IndividualOtherStockMapper individualOtherStockMapper) {
        this.rscMtRmOtherStocksRepository = rscMtRmOtherStocksRepository;
        this.rscMtRmOtherStocksMapper = rscMtRmOtherStocksMapper;
        this.individualOtherStockMapper = individualOtherStockMapper;
    }

    @Override
    public List<RscMtRmOtherStocksDTO> findAllByRscMtItemId(Long rscMtItemId) {
        return rscMtRmOtherStocksMapper.toDto(rscMtRmOtherStocksRepository.findAllByRscMtItemId(rscMtItemId));
    }

    @Override
    public List<IndividualStockTypeValueDTO> findAllByRscDtStockTypeIdAndRscMtItemId(Long stockTypeId, Long rscMtItemId) {
        return individualOtherStockMapper.toDto(rscMtRmOtherStocksRepository.findAllByRscDtStockTypeIdAndRscMtItemId(stockTypeId, rscMtItemId));
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtRmOtherStocksRepository.deleteAllByStockDate(stockDate);
    }
}