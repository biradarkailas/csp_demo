package com.example.validation.service.impl;

import com.example.validation.dto.*;
import com.example.validation.mapper.FgSkidSaturationSkidsMapper;
import com.example.validation.mapper.RscIotFgSkidSaturationSummaryMapper;
import com.example.validation.repository.RscIotFgSkidSaturationSummaryRepository;
import com.example.validation.service.RscIotFgSkidSaturationService;
import com.example.validation.service.RscIotFgSkidSaturationSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.enums.SaturationRange;
import com.example.validation.util.enums.SaturationStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotFgSkidSaturationSummaryServiceImpl implements RscIotFgSkidSaturationSummaryService {
    private final RscIotFgSkidSaturationSummaryRepository rscIotFgSkidSaturationSummaryRepository;
    private final RscIotFgSkidSaturationSummaryMapper rscIotFgSkidSaturationSummaryMapper;
    private final RscIotFgSkidSaturationService rscIotFgSkidSaturationService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final FgSkidSaturationSkidsMapper fgSkidSaturationSkidsMapper;

    public RscIotFgSkidSaturationSummaryServiceImpl(RscIotFgSkidSaturationSummaryRepository rscIotFgSkidSaturationSummaryRepository, RscIotFgSkidSaturationSummaryMapper rscIotFgSkidSaturationSummaryMapper, RscIotFgSkidSaturationService rscIotFgSkidSaturationService, RscMtPpheaderService rscMtPpheaderService, FgSkidSaturationSkidsMapper fgSkidSaturationSkidsMapper) {
        this.rscIotFgSkidSaturationSummaryRepository = rscIotFgSkidSaturationSummaryRepository;
        this.rscIotFgSkidSaturationSummaryMapper = rscIotFgSkidSaturationSummaryMapper;
        this.rscIotFgSkidSaturationService = rscIotFgSkidSaturationService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.fgSkidSaturationSkidsMapper = fgSkidSaturationSkidsMapper;
    }

    @Override
    public void createFgSkidSaturationSummary(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            if (rscIotFgSkidSaturationSummaryRepository.countByRscMtPPheaderId(pPheaderDTO.get().getId()) == 0) {
                List<RscIotFgSkidSaturationDTO> rscIotFgSkidSaturationDTOList = rscIotFgSkidSaturationService.findAll(ppHeaderId);
                if (Optional.ofNullable(rscIotFgSkidSaturationDTOList).isPresent()) {
                    List<RscIotFgSkidSaturationSummaryDTO> skidSaturationSummaryDTOList = new ArrayList<>();
                    skidSaturationSummaryDTOList.addAll(getSkidSaturationSummary(rscIotFgSkidSaturationDTOList));
                    saveAll(skidSaturationSummaryDTOList);
                }
            }
        }
    }

    private void saveAll(List<RscIotFgSkidSaturationSummaryDTO> skidSaturationSummaryDTOList) {
        rscIotFgSkidSaturationSummaryRepository.saveAll(rscIotFgSkidSaturationSummaryMapper.toEntity(skidSaturationSummaryDTOList));
    }

    private List<RscIotFgSkidSaturationSummaryDTO> getSkidSaturationSummary(List<RscIotFgSkidSaturationDTO> rscIotFgSkidSaturationDTOList) {
        List<RscIotFgSkidSaturationSummaryDTO> skidSaturationSummaryDTOList = new ArrayList<>();
        rscIotFgSkidSaturationDTOList.forEach(rscIotFgSkidSaturationDTO -> {
            if (checkGreaterThanHighestRangeOrNot(rscIotFgSkidSaturationDTO)) {
                skidSaturationSummaryDTOList.add(getSkidSaturationSummaryDTO(rscIotFgSkidSaturationDTO, SaturationStatus.High));
            } else {
                skidSaturationSummaryDTOList.add(getSkidSaturationSummaryDTO(rscIotFgSkidSaturationDTO, SaturationStatus.Low));
            }
        });
        return skidSaturationSummaryDTOList;
    }

    private RscIotFgSkidSaturationSummaryDTO getSkidSaturationSummaryDTO(RscIotFgSkidSaturationDTO rscIotFgSkidSaturationDTO, SaturationStatus status) {
        RscIotFgSkidSaturationSummaryDTO rscIotFgSkidSaturationSummaryDTO = new RscIotFgSkidSaturationSummaryDTO();
        rscIotFgSkidSaturationSummaryDTO.setRscDtSkidsId(rscIotFgSkidSaturationDTO.getRscDtSkidsId());
        rscIotFgSkidSaturationSummaryDTO.setRscMtPPheaderId(rscIotFgSkidSaturationDTO.getRscMtPPheaderId());
        rscIotFgSkidSaturationSummaryDTO.setSkidSummaryDate(LocalDate.now());
        if (status.equals(SaturationStatus.High)) {
            rscIotFgSkidSaturationSummaryDTO.setSaturationStatus(SaturationStatus.High);
            rscIotFgSkidSaturationSummaryDTO.setAvgSaturationPercentage(rscIotFgSkidSaturationDTO.getM1SkidSaturation());
        } else {
            rscIotFgSkidSaturationSummaryDTO.setAvgSaturationPercentage(rscIotFgSkidSaturationDTO.getM1SkidSaturation());
            if (rscIotFgSkidSaturationSummaryDTO.getAvgSaturationPercentage() > SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal()) {
                rscIotFgSkidSaturationSummaryDTO.setSaturationStatus(SaturationStatus.Medium);
            } else {
                rscIotFgSkidSaturationSummaryDTO.setSaturationStatus(SaturationStatus.Low);
            }
        }
        return rscIotFgSkidSaturationSummaryDTO;
    }


    private boolean checkGreaterThanHighestRangeOrNot(RscIotFgSkidSaturationDTO rscIotFgSkidSaturationDTO) {
        if (rscIotFgSkidSaturationDTO.getM1SkidSaturation() >= SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<RscIotFgSkidSaturationSummaryDTO> getFgSkidSaturationsList(Long ppHeaderId) {
        List<RscIotFgSkidSaturationSummaryDTO> skidSaturationSummaryDTOList = new ArrayList<>();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            skidSaturationSummaryDTOList.addAll(rscIotFgSkidSaturationSummaryMapper.toDto(rscIotFgSkidSaturationSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        }
        return skidSaturationSummaryDTOList;
    }

    @Override
    public FgSkidSaturationSkidWiseDetailsDTO getSingleSkidsDetails(Long ppHeaderId, Long rscDtSkidsId) {
        FgSkidSaturationSkidWiseDetailsDTO fgSkidSaturationSkidWiseDetailsDTO = new FgSkidSaturationSkidWiseDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            RscIotFgSkidSaturationSummaryDTO rscIotFgSkidSaturationSummaryDTO = rscIotFgSkidSaturationSummaryMapper.toDto(rscIotFgSkidSaturationSummaryRepository.findByRscMtPPheaderIdAndRscDtSkidsId(rscMtPPheaderDTO.getId(), rscDtSkidsId));
            if (Optional.ofNullable(rscIotFgSkidSaturationSummaryDTO).isPresent()) {
                fgSkidSaturationSkidWiseDetailsDTO.setAvgReceiptPercentage(rscIotFgSkidSaturationSummaryDTO.getAvgSaturationPercentage());
                fgSkidSaturationSkidWiseDetailsDTO.setRscDtSkidsId(rscIotFgSkidSaturationSummaryDTO.getRscDtSkidsId());
                fgSkidSaturationSkidWiseDetailsDTO.setSkidName(rscIotFgSkidSaturationSummaryDTO.getSkidName());
                fgSkidSaturationSkidWiseDetailsDTO.setSaturationStatus(rscIotFgSkidSaturationSummaryDTO.getSaturationStatus());
            }
            fgSkidSaturationSkidWiseDetailsDTO.setRscIotFgSkidSaturationDTOList(rscIotFgSkidSaturationService.findAllBySkidsIdAndRscMtPPheaderId(rscDtSkidsId, ppHeaderId));
        }
        return fgSkidSaturationSkidWiseDetailsDTO;
    }

    @Override
    public FgSkidSaturationSkidsDetailsDTO getFgSkidSaturationsSkids(Long ppHeaderId) {
        FgSkidSaturationSkidsDetailsDTO skidsDetailsDTO = new FgSkidSaturationSkidsDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            skidsDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
            skidsDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            skidsDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            skidsDetailsDTO.setHighSaturationRangeSkidsDTOList(getSaturationSkidsList(rscMtPPheaderDTO.getId(), SaturationStatus.High));
            skidsDetailsDTO.setMediumSaturationRangeSkidsDTOList(getSaturationSkidsList(rscMtPPheaderDTO.getId(), SaturationStatus.Medium));
            skidsDetailsDTO.setLowSaturationRangeSkidsDTOList(getSaturationSkidsList(rscMtPPheaderDTO.getId(), SaturationStatus.Low));
            skidsDetailsDTO.setHighSaturationRangeListCount(skidsDetailsDTO.getHighSaturationRangeSkidsDTOList().size());
            skidsDetailsDTO.setMediumSaturationRangeListCount(skidsDetailsDTO.getMediumSaturationRangeSkidsDTOList().size());
            skidsDetailsDTO.setLowSaturationRangeListCount(skidsDetailsDTO.getLowSaturationRangeSkidsDTOList().size());
            skidsDetailsDTO.setHighSaturationRange(">= " + SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal() + "%");
            skidsDetailsDTO.setMediumSaturationRange(" " + SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal() + "% - " + SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal() + "%");
            skidsDetailsDTO.setLowSaturationRange("< " + SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal() + "%");
            skidsDetailsDTO.setTotalCount(getTotalCount(skidsDetailsDTO.getHighSaturationRangeListCount(), skidsDetailsDTO.getMediumSaturationRangeListCount(), skidsDetailsDTO.getLowSaturationRangeListCount()));

        }
        return skidsDetailsDTO;
    }

    private List<FgSkidSaturationSkidsDTO> getSaturationSkidsList(Long ppHeaderId, Enum status) {
        return fgSkidSaturationSkidsMapper.toDto(rscIotFgSkidSaturationSummaryRepository.findAllByRscMtPPheaderIdAndSaturationStatus(ppHeaderId, status));
    }

    private Integer getTotalCount(Integer highReceiptCount, Integer mediumReceiptCount, Integer lowReceiptCount) {
        return highReceiptCount + mediumReceiptCount + lowReceiptCount;
    }

    @Override
    public List<RscIotFgSkidSaturationSummaryDTO> getSkidsList(Long rscDtSkidsId) {
        return rscIotFgSkidSaturationSummaryMapper.toDto(rscIotFgSkidSaturationSummaryRepository.findAllByRscDtSkidsId(rscDtSkidsId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFgSkidSaturationSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}