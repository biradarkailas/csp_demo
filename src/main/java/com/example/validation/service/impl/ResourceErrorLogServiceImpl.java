package com.example.validation.service.impl;

import com.example.validation.dto.ResourceErrorLogDTO;
import com.example.validation.mapper.ResourceErrorLogMapper;
import com.example.validation.repository.ResourceErrorLogRepository;
import com.example.validation.service.ResourceErrorLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ResourceErrorLogServiceImpl implements ResourceErrorLogService {
    private final ResourceErrorLogRepository resourceErrorLogRepository;
    private final ResourceErrorLogMapper resourceErrorLogMapper;

    public ResourceErrorLogServiceImpl(ResourceErrorLogRepository resourceErrorLogRepository, ResourceErrorLogMapper resourceErrorLogMapper) {
        this.resourceErrorLogRepository = resourceErrorLogRepository;
        this.resourceErrorLogMapper = resourceErrorLogMapper;
    }

    @Override
    public List<ResourceErrorLogDTO> findAll() {
        return resourceErrorLogMapper.toDto(resourceErrorLogRepository.findAll());
    }

    @Override
    public void deleteAll() {
        resourceErrorLogRepository.deleteAll();
    }
}