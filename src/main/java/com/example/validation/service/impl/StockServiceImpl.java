package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.domain.RscMtPmMes;
import com.example.validation.domain.RscMtPmRtd;
import com.example.validation.dto.*;
import com.example.validation.mapper.RscMtPmMesMapper;
import com.example.validation.mapper.RscMtPmRtdMapper;
import com.example.validation.repository.RscMtPmMesRejectedStockRepository;
import com.example.validation.repository.RscMtPmMesRepository;
import com.example.validation.repository.RscMtPmRtdRepository;
import com.example.validation.service.*;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.StockUtil;
import com.example.validation.util.excel_export.PmMesRejectionStockExporter;
import com.example.validation.util.excel_export.PmMesStockExporter;
import com.example.validation.util.excel_export.PmRtdStockExporter;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class StockServiceImpl implements StockService {
    private final RscMtPmRtdRepository rscMtPmRtdRepository;
    private final RscMtPmRtdMapper rscMtPmRtdMapper;
    private final RscMtPmMesRepository rscMtPmMesRepository;
    private final RscMtPmMesMapper rscMtPmMesMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscItPMStockService rscItPMStockService;
    private final RscMtPmMesService rscMtPmMesService;
    private final RscMtPmRtdService rscMtPmRtdService;
    private final RscMtPmMesRejectedStockRepository rscMtPmMesRejectedStockRepository;

    public StockServiceImpl(RscMtPmRtdRepository rscMtPmRtdRepository, RscMtPmRtdMapper rscMtPmRtdMapper, RscMtPmMesRepository rscMtPmMesRepository, RscMtPmMesMapper rscMtPmMesMapper, RscMtPpheaderService rscMtPpheaderService, RscItPMStockService rscItPMStockService, RscMtPmMesService rscMtPmMesService, RscMtPmRtdService rscMtPmRtdService, RscMtPmMesRejectedStockRepository rscMtPmMesRejectedStockRepository) {
        this.rscMtPmRtdRepository = rscMtPmRtdRepository;
        this.rscMtPmRtdMapper = rscMtPmRtdMapper;
        this.rscMtPmMesRepository = rscMtPmMesRepository;
        this.rscMtPmMesMapper = rscMtPmMesMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscItPMStockService = rscItPMStockService;
        this.rscMtPmMesService = rscMtPmMesService;
        this.rscMtPmRtdService = rscMtPmRtdService;
        this.rscMtPmMesRejectedStockRepository = rscMtPmMesRejectedStockRepository;
    }

    @Override
    public PMStockDTO getPmStockByMpsID(Long mpsId, String stockType) {
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne( mpsId ).orElse( null );
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent()) {
            return getPMStockDTO( stockType, rscMtPPheaderDTO );
        }
        return null;
    }

    private PMStockDTO getPMStockDTO(String stockType, RscMtPPheaderDTO rscMtPPheaderDTO) {
        PMStockDTO pmStockDTO = new PMStockDTO();
        pmStockDTO.setMpsId( rscMtPPheaderDTO.getId() );
        pmStockDTO.setMpsDate( rscMtPPheaderDTO.getMpsDate() );
        pmStockDTO.setMpsName( rscMtPPheaderDTO.getMpsName() );
        List<PmStockItemCodeDTO> pmStockItemCodeDTOS = new ArrayList<>();
        for (PmStockItemCodeDTO pmStockItemCodeDTO : rscItPMStockService.findAllDistinctByRscMtItem()) {
            var stockItem = getStockItemList( pmStockItemCodeDTO, stockType, rscMtPPheaderDTO.getId() );
            if (stockItem != null) pmStockItemCodeDTOS.add( stockItem );
        }
        pmStockDTO.setItemCodes( pmStockItemCodeDTOS );
        return pmStockDTO;
    }

    private PmStockItemCodeDTO getStockItemList(PmStockItemCodeDTO pmStockItemCodeDTO, String stockType, Long ppHeaderId) {
        PmStockItemCodeDTO itemCodeDTO = new PmStockItemCodeDTO();
        itemCodeDTO.setId( pmStockItemCodeDTO.getId() );
        itemCodeDTO.setCode( pmStockItemCodeDTO.getCode() );
        itemCodeDTO.setDescription( pmStockItemCodeDTO.getDescription() );
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );

        if (stockType != null && rscMtPPheaderDTO.isPresent()) {
            if (stockType.equals( MaterialCategoryConstants.MES_STOCK_TYPE )) {
                itemCodeDTO.setMes( getPmMesDetails( pmStockItemCodeDTO.getId(), rscMtPPheaderDTO.get().getMpsDate() ) );
                if (itemCodeDTO.getMes() == null) return null;
            } else if (stockType.equals( MaterialCategoryConstants.RTD_STOCK_TYPE )) {
                itemCodeDTO.setRtd( getPmRtdDetails( pmStockItemCodeDTO.getId(), rscMtPPheaderDTO.get().getMpsDate() ) );
                if (itemCodeDTO.getRtd() == null) return null;
            }
        }
        return itemCodeDTO;
    }

    private PmStockDetailDTO.PmStockDetailItemCode.OpeningStock createOpeningStock(PmStockDetailDTO.PmStockDetailItemCode pmStockDetailItemCode, List<RscMtPmRtdDTO> rscMtPmRtdDTOs, List<RscMtPmMesDTO> rscMtPmMesDTOs, Long rscMtItemId, LocalDate mpsDate) {
        PmStockDetailDTO.PmStockDetailItemCode.OpeningStock openingStock = pmStockDetailItemCode.new OpeningStock();
        openingStock.setStockType( createStockType( openingStock, rscMtPmRtdDTOs, rscMtPmMesDTOs, rscMtItemId, mpsDate ) );
        openingStock.setValue( openingStock.getStockType().getMes().getTotalValue() + openingStock.getStockType().getRtd().getTotalValue() );
        return openingStock;
    }

    private PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType createStockType(PmStockDetailDTO.PmStockDetailItemCode.OpeningStock openingStock, List<RscMtPmRtdDTO> rscMtPmRtdDTOs, List<RscMtPmMesDTO> rscMtPmMesDTOs, Long rscMtItemId, LocalDate mpsDate) {
        PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType stockType = openingStock.new StockType();
        stockType.setMes( createMes( stockType, rscMtPmMesDTOs, rscMtItemId, mpsDate ) );
        stockType.setRtd( createRtd( stockType, rscMtPmRtdDTOs, rscMtItemId, mpsDate ) );
        return stockType;
    }


    private PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Mes createMes(PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType stockType, List<RscMtPmMesDTO> rscMtPmMesDTOs, Long rscMtItemId, LocalDate mpsDate) {
        PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Mes mes = stockType.new Mes();
        Map<String, Object> map = getMesIndividualValueList( rscMtPmMesDTOs, rscMtItemId, mes, mpsDate );
        mes.setName( MaterialCategoryConstants.MONTH_END_STOCK );
        mes.setAliasName( MaterialCategoryConstants.MES_STOCK_TYPE );
        mes.setIndividualValues( (List<PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Mes.MesIndividualValue>) map.get( "list" ) );
        mes.setTotalValue( (Double) map.get( "mesTotalValue" ) );
        return mes;
    }

    private PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Rtd createRtd(PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType stockType, List<RscMtPmRtdDTO> rscMtPmRtdDTOs, Long rscMtItemId, LocalDate mpsDate) {
        PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Rtd rtd = stockType.new Rtd();
        Map<String, Object> map = getRtdIndividualValueList( rscMtPmRtdDTOs, rscMtItemId, rtd, mpsDate );
        rtd.setAliasName( MaterialCategoryConstants.RTD_STOCK_TYPE );
        rtd.setName( MaterialCategoryConstants.RECEIPT_TILL_DATE );
        rtd.setIndividualValues( (List<PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Rtd.RtdIndividualValue>) map.get( "list" ) );
        rtd.setTotalValue( (Double) map.get( "rtdTotalValue" ) );
        return rtd;
    }

    private Map<String, Object> getMesIndividualValueList(List<RscMtPmMesDTO> rscMtPmMesDTOS, Long rscMtItemId, PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Mes mes, LocalDate mpsDate) {
        HashMap<String, Object> map = new HashMap<>();
        map.put( "mesTotalValue", 0.0 );
        List<PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Mes.MesIndividualValue> list;
        list = rscMtPmMesDTOS.stream().filter( a -> a.getRscMtItemId().equals( rscMtItemId ) && DateUtils.isEqualMonthAndYear( a.getStockDate(), mpsDate ) ).map( b -> {
            PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Mes.MesIndividualValue individualValue = mes.new MesIndividualValue();
            individualValue.setLotNumber( b.getLotNumber() );
            individualValue.setStockDate( b.getStockDate() );
            individualValue.setValue( b.getQuantity() );
            individualValue.setStockStatus( b.getStockStatus() );
            Double totalValue = (Double) map.get( "mesTotalValue" );
            totalValue += b.getQuantity();
            map.put( "mesTotalValue", totalValue );
            return individualValue;
        } ).collect( Collectors.toList() );
        map.put( "list", list );
        return map;
    }

    private Map<String, Object> getRtdIndividualValueList(List<RscMtPmRtdDTO> rscMtPmRtdDTOS, Long rscMtItemId, PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Rtd rtd, LocalDate mpsDate) {
        HashMap<String, Object> map = new HashMap<>();
        map.put( "rtdTotalValue", 0.0 );
        List<PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Rtd.RtdIndividualValue> list;
        list = rscMtPmRtdDTOS.stream().filter( a -> a.getRscMtItemId().equals( rscMtItemId ) && DateUtils.isEqualMonthAndYear( a.getReceiptDate(), mpsDate ) ).map( b -> {
            PmStockDetailDTO.PmStockDetailItemCode.OpeningStock.StockType.Rtd.RtdIndividualValue individualValue = rtd.new RtdIndividualValue();
            individualValue.setLotNumber( b.getLotNumber() );
            individualValue.setSupplierLotNumber( b.getSupplierLotNumber() );
            individualValue.setReceiptDate( b.getReceiptDate() );
            individualValue.setValue( b.getQuantity() );
            individualValue.setSupplierName( b.getSupplierName() );
            Double totalValue = (Double) map.get( "rtdTotalValue" );
            totalValue += b.getQuantity();
            map.put( "rtdTotalValue", totalValue );
            return individualValue;
        } ).collect( Collectors.toList() );
        map.put( "list", list );
        return map;
    }

    public PmStockDetailDTO getPmStockDetailByMpsID(String mpsId, Long rscMtItemID) {
        PmStockDetailDTO pmStockDetailDTO = new PmStockDetailDTO();
        List<PmStockDetailDTO.PmStockDetailItemCode> itemCodes = new ArrayList<>();
        pmStockDetailDTO.setItemCodes( itemCodes );
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne( Long.parseLong( mpsId ) ).orElse( null );
        pmStockDetailDTO.setMpsId( mpsId );

        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent()) {
            pmStockDetailDTO.setMpsName( rscMtPPheaderDTO.getMpsName() );
            pmStockDetailDTO.setMpsDate( rscMtPPheaderDTO.getMpsDate() );
        }

        List<RscMtPmRtdDTO> rscMtPmRtdDTOs = rscMtPmRtdMapper.toDto( pmRtdByRscMtItemId( rscMtItemID ) );
        List<RscMtPmMesDTO> rscMtPmMesDTOs = rscMtPmMesMapper.toDto( pmMesByRscMtItemId( rscMtItemID ) );

        itemCodes.add( createPmStockDetailsItemCode( pmStockDetailDTO, rscMtPmRtdDTOs, rscMtPmMesDTOs, rscMtItemID ) );
        return pmStockDetailDTO;
    }

    private PmStockDetailDTO.PmStockDetailItemCode createPmStockDetailsItemCode(PmStockDetailDTO pmStockDetailDTO, List<RscMtPmRtdDTO> rscMtPmRtdDTOs, List<RscMtPmMesDTO> rscMtPmMesDTOs, Long rscMtItemId) {
        PmStockDetailDTO.PmStockDetailItemCode pmStockItemCode = pmStockDetailDTO.new PmStockDetailItemCode();
        pmStockItemCode.setId( rscMtItemId );
        pmStockItemCode.setOpeningStock( createOpeningStock( pmStockItemCode, rscMtPmRtdDTOs, rscMtPmMesDTOs, rscMtItemId, pmStockDetailDTO.getMpsDate() ) );

        StringBuilder formulaValue = new StringBuilder( DoubleUtils.getFormattedValue( pmStockItemCode.getOpeningStock().getValue() ).toString() + " = " );
        formulaValue.append( DoubleUtils.getFormattedValue( pmStockItemCode.getOpeningStock().getStockType().getMes().getTotalValue() ) ).append( " + " );
        formulaValue.append( DoubleUtils.getFormattedValue( pmStockItemCode.getOpeningStock().getStockType().getRtd().getTotalValue() ) );

        pmStockItemCode.setFormulaValue( formulaValue );

        if (rscMtPmMesDTOs.size() > 0) {
            RscMtPmMesDTO rscMtPmMesDTO = rscMtPmMesDTOs.get( 0 );
            pmStockItemCode.setCode( rscMtPmMesDTO.getItemCode() );
            pmStockItemCode.setDescription( rscMtPmMesDTO.getItemDescription() );
        } else if (rscMtPmRtdDTOs.size() > 0) {
            RscMtPmRtdDTO rscMtPmRtdDTO = rscMtPmRtdDTOs.get( 0 );
            pmStockItemCode.setCode( rscMtPmRtdDTO.getItemCode() );
            pmStockItemCode.setDescription( rscMtPmRtdDTO.getItemDescription() );
        }
        return pmStockItemCode;
    }

    private MesDTO getPmMesDetails(Long rscMtItemId, LocalDate mpsDate) {
        List<IndividualValuesMesDTO> individualValuesMesDTOList = rscMtPmMesService.findAllIndividualValueDTOByRscMtItemId( rscMtItemId );

        List<IndividualValuesMesDTO> individualMesValuesList = StockUtil.getIndividualMesValuesList( individualValuesMesDTOList, mpsDate );
        return StockUtil.getMesStockType( individualMesValuesList, mpsDate );
    }

    private RtdDTO getPmRtdDetails(Long rscMtItemId, LocalDate mpsDate) {
        List<IndividualValuesRtdDTO> individualValuesRtdDTOList = rscMtPmRtdService.findAllIndividualValueDTOByRscMtItemId( rscMtItemId );

        List<IndividualValuesRtdDTO> individualRtdValuesList = StockUtil.getIndividualRtdValuesList( individualValuesRtdDTOList, mpsDate );
        return StockUtil.getRtdStockType( individualRtdValuesList, mpsDate );
    }

    @Override
    public ExcelExportFiles exportMesStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile( PmMesStockExporter.mesStockToExcel( getPmStockWithMesWithNamedQuery( rscMtPPheaderDTO.getId() ), rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.PM_MIDNIGHT_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }



    @Override
    public ExcelExportFiles exportMesStockRejection(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile( PmMesRejectionStockExporter.mesRejectionStockToExcel( rscMtPPheaderDTO.getMpsDate(), getPmStockWithMesRejectionWithNamedQuery( rscMtPPheaderDTO.getId() ) ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.PM_MIDNIGHT_Rejection_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles exportRtdStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile( PmRtdStockExporter.rtdStockToExcel( getPmStockWithRtdWithNamedQuery( rscMtPPheaderDTO.getId() ), rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.PM_RECEIPTS_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<RscMtPmRtd> pmRtdByRscMtItemId(Long rscMtItemId) {
        return rscMtPmRtdRepository.findByRscMtItemId( rscMtItemId );
    }

    @Override
    public List<RscMtPmMes> pmMesByRscMtItemId(Long rscMtItemId) {
        return rscMtPmMesRepository.findByRscMtItemId( rscMtItemId );
    }

    @Override
    public PMStockDTO getPmStockWithMes(Long mpsId) {
        return getPmStockByMpsID( mpsId, MaterialCategoryConstants.MES_STOCK_TYPE );
    }

    @Override
    public PMStockDTO getPmStockWithRtd(Long mpsId) {
        return getPmStockByMpsID( mpsId, MaterialCategoryConstants.RTD_STOCK_TYPE );
    }

  /*  @Override
    public List getPmStockWithMesWithPagination(Long ppHeaderId, Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            String date = rscMtPPheaderDTO.get().getMpsDate().getYear() + "" + rscMtPPheaderDTO.get().getMpsDate().getMonth().getValue();
            List stockData = rscMtPmMesRepository.findPmMesStockByMpsDateAndStockType( date, MaterialCategoryConstants.STOCK_TYPE_TAG_PM_MES, paging );
            if (stockData.size() > 0) {
                return stockData;
            } else {
                return new ArrayList<>();
            }
        } else return new ArrayList();
    }

    @Override
    public List getPmStockWithMesRejectionWithPagination(Long ppHeaderId, Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            String date = rscMtPPheaderDTO.get().getMpsDate().getYear() + "" + rscMtPPheaderDTO.get().getMpsDate().getMonth().getValue();
            List stockData = rscMtPmMesRejectedStockRepository.findPmMesRejectedStockByMpsDateAndStockType( date, MaterialCategoryConstants.STOCK_TYPE_TAG_PM_REJECTION, paging );
            if (stockData.size() > 0) {
                return stockData;
            } else {
                return new ArrayList<>();
            }
        } else return new ArrayList();
    }

    @Override
    public List getPmStockWithRtdWithPagination(Long ppHeaderId, Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            String date = rscMtPPheaderDTO.get().getMpsDate().getYear() + "" + rscMtPPheaderDTO.get().getMpsDate().getMonth().getValue();
            List stockData = rscMtPmRtdRepository.findPmRtdStockByMpsDateAndStockType( date, MaterialCategoryConstants.STOCK_TYPE_TAG_PM_RTD, paging );
            if (stockData.size() > 0) {
                return stockData;
            } else {
                return new ArrayList<>();
            }
        } else return new ArrayList();
    }
*/


    @Override
    public List getPmStockWithMesWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            String date = rscMtPPheaderDTO.get().getMpsDate().getYear() + "" + rscMtPPheaderDTO.get().getMpsDate().getMonth().getValue();
            List stockData = rscMtPmMesRepository.findPmMesStockByMpsDateAndStockType( date, MaterialCategoryConstants.STOCK_TYPE_TAG_PM_MES );
            if (stockData.size() > 0) {
                return stockData;
            } else {
                return new ArrayList<>();
            }
        } else return new ArrayList();
    }

    @Override
    public List getPmStockWithRtdWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            String date = rscMtPPheaderDTO.get().getMpsDate().getYear() + "" + rscMtPPheaderDTO.get().getMpsDate().getMonth().getValue();
            List stockData = rscMtPmRtdRepository.findPmRtdStockByMpsDateAndStockType( date, MaterialCategoryConstants.STOCK_TYPE_TAG_PM_RTD );
            if (stockData.size() > 0) {
                return stockData;
            } else {
                return new ArrayList<>();
            }
        } else return new ArrayList();
    }

    @Override
    public List getPmStockWithMesRejectionWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            String date = rscMtPPheaderDTO.get().getMpsDate().getYear() + "" + rscMtPPheaderDTO.get().getMpsDate().getMonth().getValue();
            List stockData = rscMtPmMesRejectedStockRepository.findPmMesRejectedStockByMpsDateAndStockType( date, MaterialCategoryConstants.STOCK_TYPE_TAG_PM_REJECTION );
            if (stockData.size() > 0) {
                return stockData;
            } else {
                return new ArrayList<>();
            }
        } else return new ArrayList();
    }
}