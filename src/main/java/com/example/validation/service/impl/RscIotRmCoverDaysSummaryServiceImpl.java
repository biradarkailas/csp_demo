package com.example.validation.service.impl;

import com.example.validation.dto.CoverDaysConsumptionDTO;
import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import com.example.validation.dto.RmCoverDaysSummaryDTO;
import com.example.validation.dto.RscIotRmCoverDaysSummaryDTO;
import com.example.validation.mapper.RmCoverDaysSummaryDetailsMapper;
import com.example.validation.mapper.RmCoverDaysSummaryMapper;
import com.example.validation.mapper.RscIotRmCoverDaysConsumptionMapper;
import com.example.validation.mapper.RscIotRmCoverDaysSummaryMapper;
import com.example.validation.repository.RscIotRmCoverDaysSummaryRepository;
import com.example.validation.service.RscIotRmCoverDaysSummaryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RscIotRmCoverDaysSummaryServiceImpl implements RscIotRmCoverDaysSummaryService {
    private final RscIotRmCoverDaysSummaryRepository rscIotRmCoverDaysSummaryRepository;
    private final RscIotRmCoverDaysSummaryMapper rscIotRmCoverDaysSummaryMapper;
    private final RmCoverDaysSummaryMapper rmCoverDaysSummaryMapper;
    private final RmCoverDaysSummaryDetailsMapper rmCoverDaysSummaryDetailsMapper;
    private final RscIotRmCoverDaysConsumptionMapper rscIotRmCoverDaysConsumptionMapper;

    public RscIotRmCoverDaysSummaryServiceImpl(RscIotRmCoverDaysSummaryRepository rscIotRmCoverDaysSummaryRepository, RscIotRmCoverDaysSummaryMapper rscIotRmCoverDaysSummaryMapper, RmCoverDaysSummaryMapper rmCoverDaysSummaryMapper, RmCoverDaysSummaryDetailsMapper rmCoverDaysSummaryDetailsMapper, RscIotRmCoverDaysConsumptionMapper rscIotRmCoverDaysConsumptionMapper) {
        this.rscIotRmCoverDaysSummaryRepository = rscIotRmCoverDaysSummaryRepository;
        this.rscIotRmCoverDaysSummaryMapper = rscIotRmCoverDaysSummaryMapper;
        this.rmCoverDaysSummaryMapper = rmCoverDaysSummaryMapper;
        this.rmCoverDaysSummaryDetailsMapper = rmCoverDaysSummaryDetailsMapper;
        this.rscIotRmCoverDaysConsumptionMapper = rscIotRmCoverDaysConsumptionMapper;
    }

    @Override
    public void saveCoverDaysSummary(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO) {
        rscIotRmCoverDaysSummaryRepository.save(rscIotRmCoverDaysSummaryMapper.toEntity(rscIotRmCoverDaysSummaryDTO));
    }

    @Override
    public RmCoverDaysSummaryDTO getRmCoverDaysSummaryDetailsofMonths(Long ppHeaderId) {
        return rmCoverDaysSummaryMapper.toDto(rscIotRmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RscIotRmCoverDaysSummaryDTO getRmCoverDaysSummary(Long ppHeaderId) {
        return rscIotRmCoverDaysSummaryMapper.toDto(rscIotRmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public CoverDaysSummaryDetailsDTO getRmCoverDaysSummaryAllDetails(Long ppHeaderId) {
        return rmCoverDaysSummaryDetailsMapper.toDto(rscIotRmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public long countByRscMtPPheaderId(Long ppHeaderId) {
        return rscIotRmCoverDaysSummaryRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public void deleteAllById(Long ppHeaderId) {
        rscIotRmCoverDaysSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public CoverDaysConsumptionDTO getTotalCoverDaysConsumption(Long ppHeaderId) {
        return rscIotRmCoverDaysConsumptionMapper.toDto(rscIotRmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }
}
