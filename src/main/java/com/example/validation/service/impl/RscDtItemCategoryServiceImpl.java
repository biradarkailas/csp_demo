package com.example.validation.service.impl;

import com.example.validation.dto.RscDtItemCategoryDTO;
import com.example.validation.mapper.RscDtItemCategoryMapper;
import com.example.validation.repository.RscDtItemCategoryRepository;
import com.example.validation.service.RscDtItemCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtItemCategoryServiceImpl implements RscDtItemCategoryService{
    private final RscDtItemCategoryRepository rscRcsDtItemCategoryRepository;
    private final RscDtItemCategoryMapper rscDtItemCategoryMapper;

    public RscDtItemCategoryServiceImpl(RscDtItemCategoryRepository rscRcsDtItemCategoryRepository, RscDtItemCategoryMapper rscDtItemCategoryMapper){
        this.rscRcsDtItemCategoryRepository = rscRcsDtItemCategoryRepository;
        this.rscDtItemCategoryMapper = rscDtItemCategoryMapper;
    }

    @Override
    public List<RscDtItemCategoryDTO> findAll(){
        return rscDtItemCategoryMapper.toDto(rscRcsDtItemCategoryRepository.findAll());
    }

}
