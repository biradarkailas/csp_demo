package com.example.validation.service.impl;

import com.example.validation.dto.RscIotRmSlobSummaryDTO;
import com.example.validation.dto.SlobSummaryDashboardDTO;
import com.example.validation.mapper.RmSlobSummaryDashboardMapper;
import com.example.validation.mapper.RscIotRmSlobSummaryMapper;
import com.example.validation.repository.RscIotRmSlobSummaryRepository;
import com.example.validation.service.RscIotRmSlobSummaryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RscIotRmSlobSummaryServiceImpl implements RscIotRmSlobSummaryService {
    private final RscIotRmSlobSummaryRepository rscIotRmSlobSummaryRepository;
    private final RscIotRmSlobSummaryMapper rscIotRmSlobSummaryMapper;
    private final RmSlobSummaryDashboardMapper rmSlobSummaryDashboardMapper;

    public RscIotRmSlobSummaryServiceImpl(RscIotRmSlobSummaryRepository rscIotRmSlobSummaryRepository, RscIotRmSlobSummaryMapper rscIotRmSlobSummaryMapper, RmSlobSummaryDashboardMapper rmSlobSummaryDashboardMapper) {
        this.rscIotRmSlobSummaryRepository = rscIotRmSlobSummaryRepository;
        this.rscIotRmSlobSummaryMapper = rscIotRmSlobSummaryMapper;
        this.rmSlobSummaryDashboardMapper = rmSlobSummaryDashboardMapper;
    }

    @Override
    public void saveRmSlobSummary(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        rscIotRmSlobSummaryRepository.save(rscIotRmSlobSummaryMapper.toEntity(rscIotRmSlobSummaryDTO));
    }

    @Override
    public RscIotRmSlobSummaryDTO findByRscMtPPheaderId(Long rscMtPPHeaderId) {
        return rscIotRmSlobSummaryMapper.toDto(rscIotRmSlobSummaryRepository.findByRscMtPPheaderId(rscMtPPHeaderId));
    }

    @Override
    public Long countByRscMtPPheaderId(Long rscMtPPHeaderId) {
        return rscIotRmSlobSummaryRepository.countByRscMtPPheaderId(rscMtPPHeaderId);
    }

    @Override
    public void deleteById(Long ppHeaderId) {
        rscIotRmSlobSummaryRepository.deleteByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public SlobSummaryDashboardDTO getRmSlobSummaryDashboard(Long ppHeaderId) {
        return rmSlobSummaryDashboardMapper.toDto(rscIotRmSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
    }

}
