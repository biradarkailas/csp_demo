package com.example.validation.service.impl;

import com.example.validation.domain.PartyAddressDetails;
import com.example.validation.repository.PartyAddressDetailsRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartyAddressDetailsService {
    @Autowired
    PartyAddressDetailsRepo partyAddressDetailsRepo;

    public PartyAddressDetails save(PartyAddressDetails partyAddressDetails) {
        return partyAddressDetailsRepo.save(partyAddressDetails);
    }
}
