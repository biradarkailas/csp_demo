package com.example.validation.service.impl;

import com.example.validation.dto.*;
import com.example.validation.mapper.PmOtifCumulativeScoreMapper;
import com.example.validation.repository.PmOtifCumulativeScoreRepository;
import com.example.validation.service.PmOtifCumulativeScoreService;
import com.example.validation.service.RscIotPmOtifSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.enums.SupplierWiseReceiptRange;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PmOtifCumulativeScoreServiceImpl implements PmOtifCumulativeScoreService {
    private final PmOtifCumulativeScoreRepository pmOtifCumulativeScoreRepository;
    private final PmOtifCumulativeScoreMapper pmOtifCumulativeScoreMapper;
    private final RscIotPmOtifSummaryService rscIotPmOtifSummaryService;
    private final RscMtPpheaderService rscMtPpheaderService;

    public PmOtifCumulativeScoreServiceImpl(PmOtifCumulativeScoreRepository pmOtifCumulativeScoreRepository, PmOtifCumulativeScoreMapper pmOtifCumulativeScoreMapper, RscIotPmOtifSummaryService rscIotPmOtifSummaryService, RscMtPpheaderService rscMtPpheaderService) {
        this.pmOtifCumulativeScoreRepository = pmOtifCumulativeScoreRepository;
        this.pmOtifCumulativeScoreMapper = pmOtifCumulativeScoreMapper;
        this.rscIotPmOtifSummaryService = rscIotPmOtifSummaryService;
        this.rscMtPpheaderService = rscMtPpheaderService;
    }

    @Override
    public void calculateOtifCumulativeScore() {
        List<RscDtSupplierDTO> rscDtSupplierDTOList = rscIotPmOtifSummaryService.getAllUniqueSuppliersList();
        if (Optional.ofNullable(rscDtSupplierDTOList).isPresent()) {
            List<PmOtifCumulativeScoreDTO> pmOtifCumulativeScoreDTOS = new ArrayList<>();
            rscDtSupplierDTOList.forEach(rscDtSupplierDTO -> {
                PmOtifCumulativeScoreDTO pmOtifCumulativeScoreDTO = pmOtifCumulativeScoreMapper.toDto(pmOtifCumulativeScoreRepository.findFirstByRscDtSupplierId(rscDtSupplierDTO.getId()));
                List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList = rscIotPmOtifSummaryService.getSuppliersList(rscDtSupplierDTO.getId());
                if (Optional.ofNullable(pmOtifCumulativeScoreDTO).isPresent()) {
                    pmOtifCumulativeScoreDTO.setAveragePercentage(getAveragePercentage(pmOtifSummaryDTOList));
                    pmOtifCumulativeScoreDTO.setMaxPercentage(getMaxPercentage(pmOtifSummaryDTOList));
                    pmOtifCumulativeScoreDTO.setMinPercentage(getMinPercentage(pmOtifSummaryDTOList));
                    pmOtifCumulativeScoreDTO.setScore(getScoreByAverage(pmOtifCumulativeScoreDTO.getAveragePercentage()));
                    pmOtifCumulativeScoreDTOS.add(getCumulativeScoreFormattedAveragePercentage(pmOtifCumulativeScoreDTO));
                } else {
                    PmOtifCumulativeScoreDTO pmOtifCumulativeScoreDTO1 = new PmOtifCumulativeScoreDTO();
                    pmOtifCumulativeScoreDTO1.setSupplierId(rscDtSupplierDTO.getId());
                    pmOtifCumulativeScoreDTO1.setAveragePercentage(getAveragePercentage(pmOtifSummaryDTOList));
                    pmOtifCumulativeScoreDTO1.setMaxPercentage(getMaxPercentage(pmOtifSummaryDTOList));
                    pmOtifCumulativeScoreDTO1.setMinPercentage(getMinPercentage(pmOtifSummaryDTOList));
                    pmOtifCumulativeScoreDTO1.setScore(getScoreByAverage(pmOtifCumulativeScoreDTO1.getAveragePercentage()));
                    pmOtifCumulativeScoreDTOS.add(getCumulativeScoreFormattedAveragePercentage(pmOtifCumulativeScoreDTO1));
                }
            });
            saveAll(pmOtifCumulativeScoreDTOS);
        }
    }

    private PmOtifCumulativeScoreDTO getCumulativeScoreFormattedAveragePercentage(PmOtifCumulativeScoreDTO pmOtifCumulativeScoreDTO1) {
        pmOtifCumulativeScoreDTO1.setAveragePercentage(DoubleUtils.getTwoDecimalFormattedValue(pmOtifCumulativeScoreDTO1.getAveragePercentage()));
        return pmOtifCumulativeScoreDTO1;
    }

    private Double getScoreByAverage(Double average) {
        if (average >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE4.getNumVal()) {
            return 4.0;
        } else if (average >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE3.getNumVal() && average < SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE3.getNumVal()) {
            return 3.0;
        } else if (average >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE2.getNumVal() && average < SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE2.getNumVal()) {
            return 2.0;
        } else if (average >= SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE1.getNumVal() && average < SupplierWiseReceiptRange.RECEIPT_RANGE_MAX_COUNT_FOR_SCORE2.getNumVal()) {
            return 1.0;
        } else {
            return 0.0;
        }
    }

    private Double getMinPercentage(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList) {
        return pmOtifSummaryDTOList.stream().mapToDouble(RscIotPmOtifSummaryDTO::getAvgReceiptPercentage).min().orElse(0);
    }

    private Double getMaxPercentage(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList) {
        return pmOtifSummaryDTOList.stream().mapToDouble(RscIotPmOtifSummaryDTO::getAvgReceiptPercentage).max().orElse(0);
    }

    private Double getAveragePercentage(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList) {
        return pmOtifSummaryDTOList.stream().mapToDouble(RscIotPmOtifSummaryDTO::getAvgReceiptPercentage).average().orElse(0);
    }

    public void saveAll(List<PmOtifCumulativeScoreDTO> rscIotFgSkidSaturationDTOList) {
        pmOtifCumulativeScoreRepository.saveAll(pmOtifCumulativeScoreMapper.toEntity(rscIotFgSkidSaturationDTOList));
    }

    @Override
    public OtifCumulativeSuppliersDetailsDTO getSuppliersListByScore() {
        OtifCumulativeSuppliersDetailsDTO otifCumulativeSuppliersDetailsDTO = new OtifCumulativeSuppliersDetailsDTO();
        otifCumulativeSuppliersDetailsDTO.setScore4SuppliersList(pmOtifCumulativeScoreMapper.toDto(pmOtifCumulativeScoreRepository.findAllByScore(4.0)));
        otifCumulativeSuppliersDetailsDTO.setScore3SuppliersList(pmOtifCumulativeScoreMapper.toDto(pmOtifCumulativeScoreRepository.findAllByScore(3.0)));
        otifCumulativeSuppliersDetailsDTO.setScore2SuppliersList(pmOtifCumulativeScoreMapper.toDto(pmOtifCumulativeScoreRepository.findAllByScore(2.0)));
        otifCumulativeSuppliersDetailsDTO.setScore1SuppliersList(pmOtifCumulativeScoreMapper.toDto(pmOtifCumulativeScoreRepository.findAllByScore(1.0)));
        otifCumulativeSuppliersDetailsDTO.setScore0SuppliersList(pmOtifCumulativeScoreMapper.toDto(pmOtifCumulativeScoreRepository.findAllByScore(0.0)));
        return otifCumulativeSuppliersDetailsDTO;
    }

    @Override
    public OtifCumulativeScoreAnalysisDTO getSuppliersDetails(Long supplierId) {
        OtifCumulativeScoreAnalysisDTO otifCumulativeScoreAnalysisDTO = new OtifCumulativeScoreAnalysisDTO();
        List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList = rscIotPmOtifSummaryService.getSuppliersList(supplierId);
        if (Optional.ofNullable(pmOtifSummaryDTOList).isPresent() & pmOtifSummaryDTOList.size() > 0) {
            otifCumulativeScoreAnalysisDTO.setOtifCumulativeScoreByMonthDTOS(getOtifCumulativeScoreDetailsBySuppliers(pmOtifSummaryDTOList));
            otifCumulativeScoreAnalysisDTO.setAverageReceiptPercentage(getAveragePercentageForList(otifCumulativeScoreAnalysisDTO.getOtifCumulativeScoreByMonthDTOS()));
            otifCumulativeScoreAnalysisDTO.setScoreByAverageReceipt(getAverageScoreForList(otifCumulativeScoreAnalysisDTO.getOtifCumulativeScoreByMonthDTOS()));
        }
        return otifCumulativeScoreAnalysisDTO;
    }


    private Double getAveragePercentageForList(List<OtifCumulativeScoreByMonthDTO> otifCumulativeScoreByMonthDTOS) {
        return DoubleUtils.round(otifCumulativeScoreByMonthDTOS.stream().filter(otifCumulativeScoreByMonthDTO -> otifCumulativeScoreByMonthDTO.getActualReceiptPercentage() != null).mapToDouble(OtifCumulativeScoreByMonthDTO::getActualReceiptPercentage).sum() / 12);
    }

    private Double getAverageScoreForList(List<OtifCumulativeScoreByMonthDTO> otifCumulativeScoreByMonthDTOS) {
        return DoubleUtils.round(otifCumulativeScoreByMonthDTOS.stream().filter(otifCumulativeScoreByMonthDTO -> otifCumulativeScoreByMonthDTO.getScoreByActualReceipt() != null).mapToDouble(OtifCumulativeScoreByMonthDTO::getScoreByActualReceipt).sum() / 12);
    }

    private List<OtifCumulativeScoreByMonthDTO> getOtifCumulativeScoreDetailsBySuppliers(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList) {
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllPPheaderListSortedBasedOnMpsDateandMpsValidated();
        boolean isPlanValidated = true;
        int monthNo = 0;
        List<OtifCumulativeScoreByMonthDTO> otifCumulativeScoreByMonthDTOList = new ArrayList<>();
        if (Optional.ofNullable(pPheaderDTOList).isPresent() && pPheaderDTOList.size() > 0) {
            while (monthNo < pPheaderDTOList.size() && isPlanValidated) {
                otifCumulativeScoreByMonthDTOList.add(getOtifDetails(pPheaderDTOList.get(monthNo), pmOtifSummaryDTOList));
                monthNo++;
            }
            isPlanValidated = false;
        }
        if (!isPlanValidated) {
            while (monthNo < 12) {
                OtifCumulativeScoreByMonthDTO otifCumulativeScoreByMonthDTO = new OtifCumulativeScoreByMonthDTO();
                otifCumulativeScoreByMonthDTO.setMonthValue(getCumulativeMonthValue(pPheaderDTOList.get(0).getMpsDate().plusMonths(monthNo)));
                otifCumulativeScoreByMonthDTOList.add(otifCumulativeScoreByMonthDTO);
                monthNo++;
            }
        }
        return otifCumulativeScoreByMonthDTOList;
    }

    private OtifCumulativeScoreByMonthDTO getOtifDetails(RscMtPPheaderDTO pPheaderDTOList, List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList) {
        OtifCumulativeScoreByMonthDTO otifCumulativeScoreByMonthDTO = new OtifCumulativeScoreByMonthDTO();
        otifCumulativeScoreByMonthDTO.setMonthValue(getCumulativeMonthValue(pPheaderDTOList.getMpsDate()));
        otifCumulativeScoreByMonthDTO.setScoreByActualReceipt(getScoreByActualReceipt(pmOtifSummaryDTOList, pPheaderDTOList));
        otifCumulativeScoreByMonthDTO.setActualReceiptPercentage(getPercentageByActualReceipt(pmOtifSummaryDTOList, pPheaderDTOList));
        return otifCumulativeScoreByMonthDTO;
    }

    private Double getScoreByActualReceipt(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList, RscMtPPheaderDTO pPheaderDTOList) {
        if (pmOtifSummaryDTOList.stream().filter(rscIotPmOtifSummaryDTO -> rscIotPmOtifSummaryDTO.getReceiptHeaderDate().getMonth().name().matches(pPheaderDTOList.getMpsDate().getMonth().name())).count() > 0) {
            return pmOtifSummaryDTOList.stream().filter(rscIotPmOtifSummaryDTO -> rscIotPmOtifSummaryDTO.getReceiptHeaderDate().getMonth().name().matches(pPheaderDTOList.getMpsDate().getMonth().name())).mapToDouble(RscIotPmOtifSummaryDTO::getAvgScore).sum();
        } else {
            return null;
        }
    }

    private Double getPercentageByActualReceipt(List<RscIotPmOtifSummaryDTO> pmOtifSummaryDTOList, RscMtPPheaderDTO pPheaderDTOList) {
        if (pmOtifSummaryDTOList.stream().filter(rscIotPmOtifSummaryDTO -> rscIotPmOtifSummaryDTO.getReceiptHeaderDate().getMonth().name().matches(pPheaderDTOList.getMpsDate().getMonth().name())).count() > 0) {
            return pmOtifSummaryDTOList.stream().filter(rscIotPmOtifSummaryDTO -> rscIotPmOtifSummaryDTO.getReceiptHeaderDate().getMonth().name().matches(pPheaderDTOList.getMpsDate().getMonth().name())).mapToDouble(RscIotPmOtifSummaryDTO::getAvgReceiptPercentage).sum();
        } else {
            return null;
        }
    }

    private String getCumulativeMonthValue(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate) + "-" + mpsDate.getYear() % 100;
    }
}
