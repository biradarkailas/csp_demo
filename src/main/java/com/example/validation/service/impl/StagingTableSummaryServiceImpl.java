package com.example.validation.service.impl;

import com.example.validation.dto.StagingTableSummaryDTO;
import com.example.validation.mapper.StagingTableSummaryMapper;
import com.example.validation.repository.StagingTableSummaryRepository;
import com.example.validation.service.StagingTableSummaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class StagingTableSummaryServiceImpl implements StagingTableSummaryService {
    private final Logger log = LoggerFactory.getLogger(StagingTableSummaryServiceImpl.class);
    private final StagingTableSummaryRepository stagingTableSummaryRepository;
    private final StagingTableSummaryMapper stagingTableSummaryMapper;

    public StagingTableSummaryServiceImpl(StagingTableSummaryRepository stagingTableSummaryRepository, StagingTableSummaryMapper stagingTableSummaryMapper) {
        this.stagingTableSummaryRepository = stagingTableSummaryRepository;
        this.stagingTableSummaryMapper = stagingTableSummaryMapper;
    }

    @Override
    public void save(StagingTableSummaryDTO stagingTableSummaryDTO) {
        log.debug("Request to save stagingTableSummary : {}", stagingTableSummaryDTO);
        stagingTableSummaryRepository.save(stagingTableSummaryMapper.toEntity(stagingTableSummaryDTO));
    }

    @Override
    public void saveAll(List<StagingTableSummaryDTO> stagingTableSummaryDTOList) {
        log.debug("Request to save all stagingTableSummary : {}", stagingTableSummaryDTOList);
        stagingTableSummaryRepository.saveAll(stagingTableSummaryMapper.toEntity(stagingTableSummaryDTOList));
    }

    @Override
    public List<StagingTableSummaryDTO> getAllStagingTableSummary(List<String> tableNames) {
        log.debug("Request to findAll active stagingTableSummary : {}");
        return stagingTableSummaryMapper.toDto(stagingTableSummaryRepository.findByTableNameInAndIsActive(tableNames, true));
    }

    @Override
    public List<StagingTableSummaryDTO> findAllByTableNames(List<String> sanitizeTableNames) {
        log.debug("Request to Find all Validation Table Names by Sanitize Table Name : {}", sanitizeTableNames);
        return stagingTableSummaryMapper.toDto(stagingTableSummaryRepository.findByTableNameInAndIsSanitizeAndIsActive(sanitizeTableNames, true, true));
    }

    @Override
    public List<StagingTableSummaryDTO> findAllByTableNamesAndIsValidate(List<String> sanitizeTableNames) {
        log.debug("Request to Find all Validation Table Names by Sanitize Table Name : {}", sanitizeTableNames);
        return stagingTableSummaryMapper.toDto(stagingTableSummaryRepository.findByTableNameInAndIsValidateAndIsActive(sanitizeTableNames, true, true));
    }

    @Override
    public Boolean isValidStagingTableSummaryCount() {
        return stagingTableSummaryRepository.countByIsActiveAndIsValidate(true, false) == 0;
    }

    @Override
    public List<String> findAllTableNamesByValidate() {
        return stagingTableSummaryMapper.toDto(stagingTableSummaryRepository.findByIsValidateAndIsActive(true, true))
                .stream()
                .map(StagingTableSummaryDTO::getTableName)
                .collect(Collectors.toList());
    }

    @Override
    public Boolean isValidStagingTableSummaryByTableName(String activeTableName) {
        return stagingTableSummaryRepository.countByTableNameAndIsActive(activeTableName, true) == 0;
    }

    @Override
    public List<StagingTableSummaryDTO> findAllByIsActive() {
        return stagingTableSummaryMapper.toDto(stagingTableSummaryRepository.findByIsActive(true));
    }

    @Override
    public StagingTableSummaryDTO findByIsActiveAndTableName(String tableName) {
        return stagingTableSummaryMapper.toDto(stagingTableSummaryRepository.findByIsActiveAndTableName(true, tableName));
    }

    @Override
    public void inActiveAllStagingTableSummary() {
        List<StagingTableSummaryDTO> activeStagingTableSummaryDTO = findAllByIsActive();
        if (Optional.ofNullable(activeStagingTableSummaryDTO).isPresent()) {
            for (StagingTableSummaryDTO stagingTableSummary : activeStagingTableSummaryDTO) {
                stagingTableSummary.setActive(false);
            }
            saveAll(activeStagingTableSummaryDTO);
        }
    }

    @Override
    public void deleteAll(LocalDate mpsDate) {
        stagingTableSummaryRepository.deleteAllByMpsDateBetween(mpsDate.withDayOfMonth(1), mpsDate.withDayOfMonth(mpsDate.lengthOfMonth()));
    }
}