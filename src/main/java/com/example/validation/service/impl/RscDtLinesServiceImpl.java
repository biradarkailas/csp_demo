package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtLinesDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.RscDtLinesMapper;
import com.example.validation.repository.RscDtLinesRepository;
import com.example.validation.service.RscDtLinesService;
import com.example.validation.util.excel_export.FgMasterDataLineSaturationExporter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RscDtLinesServiceImpl implements RscDtLinesService {
    private final RscDtLinesRepository rscDtLinesRepository;
    private final RscDtLinesMapper rscDtLinesMapper;

    public RscDtLinesServiceImpl(RscDtLinesRepository rscDtLinesRepository, RscDtLinesMapper rscDtLinesMapper) {
        this.rscDtLinesRepository = rscDtLinesRepository;
        this.rscDtLinesMapper = rscDtLinesMapper;
    }

    @Override
    public List<RscDtLinesDTO> findAll() {
        return rscDtLinesMapper.toDto( rscDtLinesRepository.findAll() );
    }

    @Override
    public List<RscDtLinesDTO> findFgLinesWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<RscDtLinesDTO> materialCategoryListResult = rscDtLinesRepository.findAll( paging )
                .map( rscDtLinesMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public RscDtLinesDTO findById(Long rscDtLinesId) {
        return rscDtLinesMapper.toDto( rscDtLinesRepository.findById( rscDtLinesId ).get() );
    }

    public ExcelExportFiles fgMasterDatalineExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscDtLinesDTO> rscDtLinesDTOS = findAll();
        excelExportFiles.setFile( FgMasterDataLineSaturationExporter.masterDatalineSaturationToExcel( rscDtLinesDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.FG_Master_Data_LINE_SATURATION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }
}