package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.mapper.*;
import com.example.validation.repository.RscIotRmCoverDaysClassASummaryRepository;
import com.example.validation.repository.RscIotRmCoverDaysClassBSummaryRepository;
import com.example.validation.repository.RscIotRmCoverDaysClassCSummaryRepository;
import com.example.validation.service.*;
import com.example.validation.util.CoverDaysMonthUtils;
import com.example.validation.util.excel_export.RmCoverDaysSummaryExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotRmCoverDaysClassSummaryServiceImpl implements RscIotRmCoverDaysClassSummaryService {
    private final RscIotRmCoverDaysClassASummaryRepository rscIotRmCoverDaysClassASummaryRepository;
    private final RscIotRmCoverDaysClassBSummaryRepository rscIotRmCoverDaysClassBSummaryRepository;
    private final RscIotRmCoverDaysClassCSummaryRepository rscIotRmCoverDaysClassCSummaryRepository;
    private final RscIotRmCoverDaysClassASummaryMapper rscIotRmCoverDaysClassASummaryMapper;
    private final RscIotRmCoverDaysClassBSummaryMapper rscIotRmCoverDaysClassBSummaryMapper;
    private final RscIotRmCoverDaysClassCSummaryMapper rscIotRmCoverDaysClassCSummaryMapper;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;
    private final CoverDaysMonthUtils coverDaysMonthUtils;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotRmCoverDaysClassAConsumptionMapper rscIotRmCoverDaysClassAConsumptionMapper;
    private final RscIotRmCoverDaysClassBConsumptionMapper rscIotRmCoverDaysClassBConsumptionMapper;
    private final RscIotRmCoverDaysClassCConsumptionMapper rscIotRmCoverDaysClassCConsumptionMapper;
    private final RscIotRmCoverDaysService rscIotRmCoverDaysService;
    private final RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService;
    private final RscIotRMSlobService rscIotRMSlobService;

    public RscIotRmCoverDaysClassSummaryServiceImpl(RscIotRmCoverDaysClassASummaryRepository rscIotRmCoverDaysClassASummaryRepository, RscIotRmCoverDaysClassBSummaryRepository rscIotRmCoverDaysClassBSummaryRepository, RscIotRmCoverDaysClassCSummaryRepository rscIotRmCoverDaysClassCSummaryRepository, RscIotRmCoverDaysClassASummaryMapper rscIotRmCoverDaysClassASummaryMapper, RscIotRmCoverDaysClassBSummaryMapper rscIotRmCoverDaysClassBSummaryMapper, RscIotRmCoverDaysClassCSummaryMapper rscIotRmCoverDaysClassCSummaryMapper, RscIotRmSlobSummaryService rscIotRmSlobSummaryService, CoverDaysMonthUtils coverDaysMonthUtils, RscMtPpheaderService rscMtPpheaderService, RscIotRmCoverDaysClassAConsumptionMapper rscIotRmCoverDaysClassAConsumptionMapper, RscIotRmCoverDaysClassBConsumptionMapper rscIotRmCoverDaysClassBConsumptionMapper, RscIotRmCoverDaysClassCConsumptionMapper rscIotRmCoverDaysClassCConsumptionMapper, RscIotRmCoverDaysService rscIotRmCoverDaysService, RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService, RscIotRMSlobService rscIotRMSlobService) {
        this.rscIotRmCoverDaysClassASummaryRepository = rscIotRmCoverDaysClassASummaryRepository;
        this.rscIotRmCoverDaysClassBSummaryRepository = rscIotRmCoverDaysClassBSummaryRepository;
        this.rscIotRmCoverDaysClassCSummaryRepository = rscIotRmCoverDaysClassCSummaryRepository;
        this.rscIotRmCoverDaysClassASummaryMapper = rscIotRmCoverDaysClassASummaryMapper;
        this.rscIotRmCoverDaysClassBSummaryMapper = rscIotRmCoverDaysClassBSummaryMapper;
        this.rscIotRmCoverDaysClassCSummaryMapper = rscIotRmCoverDaysClassCSummaryMapper;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
        this.coverDaysMonthUtils = coverDaysMonthUtils;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotRmCoverDaysClassAConsumptionMapper = rscIotRmCoverDaysClassAConsumptionMapper;
        this.rscIotRmCoverDaysClassBConsumptionMapper = rscIotRmCoverDaysClassBConsumptionMapper;
        this.rscIotRmCoverDaysClassCConsumptionMapper = rscIotRmCoverDaysClassCConsumptionMapper;
        this.rscIotRmCoverDaysService = rscIotRmCoverDaysService;
        this.rscIotRmCoverDaysSummaryService = rscIotRmCoverDaysSummaryService;
        this.rscIotRMSlobService = rscIotRMSlobService;
    }

    @Override
    public void calculateRmCoverDaysAllClassSummary(Long rscMtPPHeaderId) {
        List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList = rscIotRmCoverDaysService.getRscItRmCoverDaysList(rscMtPPHeaderId);
        if (Optional.ofNullable(rmCoverDaysDTOList).isPresent()) {
            if (rscIotRmCoverDaysClassASummaryRepository.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
                calculateRmCoverDaysClassASummary(getClassACoverDaysList(rmCoverDaysDTOList), rscMtPPHeaderId);
            }
            if (rscIotRmCoverDaysClassBSummaryRepository.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
                calculateRmCoverDaysClassBSummary(getClassBCoverDaysList(rmCoverDaysDTOList), rscMtPPHeaderId);
            }
            if (rscIotRmCoverDaysClassCSummaryRepository.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
                calculateRmCoverDaysClassCSummary(getClassCCoverDaysList(rmCoverDaysDTOList), rscMtPPHeaderId);
            }
        }
    }

    private List<RscIotRmCoverDaysDTO> getClassACoverDaysList(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList) {
        return rmCoverDaysDTOList.stream().filter(rscIotRmCoverDaysDTO -> rscIotRmCoverDaysDTO.getClassName().equals("A")).collect(Collectors.toList());
    }

    private List<RscIotRmCoverDaysDTO> getClassBCoverDaysList(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList) {
        return rmCoverDaysDTOList.stream().filter(rscIotRmCoverDaysDTO -> rscIotRmCoverDaysDTO.getClassName().equals("B")).collect(Collectors.toList());
    }

    private List<RscIotRmCoverDaysDTO> getClassCCoverDaysList(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList) {
        return rmCoverDaysDTOList.stream().filter(rscIotRmCoverDaysDTO -> rscIotRmCoverDaysDTO.getClassName().equals("C")).collect(Collectors.toList());
    }

    private void calculateRmCoverDaysClassASummary(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList, Long rscMtPPHeaderId) {
        rscIotRmCoverDaysClassASummaryRepository.save(rscIotRmCoverDaysClassASummaryMapper.toEntity(getCoverDaysSummary(rmCoverDaysDTOList, rscMtPPHeaderId)));
    }

    private void calculateRmCoverDaysClassBSummary(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList, Long rscMtPPHeaderId) {
        rscIotRmCoverDaysClassBSummaryRepository.save(rscIotRmCoverDaysClassBSummaryMapper.toEntity(getCoverDaysSummary(rmCoverDaysDTOList, rscMtPPHeaderId)));
    }

    private void calculateRmCoverDaysClassCSummary(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList, Long rscMtPPHeaderId) {
        rscIotRmCoverDaysClassCSummaryRepository.save(rscIotRmCoverDaysClassCSummaryMapper.toEntity(getCoverDaysSummary(rmCoverDaysDTOList, rscMtPPHeaderId)));
    }

    private RscIotRmCoverDaysSummaryDTO getCoverDaysSummary(List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList, Long rscMtPPHeaderId) {
        RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryClassDTO = new RscIotRmCoverDaysSummaryDTO();
        rscIotRmCoverDaysSummaryClassDTO.setRscMtPPHeaderId(rscMtPPHeaderId);
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);

        if (Optional.ofNullable(rmCoverDaysDTOList).isPresent()) {
            RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId(rscMtPPHeaderId);
            Double sitValue = 0.0;
            if (rscIotRmSlobSummaryDTO != null) {
                sitValue = rscIotRmSlobSummaryDTO.getSitValue();
            }
            if (sitValue == 0.0) {
                rscIotRmCoverDaysSummaryClassDTO = coverDaysMonthUtils.getRmCoverDaysWhenSitValueZero(rmCoverDaysDTOList, rscIotRmCoverDaysSummaryClassDTO, pPheaderDTO, sitValue, rscIotRmSlobSummaryDTO);
            } else {
                rscIotRmCoverDaysSummaryClassDTO = coverDaysMonthUtils.getRmCoverDaysWhenSitValueZero(rmCoverDaysDTOList, rscIotRmCoverDaysSummaryClassDTO, pPheaderDTO, 0.0, rscIotRmSlobSummaryDTO);
                coverDaysMonthUtils.getRmCoverDaysWhenSitValueZero(rmCoverDaysDTOList, rscIotRmCoverDaysSummaryClassDTO, pPheaderDTO, sitValue, rscIotRmSlobSummaryDTO);
            }
        }
        return rscIotRmCoverDaysSummaryClassDTO;
    }

    @Override
    public ClassWiseCoverDaysDetailsDTO getAllClassConsumptionByPpHeaderId(Long rscMtPPHeaderId) {
        ClassWiseCoverDaysDetailsDTO classWiseCoverDaysDetailsDTO = new ClassWiseCoverDaysDetailsDTO();
        classWiseCoverDaysDetailsDTO.setClassACoverDaysConsumptionDTO(getClassAConsumptionDetails(rscMtPPHeaderId));
        classWiseCoverDaysDetailsDTO.setClassBCoverDaysConsumptionDTO(getClassBConsumptionDetails(rscMtPPHeaderId));
        classWiseCoverDaysDetailsDTO.setClassCCoverDaysConsumptionDTO(getClassCConsumptionDetails(rscMtPPHeaderId));
        return classWiseCoverDaysDetailsDTO;
    }

    private ClassWiseDetailsDTO getClassAConsumptionDetails(Long rscMtPPHeaderId) {
        return rscIotRmCoverDaysClassAConsumptionMapper.toDto(rscIotRmCoverDaysClassASummaryRepository.findByRscMtPPheaderId(rscMtPPHeaderId));
    }

    private ClassWiseDetailsDTO getClassBConsumptionDetails(Long rscMtPPHeaderId) {
        return rscIotRmCoverDaysClassBConsumptionMapper.toDto(rscIotRmCoverDaysClassBSummaryRepository.findByRscMtPPheaderId(rscMtPPHeaderId));
    }

    private ClassWiseDetailsDTO getClassCConsumptionDetails(Long rscMtPPHeaderId) {
        return rscIotRmCoverDaysClassCConsumptionMapper.toDto(rscIotRmCoverDaysClassCSummaryRepository.findByRscMtPPheaderId(rscMtPPHeaderId));
    }

    @Override
    public void deleteClassSummaryById(Long rscMtPPHeaderId) {
        rscIotRmCoverDaysClassASummaryRepository.deleteByRscMtPPheaderId(rscMtPPHeaderId);
        rscIotRmCoverDaysClassBSummaryRepository.deleteByRscMtPPheaderId(rscMtPPHeaderId);
        rscIotRmCoverDaysClassCSummaryRepository.deleteByRscMtPPheaderId(rscMtPPHeaderId);
    }

    @Override
    public ClassWiseCoverDaysDetailsDTO getRmCoverDaysClassWiseSummaryDetails(Long ppHeaderId) {
        ClassWiseCoverDaysDetailsDTO classWiseCoverDaysDetailsDTO = new ClassWiseCoverDaysDetailsDTO();
        Optional<RscMtPPheaderDTO> pPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPHeaderDTO.isPresent()) {
            classWiseCoverDaysDetailsDTO = getAllClassConsumptionByPpHeaderId(ppHeaderId);
            classWiseCoverDaysDetailsDTO.setTotalCoverDaysConsumptionDTO(rscIotRmCoverDaysSummaryService.getTotalCoverDaysConsumption(ppHeaderId));
            classWiseCoverDaysDetailsDTO.setRscMtPPHeaderId(ppHeaderId);
        }
        return classWiseCoverDaysDetailsDTO;
    }

    @Override
    public ExcelExportFiles rmCoverDaysSummaryExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO = rscIotRmCoverDaysService.getRmCoverDaysSummaryAllDetails(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(RmCoverDaysSummaryExporter.rmCoverDaysSummaryToExcel(rscIotRMSlobService.getRmSlobSummaryDetails(rscMtPPheaderDTO.getId()), coverDaysSummaryDetailsDTO, getRmCoverDaysClassWiseSummaryDetails(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_SLOB_COVER_DAYS_SUMMARY + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }
}
