package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtMouldDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.RscDtMouldMapper;
import com.example.validation.repository.RscDtMouldRepository;
import com.example.validation.service.RscDtMouldService;
import com.example.validation.util.excel_export.PmMasterDataMouldExporter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RscDtMouldServiceImpl implements RscDtMouldService {
    private final RscDtMouldRepository rscDtMouldRepository;
    private final RscDtMouldMapper rscDtMouldMapper;

    public RscDtMouldServiceImpl(RscDtMouldRepository rscDtMouldRepository, RscDtMouldMapper rscDtMouldMapper) {
        this.rscDtMouldRepository = rscDtMouldRepository;
        this.rscDtMouldMapper = rscDtMouldMapper;
    }

    @Override
    public List<RscDtMouldDTO> findAll() {
        return rscDtMouldMapper.toDto( rscDtMouldRepository.findAll() );
    }

    @Override
    public List<RscDtMouldDTO> findPmMouldWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<RscDtMouldDTO> materialCategoryListResult = rscDtMouldRepository.findAll( paging )
                .map( rscDtMouldMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles pmMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscDtMouldDTO> rscDtMouldDTOS = findAll();
        excelExportFiles.setFile( PmMasterDataMouldExporter.masterDataMouldToExcel( rscDtMouldDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.PM_Master_Data_Mould + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }
}