package com.example.validation.service.impl;

import com.example.validation.dto.ExecutionErrorLogDTO;
import com.example.validation.mapper.ExecutionErrorLogMapper;
import com.example.validation.repository.ExecutionErrorLogRepository;
import com.example.validation.service.ExecutionErrorLogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class ExecutionErrorLogServiceImpl implements ExecutionErrorLogService {
    private final ExecutionErrorLogRepository executionErrorLogRepository;
    private final ExecutionErrorLogMapper executionErrorLogMapper;

    public ExecutionErrorLogServiceImpl(ExecutionErrorLogRepository executionErrorLogRepository, ExecutionErrorLogMapper executionErrorLogMapper) {
        this.executionErrorLogRepository = executionErrorLogRepository;
        this.executionErrorLogMapper = executionErrorLogMapper;
    }

    @Override
    public void saveExecutionError(String methodName, String errorDescription, Long ppHeaderId) {
        ExecutionErrorLogDTO executionErrorLogDTO = new ExecutionErrorLogDTO();
        executionErrorLogDTO.setMethodName(methodName);
        executionErrorLogDTO.setErrorDescription(errorDescription);
        executionErrorLogDTO.setRscMtPPheaderId(ppHeaderId);
        executionErrorLogDTO.setCreatedDate(LocalDateTime.now());
        executionErrorLogRepository.save(executionErrorLogMapper.toEntity(executionErrorLogDTO));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        executionErrorLogRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}

