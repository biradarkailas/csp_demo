package com.example.validation.service.impl;

import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.RscItWIPGrossConsumptionMapper;
import com.example.validation.mapper.WIPGrossConsumptionMapper;
import com.example.validation.mapper.WIPItemGrossConsumptionMapper;
import com.example.validation.repository.RscItWIPGrossConsumptionRepository;
import com.example.validation.service.RscItWIPGrossConsumptionService;
import com.example.validation.util.consumption.ConsumptionUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class RscItWIPGrossConsumptionServiceImpl implements RscItWIPGrossConsumptionService {
    private final RscItWIPGrossConsumptionRepository rscItWIPGrossConsumptionRepository;
    private final RscItWIPGrossConsumptionMapper rscItWIPGrossConsumptionMapper;
    private final WIPGrossConsumptionMapper wipGrossConsumptionMapper;
    private final ConsumptionUtil consumptionUtil;
    private final WIPItemGrossConsumptionMapper wipItemGrossConsumptionMapper;

    public RscItWIPGrossConsumptionServiceImpl(RscItWIPGrossConsumptionRepository rscItWIPGrossConsumptionRepository,
                                               RscItWIPGrossConsumptionMapper rscItWIPGrossConsumptionMapper,
                                               ConsumptionUtil consumptionUtil,
                                               WIPGrossConsumptionMapper wipGrossConsumptionMapper,
                                               WIPItemGrossConsumptionMapper wipItemGrossConsumptionMapper) {
        this.rscItWIPGrossConsumptionRepository = rscItWIPGrossConsumptionRepository;
        this.rscItWIPGrossConsumptionMapper = rscItWIPGrossConsumptionMapper;
        this.consumptionUtil = consumptionUtil;
        this.wipGrossConsumptionMapper = wipGrossConsumptionMapper;
        this.wipItemGrossConsumptionMapper = wipItemGrossConsumptionMapper;
    }

    @Override
    public void saveAll(List<RscItGrossConsumptionDTO> wipRscItGrossConsumptionDTOs, Long ppHeaderId) {
        if (rscItWIPGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            wipRscItGrossConsumptionDTOs.sort(Comparator.comparing(RscItGrossConsumptionDTO::getItemCode));
            rscItWIPGrossConsumptionRepository.saveAll(rscItWIPGrossConsumptionMapper.toEntity(wipRscItGrossConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItWIPGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscItWIPGrossConsumptionMapper.toDto(rscItWIPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllGrossConsumptionDTOByRscMtPPheaderId(Long ppHeaderId) {
        List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs = new ArrayList<>();
        rscItGrossConsumptionDTOs.addAll(rscItWIPGrossConsumptionMapper.toDto(rscItWIPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        return rscItGrossConsumptionDTOs;
    }

    @Override
    public ItemDetailDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long wipItemId) {
        return wipItemGrossConsumptionMapper.toDto(rscItWIPGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, wipItemId));
    }

    @Override
    public List<ItemDetailDTO> findByAllRscMtPPheaderId(Long ppHeaderId) {
        return wipItemGrossConsumptionMapper.toDto(rscItWIPGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItWIPGrossConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}