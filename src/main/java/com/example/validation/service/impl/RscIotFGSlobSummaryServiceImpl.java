package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.FGSlobSummaryWithFgTypeAndDivisionDTO;
import com.example.validation.dto.RscDtFGTypeDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.slob.*;
import com.example.validation.mapper.FGSlobSummaryDashboardMapper;
import com.example.validation.mapper.FGSlobSummaryMapper;
import com.example.validation.mapper.RscIotFGSlobSummaryDashboardMapper;
import com.example.validation.mapper.RscIotFGSlobSummaryMapper;
import com.example.validation.repository.RscIotFGSlobSummaryRepository;
import com.example.validation.service.*;
import com.example.validation.util.FGSlobSummaryUtil;
import com.example.validation.util.SlobSummaryUtil;
import com.example.validation.util.excel_export.FgSlobSummaryExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RscIotFGSlobSummaryServiceImpl implements RscIotFGSlobSummaryService {
    private final RscIotFGSlobSummaryRepository rscIotFGSlobSummaryRepository;
    private final RscIotFGSlobSummaryMapper rscIotFGSlobSummaryMapper;
    private final RscIotFGTypeSlobSummaryService rscIotFGTypeSlobSummaryService;
    private final FGSlobSummaryMapper fgSlobSummaryMapper;
    private final RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final FGSlobSummaryUtil fgSlobSummaryUtil;
    private final FGSlobSummaryDashboardMapper fgSlobSummaryDashboardMapper;
    private final RscDtFGTypeService rscDtFGTypeService;
    private final RscIotFGSlobSummaryDashboardMapper rscIotFGSlobSummaryDashboardMapper;
    private final SlobSummaryUtil slobSummaryUtil;

    public RscIotFGSlobSummaryServiceImpl(RscIotFGSlobSummaryRepository rscIotFGSlobSummaryRepository, RscIotFGSlobSummaryMapper rscIotFGSlobSummaryMapper, RscIotFGTypeSlobSummaryService rscIotFGTypeSlobSummaryService, FGSlobSummaryMapper fgSlobSummaryMapper, RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService, RscMtPpheaderService rscMtPpheaderService, FGSlobSummaryUtil fgSlobSummaryUtil, FGSlobSummaryDashboardMapper fgSlobSummaryDashboardMapper, RscDtFGTypeService rscDtFGTypeService, RscIotFGSlobSummaryDashboardMapper rscIotFGSlobSummaryDashboardMapper, SlobSummaryUtil slobSummaryUtil) {
        this.rscIotFGSlobSummaryRepository = rscIotFGSlobSummaryRepository;
        this.rscIotFGSlobSummaryMapper = rscIotFGSlobSummaryMapper;
        this.rscIotFGTypeSlobSummaryService = rscIotFGTypeSlobSummaryService;
        this.fgSlobSummaryMapper = fgSlobSummaryMapper;
        this.rscIotFGTypeDivisionSlobSummaryService = rscIotFGTypeDivisionSlobSummaryService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.fgSlobSummaryUtil = fgSlobSummaryUtil;
        this.fgSlobSummaryDashboardMapper = fgSlobSummaryDashboardMapper;
        this.rscDtFGTypeService = rscDtFGTypeService;
        this.rscIotFGSlobSummaryDashboardMapper = rscIotFGSlobSummaryDashboardMapper;
        this.slobSummaryUtil = slobSummaryUtil;
    }

    @Override
    public void createFGSlobSummary(Long ppHeaderId) {
        if (rscIotFGSlobSummaryRepository.countByRscMtPPheaderId(ppHeaderId) == 0)
            rscIotFGSlobSummaryRepository.save(rscIotFGSlobSummaryMapper.toEntity(getRscIotFGSlobSummaryDTOs(ppHeaderId)));
    }

    private RscIotFGSlobSummaryDTO getRscIotFGSlobSummaryDTOs(Long ppHeaderId) {
        List<RscIotFGTypeSlobSummaryDTO> rscIotFGTypeSlobSummaryDTOS = rscIotFGTypeSlobSummaryService.findAll(ppHeaderId);
        Map<String, Double> fgSlobMap = new HashMap<>();
        if (Optional.ofNullable(rscIotFGTypeSlobSummaryDTOS).isPresent()) {
            for (RscIotFGTypeSlobSummaryDTO rscIotFGTypeSlobSummaryDTO : rscIotFGTypeSlobSummaryDTOS) {
                fgSlobMap.put(MaterialCategoryConstants.STOCK, fgSlobSummaryUtil.getValidTotalValue(fgSlobMap, rscIotFGTypeSlobSummaryDTO.getTotalStockValue(), MaterialCategoryConstants.STOCK));
                fgSlobMap.put(MaterialCategoryConstants.SLOB, fgSlobSummaryUtil.getValidTotalValue(fgSlobMap, rscIotFGTypeSlobSummaryDTO.getTotalSlobValue(), MaterialCategoryConstants.SLOB));
                fgSlobMap.put(MaterialCategoryConstants.SLOW, fgSlobSummaryUtil.getValidTotalValue(fgSlobMap, rscIotFGTypeSlobSummaryDTO.getTotalSlowItemValue(), MaterialCategoryConstants.SLOW));
                fgSlobMap.put(MaterialCategoryConstants.OBSOLETE, fgSlobSummaryUtil.getValidTotalValue(fgSlobMap, rscIotFGTypeSlobSummaryDTO.getTotalObsoleteItemValue(), MaterialCategoryConstants.OBSOLETE));
            }
        }
        return getRscIotFGSlobSummaryDTO(fgSlobMap, ppHeaderId);
    }

    private RscIotFGSlobSummaryDTO getRscIotFGSlobSummaryDTO(Map<String, Double> fgSlobMap, Long ppHeaderId) {
        RscIotFGSlobSummaryDTO rscIotFGSlobSummaryDTO = new RscIotFGSlobSummaryDTO();
        if (Optional.ofNullable(fgSlobMap).isPresent()) {
            rscIotFGSlobSummaryDTO.setRscMtPPheaderId(ppHeaderId);
            rscIotFGSlobSummaryDTO.setTotalStockValue(fgSlobMap.get(MaterialCategoryConstants.STOCK));
            rscIotFGSlobSummaryDTO.setTotalSlobValue(fgSlobMap.get(MaterialCategoryConstants.SLOB));
            rscIotFGSlobSummaryDTO.setTotalSlowItemValue(fgSlobMap.get(MaterialCategoryConstants.SLOW));
            rscIotFGSlobSummaryDTO.setTotalObsoleteItemValue(fgSlobMap.get(MaterialCategoryConstants.OBSOLETE));
            Double slobPercentageValue = fgSlobSummaryUtil.getPercentageValue(fgSlobMap, MaterialCategoryConstants.SLOB);
            rscIotFGSlobSummaryDTO.setSlobItemPercentage(slobPercentageValue);
            rscIotFGSlobSummaryDTO.setSlowItemPercentage(fgSlobSummaryUtil.getPercentageValue(fgSlobMap, MaterialCategoryConstants.SLOW));
            rscIotFGSlobSummaryDTO.setObsoleteItemPercentage(fgSlobSummaryUtil.getPercentageValue(fgSlobMap, MaterialCategoryConstants.OBSOLETE));
            rscIotFGSlobSummaryDTO.setStockQualityPercentage(fgSlobSummaryUtil.getStockQualityPercentage(slobPercentageValue));
            rscIotFGSlobSummaryDTO.setStockQualityValue(fgSlobSummaryUtil.getStockQualityValue(rscIotFGSlobSummaryDTO.getTotalStockValue(), rscIotFGSlobSummaryDTO.getStockQualityPercentage()));
        }

        return rscIotFGSlobSummaryDTO;
    }

    @Override
    public FGSlobSummaryDTO getFGSlobSummaryDTO(Long ppHeaderId) {
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        return getFgSlobSummary(rscMtPPheaderDTO);
    }

    private FGSlobSummaryDTO getFgSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO) {
        FGSlobSummaryDTO fgSlobSummaryDTO = fgSlobSummaryMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(rscMtPPheaderDTO.getId()));
        if (Optional.ofNullable(fgSlobSummaryDTO).isPresent()) {
            fgSlobSummaryDTO.setCurrentMonthValue(slobSummaryUtil.getPreviousMonthName(rscMtPPheaderDTO.getMpsDate()));
            fgSlobSummaryDTO.setPreviousMonthValue(slobSummaryUtil.getPreviousSecondMonthName(rscMtPPheaderDTO.getMpsDate()));
            List<TypeWiseFGSlobSummaryDTO> typeWiseFGSlobSummaryDTOS = new ArrayList<>();
            for (TypeWiseFGSlobSummaryDTO typeWiseFGSlobSummaryDTO : rscIotFGTypeSlobSummaryService.findAllMaterialWiseFGSlobSummaryDTO(rscMtPPheaderDTO.getId())) {
                typeWiseFGSlobSummaryDTO.setDivisionWiseFGSlobSummaryDTOs(rscIotFGTypeDivisionSlobSummaryService.findAllByFGTypeId(rscMtPPheaderDTO.getId(), typeWiseFGSlobSummaryDTO.getFgTypeId()));
                typeWiseFGSlobSummaryDTOS.add(typeWiseFGSlobSummaryDTO);
            }
            fgSlobSummaryDTO.setTypeWiseFGSlobSummaryDTOS(typeWiseFGSlobSummaryDTOS);
        }
        return fgSlobSummaryDTO;
    }

    @Override
    public FGSlobDeviationMainDTO getFGTotalSlobDeviation(Long ppHeaderId) {
        FGSlobDeviationMainDTO fgSlobDeviationMainDTO = new FGSlobDeviationMainDTO();
        FGSlobSummaryDTO currentMonthFGSlobSummaryDTO = fgSlobSummaryMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
        Long previousMonthMpsId = rscMtPpheaderService.getPreviousMonthsFirstCutMpsId(ppHeaderId);
        if (Optional.ofNullable(currentMonthFGSlobSummaryDTO).isPresent()) {
            if (Optional.ofNullable(previousMonthMpsId).isPresent()) {
                FGSlobSummaryDTO previousMonthFGSlobSummaryDTO = fgSlobSummaryMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(previousMonthMpsId));
                if (Optional.ofNullable(previousMonthFGSlobSummaryDTO).isPresent()) {
                    fgSlobDeviationMainDTO.setObDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalObsoleteItemValue(), previousMonthFGSlobSummaryDTO.getTotalObsoleteItemValue()));
                    fgSlobDeviationMainDTO.setSlDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalSlowItemValue(), previousMonthFGSlobSummaryDTO.getTotalSlowItemValue()));
                    fgSlobDeviationMainDTO.setSlobDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalSlobValue(), previousMonthFGSlobSummaryDTO.getTotalSlobValue()));
                    fgSlobDeviationMainDTO.setStockDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalStockValue(), previousMonthFGSlobSummaryDTO.getTotalStockValue()));
                    fgSlobDeviationMainDTO.setSlPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getSlowItemPercentage(), previousMonthFGSlobSummaryDTO.getSlowItemPercentage()));
                    fgSlobDeviationMainDTO.setObPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getObsoleteItemPercentage(), previousMonthFGSlobSummaryDTO.getObsoleteItemPercentage()));
                    fgSlobDeviationMainDTO.setStockPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getStockQualityPercentage(), previousMonthFGSlobSummaryDTO.getStockQualityPercentage()));
                } else {
                    fgSlobDeviationMainDTO.setObDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalObsoleteItemValue(), 0.0));
                    fgSlobDeviationMainDTO.setSlDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalSlowItemValue(), 0.0));
                    fgSlobDeviationMainDTO.setSlobDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalSlobValue(), 0.0));
                    fgSlobDeviationMainDTO.setStockDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalStockValue(), 0.0));
                    fgSlobDeviationMainDTO.setSlPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getSlowItemPercentage(), 0.0));
                    fgSlobDeviationMainDTO.setObPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getObsoleteItemPercentage(), 0.0));
                    fgSlobDeviationMainDTO.setStockPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getStockQualityPercentage(), 0.0));
                }
            } else {
                fgSlobDeviationMainDTO.setObDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalObsoleteItemValue(), 0.0));
                fgSlobDeviationMainDTO.setSlDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalSlowItemValue(), 0.0));
                fgSlobDeviationMainDTO.setSlobDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalSlobValue(), 0.0));
                fgSlobDeviationMainDTO.setStockDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getTotalStockValue(), 0.0));
                fgSlobDeviationMainDTO.setSlPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getSlowItemPercentage(), 0.0));
                fgSlobDeviationMainDTO.setObPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getObsoleteItemPercentage(), 0.0));
                fgSlobDeviationMainDTO.setStockPercentageDeviationDTO(fgSlobSummaryUtil.getFGSlobDeviationDTO(currentMonthFGSlobSummaryDTO.getStockQualityPercentage(), 0.0));
            }
        }
        return fgSlobDeviationMainDTO;
    }

    @Override
    public ExcelExportFiles exportFgSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        FGSlobDeviationMainDTO fgSlobDeviationMainDTO = getFGTotalSlobDeviation(rscMtPPheaderDTO.getId());
        FGSlobSummaryDTO fgSlobSummaryDTO = getFgSlobSummary(rscMtPPheaderDTO);
        List<TypeWiseFGSlobDeviationDTO> typeWiseFGSlobDeviationDTO = rscIotFGTypeSlobSummaryService.getDivisionWiseFGSlobDeviation(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(FgSlobSummaryExporter.fgSlobDetailsToExcelList(fgSlobDeviationMainDTO, typeWiseFGSlobDeviationDTO, fgSlobSummaryDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.FG_SLOB_SUMMARY + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public FGSlobSummaryDashboardDTO getFGSlobSummaryDashboardDTO(Long ppHeaderId) {
        return fgSlobSummaryDashboardMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public DivisionFGSlobSummaryIQDashboardDTO getDivisionFGSlobSummaryIQDashboardDTO(Long ppHeaderId, Long fgTypeId) {
        DivisionFGSlobSummaryIQDashboardDTO divisionFGSlobSummaryIQDashboardDTO = new DivisionFGSlobSummaryIQDashboardDTO();
        RscDtFGTypeDTO rscDtFGTypeDTO = rscDtFGTypeService.findById(fgTypeId);
        if (rscDtFGTypeDTO.getType().equals(MaterialCategoryConstants.ALL)) {
            RscIotFGSlobSummaryDTO rscIotFGSlobSummaryDTO = rscIotFGSlobSummaryMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
            divisionFGSlobSummaryIQDashboardDTO.setMaterialType(rscDtFGTypeDTO.getType());
            divisionFGSlobSummaryIQDashboardDTO.setMaterialTypeId(rscDtFGTypeDTO.getId());
            divisionFGSlobSummaryIQDashboardDTO.setIqValue(rscIotFGSlobSummaryDTO.getStockQualityPercentage());
            divisionFGSlobSummaryIQDashboardDTO.setDivisionWiseFGSlobSummaryIQDashboardDTOs(rscIotFGTypeSlobSummaryService.findAllMaterialWiseFGSlobSummaryIQDashboardDTO(ppHeaderId));
        } else {
            RscIotFGTypeSlobSummaryDTO rscIotFGTypeSlobSummaryDTO = rscIotFGTypeSlobSummaryService.findByPPHeaderIdAndTypeId(ppHeaderId, rscDtFGTypeDTO.getId());
            divisionFGSlobSummaryIQDashboardDTO.setMaterialType(rscIotFGTypeSlobSummaryDTO.getFgType());
            divisionFGSlobSummaryIQDashboardDTO.setMaterialTypeId(rscIotFGTypeSlobSummaryDTO.getFgTypeId());
            divisionFGSlobSummaryIQDashboardDTO.setIqValue(rscIotFGTypeSlobSummaryDTO.getStockQualityPercentage());
            divisionFGSlobSummaryIQDashboardDTO.setDivisionWiseFGSlobSummaryIQDashboardDTOs(rscIotFGTypeDivisionSlobSummaryService.findAllDivisionWiseFGSlobSummaryIQDashboardDTO(ppHeaderId, rscDtFGTypeDTO.getId()));
        }
        return divisionFGSlobSummaryIQDashboardDTO;
    }

    @Override
    public RscIotFGSlobSummaryDTO findByRscMtPPheaderId(Long ppHeaderId) {
        return rscIotFGSlobSummaryMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public FGSlobSummaryDashboardDTO findSlobSummaryByRscMtPPheaderId(Long ppHeaderId) {
        return rscIotFGSlobSummaryDashboardMapper.toDto(rscIotFGSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public FGSlobSummaryWithFgTypeAndDivisionDTO getFGSlobSummaryDashboardDTO(Long ppHeaderId, Long fgTypeId, Long divisionId) {
        FGSlobSummaryWithFgTypeAndDivisionDTO fgSlobSummaryWithFgTypeAndDivisionDTO = new FGSlobSummaryWithFgTypeAndDivisionDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            RscDtFGTypeDTO rscDtFGTypeDTO = rscDtFGTypeService.findById(fgTypeId);
            fgSlobSummaryWithFgTypeAndDivisionDTO.setFgTypeId(fgTypeId);
            fgSlobSummaryWithFgTypeAndDivisionDTO.setFgType(rscDtFGTypeDTO.getType());
            if (Optional.ofNullable(divisionId).isPresent()) {
                RscIotFGTypeDivisionSlobSummaryDTO iotFGTypeDivisionSlobSummaryDTO = rscIotFGTypeDivisionSlobSummaryService.findByDivisionId(ppHeaderId, fgTypeId, divisionId);
                if (Optional.ofNullable(iotFGTypeDivisionSlobSummaryDTO).isPresent()) {
                    fgSlobSummaryWithFgTypeAndDivisionDTO.setDivisionId(iotFGTypeDivisionSlobSummaryDTO.getRscDtDivisionId());
                    fgSlobSummaryWithFgTypeAndDivisionDTO.setDivisionName(iotFGTypeDivisionSlobSummaryDTO.getDivisionName());
                    fgSlobSummaryWithFgTypeAndDivisionDTO.setFgSlobSummaryDashboardDTO(rscIotFGTypeDivisionSlobSummaryService.findByFGTypeAndDivisionId(ppHeaderId, fgTypeId, divisionId));
                }
            } else {
                if (rscDtFGTypeDTO.getType().equals(MaterialCategoryConstants.ALL)) {
                    fgSlobSummaryWithFgTypeAndDivisionDTO.setFgSlobSummaryDashboardDTO(findSlobSummaryByRscMtPPheaderId(ppHeaderId));
                } else {
                    fgSlobSummaryWithFgTypeAndDivisionDTO.setFgSlobSummaryDashboardDTO(rscIotFGTypeSlobSummaryService.findSlobSummaryByPPHeaderIdAndTypeId(ppHeaderId, fgTypeId));
                }
            }
        }
        return fgSlobSummaryWithFgTypeAndDivisionDTO;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFGSlobSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}