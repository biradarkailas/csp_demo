package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.ConsumptionPivotMapper;
import com.example.validation.mapper.RMGrossConsumptionMapper;
import com.example.validation.mapper.RmSlobTotalConsumptionDetailsMapper;
import com.example.validation.mapper.RscItRMGrossConsumptionMapper;
import com.example.validation.repository.RscItRMGrossConsumptionRepository;
import com.example.validation.service.MonthService;
import com.example.validation.service.RscItRMGrossConsumptionService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.consumption.ConsumptionUtil;
import com.example.validation.util.excel_export.RmGrossConsumptionExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscItRMGrossConsumptionServiceImpl implements RscItRMGrossConsumptionService {
    private final RscItRMGrossConsumptionRepository rscItRMGrossConsumptionRepository;
    private final RmSlobTotalConsumptionDetailsMapper rmSlobTotalConsumptionDetailsMapper;
    private final RscItRMGrossConsumptionMapper rscItRMGrossConsumptionMapper;
    private final RMGrossConsumptionMapper rmGrossConsumptionMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final ConsumptionUtil consumptionUtil;
    private final MonthService monthService;
    private final ConsumptionPivotMapper consumptionPivotMapper;

    public RscItRMGrossConsumptionServiceImpl(RscItRMGrossConsumptionRepository rscItRMGrossConsumptionRepository, RmSlobTotalConsumptionDetailsMapper rmSlobTotalConsumptionDetailsMapper, RscItRMGrossConsumptionMapper rscItRMGrossConsumptionMapper, RMGrossConsumptionMapper rmGrossConsumptionMapper, RscMtPpheaderService rscMtPpheaderService, ConsumptionUtil consumptionUtil, MonthService monthService, ConsumptionPivotMapper consumptionPivotMapper) {
        this.rscItRMGrossConsumptionRepository = rscItRMGrossConsumptionRepository;
        this.rmSlobTotalConsumptionDetailsMapper = rmSlobTotalConsumptionDetailsMapper;
        this.rscItRMGrossConsumptionMapper = rscItRMGrossConsumptionMapper;
        this.rmGrossConsumptionMapper = rmGrossConsumptionMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.consumptionUtil = consumptionUtil;
        this.monthService = monthService;
        this.consumptionPivotMapper = consumptionPivotMapper;
    }

    @Override
    public void saveAll(List<RscItGrossConsumptionDTO> rmRscItGrossConsumptionDTOs, Long ppHeaderId) {
        if (rscItRMGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscItGrossConsumptionDTO> obj1 = new ArrayList<>();
            List<RscItGrossConsumptionDTO> obj2 = new ArrayList<>();
            for (RscItGrossConsumptionDTO rscItGrossConsumptionDTO : rmRscItGrossConsumptionDTOs) {
                if (isValid(rscItGrossConsumptionDTO)) {
                    obj1.add(rscItGrossConsumptionDTO);
                } else {
                    obj2.add(rscItGrossConsumptionDTO);
                }
            }
            obj1.sort(Comparator.comparingLong(obj -> Long.parseLong(obj.getItemCode())));
            obj2.sort(Comparator.comparing(obj -> obj.getItemCode()));
            obj1.addAll(obj2);
            rscItRMGrossConsumptionRepository.saveAll(rscItRMGrossConsumptionMapper.toEntity(obj1));
        }
    }

    private Boolean isValid(RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        try {
            Long.parseLong(rscItGrossConsumptionDTO.getItemCode());
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItRMGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public GrossConsumptionMainDTO getAllRMGrossConsumption(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        List<GrossConsumptionDTO> grossConsumptionDTOs = rmGrossConsumptionMapper.toDto(rscItRMGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
        return consumptionUtil.getGrossConsumptionMainDTO(optionalRscMtPPHeaderDTO.get(), grossConsumptionDTOs);
    }

    @Override
    public List<GrossConsumptionDTO> getRmGrossConsumption(Long ppHeaderId) {
        return rmGrossConsumptionMapper.toDto(rscItRMGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItGrossConsumptionDTO> getRscItGrossConsumptionDTO(Long ppHeaderId) {
        return rscItRMGrossConsumptionMapper.toDto(rscItRMGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllRMGrossConsumptionByRscMtPPHeaderId(Long ppHeaderId) {
        return rscItRMGrossConsumptionMapper.toDto(rscItRMGrossConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemRscDtItemCategoryDescription(ppHeaderId, MaterialCategoryConstants.RAW_MATERIAL));
    }

    @Override
    public ExcelExportFiles getRmGrossConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmGrossConsumptionExporter.grossConsumptionsToExcel(getRmGrossConsumption(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_GROSS_CONSUMPTION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId) {
        return rscItRMGrossConsumptionMapper.toDto(rscItRMGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, rscMtItemId));
    }

    @Override
    public SlobTotalConsumptionDetailsDTO getRmSlobTotalConsumptionDetailsDTO(Long ppHeaderId, Long rscMtItemId) {
        return rmSlobTotalConsumptionDetailsMapper.toDto(rscItRMGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, rscMtItemId));
    }

    @Override
    public List<ConsumptionPivotDTO> findAllByRscMtPPHeaderId(Long ppHeaderId) {
        return consumptionPivotMapper.toDto(rscItRMGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RscItGrossConsumptionDTO findByGrossConsumptionId(Long grossConsumptionId) {
        return rscItRMGrossConsumptionMapper.toDto(rscItRMGrossConsumptionRepository.findById(grossConsumptionId).get());
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItRMGrossConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}