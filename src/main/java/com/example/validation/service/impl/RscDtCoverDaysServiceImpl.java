package com.example.validation.service.impl;

import com.example.validation.dto.RscDtCoverDaysDTO;
import com.example.validation.mapper.RscDtCoverDaysMapper;
import com.example.validation.repository.RscDtCoverDaysRepository;
import com.example.validation.service.RscDtCoverDaysService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtCoverDaysServiceImpl implements RscDtCoverDaysService {
    private final RscDtCoverDaysRepository rscDtCoverDaysRepository;
    private final RscDtCoverDaysMapper rscDtCoverDaysMapper;

    public RscDtCoverDaysServiceImpl(RscDtCoverDaysRepository rscDtCoverDaysRepository, RscDtCoverDaysMapper rscDtCoverDaysMapper){
        this.rscDtCoverDaysMapper = rscDtCoverDaysMapper;
        this.rscDtCoverDaysRepository = rscDtCoverDaysRepository;
    }

    @Override
    public List<RscDtCoverDaysDTO> findAll() {
        return rscDtCoverDaysMapper.toDto(rscDtCoverDaysRepository.findAll());
    }
}
