package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.RscMtRmOpenPoDTO;
import com.example.validation.dto.supply.OriginalRMPurchasePlanDTO;
import com.example.validation.dto.supply.RMPurchasePlanDTO;
import com.example.validation.dto.supply.RMPurchasePlanMainDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanDTO;
import com.example.validation.mapper.OriginalRMPurchasePlanMapper;
import com.example.validation.mapper.RMPurchasePlanMapper;
import com.example.validation.mapper.RscIotRMLatestPurchasePlanMapper;
import com.example.validation.mapper.RscIotRMOriginalPurchasePlanMapper;
import com.example.validation.repository.RscIotRMLatestPurchasePlanRepository;
import com.example.validation.repository.RscIotRMOriginalPurchasePlanRepository;
import com.example.validation.service.RscIotRMPurchasePlanCalculationsService;
import com.example.validation.service.RscIotRMPurchasePlanService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.service.RscMtRmOpenPoService;
import com.example.validation.util.excel_export.RmPurchasePlanExporter;
import com.example.validation.util.supply.RMPurchasePlanUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class RscIotRMPurchasePlanServiceImpl implements RscIotRMPurchasePlanService {
    private final RscIotRMOriginalPurchasePlanMapper rscIotRMOriginalPurchasePlanMapper;
    private final RscIotRMLatestPurchasePlanMapper rscIotRMLatestPurchasePlanMapper;
    private final RscIotRMOriginalPurchasePlanRepository rscIotRMOriginalPurchasePlanRepository;
    private final RscIotRMLatestPurchasePlanRepository rscIotRMLatestPurchasePlanRepository;
    private final RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService;
    private final RscMtRmOpenPoService rscMtRmOpenPoService;
    private final RMPurchasePlanMapper rmPurchasePlanMapper;
    private final RMPurchasePlanUtil rmPurchasePlanUtil;
    private final OriginalRMPurchasePlanMapper originalRMPurchasePlanMapper;
    private final RscMtPpheaderService rscMtPpheaderService;

    public RscIotRMPurchasePlanServiceImpl(RscIotRMOriginalPurchasePlanRepository rscIotRMOriginalPurchasePlanRepository,
                                           RscIotRMOriginalPurchasePlanMapper rscIotRMOriginalPurchasePlanMapper,
                                           RscIotRMLatestPurchasePlanRepository rscIotRMLatestPurchasePlanRepository,
                                           RscIotRMLatestPurchasePlanMapper rscIotRMLatestPurchasePlanMapper,
                                           RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService,
                                           RscMtRmOpenPoService rscMtRmOpenPoService, RMPurchasePlanMapper rmPurchasePlanMapper, RMPurchasePlanUtil rmPurchasePlanUtil,
                                           OriginalRMPurchasePlanMapper originalRMPurchasePlanMapper, RscMtPpheaderService rscMtPpheaderService) {
        this.rscIotRMOriginalPurchasePlanMapper = rscIotRMOriginalPurchasePlanMapper;
        this.rscIotRMOriginalPurchasePlanRepository = rscIotRMOriginalPurchasePlanRepository;
        this.rscIotRMLatestPurchasePlanRepository = rscIotRMLatestPurchasePlanRepository;
        this.rscIotRMLatestPurchasePlanMapper = rscIotRMLatestPurchasePlanMapper;
        this.rscIotRMPurchasePlanCalculationsService = rscIotRMPurchasePlanCalculationsService;
        this.rscMtRmOpenPoService = rscMtRmOpenPoService;
        this.rmPurchasePlanMapper = rmPurchasePlanMapper;
        this.rmPurchasePlanUtil = rmPurchasePlanUtil;
        this.originalRMPurchasePlanMapper = originalRMPurchasePlanMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
    }

    @Override
    public Boolean createRMPurchasePlan(Long ppHeaderId) {
        if (rscIotRMOriginalPurchasePlanRepository.countByRscMtPPheaderId(ppHeaderId) == 0 && rscIotRMLatestPurchasePlanRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotRMPurchasePlanDTO> updatedRMPurchasePlanCalculationsDTOs = getUpdatedRMPurchasePlanCalculationsDTOs(ppHeaderId, rscIotRMPurchasePlanCalculationsService.findAllByPPHeaderId(ppHeaderId));
            rscIotRMOriginalPurchasePlanRepository.saveAll(rscIotRMOriginalPurchasePlanMapper.toEntity(updatedRMPurchasePlanCalculationsDTOs));
            rscIotRMLatestPurchasePlanRepository.saveAll(rscIotRMLatestPurchasePlanMapper.toEntity(updatedRMPurchasePlanCalculationsDTOs));
        }
        return true;
    }

    @Override
    public RMPurchasePlanMainDTO getRMPurchasePlan(Long ppHeaderId) {
        RMPurchasePlanMainDTO rmPurchasePlanMainDTO = new RMPurchasePlanMainDTO();
        Optional<RscMtPPheaderDTO> optionalRscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPPheaderDTO.isPresent()) {
            List<OriginalRMPurchasePlanDTO> originalRMPurchasePlanDTOs = originalRMPurchasePlanMapper.toDto(rscIotRMOriginalPurchasePlanRepository.findByRscMtPPheaderId(ppHeaderId));
            List<RMPurchasePlanDTO> rmPurchasePlanDTOs = rmPurchasePlanMapper.toDto(rscIotRMLatestPurchasePlanRepository.findByRscMtPPheaderId(ppHeaderId));
            rmPurchasePlanMainDTO.setMpsDate(optionalRscMtPPheaderDTO.get().getMpsDate());
            rmPurchasePlanMainDTO.setMpsName(optionalRscMtPPheaderDTO.get().getMpsName());
            rmPurchasePlanMainDTO.setItemCodes(getItemCodes(originalRMPurchasePlanDTOs, rmPurchasePlanDTOs));
        }
        return rmPurchasePlanMainDTO;
    }

    /*@Override
    public List<RMPurchasePlanDTO> saveAll(List<RMPurchasePlanDTO> rmPurchasePlanDTOs) {
        List<RscIotRMLatestPurchasePlan> rscIotPMLatestPurchasePlans = rscIotRMLatestPurchasePlanRepository.saveAll(rmPurchasePlanMapper.toEntity(rmPurchasePlanDTOs));
        getRmPurchasePlanExport(rmPurchasePlanDTOs.get(0).getMpsId());
        return rmPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlans);
    }*/

    private List<RscIotRMPurchasePlanDTO> getUpdatedRMPurchasePlanCalculationsDTOs(Long mpsId, List<RscIotRMPurchasePlanDTO> rmPurchasePlanCalculationsDTOs) {
        List<RscIotRMPurchasePlanDTO> updatedRMPurchasePlanCalculationsDTOs = new ArrayList<>();
        if (Optional.ofNullable(rmPurchasePlanCalculationsDTOs).isPresent()) {
            for (RscIotRMPurchasePlanDTO rmPurchasePlanCalculationsDTO : rmPurchasePlanCalculationsDTOs) {
                List<RscMtRmOpenPoDTO> rscMtRmOpenPoDTOs = rscMtRmOpenPoService.findAllByRscMtItemIdAndMtSupplierId(mpsId, rmPurchasePlanCalculationsDTO.getRscMtItemId(), rmPurchasePlanCalculationsDTO.getRscMtSupplierId());
                if (Optional.ofNullable(rscMtRmOpenPoDTOs).isPresent())
                    rmPurchasePlanCalculationsDTO.setPoRemark(rmPurchasePlanUtil.getPORemarkValue(rscMtRmOpenPoDTOs));
                rmPurchasePlanCalculationsDTO.setDescription(getDescription(rmPurchasePlanCalculationsDTO));
                updatedRMPurchasePlanCalculationsDTOs.add(rmPurchasePlanCalculationsDTO);
            }
        }
        return updatedRMPurchasePlanCalculationsDTOs;
    }

    private String getDescription(RscIotRMPurchasePlanDTO rmPurchasePlanCalculationsDTO) {
        if (Optional.ofNullable(rmPurchasePlanCalculationsDTO.getContainerType()).isPresent())
            if (!rmPurchasePlanCalculationsDTO.getContainerType().equals(MaterialCategoryConstants.NORMAL_CONTAINER))
                return rmPurchasePlanCalculationsDTO.getItemDescription() + " - " + rmPurchasePlanCalculationsDTO.getContainerType();
        return rmPurchasePlanCalculationsDTO.getItemDescription();
    }

    private List<RMPurchasePlanDTO> getItemCodes(List<OriginalRMPurchasePlanDTO> originalRMPurchasePlanDTOs, List<RMPurchasePlanDTO> rmPurchasePlanDTOs) {
        List<RMPurchasePlanDTO> rmPurchasePlanDTOList = new ArrayList<>();
        Map<Long, OriginalRMPurchasePlanDTO> rmPurchasePlanDTOMap = getOriginalPurchasePlanMap(originalRMPurchasePlanDTOs);
        for (RMPurchasePlanDTO rmPurchasePlanDTO : rmPurchasePlanDTOs) {
            rmPurchasePlanDTO.setOriginalRMPurchasePlan(rmPurchasePlanDTOMap.get(rmPurchasePlanDTO.getMaterialId()));
            rmPurchasePlanDTOList.add(rmPurchasePlanDTO);
        }
        return rmPurchasePlanDTOList;
    }

    private Map<Long, OriginalRMPurchasePlanDTO> getOriginalPurchasePlanMap(List<OriginalRMPurchasePlanDTO> originalRMPurchasePlanDTOs) {
        Map<Long, OriginalRMPurchasePlanDTO> rmPurchasePlanDTOMap = new HashMap<>();
        for (OriginalRMPurchasePlanDTO originalRMPurchasePlanDTO : originalRMPurchasePlanDTOs) {
            rmPurchasePlanDTOMap.put(originalRMPurchasePlanDTO.getMaterialId(), originalRMPurchasePlanDTO);
        }
        return rmPurchasePlanDTOMap;
    }

    @Override
    public ExcelExportFiles getRmPurchasePlanExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmPurchasePlanExporter.purchasePlanToExcel(getRMPurchasePlan(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_PURCHASE_PLAN + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotRMLatestPurchasePlanRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
        rscIotRMOriginalPurchasePlanRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}