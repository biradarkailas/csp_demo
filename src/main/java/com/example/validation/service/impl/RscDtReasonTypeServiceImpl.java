package com.example.validation.service.impl;

import com.example.validation.dto.RscDtReasonTypeDTO;
import com.example.validation.mapper.RscDtReasonTypeMapper;
import com.example.validation.repository.RscDtReasonTypeRepository;
import com.example.validation.service.RscDtReasonTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtReasonTypeServiceImpl implements RscDtReasonTypeService {
    private final RscDtReasonTypeRepository rscDtReasonTypeRepository;
    private final RscDtReasonTypeMapper rscDtReasonTypeMapper;

    public RscDtReasonTypeServiceImpl(RscDtReasonTypeRepository rscDtReasonTypeRepository, RscDtReasonTypeMapper rscDtReasonTypeMapper){
        this.rscDtReasonTypeRepository = rscDtReasonTypeRepository;
        this.rscDtReasonTypeMapper = rscDtReasonTypeMapper;
    }

    @Override
    public List<RscDtReasonTypeDTO> findAll() { return rscDtReasonTypeMapper.toDto(rscDtReasonTypeRepository.findAll()); }
}
