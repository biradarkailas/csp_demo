package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.mapper.ConsumptionItemDetailsMapper;
import com.example.validation.mapper.RscItRMPivotConsumptionMapper;
import com.example.validation.repository.RscItRMPivotConsumptionRepository;
import com.example.validation.service.RscItRMGrossConsumptionService;
import com.example.validation.service.RscItRMLogicalConsumptionService;
import com.example.validation.service.RscItRMPivotConsumptionService;
import com.example.validation.service.RscMtBomService;
import com.example.validation.util.excel_export.RmPivotConsumptionExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class RscItRMPivotConsumptionServiceImpl implements RscItRMPivotConsumptionService {
    private final RscItRMPivotConsumptionRepository rscItRMPivotConsumptionRepository;
    private final RscItRMPivotConsumptionMapper rscItRMPivotConsumptionMapper;
    private final RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService;
    private final ConsumptionItemDetailsMapper consumptionItemDetailsMapper;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RscMtBomService rscMtBomService;

    public RscItRMPivotConsumptionServiceImpl(RscItRMPivotConsumptionRepository rscItRMPivotConsumptionRepository, RscItRMPivotConsumptionMapper rscItRMPivotConsumptionMapper, RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService, ConsumptionItemDetailsMapper consumptionItemDetailsMapper, RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RscMtBomService rscMtBomService) {
        this.rscItRMPivotConsumptionRepository = rscItRMPivotConsumptionRepository;
        this.rscItRMPivotConsumptionMapper = rscItRMPivotConsumptionMapper;
        this.rscItRMLogicalConsumptionService = rscItRMLogicalConsumptionService;
        this.consumptionItemDetailsMapper = consumptionItemDetailsMapper;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rscMtBomService = rscMtBomService;
    }

    @Override
    public void createRMPivotConsumption(Long ppHeaderId) {
        if (rscItRMPivotConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscItRMPivotConsumptionDTO> rscItRMPivotConsumptionDTOs = new ArrayList<>();
            for (LogicalConsumptionMainDTO logicalConsumptionMainDTO : rscItRMLogicalConsumptionService.getRMLogicalConsumption(ppHeaderId)) {
                rscItRMPivotConsumptionDTOs.add(getRscItRMPivotConsumptionDTO(logicalConsumptionMainDTO, ppHeaderId));
            }
            rscItRMPivotConsumptionRepository.saveAll(rscItRMPivotConsumptionMapper.toEntity(rscItRMPivotConsumptionDTOs));
        }
    }

    private RscItRMPivotConsumptionDTO getRscItRMPivotConsumptionDTO(LogicalConsumptionMainDTO logicalConsumptionMainDTO, Long ppHeaderId) {
        RscItRMPivotConsumptionDTO rscItRMPivotConsumptionDTO = new RscItRMPivotConsumptionDTO();
        rscItRMPivotConsumptionDTO.setM1Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth1().getValue());
        rscItRMPivotConsumptionDTO.setM2Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth2().getValue());
        rscItRMPivotConsumptionDTO.setM3Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth3().getValue());
        rscItRMPivotConsumptionDTO.setM4Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth4().getValue());
        rscItRMPivotConsumptionDTO.setM5Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth5().getValue());
        rscItRMPivotConsumptionDTO.setM6Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth6().getValue());
        rscItRMPivotConsumptionDTO.setM7Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth7().getValue());
        rscItRMPivotConsumptionDTO.setM8Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth8().getValue());
        rscItRMPivotConsumptionDTO.setM9Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth9().getValue());
        rscItRMPivotConsumptionDTO.setM10Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth10().getValue());
        rscItRMPivotConsumptionDTO.setM11Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth11().getValue());
        rscItRMPivotConsumptionDTO.setM12Value(logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth12().getValue());
        rscItRMPivotConsumptionDTO.setTotalValue(logicalConsumptionMainDTO.getTotalValue());
        rscItRMPivotConsumptionDTO.setRscMtItemChildId(logicalConsumptionMainDTO.getChildId());
        rscItRMPivotConsumptionDTO.setRscMtItemParentId(logicalConsumptionMainDTO.getParentId());
        rscItRMPivotConsumptionDTO.setRscMtPPheaderId(ppHeaderId);
        rscItRMPivotConsumptionDTO.setFgItemDetails(getSubString(getFGItemDetails(logicalConsumptionMainDTO.getParentId(), ppHeaderId)));
        return rscItRMPivotConsumptionDTO;
    }

    private String getFGItemDetails(Long itemId, Long ppHeaderId) {
        StringBuilder result = new StringBuilder();
        for (RscMtBomDTO rscMtBomDTO : rscMtBomService.findAllByRscMtItemChildId(itemId, ppHeaderId)) {
            switch (rscMtBomDTO.getParentItemCategory()) {
                case MaterialCategoryConstants.FG_MATERIAL:
                    result.append(getFGName(rscMtBomDTO));
                    break;
                case MaterialCategoryConstants.WIP_MATERIAL:
                    List<RscMtBomDTO> rscMtBomDTOList = rscMtBomService.findAllByRscMtItemChildId(rscMtBomDTO.getRscMtItemParentId(), rscMtBomDTO.getMpsId());
                    if (rscMtBomDTOList.size() == 0) {
                        result.append(getFGName(rscMtBomDTO));
                    } else {
                        result.append(getFGItemDetails(rscMtBomDTO.getRscMtItemParentId(), rscMtBomDTO.getMpsId()));
                    }
                    break;
                default:
                    result.append(getFGItemDetails(rscMtBomDTO.getRscMtItemParentId(), rscMtBomDTO.getMpsId()));
                    break;
            }
        }
        return result.toString();
    }

    private String getSubString(String result) {
        if (result.length() > 2) {
            return result.substring(0, result.length() - 2);
        }
        return result;
    }

    private String getFGName(RscMtBomDTO rscMtBomDTO) {
        return rscMtBomDTO.getParentItemCode() + "-" + rscMtBomDTO.getParentItemDescription() + ", ";
    }

    @Override
    public ConsumptionPivotMainDTO getRMPivotConsumption(Long ppHeaderId) {
        ConsumptionPivotMainDTO consumptionPivotMainDTO = new ConsumptionPivotMainDTO();
        Map<Long, List<ConsumptionItemDetailsDTO>> consumptionItemDetailsDTOMap = getConsumptionItemDetailsDTOMap(consumptionItemDetailsMapper.toDto(rscItRMPivotConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        List<ConsumptionPivotDTO> consumptionPivotDTOs = new ArrayList<>();
        for (ConsumptionPivotDTO consumptionPivotDTO : rscItRMGrossConsumptionService.findAllByRscMtPPHeaderId(ppHeaderId)) {
            consumptionPivotDTO.setConsumptionItemDetailsDTOs(consumptionItemDetailsDTOMap.get(consumptionPivotDTO.getChildItemId()));
            consumptionPivotDTOs.add(consumptionPivotDTO);
        }
        consumptionPivotMainDTO.setMpsId(ppHeaderId);
        consumptionPivotMainDTO.setConsumptionPivotDTOs(consumptionPivotDTOs);
        return consumptionPivotMainDTO;
    }

    public ExcelExportFiles getRmPivotConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmPivotConsumptionExporter.rmPivotConsumptionToExcel(getRMPivotConsumption(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.Rm_Pivot_Consumption + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    private Map<Long, List<ConsumptionItemDetailsDTO>> getConsumptionItemDetailsDTOMap(List<ConsumptionItemDetailsDTO> consumptionItemDetailsDTOs) {
        Map<Long, List<ConsumptionItemDetailsDTO>> consumptionItemDetailsDTOMap = new HashMap<>();
        for (ConsumptionItemDetailsDTO consumptionItemDetailsDTO : consumptionItemDetailsDTOs) {
            List<ConsumptionItemDetailsDTO> consumptionItemDetailsDTOList = new ArrayList<>();
            if (!consumptionItemDetailsDTOMap.isEmpty()) {
                if (consumptionItemDetailsDTOMap.containsKey(consumptionItemDetailsDTO.getChildItemId())) {
                    consumptionItemDetailsDTOList.addAll(consumptionItemDetailsDTOMap.get(consumptionItemDetailsDTO.getChildItemId()));
                    consumptionItemDetailsDTOList.add(consumptionItemDetailsDTO);
                } else {
                    consumptionItemDetailsDTOList.add(consumptionItemDetailsDTO);
                }
            } else {
                consumptionItemDetailsDTOList.add(consumptionItemDetailsDTO);
            }
            consumptionItemDetailsDTOMap.put(consumptionItemDetailsDTO.getChildItemId(), consumptionItemDetailsDTOList);
        }
        return consumptionItemDetailsDTOMap;
    }

    @Override
    public List<FGDetailsDTO> getFGDetails(Long ppHeaderId, Long itemId) {
        return getFGDetailsDTOs(ppHeaderId, itemId);
    }

    private List<FGDetailsDTO> getFGDetailsDTOs(Long ppHeaderId, Long itemId) {
        List<FGDetailsDTO> fgDetailsDTOs = new ArrayList<>();
        for (RscMtBomDTO rscMtBomDTO : rscMtBomService.findAllByRscMtItemChildId(itemId, ppHeaderId)) {
            switch (rscMtBomDTO.getParentItemCategory()) {
                case MaterialCategoryConstants.FG_MATERIAL:
                    fgDetailsDTOs.add(getFGDetailsDTO(rscMtBomDTO));
                    break;
                case MaterialCategoryConstants.WIP_MATERIAL:
                    List<RscMtBomDTO> rscMtBomDTOList = rscMtBomService.findAllByRscMtItemChildId(rscMtBomDTO.getRscMtItemParentId(), rscMtBomDTO.getMpsId());
                    if (rscMtBomDTOList.size() == 0) {
                        fgDetailsDTOs.add(getFGDetailsDTO(rscMtBomDTO));
                    } else {
                        fgDetailsDTOs.addAll(getFGDetailsDTOs(rscMtBomDTO.getMpsId(), rscMtBomDTO.getRscMtItemParentId()));
                    }
                    break;
                default:
                    fgDetailsDTOs.addAll(getFGDetailsDTOs(rscMtBomDTO.getMpsId(), rscMtBomDTO.getRscMtItemParentId()));
                    break;
            }
        }
        return fgDetailsDTOs;
    }

    private FGDetailsDTO getFGDetailsDTO(RscMtBomDTO rscMtBomDTO) {
        FGDetailsDTO fgDetailsDTO = new FGDetailsDTO();
        fgDetailsDTO.setFgCode(rscMtBomDTO.getParentItemCode());
        fgDetailsDTO.setFgDescription(rscMtBomDTO.getParentItemDescription());
        return fgDetailsDTO;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItRMPivotConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}