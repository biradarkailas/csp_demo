package com.example.validation.service.impl;

import com.example.validation.dto.RscDtItemCodeDTO;
import com.example.validation.mapper.RscDtItemCodeMapper;
import com.example.validation.repository.RscDtItemCodeRepository;
import com.example.validation.service.RscDtItemCodeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtItemCodeServiceImpl implements RscDtItemCodeService {
    private final RscDtItemCodeRepository rscDtItemCodeRepository;
    private final RscDtItemCodeMapper rscDtItemCodeMapper;

    public RscDtItemCodeServiceImpl(RscDtItemCodeRepository rscDtItemCodeRepository, RscDtItemCodeMapper rscDtItemCodeMapper) {
        this.rscDtItemCodeRepository = rscDtItemCodeRepository;
        this.rscDtItemCodeMapper = rscDtItemCodeMapper;
    }

    @Override
    public List<RscDtItemCodeDTO> findAll() {
        return rscDtItemCodeMapper.toDto(rscDtItemCodeRepository.findAll());
    }
}
