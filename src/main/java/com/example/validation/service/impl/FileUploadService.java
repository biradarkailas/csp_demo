package com.example.validation.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class FileUploadService {

    public String tempPath = System.getProperty("os.name").indexOf("Win") >= 0 ? "D:\\git_repos\\TEMP\\" : "/home/ubuntu/lorealTemp/";

    public String slash = System.getProperty("os.name").indexOf("Win") >= 0 ? "\\" : "/";

    public int store(MultipartFile file, String tempFolderName, HttpSession session) {

        File filen = new File(tempPath + tempFolderName);

        filen.mkdir();

        Path rootLocation = Paths.get(tempPath + tempFolderName);

        System.out.println("rootLocation  ==  " + rootLocation);

        String nameExtension[] = file.getContentType().split("/");

        String name = file.getOriginalFilename();

        System.out.println("file  :: " + name);

        if (file != null) {
            try {
                Files.copy(file.getInputStream(), rootLocation.resolve(name));
                return 1;
            } catch (Exception exception) {
                System.out.println("error while uploading image catch:: " + exception.getMessage());
                return -5;
            }

        } else {
            System.out.println("Error while uploading image when image is already Exists :: ");
            return -5;
        }
    }

    public void deleteNonEmptyDir(File dir) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                deleteNonEmptyDir(new File(dir, children[i]));
            }
        }
        dir.delete();
    }

    public Integer uploadLicence(MultipartFile file, HttpSession session) {
        deleteNonEmptyDir(new File("LICENCE"));
        File filen = new File("LICENCE");
        filen.mkdir();
        Path rootLocation = Paths.get("LICENCE");
        System.out.println("rootLocation  ==  " + rootLocation);
        String nameExtension[] = file.getContentType().split("/");
        String name = file.getOriginalFilename();
        if (file != null) {
            try {
                Files.copy(file.getInputStream(), rootLocation.resolve(name));
                return 1;
            } catch (Exception exception) {
                System.out.println("error while uploading image catch:: " + exception.getMessage());
                return -5;
            }

        } else {
            System.out.println("Error while uploading image when image is already Exists :: ");
            return -5;
        }
    }
}
