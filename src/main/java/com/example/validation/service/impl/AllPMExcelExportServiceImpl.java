package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AllPMExcelExportServiceImpl implements AllPMExcelExportService {
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final StockService stockService;
    private final RscIotPMPurchasePlanService rscIotPMPurchasePlanService;
    private final RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService;
    private final RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService;
    private final RscItPMSlobService rscItPMSlobService;
    private final RscIotPmCoverDaysService rscIotPmCoverDaysService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscDtMouldService rscDtMouldService;
    private final RscIotPMPurchasePlanCoverDaysService rscIotPMPurchasePlanCoverDaysService;
    private final MaterialMastersService materialMastersService;
    private final SupplierMastersService supplierMastersService;
    private final MaterialCategoryMasterService materialCategoryMasterService;
    private final MonthService monthService;

    public AllPMExcelExportServiceImpl(RscItPMGrossConsumptionService rscItPMGrossConsumptionService, StockService stockService, RscIotPMPurchasePlanService rscIotPMPurchasePlanService, RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService, RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService, RscItPMSlobService rscItPMSlobService, RscIotPmCoverDaysService rscIotPmCoverDaysService, RscMtPpheaderService rscMtPpheaderService, RscDtMouldService rscDtMouldService, RscIotPMPurchasePlanCoverDaysService rscIotPMPurchasePlanCoverDaysService, MaterialMastersService materialMastersService, SupplierMastersService supplierMastersService, MaterialCategoryMasterService materialCategoryMasterService, MonthService monthService) {
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.stockService = stockService;
        this.rscIotPMPurchasePlanService = rscIotPMPurchasePlanService;
        this.rscIotLatestTotalMouldSaturationService = rscIotLatestTotalMouldSaturationService;
        this.rscIotLatestMouldSaturationService = rscIotLatestMouldSaturationService;
        this.rscItPMSlobService = rscItPMSlobService;
        this.rscIotPmCoverDaysService = rscIotPmCoverDaysService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscDtMouldService = rscDtMouldService;
        this.rscIotPMPurchasePlanCoverDaysService = rscIotPMPurchasePlanCoverDaysService;
        this.materialMastersService = materialMastersService;
        this.supplierMastersService = supplierMastersService;
        this.materialCategoryMasterService = materialCategoryMasterService;
        this.monthService = monthService;
    }


    @Override
    public String createAllPMExports(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            String year = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() );
            String month = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() );
            String date = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() );
            String name = optionalRscMtPpHeaderDTO.get().getMpsName();
            createDirectory( year, month, date, name );
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails( optionalRscMtPpHeaderDTO.get().getMpsDate() );
            savePmExport( optionalRscMtPpHeaderDTO.get(), year, month, date, monthDetailDTO );
        }
        return "Downloaded All Exports Successfully";
    }

    private void savePmExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> pmExportList = new ArrayList<>();
        pmExportList.add( rscItPMGrossConsumptionService.getPmGrossConsumptionExport( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( stockService.exportMesStock( rscMtPPheaderDTO ) );
        pmExportList.add( stockService.exportMesStockRejection( rscMtPPheaderDTO ) );
        pmExportList.add( stockService.exportRtdStock( rscMtPPheaderDTO ) );
        pmExportList.add( rscIotPMPurchasePlanService.purchasePlanToExcel( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( rscIotLatestTotalMouldSaturationService.mouldWiseSaturationToExcel( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( rscIotLatestMouldSaturationService.getSupplierWiseMouldExport( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( rscItPMSlobService.exportPmSlobExporter( rscMtPPheaderDTO ) );
        pmExportList.add( rscIotPmCoverDaysService.exportPmSlobExporter( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( materialCategoryMasterService.pmMasterDataMouldExport( rscMtPPheaderDTO ) );
        pmExportList.add( rscItPMSlobService.exportPmSlobSummary( rscMtPPheaderDTO ) );
        pmExportList.add( rscIotPMPurchasePlanCoverDaysService.pmPurchaseCoverdaysExporte( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( rscIotPMPurchasePlanCoverDaysService.pmPurchaseStockEquationExporte( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( rscItPMSlobService.pmCoverDaysSummaryExporter( rscMtPPheaderDTO, monthDetailDTO ) );
        pmExportList.add( rscDtMouldService.pmMasterDataMouldExport( rscMtPPheaderDTO ) );
        pmExportList.add( materialMastersService.pmMasterDataMouldExport( rscMtPPheaderDTO ) );
        pmExportList.add( supplierMastersService.pmSuppliersMasterDataExport( rscMtPPheaderDTO ) );
        createExcelFiles( rscMtPPheaderDTO, year, month, date, pmExportList, ExcelFileNameConstants.PM_Directory );
    }

    private void createExcelFiles(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, List<ExcelExportFiles> fgExportList, String moduleType) {
        for (ExcelExportFiles excelExportFiles : fgExportList) {
            try {
                FileOutputStream fileOut = new FileOutputStream( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + rscMtPPheaderDTO.getMpsName() + "/" + moduleType + excelExportFiles.getFileName() );
                fileOut.write( excelExportFiles.getFile().readAllBytes() );
            } catch (IOException e) {
                throw new RuntimeException( "Fail to import data to Excel file: " + e.getMessage() );
            }
        }
    }

    @Override
    public void createDirectory(String year, String month, String date, String name) {
        try {
            Path pmPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.PM );
            Files.createDirectories( pmPath );
        } catch (IOException e) {
            System.err.println( "Failed to create directory!" + e.getMessage() );
        }
    }
}