package com.example.validation.service.impl;

import com.example.validation.dto.RscDtDivisionDTO;
import com.example.validation.mapper.RscDtDivisionMapper;
import com.example.validation.repository.RscDtDivisionRepository;
import com.example.validation.service.RscDtDivisionService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtDivisionServiceImpl implements RscDtDivisionService {
    private final RscDtDivisionRepository rscDtDivisionRepository;
    private final RscDtDivisionMapper rscDtDivisionMapper;

    public RscDtDivisionServiceImpl(RscDtDivisionRepository rscDtDivisionRepository, RscDtDivisionMapper rscDtDivisionMapper){
        this.rscDtDivisionRepository = rscDtDivisionRepository;
        this.rscDtDivisionMapper = rscDtDivisionMapper;
    }

    @Override
    public List<RscDtDivisionDTO> findAll() {
        return rscDtDivisionMapper.toDto(rscDtDivisionRepository.findAll());
    }
}
