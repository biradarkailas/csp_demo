package com.example.validation.service.impl;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyTypeUser;
import com.example.validation.repository.PartyRepo;
import com.example.validation.repository.PartyTypeUserRepo;
import com.example.validation.util.enums.PartyType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PartyService {
    @Autowired
    PartyRepo partyRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    PartyTypeUserRepo partyTypeUserRepo;

    public Party save(Party party) {
        if (party.getPartyTypeUser() != null) {
            if (party.getPartyTypeUser().getId() != null) {
                var userFromDb = partyTypeUserRepo.findById(party.getPartyTypeUser().getId());
                if (userFromDb.isPresent()) {
                    if (userFromDb.get().getPassword().equals(party.getPartyTypeUser().getPassword())) {
//                        party.getPartyTypeUser().setPassword(userFromDb.get().getPassword());
                        party.getPartyTypeUser().setPassword(passwordEncoder.encode(party.getPartyTypeUser().getPassword()));
                    } else {
                        party.getPartyTypeUser().setPassword(passwordEncoder.encode(party.getPartyTypeUser().getPassword()));
                    }
                }
            } else {
                party.getPartyTypeUser().setPassword(passwordEncoder.encode(party.getPartyTypeUser().getPassword()));
            }
        }
        return partyRepo.save(party);
    }

    public List<Party> findAllByType(PartyType partyType) {
        return partyRepo.findAllByType(partyType);
    }

    public Optional<Party> findById(Long id) {
        return partyRepo.findById(id);
    }

    public Party findByPartyTypeUser(PartyTypeUser partyTypeUser) {
        return partyRepo.findByPartyTypeUser(partyTypeUser);
    }
}
