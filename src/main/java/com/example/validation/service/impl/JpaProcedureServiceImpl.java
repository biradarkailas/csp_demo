package com.example.validation.service.impl;

import com.example.validation.service.ErrorLogService;
import com.example.validation.util.ResourceProcedureUtils;
import com.example.validation.util.ValidationProcedureUtils;
import org.springframework.stereotype.Service;

/**
 * @author Kailas Biradar
 */
@Service
public class JpaProcedureServiceImpl {
    private final ErrorLogService errorLogService;
    private final ValidationProcedureUtils validationProcedureUtils;
    private final ResourceProcedureUtils resourceProcedureUtils;

    public JpaProcedureServiceImpl(ErrorLogService errorLogService, ValidationProcedureUtils validationProcedureUtils, ResourceProcedureUtils resourceProcedureUtils) {
        this.errorLogService = errorLogService;
        this.validationProcedureUtils = validationProcedureUtils;
        this.resourceProcedureUtils = resourceProcedureUtils;
    }

    boolean dataSanitize(String tableName) {
        return errorLogService.dataSanitization(tableName);
    }

    boolean callProcedureByTableName(String tableName, boolean isValidationProcedure) {
        switch (tableName) {
            case "stg_lines":
                if (isValidationProcedure)
                    return validationProcedureUtils.linesValidation();
                else
                    return resourceProcedureUtils.srcPostLinesData();
            case "stg_skids":
                if (isValidationProcedure)
                    return validationProcedureUtils.skidsValidation();
                else
                    return resourceProcedureUtils.srcPostSkidsData();
            case "stg_item":
                if (isValidationProcedure)
                    return validationProcedureUtils.itemValidation();
                else
                    return resourceProcedureUtils.srcPostItemData();
            case "stg_working_days":
                if (isValidationProcedure)
                    return validationProcedureUtils.workingDaysDataValidation();
                else
                    return resourceProcedureUtils.srcPostWorkingDays();
            case "stg_mps":
                if (isValidationProcedure)
                    return validationProcedureUtils.mpsValidation();
                else
                    return resourceProcedureUtils.srcPostMps();
            case "stg_mould_master":
                if (isValidationProcedure)
                    return validationProcedureUtils.mouldMasterValidation();
                else
                    return resourceProcedureUtils.srcPostMouldData();
            case "stg_pm_material_master":
                if (isValidationProcedure)
                    return validationProcedureUtils.pmMaterialMasterValidation();
                else
                    return resourceProcedureUtils.srcPostPMMaterial();
            case "stg_rm_material_master":
                if (isValidationProcedure)
                    return validationProcedureUtils.rmMaterialMasterValidation();
                else
                    return resourceProcedureUtils.srcPostRMMaterial();
            case "stg_bill_of_material":
                if (isValidationProcedure)
                    return validationProcedureUtils.bomValidation();
                else
                    return resourceProcedureUtils.srcPostBOM();
            case "stg_std_price":
                if (isValidationProcedure)
                    return validationProcedureUtils.stdPriceValidation();
                else
                    return resourceProcedureUtils.srcPostStdPriceData();
            case "stg_technical_size":
                if (isValidationProcedure)
                    return validationProcedureUtils.technicalSizeValidation();
                else
                    return resourceProcedureUtils.srcPostItemTechnicalSize();
            case "stg_mid_night_stock":
                if (isValidationProcedure)
                    return validationProcedureUtils.midNightStockValidation();
                else
                    return resourceProcedureUtils.srcPostMidNightStock();
            case "stg_receipts_till_date":
                if (isValidationProcedure)
                    return validationProcedureUtils.receiptsTillDateValidation();
                else
                    return resourceProcedureUtils.srcPostReceipt();
            case "stg_open_po":
                if (isValidationProcedure)
                    return validationProcedureUtils.openPoValidation();
                else
                    return resourceProcedureUtils.srcPostOpenPOData();
            case "stg_in_std":
                if (isValidationProcedure)
                    return validationProcedureUtils.inStdValidation();
                else
                    return resourceProcedureUtils.srcPostMESRejInStd();
            case "stg_us_listgen":
                if (isValidationProcedure)
                    return validationProcedureUtils.usListGenValidation();
                else
                    return resourceProcedureUtils.srcPostRMListGen();
            case "stg_other_location_stock":
                if (isValidationProcedure)
                    return validationProcedureUtils.otherLocationStockValidation();
                else
                    return resourceProcedureUtils.srcPostOtherLocationStock();
            case "stg_physical_rejection":
                if (isValidationProcedure)
                    return validationProcedureUtils.physicalRejectionValidation();
                else
                    return resourceProcedureUtils.srcPostPhysicalRejection();
            case "stg_item2":
                if (isValidationProcedure)
                    return validationProcedureUtils.item2Validation();
                else
                    return resourceProcedureUtils.srcPostItem2();
            case "stg_forecast_mps":
                if (isValidationProcedure)
                    return validationProcedureUtils.forecastMpsValidation();
                else
                    return resourceProcedureUtils.srcPostForecastMPS();
            case "stg_map":
                if (isValidationProcedure)
                    return validationProcedureUtils.mapValidation();
                else
                    return resourceProcedureUtils.srcPostMAP();
            case "stg_otif_pm_rtd":
                if (isValidationProcedure)
                    return validationProcedureUtils.pmOtifValidation();
                else
                    return resourceProcedureUtils.srcPostPMOtif();
            case "stg_item_2":
                if (isValidationProcedure)
                    return validationProcedureUtils.item2DataValidation();
                else
                    return resourceProcedureUtils.srcPostItem2Data();
            default:
                return false;
        }
    }

    void truncateTable(String tableName) {
        errorLogService.truncateTable(tableName);
    }
}