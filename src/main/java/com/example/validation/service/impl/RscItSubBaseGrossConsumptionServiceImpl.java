package com.example.validation.service.impl;

import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.RscItSubBaseGrossConsumptionMapper;
import com.example.validation.mapper.SubBaseGrossConsumptionMapper;
import com.example.validation.repository.RscItSubBaseGrossConsumptionRepository;
import com.example.validation.service.RscItSubBaseGrossConsumptionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscItSubBaseGrossConsumptionServiceImpl implements RscItSubBaseGrossConsumptionService {
    private final RscItSubBaseGrossConsumptionRepository rscItSubBaseGrossConsumptionRepository;
    private final RscItSubBaseGrossConsumptionMapper rscItSubBaseGrossConsumptionMapper;
    private final SubBaseGrossConsumptionMapper subBaseGrossConsumptionMapper;

    public RscItSubBaseGrossConsumptionServiceImpl(RscItSubBaseGrossConsumptionRepository rscItSubBaseGrossConsumptionRepository, RscItSubBaseGrossConsumptionMapper rscItSubBaseGrossConsumptionMapper, SubBaseGrossConsumptionMapper subBaseGrossConsumptionMapper) {
        this.rscItSubBaseGrossConsumptionRepository = rscItSubBaseGrossConsumptionRepository;
        this.rscItSubBaseGrossConsumptionMapper = rscItSubBaseGrossConsumptionMapper;
        this.subBaseGrossConsumptionMapper = subBaseGrossConsumptionMapper;
    }

    @Override
    public void saveAll(List<RscItGrossConsumptionDTO> subBaseRscItGrossConsumptionDTOs, Long ppHeaderId) {
        if (rscItSubBaseGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            subBaseRscItGrossConsumptionDTOs.sort(Comparator.comparing(RscItGrossConsumptionDTO::getItemCode));
            rscItSubBaseGrossConsumptionRepository.saveAll(rscItSubBaseGrossConsumptionMapper.toEntity(subBaseRscItGrossConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItSubBaseGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId) {
        return rscItSubBaseGrossConsumptionMapper.toDto(rscItSubBaseGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId) {
        return rscItSubBaseGrossConsumptionMapper.toDto(rscItSubBaseGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, bpItemId));
    }

    @Override
    public List<Long> findAllItemIdByRscMtPPHeaderId(Long ppHeaderId) {
        List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs = rscItSubBaseGrossConsumptionMapper.toDto(rscItSubBaseGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
        return rscItGrossConsumptionDTOs.stream().map(RscItGrossConsumptionDTO::getRscMtItemId).collect(Collectors.toList());
    }

    @Override
    public List<GrossConsumptionDTO> getAllSubBaseGrossConsumption(Long ppHeaderId) {
        return subBaseGrossConsumptionMapper.toDto(rscItSubBaseGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItSubBaseGrossConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
