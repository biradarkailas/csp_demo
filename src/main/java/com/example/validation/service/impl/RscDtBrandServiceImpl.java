package com.example.validation.service.impl;

import com.example.validation.dto.RscDtBrandDTO;
import com.example.validation.mapper.RscDtBrandMapper;
import com.example.validation.repository.RscDtBrandRepository;
import com.example.validation.service.RscDtBrandService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtBrandServiceImpl implements RscDtBrandService {
    private final RscDtBrandRepository rscDtBrandRepository;
    private final RscDtBrandMapper rscDtBrandMapper;

    public RscDtBrandServiceImpl(RscDtBrandRepository rscDtBrandRepository, RscDtBrandMapper rscDtBrandMapper) {
        this.rscDtBrandRepository = rscDtBrandRepository;
        this.rscDtBrandMapper = rscDtBrandMapper;
    }

    @Override
    public List<RscDtBrandDTO> findAll() {
        return rscDtBrandMapper.toDto(rscDtBrandRepository.findAll());
    }
}


