package com.example.validation.service.impl;

import com.example.validation.dto.RscIotFGStockDTO;
import com.example.validation.mapper.RscIotFGStockMapper;
import com.example.validation.repository.RscIotFGStockRepository;
import com.example.validation.service.RscIotFGStockService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.DateUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotFGStockServiceImpl implements RscIotFGStockService {
    private final RscIotFGStockRepository rscIotFGStockRepository;
    private final RscIotFGStockMapper rscIotFGStockMapper;
    private final RscMtPpheaderService rscMtPPHeaderService;

    public RscIotFGStockServiceImpl(RscIotFGStockRepository rscIotFGStockRepository, RscIotFGStockMapper rscIotFGStockMapper, RscMtPpheaderService rscMtPPHeaderService) {
        this.rscIotFGStockRepository = rscIotFGStockRepository;
        this.rscIotFGStockMapper = rscIotFGStockMapper;
        this.rscMtPPHeaderService = rscMtPPHeaderService;
    }

    @Override
    public List<RscIotFGStockDTO> findAllByStockTypeTagAndMPS(String stockTypeTag, Long ppHeaderId) {
        return rscMtPPHeaderService.findOne(ppHeaderId)
                .map(rscMtPPHeaderDTO ->
                        rscIotFGStockMapper.toDto(rscIotFGStockRepository.findByRscDtStockTypeTag(stockTypeTag))
                                .stream()
                                .filter(rscIotFGStockDTO -> DateUtils.isEqualMonthAndYear(rscIotFGStockDTO.getStockDate(), rscMtPPHeaderDTO.getMpsDate()))
                                .collect(Collectors.toList()))
                .orElse(null);
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscIotFGStockRepository.deleteAllByStockDate(stockDate);
    }
}