package com.example.validation.service.impl;

import com.example.validation.service.*;
import org.springframework.stereotype.Service;

@Service
public class CalculationServiceImpl implements CalculationService {
    private final LogicalConsumptionService logicalConsumptionService;
    private final GrossConsumptionService grossConsumptionService;
    private final RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService;
    private final RscIotPMPurchasePlanService rscIotPMPurchasePlanService;
    private final RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService;
    private final RscIotRMPurchasePlanService rscIotRMPurchasePlanService;
    private final RscItPMSlobService rscItPMSlobService;
    private final RscIotRMSlobService rscIotRMSlobService;
    private final RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService;
    private final RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService;
    private final RscIotPmCoverDaysService rscIotPmCoverDaysService;

    public CalculationServiceImpl(LogicalConsumptionService logicalConsumptionService, GrossConsumptionService grossConsumptionService, RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService, RscIotPMPurchasePlanService rscIotPMPurchasePlanService, RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService, RscIotRMPurchasePlanService rscIotRMPurchasePlanService, RscItPMSlobService rscItPMSlobService, RscIotRMSlobService rscIotRMSlobService, RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService, RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService, RscIotPmCoverDaysService rscIotPmCoverDaysService) {
        this.logicalConsumptionService = logicalConsumptionService;
        this.grossConsumptionService = grossConsumptionService;
        this.rscIotPmPurchasePlanCalculationsService = rscIotPmPurchasePlanCalculationsService;
        this.rscIotPMPurchasePlanService = rscIotPMPurchasePlanService;
        this.rscIotRMPurchasePlanCalculationsService = rscIotRMPurchasePlanCalculationsService;
        this.rscIotRMPurchasePlanService = rscIotRMPurchasePlanService;
        this.rscItPMSlobService = rscItPMSlobService;
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.rscIotOriginalMouldSaturationService = rscIotOriginalMouldSaturationService;
        this.rscIotOriginalTotalMouldSaturationService = rscIotOriginalTotalMouldSaturationService;
        this.rscIotPmCoverDaysService = rscIotPmCoverDaysService;
    }

    @Override
    public String createAllCalculation(Long ppHeaderId) {
        logicalConsumptionService.createLogicalConsumption(ppHeaderId);
        grossConsumptionService.createGrossConsumption(ppHeaderId);
        logicalConsumptionService.createBPLogicalConsumption(ppHeaderId);
        grossConsumptionService.createBPGrossConsumption(ppHeaderId);
        logicalConsumptionService.createRMLogicalConsumption(ppHeaderId);
        grossConsumptionService.createRMGrossConsumption(ppHeaderId);
        rscIotPmPurchasePlanCalculationsService.createPMSupplyCalculations(ppHeaderId);
        rscIotPMPurchasePlanService.createPMPurchasePlan(ppHeaderId);
        rscIotRMPurchasePlanCalculationsService.createRMPurchasePlanCalculations(ppHeaderId);
        rscIotRMPurchasePlanService.createRMPurchasePlan(ppHeaderId);
        rscItPMSlobService.calculateRscItPMSlob(ppHeaderId);
        rscItPMSlobService.calculateRscItPMSlobSummary(ppHeaderId,null);
        rscIotRMSlobService.calculateRscItRMSlob(ppHeaderId);
        rscIotRMSlobService.calculateRscItRMSlobSummary(ppHeaderId,null);
        rscIotOriginalMouldSaturationService.saveMouldSaturation(ppHeaderId);
        rscIotOriginalTotalMouldSaturationService.saveOriginalTotalMouldSaturation(ppHeaderId);
        rscIotPmCoverDaysService.calculateRscIotPmCoverDays(ppHeaderId);
        rscIotPmCoverDaysService.calculateRscIotPmCoverDaysSummary(ppHeaderId);
        return "All Calculation Completed";
    }
}