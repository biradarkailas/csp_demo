package com.example.validation.service.impl;

import com.example.validation.dto.RscDtTestTaskDTO;
import com.example.validation.mapper.RscDtTestTaskMapper;
import com.example.validation.repository.RscDtTestTaskRepository;
import com.example.validation.service.RscDtTestTaskService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtTestTaskServiceImpl implements RscDtTestTaskService {
    private final RscDtTestTaskRepository rscDtTestTaskRepository;
    private final RscDtTestTaskMapper rscDtTestTaskMapper;

    public RscDtTestTaskServiceImpl(RscDtTestTaskRepository rscDtTestTaskRepository, RscDtTestTaskMapper rscDtTestTaskMapper) {
        this.rscDtTestTaskRepository = rscDtTestTaskRepository;
        this.rscDtTestTaskMapper = rscDtTestTaskMapper;
    }

    @Override
    public List<RscDtTestTaskDTO> getAllRscDtTestTask() {
        return rscDtTestTaskMapper.toDto(rscDtTestTaskRepository.findAll());
    }
}
