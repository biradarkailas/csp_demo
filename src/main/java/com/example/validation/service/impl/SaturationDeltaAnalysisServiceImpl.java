package com.example.validation.service.impl;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.FgSaturationDeltaAnalysisDTO;
import com.example.validation.dto.RscIotFgLineSaturationSummaryDTO;
import com.example.validation.dto.RscIotFgSkidSaturationSummaryDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.RscIotFgLineSaturationSummaryService;
import com.example.validation.service.RscIotFgSkidSaturationSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.service.SaturationDeltaAnalysisService;
import com.example.validation.util.SaturationDeltaAnalysisUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SaturationDeltaAnalysisServiceImpl implements SaturationDeltaAnalysisService {
    private final SaturationDeltaAnalysisUtils saturationDeltaAnalysisUtils;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotFgLineSaturationSummaryService rscIotFgLineSaturationSummaryService;
    private final RscIotFgSkidSaturationSummaryService rscIotFgSkidSaturationSummaryService;

    public SaturationDeltaAnalysisServiceImpl(SaturationDeltaAnalysisUtils saturationDeltaAnalysisUtils, RscMtPpheaderService rscMtPpheaderService, RscIotFgLineSaturationSummaryService rscIotFgLineSaturationSummaryService, RscIotFgSkidSaturationSummaryService rscIotFgSkidSaturationSummaryService) {
        this.saturationDeltaAnalysisUtils = saturationDeltaAnalysisUtils;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotFgLineSaturationSummaryService = rscIotFgLineSaturationSummaryService;
        this.rscIotFgSkidSaturationSummaryService = rscIotFgSkidSaturationSummaryService;
    }

    @Override
    public FgSaturationDeltaAnalysisDTO getFgLineSaturationDeltaAnalysis(Long rscDtLinesId) {
        FgSaturationDeltaAnalysisDTO fgSaturationDeltaAnalysisDTO = new FgSaturationDeltaAnalysisDTO();
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllByActiveAndIsValidated();
        List<RscIotFgLineSaturationSummaryDTO> lineSaturationDTOList = rscIotFgLineSaturationSummaryService.getLinesList(rscDtLinesId);
        if (Optional.ofNullable(pPheaderDTOList).isPresent() && lineSaturationDTOList.size() > 0) {
            RscMtPPheaderDTO rscMtPPheaderDTO = pPheaderDTOList.get(0);
            fgSaturationDeltaAnalysisDTO.setSaturationDetailsList(saturationDeltaAnalysisUtils.getSaturationDetails(rscDtLinesId, rscMtPPheaderDTO, MaterialCategoryConstants.LINE_SATURATION_TAG));
            fgSaturationDeltaAnalysisDTO.setFinalSaturationDetails(saturationDeltaAnalysisUtils.getFinalSaturationDetails(fgSaturationDeltaAnalysisDTO.getSaturationDetailsList()));
            fgSaturationDeltaAnalysisDTO.setDeviationDetails(saturationDeltaAnalysisUtils.getDeviationDetails(fgSaturationDeltaAnalysisDTO.getFinalSaturationDetails(), fgSaturationDeltaAnalysisDTO.getSaturationDetailsList()));
        }
        return fgSaturationDeltaAnalysisDTO;
    }

    @Override
    public FgSaturationDeltaAnalysisDTO getFgSkidSaturationDeltaAnalysis(Long rscDtSkidsId) {
        FgSaturationDeltaAnalysisDTO fgSaturationDeltaAnalysisDTO = new FgSaturationDeltaAnalysisDTO();
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllByActiveAndIsValidated();
        List<RscIotFgSkidSaturationSummaryDTO> skidSaturationDTOList = rscIotFgSkidSaturationSummaryService.getSkidsList(rscDtSkidsId);
        if (Optional.ofNullable(pPheaderDTOList).isPresent() && skidSaturationDTOList.size() > 0) {
            RscMtPPheaderDTO rscMtPPheaderDTO = pPheaderDTOList.get(0);
            fgSaturationDeltaAnalysisDTO.setSaturationDetailsList(saturationDeltaAnalysisUtils.getSaturationDetails(rscDtSkidsId, rscMtPPheaderDTO, MaterialCategoryConstants.SKID_SATURATION_TAG));
            fgSaturationDeltaAnalysisDTO.setFinalSaturationDetails(saturationDeltaAnalysisUtils.getFinalSaturationDetails(fgSaturationDeltaAnalysisDTO.getSaturationDetailsList()));
            fgSaturationDeltaAnalysisDTO.setDeviationDetails(saturationDeltaAnalysisUtils.getDeviationDetails(fgSaturationDeltaAnalysisDTO.getFinalSaturationDetails(), fgSaturationDeltaAnalysisDTO.getSaturationDetailsList()));
        }
        return fgSaturationDeltaAnalysisDTO;
    }

}
