package com.example.validation.service.impl;


import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.service.MonthService;
import com.example.validation.service.RscIotMPSPlanTotalQuantityService;
import com.example.validation.service.RscMtPpdetailsService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.specification.RscMtPPDetailsCriteria;
import com.example.validation.util.MPSPlanTotalQuantityUtil;
import com.example.validation.util.excel_export.PmMPSTriangleAnalysisExporter;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class RscIotMPSPlanTotalQuantityServiceImpl implements RscIotMPSPlanTotalQuantityService {
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final MPSPlanTotalQuantityUtil mpsPlanTotalQuantityUtil;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final MonthService monthService;

    public RscIotMPSPlanTotalQuantityServiceImpl(RscMtPpdetailsService rscMtPpdetailsService, MPSPlanTotalQuantityUtil mpsPlanTotalQuantityUtil, RscMtPpheaderService rscMtPpheaderService, MonthService monthService) {
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.mpsPlanTotalQuantityUtil = mpsPlanTotalQuantityUtil;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.monthService = monthService;
    }


    @Override
    public MPSDeltaAnalysisMainDTO getFilteredMPSDeltaAnalysis(RscMtPPDetailsCriteria criteria) {
        Map<Integer, List<Double>> mpsPlanTotalQuantityDTOMap = new HashMap<>();
        List<RscMtPPheaderDTO> activeAndIsValidatedDTOs = rscMtPpheaderService.findAllByActiveAndIsValidated();
        if (Optional.ofNullable(activeAndIsValidatedDTOs).isPresent() && !activeAndIsValidatedDTOs.isEmpty()) {
            int size = activeAndIsValidatedDTOs.size();
            for (int index = 1; index <= size; index++) {
                if (index > 12)
                    break;
                RscMtPPheaderDTO rscMtPPheaderDTO = activeAndIsValidatedDTOs.get(index - 1);
                criteria.setMpsId(rscMtPPheaderDTO.getId());
                List<RscMtPPdetailsDTO> rscMtPPDetailsDTOs = rscMtPpdetailsService.findAllByCriteria(criteria);
                List<Double> monthWiseMPSPlanTotalQuantity = mpsPlanTotalQuantityUtil.getMonthWiseMPSPlanTotalQuantityDTO(rscMtPPDetailsDTOs);
                mpsPlanTotalQuantityDTOMap.put(index, monthWiseMPSPlanTotalQuantity);
            }
            return mpsPlanTotalQuantityUtil.getMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityDTOMap, size);
        }
        return null;
    }


    public ExcelExportFiles exportPmMPSTriangleAnalysisExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, MpsPlanMainDTO mpsPlanMainDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        RscMtPPDetailsCriteria criteria = getCriteriaByMpsHeaderId(rscMtPPheaderDTO.getId());
        MPSDeltaAnalysisMainDTO mpsDeltaAnalysisMainDTO = getFilteredMPSDeltaAnalysis(criteria);
        DeltaColumnNameDTO deltaColumnNameDTO = monthService.getDeltaAnalysisMonthNames();
        excelExportFiles.setFile(PmMPSTriangleAnalysisExporter.pmMPSTriangleAnalysisToExcelList(criteria, mpsDeltaAnalysisMainDTO, rscMtPPheaderDTO.getMpsDate(), deltaColumnNameDTO));
        excelExportFiles.setFileName(ExcelFileNameConstants.MPS_Triangle_Analysis + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    private RscMtPPDetailsCriteria getCriteriaByMpsHeaderId(Long ppheaderId) {
        RscMtPPDetailsCriteria rscMtPPDetailsCriteria = new RscMtPPDetailsCriteria();
        rscMtPPDetailsCriteria.setMpsId(ppheaderId);

        MpsPlanMainDTO mpsPlanMainDTO = rscMtPpdetailsService.getProductionPlan(ppheaderId);
        ListIterator<ItemDetailDTO> it = mpsPlanMainDTO.getBpiCodes().listIterator();
        HashSet itemIdList = new HashSet();
        HashSet divisionsList = new HashSet();
        HashSet skidsIdList = new HashSet();
        HashSet linesIdList = new HashSet();
        HashSet categoryIdList = new HashSet();
        HashSet subCategoryIdList = new HashSet();
        HashSet signatureIdList = new HashSet();
        HashSet brandIdList = new HashSet();
        while (it.hasNext()) {
            ItemDetailDTO obj = it.next();
            divisionsList.add(obj.getOtherInfo().getDivisionId());
            skidsIdList.add(obj.getOtherInfo().getSkidsId());
            linesIdList.add(obj.getOtherInfo().getLinesId());
            categoryIdList.add(obj.getOtherInfo().getMaterialCategoryId());
            subCategoryIdList.add(obj.getOtherInfo().getMaterialSubCategoryId());
            signatureIdList.add(obj.getOtherInfo().getSignatureId());
            brandIdList.add(obj.getOtherInfo().getBrandId());
        }
        rscMtPPDetailsCriteria.setItemIds(getListOfUniqueItems(itemIdList));
        rscMtPPDetailsCriteria.setDivisionIds(getListOfUniqueItems(divisionsList));
        rscMtPPDetailsCriteria.setSkidIds(getListOfUniqueItems(skidsIdList));
        rscMtPPDetailsCriteria.setLineIds(getListOfUniqueItems(linesIdList));
        rscMtPPDetailsCriteria.setCategoryIds(getListOfUniqueItems(categoryIdList));
        rscMtPPDetailsCriteria.setSubCategoryIds(getListOfUniqueItems(subCategoryIdList));
        rscMtPPDetailsCriteria.setSignatureIds(getListOfUniqueItems(signatureIdList));
        rscMtPPDetailsCriteria.setBrandIds(getListOfUniqueItems(brandIdList));
        return rscMtPPDetailsCriteria;
    }

    private List<Long> getListOfUniqueItems(HashSet itemsList) {
        return new ArrayList<Long>(itemsList);
    }
}