package com.example.validation.service.impl;

import com.example.validation.dto.RscDtDivisionDTO;
import com.example.validation.dto.RscDtItemClassDTO;
import com.example.validation.mapper.RscDtItemClassMapper;
import com.example.validation.repository.RscDtItemClassRepository;
import com.example.validation.service.RscDtItemClassService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtItemClassServiceImpl implements RscDtItemClassService {
    private final RscDtItemClassRepository rscDtItemClassRepository;
    private final RscDtItemClassMapper rscDtItemClassMapper;

    public RscDtItemClassServiceImpl(RscDtItemClassRepository rscDtItemClassRepository, RscDtItemClassMapper rscDtItemClassMapper){
        this.rscDtItemClassRepository = rscDtItemClassRepository;
        this.rscDtItemClassMapper = rscDtItemClassMapper;
    }

    @Override
    public List<RscDtItemClassDTO> findAll() {
        return rscDtItemClassMapper.toDto(rscDtItemClassRepository.findAll());
    }
}
