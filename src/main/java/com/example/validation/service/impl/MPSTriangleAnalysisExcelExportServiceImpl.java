package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.MpsPlanMainDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class MPSTriangleAnalysisExcelExportServiceImpl implements MPSTriangleAnalysisExcelExportService {
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final MonthService monthService;
    private final FgSlobMovementAndInventoryIndexService fgSlobMovementAndInventoryIndexService;
    private final RscIotMPSPlanTotalQuantityService rscIotMPSPlanTotalQuantityService;
    private final RscIotRMSlobService rscIotRMSlobService;
    private final RscItPMSlobService rscItPMSlobService;

    public MPSTriangleAnalysisExcelExportServiceImpl(RscMtPpdetailsService rscMtPpdetailsService, RscMtPpheaderService rscMtPpheaderService, MonthService monthService, FgSlobMovementAndInventoryIndexService fgSlobMovementAndInventoryIndexService, RscIotMPSPlanTotalQuantityService rscIotMPSPlanTotalQuantityService, RscIotRMSlobService rscIotRMSlobService, RscItPMSlobService rscItPMSlobService) {
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.monthService = monthService;
        this.fgSlobMovementAndInventoryIndexService = fgSlobMovementAndInventoryIndexService;
        this.rscIotMPSPlanTotalQuantityService = rscIotMPSPlanTotalQuantityService;
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.rscItPMSlobService = rscItPMSlobService;
    }


    @Override
    public String createAllMPSTriangleAnalysisExcelExports(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            String year = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() );
            String month = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() );
            String date = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() );
            String name = optionalRscMtPpHeaderDTO.get().getMpsName();
            createDirectory( year, month, date, name );
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails( optionalRscMtPpHeaderDTO.get().getMpsDate() );
            createMPSTriangleAnalysisExcels( optionalRscMtPpHeaderDTO.get(), year, month, monthDetailDTO );
            savePmExport( optionalRscMtPpHeaderDTO.get(), year, month, date );
            saveFgExport( optionalRscMtPpHeaderDTO.get(), year, month, date );
            saveRmExport( optionalRscMtPpHeaderDTO.get(), year, month, date );
        }
        return "Downloaded All Exports Successfully";
    }

    private void createMPSTriangleAnalysisExcels(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, MonthDetailDTO monthDetailDTO) {
        MpsPlanMainDTO mpsPlanMainDTO = rscMtPpdetailsService.getMPSPlanMainDTO( rscMtPPheaderDTO );
        ExcelExportFiles pmAnnualExcelExportFile = rscIotMPSPlanTotalQuantityService.exportPmMPSTriangleAnalysisExporter( rscMtPPheaderDTO, monthDetailDTO, mpsPlanMainDTO );
        createExcelFile( rscMtPPheaderDTO, year, month, pmAnnualExcelExportFile, ExcelFileNameConstants.PM_Directory );
        ExcelExportFiles fgAnnualExcelExportFile = rscIotMPSPlanTotalQuantityService.exportPmMPSTriangleAnalysisExporter( rscMtPPheaderDTO, monthDetailDTO, mpsPlanMainDTO );
        createExcelFile( rscMtPPheaderDTO, year, month, fgAnnualExcelExportFile, ExcelFileNameConstants.FG_Directory );
        ExcelExportFiles rmAnnualExcelExportFile = rscIotMPSPlanTotalQuantityService.exportPmMPSTriangleAnalysisExporter( rscMtPPheaderDTO, monthDetailDTO, mpsPlanMainDTO );
        createExcelFile( rscMtPPheaderDTO, year, month, rmAnnualExcelExportFile, ExcelFileNameConstants.RM_Directory );

    }

    private void saveFgExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date) {
        List<ExcelExportFiles> fgExportList = new ArrayList<>();
        fgExportList.add( fgSlobMovementAndInventoryIndexService.exportFgSlobMovementExporter( rscMtPPheaderDTO ) );
        fgExportList.add( fgSlobMovementAndInventoryIndexService.exportFgSlobInventoryQualityIndexExporter( rscMtPPheaderDTO ) );
        createExcelFiles( rscMtPPheaderDTO, year, month, fgExportList, ExcelFileNameConstants.FG_Directory );
    }

    private void saveRmExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date) {
        List<ExcelExportFiles> rmExportList = new ArrayList<>();
        rmExportList.add( rscIotRMSlobService.exportRmSlobMovementExporter( rscMtPPheaderDTO ) );
        rmExportList.add( rscIotRMSlobService.exportRmSlobInventoryQualityIndexExporter( rscMtPPheaderDTO ) );
        createExcelFiles( rscMtPPheaderDTO, year, month, rmExportList, ExcelFileNameConstants.RM_Directory );
    }

    private void savePmExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date) {
        List<ExcelExportFiles> pmExportList = new ArrayList<>();
        pmExportList.add( rscItPMSlobService.exportPmSlobMovementExporter( rscMtPPheaderDTO ) );
        pmExportList.add( rscItPMSlobService.exportPmSlobInventoryQualityIndexExporter( rscMtPPheaderDTO ) );
        createExcelFiles( rscMtPPheaderDTO, year, month, pmExportList, ExcelFileNameConstants.PM_Directory );
    }

    private void createExcelFile(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, ExcelExportFiles excelExportFile, String moduleType) {
        try {
            FileOutputStream fileOut = new FileOutputStream( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Annual" + "/" + moduleType + excelExportFile.getFileName() );
            fileOut.write( excelExportFile.getFile().readAllBytes() );
        } catch (IOException e) {
            throw new RuntimeException( "Fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private void createExcelFiles(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, List<ExcelExportFiles> fgExportList, String moduleType) {
        for (ExcelExportFiles excelExportFiles : fgExportList) {
            try {
                FileOutputStream fileOut = new FileOutputStream( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Annual" + "/" + moduleType + excelExportFiles.getFileName() );
                fileOut.write( excelExportFiles.getFile().readAllBytes() );
            } catch (IOException e) {
                throw new RuntimeException( "Fail to import data to Excel file: " + e.getMessage() );
            }
        }
    }


    @Override
    public void createDirectory(String year, String month, String date, String name) {
        try {
            Path fgAnnulPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Annual" + "/" + ExcelFileNameConstants.FG );
            Path pmAnnulPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Annual" + "/" + ExcelFileNameConstants.PM );
            Path rmAnnulPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Annual" + "/" + ExcelFileNameConstants.RM );
            Files.createDirectories( fgAnnulPath );
            Files.createDirectories( pmAnnulPath );
            Files.createDirectories( rmAnnulPath );

        } catch (IOException e) {
            System.err.println( "Failed to create directory!" + e.getMessage() );
        }
    }
}