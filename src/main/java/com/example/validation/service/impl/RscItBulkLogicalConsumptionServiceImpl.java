package com.example.validation.service.impl;

import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.mapper.BulkLogicalConsumptionMapper;
import com.example.validation.mapper.RscItBulkLogicalConsumptionMapper;
import com.example.validation.repository.RscItBulkLogicalConsumptionRepository;
import com.example.validation.service.RscItBulkLogicalConsumptionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;

@Service
@Transactional
public class RscItBulkLogicalConsumptionServiceImpl implements RscItBulkLogicalConsumptionService {
    private final RscItBulkLogicalConsumptionRepository rscItBulkLogicalConsumptionRepository;
    private final RscItBulkLogicalConsumptionMapper rscItBulkLogicalConsumptionMapper;
    private final BulkLogicalConsumptionMapper bulkLogicalConsumptionMapper;

    public RscItBulkLogicalConsumptionServiceImpl(RscItBulkLogicalConsumptionRepository rscItBulkLogicalConsumptionRepository, RscItBulkLogicalConsumptionMapper rscItBulkLogicalConsumptionMapper, BulkLogicalConsumptionMapper bulkLogicalConsumptionMapper) {
        this.rscItBulkLogicalConsumptionRepository = rscItBulkLogicalConsumptionRepository;
        this.rscItBulkLogicalConsumptionMapper = rscItBulkLogicalConsumptionMapper;
        this.bulkLogicalConsumptionMapper = bulkLogicalConsumptionMapper;
    }

    @Override
    public List<LogicalConsumptionMainDTO> getBulkLogicalConsumptionByFG(Long ppHeaderId, Long fgMtItemId) {
        return bulkLogicalConsumptionMapper.toDto(rscItBulkLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(ppHeaderId, fgMtItemId));
    }

    @Override
    public void saveAll(List<RscItLogicalConsumptionDTO> bulkRscItLogicalConsumptionDTOs, Long ppHeaderId) {
        if (rscItBulkLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            bulkRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
            bulkRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
            rscItBulkLogicalConsumptionRepository.saveAll(rscItBulkLogicalConsumptionMapper.toEntity(bulkRscItLogicalConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItBulkLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllLogicalConsumptionDTOsByItemCategory(String itemDescription) {
        return rscItBulkLogicalConsumptionMapper.toDto(rscItBulkLogicalConsumptionRepository.findAllByRscMtItemChildRscDtItemCategoryDescription(itemDescription));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAll() {
        return rscItBulkLogicalConsumptionMapper.toDto(rscItBulkLogicalConsumptionRepository.findAll());
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscItBulkLogicalConsumptionMapper.toDto(rscItBulkLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<LogicalConsumptionMainDTO> getBulkLogicalConsumption(Long ppHeaderId) {
        return bulkLogicalConsumptionMapper.toDto(rscItBulkLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderIdAndItemId(Long ppHeaderId, Long itemId) {
        return rscItBulkLogicalConsumptionMapper.toDto(rscItBulkLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemChildId(ppHeaderId, itemId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItBulkLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}