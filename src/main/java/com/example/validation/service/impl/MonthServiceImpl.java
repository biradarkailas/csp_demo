package com.example.validation.service.impl;

import com.example.validation.dto.DeltaColumnNameDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.MonthNameDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.MonthService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;

@Service
public class MonthServiceImpl implements MonthService {

    private final RscMtPpheaderService rscMtPpheaderService;

    public MonthServiceImpl(RscMtPpheaderService rscMtPpheaderService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
    }

    @Override
    public MonthDetailDTO getMonthNameDetail(Long ppHeaderId) {
        RscMtPPheaderDTO rscMtPpheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).get();
        return getMonthDetails(rscMtPpheaderDTO.getMpsDate());
    }

    @Override
    public MonthDetailDTO getMonthDetails(LocalDate date) {
        MonthDetailDTO monthDetailDTO = new MonthDetailDTO();
        monthDetailDTO.setMonth1(getMonthNameDTO(date));
        monthDetailDTO.setMonth2(getMonthNameDTO(date.plusMonths(1)));
        monthDetailDTO.setMonth3(getMonthNameDTO(date.plusMonths(2)));
        monthDetailDTO.setMonth4(getMonthNameDTO(date.plusMonths(3)));
        monthDetailDTO.setMonth5(getMonthNameDTO(date.plusMonths(4)));
        monthDetailDTO.setMonth6(getMonthNameDTO(date.plusMonths(5)));
        monthDetailDTO.setMonth7(getMonthNameDTO(date.plusMonths(6)));
        monthDetailDTO.setMonth8(getMonthNameDTO(date.plusMonths(7)));
        monthDetailDTO.setMonth9(getMonthNameDTO(date.plusMonths(8)));
        monthDetailDTO.setMonth10(getMonthNameDTO(date.plusMonths(9)));
        monthDetailDTO.setMonth11(getMonthNameDTO(date.plusMonths(10)));
        monthDetailDTO.setMonth12(getMonthNameDTO(date.plusMonths(11)));
        return monthDetailDTO;
    }

    private MonthNameDTO getMonthNameDTO(LocalDate date) {
        MonthNameDTO monthNameDTO = new MonthNameDTO();
        monthNameDTO.setMonthNameAlias(date.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH) + " " + String.valueOf(date.getYear()).substring(2));
        monthNameDTO.setMonthName(date.getMonth().getDisplayName(TextStyle.FULL, Locale.ENGLISH) + " " + String.valueOf(date.getYear()));
        return monthNameDTO;
    }

    @Override
    public DeltaColumnNameDTO getDeltaAnalysisMonthNames() {
        DeltaColumnNameDTO deltaColumnNameDTO = new DeltaColumnNameDTO();
        List<String> columnNames = new ArrayList<>();
        List<String> rowNames = new ArrayList<>();
        List<RscMtPPheaderDTO> rscMtPpheaderDTOs = rscMtPpheaderService.findAllByActiveAndIsValidated();
        if (Optional.ofNullable(rscMtPpheaderDTOs).isPresent()&&rscMtPpheaderDTOs.size()>0) {
            LocalDate mpsDate = rscMtPpheaderDTOs.get(0).getMpsDate();
            for (int i = 0; i < rscMtPpheaderDTOs.size() + 11; i++) {
                columnNames.add(getColumnName(mpsDate.plusMonths(i)));
            }
            rowNames.add("Final Plan");
            rowNames.add("Deviation");
            for (int i = 0; i < 12; i++) {
                rowNames.add(getRowName(mpsDate.plusMonths(i)));
            }
        }
        deltaColumnNameDTO.setColumnNames(columnNames);
        deltaColumnNameDTO.setRowNames(rowNames);
        return deltaColumnNameDTO;
    }

    private String getColumnName(LocalDate date) {
        return date.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH) + " " + String.valueOf(date.getYear()).substring(2);
    }

    private String getRowName(LocalDate date) {
        return date.getMonth().getDisplayName(TextStyle.SHORT, Locale.ENGLISH) + " Plan";
    }
}