package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCoverDaysDTO;
import com.example.validation.mapper.RMPurchasePlanCoverDaysEquationMapper;
import com.example.validation.mapper.RscIotRMPurchasePlanCoverDaysMapper;
import com.example.validation.repository.RscIotRMPurchasePlanCoverDaysRepository;
import com.example.validation.service.RscIotRMPurchasePlanCalculationsService;
import com.example.validation.service.RscIotRMPurchasePlanCoverDaysService;
import com.example.validation.util.excel_export.RmPurchaseCoverdaysExporter;
import com.example.validation.util.excel_export.RmPurchaseStockEquationExporter;
import com.example.validation.util.supply.PurchasePlanCoverDaysUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RscIotRMPurchasePlanCoverDaysServiceImpl implements RscIotRMPurchasePlanCoverDaysService {
    private final RscIotRMPurchasePlanCoverDaysRepository rscIotRMPurchasePlanCoverDaysRepository;
    private final RscIotRMPurchasePlanCoverDaysMapper rscIotRMPurchasePlanCoverDaysMapper;
    private final RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService;
    private final PurchasePlanCoverDaysUtil purchasePlanCoverDaysUtil;
    private final RMPurchasePlanCoverDaysEquationMapper rmPurchasePlanCoverDaysEquationMapper;

    public RscIotRMPurchasePlanCoverDaysServiceImpl(RscIotRMPurchasePlanCoverDaysRepository rscIotRMPurchasePlanCoverDaysRepository,
                                                    RscIotRMPurchasePlanCoverDaysMapper rscIotRMPurchasePlanCoverDaysMapper,
                                                    RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService,
                                                    PurchasePlanCoverDaysUtil purchasePlanCoverDaysUtil, RMPurchasePlanCoverDaysEquationMapper rmPurchasePlanCoverDaysEquationMapper) {
        this.rscIotRMPurchasePlanCoverDaysRepository = rscIotRMPurchasePlanCoverDaysRepository;
        this.rscIotRMPurchasePlanCoverDaysMapper = rscIotRMPurchasePlanCoverDaysMapper;
        this.rscIotRMPurchasePlanCalculationsService = rscIotRMPurchasePlanCalculationsService;
        this.purchasePlanCoverDaysUtil = purchasePlanCoverDaysUtil;
        this.rmPurchasePlanCoverDaysEquationMapper = rmPurchasePlanCoverDaysEquationMapper;
    }

    @Override
    public void calculateRMPurchasePlanCoverDays(Long ppHeaderId) {
        if (rscIotRMPurchasePlanCoverDaysRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotRMPurchasePlanCoverDaysDTO> rscIotRMPurchasePlanCoverDaysDTOs = new ArrayList<>();
            List<PurchasePlanCalculationsMainDTO> rmPurchasePlanCalculationsMainDTOs = rscIotRMPurchasePlanCalculationsService.findAll(ppHeaderId);
            for (PurchasePlanCalculationsMainDTO rmPurchasePlanCalculationsDTO : rmPurchasePlanCalculationsMainDTOs) {
                rscIotRMPurchasePlanCoverDaysDTOs.add(purchasePlanCoverDaysUtil.getRscIotRMPurchasePlanCoverDaysDTO(rmPurchasePlanCalculationsDTO));
            }
            rscIotRMPurchasePlanCoverDaysRepository.saveAll(rscIotRMPurchasePlanCoverDaysMapper.toEntity(rscIotRMPurchasePlanCoverDaysDTOs));
        }
    }

    @Override
    public List<RscIotRMPurchasePlanCoverDaysDTO> getAllRMPurchasePlanCoverDays(Long ppHeaderId) {
        return rscIotRMPurchasePlanCoverDaysMapper.toDto(rscIotRMPurchasePlanCoverDaysRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<PurchasePlanCoverDaysEquationDTO> getPurchasePlanCoverDaysEquationDTOs(Long ppHeaderId) {
        return rmPurchasePlanCoverDaysEquationMapper.toDto(rscIotRMPurchasePlanCoverDaysRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    public ExcelExportFiles rmPurchaseCoverdaysExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmPurchaseCoverdaysExporter.rmPurchaseCoverdaysToExcel(getAllRMPurchasePlanCoverDays(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_Purchase_Coverdays + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    public ExcelExportFiles rmPurchaseStockEquationExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmPurchaseStockEquationExporter.rmPurchaseStockEquationToExcel(getPurchasePlanCoverDaysEquationDTOs(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_Purchase_Stock_Equation + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotRMPurchasePlanCoverDaysRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}