package com.example.validation.service.impl;

import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.mapper.RscDtSupplierMapper;
import com.example.validation.repository.RscDtSupplierRepository;
import com.example.validation.service.RscDtSupplierService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtSupplierServiceImpl implements RscDtSupplierService {
    private final RscDtSupplierRepository rscDtSupplierRepository;
    private final RscDtSupplierMapper rscDtSupplierMapper;

    public RscDtSupplierServiceImpl(RscDtSupplierRepository rscDtSupplierRepository, RscDtSupplierMapper rscDtSupplierMapper){
        this.rscDtSupplierRepository = rscDtSupplierRepository;
        this.rscDtSupplierMapper = rscDtSupplierMapper;
    }

    @Override
    public List<RscDtSupplierDTO> findAll() {
        return rscDtSupplierMapper.toDto(rscDtSupplierRepository.findAll());
    }
}
