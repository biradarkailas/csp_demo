package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.mapper.MouldSaturationDashboardMapper;
import com.example.validation.mapper.RscIotLatestTotalMouldSaturationMapper;
import com.example.validation.repository.RscIotLatestTotalMouldSaturationRepository;
import com.example.validation.service.RscIotLatestMouldSaturationService;
import com.example.validation.service.RscIotLatestTotalMouldSaturationService;
import com.example.validation.service.RscIotOriginalTotalMouldSaturationService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.enums.SaturationRange;
import com.example.validation.util.excel_export.PmMouldWiseMouldExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotLatestTotalMouldSaturationServiceImpl implements RscIotLatestTotalMouldSaturationService {
    private final RscIotLatestTotalMouldSaturationRepository rscIotLatestTotalMouldSaturationRepository;
    private final RscIotLatestTotalMouldSaturationMapper rscIotLatestTotalMouldSaturationMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final MouldSaturationDashboardMapper mouldSaturationDashboardMapper;
    private final RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService;
    private final RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService;

    public RscIotLatestTotalMouldSaturationServiceImpl(RscIotLatestTotalMouldSaturationRepository rscIotLatestTotalMouldSaturationRepository, RscIotLatestTotalMouldSaturationMapper rscIotLatestTotalMouldSaturationMapper, RscMtPpheaderService rscMtPpheaderService, MouldSaturationDashboardMapper mouldSaturationDashboardMapper, RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService, RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService) {
        this.rscIotLatestTotalMouldSaturationRepository = rscIotLatestTotalMouldSaturationRepository;
        this.rscIotLatestTotalMouldSaturationMapper = rscIotLatestTotalMouldSaturationMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.mouldSaturationDashboardMapper = mouldSaturationDashboardMapper;
        this.rscIotLatestMouldSaturationService = rscIotLatestMouldSaturationService;
        this.rscIotOriginalTotalMouldSaturationService = rscIotOriginalTotalMouldSaturationService;
    }

    @Override
    public void saveLatestTotalMouldSaturation(Long ppHeaderId) {
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            if (rscIotLatestTotalMouldSaturationRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
                List<RscIotLatestMouldSaturationDTO> mouldSaturationDTOList = rscIotLatestMouldSaturationService.getMouldSaturationListByPpHeaderId(ppHeaderId);
                if (Optional.ofNullable(mouldSaturationDTOList).isPresent()) {
                    List<RscIotLatestTotalMouldSaturationDTO> latestTotalMouldSaturationDTOList = getLatestTotalMouldSaturationList(mouldSaturationDTOList);
                    saveAll(latestTotalMouldSaturationDTOList);
                    rscIotOriginalTotalMouldSaturationService.saveAllOriginalTotalMouldSaturation(latestTotalMouldSaturationDTOList);
                }
            }
        }
    }

    private List<RscIotLatestTotalMouldSaturationDTO> getLatestTotalMouldSaturationList(List<RscIotLatestMouldSaturationDTO> mouldSaturationDTOList) {
        HashMap<Long, RscIotLatestTotalMouldSaturationDTO> mouldSaturationDTOHashMap = new HashMap<>();
        mouldSaturationDTOList.forEach(rscIotLatestMouldSaturationDTO -> {
            if (mouldSaturationDTOHashMap.containsKey(rscIotLatestMouldSaturationDTO.getMould())) {
                mouldSaturationDTOHashMap.put(rscIotLatestMouldSaturationDTO.getMould(), getMouldSaturation(mouldSaturationDTOHashMap.get(rscIotLatestMouldSaturationDTO.getMould()), rscIotLatestMouldSaturationDTO));
            } else {
                mouldSaturationDTOHashMap.put(rscIotLatestMouldSaturationDTO.getMould(), getMouldSaturationWhenMouldAddedFirstTime(rscIotLatestMouldSaturationDTO));
            }
        });
        return mouldSaturationDTOHashMap.values().stream().collect(Collectors.toList());
    }

    private RscIotLatestTotalMouldSaturationDTO getMouldSaturation(RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO, RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturationDTO) {
        rscIotLatestTotalMouldSaturationDTO.setM1Value(rscIotLatestTotalMouldSaturationDTO.getM1Value() + rscIotLatestMouldSaturationDTO.getM1Value());
        rscIotLatestTotalMouldSaturationDTO.setM2Value(rscIotLatestTotalMouldSaturationDTO.getM2Value() + rscIotLatestMouldSaturationDTO.getM2Value());
        rscIotLatestTotalMouldSaturationDTO.setM3Value(rscIotLatestTotalMouldSaturationDTO.getM3Value() + rscIotLatestMouldSaturationDTO.getM3Value());
        rscIotLatestTotalMouldSaturationDTO.setM4Value(rscIotLatestTotalMouldSaturationDTO.getM4Value() + rscIotLatestMouldSaturationDTO.getM4Value());
        rscIotLatestTotalMouldSaturationDTO.setM5Value(rscIotLatestTotalMouldSaturationDTO.getM5Value() + rscIotLatestMouldSaturationDTO.getM5Value());
        rscIotLatestTotalMouldSaturationDTO.setM6Value(rscIotLatestTotalMouldSaturationDTO.getM6Value() + rscIotLatestMouldSaturationDTO.getM6Value());
        rscIotLatestTotalMouldSaturationDTO.setM7Value(rscIotLatestTotalMouldSaturationDTO.getM7Value() + rscIotLatestMouldSaturationDTO.getM7Value());
        rscIotLatestTotalMouldSaturationDTO.setM8Value(rscIotLatestTotalMouldSaturationDTO.getM8Value() + rscIotLatestMouldSaturationDTO.getM8Value());
        rscIotLatestTotalMouldSaturationDTO.setM9Value(rscIotLatestTotalMouldSaturationDTO.getM9Value() + rscIotLatestMouldSaturationDTO.getM9Value());
        rscIotLatestTotalMouldSaturationDTO.setM10Value(rscIotLatestTotalMouldSaturationDTO.getM10Value() + rscIotLatestMouldSaturationDTO.getM10Value());
        rscIotLatestTotalMouldSaturationDTO.setM11Value(rscIotLatestTotalMouldSaturationDTO.getM11Value() + rscIotLatestMouldSaturationDTO.getM11Value());
        rscIotLatestTotalMouldSaturationDTO.setM12Value(rscIotLatestTotalMouldSaturationDTO.getM12Value() + rscIotLatestMouldSaturationDTO.getM12Value());
        return rscIotLatestTotalMouldSaturationDTO;
    }

    private RscIotLatestTotalMouldSaturationDTO getMouldSaturationWhenMouldAddedFirstTime(RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturationDTO) {
        RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO = new RscIotLatestTotalMouldSaturationDTO();
        rscIotLatestTotalMouldSaturationDTO.setM1Value(rscIotLatestMouldSaturationDTO.getM1Value());
        rscIotLatestTotalMouldSaturationDTO.setM2Value(rscIotLatestMouldSaturationDTO.getM2Value());
        rscIotLatestTotalMouldSaturationDTO.setM3Value(rscIotLatestMouldSaturationDTO.getM3Value());
        rscIotLatestTotalMouldSaturationDTO.setM4Value(rscIotLatestMouldSaturationDTO.getM4Value());
        rscIotLatestTotalMouldSaturationDTO.setM5Value(rscIotLatestMouldSaturationDTO.getM5Value());
        rscIotLatestTotalMouldSaturationDTO.setM6Value(rscIotLatestMouldSaturationDTO.getM6Value());
        rscIotLatestTotalMouldSaturationDTO.setM7Value(rscIotLatestMouldSaturationDTO.getM7Value());
        rscIotLatestTotalMouldSaturationDTO.setM8Value(rscIotLatestMouldSaturationDTO.getM8Value());
        rscIotLatestTotalMouldSaturationDTO.setM9Value(rscIotLatestMouldSaturationDTO.getM9Value());
        rscIotLatestTotalMouldSaturationDTO.setM10Value(rscIotLatestMouldSaturationDTO.getM10Value());
        rscIotLatestTotalMouldSaturationDTO.setM11Value(rscIotLatestMouldSaturationDTO.getM11Value());
        rscIotLatestTotalMouldSaturationDTO.setM12Value(rscIotLatestMouldSaturationDTO.getM12Value());
        rscIotLatestTotalMouldSaturationDTO.setRscDtMouldId(rscIotLatestMouldSaturationDTO.getMould());
        rscIotLatestTotalMouldSaturationDTO.setRscMtPPheaderId(rscIotLatestMouldSaturationDTO.getRscMtPPheaderId());
        return rscIotLatestTotalMouldSaturationDTO;
    }

    private void saveAll(List<RscIotLatestTotalMouldSaturationDTO> latestTotalMouldSaturationList) {
        rscIotLatestTotalMouldSaturationRepository.saveAll(rscIotLatestTotalMouldSaturationMapper.toEntity(latestTotalMouldSaturationList));
    }
   /* @SuppressWarnings("Duplicates")
    @Override
    public String saveLatestTotalMouldSaturation(Long ppheaderId) {
        if (count(ppheaderId) == 0) {
            var rscItItemMouldList = rscItItemMouldRepository.findAll();
            var rscItItemMouldDistinctByRscDtMould = rscItItemMouldList.stream().filter(ListUtils.distinctByKey(p -> p.getRscDtMould().getId())).collect(Collectors.toList());

            for (RscItItemMould rscItItemMould : rscItItemMouldDistinctByRscDtMould) {
                Double month1Total = 0.0;
                Double month2Total = 0.0;
                Double month3Total = 0.0;
                Double month4Total = 0.0;
                Double month5Total = 0.0;
                Double month6Total = 0.0;
                Double month7Total = 0.0;
                Double month8Total = 0.0;
                Double month9Total = 0.0;
                Double month10Total = 0.0;
                Double month11Total = 0.0;
                Double month12Total = 0.0;
                Double rtdValue = 0.0;
                Double totalCapacity = 0.0;
                var rscItItemMouldRepositoryByRscDtMouldId = rscItItemMouldRepository.findByRscDtMouldId(rscItItemMould.getRscDtMould().getId());

                for (RscItItemMould rscItItemMould1 : rscItItemMouldRepositoryByRscDtMouldId) {
                    var originalPmPurchasePlan = rscIotPMLatestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlanRepository.findAllByRscMtPPheaderIdAndRscMtItemId(ppheaderId, rscItItemMould1.getRscMtItem().getId()));

                    if (Optional.ofNullable(originalPmPurchasePlan).isPresent()) {
                        month1Total += originalPmPurchasePlan.getM1Value();
                        month2Total += originalPmPurchasePlan.getM2Value();
                        month3Total += originalPmPurchasePlan.getM3Value();
                        month4Total += originalPmPurchasePlan.getM4Value();
                        month5Total += originalPmPurchasePlan.getM5Value();
                        month6Total += originalPmPurchasePlan.getM6Value();
                        month7Total += originalPmPurchasePlan.getM7Value();
                        month8Total += originalPmPurchasePlan.getM8Value();
                        month9Total += originalPmPurchasePlan.getM9Value();
                        month10Total += originalPmPurchasePlan.getM10Value();
                        month11Total += originalPmPurchasePlan.getM11Value();
                        month12Total += originalPmPurchasePlan.getM12Value();
                        rtdValue += originalPmPurchasePlan.getRtdValue();
                        totalCapacity = rscItItemMould1.getRscDtMould().getTotalCapacity();
                    }
                }
                RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO = new RscIotLatestTotalMouldSaturationDTO();
                RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay = rscIitMpsWorkingDaysService.findByMpsId(ppheaderId);

                if (rscIitMpsWorkingDay != null && rscIitMpsWorkingDay != null) {
                    rscIotLatestTotalMouldSaturationDTO.setM1Value(MouldSaturationUtil.getMonth1MouldSaturation(month1Total, month2Total, rscIitMpsWorkingDay.getMonth1Value(), rtdValue, totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM2Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month3Total, rscIitMpsWorkingDay.getMonth2Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM3Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month4Total, rscIitMpsWorkingDay.getMonth3Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM4Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month5Total, rscIitMpsWorkingDay.getMonth4Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM5Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month6Total, rscIitMpsWorkingDay.getMonth5Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM6Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month7Total, rscIitMpsWorkingDay.getMonth6Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM7Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month8Total, rscIitMpsWorkingDay.getMonth7Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM8Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month9Total, rscIitMpsWorkingDay.getMonth8Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM9Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month10Total, rscIitMpsWorkingDay.getMonth9Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM10Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month11Total, rscIitMpsWorkingDay.getMonth10Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM11Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month12Total, rscIitMpsWorkingDay.getMonth11Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setM12Value(MouldSaturationUtil.getMonthWiseMouldSaturation(0.0, rscIitMpsWorkingDay.getMonth12Value(), totalCapacity));
                    rscIotLatestTotalMouldSaturationDTO.setRscMtPPheaderId(ppheaderId);
                    rscIotLatestTotalMouldSaturationDTO.setRscDtMouldId(rscItItemMould.getRscDtMould().getId());
                }

                rscIotLatestTotalMouldSaturationRepository.save(rscIotLatestTotalMouldSaturationMapper.toEntity(rscIotLatestTotalMouldSaturationDTO));
            }
        }
        return "Latest Total Mould Saturation saved successfully";
    }*/

    @Override
    public List<RscIotLatestTotalMouldSaturationDTO> findAll() {
        return rscIotLatestTotalMouldSaturationMapper.toDto(rscIotLatestTotalMouldSaturationRepository.findAll());
    }

    @Override
    public List<RscIotLatestTotalMouldSaturationDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscIotLatestTotalMouldSaturationMapper.toDto(rscIotLatestTotalMouldSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public ExcelExportFiles mouldWiseSaturationToExcel(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscIotLatestTotalMouldSaturationDTO> rscIotOriginalTotalMouldSaturationDTOS = findAllByRscMtPPheaderId(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(PmMouldWiseMouldExporter.mouldWiseToExcel(rscIotOriginalTotalMouldSaturationDTOS, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_MOULD_WISE_MOULD + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

   /* @Override
    public Long count(Long ppHeaderId) {
        return rscIotLatestTotalMouldSaturationRepository.countByRscMtPPheaderId(ppHeaderId);
    }*/

    @Override
    public List<MouldSaturationDetailsDTO> getMouldSaturationWithHighStatusForCurrentMonth(Long ppHeaderId) {
        return mouldSaturationDashboardMapper.toDto(rscIotLatestTotalMouldSaturationRepository.findAllByRscMtPPheaderIdAndM1ValueGreaterThanEqual(ppHeaderId, Long.valueOf(SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal())));
    }

    @Override
    public List<MouldSaturationDetailsDTO> getMouldSaturationWithMediumStatusForCurrentMonth(Long ppHeaderId) {
        return mouldSaturationDashboardMapper.toDto(rscIotLatestTotalMouldSaturationRepository.findAllByRscMtPPheaderIdAndM1ValueGreaterThanEqualAndM1ValueLessThanEqual(ppHeaderId, Long.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal()), Long.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }

    @Override
    public List<MouldSaturationDetailsDTO> getMouldSaturationWithLowStatusForCurrentMonth(Long ppHeaderId) {
        return mouldSaturationDashboardMapper.toDto(rscIotLatestTotalMouldSaturationRepository.findAllByRscMtPPheaderIdAndM1ValueLessThan(ppHeaderId, Long.valueOf(SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }

    @Override
    public void deleteAllByPpHeaderId(Long ppHeaderId) {
        rscIotLatestTotalMouldSaturationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public void updateTotalMouldSaturationForUpdatedPurchasePlan(List<PurchasePlanDTO> purchasePlanDTOList, Long ppHeaderId) {
        if (purchasePlanDTOList.size() > 0) {
            List<Long> uniqueMouldsIdList = getAllUniqueMouldsList(purchasePlanDTOList);
            if (uniqueMouldsIdList.size() > 0) {
                List<RscIotLatestMouldSaturationDTO> mouldSaturationDTOList = rscIotLatestMouldSaturationService.getMouldSaturationListByPpHeaderIdAndMouldsList(ppHeaderId, uniqueMouldsIdList);
                if (Optional.ofNullable(mouldSaturationDTOList).isPresent()) {
                    List<RscIotLatestTotalMouldSaturationDTO> latestTotalMouldSaturationDTOList = getLatestTotalMouldSaturationList(mouldSaturationDTOList);
                    latestTotalMouldSaturationDTOList.forEach(rscIotLatestTotalMouldSaturationDTO -> {
                        rscIotLatestTotalMouldSaturationDTO.setId(getIdByMouldAndPpheaderId(rscIotLatestTotalMouldSaturationDTO.getRscDtMouldId(), rscIotLatestTotalMouldSaturationDTO.getRscMtPPheaderId()));
                    });
                    saveAll(latestTotalMouldSaturationDTOList);
//                    mouldWiseSaturationToExcel(ppHeaderId);
                }
            }
        }
    }

    private Long getIdByMouldAndPpheaderId(Long mouldId, Long ppHeaderId) {
        return rscIotLatestTotalMouldSaturationRepository.findByRscDtMouldIdAndRscMtPPheaderId(mouldId, ppHeaderId).getId();
    }

    @Override
    public List<Long> getAllUniqueMouldsList(List<PurchasePlanDTO> purchasePlanDTOList) {
        return purchasePlanDTOList.stream().filter(purchasePlanDTO -> purchasePlanDTO.getMouldId() != null).
                map(PurchasePlanDTO::getMouldId).distinct().collect(Collectors.toList());
    }

    @Override
    public List<RscIotLatestTotalMouldSaturationDTO> findAllByRscMtPpHeaderIdAndMouldsList(Long ppHeaderId, List<Long> uniqueMouldsIdList) {
        return rscIotLatestTotalMouldSaturationMapper.toDto(rscIotLatestTotalMouldSaturationRepository.findAllByRscMtPPheaderIdAndRscDtMouldIdIn(ppHeaderId, uniqueMouldsIdList));
    }
}
