package com.example.validation.service.impl;

import com.example.validation.dto.*;
import com.example.validation.mapper.FgLineSaturationLinesMapper;
import com.example.validation.mapper.RscIotFgLineSaturationSummaryMapper;
import com.example.validation.repository.RscIotFgLineSaturationSummaryRepository;
import com.example.validation.service.RscIotFgLineSaturationService;
import com.example.validation.service.RscIotFgLineSaturationSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.enums.SaturationRange;
import com.example.validation.util.enums.SaturationStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotFgLineSaturationSummaryServiceImpl implements RscIotFgLineSaturationSummaryService {
    private final RscIotFgLineSaturationSummaryRepository rscIotFgLineSaturationSummaryRepository;
    private final RscIotFgLineSaturationSummaryMapper rscIotFgLineSaturationSummaryMapper;
    private final RscIotFgLineSaturationService rscIotFgLineSaturationService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final FgLineSaturationLinesMapper fgLineSaturationLinesMapper;

    public RscIotFgLineSaturationSummaryServiceImpl(RscIotFgLineSaturationSummaryRepository rscIotFgLineSaturationSummaryRepository, RscIotFgLineSaturationSummaryMapper rscIotFgLineSaturationSummaryMapper, RscIotFgLineSaturationService rscIotFgLineSaturationService, RscMtPpheaderService rscMtPpheaderService, FgLineSaturationLinesMapper fgLineSaturationLinesMapper) {
        this.rscIotFgLineSaturationSummaryRepository = rscIotFgLineSaturationSummaryRepository;
        this.rscIotFgLineSaturationSummaryMapper = rscIotFgLineSaturationSummaryMapper;
        this.rscIotFgLineSaturationService = rscIotFgLineSaturationService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.fgLineSaturationLinesMapper = fgLineSaturationLinesMapper;
    }

    @Override
    public void createFgLineSaturationSummary(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            if (rscIotFgLineSaturationSummaryRepository.countByRscMtPPheaderId(pPheaderDTO.get().getId()) == 0) {
                List<RscIotFgLineSaturationDTO> rscIotFgLineSaturationDTOList = rscIotFgLineSaturationService.findAll(ppHeaderId);
                if (Optional.ofNullable(rscIotFgLineSaturationDTOList).isPresent()) {
                    List<RscIotFgLineSaturationSummaryDTO> lineSaturationSummaryDTOList = new ArrayList<>();
                    lineSaturationSummaryDTOList.addAll(getLineSaturationSummary(rscIotFgLineSaturationDTOList));
                    saveAll(lineSaturationSummaryDTOList);
                }
            }
        }
    }

    private void saveAll(List<RscIotFgLineSaturationSummaryDTO> lineSaturationSummaryDTOList) {
        rscIotFgLineSaturationSummaryRepository.saveAll(rscIotFgLineSaturationSummaryMapper.toEntity(lineSaturationSummaryDTOList));
    }

    private List<RscIotFgLineSaturationSummaryDTO> getLineSaturationSummary(List<RscIotFgLineSaturationDTO> rscIotFgLineSaturationDTOList) {
        List<RscIotFgLineSaturationSummaryDTO> lineSaturationSummaryDTOList = new ArrayList<>();
        rscIotFgLineSaturationDTOList.forEach(rscIotFgLineSaturationDTO -> {
            if (checkGreaterThanHighestRangeOrNot(rscIotFgLineSaturationDTO)) {
                lineSaturationSummaryDTOList.add(getLineSaturationSummaryDTO(rscIotFgLineSaturationDTO, SaturationStatus.High));
            } else {
                lineSaturationSummaryDTOList.add(getLineSaturationSummaryDTO(rscIotFgLineSaturationDTO, SaturationStatus.Low));
            }
        });
        return lineSaturationSummaryDTOList;
    }

    private RscIotFgLineSaturationSummaryDTO getLineSaturationSummaryDTO(RscIotFgLineSaturationDTO rscIotFgLineSaturationDTO, SaturationStatus status) {
        RscIotFgLineSaturationSummaryDTO rscIotFgLineSaturationSummaryDTO = new RscIotFgLineSaturationSummaryDTO();
        rscIotFgLineSaturationSummaryDTO.setRscDtLinesId(rscIotFgLineSaturationDTO.getRscDtLinesId());
        rscIotFgLineSaturationSummaryDTO.setRscMtPPheaderId(rscIotFgLineSaturationDTO.getRscMtPPheaderId());
        rscIotFgLineSaturationSummaryDTO.setLineSummaryDate(LocalDate.now());
        if (status.equals(SaturationStatus.High)) {
            rscIotFgLineSaturationSummaryDTO.setSaturationStatus(SaturationStatus.High);
            rscIotFgLineSaturationSummaryDTO.setAvgSaturationPercentage(rscIotFgLineSaturationDTO.getM1LineSaturation());
        } else {
            rscIotFgLineSaturationSummaryDTO.setAvgSaturationPercentage(rscIotFgLineSaturationDTO.getM1LineSaturation());
            if (rscIotFgLineSaturationSummaryDTO.getAvgSaturationPercentage() < SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal()) {
                rscIotFgLineSaturationSummaryDTO.setSaturationStatus(SaturationStatus.Low);
            } else {
                rscIotFgLineSaturationSummaryDTO.setSaturationStatus(SaturationStatus.Medium);
            }
        }
        return rscIotFgLineSaturationSummaryDTO;
    }

    private boolean checkGreaterThanHighestRangeOrNot(RscIotFgLineSaturationDTO rscIotFgLineSaturationDTO) {
        if (rscIotFgLineSaturationDTO.getM1LineSaturation() >= SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<RscIotFgLineSaturationSummaryDTO> getFgLineSaturationsList(Long ppHeaderId) {
        List<RscIotFgLineSaturationSummaryDTO> lineSaturationSummaryDTOList = new ArrayList<>();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            lineSaturationSummaryDTOList.addAll(rscIotFgLineSaturationSummaryMapper.toDto(rscIotFgLineSaturationSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId)));
        }
        return lineSaturationSummaryDTOList;
    }

    @Override
    public FgLineSaturationLineWiseDetailsDTO getSingleLinesDetails(Long ppHeaderId, Long rscDtLinesId) {
        FgLineSaturationLineWiseDetailsDTO fgLineSaturationLineWiseDetailsDTO = new FgLineSaturationLineWiseDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            RscIotFgLineSaturationSummaryDTO rscIotFgLineSaturationSummaryDTO = rscIotFgLineSaturationSummaryMapper.toDto(rscIotFgLineSaturationSummaryRepository.findByRscMtPPheaderIdAndRscDtLinesId(rscMtPPheaderDTO.getId(), rscDtLinesId));
            if (Optional.ofNullable(rscIotFgLineSaturationSummaryDTO).isPresent()) {
                fgLineSaturationLineWiseDetailsDTO.setAvgReceiptPercentage(rscIotFgLineSaturationSummaryDTO.getAvgSaturationPercentage());
                fgLineSaturationLineWiseDetailsDTO.setRscDtLinesId(rscIotFgLineSaturationSummaryDTO.getRscDtLinesId());
                fgLineSaturationLineWiseDetailsDTO.setLineName(rscIotFgLineSaturationSummaryDTO.getLineName());
                fgLineSaturationLineWiseDetailsDTO.setSaturationStatus(rscIotFgLineSaturationSummaryDTO.getSaturationStatus());
            }
            fgLineSaturationLineWiseDetailsDTO.setRscIotFgLineSaturationDTOList(rscIotFgLineSaturationService.findAllByLinesIdAndRscMtPPheaderId(rscDtLinesId, ppHeaderId));
        }
        return fgLineSaturationLineWiseDetailsDTO;
    }

    @Override
    public FgLineSaturationLinesDetailsDTO getFgLineSaturationsLines(Long ppHeaderId) {
        FgLineSaturationLinesDetailsDTO linesDetailsDTO = new FgLineSaturationLinesDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            linesDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
            linesDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            linesDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            linesDetailsDTO.setHighSaturationRangeLinesDTOList(getSaturationLinesList(rscMtPPheaderDTO.getId(), SaturationStatus.High));
            linesDetailsDTO.setMediumSaturationRangeLinesDTOList(getSaturationLinesList(rscMtPPheaderDTO.getId(), SaturationStatus.Medium));
            linesDetailsDTO.setLowSaturationRangeLinesDTOList(getSaturationLinesList(rscMtPPheaderDTO.getId(), SaturationStatus.Low));
            linesDetailsDTO.setHighSaturationRangeListCount(linesDetailsDTO.getHighSaturationRangeLinesDTOList().size());
            linesDetailsDTO.setMediumSaturationRangeListCount(linesDetailsDTO.getMediumSaturationRangeLinesDTOList().size());
            linesDetailsDTO.setLowSaturationRangeListCount(linesDetailsDTO.getLowSaturationRangeLinesDTOList().size());
            linesDetailsDTO.setHighSaturationRange(">= " + SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal() + "%");
            linesDetailsDTO.setMediumSaturationRange(" " + SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal() + "% - " + SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal() + "%");
            linesDetailsDTO.setLowSaturationRange("< " + SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal() + "%");
            linesDetailsDTO.setTotalCount(getTotalCount(linesDetailsDTO.getHighSaturationRangeListCount(), linesDetailsDTO.getMediumSaturationRangeListCount(), linesDetailsDTO.getLowSaturationRangeListCount()));
        }
        return linesDetailsDTO;
    }

    private List<FgLineSaturationLinesDTO> getSaturationLinesList(Long ppHeaderId, Enum status) {
        return fgLineSaturationLinesMapper.toDto(rscIotFgLineSaturationSummaryRepository.findAllByRscMtPPheaderIdAndSaturationStatus(ppHeaderId, status));
    }

    private Integer getTotalCount(Integer highReceiptCount, Integer mediumReceiptCount, Integer lowReceiptCount) {
        return highReceiptCount + mediumReceiptCount + lowReceiptCount;
    }

    @Override
    public List<RscIotFgLineSaturationSummaryDTO> getLinesList(Long rscDtLinesId) {
        return rscIotFgLineSaturationSummaryMapper.toDto(rscIotFgLineSaturationSummaryRepository.findAllByRscDtLinesId(rscDtLinesId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFgLineSaturationSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
