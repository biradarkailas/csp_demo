package com.example.validation.service.impl;

import com.example.validation.dto.RscMtFgMesDTO;
import com.example.validation.mapper.RscMtFgMesMapper;
import com.example.validation.repository.RscMtFgMesRepository;
import com.example.validation.service.RscMtFgMesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtFgMesServiceImpl implements RscMtFgMesService {
    private final RscMtFgMesRepository rscMtFgMesRepository;
    private final RscMtFgMesMapper rscMtFgMesMapper;

    public RscMtFgMesServiceImpl(RscMtFgMesRepository rscMtFgMesRepository, RscMtFgMesMapper rscMtFgMesMapper) {
        this.rscMtFgMesRepository = rscMtFgMesRepository;
        this.rscMtFgMesMapper = rscMtFgMesMapper;
    }

    @Override
    public List<RscMtFgMesDTO> findAll() {
        return rscMtFgMesMapper.toDto( rscMtFgMesRepository.findAll() );
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtFgMesRepository.deleteAllByStockDate(stockDate);
    }
}