package com.example.validation.service.impl;

import com.example.validation.dto.RscMtPmMesRejectedStockDTO;
import com.example.validation.mapper.RscMtPmMesRejectedStockMapper;
import com.example.validation.repository.RscMtPmMesRejectedStockRepository;
import com.example.validation.service.RscMtPmMesRejectedStockService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtPmMesRejectedStockServiceImpl implements RscMtPmMesRejectedStockService {
    private final RscMtPmMesRejectedStockRepository rscMtPmMesRejectedStockRepository;
    private final RscMtPmMesRejectedStockMapper rscMtPmMesRejectedStockMapper;

    public RscMtPmMesRejectedStockServiceImpl(RscMtPmMesRejectedStockRepository rscMtPmMesRejectedStockRepository, RscMtPmMesRejectedStockMapper rscMtPmMesRejectedStockMapper) {
        this.rscMtPmMesRejectedStockRepository = rscMtPmMesRejectedStockRepository;
        this.rscMtPmMesRejectedStockMapper = rscMtPmMesRejectedStockMapper;
    }

    @Override
    public List<RscMtPmMesRejectedStockDTO> findAll() {
        return rscMtPmMesRejectedStockMapper.toDto(rscMtPmMesRejectedStockRepository.findAll());
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtPmMesRejectedStockRepository.deleteAllByStockDate(stockDate);
    }
}