package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.SupplierMaterialsMapper;
import com.example.validation.repository.RscItItemWiseSupplierRepository;
import com.example.validation.service.RscItItemWiseSupplierService;
import com.example.validation.service.SupplierMastersService;
import com.example.validation.util.excel_export.PmSuppliersMasterDataExporter;
import com.example.validation.util.excel_export.RmSuppliersMasterDataExporter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SupplierMastersServiceImpl implements SupplierMastersService {
    private final RscItItemWiseSupplierService rscItItemWiseSupplierService;
    private final SupplierMaterialsMapper supplierMaterialsMapper;
    private final RscItItemWiseSupplierRepository rscItItemWiseSupplierRepository;

    public SupplierMastersServiceImpl(RscItItemWiseSupplierService rscItItemWiseSupplierService, SupplierMaterialsMapper supplierMaterialsMapper, RscItItemWiseSupplierRepository rscItItemWiseSupplierRepository) {
        this.rscItItemWiseSupplierService = rscItItemWiseSupplierService;
        this.supplierMaterialsMapper = supplierMaterialsMapper;
        this.rscItItemWiseSupplierRepository = rscItItemWiseSupplierRepository;

    }

    @Override
    public List<RscDtSupplierDTO> getSuppliersListOfPackingMaterial() {
        List<String> categoryList = new ArrayList<>();
        categoryList.add( MaterialCategoryConstants.PACKING_MATERIAL );
        return rscItItemWiseSupplierService.getSuppliersListByCategory( categoryList );
    }

    @Override
    public List<RscDtSupplierDTO> findSuppliersListOfPackingMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<RscDtSupplierDTO> materialCategoryListResult = rscItItemWiseSupplierRepository.findRscMtItemRscDtItemCategoryPackingMaterialDescriptionIn( paging )
                .map( supplierMaterialsMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles pmSuppliersMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscDtSupplierDTO> rscDtSupplierDTOS = getSuppliersListOfPackingMaterial();
        excelExportFiles.setFile( PmSuppliersMasterDataExporter.pmSuppliersMasterDataToExcel( rscDtSupplierDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.PM_Supplier + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<RscDtSupplierDTO> getSuppliersListOfRawMaterial() {
        List<String> categoryList = new ArrayList<>();
        categoryList.add( MaterialCategoryConstants.RAW_MATERIAL );
        return rscItItemWiseSupplierService.getSuppliersListByCategory( categoryList );
    }

    @Override
    public List<RscDtSupplierDTO> findSuppliersListOfRawMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<RscDtSupplierDTO> materialCategoryListResult = rscItItemWiseSupplierRepository.findByRscMtItemRscDtItemCategoryRawMaterialDescriptionIn( paging )
                .map( supplierMaterialsMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    public ExcelExportFiles rmSuppliersMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscDtSupplierDTO> rscDtSupplierDTOS = getSuppliersListOfRawMaterial();
        excelExportFiles.setFile( RmSuppliersMasterDataExporter.rmSuppliersMasterDataToExcel( rscDtSupplierDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.RM_Supplier + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }
}
