package com.example.validation.service.impl;

import com.example.validation.dto.RscDtSignatureDTO;
import com.example.validation.mapper.RscDtSignatureMapper;
import com.example.validation.repository.RscDtSignatureRepository;
import com.example.validation.service.RscDtSignatureService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtSignatureServiceImpl implements RscDtSignatureService {
    private final RscDtSignatureRepository rscDtSignatureRepository;
    private final RscDtSignatureMapper rscDtSignatureMapper;

    public RscDtSignatureServiceImpl(RscDtSignatureRepository rscDtSignatureRepository, RscDtSignatureMapper rscDtSignatureMapper) {
        this.rscDtSignatureRepository = rscDtSignatureRepository;
        this.rscDtSignatureMapper = rscDtSignatureMapper;
    }

    @Override
    public List<RscDtSignatureDTO> findAll() {
        return rscDtSignatureMapper.toDto(rscDtSignatureRepository.findAll());
    }
}