package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtItemTypeDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.MaterialCategoryMasterService;
import com.example.validation.service.RscDtItemTypeService;
import com.example.validation.service.RscMtItemService;
import com.example.validation.util.excel_export.PmMaterialCategoryDataExporter;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MaterialCategoryMasterServiceImpl implements MaterialCategoryMasterService {
    private final RscMtItemService rscMtItemService;
    private final RscDtItemTypeService rscDtItemTypeService;

    public MaterialCategoryMasterServiceImpl(RscMtItemService rscMtItemService, RscDtItemTypeService rscDtItemTypeService) {
        this.rscMtItemService = rscMtItemService;
        this.rscDtItemTypeService = rscDtItemTypeService;
    }

    @Override
    public List<RscDtItemTypeDTO> getMaterialCategoryListOfPackingMaterial() {
        return rscMtItemService.getMaterialCategoryMastersListByCategory(MaterialCategoryConstants.PACKING_MATERIAL);
    }

    public ExcelExportFiles pmMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscDtItemTypeDTO> rscDtItemTypeDTOS = getMaterialCategoryListOfPackingMaterial();
        excelExportFiles.setFile(PmMaterialCategoryDataExporter.pmMaterialCategoryDataToExcel(rscDtItemTypeDTOS, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.Material_Category + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<RscDtItemTypeDTO> getMaterialCategoryListOfPackingMaterialWithPagination(Integer pageNo, Integer pageSize) {
        return rscDtItemTypeService.findAllWithPagination(pageNo, pageSize);
    }
}