package com.example.validation.service.impl;

import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DeleteOldPlansServiceImpl implements DeleteOldPlansService {
    private final RscIitMpsForecastService rscIitMpsForecastService;
    private final RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService;
    private final RscIotFgLineSaturationService rscIotFgLineSaturationService;
    private final RscIotFgLineSaturationSummaryService rscIotFgLineSaturationSummaryService;
    private final RscIotFgSkidSaturationService rscIotFgSkidSaturationService;
    private final RscIotFgSkidSaturationSummaryService rscIotFgSkidSaturationSummaryService;
    private final RscIotFGSlobService rscIotFGSlobService;
    private final RscIotFGSlobSummaryService rscIotFGSlobSummaryService;
    private final RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService;
    private final RscIotFGTypeSlobSummaryService rscIotFGTypeSlobSummaryService;
    private final RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService;
    private final RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService;
    private final RscIotLatestTotalMouldSaturationSummaryService rscIotLatestTotalMouldSaturationSummaryService;
    private final RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService;
    private final RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService;
    private final RscIotPmCoverDaysService rscIotPmCoverDaysService;
    private final RscIotPmCoverDaysSummaryService rscIotPmCoverDaysSummaryService;
    private final RscIotPMLatestPurchasePlanService rscIotPMLatestPurchasePlanService;
    private final RscIotPmOtifCalculationService rscIotPmOtifCalculationService;
    private final RscIotPmOtifSummaryService rscIotPmOtifSummaryService;
    private final RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService;
    private final RscIotPMPurchasePlanCoverDaysService rscIotPMPurchasePlanCoverDaysService;
    private final RscIotPMPurchasePlanService rscIotPMPurchasePlanService;
    private final RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService;
    private final RscIotRmCoverDaysService rscIotRmCoverDaysService;
    private final RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService;
    private final RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService;
    private final RscIotRMPurchasePlanCoverDaysService rscIotRMPurchasePlanCoverDaysService;
    private final RscIotRMPurchasePlanService rscIotRMPurchasePlanService;
    private final RscIotRMSlobService rscIotRMSlobService;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;
    private final RscItBPGrossConsumptionService rscItBPGrossConsumptionService;
    private final RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService;
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final RscItPMLogicalConsumptionService rscItPMLogicalConsumptionService;
    private final RscItPMSlobService rscItPMSlobService;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService;
    private final RscItRMPivotConsumptionService rscItRMPivotConsumptionService;
    private final RscItSubBaseGrossConsumptionService rscItSubBaseGrossConsumptionService;
    private final RscItSubBaseLogicalConsumptionService rscItSubBaseLogicalConsumptionService;
    private final RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService;
    private final RscItWIPLogicalConsumptionService rscItWIPLogicalConsumptionService;
    private final RscMtBomService rscMtBomService;
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final ExecutionErrorLogService executionErrorLogService;
    private final RscIotPmSlobSummaryService rscIotPmSlobSummaryService;
    private final RscIitRmBulkOpRmService rscIitRmBulkOpRmService;
    private final RscIotFGStockService rscIotFGStockService;
    private final RscIotPmOtifReceiptsService rscIotPmOtifReceiptsService;
    private final RscItPMStockService rscItPMStockService;
    private final RscIotRmStockService rscIotRmStockService;
    private final RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService;
    private final RscMtPmMesService rscMtPmMesService;
    private final RscMtPmRtdService rscMtPmRtdService;
    private final RscMtRmListService rscMtRmListService;
    private final RscMtRmMesService rscMtRmMesService;
    private final RscMtRmOpenPoService rscMtRmOpenPoService;
    private final RscMtRmOtherStocksService rscMtRmOtherStocksService;
    private final RscMtRmRtdService rscMtRmRtdService;
    private final RscMtPmMesRejectedStockService rscMtPmMesRejectedStockService;
    private final RscMtFgMesService rscMtFgMesService;
    private final RscMtBulkMesService rscMtBulkMesService;
    private final RscMtPmOtifReceiptsService rscMtPmOtifReceiptsService;
    private final RscMtRmMesRejectedStockService rscMtRmMesRejectedStockService;
    private final RscIitIdpStatusService rscIitIdpStatusService;

    public DeleteOldPlansServiceImpl(RscIitMpsForecastService rscIitMpsForecastService, RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService, RscIotFgLineSaturationService rscIotFgLineSaturationService, RscIotFgLineSaturationSummaryService rscIotFgLineSaturationSummaryService, RscIotFgSkidSaturationService rscIotFgSkidSaturationService, RscIotFgSkidSaturationSummaryService rscIotFgSkidSaturationSummaryService, RscIotFGSlobService rscIotFGSlobService, RscIotFGSlobSummaryService rscIotFGSlobSummaryService, RscIotFGTypeDivisionSlobSummaryService rscIotFGTypeDivisionSlobSummaryService, RscIotFGTypeSlobSummaryService rscIotFGTypeSlobSummaryService, RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService, RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService, RscIotLatestTotalMouldSaturationSummaryService rscIotLatestTotalMouldSaturationSummaryService, RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService, RscIotOriginalTotalMouldSaturationService rscIotOriginalTotalMouldSaturationService, RscIotPmCoverDaysService rscIotPmCoverDaysService, RscIotPmCoverDaysSummaryService rscIotPmCoverDaysSummaryService, RscIotPMLatestPurchasePlanService rscIotPMLatestPurchasePlanService, RscIotPmOtifCalculationService rscIotPmOtifCalculationService, RscIotPmOtifSummaryService rscIotPmOtifSummaryService, RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService, RscIotPMPurchasePlanCoverDaysService rscIotPMPurchasePlanCoverDaysService, RscIotPMPurchasePlanService rscIotPMPurchasePlanService, RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService, RscIotRmCoverDaysService rscIotRmCoverDaysService, RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService, RscIotRMPurchasePlanCalculationsService rscIotRMPurchasePlanCalculationsService, RscIotRMPurchasePlanCoverDaysService rscIotRMPurchasePlanCoverDaysService, RscIotRMPurchasePlanService rscIotRMPurchasePlanService, RscIotRMSlobService rscIotRMSlobService, RscIotRmSlobSummaryService rscIotRmSlobSummaryService, RscItBPGrossConsumptionService rscItBPGrossConsumptionService, RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService, RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService, RscItPMGrossConsumptionService rscItPMGrossConsumptionService, RscItPMLogicalConsumptionService rscItPMLogicalConsumptionService, RscItPMSlobService rscItPMSlobService, RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService, RscItRMPivotConsumptionService rscItRMPivotConsumptionService, RscItSubBaseGrossConsumptionService rscItSubBaseGrossConsumptionService, RscItSubBaseLogicalConsumptionService rscItSubBaseLogicalConsumptionService, RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService, RscItWIPLogicalConsumptionService rscItWIPLogicalConsumptionService, RscMtBomService rscMtBomService, RscMtPpdetailsService rscMtPpdetailsService, RscMtPpheaderService rscMtPpheaderService, ExecutionErrorLogService executionErrorLogService, RscIotPmSlobSummaryService rscIotPmSlobSummaryService, RscIitRmBulkOpRmService rscIitRmBulkOpRmService, RscIotFGStockService rscIotFGStockService, RscIotPmOtifReceiptsService rscIotPmOtifReceiptsService, RscItPMStockService rscItPMStockService, RscIotRmStockService rscIotRmStockService, RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService, RscMtPmMesService rscMtPmMesService, RscMtPmRtdService rscMtPmRtdService, RscMtRmListService rscMtRmListService, RscMtRmMesService rscMtRmMesService, RscMtRmOpenPoService rscMtRmOpenPoService, RscMtRmOtherStocksService rscMtRmOtherStocksService, RscMtRmRtdService rscMtRmRtdService, RscMtPmMesRejectedStockService rscMtPmMesRejectedStockService, RscMtFgMesService rscMtFgMesService, RscMtBulkMesService rscMtBulkMesService, RscMtPmOtifReceiptsService rscMtPmOtifReceiptsService, RscMtRmMesRejectedStockService rscMtRmMesRejectedStockService, RscIitIdpStatusService rscIitIdpStatusService) {
        this.rscIitMpsForecastService = rscIitMpsForecastService;
        this.rscIitMpsWorkingDaysService = rscIitMpsWorkingDaysService;
        this.rscIotFgLineSaturationService = rscIotFgLineSaturationService;
        this.rscIotFgLineSaturationSummaryService = rscIotFgLineSaturationSummaryService;
        this.rscIotFgSkidSaturationService = rscIotFgSkidSaturationService;
        this.rscIotFgSkidSaturationSummaryService = rscIotFgSkidSaturationSummaryService;
        this.rscIotFGSlobService = rscIotFGSlobService;
        this.rscIotFGSlobSummaryService = rscIotFGSlobSummaryService;
        this.rscIotFGTypeDivisionSlobSummaryService = rscIotFGTypeDivisionSlobSummaryService;
        this.rscIotFGTypeSlobSummaryService = rscIotFGTypeSlobSummaryService;
        this.rscIotLatestMouldSaturationService = rscIotLatestMouldSaturationService;
        this.rscIotLatestTotalMouldSaturationService = rscIotLatestTotalMouldSaturationService;
        this.rscIotLatestTotalMouldSaturationSummaryService = rscIotLatestTotalMouldSaturationSummaryService;
        this.rscIotOriginalMouldSaturationService = rscIotOriginalMouldSaturationService;
        this.rscIotOriginalTotalMouldSaturationService = rscIotOriginalTotalMouldSaturationService;
        this.rscIotPmCoverDaysService = rscIotPmCoverDaysService;
        this.rscIotPmCoverDaysSummaryService = rscIotPmCoverDaysSummaryService;
        this.rscIotPMLatestPurchasePlanService = rscIotPMLatestPurchasePlanService;
        this.rscIotPmOtifCalculationService = rscIotPmOtifCalculationService;
        this.rscIotPmOtifSummaryService = rscIotPmOtifSummaryService;
        this.rscIotPmPurchasePlanCalculationsService = rscIotPmPurchasePlanCalculationsService;
        this.rscIotPMPurchasePlanCoverDaysService = rscIotPMPurchasePlanCoverDaysService;
        this.rscIotPMPurchasePlanService = rscIotPMPurchasePlanService;
        this.rscIotRmCoverDaysClassSummaryService = rscIotRmCoverDaysClassSummaryService;
        this.rscIotRmCoverDaysService = rscIotRmCoverDaysService;
        this.rscIotRmCoverDaysSummaryService = rscIotRmCoverDaysSummaryService;
        this.rscIotRMPurchasePlanCalculationsService = rscIotRMPurchasePlanCalculationsService;
        this.rscIotRMPurchasePlanCoverDaysService = rscIotRMPurchasePlanCoverDaysService;
        this.rscIotRMPurchasePlanService = rscIotRMPurchasePlanService;
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
        this.rscItBPGrossConsumptionService = rscItBPGrossConsumptionService;
        this.rscItBPLogicalConsumptionService = rscItBPLogicalConsumptionService;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
        this.rscItBulkLogicalConsumptionService = rscItBulkLogicalConsumptionService;
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.rscItPMLogicalConsumptionService = rscItPMLogicalConsumptionService;
        this.rscItPMSlobService = rscItPMSlobService;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rscItRMLogicalConsumptionService = rscItRMLogicalConsumptionService;
        this.rscItRMPivotConsumptionService = rscItRMPivotConsumptionService;
        this.rscItSubBaseGrossConsumptionService = rscItSubBaseGrossConsumptionService;
        this.rscItSubBaseLogicalConsumptionService = rscItSubBaseLogicalConsumptionService;
        this.rscItWIPGrossConsumptionService = rscItWIPGrossConsumptionService;
        this.rscItWIPLogicalConsumptionService = rscItWIPLogicalConsumptionService;
        this.rscMtBomService = rscMtBomService;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.executionErrorLogService = executionErrorLogService;
        this.rscIotPmSlobSummaryService = rscIotPmSlobSummaryService;
        this.rscIitRmBulkOpRmService = rscIitRmBulkOpRmService;
        this.rscIotFGStockService = rscIotFGStockService;
        this.rscIotPmOtifReceiptsService = rscIotPmOtifReceiptsService;
        this.rscItPMStockService = rscItPMStockService;
        this.rscIotRmStockService = rscIotRmStockService;
        this.rscMtOtifPmReceiptsHeaderService = rscMtOtifPmReceiptsHeaderService;
        this.rscMtPmMesService = rscMtPmMesService;
        this.rscMtPmRtdService = rscMtPmRtdService;
        this.rscMtRmListService = rscMtRmListService;
        this.rscMtRmMesService = rscMtRmMesService;
        this.rscMtRmOpenPoService = rscMtRmOpenPoService;
        this.rscMtRmOtherStocksService = rscMtRmOtherStocksService;
        this.rscMtRmRtdService = rscMtRmRtdService;
        this.rscMtPmMesRejectedStockService = rscMtPmMesRejectedStockService;
        this.rscMtFgMesService = rscMtFgMesService;
        this.rscMtBulkMesService = rscMtBulkMesService;
        this.rscMtPmOtifReceiptsService = rscMtPmOtifReceiptsService;
        this.rscMtRmMesRejectedStockService = rscMtRmMesRejectedStockService;
        this.rscIitIdpStatusService = rscIitIdpStatusService;
    }

    @Override
    public void deleteAllOldPlans(LocalDate mpsDate) {
        List<RscMtPPheaderDTO> beforeOneYearPlans = rscMtPpheaderService.getBeforeOneYearPlans(mpsDate);
        if (Optional.ofNullable(beforeOneYearPlans).isPresent()) {
            for (RscMtPPheaderDTO rscMtPPheaderDTO : beforeOneYearPlans) {
                deleteAllFGModuleData(rscMtPPheaderDTO.getId());
                deleteAllPMModuleData(rscMtPPheaderDTO.getId());
                deleteAllRMModuleData(rscMtPPheaderDTO.getId());
                deleteAllConsumptionData(rscMtPPheaderDTO.getId());
                deleteAllMasterData(rscMtPPheaderDTO.getId());
                deleteAllStockData(rscMtPPheaderDTO.getMpsDate());
                deleteAllOTIFData(rscMtPPheaderDTO.getOtifDate());
            }
        }
        rscIitIdpStatusService.deleteAll(mpsDate.minusMonths(1));
    }

    private void deleteAllFGModuleData(Long mpsId) {
        rscIitMpsWorkingDaysService.deleteAll(mpsId);
        rscIitMpsForecastService.deleteAll(mpsId);
        rscIotFgLineSaturationService.deleteAll(mpsId);
        rscIotFgLineSaturationSummaryService.deleteAll(mpsId);
        rscIotFgSkidSaturationService.deleteAll(mpsId);
        rscIotFgSkidSaturationSummaryService.deleteAll(mpsId);
        rscIotFGSlobService.deleteAll(mpsId);
        rscIotFGSlobSummaryService.deleteAll(mpsId);
        rscIotFGTypeDivisionSlobSummaryService.deleteAll(mpsId);
        rscIotFGTypeSlobSummaryService.deleteAll(mpsId);
    }

    private void deleteAllConsumptionData(Long mpsId) {
        rscItWIPLogicalConsumptionService.deleteAll(mpsId);
        rscItWIPGrossConsumptionService.deleteAll(mpsId);
        rscItPMLogicalConsumptionService.deleteAll(mpsId);
        rscItPMGrossConsumptionService.deleteAll(mpsId);
        rscItBulkLogicalConsumptionService.deleteAll(mpsId);
        rscItBulkGrossConsumptionService.deleteAll(mpsId);
        rscItBPLogicalConsumptionService.deleteAll(mpsId);
        rscItBPGrossConsumptionService.deleteAll(mpsId);
        rscItSubBaseLogicalConsumptionService.deleteAll(mpsId);
        rscItSubBaseGrossConsumptionService.deleteAll(mpsId);
        rscItRMLogicalConsumptionService.deleteConsumption(mpsId);
        rscItRMGrossConsumptionService.deleteAll(mpsId);
        rscItRMPivotConsumptionService.deleteAll(mpsId);
    }

    private void deleteAllPMModuleData(Long mpsId) {
        rscIotLatestTotalMouldSaturationSummaryService.deleteAll(mpsId);
        rscIotLatestTotalMouldSaturationService.deleteAllByPpHeaderId(mpsId);
        rscIotLatestMouldSaturationService.deleteAllByPpHeaderId(mpsId);
        rscIotOriginalMouldSaturationService.deleteAll(mpsId);
        rscIotOriginalTotalMouldSaturationService.deleteAll(mpsId);
        rscIotPMPurchasePlanCoverDaysService.deleteAll(mpsId);
        rscIotPmCoverDaysService.deleteById(mpsId);
        rscIotPmCoverDaysSummaryService.deleteAllById(mpsId);
        rscIotPmOtifCalculationService.deleteAll(mpsId);
        rscIotPmOtifSummaryService.deleteAll(mpsId);
        rscIotPmSlobSummaryService.deleteById(mpsId);
        rscItPMSlobService.deleteAll(mpsId);
        rscIotPMPurchasePlanService.deleteAll(mpsId);
        rscIotPMLatestPurchasePlanService.deleteAll(mpsId);
        rscIotPmPurchasePlanCalculationsService.deleteAll(mpsId);
    }

    private void deleteAllRMModuleData(Long mpsId) {
        rscIotRmCoverDaysService.deleteById(mpsId);
        rscIotRmCoverDaysSummaryService.deleteAllById(mpsId);
        rscIotRmCoverDaysClassSummaryService.deleteClassSummaryById(mpsId);
        rscIotRMPurchasePlanCoverDaysService.deleteAll(mpsId);
        rscIotRMSlobService.deleteAll(mpsId);
        rscIotRmSlobSummaryService.deleteById(mpsId);
        rscIotRMPurchasePlanService.deleteAll(mpsId);
        rscIotRMPurchasePlanCalculationsService.deleteAll(mpsId);
        rscMtRmOpenPoService.deleteAll(mpsId);
    }

    private void deleteAllMasterData(Long mpsId) {
        rscMtBomService.deleteAll(mpsId);
        rscMtPpdetailsService.deleteAll(mpsId);
        executionErrorLogService.deleteAll(mpsId);
        rscMtPpheaderService.deleteById(mpsId);
    }

    private void deleteAllStockData(LocalDate mpsDate) {
        LocalDate stockDate = mpsDate.withDayOfMonth(1);
        rscIitRmBulkOpRmService.deleteAll(stockDate);
        rscIotFGStockService.deleteAll(stockDate);
        rscItPMStockService.deleteAll(stockDate);
        rscIotRmStockService.deleteAll(stockDate);
        rscMtPmMesService.deleteAll(stockDate);
        rscMtPmRtdService.deleteAll(stockDate);
        rscMtRmListService.deleteAll(mpsDate);
        rscMtRmMesService.deleteAll(stockDate);
        rscMtRmOtherStocksService.deleteAll(stockDate);
        rscMtRmRtdService.deleteAll(stockDate);
        rscMtPmMesRejectedStockService.deleteAll(stockDate);
        rscMtFgMesService.deleteAll(stockDate);
        rscMtBulkMesService.deleteAll(stockDate);
        rscMtRmMesRejectedStockService.deleteAll(stockDate);
    }

    private void deleteAllOTIFData(LocalDate otifDate) {
        List<Long> allIds = rscMtOtifPmReceiptsHeaderService.getAllIds(otifDate);
        if (Optional.ofNullable(allIds).isPresent()) {
            rscIotPmOtifReceiptsService.deleteAll(allIds);
            rscMtPmOtifReceiptsService.deleteAll(allIds);
            rscMtOtifPmReceiptsHeaderService.deleteAll(allIds);
        }
    }
}