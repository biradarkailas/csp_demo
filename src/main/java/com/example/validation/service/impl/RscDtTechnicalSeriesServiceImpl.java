package com.example.validation.service.impl;

import com.example.validation.dto.RscDtTechnicalSeriesDTO;
import com.example.validation.mapper.RscDtTechnicalSeriesMapper;
import com.example.validation.repository.RscDtTechnicalSeriesRepository;
import com.example.validation.service.RscDtTechnicalSeriesService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtTechnicalSeriesServiceImpl implements RscDtTechnicalSeriesService {
    private final RscDtTechnicalSeriesRepository rscDtTechnicalSeriesRepository;
    private final RscDtTechnicalSeriesMapper rscDtTechnicalSeriesMapper;

    public RscDtTechnicalSeriesServiceImpl(RscDtTechnicalSeriesRepository rscDtTechnicalSeriesRepository, RscDtTechnicalSeriesMapper rscDtTechnicalSeriesMapper){
        this.rscDtTechnicalSeriesRepository = rscDtTechnicalSeriesRepository;
        this.rscDtTechnicalSeriesMapper = rscDtTechnicalSeriesMapper;
    }

    @Override
    public List<RscDtTechnicalSeriesDTO> findAll() {
        return rscDtTechnicalSeriesMapper.toDto(rscDtTechnicalSeriesRepository.findAll());
    }
}
