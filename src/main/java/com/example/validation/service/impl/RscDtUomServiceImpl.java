package com.example.validation.service.impl;

import com.example.validation.dto.RscDtUomDTO;
import com.example.validation.mapper.RscDtUomMapper;
import com.example.validation.repository.RscDtUomRepository;
import com.example.validation.service.RscDtUomService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtUomServiceImpl implements RscDtUomService {
    private final RscDtUomRepository rscDtUomRepository;
    private final RscDtUomMapper rscDtUomMapper;

    public RscDtUomServiceImpl(RscDtUomRepository rscDtUomRepository, RscDtUomMapper rscDtUomMapper){
        this.rscDtUomRepository = rscDtUomRepository;
        this.rscDtUomMapper = rscDtUomMapper;
    }

    @Override
    public List<RscDtUomDTO> findAll() {
        return rscDtUomMapper.toDto(rscDtUomRepository.findAll());
    }
}
