package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.domain.RscIotPmCoverDays;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.PmCoverDaysMapper;
import com.example.validation.mapper.RscIotPmCoverDaysMapper;
import com.example.validation.repository.RscIotPmCoverDaysRepository;
import com.example.validation.service.*;
import com.example.validation.util.CoverDaysMonthUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.excel_export.PmCoverDaysExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotPmCoverDaysServiceImpl implements RscIotPmCoverDaysService {
    private final RscIotPmCoverDaysMapper rscIotPmCoverDaysMapper;
    private final RscIotPmCoverDaysRepository rscIotPmCoverDaysRepository;
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotPmCoverDaysSummaryService rscIotPmCoverDaysSummaryService;
    private final CoverDaysMonthUtils coverDaysMonthUtils;
    private final PmCoverDaysMapper pmCoverDaysMapper;
    private final RscItPMSlobService rscItPMSlobService;
    private final RscIotPmSlobSummaryService rscIotPmSlobSummaryService;

    public RscIotPmCoverDaysServiceImpl(RscIotPmCoverDaysMapper rscIotPmCoverDaysMapper, RscIotPmCoverDaysRepository rscIotPmCoverDaysRepository, RscItPMGrossConsumptionService rscItPMGrossConsumptionService, RscMtPpheaderService rscMtPpheaderService, RscIotPmCoverDaysSummaryService rscIotPmCoverDaysSummaryService, CoverDaysMonthUtils coverDaysMonthUtils, PmCoverDaysMapper pmCoverDaysMapper, RscItPMSlobService rscItPMSlobService, RscIotPmSlobSummaryService rscIotPmSlobSummaryService) {
        this.rscIotPmCoverDaysMapper = rscIotPmCoverDaysMapper;
        this.rscIotPmCoverDaysRepository = rscIotPmCoverDaysRepository;
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotPmCoverDaysSummaryService = rscIotPmCoverDaysSummaryService;
        this.coverDaysMonthUtils = coverDaysMonthUtils;
        this.pmCoverDaysMapper = pmCoverDaysMapper;
        this.rscItPMSlobService = rscItPMSlobService;
        this.rscIotPmSlobSummaryService = rscIotPmSlobSummaryService;
    }

    public void calculateRscIotPmCoverDays(Long rscMtPPHeaderId) {
        if (rscIotPmCoverDaysRepository.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
            List<RscIotPmCoverDaysDTO> rscIotPmCoverDaysDTO = new ArrayList<>();
            Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
            if (pPheaderDTO.isPresent()) {
//                List<RscItPMStockDTO> rscIotPmStockDtoList = rscItPMStockService.findAll().stream().filter(rscItPMStockDTO -> DateUtils.isEqualMonthAndYear(rscItPMStockDTO.getStockDate(), pPheaderDTO.get().getMpsDate())).collect(Collectors.toList());
                List<RscItPMSlobDTO> rscItPMSlobDTOList = rscItPMSlobService.getRscItPMSlobByRscMTPPHeader(pPheaderDTO.get().getId());
                Double Rate;
                if (Optional.ofNullable(rscItPMSlobDTOList).isPresent()) {
                    for (RscItPMSlobDTO pmSlobDTO : rscItPMSlobDTOList) {
                        Rate = null;
                        RscItGrossConsumptionDTO pmGrossConsumptionDto = rscItPMGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(rscMtPPHeaderId, pmSlobDTO.getRscMtItemId());
                          RscIotPmCoverDaysDTO rscIotPmCoverDays = new RscIotPmCoverDaysDTO();
                            rscIotPmCoverDays.setRscMtItemId(pmSlobDTO.getRscMtItemId());
                            rscIotPmCoverDays.setRscMtPPHeaderId(rscMtPPHeaderId);
                            if (Optional.ofNullable(pmSlobDTO.getMapPrice()).isPresent()) {
                                rscIotPmCoverDays.setMap(DoubleUtils.getFormattedValue(pmSlobDTO.getMapPrice()));
                                Rate = pmSlobDTO.getMapPrice();
                            }
                            if (Optional.ofNullable(pmSlobDTO.getStdPrice()).isPresent()) {
                                Rate = pmSlobDTO.getStdPrice();
                                rscIotPmCoverDays.setStdPrice(DoubleUtils.getFormattedValue(pmSlobDTO.getStdPrice()));
                            }
                            if (Optional.ofNullable(pmSlobDTO.getMidNightStockQuantity()).isPresent()) {
                                rscIotPmCoverDays.setStockQuantity(DoubleUtils.getFormattedValue(pmSlobDTO.getMidNightStockQuantity()));
                                if (Optional.ofNullable(Rate).isPresent()) {
                                    rscIotPmCoverDays.setStockValue(DoubleUtils.getFormattedValue(pmSlobDTO.getMidNightStockQuantity() * Rate));
                                    rscIotPmCoverDays.setRate(Rate);
                                }
                            }
                        if (Optional.ofNullable(Rate).isPresent()) {
                            rscIotPmCoverDays.setRate(Rate);
                        }
                            if (Optional.ofNullable(pmGrossConsumptionDto).isPresent()) {
                                if (Optional.ofNullable(pmGrossConsumptionDto.getItemWiseSupplierId()).isPresent()) {
                                    rscIotPmCoverDays.setRscItItemWiseSupplierId(pmGrossConsumptionDto.getItemWiseSupplierId());
                                }
                                rscIotPmCoverDays.setRscItPMGrossConsumptionId(pmGrossConsumptionDto.getId());
                                if (Optional.ofNullable(Rate).isPresent()) {
                                    rscIotPmCoverDays.setMonth1ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM1Value(), Rate));
                                    rscIotPmCoverDays.setMonth2ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM2Value(), Rate));
                                    rscIotPmCoverDays.setMonth4ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM4Value(), Rate));
                                    rscIotPmCoverDays.setMonth5ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM5Value(), Rate));
                                    rscIotPmCoverDays.setMonth6ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM6Value(), Rate));
                                    rscIotPmCoverDays.setMonth3ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM3Value(), Rate));
                                    rscIotPmCoverDays.setMonth7ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM7Value(), Rate));
                                    rscIotPmCoverDays.setMonth8ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM8Value(), Rate));
                                    rscIotPmCoverDays.setMonth9ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM9Value(), Rate));
                                    rscIotPmCoverDays.setMonth10ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM10Value(), Rate));
                                    rscIotPmCoverDays.setMonth11ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM11Value(), Rate));
                                    rscIotPmCoverDays.setMonth12ConsumptionValue(getMonthDetail(pmGrossConsumptionDto.getM12Value(), Rate));
                                }
                            }else{
                                rscIotPmCoverDays=  setAllMonthsConsumptionValuesTOzero(rscIotPmCoverDays);
                            }
                           /* if (Rate != 0.0) {
                                rscIotPmCoverDays.setRate(Rate);
                            } else {
                                rscIotPmCoverDays.setRate(null);
                            }*/
                            rscIotPmCoverDaysDTO.add(rscIotPmCoverDays);
//                        }
//                        }
                    }
                }
                saveAll(rscIotPmCoverDaysDTO);
            }
        }
    }
    private RscIotPmCoverDaysDTO setAllMonthsConsumptionValuesTOzero( RscIotPmCoverDaysDTO rscIotPmCoverDays){
        rscIotPmCoverDays.setMonth1ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth2ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth3ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth4ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth5ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth6ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth7ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth8ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth9ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth10ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth11ConsumptionValue(0.0);
        rscIotPmCoverDays.setMonth12ConsumptionValue(0.0);
        return rscIotPmCoverDays;
    }
    private Double getMonthDetail(Double monthConsumptionQuantity, Double stdPrice) {
        return DoubleUtils.round(monthConsumptionQuantity * stdPrice);
    }

    @Override
    public void saveAll(List<RscIotPmCoverDaysDTO> rscIotPmCoverDaysDtoList) {
        rscIotPmCoverDaysRepository.saveAll(rscIotPmCoverDaysMapper.toEntity(rscIotPmCoverDaysDtoList));
    }

    @Override
    public void calculateRscIotPmCoverDaysSummary(Long rscMtPPHeaderId) {
        if (rscIotPmCoverDaysSummaryService.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
            RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO = new RscIotPmCoverDaysSummaryDTO();
            List<RscIotPmCoverDaysDTO> rscItPmCoverDaysList = getRscItPmCoverDaysList(rscMtPPHeaderId);
            rscIotPmCoverDaysSummaryDTO.setRscMtPPHeaderId(rscMtPPHeaderId);
            Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
            if (Optional.ofNullable(rscItPmCoverDaysList).isPresent()) {
                RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(rscMtPPHeaderId);
                Double sitValue = 0.0;
                if (Optional.ofNullable(rscIotPmSlobSummaryDTO).isPresent()) {
                    sitValue = rscIotPmSlobSummaryDTO.getSitValue();
                }
                Double totalStockValue = 0.0;
                Double totalStockSitValue = 0.0;
                Double[] monthsConsumptionTotalValue = new Double[12];
                for (int i = 0; i < monthsConsumptionTotalValue.length; i++) {
                    monthsConsumptionTotalValue[i] = 0.0;
                }
                Integer[] monthSCoverDays = new Integer[12];
                for (int i = 0; i < monthSCoverDays.length; i++) {
                    monthSCoverDays[i] = 0;
                }
                for (RscIotPmCoverDaysDTO rscItPmCoverDaysDto : rscItPmCoverDaysList) {
                    if (rscItPmCoverDaysDto.getStockValue() != null) {
                        totalStockValue += rscItPmCoverDaysDto.getStockValue();
                    }
                    monthsConsumptionTotalValue[0] += rscItPmCoverDaysDto.getMonth1ConsumptionValue();
                    monthsConsumptionTotalValue[1] += rscItPmCoverDaysDto.getMonth2ConsumptionValue();
                    monthsConsumptionTotalValue[2] += rscItPmCoverDaysDto.getMonth3ConsumptionValue();
                    monthsConsumptionTotalValue[3] += rscItPmCoverDaysDto.getMonth4ConsumptionValue();
                    monthsConsumptionTotalValue[4] += rscItPmCoverDaysDto.getMonth5ConsumptionValue();
                    monthsConsumptionTotalValue[5] += rscItPmCoverDaysDto.getMonth6ConsumptionValue();
                    monthsConsumptionTotalValue[6] += rscItPmCoverDaysDto.getMonth7ConsumptionValue();
                    monthsConsumptionTotalValue[7] += rscItPmCoverDaysDto.getMonth8ConsumptionValue();
                    monthsConsumptionTotalValue[8] += rscItPmCoverDaysDto.getMonth9ConsumptionValue();
                    monthsConsumptionTotalValue[9] += rscItPmCoverDaysDto.getMonth10ConsumptionValue();
                    monthsConsumptionTotalValue[10] += rscItPmCoverDaysDto.getMonth11ConsumptionValue();
                    monthsConsumptionTotalValue[11] += rscItPmCoverDaysDto.getMonth12ConsumptionValue();
                }
                if (totalStockValue != null) {
                    totalStockSitValue = totalStockValue + sitValue;
                }
                List coverDaysCalculatedList = coverDaysMonthUtils.getCoverDaysCalculation(totalStockSitValue, monthsConsumptionTotalValue, monthSCoverDays, pPheaderDTO);
                totalStockSitValue = (Double) coverDaysCalculatedList.get(0);
                monthSCoverDays = (Integer[]) coverDaysCalculatedList.get(1);
                rscIotPmCoverDaysSummaryDTO.setTotalStockValue(DoubleUtils.round(totalStockValue));
                rscIotPmCoverDaysSummaryDTO.setSitValue(DoubleUtils.round(sitValue));
                rscIotPmCoverDaysSummaryDTO.setTotalStockSitValue(DoubleUtils.round(totalStockSitValue));
                rscIotPmCoverDaysSummaryDTO.setMonth1ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[0]));
                rscIotPmCoverDaysSummaryDTO.setMonth2ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[1]));
                rscIotPmCoverDaysSummaryDTO.setMonth3ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[2]));
                rscIotPmCoverDaysSummaryDTO.setMonth4ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[3]));
                rscIotPmCoverDaysSummaryDTO.setMonth5ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[4]));
                rscIotPmCoverDaysSummaryDTO.setMonth6ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[5]));
                rscIotPmCoverDaysSummaryDTO.setMonth7ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[6]));
                rscIotPmCoverDaysSummaryDTO.setMonth8ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[7]));
                rscIotPmCoverDaysSummaryDTO.setMonth9ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[8]));
                rscIotPmCoverDaysSummaryDTO.setMonth10ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[9]));
                rscIotPmCoverDaysSummaryDTO.setMonth11ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[10]));
                rscIotPmCoverDaysSummaryDTO.setMonth12ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[11]));
                rscIotPmCoverDaysSummaryDTO.setMonth1CoverDays(monthSCoverDays[0]);
                rscIotPmCoverDaysSummaryDTO.setMonth2CoverDays(monthSCoverDays[1]);
                rscIotPmCoverDaysSummaryDTO.setMonth3CoverDays(monthSCoverDays[2]);
                rscIotPmCoverDaysSummaryDTO.setMonth4CoverDays(monthSCoverDays[3]);
                rscIotPmCoverDaysSummaryDTO.setMonth5CoverDays(monthSCoverDays[4]);
                rscIotPmCoverDaysSummaryDTO.setMonth6CoverDays(monthSCoverDays[5]);
                rscIotPmCoverDaysSummaryDTO.setMonth7CoverDays(monthSCoverDays[6]);
                rscIotPmCoverDaysSummaryDTO.setMonth8CoverDays(monthSCoverDays[7]);
                rscIotPmCoverDaysSummaryDTO.setMonth9CoverDays(monthSCoverDays[8]);
                rscIotPmCoverDaysSummaryDTO.setMonth10CoverDays(monthSCoverDays[9]);
                rscIotPmCoverDaysSummaryDTO.setMonth11CoverDays(monthSCoverDays[10]);
                rscIotPmCoverDaysSummaryDTO.setMonth12CoverDays(monthSCoverDays[11]);
            }
            saveCoverDaysSummary(rscIotPmCoverDaysSummaryDTO);
        }
    }

    private List<RscIotPmCoverDaysDTO> getRscItPmCoverDaysList(Long rscMtPPHeaderId) {
        return rscIotPmCoverDaysMapper.toDto(rscIotPmCoverDaysRepository.findAllByRscMtPPheaderId(rscMtPPHeaderId));
    }

    private void saveCoverDaysSummary(RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO) {
        rscIotPmCoverDaysSummaryService.saveCoverDaysSummary(rscIotPmCoverDaysSummaryDTO);
    }

    @Override
    public PmCoverDaysSummaryDTO getPmCoverDaysSummaryDetails(Long ppHeaderId) {
        return rscIotPmCoverDaysSummaryService.getPmCoverDaysSummaryDetails(ppHeaderId);
    }

    @Override
    public PmCoverDaysDetailsDTO getPmCoverDaysDetails(Long ppHeaderId) {
        PmCoverDaysDetailsDTO pmCoverDaysDetailsDTO = new PmCoverDaysDetailsDTO();
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            pmCoverDaysDetailsDTO.setMpsId(pPheaderDTO.get().getId());
            pmCoverDaysDetailsDTO.setMpsName(pPheaderDTO.get().getMpsName());
            pmCoverDaysDetailsDTO.setMpsDate(pPheaderDTO.get().getMpsDate());
            List<RscIotPmCoverDays> rscIotPmCoverDays = rscIotPmCoverDaysRepository.findAllByRscMtPPheaderId(ppHeaderId);
            if (Optional.ofNullable(rscIotPmCoverDays).isPresent()) {
                pmCoverDaysDetailsDTO.setPmCoverDaysDTOList(pmCoverDaysMapper.toDto(rscIotPmCoverDays));
            }
        }
        return pmCoverDaysDetailsDTO;
    }

    @Override
    public PmCoverDaysDetailsDTO getNullStdPriceDetails(Long rscMtPPHeaderId) {
        PmCoverDaysDetailsDTO pmCoverDaysDetailsDTO = new PmCoverDaysDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            pmCoverDaysDetailsDTO.setMpsId(rscMtPPheaderDTO.get().getId());
            pmCoverDaysDetailsDTO.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
            pmCoverDaysDetailsDTO.setMpsName(rscMtPPheaderDTO.get().getMpsName());
            pmCoverDaysDetailsDTO.setPmCoverDaysDTOList(getPmSlobDetails(rscMtPPHeaderId));
        }
        return pmCoverDaysDetailsDTO;
    }

    private List<PmCoverDaysDTO> getPmSlobDetails(Long rscMtPPHeaderId) {
        List<PmCoverDaysDTO> coverDaysDTOList = pmCoverDaysMapper.toDto(rscIotPmCoverDaysRepository.findAllByRscMtPPheaderIdAndRateIsNull(rscMtPPHeaderId));
        List<PmCoverDaysDTO> pmCoverDaysDTOList = new ArrayList<>();
        if (Optional.ofNullable(coverDaysDTOList).isPresent()) {
            pmCoverDaysDTOList.addAll(coverDaysDTOList);
        }
        return pmCoverDaysDTOList;
    }

    @Override
    public CoverDaysSummaryDetailsDTO getPmCoverDaysSummaryAllDetails(Long ppHeaderId) {
        CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO = new CoverDaysSummaryDetailsDTO();
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO = rscIotPmCoverDaysSummaryService.getPmCoverDaysSummary(ppHeaderId);
            if (Optional.ofNullable(rscIotPmCoverDaysSummaryDTO).isPresent()) {
                coverDaysSummaryDetailsDTO = rscIotPmCoverDaysSummaryService.getPmCoverDaysSummaryAllDetails(ppHeaderId);
                coverDaysSummaryDetailsDTO.setCoverDaysInMonthDTO(coverDaysMonthUtils.getCoverDaysOfAllMonths(pPheaderDTO.get().getMpsDate()));
                coverDaysSummaryDetailsDTO.setTotalCoverDays(getTotalCoverDays(rscIotPmCoverDaysSummaryDTO));
                coverDaysSummaryDetailsDTO.setTotalDaysOfMonths(coverDaysMonthUtils.getTotalDaysOfAllMonths(pPheaderDTO.get().getMpsDate()));
            }
        }
        return coverDaysSummaryDetailsDTO;
    }


    private Integer getTotalCoverDays(RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO) {
        if (Optional.ofNullable(rscIotPmCoverDaysSummaryDTO).isPresent()) {
            return rscIotPmCoverDaysSummaryDTO.getMonth1CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth2CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth3CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth4CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth5CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth6CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth7CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth8CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth9CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth10CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth11CoverDays() + rscIotPmCoverDaysSummaryDTO.getMonth12CoverDays();
        } else {
            return 0;
        }
    }

    @Override
    public List<RscIotPmCoverDaysSummaryDTO> findAllByCoverDaysSummaryDTOppHeaderId(Long ppHeaderId) {
        return rscIotPmCoverDaysSummaryService.getAllPmCoverDays(ppHeaderId);
    }

    @Override
    public ExcelExportFiles exportPmSlobExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmCoverDaysExporter.pmCoverDaysToExcel(getPmCoverDaysDetails(rscMtPPheaderDTO.getId()), getPmCoverDaysSummaryDetails(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_COVER_DAYS + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<PmCoverDaysDTO> saveAllPmCoverDaysDetails(List<PmCoverDaysDTO> pmCoverDaysDTO) {
        List<RscIotPmCoverDays> pmCoverDaysList = new ArrayList<>();
        pmCoverDaysDTO.forEach(pmCoverDays -> {
            if (Optional.ofNullable(pmCoverDays.getStdPrice()).isPresent()) {
                pmCoverDays.setRate(pmCoverDays.getStdPrice());
                pmCoverDays.setStdPrice(pmCoverDays.getStdPrice());
                if (Optional.ofNullable(pmCoverDays.getStockQuantity()).isPresent()) {
                    pmCoverDays.setStockValue(DoubleUtils.round(pmCoverDays.getStockQuantity() * pmCoverDays.getStdPrice()));
                }
                if (Optional.ofNullable(pmCoverDays.getCoverDaysMonthDetailsDTO()).isPresent()) {
                    pmCoverDays.setCoverDaysMonthDetailsDTO(getMonthDetails(pmCoverDays.getCoverDaysMonthDetailsDTO(), pmCoverDays.getStdPrice()));
                }
            }
            pmCoverDaysList.add(pmCoverDaysMapper.toEntity(pmCoverDays));
        });
        rscIotPmCoverDaysRepository.saveAll(pmCoverDaysList);
        return pmCoverDaysDTO;
    }

    private CoverDaysMonthDetailsDTO getMonthDetails(CoverDaysMonthDetailsDTO coverDaysMonthDetailsDTO, Double stdPrice) {
        coverDaysMonthDetailsDTO.setMonth1Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth1ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth2Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth2ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth3Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth3ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth4Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth4ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth5Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth5ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth6Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth6ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth7Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth7ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth8Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth8ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth9Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth9ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth10Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth10ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth11Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth11ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth12Value(getMonthDetail(coverDaysMonthDetailsDTO.getMonth12ConsumptionQuantity(), stdPrice));
        return coverDaysMonthDetailsDTO;
    }

    @Override
    public void deleteById(Long ppHeaderId) {
        rscIotPmCoverDaysRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}