package com.example.validation.service.impl;

import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.mapper.RscItSubBaseLogicalConsumptionMapper;
import com.example.validation.repository.RscItSubBaseLogicalConsumptionRepository;
import com.example.validation.service.RscItSubBaseLogicalConsumptionService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscItSubBaseLogicalConsumptionServiceImpl implements RscItSubBaseLogicalConsumptionService {

    private final RscItSubBaseLogicalConsumptionRepository rscItSubBaseLogicalConsumptionRepository;
    private final RscItSubBaseLogicalConsumptionMapper rscItSubBaseLogicalConsumptionMapper;

    public RscItSubBaseLogicalConsumptionServiceImpl(RscItSubBaseLogicalConsumptionRepository rscItSubBaseLogicalConsumptionRepository, RscItSubBaseLogicalConsumptionMapper rscItSubBaseLogicalConsumptionMapper) {
        this.rscItSubBaseLogicalConsumptionRepository = rscItSubBaseLogicalConsumptionRepository;
        this.rscItSubBaseLogicalConsumptionMapper = rscItSubBaseLogicalConsumptionMapper;
    }

    @Override
    public void saveAll(List<RscItLogicalConsumptionDTO> subBaseRscItLogicalConsumptionDTOs) {
        subBaseRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
        subBaseRscItLogicalConsumptionDTOs.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
        rscItSubBaseLogicalConsumptionRepository.saveAll(rscItSubBaseLogicalConsumptionMapper.toEntity(subBaseRscItLogicalConsumptionDTOs));
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItSubBaseLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId) {
        return rscItSubBaseLogicalConsumptionMapper.toDto(rscItSubBaseLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public Set<Long> findAllItemIdByRscMtPPHeaderId(Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs = rscItSubBaseLogicalConsumptionMapper.toDto(rscItSubBaseLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
        return rscItLogicalConsumptionDTOs.stream().map(RscItLogicalConsumptionDTO::getRscMtItemChildId).collect(Collectors.toSet());
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderIdAndChildItemId(Long ppHeaderId, Long subBaseItemId) {
        return rscItSubBaseLogicalConsumptionMapper.toDto(rscItSubBaseLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemChildId(ppHeaderId, subBaseItemId));
    }

    @Override
    public void deleteByRscMtPPheaderIdAndRscMtParentItemId(Long ppHeaderId, Long bpParentItemId) {
        rscItSubBaseLogicalConsumptionRepository.deleteAllByRscMtPPheaderIdAndRscMtItemParentId(ppHeaderId, bpParentItemId);
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItSubBaseLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}