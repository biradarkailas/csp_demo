package com.example.validation.service.impl;

import com.example.validation.dto.RscIotPmSlobSummaryDTO;
import com.example.validation.dto.SlobSummaryDashboardDTO;
import com.example.validation.mapper.PmSlobSummaryDashboardMapper;
import com.example.validation.mapper.RscIotPmSlobSummaryMapper;
import com.example.validation.repository.RscIotPmSlobSummaryRepository;
import com.example.validation.service.RscIotPmSlobSummaryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscIotPmSlobSummaryServiceImpl implements RscIotPmSlobSummaryService {
    private final RscIotPmSlobSummaryMapper rscIotPmSlobSummaryMapper;
    private final RscIotPmSlobSummaryRepository rscIotPmSlobSummaryRepository;
    private final PmSlobSummaryDashboardMapper pmSlobSummaryDashboardMapper;

    public RscIotPmSlobSummaryServiceImpl(RscIotPmSlobSummaryMapper rscIotPmSlobSummaryMapper, RscIotPmSlobSummaryRepository rscIotPmSlobSummaryRepository, PmSlobSummaryDashboardMapper pmSlobSummaryDashboardMapper) {
        this.rscIotPmSlobSummaryMapper = rscIotPmSlobSummaryMapper;
        this.rscIotPmSlobSummaryRepository = rscIotPmSlobSummaryRepository;
        this.pmSlobSummaryDashboardMapper = pmSlobSummaryDashboardMapper;
    }

    @Override
    public List<RscIotPmSlobSummaryDTO> findAll(Long ppHeaderId) {
        return rscIotPmSlobSummaryMapper.toDto(rscIotPmSlobSummaryRepository.findAll());
    }

    @Override
    public void save(RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO) {
        rscIotPmSlobSummaryRepository.save(rscIotPmSlobSummaryMapper.toEntity(rscIotPmSlobSummaryDTO));
    }

    @Override
    public RscIotPmSlobSummaryDTO findByRscMtPPheaderId(Long rscMtPPHeaderId) {
        return rscIotPmSlobSummaryMapper.toDto(rscIotPmSlobSummaryRepository.findByRscMtPPheaderId(rscMtPPHeaderId));
    }

    @Override
    public long countByRscMtPPheaderId(Long ppHeaderId) {
        return rscIotPmSlobSummaryRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public void deleteById(Long ppHeaderId) {
        rscIotPmSlobSummaryRepository.deleteByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public SlobSummaryDashboardDTO getPmSlobSummaryDashboard(Long ppHeaderId) {
        return pmSlobSummaryDashboardMapper.toDto(rscIotPmSlobSummaryRepository.findByRscMtPPheaderId(ppHeaderId));
    }

}