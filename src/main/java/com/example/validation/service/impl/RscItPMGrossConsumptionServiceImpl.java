package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.SlobTotalConsumptionDetailsDTO;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.PMGrossConsumptionMapper;
import com.example.validation.mapper.PmSlobTotalConsumptionDetailsMapper;
import com.example.validation.mapper.RscItPMGrossConsumptionMapper;
import com.example.validation.repository.RscItPMGrossConsumptionRepository;
import com.example.validation.service.RscItPMGrossConsumptionService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.excel_export.PmGrossConsumptionExporter;
import com.example.validation.util.consumption.ConsumptionUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscItPMGrossConsumptionServiceImpl implements RscItPMGrossConsumptionService {
    private final RscItPMGrossConsumptionRepository rscItPmGrossConsumptionRepository;
    private final PMGrossConsumptionMapper pmGrossConsumptionMapper;
    private final RscItPMGrossConsumptionMapper rscItPMGrossConsumptionMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final ConsumptionUtil consumptionUtil;
    private final PmSlobTotalConsumptionDetailsMapper pmSlobTotalConsumptionDetailsMapper;

    public RscItPMGrossConsumptionServiceImpl(RscItPMGrossConsumptionRepository rscItPmGrossConsumptionRepository, PMGrossConsumptionMapper pmGrossConsumptionMapper, RscItPMGrossConsumptionMapper rscItPMGrossConsumptionMapper, RscMtPpheaderService rscMtPpheaderService, ConsumptionUtil consumptionUtil, PmSlobTotalConsumptionDetailsMapper pmSlobTotalConsumptionDetailsMapper) {
        this.rscItPmGrossConsumptionRepository = rscItPmGrossConsumptionRepository;
        this.pmGrossConsumptionMapper = pmGrossConsumptionMapper;
        this.rscItPMGrossConsumptionMapper = rscItPMGrossConsumptionMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.consumptionUtil = consumptionUtil;
        this.pmSlobTotalConsumptionDetailsMapper = pmSlobTotalConsumptionDetailsMapper;
    }

    @Override
    public GrossConsumptionMainDTO getAllPMGrossConsumption(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        return getPMGrossConsumptionMainDTO(optionalRscMtPPHeaderDTO.get());
    }

    private GrossConsumptionMainDTO getPMGrossConsumptionMainDTO(RscMtPPheaderDTO rscMtPPHeaderDTO) {
        List<GrossConsumptionDTO> grossConsumptionDTOs = pmGrossConsumptionMapper.toDto(rscItPmGrossConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemRscDtItemCategoryDescription(rscMtPPHeaderDTO.getId(), MaterialCategoryConstants.PACKING_MATERIAL));
        return consumptionUtil.getGrossConsumptionMainDTO(rscMtPPHeaderDTO, grossConsumptionDTOs);
    }

    @Override
    public void saveAll(List<RscItGrossConsumptionDTO> pmRscItGrossConsumptionDTOs, Long ppHeaderId) {
        if (rscItPmGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            pmRscItGrossConsumptionDTOs.sort(Comparator.comparing(RscItGrossConsumptionDTO::getItemCode));
            rscItPmGrossConsumptionRepository.saveAll(rscItPMGrossConsumptionMapper.toEntity(pmRscItGrossConsumptionDTOs));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItPmGrossConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscItPMGrossConsumptionMapper.toDto(rscItPmGrossConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscItGrossConsumptionDTO> findAllPMGrossConsumptionByRscMtPPheaderId(Long ppHeaderId) {
        return rscItPMGrossConsumptionMapper.toDto(rscItPmGrossConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemRscDtItemCategoryDescription(ppHeaderId, MaterialCategoryConstants.PACKING_MATERIAL));
    }

    @Override
    public RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId) {
        return rscItPMGrossConsumptionMapper.toDto(rscItPmGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, rscMtItemId));
    }

    public List<RscItGrossConsumptionDTO> findAll() {
        return rscItPMGrossConsumptionMapper.toDto(rscItPmGrossConsumptionRepository.findAll());
    }

    @Override
    public ExcelExportFiles getPmGrossConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmGrossConsumptionExporter.grossConsumptionsToExcel(getPMGrossConsumptionMainDTO(rscMtPPheaderDTO), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_GROSS_CONSUMPTION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public SlobTotalConsumptionDetailsDTO getPmSlobTotalConsumptionDetailsDTO(Long ppHeaderId, Long rscMtItemId) {
        return pmSlobTotalConsumptionDetailsMapper.toDto(rscItPmGrossConsumptionRepository.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, rscMtItemId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItPmGrossConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}