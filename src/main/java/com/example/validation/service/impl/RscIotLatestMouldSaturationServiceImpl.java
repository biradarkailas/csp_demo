package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.domain.RscIotLatestMouldSaturation;
import com.example.validation.dto.*;
import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.mapper.ItemWiseLatestMouldSaturationMapper;
import com.example.validation.mapper.LatestMouldSaturationMapper;
import com.example.validation.mapper.MouldSaturationUniqueSuppliersMapper;
import com.example.validation.mapper.RscIotLatestMouldSaturationMapper;
import com.example.validation.repository.RscIotLatestMouldSaturationRepository;
import com.example.validation.service.*;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.ListUtils;
import com.example.validation.util.MouldSaturationUtil;
import com.example.validation.util.excel_export.PmSupplierWiseMouldExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotLatestMouldSaturationServiceImpl implements RscIotLatestMouldSaturationService {
    private final RscIotLatestMouldSaturationRepository rscIotLatestMouldSaturationRepository;
    private final RscIotLatestMouldSaturationMapper rscIotLatestMouldSaturationMapper;
    private final ItemWiseLatestMouldSaturationMapper itemWiseLatestMouldSaturationMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService;
    private final MouldSaturationUniqueSuppliersMapper mouldSaturationUniqueSuppliersMapper;
    private final LatestMouldSaturationMapper latestMouldSaturationMapper;
    private final RscIotPMLatestPurchasePlanService rscIotPMLatestPurchasePlanService;
    private final RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService;

    public RscIotLatestMouldSaturationServiceImpl(RscIotLatestMouldSaturationRepository rscIotLatestMouldSaturationRepository, RscIotLatestMouldSaturationMapper rscIotLatestMouldSaturationMapper, ItemWiseLatestMouldSaturationMapper itemWiseLatestMouldSaturationMapper, RscMtPpheaderService rscMtPpheaderService, RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService, MouldSaturationUniqueSuppliersMapper mouldSaturationUniqueSuppliersMapper, LatestMouldSaturationMapper latestMouldSaturationMapper, RscIotPMLatestPurchasePlanService rscIotPMLatestPurchasePlanService, RscIotOriginalMouldSaturationService rscIotOriginalMouldSaturationService) {
        this.rscIotLatestMouldSaturationRepository = rscIotLatestMouldSaturationRepository;
        this.rscIotLatestMouldSaturationMapper = rscIotLatestMouldSaturationMapper;
        this.itemWiseLatestMouldSaturationMapper = itemWiseLatestMouldSaturationMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIitMpsWorkingDaysService = rscIitMpsWorkingDaysService;
        this.mouldSaturationUniqueSuppliersMapper = mouldSaturationUniqueSuppliersMapper;
        this.latestMouldSaturationMapper = latestMouldSaturationMapper;
        this.rscIotPMLatestPurchasePlanService = rscIotPMLatestPurchasePlanService;
        this.rscIotOriginalMouldSaturationService = rscIotOriginalMouldSaturationService;
    }

    @Override
    public List<LatestPurchasePlanDTO> findAll() {
        return rscIotLatestMouldSaturationMapper.toDto(rscIotLatestMouldSaturationRepository.findAll());
    }

    @Override
    public void saveMouldSaturation(Long ppHeaderId) {
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            if (rscIotLatestMouldSaturationRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
                List<LatestPurchasePlanDTO> latestPurchasePlanDTOList = new ArrayList<>();
                List<LatestPurchasePlanDTO> originalPurchasePlanDataList = rscIotPMLatestPurchasePlanService.getPurchasePlanListForMouldCalculation(ppHeaderId);
                RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay = rscIitMpsWorkingDaysService.findByMpsId(ppHeaderId);
                if (Optional.ofNullable(rscIitMpsWorkingDay).isPresent() && originalPurchasePlanDataList.size() > 0) {
                    originalPurchasePlanDataList.forEach(latestPurchasePlanDTO -> {
                        if (latestPurchasePlanDTO.getMouldId() != null) {
                            latestPurchasePlanDTOList.add(getCalculatedLatestPurchasePlanDTO(rscIitMpsWorkingDay, latestPurchasePlanDTO));
                        }
                    });
                }
                saveAll(latestPurchasePlanDTOList);
                rscIotOriginalMouldSaturationService.saveAllOriginalMouldSaturation(latestPurchasePlanDTOList);
            }
        }
    }

    @Override
    public List<RscIotLatestMouldSaturationDTO> getMouldSaturationListByPpHeaderId(Long ppHeaderId) {
        return latestMouldSaturationMapper.toDto(rscIotLatestMouldSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    private LatestPurchasePlanDTO getCalculatedLatestPurchasePlanDTO(RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay, LatestPurchasePlanDTO latestPurchasePlanDTO) {
        latestPurchasePlanDTO.setM1Value(MouldSaturationUtil.getMonth1MouldSaturation(latestPurchasePlanDTO.getM1Value(), latestPurchasePlanDTO.getM2Value(), rscIitMpsWorkingDay.getMonth1Value(), latestPurchasePlanDTO.getRtdValue(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM2Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM3Value(), rscIitMpsWorkingDay.getMonth2Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM3Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM4Value(), rscIitMpsWorkingDay.getMonth3Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM4Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM5Value(), rscIitMpsWorkingDay.getMonth4Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM5Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM6Value(), rscIitMpsWorkingDay.getMonth5Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM6Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM7Value(), rscIitMpsWorkingDay.getMonth6Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM7Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM8Value(), rscIitMpsWorkingDay.getMonth7Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM8Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM9Value(), rscIitMpsWorkingDay.getMonth8Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM9Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM10Value(), rscIitMpsWorkingDay.getMonth9Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM10Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM11Value(), rscIitMpsWorkingDay.getMonth10Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM11Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM12Value(), rscIitMpsWorkingDay.getMonth11Value(), latestPurchasePlanDTO.getCapacity()));
        latestPurchasePlanDTO.setM12Value(MouldSaturationUtil.getMonthWiseMouldSaturation(0.0, rscIitMpsWorkingDay.getMonth12Value(), latestPurchasePlanDTO.getCapacity()));
        return latestPurchasePlanDTO;
    }

    @SuppressWarnings("Duplicates")
  /*  @Override
    public String saveMouldSaturation(Long ppHeaderId) {
        List<LatestPurchasePlanDTO> latestPurchasePlanDTOList = new ArrayList<>();
        List<LatestPurchasePlanDTO> originalPurchasePlanDataList = latestPurchasePlanMapper.toDto(this.rscIotPMLatestPurchasePlanListByPpHeaderId(ppHeaderId));
        RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay = rscIitMpsWorkingDaysService.findByMpsId(ppHeaderId);

        if (rscIitMpsWorkingDay != null && originalPurchasePlanDataList.size() != 0) {
            for (LatestPurchasePlanDTO latestPurchasePlanDTO : originalPurchasePlanDataList) {
                if (latestPurchasePlanDTO.getMouldId() != null) {
                    latestPurchasePlanDTO.setM1Value(MouldSaturationUtil.getMonth1MouldSaturation(latestPurchasePlanDTO.getM1Value(), latestPurchasePlanDTO.getM2Value(), rscIitMpsWorkingDay.getMonth1Value(), latestPurchasePlanDTO.getRtdValue(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM2Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM3Value(), rscIitMpsWorkingDay.getMonth2Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM3Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM4Value(), rscIitMpsWorkingDay.getMonth3Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM4Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM5Value(), rscIitMpsWorkingDay.getMonth4Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM5Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM6Value(), rscIitMpsWorkingDay.getMonth5Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM6Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM7Value(), rscIitMpsWorkingDay.getMonth6Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM7Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM8Value(), rscIitMpsWorkingDay.getMonth7Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM8Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM9Value(), rscIitMpsWorkingDay.getMonth8Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM9Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM10Value(), rscIitMpsWorkingDay.getMonth9Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM10Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM11Value(), rscIitMpsWorkingDay.getMonth10Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM11Value(MouldSaturationUtil.getMonthWiseMouldSaturation(latestPurchasePlanDTO.getM12Value(), rscIitMpsWorkingDay.getMonth11Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTO.setM12Value(MouldSaturationUtil.getMonthWiseMouldSaturation(0.0, rscIitMpsWorkingDay.getMonth12Value(), latestPurchasePlanDTO.getCapacity()));
                    latestPurchasePlanDTOList.add(latestPurchasePlanDTO);
                }
            }
        }
        saveAll(latestPurchasePlanDTOList);
        return "Original Mould Saturation Saved Successfully";
    }
*/
    /*@Override
    public List<RscIotPMLatestPurchasePlan> rscIotPMLatestPurchasePlanListByPpHeaderId(Long ppHeaderId) {
        return rscIotPMLatestPurchasePlanRepository.findAllByRscMtPPheaderId(ppHeaderId);
    }*/

    @Override
    public void saveAll(List<LatestPurchasePlanDTO> latestPurchasePlanDTOList) {
        rscIotLatestMouldSaturationRepository.saveAll(rscIotLatestMouldSaturationMapper.toEntity(latestPurchasePlanDTOList));
    }

    @Override
    public List<SupplierWiseLatestMouldSaturationDTO> getSupplierWiseMouldSaturation(Long ppHeaderId) {
        List<RscIotLatestMouldSaturation> rscIotLatestMouldSaturationList = rscIotLatestMouldSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId);
        List<RscIotLatestMouldSaturation> distinctBySupplierIdList = rscIotLatestMouldSaturationList.stream().filter(ListUtils.distinctByKey(p -> p.getRscIotPMLatestPurchasePlan().getRscItItemWiseSupplier().getRscMtSupplier().getId() + "" + p.getRscDtMould().getMould())).collect(Collectors.toList());
        List<SupplierWiseLatestMouldSaturationDTO> supplierWiseLatestMouldSaturationDTOS = new ArrayList<>();
        for (RscIotLatestMouldSaturation rscIotOriginalMouldSaturation : distinctBySupplierIdList) {
            Double month1Total = 0.0;
            Double month2Total = 0.0;
            Double month3Total = 0.0;
            Double month4Total = 0.0;
            Double month5Total = 0.0;
            Double month6Total = 0.0;
            Double month7Total = 0.0;
            Double month8Total = 0.0;
            Double month9Total = 0.0;
            Double month10Total = 0.0;
            Double month11Total = 0.0;
            Double month12Total = 0.0;
            if (rscIotOriginalMouldSaturation != null) {
                SupplierWiseLatestMouldSaturationDTO supplierWiseLatestMouldSaturationDTO = new SupplierWiseLatestMouldSaturationDTO();
                supplierWiseLatestMouldSaturationDTO.setSupplierName(rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscItItemWiseSupplier().getRscMtSupplier().getRscDtSupplier().getSupplierName());

                if (rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan() != null && rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscMtItem() != null && rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscMtItem().getRscItItemMould() != null && rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscMtItem().getRscItItemMould().getRscDtMould() != null) {
                    supplierWiseLatestMouldSaturationDTO.setMould(rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscMtItem().getRscItItemMould().getRscDtMould().getMould());
                    supplierWiseLatestMouldSaturationDTO.setCapacity(rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscMtItem().getRscItItemMould().getRscDtMould().getTotalCapacity());
                }
                List<RscIotLatestMouldSaturation> iotOriginalMouldSaturationList = rscIotLatestMouldSaturationList.stream().filter(rscIotOriginalMouldSaturation1 -> rscIotOriginalMouldSaturation1.getRscIotPMLatestPurchasePlan().getRscItItemWiseSupplier().getRscMtSupplier().getRscDtSupplier() == rscIotOriginalMouldSaturation.getRscIotPMLatestPurchasePlan().getRscItItemWiseSupplier().getRscMtSupplier().getRscDtSupplier() && rscIotOriginalMouldSaturation1.getRscDtMould().getMould().equals(rscIotOriginalMouldSaturation.getRscDtMould().getMould())).collect(Collectors.toList());

                var itemWiseOriginalMouldSaturationDTOList = itemWiseLatestMouldSaturationMapper.toDto(iotOriginalMouldSaturationList);
                for (ItemWiseLatestMouldSaturationDTO itemWiseOriginalMouldSaturationDTO : itemWiseOriginalMouldSaturationDTOList) {
                    month1Total += itemWiseOriginalMouldSaturationDTO.getMonth1().getValue();
                    month2Total += itemWiseOriginalMouldSaturationDTO.getMonth2().getValue();
                    month3Total += itemWiseOriginalMouldSaturationDTO.getMonth3().getValue();
                    month4Total += itemWiseOriginalMouldSaturationDTO.getMonth4().getValue();
                    month5Total += itemWiseOriginalMouldSaturationDTO.getMonth5().getValue();
                    month6Total += itemWiseOriginalMouldSaturationDTO.getMonth6().getValue();
                    month7Total += itemWiseOriginalMouldSaturationDTO.getMonth7().getValue();
                    month8Total += itemWiseOriginalMouldSaturationDTO.getMonth8().getValue();
                    month9Total += itemWiseOriginalMouldSaturationDTO.getMonth9().getValue();
                    month10Total += itemWiseOriginalMouldSaturationDTO.getMonth10().getValue();
                    month11Total += itemWiseOriginalMouldSaturationDTO.getMonth11().getValue();
                    month12Total += itemWiseOriginalMouldSaturationDTO.getMonth12().getValue();
                }
                supplierWiseLatestMouldSaturationDTO.setMonth1Total(month1Total);
                supplierWiseLatestMouldSaturationDTO.setMonth2Total(month2Total);
                supplierWiseLatestMouldSaturationDTO.setMonth3Total(month3Total);
                supplierWiseLatestMouldSaturationDTO.setMonth4Total(month4Total);
                supplierWiseLatestMouldSaturationDTO.setMonth5Total(month5Total);
                supplierWiseLatestMouldSaturationDTO.setMonth6Total(month6Total);
                supplierWiseLatestMouldSaturationDTO.setMonth7Total(month7Total);
                supplierWiseLatestMouldSaturationDTO.setMonth8Total(month8Total);
                supplierWiseLatestMouldSaturationDTO.setMonth9Total(month9Total);
                supplierWiseLatestMouldSaturationDTO.setMonth10Total(month10Total);
                supplierWiseLatestMouldSaturationDTO.setMonth11Total(month11Total);
                supplierWiseLatestMouldSaturationDTO.setMonth12Total(month12Total);
                supplierWiseLatestMouldSaturationDTO.setItemWiseLatestMouldSaturationDTOList(itemWiseOriginalMouldSaturationDTOList);
                supplierWiseLatestMouldSaturationDTOS.add(supplierWiseLatestMouldSaturationDTO);
            }
        }
        return supplierWiseLatestMouldSaturationDTOS;
    }

    @Override
    public ExcelExportFiles getSupplierWiseMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<SupplierWiseLatestMouldSaturationDTO> supplierWiseLatestMouldSaturationDTOList = getSupplierWiseMouldSaturation(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(PmSupplierWiseMouldExporter.supplierWiseMouldToExcel(supplierWiseLatestMouldSaturationDTOList, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_SUPPLIER_WISE_MOULD + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<RscDtSupplierDTO> getSuppliersListOfMouldSaturation(Long ppHeaderId) {
        List<RscDtSupplierDTO> suppliersDashboardList = new ArrayList<>();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            List<RscIotLatestMouldSaturation> rscIotLatestMouldSaturationList = rscIotLatestMouldSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId);
            List<RscIotLatestMouldSaturation> distinctBySupplierIdList = rscIotLatestMouldSaturationList.stream().filter(ListUtils.distinctByKey(p -> p.getRscIotPMLatestPurchasePlan().getRscItItemWiseSupplier().getRscMtSupplier().getRscDtSupplier().getId())).collect(Collectors.toList());
            suppliersDashboardList = mouldSaturationUniqueSuppliersMapper.toDto(distinctBySupplierIdList).stream().sorted(Comparator.comparing(RscDtSupplierDTO::getSupplierName)).collect(Collectors.toList());
        }
        return suppliersDashboardList;
    }

    @Override
    public List<RscIotLatestMouldSaturationDTO> getMouldSaturationListBySupplier(Long ppHeaderId, Long supplierId) {
        List<RscIotLatestMouldSaturationDTO> mouldSaturationDTOList = new ArrayList<>();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            List<RscIotLatestMouldSaturation> rscIotLatestMouldSaturationList = rscIotLatestMouldSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId);
            List<RscIotLatestMouldSaturationDTO> mouldSaturationList = latestMouldSaturationMapper.toDto(rscIotLatestMouldSaturationList.stream().filter(rscIotLatestMouldSaturation -> rscIotLatestMouldSaturation.getRscIotPMLatestPurchasePlan().getRscItItemWiseSupplier().getRscMtSupplier().getRscDtSupplier().getId().equals(supplierId)).collect(Collectors.toList()));
            List<Long> UniqueMouldsIdList = mouldSaturationList.stream().
                    map(RscIotLatestMouldSaturationDTO::getMould).distinct().collect(Collectors.toList());
            UniqueMouldsIdList.forEach(mouldId -> {
                RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturation = new RscIotLatestMouldSaturationDTO();
                rscIotLatestMouldSaturation.setMould(mouldId);
                Double[] monthsTotalMouldSaturation = new Double[12];
                for (int i = 0; i < monthsTotalMouldSaturation.length; i++) {
                    monthsTotalMouldSaturation[i] = 0.0;
                }
                mouldSaturationList.forEach(rscIotLatestMouldSaturationDTO -> {
                    if (rscIotLatestMouldSaturationDTO.getMould() == mouldId) {
                        monthsTotalMouldSaturation[0] += rscIotLatestMouldSaturationDTO.getM1Value();
                        monthsTotalMouldSaturation[1] += rscIotLatestMouldSaturationDTO.getM2Value();
                        monthsTotalMouldSaturation[3] += rscIotLatestMouldSaturationDTO.getM4Value();
                        monthsTotalMouldSaturation[2] += rscIotLatestMouldSaturationDTO.getM3Value();
                        monthsTotalMouldSaturation[4] += rscIotLatestMouldSaturationDTO.getM5Value();
                        monthsTotalMouldSaturation[5] += rscIotLatestMouldSaturationDTO.getM6Value();
                        monthsTotalMouldSaturation[6] += rscIotLatestMouldSaturationDTO.getM7Value();
                        monthsTotalMouldSaturation[7] += rscIotLatestMouldSaturationDTO.getM8Value();
                        monthsTotalMouldSaturation[8] += rscIotLatestMouldSaturationDTO.getM9Value();
                        monthsTotalMouldSaturation[9] += rscIotLatestMouldSaturationDTO.getM10Value();
                        monthsTotalMouldSaturation[10] += rscIotLatestMouldSaturationDTO.getM11Value();
                        monthsTotalMouldSaturation[11] += rscIotLatestMouldSaturationDTO.getM12Value();
                        rscIotLatestMouldSaturation.setMouldName(rscIotLatestMouldSaturationDTO.getMouldName());
                    }
                });
                mouldSaturationDTOList.add(getMouldSaturation(rscIotLatestMouldSaturation, monthsTotalMouldSaturation));
            });
        }
        return mouldSaturationDTOList.stream().sorted(Comparator.comparing(RscIotLatestMouldSaturationDTO::getMonthAverage).reversed()).collect(Collectors.toList());
    }

    private RscIotLatestMouldSaturationDTO getMouldSaturation(RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturationDTO, Double[] monthsTotalMouldSaturation) {
        rscIotLatestMouldSaturationDTO.setM1Value(monthsTotalMouldSaturation[0]);
        rscIotLatestMouldSaturationDTO.setM2Value(monthsTotalMouldSaturation[1]);
        rscIotLatestMouldSaturationDTO.setM3Value(monthsTotalMouldSaturation[2]);
        rscIotLatestMouldSaturationDTO.setM4Value(monthsTotalMouldSaturation[3]);
        rscIotLatestMouldSaturationDTO.setM5Value(monthsTotalMouldSaturation[4]);
        rscIotLatestMouldSaturationDTO.setM6Value(monthsTotalMouldSaturation[5]);
        rscIotLatestMouldSaturationDTO.setM7Value(monthsTotalMouldSaturation[6]);
        rscIotLatestMouldSaturationDTO.setM8Value(monthsTotalMouldSaturation[7]);
        rscIotLatestMouldSaturationDTO.setM9Value(monthsTotalMouldSaturation[8]);
        rscIotLatestMouldSaturationDTO.setM10Value(monthsTotalMouldSaturation[9]);
        rscIotLatestMouldSaturationDTO.setM11Value(monthsTotalMouldSaturation[10]);
        rscIotLatestMouldSaturationDTO.setM12Value(monthsTotalMouldSaturation[11]);
        rscIotLatestMouldSaturationDTO.setMonthAverage(getMonthAverage(monthsTotalMouldSaturation));
        return rscIotLatestMouldSaturationDTO;
    }

    private Double getMonthAverage(Double[] monthsTotalMouldSaturation) {
        return DoubleUtils.round(Arrays.stream(monthsTotalMouldSaturation).mapToLong(Double::longValue).average().orElse(0));
    }

    @Override
    public void deleteAllByPpHeaderId(Long ppHeaderId) {
        rscIotLatestMouldSaturationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public void updateMouldSaturationForUpdatedPurchasePlan(List<PurchasePlanDTO> purchasePlanDTOList, Long ppHeaderId) {
        RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay = rscIitMpsWorkingDaysService.findByMpsId(ppHeaderId);
        if (purchasePlanDTOList.size() > 0) {
            HashMap<Long, PurchasePlanDTO> purchasePlanDTOHashMap = getPurchasePlanHashMap(purchasePlanDTOList);
            List<Long> purchasePlanIdList = purchasePlanDTOList.stream().
                    map(PurchasePlanDTO::getId).distinct().collect(Collectors.toList());

            List<RscIotLatestMouldSaturationDTO> mouldsListToUpdate = getMouldSaturationListToUpdate(purchasePlanIdList, ppHeaderId);

            if (Optional.ofNullable(rscIitMpsWorkingDay).isPresent()) {
                List<RscIotLatestMouldSaturationDTO> mouldSaturationDTOList = new ArrayList<>();
                mouldsListToUpdate.forEach(rscIotLatestMouldSaturation -> {
                    if (rscIotLatestMouldSaturation.getMould() != null) {
                        PurchasePlanDTO purchasePlanDTO = purchasePlanDTOHashMap.get(rscIotLatestMouldSaturation.getRscIotPMLatestPurchasePlanId());

                        RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturationDTO = new RscIotLatestMouldSaturationDTO();
                        rscIotLatestMouldSaturationDTO.setId(rscIotLatestMouldSaturation.getId());
                        rscIotLatestMouldSaturationDTO.setMould(rscIotLatestMouldSaturation.getMould());
                        rscIotLatestMouldSaturationDTO.setRscIotPMLatestPurchasePlanId(rscIotLatestMouldSaturation.getRscIotPMLatestPurchasePlanId());
                        rscIotLatestMouldSaturationDTO.setRscMtPPheaderId(rscIotLatestMouldSaturation.getRscMtPPheaderId());

                        if (Optional.ofNullable(purchasePlanDTO).isPresent()) {
                            mouldSaturationDTOList.add(getUpdatedMouldSaturationDto(rscIotLatestMouldSaturationDTO, purchasePlanDTO, rscIitMpsWorkingDay));
                        }
                    }
                });
                rscIotLatestMouldSaturationRepository.saveAll(latestMouldSaturationMapper.toEntity(mouldSaturationDTOList));
//                getSupplierWiseMouldExport(ppHeaderId);
            }
        }
    }

    private RscIotLatestMouldSaturationDTO getUpdatedMouldSaturationDto(RscIotLatestMouldSaturationDTO rscIotLatestMouldSaturationDTO, PurchasePlanDTO purchasePlanDTO, RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay) {
        rscIotLatestMouldSaturationDTO.setM1Value(MouldSaturationUtil.getMonth1MouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth1().getValue(), purchasePlanDTO.getSupplyMonthValues().getMonth2().getValue(), rscIitMpsWorkingDay.getMonth1Value(), purchasePlanDTO.getRtdValue(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM2Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth3().getValue(), rscIitMpsWorkingDay.getMonth2Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM3Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth4().getValue(), rscIitMpsWorkingDay.getMonth3Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM4Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth5().getValue(), rscIitMpsWorkingDay.getMonth4Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM5Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth6().getValue(), rscIitMpsWorkingDay.getMonth5Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM6Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth7().getValue(), rscIitMpsWorkingDay.getMonth6Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM7Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth8().getValue(), rscIitMpsWorkingDay.getMonth7Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM8Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth9().getValue(), rscIitMpsWorkingDay.getMonth8Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM9Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth10().getValue(), rscIitMpsWorkingDay.getMonth9Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM10Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth11().getValue(), rscIitMpsWorkingDay.getMonth10Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM11Value(MouldSaturationUtil.getMonthWiseMouldSaturation(purchasePlanDTO.getSupplyMonthValues().getMonth12().getValue(), rscIitMpsWorkingDay.getMonth11Value(), purchasePlanDTO.getCapacity()));
        rscIotLatestMouldSaturationDTO.setM12Value(MouldSaturationUtil.getMonthWiseMouldSaturation(0.0, rscIitMpsWorkingDay.getMonth12Value(), purchasePlanDTO.getCapacity()));
        return rscIotLatestMouldSaturationDTO;
    }

    private List<RscIotLatestMouldSaturationDTO> getMouldSaturationListToUpdate(List<Long> purchasePlanIdList, Long ppHeaderId) {
        return latestMouldSaturationMapper.toDto(rscIotLatestMouldSaturationRepository.findAllByRscMtPPheaderIdAndRscIotPMLatestPurchasePlanIdIn(ppHeaderId, purchasePlanIdList));
    }

    private HashMap<Long, PurchasePlanDTO> getPurchasePlanHashMap(List<PurchasePlanDTO> purchasePlanDTOList) {
        HashMap<Long, PurchasePlanDTO> purchasePlanDTOHashMap = new HashMap();
        purchasePlanDTOList.forEach(purchasePlanDTO -> {
            purchasePlanDTOHashMap.put(purchasePlanDTO.getId(), purchasePlanDTO);
        });
        return purchasePlanDTOHashMap;
    }

    @Override
    public List<RscIotLatestMouldSaturationDTO> getMouldSaturationListByPpHeaderIdAndMouldsList(Long ppHeaderId, List<Long> uniqueMouldsIdList) {
        return latestMouldSaturationMapper.toDto(rscIotLatestMouldSaturationRepository.findAllByRscMtPPheaderIdAndRscDtMouldIdIn(ppHeaderId, uniqueMouldsIdList));
    }
}
