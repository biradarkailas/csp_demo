package com.example.validation.service.impl;

import com.example.validation.util.ProjectConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

@Service
public class EmailService {
    @Autowired
    public JavaMailSender emailSender;

    public FileUploadService fileUploadService;

   /* @Autowired
    private CustomShipmentService shipmentService;*/

    EmailService() {
        emlTempPath = System.getProperty("os.name").indexOf("Win") >= 0 ? "E:\\" : "/home/ubuntu/lorealTemp/";
    }

    static String emlTempPath;

    public void sendEmailWithoutAttachment(String[] toArray, String[] ccArray, String[] bccArray, String subject, String message) {
        try {
            MimeMessage msg = createCustomMimeMessage(toArray, ccArray, bccArray, subject, message, null);
            Transport.send(msg);
            System.out.println("Mail Sent");
        } catch (MessagingException mex) {
            System.out.println("send failed, exception: " + mex);
        }
    }

    public HttpStatus sendMailWithAttachment(String[] toArray, String[] ccArray, String[] bccArray, String subject, String message, Object[] arr) {
        /*String[] filePaths = new String[arr.length];*/
        try {
            if (arr != null) {
                String[] filePaths = Arrays.copyOf(arr, arr.length, String[].class);
                MimeMessage mimeMessage = createCustomMimeMessage(toArray, ccArray, bccArray, subject, message, filePaths);
                Transport.send(mimeMessage);
                System.out.println("Mail Sent");
                return HttpStatus.OK;
            } else {
                MimeMessage mimeMessage = createCustomMimeMessage(toArray, ccArray, bccArray, subject, message, null);
                Transport.send(mimeMessage);
                System.out.println("Mail Sent");
                return HttpStatus.OK;
            }
        } catch (MessagingException mex) {
            detectInvalidAddress(mex);
            System.out.println("send failed, exception: " + mex);
            return HttpStatus.NOT_FOUND;
        }
    }

    public void generateEmlFile(String[] toArray, String[] ccArray, String[] bccArray, String subject, String message, HttpServletResponse response) {
        try {
            MimeMessage mimeMessage = createCustomMimeMessage(toArray, ccArray, bccArray, subject, message, null);
            var tempEmlName = getAlphaNumericString(10);
            // This code is to write to eml file
            File outputFilePath = new File(emlTempPath + tempEmlName + ".eml");
            try (FileOutputStream fileOutputStream = new FileOutputStream(outputFilePath)) {
                mimeMessage.writeTo(fileOutputStream);
            } catch (Exception exception) {
                System.out.println("file generation failed, exception" + exception);
            }
            System.out.println("Eml file generated");
            try {
                InputStream inputStream = new FileInputStream(outputFilePath);
                ArrayList<byte[]> file = new ArrayList<byte[]>();
                byte[] temp;
                while ((temp = inputStream.readAllBytes()).length != 0)
                    file.add(temp);
                response.setHeader("Content-Disposition", "attachments; filename=Shipment_Document.eml");
                var iterator = file.iterator();
                while (iterator.hasNext())
                    response.getOutputStream().write(iterator.next());
                inputStream.close();
                System.out.println("temp file deleted" + outputFilePath.delete());
            } catch (Exception exception) {
                System.out.println("eml downloading failed, exception" + exception);
            }
        } catch (MessagingException mex) {
            detectInvalidAddress(mex);
            System.out.println("send failed, exception: " + mex);
        }
    }

    private MimeMessage createCustomMimeMessage(String[] toArray, String[] ccArray, String[] bccArray, String subject, String message, String[] filePaths) throws MessagingException {
        String cc = "";
        String bcc = "";
        String to = "";
        if (ccArray != null && ccArray.length != 0) {
            cc = getCommaSeparatedStringFromStringsArray(ccArray);
        }
        if (bccArray != null && bccArray.length != 0) {
            bcc = getCommaSeparatedStringFromStringsArray(bccArray);
        }
        if (toArray.length != 0) {
            to = getCommaSeparatedStringFromStringsArray(toArray);
        }
        Properties props = new Properties();
        props.put("mail.smtp.host", ProjectConstants.smtpServer);
        props.put("mail.from", ProjectConstants.applicationEmailAddress);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", true);
        props.put("mail.port", ProjectConstants.smtpPort);
        // TO DO- generalize this part
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ProjectConstants.applicationEmailAddress, ProjectConstants.emailPassword);
            }
        };
        Session session = Session.getInstance(props, auth);
        MimeMessage mimeMessage = new MimeMessage(session);
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(ProjectConstants.applicationEmailAddress);
        if (!to.isEmpty()) {
            InternetAddress[] toList = InternetAddress.parse(to);
            helper.setTo(toList);
        }
        if (!cc.isEmpty()) {
            InternetAddress[] ccList = InternetAddress.parse(cc);
            helper.setCc(ccList);
        }
        if (!bcc.isEmpty()) {
            InternetAddress[] bccList = InternetAddress.parse(bcc);
            helper.setBcc(bccList);
        }
        if (filePaths != null && filePaths.length != 0) {
            for (String filePath : filePaths) {
                if (filePath != null) {
                    FileSystemResource file = new FileSystemResource(filePath);
                    helper.addAttachment(file.getFilename(), file);
                }
            }
        }
        helper.setSubject(subject);
        helper.setSentDate(new Date());
        helper.setText(message);
        return mimeMessage;
    }

    private void detectInvalidAddress(MessagingException messageException) {
        if (messageException instanceof SendFailedException) {
            SendFailedException sfe = (SendFailedException) messageException;
            Address[] invalidAddresses = sfe.getInvalidAddresses();
            StringBuilder addressStr = new StringBuilder();
            for (Address address : invalidAddresses) {
                addressStr.append(address.toString()).append("; ");
            }

            System.out.println("invalid address(es)：{}" + addressStr);
            return;
        }
        System.out.println("exception while sending mail." + messageException);
    }

    public String getCommaSeparatedStringFromStringsArray(String[] collectionOfStrings) {
        StringBuilder result = new StringBuilder();
        for (String string : collectionOfStrings) {
            result.append(string);
            result.append(",");
        }
        return result.length() > 0 ? result.substring(0, result.length() - 1) : "";
    }

    static public String getAlphaNumericString(int n) {
        // chose a Character random from this String
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        // create StringBuffer size of AlphaNumericString
        StringBuilder sb = new StringBuilder(n);
        for (int i = 0; i < n; i++) {
            // generate a random number between
            // 0 to AlphaNumericString variable length
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());
            // add Character one by one in end of sb
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }

    public void sendDocumentsByFFWithAttachment(String[] toList, MultipartFile[] attachments, HttpSession session) {
        for (int i = 0; i < attachments.length; i++) {
            fileUploadService.store(attachments[i], "File_" + i, session);
        }
    }

    //  new mail send function  with dynamic from, to and file upload
    public HttpStatus sendMailWithAttachmentNew(String fromAddress, String fromPassword, String[] toArray, String[] ccArray, String[] bccArray, String subject, String message, Object[] arr) {
        /*String[] filePaths = new String[arr.length];*/
        try {
            if (arr != null) {
                String[] filePaths = Arrays.copyOf(arr, arr.length, String[].class);
                MimeMessage mimeMessage = createCustomMimeMessageNew(fromAddress, fromPassword, toArray, ccArray, bccArray, subject, message, filePaths);
                Transport.send(mimeMessage);
                System.out.println("Mail Sent");
                return HttpStatus.OK;
            } else {
                MimeMessage mimeMessage = createCustomMimeMessageNew(fromAddress, fromPassword, toArray, ccArray, bccArray, subject, message, null);
                Transport.send(mimeMessage);
                System.out.println("Mail Sent");
                return HttpStatus.OK;
            }
        } catch (MessagingException mex) {
            detectInvalidAddress(mex);
            System.out.println("send failed, exception: " + mex);
            return HttpStatus.NOT_FOUND;
        }
    }

    private MimeMessage createCustomMimeMessageNew(String fromAddress, String fromPassword, String[] toArray, String[] ccArray, String[] bccArray, String subject, String message, String[] filePaths) throws MessagingException {
        String cc = "";
        String bcc = "";
        String to = "";
        if (ccArray != null && ccArray.length != 0 && !ccArray[0].contains("undefined")) {
            cc = getCommaSeparatedStringFromStringsArray(ccArray);
        }
        if (bccArray != null && bccArray.length != 0 && !bccArray[0].contains("undefined")) {
            bcc = getCommaSeparatedStringFromStringsArray(bccArray);
        }
        if (toArray.length != 0 && !toArray[0].contains("undefined")) {
            to = getCommaSeparatedStringFromStringsArray(toArray);
        }
        Properties props = new Properties();
        props.put("mail.smtp.host", ProjectConstants.smtpServer);
//        props.put("mail.smtp.host", "mail.sanjaliconsultants.com");
        props.put("mail.from", ProjectConstants.applicationEmailAddress);
        props.put("mail.smtp.starttls.enable", true);
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.ssl.enable", true);
        props.put("mail.port", ProjectConstants.smtpPort);
        // TO DO- generalize this part
        Authenticator auth = new Authenticator() {
            //override the getPasswordAuthentication method
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(ProjectConstants.applicationEmailAddress, ProjectConstants.emailPassword);
            }
        };
        Session session = Session.getInstance(props, auth);
        MimeMessage mimeMessage = new MimeMessage(session);
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true);
        helper.setFrom(ProjectConstants.applicationEmailAddress);
        if (!to.isEmpty()) {
            InternetAddress[] toList = InternetAddress.parse(to);
            helper.setTo(toList);
        }
        if (!cc.isEmpty()) {
            InternetAddress[] ccList = InternetAddress.parse(cc);
            helper.setCc(ccList);
        }
        if (!bcc.isEmpty()) {
            InternetAddress[] bccList = InternetAddress.parse(bcc);
            helper.setBcc(bccList);
        }
        if (filePaths != null && filePaths.length != 0 && !filePaths[0].contains("undefined")) {
            for (String filePath : filePaths) {
                if (filePath != null) {
                    FileSystemResource file = new FileSystemResource(filePath);
                    helper.addAttachment(file.getFilename(), file);
                }
            }
        }
        helper.setSubject(subject);
        helper.setSentDate(new Date());
        helper.setText(message);
        return mimeMessage;
    }

   /* public void sendShipmentRefNumberGenerationMail(Long shipmentId) {
        shipmentService.sendMailOfReferenceNumberGenerate(shipmentService.shipmentRepo.findById(shipmentId).orElse(null));
    }*/

}

