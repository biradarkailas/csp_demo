package com.example.validation.service.impl;

import com.example.validation.dto.RscDtFGTypeDTO;
import com.example.validation.mapper.RscDtFGTypeMapper;
import com.example.validation.repository.RscDtFGTypeRepository;
import com.example.validation.service.RscDtFGTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtFGTypeServiceImpl implements RscDtFGTypeService {
    private final RscDtFGTypeRepository rscDtFGTypeRepository;
    private final RscDtFGTypeMapper rscDtFGTypeMapper;

    public RscDtFGTypeServiceImpl(RscDtFGTypeRepository rscDtFGTypeRepository, RscDtFGTypeMapper rscDtFGTypeMapper) {
        this.rscDtFGTypeRepository = rscDtFGTypeRepository;
        this.rscDtFGTypeMapper = rscDtFGTypeMapper;
    }

    @Override
    public Long getRscDtFGTypeIdByType(String type) {
        return rscDtFGTypeMapper.toDto(rscDtFGTypeRepository.findByType(type)).getId();
    }

    @Override
    public List<RscDtFGTypeDTO> findAll() {
        return rscDtFGTypeMapper.toDto(rscDtFGTypeRepository.findAll());
    }

    @Override
    public RscDtFGTypeDTO findById(Long id) {
        return rscDtFGTypeRepository.findById(id).map(rscDtFGTypeMapper::toDto).orElse(null);
    }
}