package com.example.validation.service.impl;

import com.example.validation.dto.RscDtMoqDTO;
import com.example.validation.mapper.RscDtMoqMapper;
import com.example.validation.repository.RscDtMoqRepository;
import com.example.validation.service.RscDtMoqService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtMoqServiceImpl implements RscDtMoqService {
    private final RscDtMoqRepository rscDtMoqRepository;
    private final RscDtMoqMapper rscDtMoqMapper;

    public RscDtMoqServiceImpl(RscDtMoqRepository rscDtMoqRepository, RscDtMoqMapper rscDtMoqMapper){
        this.rscDtMoqRepository = rscDtMoqRepository;
        this.rscDtMoqMapper = rscDtMoqMapper;
    }

    @Override
    public List<RscDtMoqDTO> findAll() {
        return rscDtMoqMapper.toDto(rscDtMoqRepository.findAll());
    }
}
