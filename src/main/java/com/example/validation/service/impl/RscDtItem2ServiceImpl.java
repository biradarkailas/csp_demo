package com.example.validation.service.impl;

import com.example.validation.dto.RscDtItem2DTO;
import com.example.validation.mapper.RscDtItem2Mapper;
import com.example.validation.repository.RscDtItem2Repository;
import com.example.validation.service.RscDtItem2Service;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class RscDtItem2ServiceImpl implements RscDtItem2Service {
    private final RscDtItem2Repository rscDtItem2Repository;
    private final RscDtItem2Mapper rscDtItem2Mapper;

    public RscDtItem2ServiceImpl(RscDtItem2Repository rscDtItem2Repository, RscDtItem2Mapper rscDtItem2Mapper) {
        this.rscDtItem2Repository = rscDtItem2Repository;
        this.rscDtItem2Mapper = rscDtItem2Mapper;
    }

    @Override
    public String get8DigitCode(String fgItemCode) {
        RscDtItem2DTO rscDtItem2DTO = rscDtItem2Mapper.toDto(rscDtItem2Repository.findByBpiCode(fgItemCode));
        if (Optional.ofNullable(rscDtItem2DTO).isPresent()) {
            if (rscDtItem2DTO.getBpiCode().equals(rscDtItem2DTO.getBaseCode())) {
                return rscDtItem2DTO.getBaseCode().substring(0, 6);
            } else {
                if (rscDtItem2DTO.getBaseCode().substring(rscDtItem2DTO.getBaseCode().length() - 1).equals("0")) {
                    return rscDtItem2DTO.getBaseCode().split("-")[0];
                } else {
                    return rscDtItem2DTO.getBaseCode();
                }
            }
        }
        return null;
    }
}