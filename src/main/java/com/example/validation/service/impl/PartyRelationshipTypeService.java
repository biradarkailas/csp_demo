package com.example.validation.service.impl;

import com.example.validation.domain.PartyRelationshipType;
import com.example.validation.repository.PartyRelationshipTypeRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PartyRelationshipTypeService {
    @Autowired
    PartyRelationshipTypeRepo partyRelationshipTypeRepo;

    public PartyRelationshipType save(PartyRelationshipType partyRelationshipType) {
        return partyRelationshipTypeRepo.save(partyRelationshipType);
    }

    public List<PartyRelationshipType> findAll() {
        return partyRelationshipTypeRepo.findAll();
    }

    public PartyRelationshipType findByName(String name) {
        return partyRelationshipTypeRepo.findByName(name);
    }
}
