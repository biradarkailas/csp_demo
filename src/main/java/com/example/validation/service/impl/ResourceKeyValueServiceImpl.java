package com.example.validation.service.impl;

import com.example.validation.domain.ResourceKeyValue;
import com.example.validation.mapper.ResourceKeyValueMapper;
import com.example.validation.repository.ResourceKeyValueRepository;
import com.example.validation.service.ResourceKeyValueService;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ResourceKeyValueServiceImpl implements ResourceKeyValueService {
    private final ResourceKeyValueRepository resourceKeyValueRepository;
    private final ResourceKeyValueMapper resourceKeyValueMapper;

    public ResourceKeyValueServiceImpl(ResourceKeyValueRepository resourceKeyValueRepository, ResourceKeyValueMapper resourceKeyValueMapper) {
        this.resourceKeyValueRepository = resourceKeyValueRepository;
        this.resourceKeyValueMapper = resourceKeyValueMapper;
    }

    @Override
    public String getResourceValueByKey(String key) {
        Optional<ResourceKeyValue> optionalResourceKeyValue = resourceKeyValueRepository.findByResourceKeyAndIsDelete(key, false);
        if (optionalResourceKeyValue.isPresent()) {
            return resourceKeyValueMapper.toDto(optionalResourceKeyValue.get()).getResourceValue();
        }
        return null;
    }
}
