package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.domain.RscIotRmCoverDays;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.RmCoverDaysMapper;
import com.example.validation.mapper.RscIotRmCoverDaysMapper;
import com.example.validation.repository.RscIotRmCoverDaysRepository;
import com.example.validation.service.*;
import com.example.validation.util.CoverDaysMonthUtils;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.MathUtils;
import com.example.validation.util.enums.CoverDaysClassNameRange;
import com.example.validation.util.excel_export.RmCoverDaysExporter;
import com.example.validation.util.excel_export.RmCoverDaysSummaryExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotRmCoverDaysServiceImpl implements RscIotRmCoverDaysService {
    private final RscIotRmCoverDaysRepository rscIotRmCoverDaysRepository;
    private final RscIotRmCoverDaysMapper rscIotRmCoverDaysMapper;
    private final RscIotRMSlobService rscIotRMSlobService;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RscMtItemService rscMtItemService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService;
    private final RmCoverDaysMapper rmCoverDaysMapper;
    private final CoverDaysMonthUtils coverDaysMonthUtils;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;

    public RscIotRmCoverDaysServiceImpl(RscIotRmCoverDaysRepository rscIotRmCoverDaysRepository, RscIotRmCoverDaysMapper rscIotRmCoverDaysMapper, RscIotRMSlobService rscIotRMSlobService, RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RscMtItemService rscMtItemService, RscMtPpheaderService rscMtPpheaderService, RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService, RmCoverDaysMapper rmCoverDaysMapper, CoverDaysMonthUtils coverDaysMonthUtils, RscIotRmSlobSummaryService rscIotRmSlobSummaryService) {
        this.rscIotRmCoverDaysRepository = rscIotRmCoverDaysRepository;
        this.rscIotRmCoverDaysMapper = rscIotRmCoverDaysMapper;
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rscMtItemService = rscMtItemService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotRmCoverDaysSummaryService = rscIotRmCoverDaysSummaryService;
        this.rmCoverDaysMapper = rmCoverDaysMapper;
        this.coverDaysMonthUtils = coverDaysMonthUtils;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
    }

    public void calculateRscIotRmCoverDays(Long rscMtPPHeaderId) {
        if (rscIotRmCoverDaysRepository.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
            List<RscIotRmCoverDaysDTO> rscIotRmCoverDaysDTOList = new ArrayList<>();
            List<RscIotRmSlobDTO> rscIotRmSlobDTOList = rscIotRMSlobService.findAllRMSlobsByRscMtPpHeaderId(rscMtPPHeaderId);
            Double Rate;
            Double totalMonthConsumption = 0.0;
            if (Optional.ofNullable(rscIotRmSlobDTOList).isPresent()) {
                for (RscIotRmSlobDTO rscIotRmSlobDTO : rscIotRmSlobDTOList) {
                    Rate = null;
                    RscItGrossConsumptionDTO rmGrossConsumptionDto = rscItRMGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(rscMtPPHeaderId, rscIotRmSlobDTO.getRscMtItemId());
                        RscIotRmCoverDaysDTO rscIotRmCoverDaysDTO = new RscIotRmCoverDaysDTO();
                        rscIotRmCoverDaysDTO.setRscMtItemId(rscIotRmSlobDTO.getRscMtItemId());
                        rscIotRmCoverDaysDTO.setRscMtPPHeaderId(rscMtPPHeaderId);
                        if (Optional.ofNullable(rscIotRmSlobDTO.getMapPrice()).isPresent()) {
                            rscIotRmCoverDaysDTO.setMap(rscIotRmSlobDTO.getMapPrice());
                            Rate = rscIotRmSlobDTO.getMapPrice();
                        }
                        RscMtItemDTO rscMtItemDTO = rscMtItemService.findById(rscIotRmSlobDTO.getRscMtItemId());
                        if (Optional.ofNullable(rscMtItemDTO).isPresent()) {
                            if (Optional.ofNullable(rscMtItemDTO.getStdPrice()).isPresent()) {
                                rscIotRmCoverDaysDTO.setStdPrice(rscMtItemDTO.getStdPrice());
                                Rate = rscMtItemDTO.getStdPrice();
                            }
                        }
                        if (Optional.ofNullable(rscIotRmSlobDTO.getStockQuantity()).isPresent()) {
                            rscIotRmCoverDaysDTO.setStockQuantity(DoubleUtils.getFormattedValue(rscIotRmSlobDTO.getStockQuantity()));
                            if (Optional.ofNullable(Rate).isPresent()) {
                                rscIotRmCoverDaysDTO.setStockValue(DoubleUtils.getFormattedValue(rscIotRmSlobDTO.getStockQuantity() * Rate));
                            }
                        }
                    if (Optional.ofNullable(Rate).isPresent()) {
                            rscIotRmCoverDaysDTO.setRate(Rate);
                    }
                        if (Optional.ofNullable(rmGrossConsumptionDto).isPresent()) {
                            rscIotRmCoverDaysDTO.setRscItRMGrossConsumptionId(rmGrossConsumptionDto.getId());
                            if (Optional.ofNullable(Rate).isPresent()) {
                                rscIotRmCoverDaysDTO.setRate(Rate);
                                rscIotRmCoverDaysDTO.setMonth1ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM1Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth2ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM2Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth3ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM3Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth4ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM4Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth5ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM5Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth6ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM6Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth7ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM7Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth8ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM8Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth9ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM9Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth10ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM10Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth11ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM11Value(), Rate));
                                rscIotRmCoverDaysDTO.setMonth12ConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getM12Value(), Rate));
                                rscIotRmCoverDaysDTO.setAllMonthsConsumptionValue(getMonthDetails(rmGrossConsumptionDto.getTotalValue(), Rate));
                                totalMonthConsumption += rscIotRmCoverDaysDTO.getAllMonthsConsumptionValue();
                                if (Rate != 0.0) {
                                    rscIotRmCoverDaysDTO.setRate(Rate);
                                }
                            }
                            else {
                                rscIotRmCoverDaysDTO=  setAllMonthsConsumptionValuesTozero(rscIotRmCoverDaysDTO);
                            }
                        }else {
                            rscIotRmCoverDaysDTO=  setAllMonthsConsumptionValuesTozero(rscIotRmCoverDaysDTO);
                        }

                        if (!Optional.ofNullable(rscIotRmSlobDTO.getStockQuantity()).isPresent()) {
                            if ( Rate == null) {
                                rscIotRmCoverDaysDTO.setRate(null);
                            }
                        }
                        rscIotRmCoverDaysDTOList.add(rscIotRmCoverDaysDTO);

                }
            }
            SaveAll(getConsumptionAndAggregatedPercentageCoverDaysList(rscIotRmCoverDaysDTOList, totalMonthConsumption));
        }
    }
    private RscIotRmCoverDaysDTO setAllMonthsConsumptionValuesTozero( RscIotRmCoverDaysDTO rscIotRmCoverDaysDTO){
        rscIotRmCoverDaysDTO.setMonth1ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth2ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth3ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth4ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth5ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth6ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth7ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth8ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth9ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth10ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth11ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setMonth12ConsumptionValue(0.0);
        rscIotRmCoverDaysDTO.setAllMonthsConsumptionValue(0.0);
        return rscIotRmCoverDaysDTO;
    }
    private List<RscIotRmCoverDaysDTO> getConsumptionAndAggregatedPercentageCoverDaysList(List<RscIotRmCoverDaysDTO> rscIotRmCoverDaysDTOList, Double totalMonthConsumption) {
        List<RscIotRmCoverDaysDTO> rmCoverDaysDTOList = rscIotRmCoverDaysDTOList.stream().sorted(Comparator.comparing(RscIotRmCoverDaysDTO::getAllMonthsConsumptionValue).reversed()).collect(Collectors.toList());
        Double currentConsumptionValue;
        Double previousConsumptionValue = 0.0;
        for (RscIotRmCoverDaysDTO rscIotRmCoverDaysDTO : rmCoverDaysDTOList) {
            currentConsumptionValue = getCurrentConsumptionValue(rscIotRmCoverDaysDTO.getAllMonthsConsumptionValue(), totalMonthConsumption);
            rscIotRmCoverDaysDTO.setConsumptionPercentage(getPercentage(currentConsumptionValue));
            rscIotRmCoverDaysDTO.setAggregatedPercentage(getPercentage(currentConsumptionValue + previousConsumptionValue));
            rscIotRmCoverDaysDTO.setClassName(getClassName(rscIotRmCoverDaysDTO.getAggregatedPercentage()));
            previousConsumptionValue = currentConsumptionValue + previousConsumptionValue;
        }
        return rmCoverDaysDTOList;
    }

    private String getClassName(Double aggregatedPercentage) {
        if (aggregatedPercentage <= CoverDaysClassNameRange.CLASS_A_RANGE_MAX_COUNT.getNumVal()) {
            return MaterialCategoryConstants.COVER_DAYS_CLASS_A;
        } else if (aggregatedPercentage >= CoverDaysClassNameRange.CLASS_B_RANGE_MIN_COUNT.getNumVal() && aggregatedPercentage <= CoverDaysClassNameRange.CLASS_B_RANGE_MAX_COUNT.getNumVal()) {
            return MaterialCategoryConstants.COVER_DAYS_CLASS_B;
        } else {
            return MaterialCategoryConstants.COVER_DAYS_CLASS_C;
        }
    }

    private Double getPercentage(Double consumptionValue) {
        return DoubleUtils.round(MathUtils.multiply(consumptionValue, 100));
    }

    private Double getCurrentConsumptionValue(Double monthsConsumptionValue, Double totalMonthConsumption) {
        return MathUtils.divide(monthsConsumptionValue, totalMonthConsumption);
    }

    private Double getMonthDetails(Double monthConsumptionQuantity, Double stdPrice) {
        return DoubleUtils.getTwoDecimalFormattedValue(monthConsumptionQuantity) * stdPrice;
    }

    private void SaveAll(List<RscIotRmCoverDaysDTO> rscIotPmCoverDaysDTO) {
        rscIotRmCoverDaysRepository.saveAll(rscIotRmCoverDaysMapper.toEntity(rscIotPmCoverDaysDTO));
    }


    public void calculateRscIotRmCoverDaysSummary(Long rscMtPPHeaderId) {
        if (rscIotRmCoverDaysSummaryService.countByRscMtPPheaderId(rscMtPPHeaderId) == 0) {
            RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO = new RscIotRmCoverDaysSummaryDTO();
            List<RscIotRmCoverDaysDTO> rscItRmCoverDaysLists = getRscItRmCoverDaysList(rscMtPPHeaderId);
            rscIotRmCoverDaysSummaryDTO.setRscMtPPHeaderId(rscMtPPHeaderId);
            Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
            if (Optional.ofNullable(rscItRmCoverDaysLists).isPresent()) {
                RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId(rscMtPPHeaderId);
                Double sitValue = 0.0;
                if (Optional.ofNullable(rscIotRmSlobSummaryDTO).isPresent()) {
                    sitValue = rscIotRmSlobSummaryDTO.getSitValue();
                }
                if (sitValue == 0.0) {
                    rscIotRmCoverDaysSummaryDTO = coverDaysMonthUtils.getRmCoverDaysWhenSitValueZero(rscItRmCoverDaysLists, rscIotRmCoverDaysSummaryDTO, pPheaderDTO, sitValue, rscIotRmSlobSummaryDTO);
                } else {
                    rscIotRmCoverDaysSummaryDTO = coverDaysMonthUtils.getRmCoverDaysWhenSitValueZero(rscItRmCoverDaysLists, rscIotRmCoverDaysSummaryDTO, pPheaderDTO, 0.0, rscIotRmSlobSummaryDTO);
                    coverDaysMonthUtils.getRmCoverDaysWhenSitValueZero(rscItRmCoverDaysLists, rscIotRmCoverDaysSummaryDTO, pPheaderDTO, sitValue, rscIotRmSlobSummaryDTO);
                }
            }
            saveCoverDaysSummary(rscIotRmCoverDaysSummaryDTO);
          /*  if (rscIotRmCoverDaysSummaryDTO.getSitValue() == 0.0) {
                rscIotRmCoverDaysClassSummaryService.calculateRmCoverDaysAllClassSummary(getRscItRmCoverDaysList(rscMtPPHeaderId), rscMtPPHeaderId);
            }*/
        }
    }

    @Override
    public List<RscIotRmCoverDaysDTO> getRscItRmCoverDaysList(Long rscMtPPHeaderId) {
        return rscIotRmCoverDaysMapper.toDto(rscIotRmCoverDaysRepository.findAllByRscMtPPheaderId(rscMtPPHeaderId));
    }

    private void saveCoverDaysSummary(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO) {
        rscIotRmCoverDaysSummaryService.saveCoverDaysSummary(rscIotRmCoverDaysSummaryDTO);
    }

    @Override
    public RmCoverDaysSummaryDTO getRmCoverDaysSummaryDetails(Long ppHeaderId) {
        return rscIotRmCoverDaysSummaryService.getRmCoverDaysSummaryDetailsofMonths(ppHeaderId);
    }

    @Override
    public RmCoverDaysDetailsDTO getRmCoverDaysDetails(Long ppHeaderId) {
        RmCoverDaysDetailsDTO rmCoverDaysDetailsDTO = new RmCoverDaysDetailsDTO();
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            rmCoverDaysDetailsDTO.setMpsId(pPheaderDTO.get().getId());
            rmCoverDaysDetailsDTO.setMpsName(pPheaderDTO.get().getMpsName());
            rmCoverDaysDetailsDTO.setMpsDate(pPheaderDTO.get().getMpsDate());
            List<RscIotRmCoverDays> rscIotRmCoverDays = rscIotRmCoverDaysRepository.findAllByRscMtPPheaderId(ppHeaderId);
            if (Optional.ofNullable(rscIotRmCoverDays).isPresent()) {
                rmCoverDaysDetailsDTO.setRmCoverDaysDTO(rmCoverDaysMapper.toDto(rscIotRmCoverDays));
            }
        }
        return rmCoverDaysDetailsDTO;
    }

    @Override
    public CoverDaysSummaryDetailsDTO getRmCoverDaysSummaryAllDetails(Long ppHeaderId) {
        CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO = new CoverDaysSummaryDetailsDTO();
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO = rscIotRmCoverDaysSummaryService.getRmCoverDaysSummary(ppHeaderId);
            if (Optional.ofNullable(rscIotRmCoverDaysSummaryDTO).isPresent()) {
                coverDaysSummaryDetailsDTO = rscIotRmCoverDaysSummaryService.getRmCoverDaysSummaryAllDetails(ppHeaderId);
                coverDaysSummaryDetailsDTO.setCoverDaysInMonthDTO(coverDaysMonthUtils.getCoverDaysOfAllMonths(pPheaderDTO.get().getMpsDate()));
              /*  coverDaysSummaryDetailsDTO.setTotalCoverDays(rscIotRmCoverDaysSummaryDTO.getTotalCoverDaysWithoutSit());
                coverDaysSummaryDetailsDTO.setTotalCoverDaysWithSit(rscIotRmCoverDaysSummaryDTO.getTotalCoverDaysWithSit());
*/
                coverDaysSummaryDetailsDTO.setOpeningMonthValue(DateUtils.getCurrentMonthName(pPheaderDTO.get().getMpsDate()));
                coverDaysSummaryDetailsDTO.setTotalDaysOfMonths(coverDaysMonthUtils.getTotalDaysOfAllMonths(pPheaderDTO.get().getMpsDate()));
            }
        }
        return coverDaysSummaryDetailsDTO;
    }

   /* private Integer getTotalCoverDays(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO) {
        return rscIotRmCoverDaysSummaryDTO.getMonth1CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth2CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth3CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth4CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth5CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth6CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth7CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth8CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth9CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth10CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth11CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth12CoverDays();
    }*/


    @Override
    public RmCoverDaysDetailsDTO getNullStdPriceDetails(Long rscMtPPHeaderId) {
        RmCoverDaysDetailsDTO rmCoverDaysDetailsDTO = new RmCoverDaysDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(rscMtPPHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            rmCoverDaysDetailsDTO.setMpsId(rscMtPPheaderDTO.get().getId());
            rmCoverDaysDetailsDTO.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
            rmCoverDaysDetailsDTO.setMpsName(rscMtPPheaderDTO.get().getMpsName());
            rmCoverDaysDetailsDTO.setRmCoverDaysDTO(getRmSlobDetails(rscMtPPHeaderId));
        }
        return rmCoverDaysDetailsDTO;
    }

    private List<RmCoverDaysDTO> getRmSlobDetails(Long rscMtPPHeaderId) {
        List<RmCoverDaysDTO> coverDaysDTOList = rmCoverDaysMapper.toDto(rscIotRmCoverDaysRepository.findAllByRscMtPPheaderIdAndRateIsNull(rscMtPPHeaderId));
        List<RmCoverDaysDTO> rmCoverDaysDTOList = new ArrayList<>();
        if (Optional.ofNullable(coverDaysDTOList).isPresent()) {
            rmCoverDaysDTOList.addAll(coverDaysDTOList);
        }
        return rmCoverDaysDTOList;
    }

    @Override
    public List<RmCoverDaysDTO> saveAllRmCoverDaysDetails(List<RmCoverDaysDTO> rmCoverDaysDTO) {
        List<RscIotRmCoverDays> rmCoverDaysList = new ArrayList<>();
        rmCoverDaysDTO.forEach(rmCoverDays -> {
            if (Optional.ofNullable(rmCoverDays.getStdPrice()).isPresent()) {
                rmCoverDays.setRate(rmCoverDays.getStdPrice());
                rmCoverDays.setStdPrice(rmCoverDays.getStdPrice());
                if (Optional.ofNullable(rmCoverDays.getStockQuantity()).isPresent()) {
                    rmCoverDays.setStockValue(DoubleUtils.round(rmCoverDays.getStockQuantity() * rmCoverDays.getStdPrice()));
                }
                if (Optional.ofNullable(rmCoverDays.getCoverDaysMonthDetailsDTO()).isPresent()) {
                    rmCoverDays.setCoverDaysMonthDetailsDTO(getMonthsDetails(rmCoverDays.getCoverDaysMonthDetailsDTO(), rmCoverDays.getStdPrice()));
                }
            }
            rmCoverDaysList.add(rmCoverDaysMapper.toEntity(rmCoverDays));
        });
        rscIotRmCoverDaysRepository.saveAll(rmCoverDaysList);
        return rmCoverDaysDTO;
    }

    private CoverDaysMonthDetailsDTO getMonthsDetails(CoverDaysMonthDetailsDTO coverDaysMonthDetailsDTO, Double stdPrice) {
        coverDaysMonthDetailsDTO.setMonth1Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth1ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth2Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth2ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth3Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth3ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth4Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth4ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth5Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth5ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth6Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth6ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth7Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth7ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth8Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth8ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth9Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth9ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth10Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth10ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth11Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth11ConsumptionQuantity(), stdPrice));
        coverDaysMonthDetailsDTO.setMonth12Value(getMonthDetails(coverDaysMonthDetailsDTO.getMonth12ConsumptionQuantity(), stdPrice));
        return coverDaysMonthDetailsDTO;
    }

    @Override
    public void deleteById(Long ppHeaderId) {
        rscIotRmCoverDaysRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

   /* @Override
    public ClassWiseCoverDaysDetailsDTO getRmCoverDaysClassWiseSummaryDetails(Long ppHeaderId) {
        ClassWiseCoverDaysDetailsDTO classWiseCoverDaysDetailsDTO = new ClassWiseCoverDaysDetailsDTO();
        Optional<RscMtPPheaderDTO> pPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPHeaderDTO.isPresent()) {
            classWiseCoverDaysDetailsDTO = rscIotRmCoverDaysClassSummaryService.getAllClassConsumptionByPpHeaderId(ppHeaderId);
            classWiseCoverDaysDetailsDTO.setTotalCoverDaysConsumptionDTO(rscIotRmCoverDaysSummaryService.getTotalCoverDaysConsumption(ppHeaderId));
            classWiseCoverDaysDetailsDTO.setRscMtPPHeaderId(ppHeaderId);
        }
        return classWiseCoverDaysDetailsDTO;
    }*/

    @Override
    public ExcelExportFiles exportRmCoverDaysExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmCoverDaysExporter.rmCoverDaysToExcel(getRmCoverDaysDetails(rscMtPPheaderDTO.getId()), getRmCoverDaysSummaryDetails(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_COVER_DAYS + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

  /*  @Override
    public ExcelExportFiles rmCoverDaysSummaryExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO = getRmCoverDaysSummaryAllDetails(rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(RmCoverDaysSummaryExporter.rmCoverDaysSummaryToExcel(rscIotRMSlobService.getRmSlobSummaryDetails(rscMtPPheaderDTO.getId()), coverDaysSummaryDetailsDTO, getRmCoverDaysClassWiseSummaryDetails(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_SLOB_COVER_DAYS_SUMMARY + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }*/
}