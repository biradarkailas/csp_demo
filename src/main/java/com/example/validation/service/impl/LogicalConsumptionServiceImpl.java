package com.example.validation.service.impl;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscMtBomDTO;
import com.example.validation.dto.RscMtPPdetailsDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.service.*;
import com.example.validation.util.consumption.ConsumptionUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;

@Service
@Transactional
public class LogicalConsumptionServiceImpl implements LogicalConsumptionService {
    private final RscItWIPLogicalConsumptionService rscItWIPLogicalConsumptionService;
    private final RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService;
    private final RscItPMLogicalConsumptionService rscItPmLogicalConsumptionService;
    private final RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService;
    private final RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscMtBomService rscMtBomService;
    private final ConsumptionUtil consumptionUtil;
    private final RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService;
    private final RscItBPGrossConsumptionService rscItBPGrossConsumptionService;
    private final RscItSubBaseLogicalConsumptionService rscItSubBaseLogicalConsumptionService;

    public LogicalConsumptionServiceImpl(RscItPMLogicalConsumptionService rscItPmLogicalConsumptionService, RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService, RscMtPpdetailsService rscMtPpdetailsService, RscMtBomService rscMtBomService, ConsumptionUtil consumptionUtil, RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService, RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService, RscItBPGrossConsumptionService rscItBPGrossConsumptionService, RscItSubBaseLogicalConsumptionService rscItSubBaseLogicalConsumptionService, RscItWIPLogicalConsumptionService rscItWIPLogicalConsumptionService, RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService) {
        this.rscItWIPLogicalConsumptionService = rscItWIPLogicalConsumptionService;
        this.rscItWIPGrossConsumptionService = rscItWIPGrossConsumptionService;
        this.rscItPmLogicalConsumptionService = rscItPmLogicalConsumptionService;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscMtBomService = rscMtBomService;
        this.rscItRMLogicalConsumptionService = rscItRMLogicalConsumptionService;
        this.consumptionUtil = consumptionUtil;
        this.rscItBulkLogicalConsumptionService = rscItBulkLogicalConsumptionService;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
        this.rscItBPLogicalConsumptionService = rscItBPLogicalConsumptionService;
        this.rscItBPGrossConsumptionService = rscItBPGrossConsumptionService;
        this.rscItSubBaseLogicalConsumptionService = rscItSubBaseLogicalConsumptionService;
    }

    @Override
    public void createWIPLogicalConsumption(Long ppHeaderId) {
        if (rscItWIPLogicalConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> wipRscItLogicalConsumptionDTOs = new ArrayList<>();
            for (RscMtPPdetailsDTO rscMtPPdetailsDTO : rscMtPpdetailsService.findAllPPdetailsByPPheaderAndNoCoPackingItem(ppHeaderId)) {
                List<RscItLogicalConsumptionDTO> logicalConsumptionByParentItem = getAllWIPLogicalConsumptionByParentItem(rscMtPPdetailsDTO.getRscMtItemId(), rscMtPPdetailsDTO, ppHeaderId);
                wipRscItLogicalConsumptionDTOs.addAll(logicalConsumptionByParentItem);
            }
            rscItWIPLogicalConsumptionService.saveAll(wipRscItLogicalConsumptionDTOs, ppHeaderId);
        }
    }

    @Override
    public void createLogicalConsumption(Long ppHeaderId) {
        if (rscItPmLogicalConsumptionService.count(ppHeaderId) == 0 && rscItBulkLogicalConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> pmRscItLogicalConsumptionDTOs = new ArrayList<>();
            List<RscItLogicalConsumptionDTO> bulkRscItLogicalConsumptionDTOs = new ArrayList<>();
            for (RscMtPPdetailsDTO rscMtPPdetailsDTO : rscMtPpdetailsService.findAllPPdetailsByPPheaderAndNoCoPackingItem(ppHeaderId)) {
                Map<String, List<RscItLogicalConsumptionDTO>> childMap = getAllItemLogicalConsumptionByParentItem(rscMtPPdetailsDTO.getMpsDate(), rscMtPPdetailsDTO.getRscMtItemId(), rscMtPPdetailsDTO, null, ppHeaderId);
                pmRscItLogicalConsumptionDTOs.addAll(childMap.get(MaterialCategoryConstants.PM));
                bulkRscItLogicalConsumptionDTOs.addAll(childMap.get(MaterialCategoryConstants.BULK));
            }
            List<RscItGrossConsumptionDTO> wipGrossConsumptionDTOs = rscItWIPGrossConsumptionService.findAllByRscMtPPheaderId(ppHeaderId);
            Map<String, List<RscItLogicalConsumptionDTO>> childMapByWIP = getAllItemLogicalConsumptionByWIPItem(wipGrossConsumptionDTOs);
            if (Optional.ofNullable(childMapByWIP).isPresent()) {
                pmRscItLogicalConsumptionDTOs.addAll(childMapByWIP.get(MaterialCategoryConstants.PM));
                bulkRscItLogicalConsumptionDTOs.addAll(childMapByWIP.get(MaterialCategoryConstants.BULK));
            }
            rscItPmLogicalConsumptionService.saveAll(pmRscItLogicalConsumptionDTOs, ppHeaderId);
            rscItBulkLogicalConsumptionService.saveAll(bulkRscItLogicalConsumptionDTOs, ppHeaderId);
        }
    }

    @Override
    public void createBPLogicalConsumption(Long ppHeaderId) {
        if (rscItBPLogicalConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOs = new ArrayList<>();
            for (RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO : rscItBulkGrossConsumptionService.findAllByRscMtPPheaderId(ppHeaderId)) {
                List<RscItLogicalConsumptionDTO> logicalConsumptionByParentItem = getAllBPLogicalConsumptionByParentItem(bulkRscItGrossConsumptionDTO.getRscMtItemId(), bulkRscItGrossConsumptionDTO.getMpsDate(), bulkRscItGrossConsumptionDTO, ppHeaderId);
                bpRscItLogicalConsumptionDTOs.addAll(logicalConsumptionByParentItem);
            }
            rscItBPLogicalConsumptionService.saveAll(bpRscItLogicalConsumptionDTOs, ppHeaderId);
            rscItBPLogicalConsumptionService.saveAllUnique(consumptionUtil.getUniqueLogicalConsumptionDTOs(bpRscItLogicalConsumptionDTOs), ppHeaderId);
        }
    }

    @Override
    public void createSubBaseLogicalConsumption(Long ppHeaderId) {
        if (rscItSubBaseLogicalConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> subBaseRscItLogicalConsumptionDTOs = new ArrayList<>();
            List<Long> bpItemIds = rscItBPGrossConsumptionService.findAllItemIdByRscMtPPHeaderId(ppHeaderId);
            for (RscItGrossConsumptionDTO bpRscItGrossConsumptionDTO : rscItBPGrossConsumptionService.findAllByRscMtPPHeaderId(ppHeaderId)) {
                subBaseRscItLogicalConsumptionDTOs.addAll(getAllSubBaseLogicalConsumptionByParentItem(bpRscItGrossConsumptionDTO.getRscMtItemId(), bpRscItGrossConsumptionDTO.getMpsDate(), bpRscItGrossConsumptionDTO, null, bpItemIds, ppHeaderId));
                rscItSubBaseLogicalConsumptionService.saveAll(subBaseRscItLogicalConsumptionDTOs);
            }
        }
    }

    @Override
    public void createRMLogicalConsumption(Long ppHeaderId) {
        if (rscItRMLogicalConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOs = new ArrayList<>();
            for (RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO : rscItBulkGrossConsumptionService.findAllGrossConsumptionDTOByRscMtPPheaderId(ppHeaderId)) {
                List<RscItLogicalConsumptionDTO> logicalConsumptionByParentItem = getAllRMLogicalConsumptionByParentItem(bulkRscItGrossConsumptionDTO.getRscMtItemId(), bulkRscItGrossConsumptionDTO, ppHeaderId);
                rmRscItLogicalConsumptionDTOs.addAll(logicalConsumptionByParentItem);
            }
            rscItRMLogicalConsumptionService.saveAll(rmRscItLogicalConsumptionDTOs, ppHeaderId);
            rscItRMLogicalConsumptionService.saveAllUnique(consumptionUtil.getUniqueLogicalConsumptionDTOs(rmRscItLogicalConsumptionDTOs), ppHeaderId);
        }
    }

    private Map<String, List<RscItLogicalConsumptionDTO>> getAllItemLogicalConsumptionByWIPItem(List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs) {
        List<RscItLogicalConsumptionDTO> pmRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscItLogicalConsumptionDTO> bulkRscItLogicalConsumptionDTOs = new ArrayList<>();
        Map<String, List<RscItLogicalConsumptionDTO>> parentMap = new HashMap<>();
        if (Optional.ofNullable(rscItGrossConsumptionDTOs).isPresent()) {
            for (RscItGrossConsumptionDTO wipRscItGrossConsumptionDTO : rscItGrossConsumptionDTOs) {
                Map<String, List<RscItLogicalConsumptionDTO>> childMap = getAllItemLogicalConsumptionByParentItem(wipRscItGrossConsumptionDTO.getMpsDate(), wipRscItGrossConsumptionDTO.getRscMtItemId(), null, wipRscItGrossConsumptionDTO, wipRscItGrossConsumptionDTO.getRscMtPPheaderId());
                pmRscItLogicalConsumptionDTOs.addAll(childMap.get(MaterialCategoryConstants.PM));
                bulkRscItLogicalConsumptionDTOs.addAll(childMap.get(MaterialCategoryConstants.BULK));
            }
        }
        parentMap.put(MaterialCategoryConstants.PM, pmRscItLogicalConsumptionDTOs);
        parentMap.put(MaterialCategoryConstants.BULK, bulkRscItLogicalConsumptionDTOs);
        return parentMap;
    }

    private Map<String, List<RscItLogicalConsumptionDTO>> getAllItemLogicalConsumptionByParentItem(LocalDate mpsDate, Long parentItemId, RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItGrossConsumptionDTO wipRscItGrossConsumptionDTO, Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> pmRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscItLogicalConsumptionDTO> bulkRscItLogicalConsumptionDTOs = new ArrayList<>();
        Map<String, List<RscItLogicalConsumptionDTO>> parentMap = new HashMap<>();
        for (RscMtBomDTO rscMtBomDTO : rscMtBomService.findAllByRscMtItemParentId(parentItemId, ppHeaderId)) {
            if (isValidApplicationToDate(mpsDate, rscMtBomDTO.getApplicableTo())) {
                if (consumptionUtil.isPackingMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    pmRscItLogicalConsumptionDTOs.add(consumptionUtil.getLogicalConsumptionDTO(rscMtPPdetailsDTO, null, rscMtBomDTO, wipRscItGrossConsumptionDTO, false, true));
                } else if (consumptionUtil.isBulkMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    RscItLogicalConsumptionDTO bulkLogicalConsumptionDTO = consumptionUtil.getLogicalConsumptionDTO(rscMtPPdetailsDTO, null, rscMtBomDTO, wipRscItGrossConsumptionDTO, false, false);
                    bulkRscItLogicalConsumptionDTOs.add(bulkLogicalConsumptionDTO);
                }
            }
        }
        parentMap.put(MaterialCategoryConstants.PM, pmRscItLogicalConsumptionDTOs);
        parentMap.put(MaterialCategoryConstants.BULK, bulkRscItLogicalConsumptionDTOs);
        return parentMap;
    }

    private List<RscItLogicalConsumptionDTO> getAllWIPLogicalConsumptionByParentItem(Long parentItemId, RscMtPPdetailsDTO rscMtPPdetailsDTO, Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> wipRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomService.findAllByParentItemIdAndChildItemCategoryDescription(parentItemId, MaterialCategoryConstants.WIP_MATERIAL, ppHeaderId);
        if (Optional.ofNullable(rscMtBomDTOs).isPresent()) {
            for (RscMtBomDTO rscMtBomDTO : rscMtBomDTOs) {
                if (isValidApplicationToDate(rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO.getApplicableTo())) {
                    wipRscItLogicalConsumptionDTOs.add(consumptionUtil.getLogicalConsumptionDTO(rscMtPPdetailsDTO, null, rscMtBomDTO, null, false, true));
                }
            }
        }
        return wipRscItLogicalConsumptionDTOs;
    }

    private List<RscItLogicalConsumptionDTO> getAllRMLogicalConsumptionByParentItem(Long parentItemId, RscItGrossConsumptionDTO bulkGrossConsumptionDTO, Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomService.findAllByParentItemIdAndChildItemCategoryDescription(parentItemId, MaterialCategoryConstants.RAW_MATERIAL, ppHeaderId);
        if (Optional.ofNullable(rscMtBomDTOs).isPresent()) {
            for (RscMtBomDTO rscMtBomDTO : rscMtBomDTOs) {
                if (isValidApplicationToDate(bulkGrossConsumptionDTO.getMpsDate(), rscMtBomDTO.getApplicableTo())) {
                    rmRscItLogicalConsumptionDTOs.add(consumptionUtil.getLogicalConsumptionDTO(null, null, rscMtBomDTO, bulkGrossConsumptionDTO, true, false));
                }
            }
        }
        return rmRscItLogicalConsumptionDTOs;
    }

    private List<RscItLogicalConsumptionDTO> getAllBPLogicalConsumptionByParentItem(Long parentItemId, LocalDate mpsDate, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomService.findAllByParentItemIdAndChildItemCategoryDescription(parentItemId, MaterialCategoryConstants.SUB_BASES, ppHeaderId);
        if (Optional.ofNullable(rscMtBomDTOs).isPresent()) {
            for (RscMtBomDTO rscMtBomDTO : rscMtBomDTOs) {
                if (isValidApplicationToDate(mpsDate, rscMtBomDTO.getApplicableTo())) {
                    bpRscItLogicalConsumptionDTOs.add(consumptionUtil.getLogicalConsumptionDTO(null, null, rscMtBomDTO, bulkRscItGrossConsumptionDTO, true, false));
                }
            }
        }
        return bpRscItLogicalConsumptionDTOs;
    }

    private List<RscItLogicalConsumptionDTO> getAllSubBaseLogicalConsumptionByParentItem(Long parentItemId, LocalDate mpsDate, RscItGrossConsumptionDTO bpRscItGrossConsumptionDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, List<Long> bpItemIds, Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> subBaseRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomService.findAllByParentItemIdAndChildItemCategoryDescription(parentItemId, MaterialCategoryConstants.SUB_BASES, ppHeaderId);
        if (Optional.ofNullable(rscMtBomDTOs).isPresent()) {
            for (RscMtBomDTO rscMtBomDTO : rscMtBomDTOs) {
                if (isValidApplicationToDate(mpsDate, rscMtBomDTO.getApplicableTo())) {
                    RscItLogicalConsumptionDTO subBaseLogicalConsumptionDTO = consumptionUtil.getLogicalConsumptionDTO(null, rscItLogicalConsumptionDTO, rscMtBomDTO, bpRscItGrossConsumptionDTO, true, false);
                    if (bpItemIds.contains(rscMtBomDTO.getRscMtItemChildId())) {
                        updateBPGrossConsumption(ppHeaderId, subBaseLogicalConsumptionDTO, rscMtBomDTO.getRscMtItemChildId());
                    }
                    subBaseRscItLogicalConsumptionDTOs.add(subBaseLogicalConsumptionDTO);
                    List<RscItLogicalConsumptionDTO> childBaseLogicalConsumptionDTOs = getAllSubBaseLogicalConsumptionByParentItem(subBaseLogicalConsumptionDTO.getRscMtItemChildId(), mpsDate, null, subBaseLogicalConsumptionDTO, bpItemIds, ppHeaderId);
                    if (Optional.ofNullable(childBaseLogicalConsumptionDTOs).isPresent())
                        subBaseRscItLogicalConsumptionDTOs.addAll(childBaseLogicalConsumptionDTOs);
                }
            }
        }
        return subBaseRscItLogicalConsumptionDTOs;
    }

    private void updateBPGrossConsumption(Long ppHeaderId, RscItLogicalConsumptionDTO subBaseLogicalConsumptionDTO, Long bpItemId) {
        RscItGrossConsumptionDTO bpGrossConsumptionDTO = rscItBPGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, bpItemId);
        RscItGrossConsumptionDTO updatedBPGrossConsumptionDTO = getBPGrossConsumptionDTO(subBaseLogicalConsumptionDTO, bpGrossConsumptionDTO);
        rscItBPGrossConsumptionService.save(updatedBPGrossConsumptionDTO);
        updateSubBaseLogicalConsumptionAfterDeleteItem(ppHeaderId, bpItemId, updatedBPGrossConsumptionDTO);
    }

    private void updateSubBaseLogicalConsumptionAfterDeleteItem(Long ppHeaderId, Long bpItemId, RscItGrossConsumptionDTO updatedBPGrossConsumptionDTO) {
        rscItSubBaseLogicalConsumptionService.deleteByRscMtPPheaderIdAndRscMtParentItemId(ppHeaderId, bpItemId);
        rscItSubBaseLogicalConsumptionService.saveAll(getUpdatedSubBaseLogicalConsumptionByParentItem(bpItemId, updatedBPGrossConsumptionDTO.getMpsDate(), updatedBPGrossConsumptionDTO, ppHeaderId));
    }

    private List<RscItLogicalConsumptionDTO> getUpdatedSubBaseLogicalConsumptionByParentItem(Long parentItemId, LocalDate mpsDate, RscItGrossConsumptionDTO bpRscItGrossConsumptionDTO, Long ppHeaderId) {
        List<RscItLogicalConsumptionDTO> subBaseRscItLogicalConsumptionDTOs = new ArrayList<>();
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomService.findAllByParentItemIdAndChildItemCategoryDescription(parentItemId, MaterialCategoryConstants.SUB_BASES, ppHeaderId);
        if (Optional.ofNullable(rscMtBomDTOs).isPresent()) {
            for (RscMtBomDTO rscMtBomDTO : rscMtBomDTOs) {
                if (isValidApplicationToDate(mpsDate, rscMtBomDTO.getApplicableTo())) {
                    RscItLogicalConsumptionDTO subBaseLogicalConsumptionDTO = consumptionUtil.getLogicalConsumptionDTO(null, null, rscMtBomDTO, bpRscItGrossConsumptionDTO, true, false);
                    subBaseRscItLogicalConsumptionDTOs.add(subBaseLogicalConsumptionDTO);
                }
            }
        }
        return subBaseRscItLogicalConsumptionDTOs;
    }

    private RscItGrossConsumptionDTO getBPGrossConsumptionDTO(RscItLogicalConsumptionDTO subBaseLogicalConsumptionDTO, RscItGrossConsumptionDTO bpGrossConsumptionDTO) {
        bpGrossConsumptionDTO.setM1Value(bpGrossConsumptionDTO.getM1Value() + subBaseLogicalConsumptionDTO.getM1Value());
        bpGrossConsumptionDTO.setM2Value(bpGrossConsumptionDTO.getM2Value() + subBaseLogicalConsumptionDTO.getM2Value());
        bpGrossConsumptionDTO.setM3Value(bpGrossConsumptionDTO.getM3Value() + subBaseLogicalConsumptionDTO.getM3Value());
        bpGrossConsumptionDTO.setM4Value(bpGrossConsumptionDTO.getM4Value() + subBaseLogicalConsumptionDTO.getM4Value());
        bpGrossConsumptionDTO.setM5Value(bpGrossConsumptionDTO.getM5Value() + subBaseLogicalConsumptionDTO.getM5Value());
        bpGrossConsumptionDTO.setM6Value(bpGrossConsumptionDTO.getM6Value() + subBaseLogicalConsumptionDTO.getM6Value());
        bpGrossConsumptionDTO.setM7Value(bpGrossConsumptionDTO.getM7Value() + subBaseLogicalConsumptionDTO.getM7Value());
        bpGrossConsumptionDTO.setM8Value(bpGrossConsumptionDTO.getM8Value() + subBaseLogicalConsumptionDTO.getM8Value());
        bpGrossConsumptionDTO.setM9Value(bpGrossConsumptionDTO.getM9Value() + subBaseLogicalConsumptionDTO.getM9Value());
        bpGrossConsumptionDTO.setM10Value(bpGrossConsumptionDTO.getM10Value() + subBaseLogicalConsumptionDTO.getM10Value());
        bpGrossConsumptionDTO.setM11Value(bpGrossConsumptionDTO.getM11Value() + subBaseLogicalConsumptionDTO.getM11Value());
        bpGrossConsumptionDTO.setM12Value(bpGrossConsumptionDTO.getM12Value() + subBaseLogicalConsumptionDTO.getM12Value());
        bpGrossConsumptionDTO.setTotalValue(bpGrossConsumptionDTO.getTotalValue() + subBaseLogicalConsumptionDTO.getTotalValue());
        return bpGrossConsumptionDTO;
    }

    private boolean isValidApplicationToDate(LocalDate mpsDate, LocalDate applicationToDate) {
        if (applicationToDate != null && mpsDate != null) {
            return applicationToDate.isAfter(mpsDate);
        }
        return true;
    }
}