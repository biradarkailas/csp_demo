package com.example.validation.service.impl;

import com.example.validation.dto.RscItItemWiseSupplierDTO;
import com.example.validation.dto.RscMtItemDTO;
import com.example.validation.dto.RscMtPPdetailsDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.service.*;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.consumption.ConsumptionUtil;
import com.example.validation.util.consumption.GrossConsumptionMonthUtil;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class GrossConsumptionServiceImpl implements GrossConsumptionService {
    private final RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService;
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final RscItBPGrossConsumptionService rscItBPGrossConsumptionService;
    private final RscItSubBaseGrossConsumptionService rscItSubBaseGrossConsumptionService;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RscItWIPLogicalConsumptionService rscItWIPLogicalConsumptionService;
    private final RscItPMLogicalConsumptionService rscItPMLogicalConsumptionService;
    private final RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService;
    private final RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService;
    private final RscItSubBaseLogicalConsumptionService rscItSubBaseLogicalConsumptionService;
    private final RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService;
    private final ConsumptionUtil consumptionUtil;
    private final RscMtItemService rscMtItemService;
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscItItemWiseSupplierService rscItItemWiseSupplierService;

    public GrossConsumptionServiceImpl(ConsumptionUtil consumptionUtil, RscItPMLogicalConsumptionService rscItPMLogicalConsumptionService,
                                       RscItPMGrossConsumptionService rscItPMGrossConsumptionService, RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService,
                                       RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService,
                                       RscItBulkLogicalConsumptionService rscItBulkLogicalConsumptionService, RscMtItemService rscMtItemService,
                                       RscMtPpdetailsService rscMtPpdetailsService, RscItBPLogicalConsumptionService rscItBPLogicalConsumptionService,
                                       RscItBPGrossConsumptionService rscItBPGrossConsumptionService, RscItItemWiseSupplierService rscItItemWiseSupplierService,
                                       RscItSubBaseLogicalConsumptionService rscItSubBaseLogicalConsumptionService, RscItSubBaseGrossConsumptionService rscItSubBaseGrossConsumptionService,
                                       RscItWIPGrossConsumptionService rscItWIPGrossConsumptionService, RscItWIPLogicalConsumptionService rscItWIPLogicalConsumptionService) {
        this.rscItWIPLogicalConsumptionService = rscItWIPLogicalConsumptionService;
        this.rscItPMLogicalConsumptionService = rscItPMLogicalConsumptionService;
        this.rscItBulkLogicalConsumptionService = rscItBulkLogicalConsumptionService;
        this.rscItBPLogicalConsumptionService = rscItBPLogicalConsumptionService;
        this.rscItSubBaseLogicalConsumptionService = rscItSubBaseLogicalConsumptionService;
        this.rscItRMLogicalConsumptionService = rscItRMLogicalConsumptionService;
        this.rscItWIPGrossConsumptionService = rscItWIPGrossConsumptionService;
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
        this.rscItBPGrossConsumptionService = rscItBPGrossConsumptionService;
        this.rscItSubBaseGrossConsumptionService = rscItSubBaseGrossConsumptionService;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.consumptionUtil = consumptionUtil;
        this.rscMtItemService = rscMtItemService;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscItItemWiseSupplierService = rscItItemWiseSupplierService;
    }

    @Override
    public void createWIPGrossConsumption(Long ppHeaderId) {
        if (rscItWIPGrossConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> wipRscItLogicalConsumptionDTOs = rscItWIPLogicalConsumptionService.findAllByRscMtPPHeaderId(ppHeaderId);
            rscItWIPGrossConsumptionService.saveAll(getRscItGrossConsumptionDTOList(wipRscItLogicalConsumptionDTOs, false, true), ppHeaderId);
        }
    }

    @Override
    public void createGrossConsumption(Long ppHeaderId) {
        if (rscItPMGrossConsumptionService.count(ppHeaderId) == 0 && rscItBulkGrossConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> pmRscItLogicalConsumptionDTOs = rscItPMLogicalConsumptionService.findAllByRscMtPPheaderId(ppHeaderId);
            rscItPMGrossConsumptionService.saveAll(getRscItGrossConsumptionDTOList(pmRscItLogicalConsumptionDTOs, false, true), ppHeaderId);
            List<RscItLogicalConsumptionDTO> bulkRscItLogicalConsumptionDTOs = rscItBulkLogicalConsumptionService.findAllByRscMtPPheaderId(ppHeaderId);
            rscItBulkGrossConsumptionService.saveAll(getRscItGrossConsumptionDTOList(bulkRscItLogicalConsumptionDTOs, true, false), ppHeaderId);
        }
    }

    @Override
    public void createBPGrossConsumption(Long ppHeaderId) {
        if (rscItBPGrossConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> bpRscItLogicalConsumptionDTOs = rscItBPLogicalConsumptionService.findAllByRscMtPPHeaderId(ppHeaderId);
            rscItBPGrossConsumptionService.saveAll(getRscItGrossConsumptionDTOList(bpRscItLogicalConsumptionDTOs, true, false), ppHeaderId);
        }
    }

    @Override
    public void createSubBaseGrossConsumption(Long ppHeaderId) {
        if (rscItSubBaseGrossConsumptionService.count(ppHeaderId) == 0) {
            List<Long> bpGrossItemIds = rscItBPGrossConsumptionService.findAllItemIdByRscMtPPHeaderId(ppHeaderId);
            Set<Long> subBaseLogicalItemIds = rscItSubBaseLogicalConsumptionService.findAllItemIdByRscMtPPHeaderId(ppHeaderId);
            rscItSubBaseGrossConsumptionService.saveAll(getSubBaseRscItGrossConsumptionDTOs(bpGrossItemIds, subBaseLogicalItemIds, ppHeaderId), ppHeaderId);
        }
    }

    @Override
    public void createRMGrossConsumption(Long ppHeaderId) {
        if (rscItRMGrossConsumptionService.count(ppHeaderId) == 0) {
            List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOs = rscItRMLogicalConsumptionService.findAllByRscMtPPheaderId(ppHeaderId);
            rscItRMGrossConsumptionService.saveAll(getRscItGrossConsumptionDTOList(rmRscItLogicalConsumptionDTOs, false, false), ppHeaderId);
        }
    }

    private List<RscItGrossConsumptionDTO> getRscItGrossConsumptionDTOList(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, boolean isBulk, boolean isPM) {
        List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOS = new ArrayList<>();
        for (Long mtItemId : consumptionUtil.getDistinctLCMtItemId(rscItLogicalConsumptionDTOs)) {
            List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOsByMtItemId = rscItLogicalConsumptionDTOs.stream()
                    .filter(logicalConsumptionDTO -> mtItemId.equals(logicalConsumptionDTO.getRscMtItemChildId()))
                    .collect(Collectors.toList());
            RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO = rscItItemWiseSupplierService.findOneByRscMtItemIdAndAllocation(mtItemId);
            RscItGrossConsumptionDTO grossConsumptionDTO = getGrossConsumptionDTO(rscItLogicalConsumptionDTOsByMtItemId, isBulk, isPM);
            if (isPM)
                grossConsumptionDTO.setPriority(getPMPriority(rscItLogicalConsumptionDTOsByMtItemId.get(0)));
            grossConsumptionDTO.setTotalValue(DoubleUtils.getFormattedValue(getTwelveMonthConsumption(grossConsumptionDTO)));
            if (Optional.ofNullable(rscItItemWiseSupplierDTO).isPresent())
                grossConsumptionDTO.setItemWiseSupplierId(rscItItemWiseSupplierDTO.getId());
            rscItGrossConsumptionDTOS.add(grossConsumptionDTO);
        }
        return rscItGrossConsumptionDTOS;
    }

    private List<RscItGrossConsumptionDTO> getSubBaseRscItGrossConsumptionDTOs(List<Long> bpGrossItemIds, Set<Long> subBaseLogicalItemIds, Long ppHeaderId) {
        List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOS = new ArrayList<>();
        for (Long subBaseItemId : subBaseLogicalItemIds) {
            if (!bpGrossItemIds.contains(subBaseItemId)) {
                List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs = new ArrayList<>();
                rscItLogicalConsumptionDTOs.addAll(rscItSubBaseLogicalConsumptionService.findAllByRscMtPPHeaderIdAndChildItemId(ppHeaderId, subBaseItemId));
                RscItGrossConsumptionDTO grossConsumptionDTO = getGrossConsumptionDTO(rscItLogicalConsumptionDTOs, true, false);
                grossConsumptionDTO.setTotalValue(getTwelveMonthConsumption(grossConsumptionDTO));
                rscItGrossConsumptionDTOS.add(grossConsumptionDTO);
            }
        }
        return rscItGrossConsumptionDTOS;
    }

    private int getPMPriority(RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO) {
        if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
            RscMtPPdetailsDTO rscMtPPdetailsDTO = rscMtPpdetailsService.findOneRscMtPPdetailsDTO(rscItLogicalConsumptionDTO.getRscMtPPheaderId(), rscItLogicalConsumptionDTO.getRscMtItemMpsParentId());
            if (Optional.ofNullable(rscMtPPdetailsDTO).isPresent() && Optional.ofNullable(rscItLogicalConsumptionDTO.getChildItemCoverDays()).isPresent())
                return getPriorityValue(rscItLogicalConsumptionDTO.getChildItemCoverDays(), rscMtPPdetailsDTO.getM2Value());
        }
        return 0;
    }

    private int getPriorityValue(Double coverDays, Double m2Value) {
        if (coverDays < 50 && m2Value > 0)
            return 1;
        return 0;
    }

    private Double getTwelveMonthConsumption(RscItGrossConsumptionDTO grossConsumptionDTO) {
        return grossConsumptionDTO.getM1Value()
                + grossConsumptionDTO.getM2Value()
                + grossConsumptionDTO.getM3Value()
                + grossConsumptionDTO.getM4Value()
                + grossConsumptionDTO.getM5Value()
                + grossConsumptionDTO.getM6Value()
                + grossConsumptionDTO.getM7Value()
                + grossConsumptionDTO.getM8Value()
                + grossConsumptionDTO.getM9Value()
                + grossConsumptionDTO.getM10Value()
                + grossConsumptionDTO.getM11Value()
                + grossConsumptionDTO.getM12Value();
    }

    private RscItGrossConsumptionDTO getGrossConsumptionDTO(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, Boolean isBulk, Boolean isPM) {
        RscItGrossConsumptionDTO grossConsumptionDTO = new RscItGrossConsumptionDTO();
        if (Optional.ofNullable(rscItLogicalConsumptionDTOs).isPresent()) {
            RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO = rscItLogicalConsumptionDTOs.get(0);
            RscMtItemDTO rscMtItemDTO = rscMtItemService.findById(rscItLogicalConsumptionDTO.getRscMtItemChildId());
            grossConsumptionDTO.setRscMtItemId(rscItLogicalConsumptionDTO.getRscMtItemChildId());
            grossConsumptionDTO.setRscMtPPheaderId(rscItLogicalConsumptionDTO.getRscMtPPheaderId());
            grossConsumptionDTO.setItemCode(rscItLogicalConsumptionDTO.getRscMtItemChildCode());
            grossConsumptionDTO.setM1Value(GrossConsumptionMonthUtil.getM1GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM2Value(GrossConsumptionMonthUtil.getM2GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM3Value(GrossConsumptionMonthUtil.getM3GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM4Value(GrossConsumptionMonthUtil.getM4GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM5Value(GrossConsumptionMonthUtil.getM5GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM6Value(GrossConsumptionMonthUtil.getM6GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM7Value(GrossConsumptionMonthUtil.getM7GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM8Value(GrossConsumptionMonthUtil.getM8GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM9Value(GrossConsumptionMonthUtil.getM9GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM10Value(GrossConsumptionMonthUtil.getM10GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM11Value(GrossConsumptionMonthUtil.getM11GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
            grossConsumptionDTO.setM12Value(GrossConsumptionMonthUtil.getM12GrossConsumption(rscItLogicalConsumptionDTOs, rscMtItemDTO, isBulk, isPM));
        }
        return grossConsumptionDTO;
    }
}