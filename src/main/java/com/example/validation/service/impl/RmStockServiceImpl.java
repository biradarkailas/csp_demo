package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.domain.RscDtStockType;
import com.example.validation.dto.*;
import com.example.validation.service.*;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.StockUtil;
import com.example.validation.util.excel_export.*;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RmStockServiceImpl implements RmStockService {
    private final RscMtItemService rscMtItemService;
    private final RscMtRmMesService rscMtRmMesService;
    private final RscMtRmRtdService rscMtRmRtdService;
    private final RscIitRmBulkOpRmService rscIitRmBulkOpRmService;
    private final RscMtPpheaderService rscMtPpHeaderService;
    private final RscIotRmStockService rscIotRmStockService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscMtRmListService rscMtRmListService;
    private final RscMtRmOtherStocksService rscMtRmOtherStocksService;
    private final RscDtStockTypeService rscDtStockTypeService;


    public RmStockServiceImpl(RscMtItemService rscMtItemService, RscMtRmMesService rscMtRmMesService, RscMtRmRtdService rscMtRmRtdService, RscIitRmBulkOpRmService rscIitRmBulkOpRmService, RscMtPpheaderService rscMtPpHeaderService, RscIotRmStockService rscIotRmStockService, RscMtPpheaderService rscMtPpheaderService, RscMtRmListService rscMtRmListService, RscMtRmOtherStocksService rscMtRmOtherStocksService, RscDtStockTypeService rscDtStockTypeService) {
        this.rscMtItemService = rscMtItemService;
        this.rscMtRmMesService = rscMtRmMesService;
        this.rscMtRmRtdService = rscMtRmRtdService;
        this.rscIitRmBulkOpRmService = rscIitRmBulkOpRmService;
        this.rscMtPpHeaderService = rscMtPpHeaderService;
        this.rscIotRmStockService = rscIotRmStockService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscMtRmListService = rscMtRmListService;
        this.rscMtRmOtherStocksService = rscMtRmOtherStocksService;
        this.rscDtStockTypeService = rscDtStockTypeService;
    }

    @Override
    public RMStockDTO getAllRMStocksDTO(Long ppHeaderId, String stockType) {
        RMStockDTO rmStockDTO = new RMStockDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            rmStockDTO.setMpsId(rscMtPPheaderDTO.get().getId());
            rmStockDTO.setMpsDate(rscMtPPheaderDTO.get().getMpsDate());
            rmStockDTO.setMpsName(rscMtPPheaderDTO.get().getMpsName());
            List<RmStockItemCodeDTO> rmStockItemCodeDTOS = new ArrayList<>();

            List<RmStockItemCodeDTO> rmStockDTOList = rscIotRmStockService.findAllDistinctByRscMtItem();
            for (RmStockItemCodeDTO rmStockItemCodeDTO : rmStockDTOList) {
                var itemCode = getItemCodeList(ppHeaderId, rmStockItemCodeDTO, stockType, rscMtPPheaderDTO.get().getMpsDate());
                if (itemCode != null) {
                    rmStockItemCodeDTOS.add(itemCode);
                }
            }
            rmStockDTO.setItemCodes(rmStockItemCodeDTOS);
        }
        return rmStockDTO;
    }

    private RmStockItemCodeDTO getItemCodeList(Long ppHeaderId, RmStockItemCodeDTO rmStockItemCodeDTO, String stockType, LocalDate mpsDate) {
        if (Optional.ofNullable(rmStockItemCodeDTO).isPresent()) {
            RmStockItemCodeDTO itemCodeDTO = new RmStockItemCodeDTO();
            itemCodeDTO.setRscMtItemId(rmStockItemCodeDTO.getRscMtItemId());
            itemCodeDTO.setItemCode(rmStockItemCodeDTO.getItemCode());
            itemCodeDTO.setItemDescription(rmStockItemCodeDTO.getItemDescription());

            if (stockType != null) {
                switch (stockType) {
                    case MaterialCategoryConstants.MES_STOCK_TYPE:
                        itemCodeDTO.setRmMes(getRmMesDetails(rmStockItemCodeDTO.getRscMtItemId(), mpsDate));
                        if (itemCodeDTO.getRmMes() == null) {
                            return null;
                        }
                        break;
                    case MaterialCategoryConstants.RTD_STOCK_TYPE:
                        itemCodeDTO.setRmRtd(getRmRtdDetails(rmStockItemCodeDTO.getRscMtItemId(), mpsDate));
                        if (itemCodeDTO.getRmRtd() == null) {
                            return null;
                        }
                        break;
                    case MaterialCategoryConstants.BULK:
                        itemCodeDTO.setRmBulkOpRm(getBulkOpRmDetails(ppHeaderId, rmStockItemCodeDTO.getRscMtItemId(), mpsDate));
                        if (itemCodeDTO.getRmBulkOpRm() == null) {
                            return null;
                        }
                        break;
                    case MaterialCategoryConstants.RTD_TRANSFER_STOCK_TYPE:
                        itemCodeDTO.setRmTransferStock(getTransferStockDetails(ppHeaderId, rmStockItemCodeDTO.getRscMtItemId(), mpsDate));
                        if (itemCodeDTO.getRmTransferStock() == null) {
                            return null;
                        }
                        break;
                    case MaterialCategoryConstants.REJECTION:
                        itemCodeDTO.setRmRejection(getRejectionDetails(ppHeaderId, rmStockItemCodeDTO.getRscMtItemId(), mpsDate));
                        if (itemCodeDTO.getRmRejection() == null) {
                            return null;
                        }
                        break;
                }
            }
            return itemCodeDTO;
        } else {
            return null;
        }
    }

    @Override
    public RscStockDetailDTO getAllRMStocksDetailData(Long ppHeaderId, Long rscMtItemId) {
        RscStockDetailDTO rscStockDetailDTO = new RscStockDetailDTO();

        OpeningStockDTO openingStockDTO = new OpeningStockDTO();
        RscMtItemDTO rscMtItemDTO = rscMtItemService.findById(rscMtItemId);
        openingStockDTO.setStockTypes(getStockTypes(ppHeaderId, rscMtItemId));

        var itemCodeDetailDTO = getItemCodeDetailDTO(rscMtItemDTO, this.calculateTotalValue(openingStockDTO.getStockTypes()), openingStockDTO.getStockTypes());

        itemCodeDetailDTO.setOpeningStockDTO(openingStockDTO);
        rscStockDetailDTO.setItemCodeDetailDTO(itemCodeDetailDTO);
        return rscStockDetailDTO;
    }

    private StockTypeDTO getStockTypes(Long ppHeaderId, Long rscMtItemId) {
        StockTypeDTO stockTypeDTO = new StockTypeDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            stockTypeDTO.setRmMes(getRmMesDetails(rscMtItemId, rscMtPPheaderDTO.get().getMpsDate()));
            stockTypeDTO.setRmRtd(getRmRtdDetails(rscMtItemId, rscMtPPheaderDTO.get().getMpsDate()));
            stockTypeDTO.setRmBulkOpRm(getBulkOpRmDetails(ppHeaderId, rscMtItemId, rscMtPPheaderDTO.get().getMpsDate()));
            stockTypeDTO.setRmTransferStock(getTransferStockDetails(ppHeaderId, rscMtItemId, rscMtPPheaderDTO.get().getMpsDate()));
            stockTypeDTO.setRmRejection(getRejectionDetails(ppHeaderId, rscMtItemId, rscMtPPheaderDTO.get().getMpsDate()));
        }
        return stockTypeDTO;
    }

    private Double calculateTotalValue(StockTypeDTO stockTypeDTO) {
        return (stockTypeDTO.getRmMes().getTotalValue() + stockTypeDTO.getRmRtd().getTotalValue() + stockTypeDTO.getRmBulkOpRm().getTotalValue() + stockTypeDTO.getRmTransferStock().getTotalValue()) - stockTypeDTO.getRmRejection().getTotalValue();
    }

    @Override
    public RMStockDTO getRmStockWithMes(Long ppHeaderId) {
        return getAllRMStocksDTO(ppHeaderId, MaterialCategoryConstants.MES_STOCK_TYPE);
    }

    @Override
    public RMStockDTO getRmStockWithRtd(Long ppHeaderId) {
        return getAllRMStocksDTO(ppHeaderId, MaterialCategoryConstants.RTD_STOCK_TYPE);
    }

    @Override
    public RMStockDTO getRmStockWithTransferStock(Long ppHeaderId) {
        return getAllRMStocksDTO(ppHeaderId, MaterialCategoryConstants.RTD_TRANSFER_STOCK_TYPE);
    }

    @Override
    public RMStockDTO getRmStockWithRejection(Long ppHeaderId) {
        return getAllRMStocksDTO(ppHeaderId, MaterialCategoryConstants.REJECTION);
    }

    private MesDTO getRmMesDetails(Long rscMtItemId, LocalDate mpsDate) {
        List<IndividualValuesMesDTO> individualValuesMesDTOList = rscMtRmMesService.findAllIndividualValueDTORscMtItemId(rscMtItemId);
        List<IndividualValuesMesDTO> individualMesValuesList = StockUtil.getIndividualMesValuesList(individualValuesMesDTOList, mpsDate);
        for (IndividualValuesMesDTO individualValuesMesDTO : individualMesValuesList) {
            individualValuesMesDTO.setExpirationDate(getExpirationDate(individualValuesMesDTO.getLotNumber(), rscMtItemId, mpsDate));
        }
        return StockUtil.getMesStockType(individualMesValuesList, mpsDate);
    }


    private RtdDTO getRmRtdDetails(Long rscMtItemId, LocalDate mpsDate) {
        List<IndividualValuesRtdDTO> individualValuesRtdDTOList = rscMtRmRtdService.findAllIndividualValueDTORscRtdItemId(rscMtItemId);

        List<IndividualValuesRtdDTO> individualRtdValuesList = StockUtil.getIndividualRtdValuesList(individualValuesRtdDTOList, mpsDate);
        return StockUtil.getRtdStockType(individualRtdValuesList, mpsDate);
    }

    private BulkOpRmDTO getBulkOpRmDetails(Long ppHeaderId, Long rscMtItemId, LocalDate mpsDate) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne(ppHeaderId);
        List<IndividualValuesBulkOpRmDTO> individualValuesBulkOpRmDTOList = rscIitRmBulkOpRmService.findAllIndivisualValueDTORscMtItemId(rscMtItemId);
        Double totalQuantityBulkOpRm = 0.0;

        if (optionalRscMtPpHeaderDTO.isPresent() && Optional.ofNullable(individualValuesBulkOpRmDTOList).isPresent()) {
            List<IndividualValuesBulkOpRmDTO> individualBulkOpRmList = getIndividualBulkOpRmList(individualValuesBulkOpRmDTOList, optionalRscMtPpHeaderDTO.get().getMpsDate());
            if (individualBulkOpRmList.size() != 0) {
                totalQuantityBulkOpRm = getRscIotRmStockQuantity(individualBulkOpRmList, mpsDate);
                return setBulkOpRmType(totalQuantityBulkOpRm, individualBulkOpRmList);
            }
            return null;
        } else {
            return null;
        }
    }

    private OtherStockDTO getTransferStockDetails(Long ppHeaderId, Long rscMtItemId, LocalDate mpsDate) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne(ppHeaderId);
        RscDtStockType otherStockType = rscDtStockTypeService.findOneByTag(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other);
        List<IndividualStockTypeValueDTO> individualStockTypeValueDTOList = rscMtRmOtherStocksService.findAllByRscDtStockTypeIdAndRscMtItemId(otherStockType.getId(), rscMtItemId);
        Double totalQuantityOtherStock = 0.0;

        if (optionalRscMtPpHeaderDTO.isPresent() && Optional.ofNullable(individualStockTypeValueDTOList).isPresent()) {
            List<IndividualStockTypeValueDTO> indivisualOtherStockTypeValueDTOList = getListByComparingStockAndMpsDate(individualStockTypeValueDTOList, optionalRscMtPpHeaderDTO.get().getMpsDate());

            if (indivisualOtherStockTypeValueDTOList.size() != 0) {
                totalQuantityOtherStock = calculationByStockType(indivisualOtherStockTypeValueDTOList, mpsDate);
                return setOtherStockType(totalQuantityOtherStock, indivisualOtherStockTypeValueDTOList);
            } else {
                return null;
            }

        } else {
            return null;
        }
    }

    private OtherStockDTO getRejectionDetails(Long ppHeaderId, Long rscMtItemId, LocalDate mpsDate) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpHeaderService.findOne(ppHeaderId);
        RscDtStockType rejectionStockType = rscDtStockTypeService.findOneByTag(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION);

        List<IndividualStockTypeValueDTO> individualRejectionStockTypeValueDTOList = rscMtRmOtherStocksService.findAllByRscDtStockTypeIdAndRscMtItemId(rejectionStockType.getId(), rscMtItemId);
        Double totalQuantityRejection = 0.0;

        if (optionalRscMtPpHeaderDTO.isPresent() && Optional.ofNullable(individualRejectionStockTypeValueDTOList).isPresent()) {
            List<IndividualStockTypeValueDTO> indivdualRejectionStockTypeValueDTOList = getListByComparingStockAndMpsDate(individualRejectionStockTypeValueDTOList, optionalRscMtPpHeaderDTO.get().getMpsDate());

            if (indivdualRejectionStockTypeValueDTOList.size() != 0) {
                totalQuantityRejection = calculationByStockType(indivdualRejectionStockTypeValueDTOList, mpsDate);
                return setRejectionStockType(totalQuantityRejection, indivdualRejectionStockTypeValueDTOList);
            } else {
                return null;
            }
        } else {
            return null;
        }
    }

    private ItemCodeDetailDTO getItemCodeDetailDTO(RscMtItemDTO rscMtItemDTO, Double totalStockValue, StockTypeDTO stockTypeDTO) {
        OpeningStockDTO openingStockDTO = new OpeningStockDTO();
        ItemCodeDetailDTO itemCodeDetailDTO = new ItemCodeDetailDTO();
        openingStockDTO.setValue(totalStockValue);
        if (Optional.ofNullable(rscMtItemDTO).isPresent()) {
            itemCodeDetailDTO.setItemId(rscMtItemDTO.getId());
            itemCodeDetailDTO.setItemCode(rscMtItemDTO.getItemCode());
            itemCodeDetailDTO.setItemDescription(rscMtItemDTO.getItemDescription());
            itemCodeDetailDTO.setFormula(MaterialCategoryConstants.RM_STOCK_DETAIL_FORMULA);
            itemCodeDetailDTO.setFormulaValue(DoubleUtils.getFormattedValue(totalStockValue) + "    =   " + DoubleUtils.getFormattedValue(stockTypeDTO.getRmMes().getTotalValue()) + " +   " + DoubleUtils.getFormattedValue(stockTypeDTO.getRmRtd().getTotalValue()) + " +   " + DoubleUtils.getFormattedValue(stockTypeDTO.getRmBulkOpRm().getTotalValue()) + "  +   " + DoubleUtils.getFormattedValue(stockTypeDTO.getRmTransferStock().getTotalValue()) + "  -   " + DoubleUtils.getFormattedValue(stockTypeDTO.getRmRejection().getTotalValue()));
        }
        itemCodeDetailDTO.setOpeningStockDTO(openingStockDTO);
        return itemCodeDetailDTO;
    }


    private List<IndividualValuesBulkOpRmDTO> getIndividualBulkOpRmList(List<IndividualValuesBulkOpRmDTO> individualValuesBulkOpRmDTO, LocalDate mpsDate) {
        return individualValuesBulkOpRmDTO.stream().filter(individualValuesBulkOpRmDTO1 -> DateUtils.isEqualMonthAndYear(individualValuesBulkOpRmDTO1.getStockDate(), mpsDate)).
                collect(Collectors.toList());
    }

    private List<IndividualStockTypeValueDTO> getListByComparingStockAndMpsDate(List<IndividualStockTypeValueDTO> indivisualStockTypeValueDTOS, LocalDate mpsDate) {
        return indivisualStockTypeValueDTOS.stream().filter(individualValuesBulkOpRmDTO1 -> DateUtils.isEqualMonthAndYear(individualValuesBulkOpRmDTO1.getStockDate(), mpsDate)).
                collect(Collectors.toList());
    }

    private LocalDate getExpirationDate(String lotNumber, Long rscMtItemId, LocalDate mpsDate) {
        List<RscMtRmListDTO> rscMtRmDataList = rscMtRmListService.findAllByLotNumberAndRscMtItemId(lotNumber, rscMtItemId);
        Optional<RscMtRmListDTO> rscMtRmData = rscMtRmDataList.stream().filter(rscMtRmListDTO -> DateUtils.isEqualMonthAndYear(rscMtRmListDTO.getMpsDate(), mpsDate)).findAny();
        if (rscMtRmData.isPresent() && rscMtRmData.get().getExpirationDate() != null) {
            return rscMtRmData.get().getExpirationDate();
        } else {
            return null;
        }
    }

    private BulkOpRmDTO setBulkOpRmType(Double totalValue, List<IndividualValuesBulkOpRmDTO> indivisualValuesBulkOpRmDTOS) {
        BulkOpRmDTO bulkOpRmDTO = new BulkOpRmDTO();
        bulkOpRmDTO.setName(MaterialCategoryConstants.BULK_OP_RM);
        bulkOpRmDTO.setAliasName(MaterialCategoryConstants.BULK);
        bulkOpRmDTO.setTotalValue(DoubleUtils.getFormattedValue(totalValue));
        bulkOpRmDTO.setIndividualValuesBulkOpRmDTOList(indivisualValuesBulkOpRmDTOS);
        return bulkOpRmDTO;
    }

    private OtherStockDTO setOtherStockType(Double totalValue, List<IndividualStockTypeValueDTO> indivisualStockTypeValueDTOS) {
        OtherStockDTO otherStockDTO = new OtherStockDTO();
        otherStockDTO.setName(MaterialCategoryConstants.OTHER_STOCK);
        otherStockDTO.setAliasName(MaterialCategoryConstants.OTHER_STOCK);
        otherStockDTO.setTotalValue(DoubleUtils.getFormattedValue(totalValue));
        otherStockDTO.setIndividualStockTypeValueDTOList(indivisualStockTypeValueDTOS);
        return otherStockDTO;
    }

    private OtherStockDTO setRejectionStockType(Double totalValue, List<IndividualStockTypeValueDTO> indivisualStockTypeValueDTOS) {
        OtherStockDTO otherStockDTO = new OtherStockDTO();
        otherStockDTO.setName(MaterialCategoryConstants.REJECTION);
        otherStockDTO.setAliasName(MaterialCategoryConstants.REJECTION);
        otherStockDTO.setTotalValue(DoubleUtils.getFormattedValue(totalValue));
        otherStockDTO.setIndividualStockTypeValueDTOList(indivisualStockTypeValueDTOS);
        return otherStockDTO;
    }

    private Double getRscIotRmStockQuantity(List<IndividualValuesBulkOpRmDTO> individualBulkRmValuesList, LocalDate mpsDate) {
        double totalQuantity = 0.0;
        for (IndividualValuesBulkOpRmDTO indivisualValuesBulkOpRmDTO : individualBulkRmValuesList) {
            if (indivisualValuesBulkOpRmDTO.getStockDate() != null) {
                boolean isDateMatches = DateUtils.isEqualMonthAndYear(indivisualValuesBulkOpRmDTO.getStockDate(), mpsDate);
                if (isDateMatches) {
                    totalQuantity += indivisualValuesBulkOpRmDTO.getValue();
                }
            }
        }
        return totalQuantity;
    }

    private Double calculationByStockType(List<IndividualStockTypeValueDTO> individualBulkRmValuesList, LocalDate mpsDate) {
        double totalQuantity = 0.0;
        for (IndividualStockTypeValueDTO indivisualValuesBulkOpRmDTO : individualBulkRmValuesList) {
            if (indivisualValuesBulkOpRmDTO.getStockDate() != null) {
                boolean isDateMatches = DateUtils.isEqualMonthAndYear(indivisualValuesBulkOpRmDTO.getStockDate(), mpsDate);
                if (isDateMatches) {
                    totalQuantity += indivisualValuesBulkOpRmDTO.getQuantity();
                }
            }
        }
        return totalQuantity;
    }


    @Override
    public RMStockDTO getRmStockWithBulkOpRm(Long ppHeaderId) {
        return getAllRMStocksDTO(ppHeaderId, MaterialCategoryConstants.BULK);
    }


    //     Named Native Query
    @Override
    public List getRmStockWithMesWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES);
        } else return new ArrayList();
    }

    @Override
    public List getRmStockWithMesRejectionWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJ);
        } else return new ArrayList();
    }

    @Override
    public List getRmStockWithTransferStockWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other);
        } else return new ArrayList();
    }

    @Override
    public List getRmStockWithRtdWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD);
        } else return new ArrayList();
    }

    @Override
    public List getRmStockWithRejectionWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION);
        } else return new ArrayList();
    }

    @Override
    public List getRmStockWithBulkOpRmWithNamedQuery(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM);
        } else return new ArrayList();
    }


    //    Excel code with native query
    @Override
    public ExcelExportFiles exportRMStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmStockExporter.stockToExcelList(getRmStockWithMesWithNamedQuery(rscMtPPheaderDTO.getId()), rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_MIDNIGHT_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles exportRmMesStockRejection(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmMesRejectionStockExporter.mesRejectionStockToExcel(rscMtPPheaderDTO.getMpsDate(), getRmStockWithMesRejectionWithNamedQuery(rscMtPPheaderDTO.getId())));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_MIDNIGHT_Rejection_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles exportRtdStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmStockRtdExporter.rtdStockToExcelList(getRmStockWithRtdWithNamedQuery(rscMtPPheaderDTO.getId()), rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_RECEIPTS_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles exportBulkOpRmStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmStockBulkOpRmExporter.bulkOpRmStockToExcelList(getRmStockWithBulkOpRmWithNamedQuery(rscMtPPheaderDTO.getId()), rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_BULK_OP_RM_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles exportTransferStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmStockTransferStockExporter.transferStockToExcelList(getRmStockWithTransferStockWithNamedQuery(rscMtPPheaderDTO.getId()), rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_TRANSFER_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles exportRejectionStock(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(RmStockRejectionStockExporter.rejectionStockToExcelList(getRmStockWithRejectionWithNamedQuery(rscMtPPheaderDTO.getId()), rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_REJECTION_STOCK + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }
 /* @Override
    public List getRmMesAQStock(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES);
        } else return new ArrayList();
    }*/

  /*  @Override
    public List getRmStockWithMesRejection(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJ);
        } else return new ArrayList();
    }*/

  /*  @Override
    public List getRmStockWithBulkOpRmWithPagination(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM);
        } else return new ArrayList();
    }*/
/*
    @Override
    public List getRmStockRejection(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION);
        } else return new ArrayList();
    }*/

  /*  @Override
    public List getRmStockWithTransferStockWithPagination(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other);
        } else return new ArrayList();
    }*/

  /*  @Override
    public List getRmStockRtd(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            return rscIotRmStockService.findDataByMpsDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD);
        } else return new ArrayList();
    }*/


    //    Mes New Api with optimization
    @Override
    public List<RmMesStockPaginationDto> getStockWithMes(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        List<RmMesStockPaginationDto> mesStockDtoList = new ArrayList<>();
        if (rscMtPPheaderDTO.isPresent()) {
            HashMap<Long, RscIotRmStockDTO> rscIotRmStockDTOHashMapList = rscIotRmStockService.findAllByStockDateAndStockType(rscMtPPheaderDTO.get().getMpsDate(), MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES);
            if (rscIotRmStockDTOHashMapList.size() > 0) {
                mesStockDtoList = getMesListByStockDate(rscMtPPheaderDTO.get().getMpsDate());
                if (Optional.ofNullable(mesStockDtoList).isPresent()) {
                    mesStockDtoList = setTotalQuantityOfRmMesFromStockList(mesStockDtoList, rscIotRmStockDTOHashMapList);
                }
            }
        }
        return mesStockDtoList;
    }

    private List<RmMesStockPaginationDto> getMesListByStockDate(LocalDate stockDate) {
        return rscMtRmMesService.findAllByStockDate(stockDate);
    }

    private List<RmMesStockPaginationDto> setTotalQuantityOfRmMesFromStockList(List<RmMesStockPaginationDto> mesStockDtoList, HashMap<Long, RscIotRmStockDTO> rscIotRmStockDTOHashMapList) {
        mesStockDtoList.forEach(rmMesStockPaginationDto -> rmMesStockPaginationDto.setQuantity(rscIotRmStockDTOHashMapList.get(rmMesStockPaginationDto.getRscMtItemId()).getQuantity()));
        return mesStockDtoList;
    }
}