package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.service.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@Service
public class AllExcelExportServiceImpl implements AllExcelExportService {
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscItPMLogicalConsumptionService rscItPMLogicalConsumptionService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final RscIotFgLineSaturationService rscIotFgLineSaturationService;
    private final RscIotFgSkidSaturationService rscIotFgSkidSaturationService;
    private final RscIotFGSlobService rscIotFGSlobService;
    private final RscIotFGSlobSummaryService rscIotFGSlobSummaryService;
    private final RscDtSkidsService rscDtSkidsService;
    private final RscDtLinesService rscDtLinesService;
    private final MaterialMastersService materialMastersService;
    private final RscIotPmOtifSummaryService rscIotPmOtifSummaryService;
    private final MonthService monthService;

    public AllExcelExportServiceImpl(RscMtPpdetailsService rscMtPpdetailsService, RscItPMLogicalConsumptionService rscItPMLogicalConsumptionService, RscMtPpheaderService rscMtPpheaderService, RscItRMLogicalConsumptionService rscItRMLogicalConsumptionService, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService, RscIotFgLineSaturationService rscIotFgLineSaturationService, RscIotFgSkidSaturationService rscIotFgSkidSaturationService, RscIotFGSlobService rscIotFGSlobService, RscIotFGSlobSummaryService rscIotFGSlobSummaryService, RscDtSkidsService rscDtSkidsService, RscDtLinesService rscDtLinesService, MaterialMastersService materialMastersService, RscIotPmOtifSummaryService rscIotPmOtifSummaryService, MonthService monthService) {
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscItPMLogicalConsumptionService = rscItPMLogicalConsumptionService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscItRMLogicalConsumptionService = rscItRMLogicalConsumptionService;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
        this.rscIotFgLineSaturationService = rscIotFgLineSaturationService;
        this.rscIotFgSkidSaturationService = rscIotFgSkidSaturationService;
        this.rscIotFGSlobService = rscIotFGSlobService;
        this.rscIotFGSlobSummaryService = rscIotFGSlobSummaryService;
        this.rscDtSkidsService = rscDtSkidsService;
        this.rscDtLinesService = rscDtLinesService;
        this.materialMastersService = materialMastersService;
        this.rscIotPmOtifSummaryService = rscIotPmOtifSummaryService;
        this.monthService = monthService;
    }

    @Override
    public String createAllMpsFgExports(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            String year = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getYear() );
            String month = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth() );
            String date = String.valueOf( optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth() );
            String name = optionalRscMtPpHeaderDTO.get().getMpsName();
            createDirectory( year, month, date, name );
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails( optionalRscMtPpHeaderDTO.get().getMpsDate() );
          if(optionalRscMtPpHeaderDTO.get().getIsExecutionDone()==false){
              createMPSExcels( optionalRscMtPpHeaderDTO.get(), year, month, date, monthDetailDTO );
              createAllLogicalConsumptionExcels( optionalRscMtPpHeaderDTO.get(), year, month, date, monthDetailDTO );
              saveFgExport( optionalRscMtPpHeaderDTO.get(), year, month, date, monthDetailDTO );
          }
              savePMOTIFExport( optionalRscMtPpHeaderDTO.get(), year, month, date, monthDetailDTO );
        }
        return "Downloaded All Exports Successfully";
    }

    private void createMPSExcels(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, MonthDetailDTO monthDetailDTO) {
        MpsPlanMainDTO mpsPlanMainDTO = rscMtPpdetailsService.getMPSPlanMainDTO( rscMtPPheaderDTO );
        ExcelExportFiles pmExcelExportFile = rscMtPpdetailsService.mpsPlanToExcel( rscMtPPheaderDTO, monthDetailDTO, mpsPlanMainDTO );
        createExcelFile( rscMtPPheaderDTO, year, month, date, pmExcelExportFile, ExcelFileNameConstants.PM_Directory );
        ExcelExportFiles fgExcelExportFile = rscMtPpdetailsService.mpsPlanToExcel( rscMtPPheaderDTO, monthDetailDTO, mpsPlanMainDTO );
        createExcelFile( rscMtPPheaderDTO, year, month, date, fgExcelExportFile, ExcelFileNameConstants.FG_Directory );
        ExcelExportFiles rmExcelExportFile = rscMtPpdetailsService.mpsPlanToExcel( rscMtPPheaderDTO, monthDetailDTO, mpsPlanMainDTO );
        createExcelFile( rscMtPPheaderDTO, year, month, date, rmExcelExportFile, ExcelFileNameConstants.RM_Directory );
    }

    private Map<Long, ItemDetailDTO> getMpsPlanDetailMap(Long mpsId) {
        Map<Long, ItemDetailDTO> mpsPlanDetailMap = new HashMap<>();
        List<ItemDetailDTO> mpsPlanDetails = rscMtPpdetailsService.getCodeMPSPlanByFG( mpsId );
        for (ItemDetailDTO itemDetailDTO : mpsPlanDetails) {
            mpsPlanDetailMap.put( itemDetailDTO.getId(), itemDetailDTO );
        }
        return mpsPlanDetailMap;
    }

    private Map<Long, GrossConsumptionDTO> getBulkGrossConsumptionDetailMap(Long mpsId) {
        Map<Long, GrossConsumptionDTO> grossConsumptionMap = new HashMap<>();
        GrossConsumptionMainDTO grossConsumptionDetails = rscItBulkGrossConsumptionService.getAllBulkGrossConsumption( mpsId );
        for (GrossConsumptionDTO grossConsumptionDTO : grossConsumptionDetails.getItemCodes()) {
            grossConsumptionMap.put( grossConsumptionDTO.getMaterialId(), grossConsumptionDTO );
        }
        return grossConsumptionMap;
    }

    private void createAllLogicalConsumptionExcels(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> fgExportList = new ArrayList<>();
        Map<Long, ItemDetailDTO> mpsPlanDetailMap = getMpsPlanDetailMap( rscMtPPheaderDTO.getId() );
        Map<Long, GrossConsumptionDTO> grossConsumptionMap = getBulkGrossConsumptionDetailMap( rscMtPPheaderDTO.getId() );
        ExcelExportFiles excelExportFile = rscItPMLogicalConsumptionService.getPmLogicalConsumptionExport( rscMtPPheaderDTO, monthDetailDTO, mpsPlanDetailMap );
        createExcelFile( rscMtPPheaderDTO, year, month, date, excelExportFile, ExcelFileNameConstants.PM_Directory );
        fgExportList.add( rscItRMLogicalConsumptionService.getBulkLogicalConsumptionExport( rscMtPPheaderDTO, monthDetailDTO, mpsPlanDetailMap ) );
        fgExportList.add( rscItRMLogicalConsumptionService.getRmLogicalConsumptionExport( rscMtPPheaderDTO, monthDetailDTO, grossConsumptionMap ) );
        createExcelFiles( rscMtPPheaderDTO, year, month, date, fgExportList, ExcelFileNameConstants.RM_Directory );
    }

    private void saveFgExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> fgExportList = new ArrayList<>();
        fgExportList.add( rscIotFgLineSaturationService.fgLineExport( rscMtPPheaderDTO, monthDetailDTO ) );
        fgExportList.add( rscIotFgSkidSaturationService.fgSkidExport( rscMtPPheaderDTO, monthDetailDTO ) );
        fgExportList.add( rscIotFGSlobService.fgSlobExport( rscMtPPheaderDTO ) );
        fgExportList.add( rscDtSkidsService.fgMasterDataSkidExport( rscMtPPheaderDTO ) );
        fgExportList.add( rscDtLinesService.fgMasterDatalineExport( rscMtPPheaderDTO ) );
        fgExportList.add( materialMastersService.fgMaterialMasterDataExport( rscMtPPheaderDTO ) );
        fgExportList.add( materialMastersService.fgWipMaterialMasterDataExport( rscMtPPheaderDTO ) );
        fgExportList.add( rscIotFGSlobSummaryService.exportFgSlobSummary( rscMtPPheaderDTO ) );
        createExcelFiles( rscMtPPheaderDTO, year, month, date, fgExportList, ExcelFileNameConstants.FG_Directory );
    }

    private void createExcelFile(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, ExcelExportFiles excelExportFile, String moduleType) {
        try {
            FileOutputStream fileOut = new FileOutputStream( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + rscMtPPheaderDTO.getMpsName() + "/" + moduleType + excelExportFile.getFileName() );
            fileOut.write( excelExportFile.getFile().readAllBytes() );
        } catch (IOException e) {
            throw new RuntimeException( "Fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private void createExcelFiles(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, List<ExcelExportFiles> fgExportList, String moduleType) {
        for (ExcelExportFiles excelExportFiles : fgExportList) {
            try {
                FileOutputStream fileOut = new FileOutputStream( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date  + "/" + rscMtPPheaderDTO.getMpsName() + "/" + moduleType + excelExportFiles.getFileName() );
                fileOut.write( excelExportFiles.getFile().readAllBytes() );
            } catch (IOException e) {
                throw new RuntimeException( "Fail to import data to Excel file: " + e.getMessage() );
            }
        }
    }

    private void savePMOTIFExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> pmOtifExportList = new ArrayList<>();
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent() && Optional.ofNullable( rscMtPPheaderDTO.getOtifDate() ).isPresent()) {
            pmOtifExportList.add( rscIotPmOtifSummaryService.pmOtifSummaryExport( rscMtPPheaderDTO, monthDetailDTO ) );
            for (ExcelExportFiles excelExportFiles : pmOtifExportList) {
                try {
                    FileOutputStream fileOut = new FileOutputStream( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Month" + "/" + "PM" + "/" + "Otif" + "/" + excelExportFiles.getFileName() );
                    fileOut.write( excelExportFiles.getFile().readAllBytes() );
                } catch (IOException e) {
                    throw new RuntimeException( "Fail to import data to Excel file: " + e.getMessage() );
                }
            }
        }
    }

    @Override
    public void createDirectory(String year, String month, String date, String name) {
        try {
            Path fgPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.FG );
            Path pmPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.PM );
            Path rmPath = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.RM );
            Path pmOtif = Paths.get( ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Analysis" + "/" + "Month" + "/" + "PM" + "/" + "Otif" );
            Files.createDirectories( fgPath );
            Files.createDirectories( pmPath );
            Files.createDirectories( rmPath );
            Files.createDirectories( pmOtif );
        } catch (IOException e) {
        }
    }
}