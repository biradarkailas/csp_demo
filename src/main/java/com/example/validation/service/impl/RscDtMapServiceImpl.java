package com.example.validation.service.impl;

import com.example.validation.dto.RscDtMapDTO;
import com.example.validation.mapper.RscDtMapMapper;
import com.example.validation.repository.RscDtMapRepository;
import com.example.validation.service.RscDtMapService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtMapServiceImpl implements RscDtMapService {

    private final RscDtMapRepository rscDtMapRepository;
    private final RscDtMapMapper rscDtMapMapper;

    public RscDtMapServiceImpl(RscDtMapRepository rscDtMapRepository, RscDtMapMapper rscDtMapMapper){
        this.rscDtMapRepository = rscDtMapRepository;
        this.rscDtMapMapper = rscDtMapMapper;
    }

    @Override
    public List<RscDtMapDTO> findAll() {
        return rscDtMapMapper.toDto(rscDtMapRepository.findAll());
    }
}

