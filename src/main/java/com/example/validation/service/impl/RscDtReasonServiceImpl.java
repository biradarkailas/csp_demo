package com.example.validation.service.impl;

import com.example.validation.domain.RscDtReason;
import com.example.validation.dto.RscDtReasonDTO;
import com.example.validation.mapper.RscDtReasonMapper;
import com.example.validation.repository.RscDtReasonRepository;
import com.example.validation.service.RscDtReasonService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtReasonServiceImpl implements RscDtReasonService {
    private final RscDtReasonRepository rscDtReasonRepository;
    private final RscDtReasonMapper rscDtReasonMapper;

    public RscDtReasonServiceImpl(RscDtReasonRepository rscDtReasonTypeRepository, RscDtReasonMapper rscDtReasonMapper) {
        this.rscDtReasonRepository = rscDtReasonTypeRepository;
        this.rscDtReasonMapper = rscDtReasonMapper;
    }

    @Override
    public List<RscDtReasonDTO> findAll() {
        return rscDtReasonMapper.toDto(rscDtReasonRepository.findAll());
    }

    @Override
    public List<RscDtReasonDTO> save(List<RscDtReasonDTO> rscDtReasonDTOs) {
        List<RscDtReason> rscDtReasons = rscDtReasonRepository.saveAll(rscDtReasonMapper.toEntity(rscDtReasonDTOs));
        return rscDtReasonMapper.toDto(rscDtReasons);
    }
}
