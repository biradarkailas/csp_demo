package com.example.validation.service.impl;

import com.example.validation.dto.RscDtMaterialCategoryDTO;
import com.example.validation.mapper.RscDtMaterialCategoryMapper;
import com.example.validation.repository.RscDtMaterialCategoryRepository;
import com.example.validation.service.RscDtMaterialCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtMaterialCategoryServiceImpl implements RscDtMaterialCategoryService {
    private final RscDtMaterialCategoryRepository rscDtMaterialCategoryRepository;
    private final RscDtMaterialCategoryMapper rscDtMaterialCategoryMapper;


    public RscDtMaterialCategoryServiceImpl(RscDtMaterialCategoryRepository rscDtMaterialCategoryRepository, RscDtMaterialCategoryMapper rscDtMaterialCategoryMapper){
        this.rscDtMaterialCategoryRepository = rscDtMaterialCategoryRepository;
        this.rscDtMaterialCategoryMapper = rscDtMaterialCategoryMapper;
    }

    @Override
    public List<RscDtMaterialCategoryDTO> findAll() { return rscDtMaterialCategoryMapper.toDto(rscDtMaterialCategoryRepository.findAll()); }
}


