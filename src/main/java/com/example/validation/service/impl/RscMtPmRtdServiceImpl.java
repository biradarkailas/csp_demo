package com.example.validation.service.impl;

import com.example.validation.dto.IndividualValuesRtdDTO;
import com.example.validation.dto.RscMtPmRtdDTO;
import com.example.validation.mapper.IndividualValuesPmRtdMapper;
import com.example.validation.mapper.RscMtPmRtdMapper;
import com.example.validation.repository.RscMtPmRtdRepository;
import com.example.validation.service.RscMtPmRtdService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtPmRtdServiceImpl implements RscMtPmRtdService {
    private final RscMtPmRtdRepository rscMtPmRtdRepository;
    private final RscMtPmRtdMapper rscMtPmRtdMapper;
    private final IndividualValuesPmRtdMapper individualValuesPmRtdMapper;

    public RscMtPmRtdServiceImpl(RscMtPmRtdRepository rscMtPmRtdRepository, RscMtPmRtdMapper rscMtPmRtdMapper, IndividualValuesPmRtdMapper individualValuesPmRtdMapper) {
        this.rscMtPmRtdRepository = rscMtPmRtdRepository;
        this.rscMtPmRtdMapper = rscMtPmRtdMapper;
        this.individualValuesPmRtdMapper = individualValuesPmRtdMapper;
    }

    @Override
    public List<RscMtPmRtdDTO> findAll() {
        return rscMtPmRtdMapper.toDto(rscMtPmRtdRepository.findAll());
    }

    @Override
    public List<RscMtPmRtdDTO> findByReceiptDate(LocalDate receiptDate) {
        return rscMtPmRtdMapper.toDto(rscMtPmRtdRepository.findAllByReceiptDate(receiptDate));
    }

    @Override
    public List<IndividualValuesRtdDTO> findAllIndividualValueDTOByRscMtItemId(Long rscMtItemId) {
        return individualValuesPmRtdMapper.toDto(rscMtPmRtdRepository.findAllByRscMtItemId(rscMtItemId));
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtPmRtdRepository.deleteByReceiptDateBetween(stockDate, stockDate.withDayOfMonth(stockDate.lengthOfMonth()));
    }
}