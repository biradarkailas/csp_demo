package com.example.validation.service.impl;

import com.example.validation.dto.IndividualValuesRtdDTO;
import com.example.validation.dto.RscMtRmRtdDto;
import com.example.validation.dto.RscMtRmRtdExportDTO;
import com.example.validation.mapper.IndividualValuesRtdMapper;
import com.example.validation.mapper.RscMtRmRtdExportMapper;
import com.example.validation.mapper.RscMtRmRtdMapper;
import com.example.validation.repository.RscMtRmRtdRepository;
import com.example.validation.service.RscMtRmRtdService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtRmRtdServiceImpl implements RscMtRmRtdService {
    private final RscMtRmRtdRepository rscMtRmRtdRepository;
    private final RscMtRmRtdMapper rscMtRmRtdMapper;
    private final IndividualValuesRtdMapper indivisualValuesRtdMapper;
    private final RscMtRmRtdExportMapper rscMtRmRtdExportMapper;

    public RscMtRmRtdServiceImpl(RscMtRmRtdRepository rscMtRmRtdRepository, RscMtRmRtdMapper rscMtRmRtdMapper, IndividualValuesRtdMapper indivisualValuesRtdMapper, RscMtRmRtdExportMapper rscMtRmRtdExportMapper) {
        this.rscMtRmRtdRepository = rscMtRmRtdRepository;
        this.rscMtRmRtdMapper = rscMtRmRtdMapper;
        this.indivisualValuesRtdMapper = indivisualValuesRtdMapper;
        this.rscMtRmRtdExportMapper = rscMtRmRtdExportMapper;
    }

    @Override
    public List<RscMtRmRtdDto> findAllByRscMtItemId(Long rscMtItemId) {
        return rscMtRmRtdMapper.toDto(rscMtRmRtdRepository.findAllByRscMtItemId(rscMtItemId));
    }

    @Override
    public List<IndividualValuesRtdDTO> findAllIndividualValueDTORscRtdItemId(Long rscMtItemId) {
        return indivisualValuesRtdMapper.toDto(rscMtRmRtdRepository.findAllByRscMtItemId(rscMtItemId));
    }

    @Override
    public List<RscMtRmRtdExportDTO> findAll() {
        return rscMtRmRtdExportMapper.toDto(rscMtRmRtdRepository.findAll());
    }

    @Override
    public void deleteAll(LocalDate receiptDate) {
        rscMtRmRtdRepository.deleteByReceiptDateBetween(receiptDate, receiptDate.withDayOfMonth(receiptDate.lengthOfMonth()));
    }
}