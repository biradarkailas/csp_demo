package com.example.validation.service.impl;

import com.example.validation.dto.StagingTableDetailsDTO;
import com.example.validation.mapper.StagingTableDetailsMapper;
import com.example.validation.repository.StagingTableNameRepository;
import com.example.validation.service.StagingTableNameService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class StagingTableNameServiceImpl implements StagingTableNameService {

    private final Logger log = LoggerFactory.getLogger(StagingTableNameServiceImpl.class);
    private final StagingTableNameRepository stagingTableNameRepository;
    private final StagingTableDetailsMapper stagingTableDetailsMapper;

    public StagingTableNameServiceImpl(StagingTableNameRepository stagingTableNameRepository, StagingTableDetailsMapper stagingTableDetailsMapper) {
        this.stagingTableNameRepository = stagingTableNameRepository;
        this.stagingTableDetailsMapper = stagingTableDetailsMapper;
    }

    @Override
    @Transactional(readOnly = true)
    public List<StagingTableDetailsDTO> findAll() {
        log.debug("Request to get all StagingTableName");
        return stagingTableDetailsMapper.toDto(stagingTableNameRepository.findAll());
    }

    @Override
    @Transactional(readOnly = true)
    public List<StagingTableDetailsDTO> findAllByTableNamesAndValidationSequenceAsc(List<String> tableNames) {
        log.debug("Request to get all StagingTableName");
        List<StagingTableDetailsDTO> stagingTableDetailsDTOS = stagingTableDetailsMapper.toDto(stagingTableNameRepository.findByTableNameIn(tableNames));
        stagingTableDetailsDTOS.sort(Comparator.comparing(StagingTableDetailsDTO::getExecutionSequence));
        return stagingTableDetailsDTOS;
    }

    @Override
    @Transactional(readOnly = true)
    public List<StagingTableDetailsDTO> findAllByTableNamesAndResourceSequenceAsc(List<String> tableNames) {
        log.debug("Request to get all StagingTableName");
        List<StagingTableDetailsDTO> stagingTableDetailsDTOS = stagingTableDetailsMapper.toDto(stagingTableNameRepository.findByTableNameIn(tableNames));
        stagingTableDetailsDTOS.sort(Comparator.comparing(StagingTableDetailsDTO::getResourceSequence));
        return stagingTableDetailsDTOS;
    }

    @Override
    public List<String> findAllTableNameByExecutionSequence(Integer executionSequence) {
        return stagingTableDetailsMapper.toDto(stagingTableNameRepository.findByExecutionSequenceGreaterThanEqual(executionSequence))
                .stream()
                .map(StagingTableDetailsDTO::getTableName)
                .collect(Collectors.toList());
    }

    @Override
    public StagingTableDetailsDTO findOneByTableName(String tableName) {
        return stagingTableDetailsMapper.toDto(stagingTableNameRepository.findOneByTableName(tableName));
    }
}