package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.domain.RscIotPMLatestPurchasePlan;
import com.example.validation.domain.RscIotRMLatestPurchasePlan;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.dto.supply.RMPurchasePlanDTO;
import com.example.validation.mapper.PMLatestPurchasePlanMapper;
import com.example.validation.mapper.RMPurchasePlanMapper;
import com.example.validation.repository.RscIotPMLatestPurchasePlanRepository;
import com.example.validation.repository.RscIotRMLatestPurchasePlanRepository;
import com.example.validation.service.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class RecalculationAndExcelCreationOfPurchasePlanServiceImpl implements RecalculationAndExcelCreationOfPurchasePlanService {
    private final PMLatestPurchasePlanMapper pmLatestPurchasePlanMapper;
    private final RscIotPMLatestPurchasePlanRepository rscIotPMLatestPurchasePlanRepository;
    private final RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService;
    private final RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService;
    private final RscIotLatestTotalMouldSaturationSummaryService rscIotLatestTotalMouldSaturationSummaryService;
    private final RscIotPMPurchasePlanService rscIotPMPurchasePlanService;
    private final AllExcelExportService allExcelExportService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotRMLatestPurchasePlanRepository rscIotRMLatestPurchasePlanRepository;
    private final RMPurchasePlanMapper rmPurchasePlanMapper;
    private final RscIotRMPurchasePlanService rscIotRMPurchasePlanService;
    private final MonthService monthService;

    public RecalculationAndExcelCreationOfPurchasePlanServiceImpl(PMLatestPurchasePlanMapper pmLatestPurchasePlanMapper, RscIotPMLatestPurchasePlanRepository rscIotPMLatestPurchasePlanRepository, RscIotLatestMouldSaturationService rscIotLatestMouldSaturationService, RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService, RscIotLatestTotalMouldSaturationSummaryService rscIotLatestTotalMouldSaturationSummaryService, RscIotPMPurchasePlanService rscIotPMPurchasePlanService, AllExcelExportService allExcelExportService, RscMtPpheaderService rscMtPpheaderService, RscIotRMLatestPurchasePlanRepository rscIotRMLatestPurchasePlanRepository, RMPurchasePlanMapper rmPurchasePlanMapper, RscIotRMPurchasePlanService rscIotRMPurchasePlanService, MonthService monthService) {
        this.pmLatestPurchasePlanMapper = pmLatestPurchasePlanMapper;
        this.rscIotPMLatestPurchasePlanRepository = rscIotPMLatestPurchasePlanRepository;
        this.rscIotLatestMouldSaturationService = rscIotLatestMouldSaturationService;
        this.rscIotLatestTotalMouldSaturationService = rscIotLatestTotalMouldSaturationService;
        this.rscIotLatestTotalMouldSaturationSummaryService = rscIotLatestTotalMouldSaturationSummaryService;
        this.rscIotPMPurchasePlanService = rscIotPMPurchasePlanService;
        this.allExcelExportService = allExcelExportService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotRMLatestPurchasePlanRepository = rscIotRMLatestPurchasePlanRepository;
        this.rmPurchasePlanMapper = rmPurchasePlanMapper;
        this.rscIotRMPurchasePlanService = rscIotRMPurchasePlanService;
        this.monthService = monthService;
    }


    @Override
    public List<PurchasePlanDTO> saveAll(List<PurchasePlanDTO> purchasePlanDTOs) {
        List<RscIotPMLatestPurchasePlan> rscIotPMLatestPurchasePlans = rscIotPMLatestPurchasePlanRepository.saveAll(pmLatestPurchasePlanMapper.toEntity(purchasePlanDTOs));
        recalculateMouldAndTotalMouldSaturation(purchasePlanDTOs, purchasePlanDTOs.get(0).getMpsId());
//        rscIotPMPurchasePlanService.purchasePlanToExcel(purchasePlanDTOs.get(0).getMpsId());
/*
        recalculateExcelBasedOnPurchasePlan(purchasePlanDTOs.get(0).getMpsId());
*/
        return pmLatestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlans);
    }

    private void recalculateMouldAndTotalMouldSaturation(List<PurchasePlanDTO> purchasePlanDTOs, Long ppHeaderId) {
        rscIotLatestMouldSaturationService.updateMouldSaturationForUpdatedPurchasePlan(purchasePlanDTOs, ppHeaderId);
        rscIotLatestTotalMouldSaturationService.updateTotalMouldSaturationForUpdatedPurchasePlan(purchasePlanDTOs, ppHeaderId);
        rscIotLatestTotalMouldSaturationSummaryService.updateMouldSaturationSummaryForUpdatedPurchasePlan(purchasePlanDTOs, ppHeaderId);
    }

    @Override
    public String createPmPurchasePlanExportOnUpdate(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            String year = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getYear());
            String month = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getMonth());
            String date = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getDayOfMonth());
            String name = rscMtPPheaderDTO.get().getMpsName();
            allExcelExportService.createDirectory(year, month, date, name);
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(rscMtPPheaderDTO.get().getMpsDate());
            savePmPurchaseExport(rscMtPPheaderDTO.get(), year, month, date, name, monthDetailDTO);
        }
        return "Downloaded Pm Purchase plan and mould Exports Successfully";

    }

    /*  private void recalculateExcelBasedOnPurchasePlan(Long ppHeaderId) {
          Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
          if (rscMtPPheaderDTO.isPresent()) {
              String year = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getYear());
              String month = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getMonth());
              String date = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getDayOfMonth());
              String name = rscMtPPheaderDTO.get().getMpsName();
              allExcelExportService.createDirectory(year, month, date, name);
              MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(rscMtPPheaderDTO.get().getMpsDate());
              savePmPurchaseExport(rscMtPPheaderDTO.get(), year, month, date, name, monthDetailDTO);
          }
      }
  */
    private void savePmPurchaseExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, String name, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> pmExportList = new ArrayList<>();
        pmExportList.add(rscIotPMPurchasePlanService.purchasePlanToExcel(rscMtPPheaderDTO, monthDetailDTO));
        pmExportList.add(rscIotLatestMouldSaturationService.getSupplierWiseMouldExport(rscMtPPheaderDTO, monthDetailDTO));
        pmExportList.add(rscIotLatestTotalMouldSaturationService.mouldWiseSaturationToExcel(rscMtPPheaderDTO, monthDetailDTO));
        for (ExcelExportFiles excelExportFiles : pmExportList) {
            try {
                FileOutputStream fileOut = new FileOutputStream(ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.PM_Directory + excelExportFiles.getFileName());
                fileOut.write(excelExportFiles.getFile().readAllBytes());
            } catch (IOException e) {
                throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
            }
        }
    }


//    RM purchase recalculation

    @Override
    public List<RMPurchasePlanDTO> saveAllRmPurchasePlan(List<RMPurchasePlanDTO> rmPurchasePlanDTOs) {
        List<RscIotRMLatestPurchasePlan> rscIotPMLatestPurchasePlans = rscIotRMLatestPurchasePlanRepository.saveAll(rmPurchasePlanMapper.toEntity(rmPurchasePlanDTOs));
//        rscIotRMPurchasePlanService.getRmPurchasePlanExport(rmPurchasePlanDTOs.get(0).getMpsId());
//        recalculateExcelBasedOnRmPurchasePlan(rmPurchasePlanDTOs.get(0).getMpsId());
        return rmPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlans);
    }

    public String createRmPurchasePlanExportOnUpdate(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            String year = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getYear());
            String month = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getMonth());
            String date = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getDayOfMonth());
            String name = rscMtPPheaderDTO.get().getMpsName();
            allExcelExportService.createDirectory(year, month, date, name);
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(rscMtPPheaderDTO.get().getMpsDate());
            saveRmPurchaseExport(rscMtPPheaderDTO.get(), year, month, date, name, monthDetailDTO);
        }
        return "Downloaded Rm Purchase plan Exports Successfully";
    }

/*
    private void recalculateExcelBasedOnRmPurchasePlan(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            String year = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getYear());
            String month = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getMonth());
            String date = String.valueOf(rscMtPPheaderDTO.get().getMpsDate().getDayOfMonth());
            String name = rscMtPPheaderDTO.get().getMpsName();
            allExcelExportService.createDirectory(year, month, date, name);
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(rscMtPPheaderDTO.get().getMpsDate());
            saveRmPurchaseExport(rscMtPPheaderDTO.get(), year, month, date, name, monthDetailDTO);
        }
    }
*/

    private void saveRmPurchaseExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, String name, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> rmExportList = new ArrayList<>();
        rmExportList.add(rscIotRMPurchasePlanService.getRmPurchasePlanExport(rscMtPPheaderDTO, monthDetailDTO));
        for (ExcelExportFiles excelExportFiles : rmExportList) {
            try {
                FileOutputStream fileOut = new FileOutputStream(ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.RM_Directory + excelExportFiles.getFileName());
                fileOut.write(excelExportFiles.getFile().readAllBytes());
            } catch (Exception e) {
                throw new RuntimeException("Fail to import data to Excel file: " + e.getMessage());
            }
        }
    }

}
