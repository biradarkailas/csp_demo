package com.example.validation.service.impl;

import com.example.validation.dto.*;
import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.mapper.MouldSaturationSummaryForSixMonthsMapper;
import com.example.validation.mapper.MouldSaturationSummaryForThreeMonthsMapper;
import com.example.validation.mapper.MouldSaturationSummaryForTwelveMonthsMapper;
import com.example.validation.mapper.RscIotLatestTotalMouldSaturationSummaryMapper;
import com.example.validation.repository.RscIotLatestTotalMouldSaturationSummaryRepository;
import com.example.validation.service.RscIotLatestTotalMouldSaturationService;
import com.example.validation.service.RscIotLatestTotalMouldSaturationSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.MathUtils;
import com.example.validation.util.enums.SaturationRange;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotLatestTotalMouldSaturationSummaryServiceImpl implements RscIotLatestTotalMouldSaturationSummaryService {
    private final RscIotLatestTotalMouldSaturationSummaryRepository rscIotLatestTotalMouldSaturationSummaryRepository;
    private final RscIotLatestTotalMouldSaturationSummaryMapper rscIotLatestTotalMouldSaturationSummaryMapper;
    private final RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final MouldSaturationSummaryForThreeMonthsMapper mouldSaturationSummaryForThreeMonthsMapper;
    private final MouldSaturationSummaryForSixMonthsMapper mouldSaturationSummaryForSixMonthsMapper;
    private final MouldSaturationSummaryForTwelveMonthsMapper mouldSaturationSummaryForTwelveMonthsMapper;

    public RscIotLatestTotalMouldSaturationSummaryServiceImpl(RscIotLatestTotalMouldSaturationSummaryRepository rscIotLatestTotalMouldSaturationSummaryRepository, RscIotLatestTotalMouldSaturationSummaryMapper rscIotLatestTotalMouldSaturationSummaryMapper, RscIotLatestTotalMouldSaturationService rscIotLatestTotalMouldSaturationService, RscMtPpheaderService rscMtPpheaderService, MouldSaturationSummaryForThreeMonthsMapper mouldSaturationSummaryForThreeMonthsMapper, MouldSaturationSummaryForSixMonthsMapper mouldSaturationSummaryForSixMonthsMapper, MouldSaturationSummaryForTwelveMonthsMapper mouldSaturationSummaryForTwelveMonthsMapper) {
        this.rscIotLatestTotalMouldSaturationSummaryRepository = rscIotLatestTotalMouldSaturationSummaryRepository;
        this.rscIotLatestTotalMouldSaturationSummaryMapper = rscIotLatestTotalMouldSaturationSummaryMapper;
        this.rscIotLatestTotalMouldSaturationService = rscIotLatestTotalMouldSaturationService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.mouldSaturationSummaryForThreeMonthsMapper = mouldSaturationSummaryForThreeMonthsMapper;
        this.mouldSaturationSummaryForSixMonthsMapper = mouldSaturationSummaryForSixMonthsMapper;
        this.mouldSaturationSummaryForTwelveMonthsMapper = mouldSaturationSummaryForTwelveMonthsMapper;
    }

    @Override
    public void createMouldSaturationSummary(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            if (rscIotLatestTotalMouldSaturationSummaryRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
                List<RscIotLatestTotalMouldSaturationDTO> mouldSaturationDTOS = rscIotLatestTotalMouldSaturationService.findAllByRscMtPPheaderId(ppHeaderId);
                List<RscIotLatestTotalMouldSaturationSummaryDTO> latestTotalMouldSaturationSummaryDTOList = new ArrayList<>();
                latestTotalMouldSaturationSummaryDTOList.addAll(getLatestTotalMouldSaturationSummary(mouldSaturationDTOS));
                saveAll(latestTotalMouldSaturationSummaryDTOList);
            }
        }

    }

    private List<RscIotLatestTotalMouldSaturationSummaryDTO> getLatestTotalMouldSaturationSummary(List<RscIotLatestTotalMouldSaturationDTO> mouldSaturationDTOS) {
        List<RscIotLatestTotalMouldSaturationSummaryDTO> totalMouldSaturationSummaryDTOList = new ArrayList<>();
        mouldSaturationDTOS.forEach(rscIotLatestTotalMouldSaturationDTO -> {
            RscIotLatestTotalMouldSaturationSummaryDTO rscIotLatestTotalMouldSaturationSummaryDTO = new RscIotLatestTotalMouldSaturationSummaryDTO();
            rscIotLatestTotalMouldSaturationSummaryDTO.setRscMtPPheaderId(rscIotLatestTotalMouldSaturationDTO.getRscMtPPheaderId());
            rscIotLatestTotalMouldSaturationSummaryDTO.setRscIotLatestTotalMouldSaturationId(rscIotLatestTotalMouldSaturationDTO.getId());
            rscIotLatestTotalMouldSaturationSummaryDTO.setThreeMonthsAverage(getThreeMonthsAverage(rscIotLatestTotalMouldSaturationDTO));
            rscIotLatestTotalMouldSaturationSummaryDTO.setSixMonthsAverage(getSixMonthsAverage(rscIotLatestTotalMouldSaturationDTO));
            rscIotLatestTotalMouldSaturationSummaryDTO.setTwelveMonthsAverage(getTwelveMonthsAverage(rscIotLatestTotalMouldSaturationDTO));

            totalMouldSaturationSummaryDTOList.add(rscIotLatestTotalMouldSaturationSummaryDTO);
        });
        return totalMouldSaturationSummaryDTOList;
    }

    private Double getTwelveMonthsAverage(RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO) {
        return DoubleUtils.getFormattedValue(MathUtils.divide(rscIotLatestTotalMouldSaturationDTO.getM1Value() + rscIotLatestTotalMouldSaturationDTO.getM2Value() + rscIotLatestTotalMouldSaturationDTO.getM3Value() + rscIotLatestTotalMouldSaturationDTO.getM4Value() + rscIotLatestTotalMouldSaturationDTO.getM5Value() + rscIotLatestTotalMouldSaturationDTO.getM6Value() + rscIotLatestTotalMouldSaturationDTO.getM7Value() + rscIotLatestTotalMouldSaturationDTO.getM8Value() + rscIotLatestTotalMouldSaturationDTO.getM9Value() + rscIotLatestTotalMouldSaturationDTO.getM10Value() + rscIotLatestTotalMouldSaturationDTO.getM11Value() + rscIotLatestTotalMouldSaturationDTO.getM12Value(), 12));
    }

    private Double getSixMonthsAverage(RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO) {
        return DoubleUtils.getFormattedValue(MathUtils.divide(rscIotLatestTotalMouldSaturationDTO.getM1Value() + rscIotLatestTotalMouldSaturationDTO.getM2Value() + rscIotLatestTotalMouldSaturationDTO.getM3Value() + rscIotLatestTotalMouldSaturationDTO.getM4Value() + rscIotLatestTotalMouldSaturationDTO.getM5Value() + rscIotLatestTotalMouldSaturationDTO.getM6Value(), 6));
    }

    private Double getThreeMonthsAverage(RscIotLatestTotalMouldSaturationDTO rscIotLatestTotalMouldSaturationDTO) {
        return DoubleUtils.getFormattedValue(MathUtils.divide(rscIotLatestTotalMouldSaturationDTO.getM1Value() + rscIotLatestTotalMouldSaturationDTO.getM2Value() + rscIotLatestTotalMouldSaturationDTO.getM3Value(), 3));
    }

    private void saveAll(List<RscIotLatestTotalMouldSaturationSummaryDTO> mouldSaturationSummaryDTOS) {
        rscIotLatestTotalMouldSaturationSummaryRepository.saveAll(rscIotLatestTotalMouldSaturationSummaryMapper.toEntity(mouldSaturationSummaryDTOS));
    }


    @Override
    public MouldSaturationDashboardDTO getMouldSaturationDashboardDtoDetails(Long ppHeaderId, Long monthValue) {
        MouldSaturationDashboardDTO mouldSaturationDashboardDTO = new MouldSaturationDashboardDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            mouldSaturationDashboardDTO = getMouldSaturationDTOBasedOnMonthValues(mouldSaturationDashboardDTO, ppHeaderId, monthValue);
            mouldSaturationDashboardDTO.setMpsId(rscMtPPheaderDTO.getId());
            mouldSaturationDashboardDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            mouldSaturationDashboardDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            mouldSaturationDashboardDTO.setHighSaturationRange(">= " + SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal() + "%");
            mouldSaturationDashboardDTO.setMediumSaturationRange(" " + SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal() + "% - " + SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal() + "%");
            mouldSaturationDashboardDTO.setLowSaturationRange("< " + SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal() + "%");
            mouldSaturationDashboardDTO.setHighSaturationRangeListCount(mouldSaturationDashboardDTO.getHighSaturationRangeMouldsDTOList().size());
            mouldSaturationDashboardDTO.setMediumSaturationRangeListCount(mouldSaturationDashboardDTO.getMediumSaturationRangeMouldsDTOList().size());
            mouldSaturationDashboardDTO.setLowSaturationRangeListCount(mouldSaturationDashboardDTO.getLowSaturationRangeMouldsDTOList().size());
            mouldSaturationDashboardDTO.setTotalCount(getTotalCount(mouldSaturationDashboardDTO.getHighSaturationRangeListCount(), mouldSaturationDashboardDTO.getMediumSaturationRangeListCount(), mouldSaturationDashboardDTO.getLowSaturationRangeListCount()));
        }
        return mouldSaturationDashboardDTO;
    }

    private MouldSaturationDashboardDTO getMouldSaturationDTOBasedOnMonthValues(MouldSaturationDashboardDTO mouldSaturationDashboardDTO, Long ppHeaderId, Long monthValue) {
        if (Optional.ofNullable(monthValue).isPresent()) {
            if (monthValue == 3) {
                mouldSaturationDashboardDTO = getMouldSaturationBasedOnFirstThreeMonth(mouldSaturationDashboardDTO, ppHeaderId);
            } else if (monthValue == 6) {
                mouldSaturationDashboardDTO = getMouldSaturationBasedOnFirstSixMonth(mouldSaturationDashboardDTO, ppHeaderId);
            } else {
                mouldSaturationDashboardDTO = getMouldSaturationBasedOnTwelveMonth(mouldSaturationDashboardDTO, ppHeaderId);
            }
        } else {
            mouldSaturationDashboardDTO = getMouldSaturationBasedOnCurrentMonth(mouldSaturationDashboardDTO, ppHeaderId);
        }
        return mouldSaturationDashboardDTO;
    }

    private MouldSaturationDashboardDTO getMouldSaturationBasedOnTwelveMonth(MouldSaturationDashboardDTO mouldSaturationDashboardDTO, Long ppHeaderId) {
        mouldSaturationDashboardDTO.setHighSaturationRangeMouldsDTOList(getMouldSaturationWithHighStatusOfTwelveMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setMediumSaturationRangeMouldsDTOList(getMouldSaturationWithMediumStatusOfTwelveMonthMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setLowSaturationRangeMouldsDTOList(getMouldSaturationWithLowStatusOfTwelveMonthMonth(ppHeaderId));
        return mouldSaturationDashboardDTO;
    }

    private MouldSaturationDashboardDTO getMouldSaturationBasedOnFirstSixMonth(MouldSaturationDashboardDTO mouldSaturationDashboardDTO, Long ppHeaderId) {
        mouldSaturationDashboardDTO.setHighSaturationRangeMouldsDTOList(getMouldSaturationWithHighStatusOfSixMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setMediumSaturationRangeMouldsDTOList(getMouldSaturationWithMediumStatusOfSixMonthMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setLowSaturationRangeMouldsDTOList(getMouldSaturationWithLowStatusOfSixMonthMonth(ppHeaderId));
        return mouldSaturationDashboardDTO;
    }

    private MouldSaturationDashboardDTO getMouldSaturationBasedOnFirstThreeMonth(MouldSaturationDashboardDTO mouldSaturationDashboardDTO, Long ppHeaderId) {
        mouldSaturationDashboardDTO.setHighSaturationRangeMouldsDTOList(getMouldSaturationWithHighStatusOfThreeMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setMediumSaturationRangeMouldsDTOList(getMouldSaturationWithMediumStatusOfThreeMonthMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setLowSaturationRangeMouldsDTOList(getMouldSaturationWithLowStatusOfThreeMonthMonth(ppHeaderId));
        return mouldSaturationDashboardDTO;
    }

    private MouldSaturationDashboardDTO getMouldSaturationBasedOnCurrentMonth(MouldSaturationDashboardDTO mouldSaturationDashboardDTO, Long ppHeaderId) {
        mouldSaturationDashboardDTO.setHighSaturationRangeMouldsDTOList(rscIotLatestTotalMouldSaturationService.getMouldSaturationWithHighStatusForCurrentMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setMediumSaturationRangeMouldsDTOList(rscIotLatestTotalMouldSaturationService.getMouldSaturationWithMediumStatusForCurrentMonth(ppHeaderId));
        mouldSaturationDashboardDTO.setLowSaturationRangeMouldsDTOList(rscIotLatestTotalMouldSaturationService.getMouldSaturationWithLowStatusForCurrentMonth(ppHeaderId));
        return mouldSaturationDashboardDTO;
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithHighStatusOfThreeMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForThreeMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndThreeMonthsAverageGreaterThanEqual(ppHeaderId, Double.valueOf(SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal())));
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithMediumStatusOfThreeMonthMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForThreeMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndThreeMonthsAverageGreaterThanEqualAndThreeMonthsAverageLessThanEqual(ppHeaderId, Double.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal()), Double.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithLowStatusOfThreeMonthMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForThreeMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndThreeMonthsAverageLessThan(ppHeaderId, Double.valueOf(SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }


    public List<MouldSaturationDetailsDTO> getMouldSaturationWithHighStatusOfSixMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForSixMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndSixMonthsAverageGreaterThanEqual(ppHeaderId, Double.valueOf(SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal())));
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithMediumStatusOfSixMonthMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForSixMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndSixMonthsAverageGreaterThanEqualAndSixMonthsAverageLessThanEqual(ppHeaderId, Double.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal()), Double.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithLowStatusOfSixMonthMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForSixMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndSixMonthsAverageLessThan(ppHeaderId, Double.valueOf(SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }


    public List<MouldSaturationDetailsDTO> getMouldSaturationWithHighStatusOfTwelveMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForTwelveMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndTwelveMonthsAverageGreaterThanEqual(ppHeaderId, Double.valueOf(SaturationRange.HIGH_SATURATION_RANGE_MIN_COUNT.getNumVal())));
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithMediumStatusOfTwelveMonthMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForTwelveMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndTwelveMonthsAverageGreaterThanEqualAndTwelveMonthsAverageLessThanEqual(ppHeaderId, Double.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MIN_COUNT.getNumVal()), Double.valueOf(SaturationRange.MEDIUM_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }

    public List<MouldSaturationDetailsDTO> getMouldSaturationWithLowStatusOfTwelveMonthMonth(Long ppHeaderId) {
        return mouldSaturationSummaryForTwelveMonthsMapper.toDto(rscIotLatestTotalMouldSaturationSummaryRepository.findAllByRscMtPPheaderIdAndTwelveMonthsAverageLessThan(ppHeaderId, Double.valueOf(SaturationRange.LOW_SATURATION_RANGE_MAX_COUNT.getNumVal())));
    }

    private Integer getTotalCount(Integer highReceiptCount, Integer mediumReceiptCount, Integer lowReceiptCount) {
        return highReceiptCount + mediumReceiptCount + lowReceiptCount;
    }

    @Override
    public void updateMouldSaturationSummaryForUpdatedPurchasePlan(List<PurchasePlanDTO> purchasePlanDTOList, Long ppHeaderId) {
        if (purchasePlanDTOList.size() > 0) {
            List<Long> uniqueMouldsIdList = rscIotLatestTotalMouldSaturationService.getAllUniqueMouldsList(purchasePlanDTOList);
            if (uniqueMouldsIdList.size() > 0) {
                List<RscIotLatestTotalMouldSaturationDTO> mouldSaturationDTOS = rscIotLatestTotalMouldSaturationService.findAllByRscMtPpHeaderIdAndMouldsList(ppHeaderId, uniqueMouldsIdList);
                List<RscIotLatestTotalMouldSaturationSummaryDTO> latestTotalMouldSaturationSummaryDTOList = getLatestTotalMouldSaturationSummary(mouldSaturationDTOS);
                latestTotalMouldSaturationSummaryDTOList.forEach(rscIotLatestTotalMouldSaturationSummaryDTO -> {
                    rscIotLatestTotalMouldSaturationSummaryDTO.setId(getIdByMouldAndPpheaderId(ppHeaderId, rscIotLatestTotalMouldSaturationSummaryDTO.getRscIotLatestTotalMouldSaturationId()));
                });
                saveAll(latestTotalMouldSaturationSummaryDTOList);
            }
        }
    }

    private Long getIdByMouldAndPpheaderId(Long ppHeaderId, Long mouldId) {
        return rscIotLatestTotalMouldSaturationSummaryRepository.findByRscMtPPheaderIdAndRscIotLatestTotalMouldSaturationId(ppHeaderId, mouldId).getId();
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotLatestTotalMouldSaturationSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
