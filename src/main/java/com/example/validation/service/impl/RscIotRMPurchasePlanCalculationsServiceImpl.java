package com.example.validation.service.impl;

import com.example.validation.dto.RscItItemWiseSupplierDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.RmPurchasePlanCalculationDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCalculationsDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanDTO;
import com.example.validation.mapper.RMPurchasePlanCalculationsMainMapper;
import com.example.validation.mapper.RMPurchasePlanCalculationsMapper;
import com.example.validation.mapper.RmPurchasePlanCalculationMapper;
import com.example.validation.mapper.RscIotRMPurchasePlanCalculationsMapper;
import com.example.validation.repository.RscIotRMPurchasePlanCalculationsRepository;
import com.example.validation.service.RscIotRMPurchasePlanCalculationsService;
import com.example.validation.service.RscItItemWiseSupplierService;
import com.example.validation.service.RscItRMGrossConsumptionService;
import com.example.validation.util.supply.RMPurchasePlanUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotRMPurchasePlanCalculationsServiceImpl implements RscIotRMPurchasePlanCalculationsService {
    private final RscIotRMPurchasePlanCalculationsRepository rscIotRMPurchasePlanCalculationsRepository;
    private final RscIotRMPurchasePlanCalculationsMapper rscIotRMPurchasePlanCalculationsMapper;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RMPurchasePlanUtil rmPurchasePlanUtil;
    private final RMPurchasePlanCalculationsMapper rmPurchasePlanCalculationsMapper;
    private final RmPurchasePlanCalculationMapper rmPurchasePlanCalculationMapper;
    private final RscItItemWiseSupplierService rscItItemWiseSupplierService;
    private final RMPurchasePlanCalculationsMainMapper rmPurchasePlanCalculationsMainMapper;

    public RscIotRMPurchasePlanCalculationsServiceImpl(RscIotRMPurchasePlanCalculationsRepository rscIotRMPurchasePlanCalculationsRepository, RscIotRMPurchasePlanCalculationsMapper rscIotRMPurchasePlanCalculationsMapper,
                                                       RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RMPurchasePlanUtil rmPurchasePlanUtil,
                                                       RMPurchasePlanCalculationsMapper rmPurchasePlanCalculationsMapper, RmPurchasePlanCalculationMapper rmPurchasePlanCalculationMapper,
                                                       RscItItemWiseSupplierService rscItItemWiseSupplierService, RMPurchasePlanCalculationsMainMapper rmPurchasePlanCalculationsMainMapper) {
        this.rscIotRMPurchasePlanCalculationsRepository = rscIotRMPurchasePlanCalculationsRepository;
        this.rscIotRMPurchasePlanCalculationsMapper = rscIotRMPurchasePlanCalculationsMapper;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rmPurchasePlanUtil = rmPurchasePlanUtil;
        this.rmPurchasePlanCalculationsMapper = rmPurchasePlanCalculationsMapper;
        this.rmPurchasePlanCalculationMapper = rmPurchasePlanCalculationMapper;
        this.rscItItemWiseSupplierService = rscItItemWiseSupplierService;
        this.rmPurchasePlanCalculationsMainMapper = rmPurchasePlanCalculationsMainMapper;
    }

    @Override
    public Boolean createRMPurchasePlanCalculations(Long ppHeaderId) {
        if (rscIotRMPurchasePlanCalculationsRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotRMPurchasePlanCalculationsDTO> rscIotRMPurchasePlanCalculationsDTOS = new ArrayList<>();
            List<RscItGrossConsumptionDTO> rscItGrossConsumptionDTOs = rscItRMGrossConsumptionService.findAllRMGrossConsumptionByRscMtPPHeaderId(ppHeaderId);
            if (Optional.ofNullable(rscItGrossConsumptionDTOs).isPresent()) {
                for (RscItGrossConsumptionDTO rscItGrossConsumptionDTO : rscItGrossConsumptionDTOs) {
                    List<RscItItemWiseSupplierDTO> rscItItemWiseSupplierDTOs = rscItItemWiseSupplierService.findByRscMtItemId(rscItGrossConsumptionDTO.getRscMtItemId(),ppHeaderId);
                    for (RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO : rscItItemWiseSupplierDTOs) {
                        RscIotRMPurchasePlanCalculationsDTO rscIotRMPurchasePlanCalculationsDTO = rmPurchasePlanUtil.getRMPurchasePlanCalculationsDTO(rscItGrossConsumptionDTO, rscItItemWiseSupplierDTO);
                        if (Optional.ofNullable(rscIotRMPurchasePlanCalculationsDTO).isPresent())
                            rscIotRMPurchasePlanCalculationsDTOS.add(rscIotRMPurchasePlanCalculationsDTO);
                    }
                }
            }
            rscIotRMPurchasePlanCalculationsRepository.saveAll(rscIotRMPurchasePlanCalculationsMapper.toEntity(rscIotRMPurchasePlanCalculationsDTOS));
        }
        return true;
    }

    @Override
    public List<RscIotRMPurchasePlanDTO> findAllByPPHeaderId(Long ppHeaderId) {
        return rmPurchasePlanCalculationsMapper.toDto(rscIotRMPurchasePlanCalculationsRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RmPurchasePlanCalculationDTO getRMPurchasePlanCalculations(Long rscMtItemId, Long ppHeaderId, Long supplierDetailsId) {
        return rmPurchasePlanCalculationMapper.toDto(rscIotRMPurchasePlanCalculationsRepository.findAllByRscMtItemIdAndRscMtPPheaderIdAndRscIitRMItemWiseSupplierDetailsId(rscMtItemId, ppHeaderId, supplierDetailsId));
    }

    @Override
    public List<PurchasePlanCalculationsMainDTO> findAll(Long ppHeaderId) {
        return rmPurchasePlanCalculationsMainMapper.toDto(rscIotRMPurchasePlanCalculationsRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotRMPurchasePlanCalculationsRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}