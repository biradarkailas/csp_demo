package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanCoverDaysDTO;
import com.example.validation.mapper.PMPurchasePlanCoverDaysEquationMapper;
import com.example.validation.mapper.RscIotPMPurchasePlanCoverDaysMapper;
import com.example.validation.repository.RscIotPMPurchasePlanCoverDaysRepository;
import com.example.validation.service.RscIotPMPurchasePlanCoverDaysService;
import com.example.validation.service.RscIotPmPurchasePlanCalculationsService;
import com.example.validation.util.excel_export.PmPurchaseCoverdaysExporter;
import com.example.validation.util.excel_export.PmPurchaseStockEquationExporter;
import com.example.validation.util.supply.PurchasePlanCoverDaysUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RscIotPMPurchasePlanCoverDaysServiceImpl implements RscIotPMPurchasePlanCoverDaysService {
    private final RscIotPMPurchasePlanCoverDaysRepository rscIotPMPurchasePlanCoverDaysRepository;
    private final RscIotPMPurchasePlanCoverDaysMapper rscIotPMPurchasePlanCoverDaysMapper;
    private final RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService;
    private final PurchasePlanCoverDaysUtil purchasePlanCoverDaysUtil;
    private final PMPurchasePlanCoverDaysEquationMapper pmPurchasePlanCoverDaysEquationMapper;

    public RscIotPMPurchasePlanCoverDaysServiceImpl(RscIotPMPurchasePlanCoverDaysRepository rscIotPMPurchasePlanCoverDaysRepository,
                                                    RscIotPMPurchasePlanCoverDaysMapper rscIotPMPurchasePlanCoverDaysMapper,
                                                    RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService,
                                                    PurchasePlanCoverDaysUtil purchasePlanCoverDaysUtil, PMPurchasePlanCoverDaysEquationMapper pmPurchasePlanCoverDaysEquationMapper) {
        this.rscIotPMPurchasePlanCoverDaysRepository = rscIotPMPurchasePlanCoverDaysRepository;
        this.rscIotPMPurchasePlanCoverDaysMapper = rscIotPMPurchasePlanCoverDaysMapper;
        this.rscIotPmPurchasePlanCalculationsService = rscIotPmPurchasePlanCalculationsService;
        this.purchasePlanCoverDaysUtil = purchasePlanCoverDaysUtil;
        this.pmPurchasePlanCoverDaysEquationMapper = pmPurchasePlanCoverDaysEquationMapper;
    }

    @Override
    public void calculatePMPurchasePlanCoverDays(Long ppHeaderId) {
        if (rscIotPMPurchasePlanCoverDaysRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotPMPurchasePlanCoverDaysDTO> rscIotPMPurchasePlanCoverDaysDTOs = new ArrayList<>();
            List<PurchasePlanCalculationsMainDTO> pmPurchasePlanCalculationsMainDTOs = rscIotPmPurchasePlanCalculationsService.findAll(ppHeaderId);
            for (PurchasePlanCalculationsMainDTO pmPurchasePlanCalculationsDTO : pmPurchasePlanCalculationsMainDTOs) {
                rscIotPMPurchasePlanCoverDaysDTOs.add(purchasePlanCoverDaysUtil.getRscIotPMPurchasePlanCoverDaysDTO(pmPurchasePlanCalculationsDTO));
            }
            rscIotPMPurchasePlanCoverDaysRepository.saveAll(rscIotPMPurchasePlanCoverDaysMapper.toEntity(rscIotPMPurchasePlanCoverDaysDTOs));
        }
    }

    @Override
    public List<RscIotPMPurchasePlanCoverDaysDTO> getAllPMPurchasePlanCoverDays(Long ppHeaderId) {
        return rscIotPMPurchasePlanCoverDaysMapper.toDto(rscIotPMPurchasePlanCoverDaysRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<PurchasePlanCoverDaysEquationDTO> getPurchasePlanCoverDaysEquationDTOs(Long ppHeaderId) {
        return pmPurchasePlanCoverDaysEquationMapper.toDto(rscIotPMPurchasePlanCoverDaysRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    public ExcelExportFiles pmPurchaseCoverdaysExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmPurchaseCoverdaysExporter.pmPurchaseCoverdaysToExcel(getAllPMPurchasePlanCoverDays(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_Purchase_Coverdays + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public ExcelExportFiles pmPurchaseStockEquationExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmPurchaseStockEquationExporter.pmPurchaseStockEquationToExcel(getPurchasePlanCoverDaysEquationDTOs(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_Purchase_Stock_Equation + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotPMPurchasePlanCoverDaysRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}