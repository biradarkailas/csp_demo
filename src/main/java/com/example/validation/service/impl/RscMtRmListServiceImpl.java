package com.example.validation.service.impl;

import com.example.validation.dto.RscMtRmListDTO;
import com.example.validation.mapper.RscMtRmListMapper;
import com.example.validation.repository.RscMtRmListRepository;
import com.example.validation.service.RscMtRmListService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtRmListServiceImpl implements RscMtRmListService {
    private final RscMtRmListMapper rscMtRmListMapper;
    private final RscMtRmListRepository rscMtRmListRepository;

    public RscMtRmListServiceImpl(RscMtRmListMapper rscMtRmListMapper, RscMtRmListRepository rscMtRmListRepository) {
        this.rscMtRmListMapper = rscMtRmListMapper;
        this.rscMtRmListRepository = rscMtRmListRepository;
    }

    @Override
    public List<RscMtRmListDTO> findAllByLotNumberAndRscMtItemId(String lotNumber, Long rscMtItemId) {
        return rscMtRmListMapper.toDto(rscMtRmListRepository.findAllByLotNumberAndRscMtItemId(lotNumber, rscMtItemId));
    }

    @Override
    public void deleteAll(LocalDate mpsDate) {
        rscMtRmListRepository.deleteAllByMpsDate(mpsDate);
    }
}
