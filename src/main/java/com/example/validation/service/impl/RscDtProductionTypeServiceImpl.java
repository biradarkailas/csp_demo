package com.example.validation.service.impl;

import com.example.validation.dto.RscDtProductionTypeDTO;
import com.example.validation.mapper.RscDtProductionTypeMapper;
import com.example.validation.repository.RscDtProductionTypeRepository;
import com.example.validation.service.RscDtProductionTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtProductionTypeServiceImpl implements RscDtProductionTypeService {
    private final RscDtProductionTypeRepository rscDtProductionTypeRepository;
    private final RscDtProductionTypeMapper rscDtProductionTypeMapper;

    public RscDtProductionTypeServiceImpl(RscDtProductionTypeRepository rscDtProductionTypeRepository, RscDtProductionTypeMapper rscDtProductionTypeMapper){
        this.rscDtProductionTypeRepository = rscDtProductionTypeRepository;
        this.rscDtProductionTypeMapper = rscDtProductionTypeMapper;
    }

    @Override
    public List<RscDtProductionTypeDTO> findAll() {
        return rscDtProductionTypeMapper.toDto(rscDtProductionTypeRepository.findAll());
    }
}
