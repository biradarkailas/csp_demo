package com.example.validation.service.impl;

import com.example.validation.dto.RscIotPmOtifReceiptsDTO;
import com.example.validation.mapper.RscIotPmOtifReceiptsMapper;
import com.example.validation.repository.RscIotPmOtifReceiptsRepository;
import com.example.validation.service.RscIotPmOtifReceiptsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscIotPmOtifReceiptsServiceImpl implements RscIotPmOtifReceiptsService {
    private final RscIotPmOtifReceiptsRepository rscIotPmOtifReceiptsRepository;
    private final RscIotPmOtifReceiptsMapper rscIotPmOtifReceiptsMapper;

    public RscIotPmOtifReceiptsServiceImpl(RscIotPmOtifReceiptsRepository rscIotPmOtifReceiptsRepository, RscIotPmOtifReceiptsMapper rscIotPmOtifReceiptsMapper) {
        this.rscIotPmOtifReceiptsRepository = rscIotPmOtifReceiptsRepository;
        this.rscIotPmOtifReceiptsMapper = rscIotPmOtifReceiptsMapper;
    }

    @Override
    public List<RscIotPmOtifReceiptsDTO> getAllRscIotPmOtifReceipts() {
        return rscIotPmOtifReceiptsMapper.toDto(rscIotPmOtifReceiptsRepository.findAll());
    }

    @Override
    public List<RscIotPmOtifReceiptsDTO> getRscIotPmOtifReceiptsByItemAndReceiptHeaderId(Long rscMtItemId, Long latestReceiptHeaderId) {
        return rscIotPmOtifReceiptsMapper.toDto(rscIotPmOtifReceiptsRepository.findAllByRscMtItemIdAndRscMtOtifPmReceiptsHeaderId(rscMtItemId, latestReceiptHeaderId));
    }

    @Override
    public RscIotPmOtifReceiptsDTO getRscIotPmOtifReceiptsByItemAndReceiptHeaderIdAndSupplierId(Long rscMtItemId, Long latestReceiptHeaderId, Long supplierId) {
        return rscIotPmOtifReceiptsMapper.toDto(rscIotPmOtifReceiptsRepository.findByRscMtItemIdAndRscMtOtifPmReceiptsHeaderIdAndRscDtSupplierId(rscMtItemId, latestReceiptHeaderId, supplierId));
    }

    @Override
    public void deleteAll(List<Long> ids) {
        rscIotPmOtifReceiptsRepository.deleteAllByRscMtOtifPmReceiptsHeaderIdIn(ids);
    }
}