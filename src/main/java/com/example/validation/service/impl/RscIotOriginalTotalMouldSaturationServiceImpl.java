package com.example.validation.service.impl;

import com.example.validation.domain.RscIotOriginalTotalMouldSaturation;
import com.example.validation.dto.RscIotLatestTotalMouldSaturationDTO;
import com.example.validation.dto.RscIotOriginalTotalMouldSaturationDTO;
import com.example.validation.mapper.OriginalTotalMouldSaturationMapper;
import com.example.validation.mapper.RscIotOriginalTotalMouldSaturationMapper;
import com.example.validation.repository.RscIotOriginalTotalMouldSaturationRepository;
import com.example.validation.service.RscIotOriginalTotalMouldSaturationService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;
import java.util.function.Predicate;

@Service
@Transactional
public class RscIotOriginalTotalMouldSaturationServiceImpl implements RscIotOriginalTotalMouldSaturationService {
    private final RscIotOriginalTotalMouldSaturationRepository rscIotOriginalTotalMouldSaturationRepository;
    private final RscIotOriginalTotalMouldSaturationMapper rscIotOriginalTotalMouldSaturationMapper;
    private final OriginalTotalMouldSaturationMapper originalTotalMouldSaturationMapper;

    public RscIotOriginalTotalMouldSaturationServiceImpl(RscIotOriginalTotalMouldSaturationRepository rscIotOriginalTotalMouldSaturationRepository, RscIotOriginalTotalMouldSaturationMapper rscIotOriginalTotalMouldSaturationMapper, OriginalTotalMouldSaturationMapper originalTotalMouldSaturationMapper) {
        this.rscIotOriginalTotalMouldSaturationRepository = rscIotOriginalTotalMouldSaturationRepository;
        this.rscIotOriginalTotalMouldSaturationMapper = rscIotOriginalTotalMouldSaturationMapper;
        this.originalTotalMouldSaturationMapper = originalTotalMouldSaturationMapper;
    }

    private static <T> Predicate<T> distinctByMouldId(Function<? super T, Object> keyExtractor) {
        Map<Object, Boolean> map = new ConcurrentHashMap<>();
        return t -> map.putIfAbsent(keyExtractor.apply(t), Boolean.TRUE) == null;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public String saveOriginalTotalMouldSaturation(Long ppheaderId) {
        /*if (count(ppheaderId) == 0) {
            var rscItItemMouldList = rscItItemMouldRepository.findAll();
            var rscItItemMouldDistinctByRscDtMould = rscItItemMouldList.stream().filter(distinctByMouldId(p -> p.getRscDtMould().getId())).collect(Collectors.toList());

            for (RscItItemMould rscItItemMould : rscItItemMouldDistinctByRscDtMould) {
                Double month1Total = 0.0;
                Double month2Total = 0.0;
                Double month3Total = 0.0;
                Double month4Total = 0.0;
                Double month5Total = 0.0;
                Double month6Total = 0.0;
                Double month7Total = 0.0;
                Double month8Total = 0.0;
                Double month9Total = 0.0;
                Double month10Total = 0.0;
                Double month11Total = 0.0;
                Double month12Total = 0.0;
                Double rtdValue = 0.0;
                Double totalCapacity = 0.0;
                var rscItItemMouldRepositoryByRscDtMouldId = rscItItemMouldRepository.findByRscDtMouldId(rscItItemMould.getRscDtMould().getId());

                for (RscItItemMould rscItItemMould1 : rscItItemMouldRepositoryByRscDtMouldId) {
                    var originalPmPurchasePlan = rscIotPMOriginalPurchasePlanMapper.toDto(rscIotPMOriginalPurchasePlanRepository.findAllByRscMtPPheaderIdAndRscMtItemId(ppheaderId, rscItItemMould1.getRscMtItem().getId()));

                    if (Optional.ofNullable(originalPmPurchasePlan).isPresent()) {
                        month1Total += originalPmPurchasePlan.getM1Value();
                        month2Total += originalPmPurchasePlan.getM2Value();
                        month3Total += originalPmPurchasePlan.getM3Value();
                        month4Total += originalPmPurchasePlan.getM4Value();
                        month5Total += originalPmPurchasePlan.getM5Value();
                        month6Total += originalPmPurchasePlan.getM6Value();
                        month7Total += originalPmPurchasePlan.getM7Value();
                        month8Total += originalPmPurchasePlan.getM8Value();
                        month9Total += originalPmPurchasePlan.getM9Value();
                        month10Total += originalPmPurchasePlan.getM10Value();
                        month11Total += originalPmPurchasePlan.getM11Value();
                        month12Total += originalPmPurchasePlan.getM12Value();
                        rtdValue += originalPmPurchasePlan.getRtdValue();
                        totalCapacity = rscItItemMould1.getRscDtMould().getTotalCapacity();
                    }
                }
                RscIotOriginalTotalMouldSaturationDTO rscIotOriginalTotalMouldSaturationDTO = new RscIotOriginalTotalMouldSaturationDTO();
                RscIitMpsWorkingDaysDTO rscIitMpsWorkingDay = rscIitMpsWorkingDaysService.findByMpsId(ppheaderId);

                rscIotOriginalTotalMouldSaturationDTO.setM1Value(MouldSaturationUtil.getMonth1MouldSaturation(month1Total, month2Total, rscIitMpsWorkingDay.getMonth11Value(), rtdValue, totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM2Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month3Total, rscIitMpsWorkingDay.getMonth2Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM3Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month4Total, rscIitMpsWorkingDay.getMonth3Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM4Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month5Total, rscIitMpsWorkingDay.getMonth4Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM5Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month6Total, rscIitMpsWorkingDay.getMonth5Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM6Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month7Total, rscIitMpsWorkingDay.getMonth6Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM7Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month8Total, rscIitMpsWorkingDay.getMonth7Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM8Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month9Total, rscIitMpsWorkingDay.getMonth8Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM9Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month10Total, rscIitMpsWorkingDay.getMonth9Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM10Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month11Total, rscIitMpsWorkingDay.getMonth10Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM11Value(MouldSaturationUtil.getMonthWiseMouldSaturation(month12Total, rscIitMpsWorkingDay.getMonth11Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setM12Value(MouldSaturationUtil.getMonthWiseMouldSaturation(0.0, rscIitMpsWorkingDay.getMonth12Value(), totalCapacity));
                rscIotOriginalTotalMouldSaturationDTO.setRscMtPPheaderId(ppheaderId);
                rscIotOriginalTotalMouldSaturationDTO.setRscDtMouldId(rscItItemMould.getRscDtMould().getId());

                rscIotOriginalTotalMouldSaturationRepository.save(rscIotOriginalTotalMouldSaturationMapper.toEntity(rscIotOriginalTotalMouldSaturationDTO));
            }
        }*/
        return "Original Total Mould Saturation saved successfully";
    }

    @Override
    public void saveAllOriginalTotalMouldSaturation(List<RscIotLatestTotalMouldSaturationDTO> latestTotalMouldSaturationDTOList) {
        rscIotOriginalTotalMouldSaturationRepository.saveAll(originalTotalMouldSaturationMapper.toEntity(latestTotalMouldSaturationDTOList));
    }

    @Override
    public void save(RscIotOriginalTotalMouldSaturationDTO rscIotOriginalTotalMouldSaturationDTO) {
        rscIotOriginalTotalMouldSaturationRepository.save(rscIotOriginalTotalMouldSaturationMapper.toEntity(rscIotOriginalTotalMouldSaturationDTO));
    }

    @Override
    public List<RscIotOriginalTotalMouldSaturationDTO> findAll() {
        List<RscIotOriginalTotalMouldSaturation> rscIotOriginalMouldSaturationList = rscIotOriginalTotalMouldSaturationRepository.findAll();
        return rscIotOriginalTotalMouldSaturationMapper.toDto(rscIotOriginalMouldSaturationList);
    }

  /*  @Override
    public ExcelExportFiles mouldWiseSaturationToExcel(Long ppheaderId) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppheaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            excelExportFiles.setFile(PmMouldWiseMouldExporter.mouldWiseToExcel(rscIotLatestTotalMouldSaturationService.findAllByRscMtPPheaderId(ppheaderId), monthService.getMonthNameDetail(ppheaderId), optionalRscMtPpHeaderDTO.get().getMpsDate()));
            excelExportFiles.setFileName(ExcelFileNameConstants.PM_MOULD_WISE_MOULD + optionalRscMtPpHeaderDTO.get().getMpsDate() + ExcelFileNameConstants.Extension);
        }
        return excelExportFiles;
    }*/

  /*  @Override
    public Long count(Long ppHeaderId) {
        return rscIotOriginalTotalMouldSaturationRepository.countByRscMtPPheaderId(ppHeaderId);
    }*/

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotOriginalTotalMouldSaturationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
