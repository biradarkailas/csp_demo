package com.example.validation.service.impl;

import com.example.validation.dto.*;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import com.example.validation.mapper.RscIotPmOtifCalculationMapper;
import com.example.validation.repository.RscIotPmOtifCalculationRepository;
import com.example.validation.service.*;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.ListUtils;
import com.example.validation.util.MathUtils;
import com.example.validation.util.enums.SupplierWiseReceiptRange;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotPmOtifCalculationServiceImpl implements RscIotPmOtifCalculationService {
    private final RscIotPmOtifCalculationRepository rscIotPmOtifCalculationRepository;
    private final RscIotPmOtifCalculationMapper rscIotPmOtifCalculationMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotPMLatestPurchasePlanService rscIotPMLatestPurchasePlanService;
    private final RscIotPmOtifReceiptsService rscIotPmOtifReceiptsService;
    private final RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService;
    private final RscIitIdpStatusService rscIitIdpStatusService;

    public RscIotPmOtifCalculationServiceImpl(RscIotPmOtifCalculationRepository rscIotPmOtifCalculationRepository, RscIotPmOtifCalculationMapper rscIotPmOtifCalculationMapper, RscMtPpheaderService rscMtPpheaderService, RscIotPMLatestPurchasePlanService rscIotPMLatestPurchasePlanService, RscIotPmOtifReceiptsService rscIotPmOtifReceiptsService, RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService, RscIitIdpStatusService rscIitIdpStatusService) {
        this.rscIotPmOtifCalculationRepository = rscIotPmOtifCalculationRepository;
        this.rscIotPmOtifCalculationMapper = rscIotPmOtifCalculationMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotPMLatestPurchasePlanService = rscIotPMLatestPurchasePlanService;
        this.rscIotPmOtifReceiptsService = rscIotPmOtifReceiptsService;
        this.rscMtOtifPmReceiptsHeaderService = rscMtOtifPmReceiptsHeaderService;
        this.rscIitIdpStatusService = rscIitIdpStatusService;
    }

    @Override
    public void calculateRscIotPmOtifCalculation(String otifDate) {
        if (!otifDate.equals("null")) {
            RscMtOtifPmReceiptsHeaderDTO rscMtOtifPmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.findByOtifDate(LocalDate.parse(otifDate));
            if (Optional.ofNullable(rscMtOtifPmReceiptsHeaderDTO).isPresent()) {
                Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findValidatedMpsPlanByOtifDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate());
                if (pPheaderDTO.isPresent()) {
                    if (rscIotPmOtifCalculationRepository.countByReceiptHeaderDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate()) == 0) {
//                        Long pPheaderId = getPrevMonthsValidatedPlan(pPheaderDTO.get().getId());
                        List<RscIotPMPurchasePlanDTO> purchasePlanDTOList = rscIotPMLatestPurchasePlanService.getPurchasePlanById(pPheaderDTO.get().getId());
                        if (Optional.ofNullable(purchasePlanDTOList).isPresent()) {
                            List<RscIotPmOtifCalculationDTO> pmOtifCalculationDTOList = getPmOtifCalculationList(purchasePlanDTOList, pPheaderDTO.get().getId(), rscMtOtifPmReceiptsHeaderDTO);
                            saveAll(pmOtifCalculationDTOList);
                        }
                    }
                }
            }
        }
    }

    @Override
    public void saveAll(List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList) {
        rscIotPmOtifCalculationRepository.saveAll(rscIotPmOtifCalculationMapper.toEntity(rscIotPmOtifCalculationDTOList));
    }

    @Override
    public Long getPpheaderIdByDate(String otifDate) {
        if (!otifDate.equals("null")) {
            RscMtOtifPmReceiptsHeaderDTO rscMtOtifPmReceiptsHeaderDTO = rscMtOtifPmReceiptsHeaderService.findByOtifDate(LocalDate.parse(otifDate));
            if (Optional.ofNullable(rscMtOtifPmReceiptsHeaderDTO).isPresent()) {
                Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findValidatedMpsPlanByOtifDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate());
                if (pPheaderDTO.isPresent()) {
                    return pPheaderDTO.get().getId();
                } else {
                    return null;
                }
            } else {
                return null;
            }
        } else {
            return null;
        }
    }


    private Long getPrevMonthsValidatedPlan(Long rscMtPPHeaderId) {
        return rscMtPpheaderService.getPreviousMonthMpsId(rscMtPPHeaderId);
    }

    private List<RscIotPmOtifCalculationDTO> getPmOtifCalculationList(List<RscIotPMPurchasePlanDTO> purchasePlanDTOList, Long rscMtPPHeaderId, RscMtOtifPmReceiptsHeaderDTO rscMtOtifPmReceiptsHeaderDTO) {
        List<RscIotPmOtifCalculationDTO> pmOtifCalculationDTOList = new ArrayList<>();
        purchasePlanDTOList.forEach(rscIotPMPurchasePlanDTO -> {
            if (getTotalOrderQuanitity(rscIotPMPurchasePlanDTO) > 0) {
                Double otifReceiptQuantityTotal = 0.0;
                RscIotPmOtifCalculationDTO rscIotPmOtifCalculationDTO = new RscIotPmOtifCalculationDTO();
                rscIotPmOtifCalculationDTO.setRscIotPMLatestPurchasePlanId(rscIotPMPurchasePlanDTO.getId());
                rscIotPmOtifCalculationDTO.setRscMtItemId(rscIotPMPurchasePlanDTO.getRscMtItemId());
                rscIotPmOtifCalculationDTO.setOtifCalcDate(LocalDate.now());
                rscIotPmOtifCalculationDTO.setRscMtPPheaderId(rscMtPPHeaderId);
                rscIotPmOtifCalculationDTO.setTotalOrderQty(getTotalOrderQuanitity(rscIotPMPurchasePlanDTO));
                rscIotPmOtifCalculationDTO.setRscDtSupplierId(rscIotPMPurchasePlanDTO.getRscDtSupplierId());
                rscIotPmOtifCalculationDTO.setCreatedDate(LocalDateTime.now());
                rscIotPmOtifCalculationDTO.setReceiptHeaderDate(rscMtOtifPmReceiptsHeaderDTO.getOtifDate());
                List<RscIotPmOtifReceiptsDTO> rscIotPmOtifReceiptsDTO = rscIotPmOtifReceiptsService.getRscIotPmOtifReceiptsByItemAndReceiptHeaderId(rscIotPMPurchasePlanDTO.getRscMtItemId(), rscMtOtifPmReceiptsHeaderDTO.getId());
                otifReceiptQuantityTotal = rscIotPmOtifReceiptsDTO.stream().mapToDouble(RscIotPmOtifReceiptsDTO::getQuantity).sum();
                if (Optional.ofNullable(rscIotPmOtifReceiptsDTO).isPresent()) {
                    rscIotPmOtifCalculationDTO.setTotalReceivedQty(otifReceiptQuantityTotal);
                    rscIotPmOtifCalculationDTO.setBalanceQty(getBalanceQuantity(rscIotPmOtifCalculationDTO.getTotalOrderQty(), otifReceiptQuantityTotal));
                    rscIotPmOtifCalculationDTO.setReceiptPercentage(getReciptPercentage(otifReceiptQuantityTotal, rscIotPmOtifCalculationDTO.getTotalOrderQty()));
                    if (rscIotPmOtifCalculationDTO.getReceiptPercentage() > SupplierWiseReceiptRange.RECEIPT_RANGE_MIN_COUNT_FOR_SCORE4.getNumVal()) {
                        rscIotPmOtifCalculationDTO.setScore(1);
                    } else {
                        rscIotPmOtifCalculationDTO.setScore(0);
                    }
                } else {
                    rscIotPmOtifCalculationDTO.setTotalReceivedQty(0.0);
                    rscIotPmOtifCalculationDTO.setBalanceQty(0.0);
                    rscIotPmOtifCalculationDTO.setReceiptPercentage(0.0);
                    rscIotPmOtifCalculationDTO.setScore(0);
                }
                pmOtifCalculationDTOList.add(rscIotPmOtifCalculationDTO);
            }
        });
        return pmOtifCalculationDTOList;
    }

    private Double getTotalOrderQuanitity(RscIotPMPurchasePlanDTO purchasePlanDTO) {
        return MathUtils.add(purchasePlanDTO.getM1Value(), purchasePlanDTO.getM2Value());
    }

    private Double getBalanceQuantity(Double totalOrderQty, Double recivedQty) {
        return MathUtils.sub(totalOrderQty, recivedQty);
    }

    private Double getReciptPercentage(Double recivedQty, Double totalOrderQty) {
        if (totalOrderQty != 0.0) {
            return DoubleUtils.round(MathUtils.divide(recivedQty, totalOrderQty) * 100);
        } else {
            return 0.0;
        }
    }

    @Override
    public List<RscIotPmOtifCalculationDTO> findAllByIdAndReceiptOtifDate(Long rscMtPPHeaderId, LocalDate otifDate) {
        return rscIotPmOtifCalculationMapper.toDto(rscIotPmOtifCalculationRepository.findAllByRscMtPPheaderIdAndReceiptHeaderDate(rscMtPPHeaderId, otifDate));
    }

    @Override
    public List<RscIotPmOtifCalculationDTO> findAllBySupplierIdAndRscMtPPheaderId(Long supplierId, Long rscMtPPHeaderId) {
        return rscIotPmOtifCalculationMapper.toDto(rscIotPmOtifCalculationRepository.findAllByRscDtSupplierIdAndRscMtPPheaderId(supplierId, rscMtPPHeaderId));
    }

    @Override
    public List<RscIotPmOtifCalculationDTO> findAllBySupplierIdAndRscMtPPheaderIdAndOtifReceiptDate(Long supplierId, Long rscMtPPHeaderId, LocalDate otifDate) {
        return rscIotPmOtifCalculationMapper.toDto(rscIotPmOtifCalculationRepository.findAllByRscDtSupplierIdAndRscMtPPheaderIdAndReceiptHeaderDate(supplierId, rscMtPPHeaderId, otifDate));
    }

    @Override
    public List<RscIotPmOtifCalculationDTO> findAllBySupplierIdAndOtifReceiptDate(Long supplierId, LocalDate otifDate) {
        return rscIotPmOtifCalculationMapper.toDto(rscIotPmOtifCalculationRepository.findAllByRscDtSupplierIdAndReceiptHeaderDate(supplierId, otifDate));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotPmOtifCalculationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<LocalDate> getPMOtifDateListOfCurrentMonth(String mpsName, String mpsDate) {
        List<LocalDate> otifDateList = new ArrayList<>();
        List<RscIitIdpStatusDTO> idpStatusDTOList = rscIitIdpStatusService.findRscIitIdpStatusByNameAndDate(mpsName, LocalDate.parse(mpsDate));
        if (Optional.ofNullable(idpStatusDTOList).isPresent()) {
            otifDateList = rscIotPmOtifCalculationMapper.toDto(rscIotPmOtifCalculationRepository.findAllByReceiptHeaderDateBetween(LocalDate.parse(mpsDate).with(TemporalAdjusters.firstDayOfMonth()), LocalDate.parse(mpsDate).with(TemporalAdjusters.lastDayOfMonth()))).stream().filter(ListUtils.distinctByKey(rscIotPmOtifCalculationDTO -> rscIotPmOtifCalculationDTO.getReceiptHeaderDate())).map(value -> value.getReceiptHeaderDate()).collect(Collectors.toList());
        }
        return otifDateList;
    }
}