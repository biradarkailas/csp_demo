package com.example.validation.service.impl;

import com.example.validation.dto.RscDtTagDTO;
import com.example.validation.mapper.RscDtTagMapper;
import com.example.validation.repository.RscDtTagRepository;
import com.example.validation.service.RscDtTagService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtTagServiceImpl  implements RscDtTagService {
    private final RscDtTagRepository rscDtTagRepository;
    private  final RscDtTagMapper rscDtTagMapper;

    public RscDtTagServiceImpl(RscDtTagRepository rscDtTagRepository,RscDtTagMapper rscDtTagMapper){
        this.rscDtTagRepository = rscDtTagRepository;
        this.rscDtTagMapper = rscDtTagMapper;
    }

    @Override
    public List<RscDtTagDTO> findAll() {
        return rscDtTagMapper.toDto(rscDtTagRepository.findAll());
    }
}

