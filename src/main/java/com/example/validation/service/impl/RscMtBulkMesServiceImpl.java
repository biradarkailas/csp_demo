package com.example.validation.service.impl;

import com.example.validation.dto.RscMtBulkMesDTO;
import com.example.validation.mapper.RscMtBulkMesMapper;
import com.example.validation.repository.RscMtBulkMesRepository;
import com.example.validation.service.RscMtBulkMesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtBulkMesServiceImpl implements RscMtBulkMesService {
    private final RscMtBulkMesRepository rscMtBulkMesRepository;
    private final RscMtBulkMesMapper rscMtBulkMesMapper;

    public RscMtBulkMesServiceImpl(RscMtBulkMesRepository rscMtBulkMesRepository, RscMtBulkMesMapper rscMtBulkMesMapper) {
        this.rscMtBulkMesRepository = rscMtBulkMesRepository;
        this.rscMtBulkMesMapper = rscMtBulkMesMapper;
    }

    @Override
    public List<RscMtBulkMesDTO> findAll() {
        return rscMtBulkMesMapper.toDto( rscMtBulkMesRepository.findAll() );
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtBulkMesRepository.deleteAllByStockDate(stockDate);
    }
}