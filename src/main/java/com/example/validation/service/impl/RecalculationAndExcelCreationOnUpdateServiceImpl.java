package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.domain.RscIotRmSlob;
import com.example.validation.domain.RscItPMSlob;
import com.example.validation.dto.*;
import com.example.validation.mapper.PmSlobMapper;
import com.example.validation.mapper.RmSlobMapper;
import com.example.validation.repository.RscIotRmSlobRepository;
import com.example.validation.repository.RscItPMSlobRepository;
import com.example.validation.service.*;
import com.example.validation.util.DoubleUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RecalculationAndExcelCreationOnUpdateServiceImpl implements RecalculationAndExcelCreationOnUpdateService {
    private final RscIotRmSlobRepository rscIotRmSlobRepository;
    private final RmSlobMapper rmSlobMapper;
    private final RscIotRMSlobService rscIotRMSlobService;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;
    private final RscIotRmLatestSlobReasonService rscIotRmLatestSlobReasonService;
    private final RscIotRmCoverDaysService rscIotRmCoverDaysService;
    private final RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService;
    private final RscItPMSlobRepository rscItPMSlobRepository;
    private final RscIotPmLatestSlobReasonService rscIotPmLatestSlobReasonService;
    private final RscIotPmSlobSummaryService rscIotPmSlobSummaryService;
    private final PmSlobMapper pmSlobMapper;
    private final RscItPMSlobService rscItPMSlobService;
    private final RscIotPmCoverDaysService rscIotPmCoverDaysService;
    private final RscIotPmCoverDaysSummaryService rscIotPmCoverDaysSummaryService;
    private final AllExcelExportService allExcelExportService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService;
    private final ExecutionErrorLogService executionErrorLogService;
    private final MonthService monthService;

    public RecalculationAndExcelCreationOnUpdateServiceImpl(RscIotRmSlobRepository rscIotRmSlobRepository, RmSlobMapper rmSlobMapper, RscIotRMSlobService rscIotRMSlobService, RscIotRmSlobSummaryService rscIotRmSlobSummaryService, RscIotRmLatestSlobReasonService rscIotRmLatestSlobReasonService, RscIotRmCoverDaysService rscIotRmCoverDaysService, RscIotRmCoverDaysSummaryService rscIotRmCoverDaysSummaryService, RscItPMSlobRepository rscItPMSlobRepository, RscIotPmLatestSlobReasonService rscIotPmLatestSlobReasonService, RscIotPmSlobSummaryService rscIotPmSlobSummaryService, PmSlobMapper pmSlobMapper, RscItPMSlobService rscItPMSlobService, RscIotPmCoverDaysService rscIotPmCoverDaysService, RscIotPmCoverDaysSummaryService rscIotPmCoverDaysSummaryService, AllExcelExportService allExcelExportService, RscMtPpheaderService rscMtPpheaderService, RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService, ExecutionErrorLogService executionErrorLogService, MonthService monthService) {
        this.rscIotRmSlobRepository = rscIotRmSlobRepository;
        this.rmSlobMapper = rmSlobMapper;
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
        this.rscIotRmLatestSlobReasonService = rscIotRmLatestSlobReasonService;
        this.rscIotRmCoverDaysService = rscIotRmCoverDaysService;
        this.rscIotRmCoverDaysSummaryService = rscIotRmCoverDaysSummaryService;
        this.rscItPMSlobRepository = rscItPMSlobRepository;
        this.rscIotPmLatestSlobReasonService = rscIotPmLatestSlobReasonService;
        this.rscIotPmSlobSummaryService = rscIotPmSlobSummaryService;
        this.pmSlobMapper = pmSlobMapper;
        this.rscItPMSlobService = rscItPMSlobService;
        this.rscIotPmCoverDaysService = rscIotPmCoverDaysService;
        this.rscIotPmCoverDaysSummaryService = rscIotPmCoverDaysSummaryService;
        this.allExcelExportService = allExcelExportService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotRmCoverDaysClassSummaryService = rscIotRmCoverDaysClassSummaryService;
        this.executionErrorLogService = executionErrorLogService;
        this.monthService = monthService;
    }

    //Rm slob recalculation
    //Rm slob recalculation
    @Override
    public List<RmSlobDTO> saveAllRmSlobDetails(List<RmSlobDTO> rmSlobDTOList, Double sitValue) {
        List<RmSlobDTO> rmSlobList = new ArrayList<>();
        Double mapPrice;
        Double totalSlobValue;
        Double expirySlobValue;
        for (RmSlobDTO rmSlobDTO : rmSlobDTOList) {
            expirySlobValue = 0.0;
            totalSlobValue = 0.0;

            if (rmSlobDTO.getMap() != null) {
                mapPrice = rmSlobDTO.getMap();
                rmSlobDTO.setPrice(rmSlobDTO.getMap());
                if (Optional.ofNullable(rmSlobDTO.getExpiredStock()).isPresent()) {
                    rmSlobDTO.setObsoleteExpiryValue(rmSlobDTO.getExpiredStock() * mapPrice);
                    expirySlobValue = rmSlobDTO.getObsoleteExpiryValue();
                    totalSlobValue += expirySlobValue;
                }
                if (Optional.ofNullable(rmSlobDTO.getMidNightStock()).isPresent()) {
                    rmSlobDTO.setStockValue(DoubleUtils.round(rmSlobDTO.getMidNightStock() * mapPrice));
                }
                if (Optional.ofNullable(rmSlobDTO.getFinalStock()).isPresent()) {
                    rmSlobDTO.setTotalStockValue(rmSlobDTO.getFinalStock() * mapPrice);
                }
                if (!Optional.ofNullable(rmSlobDTO.getTotalConsumptionQuantity()).isPresent()) {
                    if (Optional.ofNullable(mapPrice).isPresent() && rmSlobDTO.getBalanceStock() != 0) {
                        rmSlobDTO.setObsoleteNonExpiryValue(rmSlobDTO.getBalanceStock() * mapPrice);
                        rmSlobDTO.setTotalObValue(expirySlobValue + rmSlobDTO.getObsoleteNonExpiryValue());
                        totalSlobValue += rmSlobDTO.getObsoleteNonExpiryValue();
                    }
                } else {
                    rmSlobDTO.setTotalObValue(expirySlobValue);
                }

                if (!Optional.ofNullable(rmSlobDTO.getObsoleteNonExpiryValue()).isPresent()) {
                    if (Optional.ofNullable(mapPrice).isPresent() && Optional.ofNullable(rmSlobDTO.getBalanceStock()).isPresent()) {
                        rmSlobDTO.setSlowMovingValue(DoubleUtils.round(rmSlobDTO.getBalanceStock() * mapPrice));
                        totalSlobValue += rmSlobDTO.getSlowMovingValue();
                    }
                } else {
                    if (Optional.ofNullable(mapPrice).isPresent() && Optional.ofNullable(rmSlobDTO.getBalanceStock()).isPresent()) {
                        rmSlobDTO.setSlowMovingValue(DoubleUtils.round((rmSlobDTO.getBalanceStock() * mapPrice) - rmSlobDTO.getObsoleteNonExpiryValue()));
                        totalSlobValue += rmSlobDTO.getSlowMovingValue();
                    }
                }
                rmSlobDTO.setTotalSlobValue(DoubleUtils.round(totalSlobValue));
                rmSlobDTO.setSlobValue(DoubleUtils.round(totalSlobValue));
            }
            rmSlobList.add(rmSlobDTO);
        }
        rscIotRmSlobRepository.saveAll(rmSlobMapper.toEntity(rmSlobList));
        rscIotRmSlobSummaryService.deleteById(rmSlobDTOList.get(0).getRscMtPPheaderId());
        rscIotRMSlobService.calculateRscItRMSlobSummary(rmSlobDTOList.get(0).getRscMtPPheaderId(), sitValue);
//        recalculationOfTablesBasedOnRmSlob(rmSlobDTOList.get(0).getRscMtPPheaderId());
        recalculateRmCoverDaysAndSummary(rmSlobDTOList.get(0).getRscMtPPheaderId());
        return rmSlobDTOList;
    }


    @Override
    public List<RmSlobDTO> saveAllRmSlobs(List<RmSlobDTO> rmSlobDTOList, Double sitValue, Long ppHeaderId) {
        List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS = rscIotRmLatestSlobReasonService.findAll();
        RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId(ppHeaderId);
        if (Optional.ofNullable(rmSlobDTOList).isPresent() && rmSlobDTOList.size() > 0) {
            rmSlobDTOList.forEach(rmSlobDTO -> {
                saveRscIotRmLatestSlob(rmSlobDTO, rscIotRmLatestSlobReasonDTOS);
            });
        }
        List<RscIotRmSlob> rscItPMSlobs = rscIotRmSlobRepository.saveAll(rmSlobMapper.toEntity(rmSlobDTOList));
        if (Optional.ofNullable(rscIotRmSlobSummaryDTO).isPresent()) {
            rscIotRmSlobSummaryService.deleteById(ppHeaderId);
            rscIotRMSlobService.calculateRscItRMSlobSummary(ppHeaderId, sitValue);
            /*if (!rscIotRmSlobSummaryDTO.getSitValue().equals(sitValue)) {
                rscIotRmSlobSummaryService.deleteById(ppHeaderId);
                rscIotRMSlobService.calculateRscItRMSlobSummary(ppHeaderId, sitValue);
            } else {
                rscIotRmSlobSummaryService.deleteById(ppHeaderId);
                rscIotRMSlobService.calculateRscItRMSlobSummary(ppHeaderId, null);
            }*/
        }
        recalculateRmCoverDaysAndSummary(ppHeaderId);
//        recalculationOfTablesBasedOnRmSlob(ppHeaderId);
        return rmSlobMapper.toDto(rscItPMSlobs);
    }

    private void saveRscIotRmLatestSlob(RmSlobDTO rmSlobDTO, List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS) {
        List<RscIotRmLatestSlobReasonDTO> iotRmLatestSlobReasonDTOS = rscIotRmLatestSlobReasonDTOS.stream().filter(rscIotPmLatestSlobReasonDTO -> rscIotPmLatestSlobReasonDTO.getRscMtItemId().equals(rmSlobDTO.getRscMtItemId())).collect(Collectors.toList());
        if (iotRmLatestSlobReasonDTOS.size() == 0) {
            RscIotRmLatestSlobReasonDTO rscIotRmLatestSlobReasonDTO = new RscIotRmLatestSlobReasonDTO();
            rscIotRmLatestSlobReasonDTO.setRscMtItemId(rmSlobDTO.getRscMtItemId());
            rscIotRmLatestSlobReasonDTO.setRscDtReasonId(rmSlobDTO.getReasonId());
            rscIotRmLatestSlobReasonDTO.setRemark(rmSlobDTO.getRemark());
            rscIotRmLatestSlobReasonService.saveOne(rscIotRmLatestSlobReasonDTO);
        } else {
            rscIotRmLatestSlobReasonDTOS.forEach(rscIotRmLatestSlobReasonDTO -> {
                RscIotRmLatestSlobReasonDTO rmLatestSlobReasonDTO = new RscIotRmLatestSlobReasonDTO();
                rmLatestSlobReasonDTO.setId(rscIotRmLatestSlobReasonDTO.getId());
                rmLatestSlobReasonDTO.setRscMtItemId(rscIotRmLatestSlobReasonDTO.getRscMtItemId());
                if (rmSlobDTO.getReasonId() != null) {
                    rmLatestSlobReasonDTO.setRscDtReasonId(rmSlobDTO.getReasonId());
                }
                if (rmSlobDTO.getRemark() != null) {
                    rmLatestSlobReasonDTO.setRemark(rmSlobDTO.getRemark());
                }
                rscIotRmLatestSlobReasonService.saveOne(rmLatestSlobReasonDTO);
            });
        }
    }

    private void recalculateRmCoverDaysAndSummary(Long ppHeaderId) {
//        rscIotRmCoverDaysClassSummaryService.deleteClassSummaryById(ppHeaderId);
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {

            try {
                rscIotRmCoverDaysService.deleteById(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("deleteById", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
            try {
                rscIotRmCoverDaysSummaryService.deleteAllById(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("deleteAllById", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
            try {
                rscIotRmCoverDaysService.calculateRscIotRmCoverDays(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("calculateRscIotRmCoverDays", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
            try {
                rscIotRmCoverDaysService.calculateRscIotRmCoverDaysSummary(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("calculateRscIotRmCoverDaysSummary", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
        }
    }

    //Pm slob recalculation
    @Override
    public List<PmSlobDTO> saveAllPmSlobDetails(List<PmSlobDTO> pmSlobsLists, Double sitValue) {
        List<RscItPMSlob> pmSlobList = new ArrayList();
        pmSlobsLists.forEach(pmSlob -> {
            Double mapPrice = null;
            if (pmSlob.getMap() != null) {
                mapPrice = pmSlob.getMap();
                pmSlob.setMap(pmSlob.getMap());
                pmSlob.setPrice(pmSlob.getMap());
            }
            if (mapPrice != null) {
                if (pmSlob.getSlobQuantity() != null) {
                    pmSlob.setSlobValue(DoubleUtils.round(pmSlob.getSlobQuantity() * mapPrice));
                }
                pmSlob.setValue(DoubleUtils.round(pmSlob.getStock() * mapPrice));
            }
            pmSlobList.add(pmSlobMapper.toEntity(pmSlob));
        });
        rscItPMSlobRepository.saveAll(pmSlobList);
        rscIotPmSlobSummaryService.deleteById(pmSlobsLists.get(0).getRscMtPPheaderId());
        rscItPMSlobService.calculateRscItPMSlobSummary(pmSlobsLists.get(0).getRscMtPPheaderId(), sitValue);
        //recalculationOfTablesBasedOnPmSlob(pmSlobsLists.get(0).getRscMtPPheaderId());
        recalculatePmCoverDaysAndSummary(pmSlobsLists.get(0).getRscMtPPheaderId());
        return pmSlobsLists;
    }

    @Override
    public List<PmSlobDTO> saveAllPmSlobs(List<PmSlobDTO> pmSlobDTOList, Double sitValue, Long ppHeaderId) {
        List<RscIotPmLatestSlobReasonDTO> rscIotPmLatestSlobReasonDTOS = rscIotPmLatestSlobReasonService.findAll();
        RscIotPmSlobSummaryDTO rscIotPmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(ppHeaderId);

        if (Optional.ofNullable(pmSlobDTOList).isPresent() && pmSlobDTOList.size() > 0) {
            pmSlobDTOList.forEach(pmSlobDTO -> {
                saveRscIotPmLatestSlob(pmSlobDTO, rscIotPmLatestSlobReasonDTOS);
            });
        }
        List<RscItPMSlob> rscItPMSlobs = rscItPMSlobRepository.saveAll(pmSlobMapper.toEntity(pmSlobDTOList));
        if (Optional.ofNullable(rscIotPmSlobSummaryDTO).isPresent()) {
            rscIotPmSlobSummaryService.deleteById(ppHeaderId);
            rscItPMSlobService.calculateRscItPMSlobSummary(ppHeaderId, sitValue);
           /* if (!rscIotPmSlobSummaryDTO.getSitValue().equals(sitValue)) {
                rscIotPmSlobSummaryService.deleteById(ppHeaderId);
                rscItPMSlobService.calculateRscItPMSlobSummary(ppHeaderId, sitValue);
            } else {
                rscIotPmSlobSummaryService.deleteById(ppHeaderId);
                rscItPMSlobService.calculateRscItPMSlobSummary(ppHeaderId, null);
            }*/
        }
        recalculatePmCoverDaysAndSummary(ppHeaderId);
        //  recalculationOfTablesBasedOnPmSlob(ppHeaderId);
        return pmSlobMapper.toDto(rscItPMSlobs);
    }

    private void saveRscIotPmLatestSlob(PmSlobDTO pmSlobDTO, List<RscIotPmLatestSlobReasonDTO> rscIotPmLatestSlobReasonDTOS) {
        List<RscIotPmLatestSlobReasonDTO> iotPmLatestSlobReasonDTO = rscIotPmLatestSlobReasonDTOS.stream().filter(rscIotPmLatestSlobReasonDTO -> rscIotPmLatestSlobReasonDTO.getRscMtItemId().equals(pmSlobDTO.getRscMtItemId())).collect(Collectors.toList());
        if (iotPmLatestSlobReasonDTO.size() == 0) {
            RscIotPmLatestSlobReasonDTO rscIotPmLatestSlobReasonDTO = new RscIotPmLatestSlobReasonDTO();
            rscIotPmLatestSlobReasonDTO.setRscMtItemId(pmSlobDTO.getRscMtItemId());
            rscIotPmLatestSlobReasonDTO.setRscDtReasonId(pmSlobDTO.getReasonId());
            rscIotPmLatestSlobReasonDTO.setRemark(pmSlobDTO.getRemark());
            rscIotPmLatestSlobReasonService.saveOne(rscIotPmLatestSlobReasonDTO);
        } else {
            iotPmLatestSlobReasonDTO.forEach(rscIotPmLatestSlobReasonDTO -> {
                RscIotPmLatestSlobReasonDTO pmLatestSlobReasonDTO = new RscIotPmLatestSlobReasonDTO();
                pmLatestSlobReasonDTO.setId(rscIotPmLatestSlobReasonDTO.getId());
                pmLatestSlobReasonDTO.setRscMtItemId(rscIotPmLatestSlobReasonDTO.getRscMtItemId());
                if (pmSlobDTO.getReasonId() != null) {
                    pmLatestSlobReasonDTO.setRscDtReasonId(pmSlobDTO.getReasonId());
                }
                if (pmSlobDTO.getRemark() != null) {
                    pmLatestSlobReasonDTO.setRemark(pmSlobDTO.getRemark());
                }
                rscIotPmLatestSlobReasonService.saveOne(pmLatestSlobReasonDTO);
            });
        }
    }

    private void recalculatePmCoverDaysAndSummary(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (rscMtPPheaderDTO.isPresent()) {
            try {
                rscIotPmCoverDaysService.deleteById(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("deleteById", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
            try {
                rscIotPmCoverDaysSummaryService.deleteAllById(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("deleteAllById", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
            try {
                rscIotPmCoverDaysService.calculateRscIotPmCoverDays(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("calculateRscIotRmCoverDays", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
            try {
                rscIotPmCoverDaysService.calculateRscIotPmCoverDaysSummary(ppHeaderId);
            } catch (Exception e) {
                executionErrorLogService.saveExecutionError("calculateRscIotRmCoverDaysSummary", e.getMessage(), rscMtPPheaderDTO.get().getId());
            }
        }
    }

    @Override
    public String createRmExcelExportOnUpdate(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            String year = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear());
            String month = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth());
            String date = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth());
            String name = optionalRscMtPpHeaderDTO.get().getMpsName();
            allExcelExportService.createDirectory(year, month, date, name);
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(optionalRscMtPpHeaderDTO.get().getMpsDate());
            saveRmCoverDaysExport(optionalRscMtPpHeaderDTO.get(), year, month, date, name, monthDetailDTO);
        }
        return "Downloaded Rm Cover Days and slob Exports Successfully";
    }

    private void saveRmCoverDaysExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, String name, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> rmExportList = new ArrayList<>();
        rmExportList.add(rscIotRMSlobService.rmSlobExporter(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRMSlobService.exportRmSlobSummary(rscMtPPheaderDTO));
        rmExportList.add(rscIotRmCoverDaysService.exportRmCoverDaysExporter(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRmCoverDaysClassSummaryService.rmCoverDaysSummaryExporter(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.forEach(excelExportFiles -> {
            try {
                FileOutputStream fileOut = new FileOutputStream(ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.RM_Directory + excelExportFiles.getFileName());
                fileOut.write(excelExportFiles.getFile().readAllBytes());
            } catch (Exception e) {
                throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
            }
        });
    }

    @Override
    public String createPmExcelExportOnUpdate(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            String year = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear());
            String month = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth());
            String date = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth());
            String name = optionalRscMtPpHeaderDTO.get().getMpsName();
            allExcelExportService.createDirectory(year, month, date, name);
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(optionalRscMtPpHeaderDTO.get().getMpsDate());
            savePmCoverDaysExport(optionalRscMtPpHeaderDTO.get(), year, month, date, name, monthDetailDTO);
        }
        return "Downloaded Pm Cover Days and slob Exports Successfully";
    }

    private void savePmCoverDaysExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, String name, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> pmExportList = new ArrayList<>();
        pmExportList.add(rscItPMSlobService.exportPmSlobExporter(rscMtPPheaderDTO));
        pmExportList.add(rscIotPmCoverDaysService.exportPmSlobExporter(rscMtPPheaderDTO, monthDetailDTO));
        pmExportList.add(rscItPMSlobService.exportPmSlobSummary(rscMtPPheaderDTO));
        pmExportList.add(rscItPMSlobService.pmCoverDaysSummaryExporter(rscMtPPheaderDTO, monthDetailDTO));
        pmExportList.forEach(excelExportFiles -> {
            try {
                FileOutputStream fileOut = new FileOutputStream(ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.PM_Directory + excelExportFiles.getFileName());
                fileOut.write(excelExportFiles.getFile().readAllBytes());
            } catch (IOException e) {
                throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
            }
        });
    }
}