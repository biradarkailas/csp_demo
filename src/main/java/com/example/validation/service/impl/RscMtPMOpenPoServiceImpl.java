package com.example.validation.service.impl;

import com.example.validation.dto.RscMtPMOpenPoDTO;
import com.example.validation.mapper.RscMtPMOpenPoMapper;
import com.example.validation.repository.RscMtPMOpenPoRepository;
import com.example.validation.service.RscMtPMOpenPoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscMtPMOpenPoServiceImpl implements RscMtPMOpenPoService {
    private final RscMtPMOpenPoRepository rscMtPMOpenPoRepository;
    private final RscMtPMOpenPoMapper rscMtPMOpenPoMapper;

    public RscMtPMOpenPoServiceImpl(RscMtPMOpenPoRepository rscMtPMOpenPoRepository, RscMtPMOpenPoMapper rscMtPMOpenPoMapper) {
        this.rscMtPMOpenPoRepository = rscMtPMOpenPoRepository;
        this.rscMtPMOpenPoMapper = rscMtPMOpenPoMapper;
    }

    @Override
    public List<RscMtPMOpenPoDTO> findAllByRscMtItemIdAndMtSupplierId(Long mpsId, Long rscMtItemId, Long rscMtSupplierId) {
        return rscMtPMOpenPoMapper.toDto(rscMtPMOpenPoRepository.findAllByRscMtPPheaderIdAndRscMtItemIdAndRscMtSupplierId(mpsId, rscMtItemId, rscMtSupplierId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscMtPMOpenPoRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}