package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.*;
import org.springframework.stereotype.Service;

import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class AllRMExcelExportServiceImpl implements AllRMExcelExportService {
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RmStockService rmStockService;
    private final RscIotRMSlobService rscIotRMSlobService;
    private final RscIotRMPurchasePlanService rscIotRMPurchasePlanService;
    private final RscIotRmCoverDaysService rscIotRmCoverDaysService;
    private final RscIotRMPurchasePlanCoverDaysService rscIotRMPurchasePlanCoverDaysService;
    private final RscItRMPivotConsumptionService rscItRMPivotConsumptionService;
    private final MaterialMastersService materialMastersService;
    private final SupplierMastersService supplierMastersService;
    private final RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService;
    private final MonthService monthService;

    public AllRMExcelExportServiceImpl(RscMtPpheaderService rscMtPpheaderService, RscItBulkGrossConsumptionService rscItBulkGrossConsumptionService, RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RmStockService rmStockService, RscIotRMSlobService rscIotRMSlobService, RscIotRMPurchasePlanService rscIotRMPurchasePlanService, RscIotRmCoverDaysService rscIotRmCoverDaysService, RscIotRMPurchasePlanCoverDaysService rscIotRMPurchasePlanCoverDaysService, RscItRMPivotConsumptionService rscItRMPivotConsumptionService, MaterialMastersService materialMastersService, SupplierMastersService supplierMastersService, RscIotRmCoverDaysClassSummaryService rscIotRmCoverDaysClassSummaryService, MonthService monthService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscItBulkGrossConsumptionService = rscItBulkGrossConsumptionService;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rmStockService = rmStockService;
        this.rscIotRMSlobService = rscIotRMSlobService;
        this.rscIotRMPurchasePlanService = rscIotRMPurchasePlanService;
        this.rscIotRmCoverDaysService = rscIotRmCoverDaysService;
        this.rscIotRMPurchasePlanCoverDaysService = rscIotRMPurchasePlanCoverDaysService;
        this.rscItRMPivotConsumptionService = rscItRMPivotConsumptionService;
        this.materialMastersService = materialMastersService;
        this.supplierMastersService = supplierMastersService;
        this.rscIotRmCoverDaysClassSummaryService = rscIotRmCoverDaysClassSummaryService;
        this.monthService = monthService;
    }


    @Override
    public String createAllRMExports(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPpHeaderDTO.isPresent()) {
            String year = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear());
            String month = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth());
            String date = String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth());
            String name = optionalRscMtPpHeaderDTO.get().getMpsName();
            createDirectory(year, month, date, name);
            MonthDetailDTO monthDetailDTO = monthService.getMonthDetails(optionalRscMtPpHeaderDTO.get().getMpsDate());
            saveRmExport(optionalRscMtPpHeaderDTO.get(), year, month, date, monthDetailDTO);
        }
        return "Downloaded All Exports Successfully";
    }

    private void saveRmExport(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, MonthDetailDTO monthDetailDTO) {
        List<ExcelExportFiles> rmExportList = new ArrayList<>();
        rmExportList.add(rscItBulkGrossConsumptionService.exportBulkGross(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscItRMGrossConsumptionService.getRmGrossConsumptionExport(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rmStockService.exportRtdStock(rscMtPPheaderDTO));
        rmExportList.add(rmStockService.exportBulkOpRmStock(rscMtPPheaderDTO));
        rmExportList.add(rmStockService.exportRmMesStockRejection(rscMtPPheaderDTO));
        rmExportList.add(rmStockService.exportRMStock(rscMtPPheaderDTO));
        rmExportList.add(rmStockService.exportTransferStock(rscMtPPheaderDTO));
        rmExportList.add(rmStockService.exportRejectionStock(rscMtPPheaderDTO));
        rmExportList.add(rscIotRMPurchasePlanService.getRmPurchasePlanExport(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRMSlobService.rmSlobExporter(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRmCoverDaysService.exportRmCoverDaysExporter(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRmCoverDaysClassSummaryService.rmCoverDaysSummaryExporter(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRMSlobService.exportRmSlobSummary(rscMtPPheaderDTO));
        rmExportList.add(rscIotRMPurchasePlanCoverDaysService.rmPurchaseCoverdaysExporte(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(rscIotRMPurchasePlanCoverDaysService.rmPurchaseStockEquationExporte(rscMtPPheaderDTO, monthDetailDTO));
        rmExportList.add(materialMastersService.rmMaterialMasterDataExport(rscMtPPheaderDTO));
        rmExportList.add(materialMastersService.rmBulkMaterialMasterDataMouldExport(rscMtPPheaderDTO));
        rmExportList.add(materialMastersService.rmSubBasesMaterialMasterDataMouldExport(rscMtPPheaderDTO));
        rmExportList.add(supplierMastersService.rmSuppliersMasterDataExport(rscMtPPheaderDTO));
        rmExportList.add(rscItRMPivotConsumptionService.getRmPivotConsumptionExport(rscMtPPheaderDTO, monthDetailDTO));
        createExcelFiles(rscMtPPheaderDTO, year, month, date, rmExportList, ExcelFileNameConstants.RM_Directory);
    }

    private void createExcelFiles(RscMtPPheaderDTO rscMtPPheaderDTO, String year, String month, String date, List<ExcelExportFiles> fgExportList, String moduleType) {
        for (ExcelExportFiles excelExportFiles : fgExportList) {
            try {
                FileOutputStream fileOut = new FileOutputStream(ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + rscMtPPheaderDTO.getMpsName() + "/" + moduleType + excelExportFiles.getFileName());
                fileOut.write(excelExportFiles.getFile().readAllBytes());
            } catch (IOException e) {
                throw new RuntimeException("Fail to import data to Excel file: " + e.getMessage());
            }
        }
    }

    @Override
    public void createDirectory(String year, String month, String date, String name) {
        try {
            Path rmPath = Paths.get(ExcelFileNameConstants.Save_All_Exports_Path + year + "/" + month + "/" + "Plans" + "/" + date + "/" + name + "/" + ExcelFileNameConstants.RM);
            Files.createDirectories(rmPath);
        } catch (IOException e) {
            System.err.println("Failed to create directory!" + e.getMessage());
        }
    }
}