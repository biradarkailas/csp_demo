package com.example.validation.service.impl;

import com.example.validation.dto.ValidationTableLoadingDetailsDTO;
import com.example.validation.mapper.ValidationTableLoadingDetailsMapper;
import com.example.validation.repository.ValidationTableLoadingDetailsRepository;
import com.example.validation.service.ValidationTableLoadingDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ValidationTableLoadingDetailsServiceImpl implements ValidationTableLoadingDetailsService {
    private final ValidationTableLoadingDetailsRepository validationTableLoadingDetailsRepository;
    private final ValidationTableLoadingDetailsMapper validationTableLoadingDetailsMapper;

    public ValidationTableLoadingDetailsServiceImpl(ValidationTableLoadingDetailsRepository validationTableLoadingDetailsRepository, ValidationTableLoadingDetailsMapper validationTableLoadingDetailsMapper) {
        this.validationTableLoadingDetailsRepository = validationTableLoadingDetailsRepository;
        this.validationTableLoadingDetailsMapper = validationTableLoadingDetailsMapper;
    }

    @Override
    public List<ValidationTableLoadingDetailsDTO> findAll() {
        return validationTableLoadingDetailsMapper.toDto(validationTableLoadingDetailsRepository.findAll());
    }

    @Override
    public void deleteAll() {
        validationTableLoadingDetailsRepository.deleteAll();
    }
}