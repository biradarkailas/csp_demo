package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.dto.supply.PurchasePlanMainDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;
import com.example.validation.mapper.*;
import com.example.validation.repository.RscIotPMLatestPurchasePlanRepository;
import com.example.validation.repository.RscIotPMOriginalPurchasePlanRepository;
import com.example.validation.service.RscIotPMPurchasePlanService;
import com.example.validation.service.RscIotPmPurchasePlanCalculationsService;
import com.example.validation.service.RscMtPMOpenPoService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.excel_export.PmPurchasePlanExporter;
import com.example.validation.util.supply.PMPurchasePlanUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotPMPurchasePlanServiceImpl implements RscIotPMPurchasePlanService {
    private final RscIotPMOriginalPurchasePlanMapper rscIotPMOriginalPurchasePlanMapper;
    private final RscIotPMLatestPurchasePlanMapper rscIotPMLatestPurchasePlanMapper;
    private final RscIotPMOriginalPurchasePlanRepository rscIotPMOriginalPurchasePlanRepository;
    private final RscIotPMLatestPurchasePlanRepository rscIotPMLatestPurchasePlanRepository;
    private final RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final PMOriginalPurchasePlanMapper pmOriginalPurchasePlanMapper;
    private final PMLatestPurchasePlanMapper pmLatestPurchasePlanMapper;
    private final PMPurchasePlanUtil pmPurchasePlanUtil;
    private final RscIotOriginalMonthValuesMapper rscIotOriginalMonthValuesMapper;
    private final RscMtPMOpenPoService rscMtPMOpenPoService;

    public RscIotPMPurchasePlanServiceImpl(RscIotPMOriginalPurchasePlanMapper rscIotPMOriginalPurchasePlanMapper, RscIotPMLatestPurchasePlanMapper rscIotPMLatestPurchasePlanMapper, RscIotPMOriginalPurchasePlanRepository rscIotPMOriginalPurchasePlanRepository, RscIotPMLatestPurchasePlanRepository rscIotPMLatestPurchasePlanRepository, RscIotPmPurchasePlanCalculationsService rscIotPmPurchasePlanCalculationsService, RscMtPpheaderService rscMtPpheaderService, PMOriginalPurchasePlanMapper pmOriginalPurchasePlanMapper, PMLatestPurchasePlanMapper pmLatestPurchasePlanMapper, PMPurchasePlanUtil pmPurchasePlanUtil, RscIotOriginalMonthValuesMapper rscIotOriginalMonthValuesMapper, RscMtPMOpenPoService rscMtPMOpenPoService) {
        this.rscIotPMOriginalPurchasePlanMapper = rscIotPMOriginalPurchasePlanMapper;
        this.rscIotPMLatestPurchasePlanMapper = rscIotPMLatestPurchasePlanMapper;
        this.rscIotPMOriginalPurchasePlanRepository = rscIotPMOriginalPurchasePlanRepository;
        this.rscIotPMLatestPurchasePlanRepository = rscIotPMLatestPurchasePlanRepository;
        this.rscIotPmPurchasePlanCalculationsService = rscIotPmPurchasePlanCalculationsService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.pmOriginalPurchasePlanMapper = pmOriginalPurchasePlanMapper;
        this.pmLatestPurchasePlanMapper = pmLatestPurchasePlanMapper;
        this.pmPurchasePlanUtil = pmPurchasePlanUtil;
        this.rscIotOriginalMonthValuesMapper = rscIotOriginalMonthValuesMapper;
        this.rscMtPMOpenPoService = rscMtPMOpenPoService;
    }

    @Override
    public Boolean createPMPurchasePlan(Long ppHeaderId) {
        if (rscIotPMOriginalPurchasePlanRepository.countByRscMtPPheaderId(ppHeaderId) == 0 && rscIotPMLatestPurchasePlanRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotPMPurchasePlanDTO> updatedPMPurchasePlanCalculationsDTOs = getUpdatedPMPurchasePlanCalculationsDTOs(ppHeaderId, rscIotPmPurchasePlanCalculationsService.getPMSupplyCalculationsDTOByPPheaderId(ppHeaderId));
            rscIotPMOriginalPurchasePlanRepository.saveAll(rscIotPMOriginalPurchasePlanMapper.toEntity(updatedPMPurchasePlanCalculationsDTOs));
            rscIotPMLatestPurchasePlanRepository.saveAll(rscIotPMLatestPurchasePlanMapper.toEntity(updatedPMPurchasePlanCalculationsDTOs));
        }
        return true;
    }

    @Override
    public PurchasePlanMainDTO getPMOriginalPurchasePlan(Long ppHeaderId) {
        PurchasePlanMainDTO purchasePlanMainDTO = new PurchasePlanMainDTO();
        Optional<RscMtPPheaderDTO> optionalRscMtPPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPPHeaderDTO.isPresent()) {
            List<PurchasePlanDTO> purchasePlanDTOS = pmOriginalPurchasePlanMapper.toDto(rscIotPMOriginalPurchasePlanRepository.findAllByRscMtPPheaderId(ppHeaderId));
            return pmPurchasePlanUtil.getSupplyMainDTO(optionalRscMtPPHeaderDTO.get(), purchasePlanDTOS);
        }
        return purchasePlanMainDTO;
    }

    @Override
    public PurchasePlanMainDTO getPMLatestPurchasePlan(Long ppHeaderId) {
        PurchasePlanMainDTO purchasePlanMainDTO = new PurchasePlanMainDTO();
        Optional<RscMtPPheaderDTO> optionalRscMtPPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPPHeaderDTO.isPresent()) {
            List<PurchasePlanDTO> purchasePlanDTOS = pmLatestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlanRepository.findAllByRscMtPPheaderId(ppHeaderId));
            for (PurchasePlanDTO purchasePlanDTO : purchasePlanDTOS) {
                var rscIotOriginalMonthValues = getOriginalMonthValues(ppHeaderId, purchasePlanDTO.getRscMtItemId());
                purchasePlanDTO.setOriginalMonthValues(rscIotOriginalMonthValues);
            }
            return pmPurchasePlanUtil.getSupplyMainDTO(optionalRscMtPPHeaderDTO.get(), purchasePlanDTOS);
        }
        return purchasePlanMainDTO;
    }

    public RscIotOriginalMonthValueslDTO getOriginalMonthValues(Long ppHeaderId, Long rscMtItemId) {
        return rscIotOriginalMonthValuesMapper.toDto(rscIotPMOriginalPurchasePlanRepository.findAllByRscMtPPheaderIdAndRscMtItemId(ppHeaderId, rscMtItemId));
    }
/*
    @Override
    public List<PurchasePlanDTO> saveAll(List<PurchasePlanDTO> purchasePlanDTOs) {
        List<RscIotPMLatestPurchasePlan> rscIotPMLatestPurchasePlans = rscIotPMLatestPurchasePlanRepository.saveAll(pmLatestPurchasePlanMapper.toEntity(purchasePlanDTOs));
        recalculateMouldAndTotalMouldSaturation(purchasePlanDTOs, purchasePlanDTOs.get(0).getMpsId());
//        purchasePlanToExcel(purchasePlanDTOs.get(0).getMpsId());
        recalculateExcelBasedOnPurchasePlan(purchasePlanDTOs.get(0).getMpsId());
        return pmLatestPurchasePlanMapper.toDto(rscIotPMLatestPurchasePlans);
    }*/

    @Override
    public ExcelExportFiles purchasePlanToExcel(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmPurchasePlanExporter.purchasePlanToExcel(getPMLatestPurchasePlan(rscMtPPheaderDTO.getId()), monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_PURCHASE_PLAN + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotPMOriginalPurchasePlanRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

    private List<RscIotPMPurchasePlanDTO> getUpdatedPMPurchasePlanCalculationsDTOs(Long mpsId, List<RscIotPMPurchasePlanDTO> pmPurchasePlanCalculationsDTOs) {
        List<RscIotPMPurchasePlanDTO> updatedPMPurchasePlanCalculationsDTOs = new ArrayList<>();
        if (Optional.ofNullable(pmPurchasePlanCalculationsDTOs).isPresent()) {
            for (RscIotPMPurchasePlanDTO pmPurchasePlanCalculationsDTO : pmPurchasePlanCalculationsDTOs) {
                List<RscMtPMOpenPoDTO> rscMtPMOpenPoDTOs = rscMtPMOpenPoService.findAllByRscMtItemIdAndMtSupplierId(mpsId, pmPurchasePlanCalculationsDTO.getRscMtItemId(), pmPurchasePlanCalculationsDTO.getRscMtSupplierId());
                if (Optional.ofNullable(rscMtPMOpenPoDTOs).isPresent())
                    pmPurchasePlanCalculationsDTO.setPoRemark(pmPurchasePlanUtil.getPORemarkValue(rscMtPMOpenPoDTOs));
                updatedPMPurchasePlanCalculationsDTOs.add(pmPurchasePlanCalculationsDTO);
            }
        }
        return updatedPMPurchasePlanCalculationsDTOs;
    }
}