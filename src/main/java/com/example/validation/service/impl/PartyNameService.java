package com.example.validation.service.impl;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyName;
import com.example.validation.repository.PartyNameRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartyNameService {
    @Autowired
    PartyNameRepo partyNameRepo;

    public PartyName save(PartyName partyName) {
        return partyNameRepo.save(partyName);
    }

    public PartyName findByParty(Party party) {
        return partyNameRepo.findByParty(party);
    }

    public PartyName findPartyNameByOrgnizationName(String name) {
        return partyNameRepo.findByOrganizationName(name);
    }
}
