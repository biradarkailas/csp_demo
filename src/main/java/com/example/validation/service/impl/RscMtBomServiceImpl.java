package com.example.validation.service.impl;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.NodeDTO;
import com.example.validation.dto.RscMtBomDTO;
import com.example.validation.dto.RscMtItemDTO;
import com.example.validation.mapper.RscMtBomMapper;
import com.example.validation.repository.RscMtBomRepository;
import com.example.validation.service.RscMtBomService;
import com.example.validation.service.RscMtItemService;
import com.example.validation.util.consumption.ConsumptionUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscMtBomServiceImpl implements RscMtBomService {
    private final RscMtBomRepository rscMtBomRepository;
    private final ConsumptionUtil consumptionUtil;
    private final RscMtItemService rscMtItemService;
    private final RscMtBomMapper rscMtBomMapper;

    RscMtBomServiceImpl(RscMtBomRepository rscMtBomRepository, RscMtBomMapper rscMtBomMapper, ConsumptionUtil consumptionUtil, RscMtItemService rscMtItemService) {
        this.rscMtBomRepository = rscMtBomRepository;
        this.rscMtBomMapper = rscMtBomMapper;
        this.consumptionUtil = consumptionUtil;
        this.rscMtItemService = rscMtItemService;
    }

    @Override
    public List<RscMtBomDTO> findAllByRscMtItemParentId(Long parentItemId, Long ppHeaderId) {
        return rscMtBomMapper.toDto(rscMtBomRepository.findAllByRscMtItemParentIdAndRscMtPPheaderId(parentItemId, ppHeaderId));
    }

    @Override
    public List<RscMtBomDTO> findAllByParentItemIdAndChildItemCategoryDescription(Long parentItemId, String itemDescription, Long ppHeaderId) {
        return rscMtBomMapper.toDto(rscMtBomRepository.findAllByRscMtItemParentIdAndRscMtItemChildRscDtItemCategoryDescriptionAndRscMtPPheaderId(parentItemId, itemDescription, ppHeaderId));
    }

    @Override
    public NodeDTO getFGRelation(Long itemId, String itemType, Long ppHeaderId) {
        NodeDTO nodeDTO = new NodeDTO();
        RscMtItemDTO rscMtItemDTO = rscMtItemService.findById(itemId);
        nodeDTO.setName(rscMtItemDTO.getItemCode());
        nodeDTO.setChilds(getNodeDTOs(itemId, itemType, ppHeaderId));
        return nodeDTO;
    }

    @Override
    public List<Long> findFGItemIdByRscMtItemChildId(Long childItemId, Long ppHeaderId) {
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomMapper.toDto(rscMtBomRepository.findAllByRscMtItemChildIdAndRscMtPPheaderId(childItemId, ppHeaderId));
        if (Optional.ofNullable(rscMtBomDTOs).isPresent()) {
            return rscMtBomDTOs.stream().map(RscMtBomDTO::getRscMtItemParentId).collect(Collectors.toList());
        }
        return null;
    }

    private List<NodeDTO> getNodeDTOs(Long itemId, String itemType, Long ppHeaderId) {
        List<NodeDTO> nodeDTOs = new ArrayList<>();
        List<RscMtBomDTO> rscMtBomDTOs = rscMtBomMapper.toDto(rscMtBomRepository.findAllByRscMtItemParentIdAndRscMtPPheaderId(itemId, ppHeaderId));
        rscMtBomDTOs.forEach(rscMtBomDTO -> {
            NodeDTO childNodeDTO = getChildNodeDTOs(rscMtBomDTO, itemType, ppHeaderId);
            if (Optional.ofNullable(childNodeDTO.getName()).isPresent()) {
                nodeDTOs.add(childNodeDTO);
            }
        });
        return nodeDTOs;
    }

    private NodeDTO getChildNodeDTOs(RscMtBomDTO rscMtBomDTO, String itemType, Long ppHeaderId) {
        NodeDTO childNodeDTO = new NodeDTO();
        switch (itemType) {
            case MaterialCategoryConstants.PM:
                if (consumptionUtil.isPackingMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    childNodeDTO.setName(rscMtBomDTO.getChildItemCode());
                } else if (consumptionUtil.isWIPMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    childNodeDTO.setName(rscMtBomDTO.getChildItemCode());
                    childNodeDTO.setChilds(getNodeDTOs(rscMtBomDTO.getRscMtItemChildId(), itemType, ppHeaderId));
                }
                break;
            case MaterialCategoryConstants.BULK:
                if (consumptionUtil.isBulkMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    childNodeDTO.setName(rscMtBomDTO.getChildItemCode());
                    childNodeDTO.setChilds(getNodeDTOs(rscMtBomDTO.getRscMtItemChildId(), itemType, ppHeaderId));
                } else if (consumptionUtil.isWIPMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    childNodeDTO.setName(rscMtBomDTO.getChildItemCode());
                    childNodeDTO.setChilds(getNodeDTOs(rscMtBomDTO.getRscMtItemChildId(), itemType, ppHeaderId));
                }
                break;
            case MaterialCategoryConstants.RM:
                if (consumptionUtil.isRawMaterialItem(rscMtBomDTO.getRscMtItemChildId())) {
                    childNodeDTO.setName(rscMtBomDTO.getChildItemCode());
                } else if (consumptionUtil.isSubBasesItem(rscMtBomDTO.getRscMtItemChildId())) {
                    childNodeDTO.setName(rscMtBomDTO.getChildItemCode());
                    childNodeDTO.setChilds(getNodeDTOs(rscMtBomDTO.getRscMtItemChildId(), itemType, ppHeaderId));
                }
                break;
        }
        childNodeDTO.setLinkQuantity(rscMtBomDTO.getLinkQuantity());
        childNodeDTO.setWastePercentage(rscMtBomDTO.getWastePercent());
        return childNodeDTO;
    }

    public List<RscMtBomDTO> findAll(Long ppHeaderId) {
        return rscMtBomMapper.toDto(rscMtBomRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscMtBomDTO> findAllByRscMtItemChildId(Long childItemId, Long ppHeaderId) {
        return rscMtBomMapper.toDto(rscMtBomRepository.findAllByRscMtItemChildIdAndRscMtPPheaderId(childItemId, ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscMtBomRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
