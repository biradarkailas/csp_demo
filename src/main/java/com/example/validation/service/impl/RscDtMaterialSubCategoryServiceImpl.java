package com.example.validation.service.impl;

import com.example.validation.dto.RscDtMaterialSubCategoryDTO;
import com.example.validation.mapper.RscDtMaterialSubCategoryMapper;
import com.example.validation.repository.RscDtMaterialSubCategoryRepository;
import com.example.validation.service.RscDtMaterialSubCategoryService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtMaterialSubCategoryServiceImpl implements RscDtMaterialSubCategoryService {
    private final RscDtMaterialSubCategoryRepository rscDtMaterialSubCategoryRepository;
    private final RscDtMaterialSubCategoryMapper rscDtMaterialSubCategoryMapper;

    public RscDtMaterialSubCategoryServiceImpl(RscDtMaterialSubCategoryRepository rscDtMaterialSubCategoryRepository, RscDtMaterialSubCategoryMapper rscDtMaterialSubCategoryMapper) {
        this.rscDtMaterialSubCategoryRepository = rscDtMaterialSubCategoryRepository;
        this.rscDtMaterialSubCategoryMapper = rscDtMaterialSubCategoryMapper;
    }

    @Override
    public List<RscDtMaterialSubCategoryDTO> findAll() {
        return rscDtMaterialSubCategoryMapper.toDto(rscDtMaterialSubCategoryRepository.findAll());
    }

}
