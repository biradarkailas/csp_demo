package com.example.validation.service.impl;

import com.example.validation.dto.IndividualValuesMesDTO;
import com.example.validation.dto.RmMesStockPaginationDto;
import com.example.validation.dto.RscMtRmMesDTO;
import com.example.validation.dto.RscMtRmMesExportDTO;
import com.example.validation.mapper.IndividualValuesMesMapper;
import com.example.validation.mapper.RmMesStockDtoMapper;
import com.example.validation.mapper.RscMtRmMesExportMapper;
import com.example.validation.mapper.RscMtRmMesMapper;
import com.example.validation.repository.RscMtRmMesRepository;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.service.RscMtRmMesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.List;

@Service
@Transactional
public class RscMtRmMesServiceImpl implements RscMtRmMesService {
    private final RscMtRmMesRepository rscMtRmMesRepository;
    private final RscMtRmMesMapper rscMtRmMesMapper;
    private final IndividualValuesMesMapper individualValuesMesMapper;
    private final RscMtRmMesExportMapper rscMtRmMesExportMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RmMesStockDtoMapper rmMesStockDtoMapper;


    public RscMtRmMesServiceImpl(RscMtRmMesRepository rscMtRmMesRepository, RscMtRmMesMapper rscMtRmMesMapper, IndividualValuesMesMapper individualValuesMesMapper, RscMtRmMesExportMapper rscMtRmMesExportMapper, RscMtPpheaderService rscMtPpheaderService, RmMesStockDtoMapper rmMesStockDtoMapper) {
        this.rscMtRmMesRepository = rscMtRmMesRepository;
        this.rscMtRmMesMapper = rscMtRmMesMapper;
        this.individualValuesMesMapper = individualValuesMesMapper;
        this.rscMtRmMesExportMapper = rscMtRmMesExportMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rmMesStockDtoMapper = rmMesStockDtoMapper;
    }

    @Override
    public List<RscMtRmMesDTO> findAllByRscMtItemId(Long rscMtItemId) {
        return rscMtRmMesMapper.toDto(rscMtRmMesRepository.findAllByRscMtItemId(rscMtItemId));
    }

    @Override
    public List<IndividualValuesMesDTO> findAllIndividualValueDTORscMtItemId(Long rscMtItemId) {
        return individualValuesMesMapper.toDto(rscMtRmMesRepository.findAllByRscMtItemId(rscMtItemId));
    }

    public List<RscMtRmMesExportDTO> findAll() {
        return rscMtRmMesExportMapper.toDto(rscMtRmMesRepository.findAll());
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtRmMesRepository.deleteAllByStockDate(stockDate);
    }

    //  Mes New Api with optimization
    @Override
    public List<RmMesStockPaginationDto> findAllByStockDate(LocalDate stockDate) {
        return rmMesStockDtoMapper.toDto(rscMtRmMesRepository.findAllByStockDateBetween(stockDate.with(TemporalAdjusters.firstDayOfMonth()), stockDate.with(TemporalAdjusters.lastDayOfMonth())));

    }

}
