package com.example.validation.service.impl;

import com.example.validation.dto.RscIitRMItemWiseSupplierDetailsDTO;
import com.example.validation.mapper.RscIitRMItemWiseSupplierDetailsMapper;
import com.example.validation.repository.RscIitRMItemWiseSupplierDetailsRepository;
import com.example.validation.service.RscIitRMItemWiseSupplierDetailsService;
import org.springframework.stereotype.Service;

@Service
public class RscIitRMItemWiseSupplierDetailsServiceImpl implements RscIitRMItemWiseSupplierDetailsService {
    private final RscIitRMItemWiseSupplierDetailsRepository rscIitRMItemWiseSupplierDetailsRepository;
    private final RscIitRMItemWiseSupplierDetailsMapper rscIitRMItemWiseSupplierDetailsMapper;

    public RscIitRMItemWiseSupplierDetailsServiceImpl(RscIitRMItemWiseSupplierDetailsRepository rscIitRMItemWiseSupplierDetailsRepository, RscIitRMItemWiseSupplierDetailsMapper rscIitRMItemWiseSupplierDetailsMapper) {
        this.rscIitRMItemWiseSupplierDetailsRepository = rscIitRMItemWiseSupplierDetailsRepository;
        this.rscIitRMItemWiseSupplierDetailsMapper = rscIitRMItemWiseSupplierDetailsMapper;
    }

    public RscIitRMItemWiseSupplierDetailsDTO findByItemWiseSupplierId(Long itemWiseSupplierId) {
        return rscIitRMItemWiseSupplierDetailsMapper.toDto(rscIitRMItemWiseSupplierDetailsRepository.findOneByRscItItemWiseSupplierId(itemWiseSupplierId));
    }
}