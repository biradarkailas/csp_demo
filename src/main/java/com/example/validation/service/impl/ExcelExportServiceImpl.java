package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.ExcelExportService;
import com.example.validation.service.RscMtPpheaderService;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.List;
import java.util.Optional;

@Service
public class ExcelExportServiceImpl implements ExcelExportService {
    private final RscMtPpheaderService rscMtPpheaderService;

    public ExcelExportServiceImpl(RscMtPpheaderService rscMtPpheaderService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
    }

    @Override
    public ExcelExportFiles downloadfgExport(Long ppHeaderId, String fileName) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Plans" + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth()) + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + "FG/" + fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate() + ExcelFileNameConstants.Extension));
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            workbook.write(out);
            excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
            excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }

    @Override
    public ExcelExportFiles downloadFgAnnualExport( String fileName) {
        List<RscMtPPheaderDTO> rscMtPPheaderDTOList = rscMtPpheaderService.findAllPPheaderByActive();
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(rscMtPPheaderDTOList.get(rscMtPPheaderDTOList.size() - 1).getId());
        try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Analysis" + "/" + "Annual" + "/" + "FG/" + fileName + " " + ExcelFileNameConstants.Extension));
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            workbook.write(out);
            excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
            excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }

    @Override
    public ExcelExportFiles downloadPmExport(Long ppHeaderId, String fileName) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Plans" + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth()) + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + "PM/" + fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate() + ExcelFileNameConstants.Extension));
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            workbook.write(out);
            excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
            excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }

    @Override
    public ExcelExportFiles downloadPmAnnualExport( String fileName) {
        List<RscMtPPheaderDTO> rscMtPPheaderDTOList = rscMtPpheaderService.findAllPPheaderByActive();
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(rscMtPPheaderDTOList.get(rscMtPPheaderDTOList.size() - 1).getId());
        try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Analysis" + "/" + "Annual" + "/" + "PM/" + fileName + " " + ExcelFileNameConstants.Extension));
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            workbook.write(out);
            excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
            excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }

    @Override
    public ExcelExportFiles downloadPmOtifExport(Long ppHeaderId, String fileName) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
          try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            if (Optional.ofNullable(optionalRscMtPpHeaderDTO).isPresent()) {
                FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Analysis" + "/" + "Month" + "/" + "PM" + "/" + "Otif" + "/" + fileName));
                ZipSecureFile.setMinInflateRatio(0);
                XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
                ByteArrayOutputStream out = new ByteArrayOutputStream();
                workbook.write(out);
                excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
                excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            }
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }

    @Override
    public ExcelExportFiles downloadRmExport(Long ppHeaderId, String fileName) {
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Plans" + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getDayOfMonth()) + "/" + optionalRscMtPpHeaderDTO.get().getMpsName() + "/" + "RM/" + fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate() + ExcelFileNameConstants.Extension));
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            workbook.write(out);
            excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
            excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }

    @Override
    public ExcelExportFiles downloadRmAnnualExport(String fileName) {
        List<RscMtPPheaderDTO> rscMtPPheaderDTOList = rscMtPpheaderService.findAllPPheaderByActive();
        Optional<RscMtPPheaderDTO> optionalRscMtPpHeaderDTO = rscMtPpheaderService.findOne(rscMtPPheaderDTOList.get(rscMtPPheaderDTOList.size() - 1).getId());
        try {
            ExcelExportFiles excelExportFiles = new ExcelExportFiles();
            FileInputStream inputFile = new FileInputStream(new File(ExcelFileNameConstants.Save_All_Exports_Path + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getYear()) + "/" + String.valueOf(optionalRscMtPpHeaderDTO.get().getMpsDate().getMonth()) + "/" + "Analysis" + "/" + "Annual" + "/" + "RM/" + fileName + " " + ExcelFileNameConstants.Extension));
            ZipSecureFile.setMinInflateRatio(0);
            XSSFWorkbook workbook = new XSSFWorkbook(inputFile);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            workbook.write(out);
            excelExportFiles.setFile(new ByteArrayInputStream(out.toByteArray()));
            excelExportFiles.setFileName(fileName + " " + optionalRscMtPpHeaderDTO.get().getMpsDate().toString() + ExcelFileNameConstants.Extension);
            return excelExportFiles;
        } catch (Exception e) {
            throw new RuntimeException("fail to download Excel file: " + e.getMessage());
        }
    }
}
