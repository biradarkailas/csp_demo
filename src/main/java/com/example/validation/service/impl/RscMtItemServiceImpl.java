package com.example.validation.service.impl;

import com.example.validation.dto.MaterialMasterDTO;
import com.example.validation.dto.RscDtItemTypeDTO;
import com.example.validation.dto.RscMtItemDTO;
import com.example.validation.mapper.MaterialCategoryMastersMapper;
import com.example.validation.mapper.MaterialMastersMapper;
import com.example.validation.mapper.RscMtItemMapper;
import com.example.validation.repository.RscMtItemRepository;
import com.example.validation.service.RscMtItemService;
import com.example.validation.util.ListUtils;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class RscMtItemServiceImpl implements RscMtItemService {
    private final RscMtItemRepository rscMtItemRepository;
    private final RscMtItemMapper rscMtItemMapper;
    private final MaterialMastersMapper materialMastersMapper;
    private final MaterialCategoryMastersMapper materialCategoryMastersMapper;

    public RscMtItemServiceImpl(RscMtItemRepository rscMtItemRepository, RscMtItemMapper rscMtItemMapper, MaterialMastersMapper materialMastersMapper, MaterialCategoryMastersMapper materialCategoryMastersMapper) {
        this.rscMtItemRepository = rscMtItemRepository;
        this.rscMtItemMapper = rscMtItemMapper;
        this.materialMastersMapper = materialMastersMapper;
        this.materialCategoryMastersMapper = materialCategoryMastersMapper;
    }

    @Override
    public RscMtItemDTO findById(Long id) {
        return rscMtItemMapper.toDto(rscMtItemRepository.findById(id).get());
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListByCategory(String materialCategory) {
        if (materialCategory == "Finished Good") {
            return materialMastersMapper.toDto(rscMtItemRepository.
                    findAllByRscDtItemCategoryDescriptionAndRscDtItemCodeCodeIsStartingWithOrRscDtItemCodeCodeIsStartingWith(materialCategory, "F", "G"));
        } else {
            return materialMastersMapper.toDto(rscMtItemRepository.
                    findAllByRscDtItemCategoryDescription(materialCategory));
        }
    }

    @Override
    public List<RscDtItemTypeDTO> getMaterialCategoryMastersListByCategory(String materialCategory) {
        return materialCategoryMastersMapper.toDto(rscMtItemRepository.findAllByRscDtItemCategoryDescriptionAndRscDtItemTypeNotNull(materialCategory).stream().
                filter(ListUtils.distinctByKey(rscMtItem -> rscMtItem.getRscDtItemType().getId())).collect(Collectors.toList()));
    }
}