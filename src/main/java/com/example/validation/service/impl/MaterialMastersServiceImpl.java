package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MaterialMasterDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.MaterialMastersMapper;
import com.example.validation.repository.RscMtItemRepository;
import com.example.validation.service.MaterialMastersService;
import com.example.validation.service.RscMtItemService;
import com.example.validation.util.excel_export.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class MaterialMastersServiceImpl implements MaterialMastersService {
    private final RscMtItemService rscMtItemService;
    private final RscMtItemRepository rscMtItemRepository;
    private final MaterialMastersMapper materialMastersMapper;

    public MaterialMastersServiceImpl(RscMtItemService rscMtItemService, RscMtItemRepository rscMtItemRepository, MaterialMastersMapper materialMastersMapper) {
        this.rscMtItemService = rscMtItemService;
        this.rscMtItemRepository = rscMtItemRepository;
        this.materialMastersMapper = materialMastersMapper;
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListOfPackingMaterial() {
        return rscMtItemService.getMaterialMastersListByCategory( MaterialCategoryConstants.PACKING_MATERIAL );
    }

    @Override
    public List<MaterialMasterDTO> findMaterialMastersListOfPackingMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<MaterialMasterDTO> materialCategoryListResult = rscMtItemRepository.findAllByRscDtItemCategoryDescription( MaterialCategoryConstants.PACKING_MATERIAL, paging )
                .map( materialMastersMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles pmMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<MaterialMasterDTO> materialMasterDTOS = getMaterialMastersListOfPackingMaterial();
        excelExportFiles.setFile( PmMaterialMasterDataExporter.pmMaterialMasterDataToExcel( materialMasterDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.PM_Material_Masters + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListOfRawMaterial() {
        return rscMtItemService.getMaterialMastersListByCategory( MaterialCategoryConstants.RAW_MATERIAL );
    }

    @Override
    public List<MaterialMasterDTO> findMaterialMastersListOfRawMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<MaterialMasterDTO> materialCategoryListResult = rscMtItemRepository.findAllByRscDtItemCategoryDescription( MaterialCategoryConstants.RAW_MATERIAL, paging )
                .map( materialMastersMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles rmMaterialMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<MaterialMasterDTO> materialMasterDTOS = getMaterialMastersListOfRawMaterial();
        excelExportFiles.setFile( RmMaterialMasterDataExporter.rmMaterialMasterDataToExcel( materialMasterDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.RM_Material_Masters + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListOfFgMaterial() {
        return rscMtItemService.getMaterialMastersListByCategory( MaterialCategoryConstants.FG_MATERIAL );
    }

    @Override
    public List<MaterialMasterDTO> findMaterialMastersListOfFgMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<MaterialMasterDTO> materialCategoryListResult = rscMtItemRepository.findAllByRscDtItemCategoryDescriptionAndRscDtItemCodeCodeIsStartingWithOrRscDtItemCodeCodeIsStartingWith( MaterialCategoryConstants.FG_MATERIAL, "F", "G", paging )
                .map( materialMastersMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles fgMaterialMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<MaterialMasterDTO> materialMasterDTOS = getMaterialMastersListOfFgMaterial();
        excelExportFiles.setFile( FgMaterialMasterDataExporter.fgMaterialMasterDataToExcel( materialMasterDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.FG_Material_Masters + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListOfBulkMaterial() {
        return rscMtItemService.getMaterialMastersListByCategory( MaterialCategoryConstants.BULK_MATERIAL );
    }

    @Override
    public List<MaterialMasterDTO> findMaterialMastersListOfBulkMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<MaterialMasterDTO> materialCategoryListResult = rscMtItemRepository.findAllByRscDtItemCategoryDescription( MaterialCategoryConstants.BULK_MATERIAL, paging )
                .map( materialMastersMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles rmBulkMaterialMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<MaterialMasterDTO> materialMasterDTOS = getMaterialMastersListOfBulkMaterial();
        excelExportFiles.setFile( RmBulkMaterialMasterDataExporter.rmBulkMaterialMasterDataToExcel( materialMasterDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.RM_Bulk_Material_Masters + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListOfSubBasesMaterial() {
        return rscMtItemService.getMaterialMastersListByCategory( MaterialCategoryConstants.SUB_BASES );
    }

    @Override
    public List<MaterialMasterDTO> findMaterialMastersListOfSubBasesMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<MaterialMasterDTO> materialCategoryListResult = rscMtItemRepository.findAllByRscDtItemCategoryDescription( MaterialCategoryConstants.SUB_BASES, paging )
                .map( materialMastersMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles rmSubBasesMaterialMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<MaterialMasterDTO> materialMasterDTOS = getMaterialMastersListOfSubBasesMaterial();
        excelExportFiles.setFile( RmSubBasesMaterialMasterDataExporter.rmSubBasesMaterialMasterDataToExcel( materialMasterDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.RM_Base_SubBase_Material_Masters + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<MaterialMasterDTO> getMaterialMastersListOfWipMaterial() {
        return rscMtItemService.getMaterialMastersListByCategory( MaterialCategoryConstants.WIP_MATERIAL );
    }

    @Override
    public List<MaterialMasterDTO> findMaterialMastersListOfWipMaterialWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<MaterialMasterDTO> materialCategoryListResult = rscMtItemRepository.findAllByRscDtItemCategoryDescription( MaterialCategoryConstants.WIP_MATERIAL, paging )
                .map( materialMastersMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public ExcelExportFiles fgWipMaterialMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<MaterialMasterDTO> materialMasterDTOS = getMaterialMastersListOfWipMaterial();
        excelExportFiles.setFile( FgWipMaterialMasterDataExporter.fgWipMaterialMasterDataToExcel( materialMasterDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.FG_SF_WIP_Materials_Masters + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

}