package com.example.validation.service.impl;

import com.example.validation.dto.IndividualValuesBulkOpRmDTO;
import com.example.validation.dto.RscIitBulkRMStockDTO;
import com.example.validation.dto.RscIitRmBulkOpRmDTO;
import com.example.validation.mapper.IndivisualValuesBulkOpRmMapper;
import com.example.validation.mapper.RscIitBulkRmStockMapper;
import com.example.validation.mapper.RscIitRmBulkOpRmMapper;
import com.example.validation.repository.RscIitRmBulkOpRmRepository;
import com.example.validation.service.RscIitRmBulkOpRmService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscIitRmBulkOpRmServiceImpl implements RscIitRmBulkOpRmService {
    private final RscIitRmBulkOpRmRepository rscIitRmBulkOpRmRepository;
    private final RscIitRmBulkOpRmMapper rscIitRmBulkOpRmMapper;
    private final IndivisualValuesBulkOpRmMapper indivisualValuesBulkOpRmMapper;
    private final RscIitBulkRmStockMapper rscIitBulkRmStockMapper;

    public RscIitRmBulkOpRmServiceImpl(RscIitRmBulkOpRmRepository rscIitRmBulkOpRmRepository, RscIitRmBulkOpRmMapper rscIitRmBulkOpRmMapper, IndivisualValuesBulkOpRmMapper indivisualValuesBulkOpRmMapper, RscIitBulkRmStockMapper rscIitBulkRmStockMapper) {
        this.rscIitRmBulkOpRmRepository = rscIitRmBulkOpRmRepository;
        this.rscIitRmBulkOpRmMapper = rscIitRmBulkOpRmMapper;
        this.indivisualValuesBulkOpRmMapper = indivisualValuesBulkOpRmMapper;
        this.rscIitBulkRmStockMapper = rscIitBulkRmStockMapper;
    }

    @Override
    public List<RscIitRmBulkOpRmDTO> findAllByRscMtItemId(Long rscMtItemId) {
        return rscIitRmBulkOpRmMapper.toDto(rscIitRmBulkOpRmRepository.findAllByRscMtRmItemId(rscMtItemId));
    }

    @Override
    public List<IndividualValuesBulkOpRmDTO> findAllIndivisualValueDTORscMtItemId(Long rscMtItemId) {
        var a = rscIitRmBulkOpRmRepository.findAllByRscMtRmItemId(rscMtItemId);
        return indivisualValuesBulkOpRmMapper.toDto(rscIitRmBulkOpRmRepository.findAllByRscMtRmItemId(rscMtItemId));
    }

    @Override
    public List<RscIitBulkRMStockDTO> findAllByStockDate(LocalDate stockDate) {
        return rscIitBulkRmStockMapper.toDto(rscIitRmBulkOpRmRepository.findAllByStockDate(stockDate));
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscIitRmBulkOpRmRepository.deleteAllByStockDate(stockDate);
    }
}
