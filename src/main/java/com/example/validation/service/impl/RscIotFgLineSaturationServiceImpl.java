package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.*;
import com.example.validation.mapper.FgLineSaturationMapper;
import com.example.validation.mapper.LineSaturationMapper;
import com.example.validation.mapper.RscIotFgLineSaturationMapper;
import com.example.validation.repository.RscIotFgLineSaturationRepository;
import com.example.validation.service.*;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.excel_export.FgLineSaturationExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotFgLineSaturationServiceImpl implements RscIotFgLineSaturationService {
    private final RscIotFgLineSaturationRepository rscIotFgLineSaturationRepository;
    private final RscIotFgLineSaturationMapper rscIotFgLineSaturationMapper;
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscDtLinesService rscDtLinesService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService;
    private final FgLineSaturationMapper fgLineSaturationMapper;
    private final LineSaturationMapper lineSaturationMapper;

    public RscIotFgLineSaturationServiceImpl(RscIotFgLineSaturationRepository rscIotFgLineSaturationRepository, RscIotFgLineSaturationMapper rscIotFgLineSaturationMapper, RscMtPpdetailsService rscMtPpdetailsService, RscDtLinesService rscDtLinesService, RscMtPpheaderService rscMtPpheaderService, RscIitMpsWorkingDaysService rscIitMpsWorkingDaysService, FgLineSaturationMapper fgLineSaturationMapper, LineSaturationMapper lineSaturationMapper) {
        this.rscIotFgLineSaturationRepository = rscIotFgLineSaturationRepository;
        this.rscIotFgLineSaturationMapper = rscIotFgLineSaturationMapper;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscDtLinesService = rscDtLinesService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIitMpsWorkingDaysService = rscIitMpsWorkingDaysService;
        this.fgLineSaturationMapper = fgLineSaturationMapper;
        this.lineSaturationMapper = lineSaturationMapper;
    }

    @Override
    public void createFgLineSaturation(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (pPheaderDTO.isPresent()) {
            if (rscIotFgLineSaturationRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
                List<RscMtPPdetailsDTO> rscMtPPdetailsDTO = rscMtPpdetailsService.findAllPPdetailsByPPheader(ppHeaderId);
                if (Optional.ofNullable(rscMtPPdetailsDTO).isPresent()) {
                    List<RscIotFgLineSaturationDTO> rscIotFgLineSaturationDTOS = new ArrayList<>();
                    Double[] monthsTotalMpsPlanQty = new Double[12];
                    List<Long> rscDtLinesList = rscMtPpdetailsService.findUniqueRscDtLines(ppHeaderId);
                    RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO = rscIitMpsWorkingDaysService.findByMpsId(pPheaderDTO.get().getId());
                    rscDtLinesList.forEach(rscDtLines -> {
                        for (int i = 0; i < monthsTotalMpsPlanQty.length; i++) {
                            monthsTotalMpsPlanQty[i] = 0.0;
                        }
                        List<RscMtPPdetailsDTO> ppDetailsDTOS = rscMtPPdetailsDTO.stream().filter(rscMtPPdetails -> rscMtPPdetails.getRscDtLinesId().equals(rscDtLines)).collect(Collectors.toList());
                        Double[] monthsTotalMpsPlanQtyList = getMonthsTotalMpsPlanQuantity(monthsTotalMpsPlanQty, ppDetailsDTOS);
                        RscDtLinesDTO rscDtLinesDTO = rscDtLinesService.findById(rscDtLines);
                        RscIotFgLineSaturationDTO rscIotFgLineSaturationDTO = getFgLineSaturationDetails(monthsTotalMpsPlanQtyList, rscDtLinesDTO, rscIitMpsWorkingDaysDTO, pPheaderDTO.get().getId());
                        rscIotFgLineSaturationDTOS.add(rscIotFgLineSaturationDTO);
                    });
                    saveAll(rscIotFgLineSaturationDTOS);
                }
            }
        }
    }

    private RscIotFgLineSaturationDTO getFgLineSaturationDetails(Double[] monthsTotalMpsPlanQtyList, RscDtLinesDTO rscDtLinesDTO, RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO, Long ppHeaderId) {
        RscIotFgLineSaturationDTO rscIotFgLineSaturationDTO = new RscIotFgLineSaturationDTO();
        rscIotFgLineSaturationDTO.setRscMtPPheaderId(ppHeaderId);
        rscIotFgLineSaturationDTO.setRscDtLinesId(rscDtLinesDTO.getId());
        rscIotFgLineSaturationDTO.setM1TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[0]));
        rscIotFgLineSaturationDTO.setM2TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[1]));
        rscIotFgLineSaturationDTO.setM3TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[2]));
        rscIotFgLineSaturationDTO.setM4TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[3]));
        rscIotFgLineSaturationDTO.setM5TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[4]));
        rscIotFgLineSaturationDTO.setM6TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[5]));
        rscIotFgLineSaturationDTO.setM7TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[6]));
        rscIotFgLineSaturationDTO.setM8TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[7]));
        rscIotFgLineSaturationDTO.setM9TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[8]));
        rscIotFgLineSaturationDTO.setM10TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[9]));
        rscIotFgLineSaturationDTO.setM11TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[10]));
        rscIotFgLineSaturationDTO.setM12TotalMpsPlanQty(DoubleUtils.getFormattedValue(monthsTotalMpsPlanQtyList[11]));
        Double capacityPerShift;
        if (rscDtLinesDTO.getCapacityPerShift() != null || rscDtLinesDTO.getCapacityPerShift() != 0) {
            capacityPerShift = rscDtLinesDTO.getCapacityPerShift();
        } else {
            capacityPerShift = ((rscDtLinesDTO.getSpeed() * rscDtLinesDTO.getGUpTime() * rscDtLinesDTO.getHrsPerShift()) * 60) / 1000;
        }
        if (Optional.ofNullable(rscIitMpsWorkingDaysDTO).isPresent()) {
            rscIotFgLineSaturationDTO.setMonth1Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth1Value()));
            rscIotFgLineSaturationDTO.setMonth2Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth2Value()));
            rscIotFgLineSaturationDTO.setMonth3Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth3Value()));
            rscIotFgLineSaturationDTO.setMonth4Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth4Value()));
            rscIotFgLineSaturationDTO.setMonth5Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth5Value()));
            rscIotFgLineSaturationDTO.setMonth6Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth6Value()));
            rscIotFgLineSaturationDTO.setMonth7Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth7Value()));
            rscIotFgLineSaturationDTO.setMonth8Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth8Value()));
            rscIotFgLineSaturationDTO.setMonth9Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth9Value()));
            rscIotFgLineSaturationDTO.setMonth10Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth10Value()));
            rscIotFgLineSaturationDTO.setMonth11Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth11Value()));
            rscIotFgLineSaturationDTO.setMonth12Capacity(getCapacity(capacityPerShift, rscDtLinesDTO.getShifts(), rscIitMpsWorkingDaysDTO.getMonth12Value()));
            rscIotFgLineSaturationDTO.setM1LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[0], rscIotFgLineSaturationDTO.getMonth1Capacity()));
            rscIotFgLineSaturationDTO.setM2LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[1], rscIotFgLineSaturationDTO.getMonth2Capacity()));
            rscIotFgLineSaturationDTO.setM3LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[2], rscIotFgLineSaturationDTO.getMonth3Capacity()));
            rscIotFgLineSaturationDTO.setM4LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[3], rscIotFgLineSaturationDTO.getMonth4Capacity()));
            rscIotFgLineSaturationDTO.setM5LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[4], rscIotFgLineSaturationDTO.getMonth5Capacity()));
            rscIotFgLineSaturationDTO.setM6LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[5], rscIotFgLineSaturationDTO.getMonth6Capacity()));
            rscIotFgLineSaturationDTO.setM7LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[6], rscIotFgLineSaturationDTO.getMonth7Capacity()));
            rscIotFgLineSaturationDTO.setM8LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[7], rscIotFgLineSaturationDTO.getMonth8Capacity()));
            rscIotFgLineSaturationDTO.setM9LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[8], rscIotFgLineSaturationDTO.getMonth9Capacity()));
            rscIotFgLineSaturationDTO.setM10LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[9], rscIotFgLineSaturationDTO.getMonth10Capacity()));
            rscIotFgLineSaturationDTO.setM11LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[10], rscIotFgLineSaturationDTO.getMonth11Capacity()));
            rscIotFgLineSaturationDTO.setM12LineSaturation(getLineSaturation(monthsTotalMpsPlanQtyList[11], rscIotFgLineSaturationDTO.getMonth12Capacity()));
        }
        return rscIotFgLineSaturationDTO;
    }

    private Double getCapacity(Double capacityPerShift, Double shifts, Double monthValue) {
        if (Optional.ofNullable(capacityPerShift).isPresent() && Optional.ofNullable(shifts).isPresent() && Optional.ofNullable(monthValue).isPresent()) {
            return DoubleUtils.getFormattedValue(capacityPerShift * shifts * monthValue);
        } else {
            return 0.0;
        }
    }

    private Double getLineSaturation(Double monthValue, Double capacity) {
        if (capacity != 0) {
            return DoubleUtils.getFormattedValue(((monthValue / 1000) / capacity) * 100);
        } else {
            return 0.0;
        }
    }

    private Double[] getMonthsTotalMpsPlanQuantity(Double[] monthsTotalMpsPlanQty, List<RscMtPPdetailsDTO> ppdetailsDTOS) {
        for (RscMtPPdetailsDTO pdetailsDTO : ppdetailsDTOS) {
            monthsTotalMpsPlanQty[0] += pdetailsDTO.getM1Value();
            monthsTotalMpsPlanQty[1] += pdetailsDTO.getM2Value();
            monthsTotalMpsPlanQty[2] += pdetailsDTO.getM3Value();
            monthsTotalMpsPlanQty[3] += pdetailsDTO.getM4Value();
            monthsTotalMpsPlanQty[4] += pdetailsDTO.getM5Value();
            monthsTotalMpsPlanQty[5] += pdetailsDTO.getM6Value();
            monthsTotalMpsPlanQty[6] += pdetailsDTO.getM7Value();
            monthsTotalMpsPlanQty[7] += pdetailsDTO.getM8Value();
            monthsTotalMpsPlanQty[8] += pdetailsDTO.getM9Value();
            monthsTotalMpsPlanQty[9] += pdetailsDTO.getM10Value();
            monthsTotalMpsPlanQty[10] += pdetailsDTO.getM11Value();
            monthsTotalMpsPlanQty[11] += pdetailsDTO.getM12Value();
        }
        return monthsTotalMpsPlanQty;
    }

    @Override
    public void saveAll(List<RscIotFgLineSaturationDTO> rscIotFgLineSaturationDTOList) {
        rscIotFgLineSaturationRepository.saveAll(rscIotFgLineSaturationMapper.toEntity(rscIotFgLineSaturationDTOList));
    }

    @Override
    public FgLineSaturationDetailsDTO getAllFgLineSaturation(Long ppHeaderId) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        return pPheaderDTO.map(this::getFGLineSaturationDetailsDTO).orElse(null);
    }

    private FgLineSaturationDetailsDTO getFGLineSaturationDetailsDTO(RscMtPPheaderDTO rscMtPPheaderDTO) {
        FgLineSaturationDetailsDTO fgLineSaturationDetailsDTO = new FgLineSaturationDetailsDTO();
        fgLineSaturationDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
        fgLineSaturationDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
        fgLineSaturationDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
        RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO = rscIitMpsWorkingDaysService.findByMpsId(rscMtPPheaderDTO.getId());
        if (Optional.ofNullable(rscIitMpsWorkingDaysDTO).isPresent()) {
            fgLineSaturationDetailsDTO.setRscIitMpsWorkingDaysDTO(rscIitMpsWorkingDaysDTO);
        }
        List<FgLineSaturationDTO> rscIotFgLineSaturationList = fgLineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findAllByRscMtPPheaderId(rscMtPPheaderDTO.getId()));
        if (Optional.ofNullable(rscIotFgLineSaturationList).isPresent()) {
            fgLineSaturationDetailsDTO.setFgLineSaturationDTO(rscIotFgLineSaturationList);
        }
        return fgLineSaturationDetailsDTO;
    }

    @Override
    public ExcelExportFiles fgLineExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        FgLineSaturationDetailsDTO fgLineSaturationDetailsDTO = getFGLineSaturationDetailsDTO(rscMtPPheaderDTO);
        excelExportFiles.setFile(FgLineSaturationExporter.lineSaturationToExcel(fgLineSaturationDetailsDTO, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.FG_LINE_SATURATION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public LinesDetailsDTO getAllFgLines(Long ppHeaderId) {
        LinesDetailsDTO linesDetailsDTO = new LinesDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            linesDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
            linesDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            linesDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            linesDetailsDTO.setLinesDTOList(getAllFgLineSaturationDTO(rscMtPPheaderDTO.getId()));
        }
        return linesDetailsDTO;
    }

    private List<LinesDTO> getAllFgLineSaturationDTO(Long ppHeaderId) {
        return lineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public FgLineDetailsDTO getFgLinesDetails(Long ppHeaderId, Long rscDtLineId) {
        FgLineDetailsDTO lineDetailsDTO = new FgLineDetailsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            lineDetailsDTO.setMpsId(rscMtPPheaderDTO.getId());
            lineDetailsDTO.setMpsDate(rscMtPPheaderDTO.getMpsDate());
            lineDetailsDTO.setMpsName(rscMtPPheaderDTO.getMpsName());
            lineDetailsDTO.setFgLineSaturationDTO(fgLineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findByRscMtPPheaderIdAndRscDtLinesId(ppHeaderId, rscDtLineId)));
        }
        return lineDetailsDTO;
    }

    @Override
    public List<RscIotFgLineSaturationDTO> findAll(Long ppHeaderId) {
        return rscIotFgLineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscIotFgLineSaturationDTO> findAllByLinesIdAndRscMtPPheaderId(Long rscDtLineId, Long ppHeaderId) {
        return rscIotFgLineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findAllByRscDtLinesIdAndRscMtPPheaderId(rscDtLineId, ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFgLineSaturationRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public List<LinesDTO> getAllLines() {
        List<LinesDTO> linesDTOList = lineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findAllByRscMtPPheaderRscDtTagName("VD"));
        return getLinesWhoseSaturationPresentForAllMonths(linesDTOList);
    }

    private List<LinesDTO> getLinesWhoseSaturationPresentForAllMonths(List<LinesDTO> UniqueLinesDTOList) {
        List<LinesDTO> linesDTOList = lineSaturationMapper.toDto(rscIotFgLineSaturationRepository.findAllByRscMtPPheaderRscDtTagName("VD"));
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllByActiveAndIsValidated();
        HashSet<LinesDTO> linesDTOS = new HashSet<>();
        UniqueLinesDTOList.forEach(linesDTO -> {
            if (getCountOfValidatedSaturationsForLine(linesDTO.getId(), linesDTOList) == pPheaderDTOList.size()) {
                linesDTOS.add(linesDTO);
            }
        });
        return new ArrayList<>(linesDTOS);
    }

    private Long getCountOfValidatedSaturationsForLine(Long id, List<LinesDTO> linesDTOList) {
        return linesDTOList.stream().filter(linesDTO -> linesDTO.getId().equals(id)).count();
    }

}