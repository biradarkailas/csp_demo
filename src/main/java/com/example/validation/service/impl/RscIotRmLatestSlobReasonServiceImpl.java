package com.example.validation.service.impl;

import com.example.validation.dto.RscIotRmLatestSlobReasonDTO;
import com.example.validation.mapper.RscIotRmLatestSlobReasonMapper;
import com.example.validation.repository.RscIotRmLatestSlobReasonRepository;
import com.example.validation.service.RscIotRmLatestSlobReasonService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class RscIotRmLatestSlobReasonServiceImpl implements RscIotRmLatestSlobReasonService {
    private final RscIotRmLatestSlobReasonRepository rscIotRmLatestSlobReasonRepository;
    private final RscIotRmLatestSlobReasonMapper rscIotRmLatestSlobReasonMapper;

    public RscIotRmLatestSlobReasonServiceImpl(RscIotRmLatestSlobReasonRepository rscIotRmLatestSlobReasonRepository, RscIotRmLatestSlobReasonMapper rscIotRmLatestSlobReasonMapper) {
        this.rscIotRmLatestSlobReasonRepository = rscIotRmLatestSlobReasonRepository;
        this.rscIotRmLatestSlobReasonMapper = rscIotRmLatestSlobReasonMapper;
    }

    @Override
    public List<RscIotRmLatestSlobReasonDTO> findAll() {
        return rscIotRmLatestSlobReasonMapper.toDto(rscIotRmLatestSlobReasonRepository.findAll());
    }

    @Override
    public  void saveOne(RscIotRmLatestSlobReasonDTO rmLatestSlobReasonDTO) {
        rscIotRmLatestSlobReasonRepository.save(rscIotRmLatestSlobReasonMapper.toEntity(rmLatestSlobReasonDTO));
    }
}
