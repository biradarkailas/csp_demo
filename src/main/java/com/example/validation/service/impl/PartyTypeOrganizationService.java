package com.example.validation.service.impl;

import com.example.validation.domain.PartyTypeOrganization;
import com.example.validation.repository.PartyTypeOrganizationRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class PartyTypeOrganizationService {
    @Autowired
    PartyTypeOrganizationRepo partyTypeOrganizationRepo;

    public PartyTypeOrganization save(PartyTypeOrganization partyTypeOrganization) {
        return partyTypeOrganizationRepo.save(partyTypeOrganization);
    }

    public Optional<PartyTypeOrganization> findById(Long id) {
        return partyTypeOrganizationRepo.findById(id);
    }
}
