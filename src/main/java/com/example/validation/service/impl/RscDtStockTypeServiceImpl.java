package com.example.validation.service.impl;

import com.example.validation.domain.RscDtStockType;
import com.example.validation.dto.RscDtStockTypeDTO;
import com.example.validation.mapper.RscDtStockTypeMapper;
import com.example.validation.repository.RscDtStockTypeRepository;
import com.example.validation.service.RscDtStockTypeService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RscDtStockTypeServiceImpl implements RscDtStockTypeService {
    private final RscDtStockTypeRepository rscDtStockTypeRepository;
    private final RscDtStockTypeMapper rscDtStockTypeMapper;

    public RscDtStockTypeServiceImpl(RscDtStockTypeRepository rscDtStockTypeRepository, RscDtStockTypeMapper rscDtStockTypeMapper) {
        this.rscDtStockTypeRepository = rscDtStockTypeRepository;
        this.rscDtStockTypeMapper = rscDtStockTypeMapper;
    }

    @Override
    public List<RscDtStockTypeDTO> findAll() {
        return rscDtStockTypeMapper.toDto(rscDtStockTypeRepository.findAll());
    }

    @Override
    public RscDtStockType findOneByTag(String tag) {
        return rscDtStockTypeRepository.findOneByTag(tag);
    }

}
