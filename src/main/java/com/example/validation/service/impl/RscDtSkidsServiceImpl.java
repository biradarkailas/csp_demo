package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtSkidsDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.mapper.RscDtSkidsMapper;
import com.example.validation.repository.RscDtSkidsRepository;
import com.example.validation.service.RscDtSkidsService;
import com.example.validation.util.excel_export.FgMasterDataSkidSaturationExporter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class RscDtSkidsServiceImpl implements RscDtSkidsService {
    private final RscDtSkidsRepository rscDtSkidsRepository;
    private final RscDtSkidsMapper rscDtSkidsMapper;

    public RscDtSkidsServiceImpl(RscDtSkidsRepository rscDtSkidsRepository, RscDtSkidsMapper rscDtSkidsMapper) {
        this.rscDtSkidsRepository = rscDtSkidsRepository;
        this.rscDtSkidsMapper = rscDtSkidsMapper;
    }

    @Override
    public List<RscDtSkidsDTO> findAll() {
        return rscDtSkidsMapper.toDto( rscDtSkidsRepository.findAll() );
    }

    @Override
    public List<RscDtSkidsDTO> findFgSkidsWithPagination(Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of( pageNo, pageSize );
        Page<RscDtSkidsDTO> materialCategoryListResult = rscDtSkidsRepository.findAll( paging )
                .map( rscDtSkidsMapper::toDto );
        if (materialCategoryListResult.hasContent()) {
            return materialCategoryListResult.getContent();
        } else return new ArrayList<>();
    }

    @Override
    public RscDtSkidsDTO findById(Long rscDtLinesId) {
        return rscDtSkidsMapper.toDto( rscDtSkidsRepository.findById( rscDtLinesId ).get() );
    }

    @Override
    public ExcelExportFiles fgMasterDataSkidExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscDtSkidsDTO> rscDtSkidsDTOS = findAll();
        excelExportFiles.setFile( FgMasterDataSkidSaturationExporter.MasterDataSkidSaturationToExcel( rscDtSkidsDTOS, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.FG_Master_Data_SkidSaturation + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }
}