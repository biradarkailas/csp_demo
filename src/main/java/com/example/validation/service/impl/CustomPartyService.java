package com.example.validation.service.impl;

import com.example.validation.domain.*;
import com.example.validation.repository.PartyRelationshipRepo;
import com.example.validation.repository.PartyTypeOrganizationRepo;
import com.example.validation.repository.PartyTypeUserRepo;
import com.example.validation.util.EmailProperties;
import com.example.validation.util.RandomStringOrNumberGenerator;
import com.example.validation.util.enums.PartyOrganizationType;
import com.example.validation.util.enums.PartyType;
import com.example.validation.util.enums.RoleType;
import com.example.validation.util.enums.ServiceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

@Service
public class CustomPartyService {
    @Autowired
    PartyTypeUserService partyTypeUserService;

    @Autowired
    PartyNameService partyNameService;

    @Autowired
    PartyTypeOrganizationService partyTypeOrganizationService;

    @Autowired
    PartyService partyService;

    @Autowired
    PartyRelationshipService partyRelationshipService;

    @Autowired
    EmailService emailService;

    @Autowired
    EmailProperties emailProperties;

    @Autowired
    RandomStringOrNumberGenerator randomStringOrNumberGenerator;

    @Autowired
    PartyContactDetailsService partyContactDetailsService;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    PartyTypeOrganizationRepo partyTypeOrganizationRepo;
    private static final String EMPLOYEE = "EMPLOYEE";
    private static final String SUPPLIER = "SUPPLIER";
    @Autowired
    PartyRelationshipTypeService partyRelationshipTypeService;

    @Autowired
    RoleService roleService;

    @Autowired
    PartyTypeUserRepo partyTypeUserRepo;

    @Autowired
    PartyRelationshipRepo partyRelationshipRepo;

    public Party addPartyWithTypeOrganization(Party party) {
        party.setType(PartyType.ORGANIZATION);
        party.getPartyTypeOrganization().setParty(party);
        party.getPartyContactDetails().setParty(party);
        party.getPartyName().setParty(party);
        for (PartyAddressDetails partyAddressDetails : party.getPartyAddressDetails()) {
            partyAddressDetails.setParty(party);
        }
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        var lastTwoDigitOfYear = Integer.toString(currentYear).substring(2, 4);

        party.getPartyTypeOrganization().setLastReferenceNumber(lastTwoDigitOfYear + party.getPartyTypeOrganization().getVendorCode() + "000");
        partyService.save(party);
        return party;
    }

    public Party editPartyWithTypeOrganization(Party party) {
        party.getPartyTypeOrganization().setParty(party);
        party.getPartyContactDetails().setParty(party);
        party.getPartyName().setParty(party);
        for (PartyAddressDetails partyAddressDetails : party.getPartyAddressDetails()) {
            partyAddressDetails.setParty(party);
        }
        partyService.save(party);
        return party;
    }

    public List<PartyView> getPartyTypeOrganizations() {
        List partyTypeOrganizationViewList = new ArrayList<PartyView>();
        var partyList = partyService.findAllByType(PartyType.ORGANIZATION);
        for (Party partyEntity : partyList) {
            var partyTypeOrganizationView = new PartyView();
            partyTypeOrganizationView.party = partyEntity;
            partyTypeOrganizationView.partyRelationship = partyRelationshipService.findByFirstParty(partyEntity);
            if (partyEntity.getPartyTypeOrganization() != null && partyEntity.getPartyTypeOrganization().getOrganizationType() == PartyOrganizationType.ORGANIZATION) {
                partyTypeOrganizationViewList.add(partyTypeOrganizationView);
            }
        }
        return partyTypeOrganizationViewList;
    }

    public PartyView getPartyView(Long partyId) {
        var party = partyService.findById(partyId);
        if (party.isPresent()) {
            var partyRelationship = partyRelationshipService.findByFirstParty(party.get());
            var partyTypeOrganizationView = new PartyView();
            partyTypeOrganizationView.party = party.get();
            partyTypeOrganizationView.partyRelationship = partyRelationship;
            return partyTypeOrganizationView;
        } else {
            return null;
        }
    }

    public List<PartyView> getPartyTypeOrganizationsWithNoService() {
        List partyTypeOrganizationViewList = new ArrayList<PartyView>();
        var partyList = partyService.findAllByType(PartyType.ORGANIZATION);
        for (Party partyEntity : partyList) {
            var partyView = new PartyView();
            partyView.party = partyEntity;
            partyView.partyRelationship = partyRelationshipService.findByFirstParty(partyEntity);
            if (partyEntity.getPartyTypeOrganization() != null && !partyEntity.getPartyTypeOrganization().getService() &&
                    partyEntity.getPartyTypeOrganization().getOrganizationType() == PartyOrganizationType.ORGANIZATION) {
                partyTypeOrganizationViewList.add(partyView);
            }
        }
        return partyTypeOrganizationViewList;
    }

    public Party addPartyWithTypeUser(Party party) {
        party.setType(PartyType.USER);
        party.getPartyTypeUser().setParty(party);
        party.getPartyContactDetails().setParty(party);
        party.getPartyName().setParty(party);
        party.getPartyName().setFirstName(party.getPartyTypeUser().getFirstName());
        party.getPartyName().setLastName(party.getPartyTypeUser().getLastName());
        for (PartyAddressDetails partyAddressDetails : party.getPartyAddressDetails()) {
            partyAddressDetails.setParty(party);
        }
        if (party.getRoles() != null && party.getRoles().size() != 0) {
            var roleList = new ArrayList<Role>();
            for (Role role : party.getRoles()) {
                if (role.getId() != null) {
                    var roleFromDb = roleService.findById(role.getId());
                    roleFromDb.ifPresent(roleList::add);
                }
            }
            if (roleList.size() != 0) {
                party.getRoles().clear();
                party.setRoles(roleList);
            }
        }
        var password = randomStringOrNumberGenerator.generateRandomString(5);
        party.getPartyTypeUser().setPassword(password);
        String link = emailProperties.getSignInLink();
        String message = "Hello," + "\n" + "Your registration is successful. Following are," +
                " your credentials.\n" + "Username: " + party.getPartyTypeUser().getUsername() +
                "\n Password: " + password +
                "\nPlease login to your account with these credentials. Link is as follows" + "\n" + link;
        String[] toList = {party.getPartyContactDetails().getEmailIdOne()};
        emailService.sendEmailWithoutAttachment(toList, null, null, "Regarding User Registration", message);
        partyService.save(party);
        return party;
    }

    public Party editPartyWithTypeUser(Party party) {
        party.getPartyName().setParty(party);
        party.getPartyName().setFirstName(party.getPartyTypeUser().getFirstName());
        party.getPartyName().setLastName(party.getPartyTypeUser().getLastName());
        var partyFromDb = partyService.findById(party.getId());
        if (partyFromDb.isPresent()) {
            var roleList = new ArrayList<Role>();
            for (Role role : partyFromDb.get().getRoles()) {
                if (role.getId() != null) {
                    var roleFromDb = roleService.findById(role.getId());
                    roleFromDb.ifPresent(roleList::add);
                }
            }
            if (roleList.size() != 0) {
                party.getRoles().clear();
                party.setRoles(roleList);
            }
        }
        party.getPartyTypeUser().setParty(party);
        party.getPartyContactDetails().setParty(party);
        party.getPartyName().setParty(party);
        for (PartyAddressDetails partyAddressDetails : party.getPartyAddressDetails()) {
            partyAddressDetails.setParty(party);
        }
        partyService.save(party);
        return party;
    }

    public List<Party> getPartyTypeUsers(Long organizationId) {
        var organizationWiseUserList = new ArrayList<Party>();
        var partyList = partyService.findAllByType(PartyType.USER);
        for (Party partyEntity : partyList) {
            var partyRelationship = partyRelationshipService.findByFirstParty(partyEntity);
            if (partyRelationship != null && partyRelationship.getSecondParty() != null &&
                    organizationId != null && partyRelationship.getSecondParty().getId() == organizationId) {
                organizationWiseUserList.add(partyEntity);
            }
        }
        return organizationWiseUserList;
    }

    public PartyRelationship savePartyRelationship(PartyRelationship partyRelationship) {
        var party = partyService.findById(partyRelationship.getFirstParty().getId());
        if (party.isPresent()) {
            if (party.get().getType().equals(PartyType.USER)) {
                if (partyRelationship.getSecondParty() != null && partyRelationship.getSecondParty().getId() != null) {
                    var organization = partyService.findById(partyRelationship.getSecondParty().getId());
                    if (organization.isPresent()) {
                        if (organization.get().getPartyTypeOrganization() != null) {
                            var roleList = new ArrayList<Role>();
                            if (organization.get().getPartyTypeOrganization().getServiceType() == ServiceType.FF) {
                                var role = roleService.findByName(RoleType.role_FF.toString());
                                roleList.add(role);
                                if (party.get().getPartyTypeUser() != null) {
                                    party.get().setRoles(roleList);
                                    partyService.save(party.get());
                                }
                            } else if (organization.get().getPartyTypeOrganization().getServiceType() == ServiceType.CHA) {
                                var role = roleService.findByName(RoleType.role_CHA.toString());
                                roleList.add(role);
                                if (party.get().getPartyTypeUser() != null) {
                                    party.get().setRoles(roleList);
                                    partyService.save(party.get());
                                }
                            } else if (organization.get().getPartyTypeOrganization().getServiceType() == ServiceType.CBW) {
                                var role = roleService.findByName(RoleType.role_CBW.toString());
                                roleList.add(role);
                                if (party.get().getPartyTypeUser() != null) {
                                    party.get().setRoles(roleList);
                                    partyService.save(party.get());
                                }
                            } else if (organization.get().getPartyTypeOrganization().getServiceType() == ServiceType.LP) {
                                var role = roleService.findByName(RoleType.role_LP.toString());
                                roleList.add(role);
                                if (party.get().getPartyTypeUser() != null) {
                                    party.get().setRoles(roleList);
                                    partyService.save(party.get());
                                }
                            }
                        }
                    }
                }
                partyRelationship.setRelationshipType(partyRelationshipTypeService.findByName(EMPLOYEE));
            } else if (party.get().getType().equals(PartyType.ORGANIZATION) && party.get().getPartyTypeOrganization().getOrganizationType() == PartyOrganizationType.ORGANIZATION) {
                partyRelationship.setRelationshipType(partyRelationshipTypeService.findByName(SUPPLIER));
            }
        }
        return partyRelationshipService.save(partyRelationship);
    }

    public List<Party> getPartyWithServiceType(ServiceType serviceType) {
        var partyList = new ArrayList<Party>();
        var serviceTypeWisePartyList = partyTypeOrganizationRepo.findAllByServiceType(serviceType);
        if (serviceTypeWisePartyList.size() != 0) {
            for (PartyTypeOrganization partyTypeOrganization : serviceTypeWisePartyList) {
                partyList.add(partyTypeOrganization.getParty());
            }
        }
        return partyList;
    }

    public List<UsersEmail> getAllUserContactByOrganizationName(String organizationName) {
        List<UsersEmail> list = new ArrayList<>();
        PartyName partyName = partyNameService.findPartyNameByOrgnizationName(organizationName);
        var tempList = getPartyTypeUsers(partyName.getId());
        if (tempList != null) {
            tempList.forEach(party -> {
                UsersEmail usersEmail = new UsersEmail();
                usersEmail.username = party.getPartyTypeUser().getUsername();
                usersEmail.emailId = party.getPartyContactDetails().getEmailIdOne();
                list.add(usersEmail);
            });
            return list;
        }
        return null;
    }

    public class PartyView {
        public Party party;
        public PartyRelationship partyRelationship;
    }

    public PasswordChangeView changePassword(PasswordChangeView view) {
        PartyTypeUser user = partyTypeUserService.findById(view.getId()).orElse(null);
        var encoded = passwordEncoder.matches(view.getOldPassword(), user.getPassword());
        if (encoded) {
            // user.setPassword(passwordEncoder.encode(view.getNewPassword()));
            updatePassword(partyService.findByPartyTypeUser(user), view.getNewPassword());
            view.setStatus("RECOVERED");
            view.setMessage("Password Changed Successfully");
        } else {
            view.setMessage("Password not changed. Current password entered is wrong");
            view.setStatus("NOT_RECOVERED");
        }
        return view;
    }

    public ForgetUsernamePassword recoverForgetUsernamePassword(ForgetUsernamePassword view) {
        Party party = partyService.findByPartyTypeUser(partyTypeUserService.findByUsername(view.getUsername()));
        return foregetPasswordWithParty(party, view);
    }

    public ForgetUsernamePassword foregetPasswordWithParty(Party party, ForgetUsernamePassword view) {
        if (party != null) {
            var newPassword = randomStringOrNumberGenerator.generateRandomString(5);
            updatePassword(party, newPassword);
            var emailId = party.getPartyContactDetails().getEmailIdOne();
            var atIndex = emailId.indexOf('@');
            emailId = emailId.replace(emailId.substring(3, atIndex - 3), "*******");
            var message = "Your new credentials has been send to your registered email - " + emailId;
            view.setMessage(message);
            view.setStatus("RECOVERED");
        } else {
            var message = "Could not find any user with username - " + view.getUsername();
            view.setMessage(message);
            view.setStatus("NOT_RECOVERED");
        }
        return view;
    }

    private Party updatePassword(Party party, String newPassword) {
        party.setType(PartyType.USER);
        party.getPartyTypeUser().setParty(party);
        party.getPartyContactDetails().setParty(party);
        party.getPartyName().setParty(party);
        party.getPartyName().setFirstName(party.getPartyTypeUser().getFirstName());
        party.getPartyName().setLastName(party.getPartyTypeUser().getLastName());
        for (PartyAddressDetails partyAddressDetails : party.getPartyAddressDetails()) {
            partyAddressDetails.setParty(party);
        }
        if (party.getRoles() != null && party.getRoles().size() != 0) {
            var roleList = new ArrayList<Role>();
            for (Role role : party.getRoles()) {
                if (role.getId() != null) {
                    var roleFromDb = roleService.findById(role.getId());
                    roleFromDb.ifPresent(roleList::add);
                }
            }
            if (roleList.size() != 0) {
                party.getRoles().clear();
                party.setRoles(roleList);
            }
        }
        var password = newPassword;
        party.getPartyTypeUser().setPassword(password);
        String link = emailProperties.getSignInLink();
        String message = "Hello," + "\n" + "Your password change is successful. Following are," +
                " your new credentials.\n" + "Username: " + party.getPartyTypeUser().getUsername() +
                "\n Password: " + password +
                "\nPlease login to your account with these credentials. Link is as follows" + "\n" + link;
        String[] toList = {party.getPartyContactDetails().getEmailIdOne()};
        emailService.sendEmailWithoutAttachment(toList, null, null, "Regarding User Password change", message);
        partyService.save(party);
        return party;
    }

    public CustomOrgnizationDetail getOrgnizationDetailByUsername(String username) {
        var partyTypeUserFromDb = partyTypeUserService.findByUsername(username);
        if (partyTypeUserFromDb != null) {
            var partyRelationship = partyRelationshipRepo.findByFirstParty(partyTypeUserFromDb.getParty());
            if (partyRelationship != null) {
                var partyTypeOrganization = partyTypeOrganizationRepo.findByParty(partyRelationship.getSecondParty());
                if (partyTypeOrganization != null) {
                    CustomOrgnizationDetail customOrgnizationDetail;
                    customOrgnizationDetail = new CustomOrgnizationDetail();
                    customOrgnizationDetail.organizationName = partyTypeOrganization.getParty().getPartyName().getOrganizationName();
                    customOrgnizationDetail.id = partyTypeOrganization.getParty().getPartyName().getId();
                    customOrgnizationDetail.emailIdOne = partyTypeOrganization.getParty().getPartyContactDetails().getEmailIdOne();
                    return customOrgnizationDetail;
                }
            }
        }
        return null;
    }

    public List<UsersEmail> getUserContactOfSameOrganization(String username) {
        List<UsersEmail> list = new ArrayList<>();
        var partyTypeUserFromDb = partyTypeUserService.findByUsername(username);
        if (partyTypeUserFromDb != null) {
            var partyRelationship = partyRelationshipRepo.findByFirstParty(partyTypeUserFromDb.getParty());
            if (partyRelationship != null) {
                var partyTypeOrganization = partyTypeOrganizationRepo.findByParty(partyRelationship.getSecondParty());
                var tempList = getPartyTypeUsers(partyTypeOrganization.getParty().getPartyName().getId());
                if (tempList != null) {
                    tempList.forEach(party -> {
                        UsersEmail usersEmail = new UsersEmail();
                        usersEmail.username = party.getPartyTypeUser().getUsername();
                        usersEmail.emailId = party.getPartyContactDetails().getEmailIdOne();
                        list.add(usersEmail);
                    });
                    return list;
                }
            }
        }
        return null;
    }


    public List getOrgnizationNameByService() {
        var temp = partyTypeOrganizationRepo.getAllByService(false);
        List<OrgnizationForKey> list = new ArrayList<>();
        temp.forEach(a -> {
            try {
                OrgnizationForKey orgnizationForKey = new OrgnizationForKey();
                orgnizationForKey.setOrgnizationName(a.getParty().getPartyName().getOrganizationName());
                orgnizationForKey.setEmailId(a.getParty().getPartyContactDetails().getEmailIdOne());
                list.add(orgnizationForKey);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        });
        return list;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class OrgnizationForKey {
        String orgnizationName;
        String emailId;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public class UsersEmail {
        String username;
        String emailId;
    }

    public class CustomOrgnizationDetail {
        public Long id;
        public String organizationName;
        public String emailIdOne;
    }

    public PartyContactDetails getPartyContactDetailsByOrganizationName(String organizationName) {
        PartyName partyName = partyNameService.findPartyNameByOrgnizationName(organizationName);
        return partyName.getParty().getPartyContactDetails();
    }
}