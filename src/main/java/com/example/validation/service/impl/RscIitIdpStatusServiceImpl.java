package com.example.validation.service.impl;

import com.example.validation.domain.RscIitIdpStatus;
import com.example.validation.dto.FileNameDTO;
import com.example.validation.dto.FileUploadStatusDTO;
import com.example.validation.dto.MpsPlanViewDto;
import com.example.validation.dto.RscIitIdpStatusDTO;
import com.example.validation.mapper.RscIitIdpStatusMapper;
import com.example.validation.repository.RscIitIdpStatusRepository;
import com.example.validation.service.FileService;
import com.example.validation.service.RscIitIdpStatusService;
import com.example.validation.service.StagingTableSummaryService;
import com.example.validation.util.FileOperation;
import com.example.validation.util.enums.ModuleName;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RscIitIdpStatusServiceImpl implements RscIitIdpStatusService {
    private final RscIitIdpStatusRepository rscIitIdpStatusRepo;
    private final RscIitIdpStatusMapper rscIitIdpStatusMapper;
    private final FileService fileService;
    private final FileOperation fileOperation;
    private final StagingTableSummaryService stagingTableSummaryService;

    public RscIitIdpStatusServiceImpl(RscIitIdpStatusRepository rscIitIdpStatusRepo, RscIitIdpStatusMapper rscIitIdpStatusMapper, FileService fileService, FileOperation fileOperation, StagingTableSummaryService stagingTableSummaryService) {
        this.rscIitIdpStatusRepo = rscIitIdpStatusRepo;
        this.rscIitIdpStatusMapper = rscIitIdpStatusMapper;
        this.fileService = fileService;
        this.fileOperation = fileOperation;
        this.stagingTableSummaryService = stagingTableSummaryService;
    }

    @Override
    public List<RscIitIdpStatusDTO> findRscIitIdpStatusByName(String name) {
        return rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsName(name));
    }

    @Override
    public Boolean mpsNameExists(String name) {
        return rscIitIdpStatusRepo.findByMpsName(name).size() > 0;
    }

    @Override
    public void save(RscIitIdpStatusDTO rscIitIdpStatusDTO) {
        rscIitIdpStatusRepo.save(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTO));
    }

    @Override
    public RscIitIdpStatusDTO findIdpStatusDTOByMpsNameAndFileName(String mpsName, String fileName) {
        String[] splitFileName = fileName.split("[.]");
        return rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndRscDtFileNameFileNameAndRscDtFileNameExtension(mpsName, splitFileName[0], splitFileName[1]));
    }

    @Override
    public MpsPlanViewDto saveAll(MpsPlanViewDto mpsPlanViewDto) {
        stagingTableSummaryService.inActiveAllStagingTableSummary();
        InActiveAllActiveRecords();
        fileOperation.deleteDirectory();
        LocalDate mpsDate = LocalDate.parse(mpsPlanViewDto.getMpsDateStr());
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = new ArrayList<>();
        List<FileNameDTO> fileNameDTOs = fileService.findAllFiles(mpsPlanViewDto.getTagName());
        for (FileNameDTO fileNameDTO : fileNameDTOs) {
            RscIitIdpStatusDTO rscIitIdpStatusDTO = new RscIitIdpStatusDTO();
            rscIitIdpStatusDTO.setMpsName(mpsPlanViewDto.getMpsName());
            rscIitIdpStatusDTO.setMpsDate(mpsDate);
            rscIitIdpStatusDTO.setRscDtTagId(mpsPlanViewDto.getRscDtTagId());
            rscIitIdpStatusDTO.setIsActive(true);
            rscIitIdpStatusDTO.setUploadStatus(false);
            rscIitIdpStatusDTO.setValidationStatus(false);
            rscIitIdpStatusDTO.setResourceStatus(false);
            rscIitIdpStatusDTO.setIsRequired(fileNameDTO.getIsRequired());
            rscIitIdpStatusDTO.setFileNameId(fileNameDTO.getId());
            rscIitIdpStatusDTOs.add(rscIitIdpStatusDTO);
        }
        List<RscIitIdpStatus> rscIitIdpStatuses = rscIitIdpStatusRepo.saveAll(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTOs));
        mpsPlanViewDto.setMpsDate(mpsDate);
        mpsPlanViewDto.setRscIitIdpStatusDTOs(rscIitIdpStatusMapper.toDto(rscIitIdpStatuses));
        return mpsPlanViewDto;
    }

    @Override
    public MpsPlanViewDto getCurrentMpsPlan() {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByIsActive(true));
        return getMpsPlanViewDTO(rscIitIdpStatusDTOs);
    }

    private Boolean isEnable(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs, String planName) {
        Boolean result = getIsEnableValue(rscIitIdpStatusDTOs);
        if (result) {
            return true;
        } else {
            return isValidPMFirstCutAndValidatedPlan(planName);
        }
    }

    private Boolean isEditable(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOs) {
            if (rscIitIdpStatusDTO.getIsRequired()) {
                if (rscIitIdpStatusDTO.getResourceStatus()) {
                    return false;
                }
            }
        }
        return true;
    }

    private Boolean getIsEnableValue(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        boolean result = false;
        for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOs) {
            if (!rscIitIdpStatusDTO.getValidationStatus()) {
                if (rscIitIdpStatusDTO.getIsRequired()) {
                    if (rscIitIdpStatusDTO.getUploadStatus()) {
                        result = true;
                    } else {
                        return false;
                    }
                } else {
                    if (rscIitIdpStatusDTO.getUploadStatus()) {
                        result = true;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public Boolean isPlanAvailable() {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByIsActive(true));
        return !rscIitIdpStatusDTOs.isEmpty();
    }

    @Override
    public FileUploadStatusDTO uploadFile(MultipartFile file, HttpSession session, Long idpStatusId) {
        FileUploadStatusDTO fileUploadStatusDTO = fileOperation.uploadFiles(file);
        if (fileUploadStatusDTO.getStatus()) {
            RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findById(idpStatusId).orElse(null));
            rscIitIdpStatusDTO.setUploadStatus(true);
            rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.save(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTO)));
            updateAllFilesModifiedDate(idpStatusId);
        }
        return fileUploadStatusDTO;
    }

    @Override
    public void updateAllFilesModifiedDate(Long idpStatusId) {
        RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findById(idpStatusId).orElse(null));
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByIsActive(true));
        for (RscIitIdpStatusDTO rscIitIdpStatus : rscIitIdpStatusDTOs) {
            rscIitIdpStatus.setModifiedDate(rscIitIdpStatusDTO.getModifiedDate());
        }
        rscIitIdpStatusRepo.saveAll(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTOs));
    }

    @Override
    public MpsPlanViewDto uploadedMpsPlan(String mpsName) {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndUploadStatus(mpsName, true));
        return getMpsPlanViewDTO(rscIitIdpStatusDTOs);
    }

    private MpsPlanViewDto getMpsPlanViewDTO(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        if (!rscIitIdpStatusDTOs.isEmpty()) {
            RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusDTOs.get(0);
            MpsPlanViewDto mpsPlanViewDTO = new MpsPlanViewDto();
            mpsPlanViewDTO.setMpsName(rscIitIdpStatusDTO.getMpsName());
            mpsPlanViewDTO.setMpsDate(rscIitIdpStatusDTO.getMpsDate());
            mpsPlanViewDTO.setMpsDateStr(rscIitIdpStatusDTO.getMpsDate().toString());
            mpsPlanViewDTO.setRscDtTagId(rscIitIdpStatusDTO.getRscDtTagId());
            mpsPlanViewDTO.setTagName(rscIitIdpStatusDTO.getTagName());
            mpsPlanViewDTO.setTagDescription(rscIitIdpStatusDTO.getTagDescription());
            mpsPlanViewDTO.setEnable(isEnable(rscIitIdpStatusDTOs, rscIitIdpStatusDTO.getMpsName()));
            mpsPlanViewDTO.setPmOTIFEnable(isPMOTIFValidatedPlan(rscIitIdpStatusDTOs));
            mpsPlanViewDTO.setEditable(isEditable(rscIitIdpStatusDTOs));
            mpsPlanViewDTO.setOtifDate(rscIitIdpStatusDTO.getOtifDate());
            mpsPlanViewDTO.setIsExecutionDone(rscIitIdpStatusDTO.getIsExecutionDone());
            mpsPlanViewDTO.setModifiedDate(rscIitIdpStatusDTO.getModifiedDate());
            mpsPlanViewDTO.setRscIitIdpStatusDTOs(rscIitIdpStatusDTOs);
            mpsPlanViewDTO.setExecutionEnable(isExecutionEnable(rscIitIdpStatusDTOs));
            return mpsPlanViewDTO;
        }
        return null;
    }

    @Override
    public ModuleName isValidPlan(String mpsName) {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndIsRequired(mpsName, true));
        return getModuleName(rscIitIdpStatusDTOs);
    }

    private ModuleName getModuleName(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        if (isValidatedPlan(rscIitIdpStatusDTOs)) {
            return ModuleName.ALL;
        } else {
            if (isValidatedPlan(getFilteredRscIitIdpStatusDTOs(rscIitIdpStatusDTOs, "PM"))) {
                return ModuleName.PM;
            } else {
                return ModuleName.RM;
            }
        }
    }

    private List<RscIitIdpStatusDTO> getFilteredRscIitIdpStatusDTOs(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs, String moduleName) {
        return rscIitIdpStatusDTOs.stream().filter(rscIitIdpStatusDTO -> rscIitIdpStatusDTO.getFolderName().equals(moduleName)).collect(Collectors.toList());
    }

    private Boolean isValidatedPlan(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        boolean result = false;
        if (!rscIitIdpStatusDTOs.isEmpty()) {
            for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOs) {
                if (rscIitIdpStatusDTO.getUploadStatus() && rscIitIdpStatusDTO.getValidationStatus() && rscIitIdpStatusDTO.getResourceStatus()) {
                    result = true;
                } else {
                    return false;
                }
            }
        }
        return result;
    }

    private void InActiveAllActiveRecords() {
        List<RscIitIdpStatus> rscIitIdpStatusList = new ArrayList<>();
        List<RscIitIdpStatus> rscIitIdpStatuses = rscIitIdpStatusRepo.findByIsActive(true);
        for (RscIitIdpStatus rscIitIdpStatus : rscIitIdpStatuses) {
            rscIitIdpStatus.setIsActive(false);
            rscIitIdpStatusList.add(rscIitIdpStatus);
        }
        rscIitIdpStatusRepo.saveAll(rscIitIdpStatusList);
    }

    @Override
    public List<RscIitIdpStatusDTO> findAllRscIitIdpStatusByActive() {
        return rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByIsActive(true));
    }

    private Boolean isValidPMFirstCutAndValidatedPlan(String mpsName) {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndIsRequired(mpsName, true));
        if (!rscIitIdpStatusDTOs.isEmpty()) {
            RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusDTOs.get(0);
            if (rscIitIdpStatusDTO.getTagName().equals("FC")) {
                List<RscIitIdpStatusDTO> pmFirstCutPlan = rscIitIdpStatusDTOs.stream().filter(rscIitIdpStatus -> rscIitIdpStatus.getFolderName().equals("PM")).collect(Collectors.toList());
                return isValidPMOTIPPlan(pmFirstCutPlan);
            } else if (rscIitIdpStatusDTO.getTagName().equals("VD")) {
                return isValidPMOTIPPlan(rscIitIdpStatusDTOs);
            }
        }
        return false;
    }

    private Boolean isValidPMOTIPPlan(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        boolean result = false;
        if (!rscIitIdpStatusDTOs.isEmpty()) {
            if (isValidatedPlan(rscIitIdpStatusDTOs)) {
                RscIitIdpStatusDTO rscIitIdpStatusDTO = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndRscDtFileNameFileName(rscIitIdpStatusDTOs.get(0).getMpsName(), "OTIF_PM"));
                if (Optional.ofNullable(rscIitIdpStatusDTO).isPresent()) {
                    result = rscIitIdpStatusDTO.getUploadStatus() && !rscIitIdpStatusDTO.getValidationStatus();
                }
            }
        }
        return result;
    }

    @Override
    public MpsPlanViewDto updateStatusOfFiles(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        rscIitIdpStatusDTOs.forEach(rscIitIdpStatusDTO -> {
            rscIitIdpStatusDTO.setUploadStatus(false);
            rscIitIdpStatusDTO.setValidationStatus(false);
            rscIitIdpStatusDTO.setResourceStatus(false);
            fileOperation.deleteFile(rscIitIdpStatusDTO);
        });
        rscIitIdpStatusRepo.saveAll(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTOs));
        updateAllFilesModifiedDate(rscIitIdpStatusDTOs.get(0).getId());
        return getCurrentMpsPlan();
    }

    private Boolean isPMOTIFValidatedPlan(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        boolean result = false;
        if (!rscIitIdpStatusDTOs.isEmpty()) {
            for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOs) {
                if (rscIitIdpStatusDTO.getIsRequired()) {
                    if (rscIitIdpStatusDTO.getUploadStatus() && rscIitIdpStatusDTO.getValidationStatus() && rscIitIdpStatusDTO.getResourceStatus()) {
                        result = true;
                    } else {
                        return false;
                    }
                }
            }
        }
        return result;
    }

    @Override
    public MpsPlanViewDto saveOtifDate(MpsPlanViewDto mpsPlanViewDto) {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOList = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndMpsDate(mpsPlanViewDto.getMpsName(), mpsPlanViewDto.getMpsDate()));
        if (!rscIitIdpStatusDTOList.isEmpty()) {
            for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOList) {
                rscIitIdpStatusDTO.setOtifDate(mpsPlanViewDto.getOtifDate());
            }
            rscIitIdpStatusRepo.saveAll(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTOList));
        }
        return getCurrentMpsPlan();
    }

    @Override
    public MpsPlanViewDto saveExecutionStatus(MpsPlanViewDto mpsPlanViewDto) {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOList = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndMpsDate(mpsPlanViewDto.getMpsName(), mpsPlanViewDto.getMpsDate()));
        if (!rscIitIdpStatusDTOList.isEmpty()) {
            for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOList) {
                rscIitIdpStatusDTO.setIsExecutionDone(true);
            }
            rscIitIdpStatusRepo.saveAll(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTOList));
        }
        return getCurrentMpsPlan();
    }

    @Override
    public MpsPlanViewDto updateRscIitIdpStatus(MpsPlanViewDto mpsPlanViewDto) {
        List<RscIitIdpStatusDTO> rscIitIdpStatusDTOList = rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByIsActive(true));
        List<FileNameDTO> fileNameDTOs = fileService.findAllFiles(mpsPlanViewDto.getTagName());
        if (!rscIitIdpStatusDTOList.isEmpty()) {
            for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOList) {
                rscIitIdpStatusDTO.setMpsName(mpsPlanViewDto.getMpsName());
                rscIitIdpStatusDTO.setMpsDate(mpsPlanViewDto.getMpsDate());
                rscIitIdpStatusDTO.setOtifDate(mpsPlanViewDto.getOtifDate());
                if (!rscIitIdpStatusDTO.getRscDtTagId().equals(mpsPlanViewDto.getRscDtTagId())) {
                    rscIitIdpStatusDTO.setRscDtTagId(mpsPlanViewDto.getRscDtTagId());
                    rscIitIdpStatusDTO.setIsRequired(getIsRequired(fileNameDTOs, rscIitIdpStatusDTO.getFileName()));
                }
            }
            rscIitIdpStatusRepo.saveAll(rscIitIdpStatusMapper.toEntity(rscIitIdpStatusDTOList));
        }
        return getCurrentMpsPlan();
    }

    private boolean getIsRequired(List<FileNameDTO> fileNameDTOs, String fileName) {
        return fileNameDTOs.stream().filter(fileNameDTO -> fileNameDTO.getFileName().equals(fileName)).findAny().get().getIsRequired();
    }

    @Override
    public void deleteAll(LocalDate mpsDate) {
        rscIitIdpStatusRepo.deleteAllByMpsDateBetween(mpsDate, mpsDate.withDayOfMonth(mpsDate.lengthOfMonth()));
    }

    @Override
    public Boolean mpsNameExistsForEdit(String name) {
        return rscIitIdpStatusRepo.findByMpsNameAndIsActiveFalse(name).size() > 0;
    }

    @Override
    public LocalDate getMpsDateByMpsName(String name) {
        List<RscIitIdpStatus> rscIitIdpStatusList = rscIitIdpStatusRepo.findByMpsName(name);
        return rscIitIdpStatusList.stream().findAny().get().getMpsDate();
    }

    private Boolean isExecutionEnable(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs) {
        boolean result = false;
        for (RscIitIdpStatusDTO rscIitIdpStatusDTO : rscIitIdpStatusDTOs) {
            if (rscIitIdpStatusDTO.getResourceStatus()) {
                result = true;
            } else {
                return false;
            }
        }
        return result;
    }

    @Override
    public List<RscIitIdpStatusDTO> findRscIitIdpStatusByNameAndDate(String name, LocalDate mpsDate) {
        return rscIitIdpStatusMapper.toDto(rscIitIdpStatusRepo.findByMpsNameAndMpsDate(name, mpsDate));
    }
}