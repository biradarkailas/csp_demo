package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.domain.RscIotRmSlob;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.mapper.RmSlobMapper;
import com.example.validation.mapper.RscIotRmSlobMapper;
import com.example.validation.repository.RscIotRmSlobRepository;
import com.example.validation.service.*;
import com.example.validation.util.*;
import com.example.validation.util.enums.SlobType;
import com.example.validation.util.excel_export.RmSlobExporter;
import com.example.validation.util.excel_export.RmSlobInventoryQualityExporter;
import com.example.validation.util.excel_export.RmSlobMovementExporter;
import com.example.validation.util.excel_export.RmSlobSummaryExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotRMSlobServiceImpl implements RscIotRMSlobService {
    private final RscIotRmStockService rscIotRmStockService;
    private final RscIotRmSlobMapper rscIotRmSlobMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscItRMGrossConsumptionService rscItRMGrossConsumptionService;
    private final RscItItemWiseSupplierService rscItItemWiseSupplierService;
    private final RscIotRmSlobRepository rscIotRmSlobRepository;
    private final RmSlobMapper rmSlobMapper;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;
    private final RscDtReasonService rscDtReasonService;
    private final RscIotRmLatestSlobReasonService rscIotRmLatestSlobReasonService;
    private final SlobSummaryUtil slobSummaryUtil;
    private final SlobMovementsUtil slobMovementsUtil;
    private final SlobInventoryQualityIndexUtil slobInventoryQualityIndexUtil;
    private final RscMtItemService rscMtItemService;


    public RscIotRMSlobServiceImpl(RscIotRmStockService rscIotRmStockService, RscIotRmSlobMapper rscIotRmSlobMapper, RscMtPpheaderService rscMtPpheaderService, RscItRMGrossConsumptionService rscItRMGrossConsumptionService, RscItItemWiseSupplierService rscItItemWiseSupplierService, RscIotRmSlobRepository rscIotRmSlobRepository, RmSlobMapper rmSlobMapper, RscIotRmSlobSummaryService rscIotRmSlobSummaryService, RscDtReasonService rscDtReasonService, RscIotRmLatestSlobReasonService rscIotRmLatestSlobReasonService, SlobSummaryUtil slobSummaryUtil, SlobMovementsUtil slobMovementsUtil, SlobInventoryQualityIndexUtil slobInventoryQualityIndexUtil, RscMtItemService rscMtItemService) {
        this.rscIotRmStockService = rscIotRmStockService;
        this.rscIotRmSlobMapper = rscIotRmSlobMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscItRMGrossConsumptionService = rscItRMGrossConsumptionService;
        this.rscItItemWiseSupplierService = rscItItemWiseSupplierService;
        this.rscIotRmSlobRepository = rscIotRmSlobRepository;
        this.rmSlobMapper = rmSlobMapper;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
        this.rscDtReasonService = rscDtReasonService;
        this.rscIotRmLatestSlobReasonService = rscIotRmLatestSlobReasonService;
        this.slobSummaryUtil = slobSummaryUtil;
        this.slobMovementsUtil = slobMovementsUtil;
        this.slobInventoryQualityIndexUtil = slobInventoryQualityIndexUtil;
        this.rscMtItemService = rscMtItemService;
    }

    @Override
    public void calculateRscItRMSlob(Long ppHeaderId) {
        if (rscIotRmSlobRepository.countByRscMtPPheaderId( ppHeaderId ) == 0) {
            Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
            if (pPheaderDTO.isPresent()) {
                List<String> stockTypeList = new ArrayList<>();
                stockTypeList.add( MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES );
                stockTypeList.add( MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJ );
                List<RscIotRmStockDTO> rscIotRmStockDTOList = rscIotRmStockService.findAllRmStocksByStockTypeTag( stockTypeList );
                HashMap<Long, RscIotRmStockDTO> mesMap = new HashMap<>();
                HashMap<Long, RscIotRmStockDTO> expiryMap = new HashMap<>();
                List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS = rscIotRmLatestSlobReasonService.findAll();
                Set<Long> uniqueItemId = new HashSet<>();
                for (RscIotRmStockDTO rscIotRmStockDTO : rscIotRmStockDTOList) {
                    if (!rscIotRmStockDTO.getItemCode().equals( "511S" )) {
                        if (rscIotRmStockDTO.getStockTypeTag().equals( MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES ) && DateUtils.isEqualMonthAndYear( rscIotRmStockDTO.getStockDate(), pPheaderDTO.get().getMpsDate() )) {
                            mesMap.put( rscIotRmStockDTO.getRscMtItemId(), rscIotRmStockDTO );
                        } else if (DateUtils.isEqualMonthAndYear( rscIotRmStockDTO.getStockDate(), pPheaderDTO.get().getMpsDate() )) {
                            expiryMap.put( rscIotRmStockDTO.getRscMtItemId(), rscIotRmStockDTO );
                        }
                        if (DateUtils.isEqualMonthAndYear( rscIotRmStockDTO.getStockDate(), pPheaderDTO.get().getMpsDate() )) {
                            uniqueItemId.add( rscIotRmStockDTO.getRscMtItemId() );
                        }
                    }
                }
                calculateRscItRMSlobData( mesMap, expiryMap, uniqueItemId, ppHeaderId, rscIotRmLatestSlobReasonDTOS );
            }
        }
    }

    private void calculateRscItRMSlobData(HashMap<Long, RscIotRmStockDTO> mesMap, HashMap<Long, RscIotRmStockDTO> expiryMap, Set<Long> uniqueItemIds, Long ppHeaderId, List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS) {
        List<RscIotRmSlobDTO> rscIotRmSlobDTO = new ArrayList<>();
        Double stockValue;
        Double stockQuantity;
        Double expiryStockQuantity;
        Double expirySlobValue;
        Double price;
        Double balanceStock;
        Double totalSlobValue;
        for (Long uniqueItemId : uniqueItemIds) {
            stockValue = 0.0;
            stockQuantity = 0.0;
            expiryStockQuantity = 0.0;
            expirySlobValue = 0.0;
            price = null;
            balanceStock = 0.0;
            totalSlobValue = 0.0;
            RscIotRmSlobDTO rmSlobDTO = new RscIotRmSlobDTO();
            rmSlobDTO.setRscMtPPheaderId( ppHeaderId );
            rmSlobDTO.setRscMtItemId( uniqueItemId );
            RscItGrossConsumptionDTO rmGrossConsumptionDto = rscItRMGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId( ppHeaderId, uniqueItemId );
            RscItItemWiseSupplierDTO oneByRscMtItemIdAndAllocation = rscItItemWiseSupplierService.findOneByRscMtItemIdAndAllocation( uniqueItemId );
            if (Optional.ofNullable( oneByRscMtItemIdAndAllocation ).isPresent()) {
                rmSlobDTO.setRscItItemWiseSupplierId( oneByRscMtItemIdAndAllocation.getId() );
            }
            if (Optional.ofNullable( rmGrossConsumptionDto ).isPresent()) {
                rmSlobDTO.setRscItRMGrossConsumptionId( rmGrossConsumptionDto.getId() );
                rmSlobDTO.setTotalConsumptionQuantity( rmGrossConsumptionDto.getTotalValue() );
            }
            if (Optional.ofNullable( expiryMap.get( uniqueItemId ) ).isPresent()) {
                rmSlobDTO = getDetailsIfItemPresentInExpiryMap( rmSlobDTO, expiryMap.get( uniqueItemId ) );
                expiryStockQuantity = rmSlobDTO.getExpiryStockQuantity();
                expirySlobValue = rmSlobDTO.getExpirySlobValue();
                totalSlobValue += expirySlobValue;
                price = getPriceValue( rmSlobDTO );
            }
            if (Optional.ofNullable( mesMap.get( uniqueItemId ) ).isPresent()) {
                rmSlobDTO = getDetailsIfItemPresentInMesMap( rmSlobDTO, mesMap.get( uniqueItemId ) );
                stockQuantity = rmSlobDTO.getStockQuantity();
                balanceStock = rmSlobDTO.getBalanceStock();
                stockValue = rmSlobDTO.getStockValue();
                price = getPriceValue( rmSlobDTO );
            }
            if (Optional.ofNullable( balanceStock ).isPresent()) {
                rmSlobDTO.setBalanceStock( balanceStock );
            } else {
                rmSlobDTO.setBalanceStock( 0.0 );
            }
            if (Optional.ofNullable( price ).isPresent()) {
                rmSlobDTO.setPrice( price );
            }
            rmSlobDTO.setSlobValue( getSlobValue( expirySlobValue, rmSlobDTO ) );

            if (!Optional.ofNullable( rmSlobDTO.getTotalConsumptionQuantity() ).isPresent()) {
                if (Optional.ofNullable( price ).isPresent() && balanceStock != 0) {
                    rmSlobDTO.setObSlobValue( balanceStock * price );
                    rmSlobDTO.setTotalObValue( expirySlobValue + rmSlobDTO.getObSlobValue() );
                    totalSlobValue += rmSlobDTO.getObSlobValue();
                }
            } else {
                rmSlobDTO.setTotalObValue( expirySlobValue );
            }

            if (!Optional.ofNullable( rmSlobDTO.getObSlobValue() ).isPresent()) {
                if (Optional.ofNullable( price ).isPresent() && balanceStock != 0 && rmSlobDTO.getSlobType().equals( SlobType.SL )) {
                    rmSlobDTO.setSlowSlobValue( DoubleUtils.round( balanceStock * price ) );
                    totalSlobValue += rmSlobDTO.getSlowSlobValue();
                }
            } else {
                if (Optional.ofNullable( price ).isPresent() && rmSlobDTO.getSlobType().equals( SlobType.SL )) {
                    rmSlobDTO.setSlowSlobValue( DoubleUtils.round( (balanceStock * price) - rmSlobDTO.getObSlobValue() ) );
                    totalSlobValue += rmSlobDTO.getSlowSlobValue();
                }
            }
            if (balanceStock != 0.0) {
                rmSlobDTO.setSlobQuantity( balanceStock );
            }
            rmSlobDTO.setTotalStockQuantity( DoubleUtils.getFormattedValue( expiryStockQuantity + stockQuantity ) );
            rmSlobDTO.setTotalStockValue( DoubleUtils.round( stockValue + expirySlobValue ) );
            rmSlobDTO.setRscDtReasonId( getReasonIdFromSlobReason( uniqueItemId, rscIotRmLatestSlobReasonDTOS ) );
            rmSlobDTO.setTotalSlobValue( totalSlobValue );
            rscIotRmSlobDTO.add( rmSlobDTO );
        }
        getRmSlobListWithGrossConsumptionItems( rscIotRmSlobDTO, uniqueItemIds, ppHeaderId, rscIotRmLatestSlobReasonDTOS );
    }

    private void getRmSlobListWithGrossConsumptionItems(List<RscIotRmSlobDTO> rscIotRmSlobDTO, Set<Long> uniqueItemIds, Long ppHeaderId, List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS) {
        List<RscItGrossConsumptionDTO> grossConsumptionDTOList = rscItRMGrossConsumptionService.findAllRMGrossConsumptionByRscMtPPHeaderId( ppHeaderId );
        Double price;
        for (RscItGrossConsumptionDTO rscItGrossConsumptionDTO : grossConsumptionDTOList) {
            if (!uniqueItemIds.contains( rscItGrossConsumptionDTO.getRscMtItemId() ) && (!rscItGrossConsumptionDTO.getItemCode().equals( "511S" ))) {
                price = null;
                RscIotRmSlobDTO rmSlobDTO = new RscIotRmSlobDTO();
                RscItItemWiseSupplierDTO oneByRscMtItemIdAndAllocation = rscItItemWiseSupplierService.findOneByRscMtItemIdAndAllocation( rscItGrossConsumptionDTO.getRscMtItemId() );
                if (Optional.ofNullable( oneByRscMtItemIdAndAllocation ).isPresent()) {
                    rmSlobDTO.setRscItItemWiseSupplierId( oneByRscMtItemIdAndAllocation.getId() );
                }
                RscMtItemDTO rscMtItemDTO = rscMtItemService.findById( rscItGrossConsumptionDTO.getRscMtItemId() );
                rmSlobDTO.setRscMtItemId( rscItGrossConsumptionDTO.getRscMtItemId() );
                if (Optional.ofNullable( rscMtItemDTO ).isPresent()) {
                    if (Optional.ofNullable( rscMtItemDTO.getStdPrice() ).isPresent()) {
                        rmSlobDTO.setStdPrice( rscMtItemDTO.getStdPrice() );
                        price = rscMtItemDTO.getStdPrice();
                    }
                    if (Optional.ofNullable( rscMtItemDTO.getMapPrice() ).isPresent()) {
                        rmSlobDTO.setMapPrice( rscMtItemDTO.getMapPrice() );
                        price = rscMtItemDTO.getMapPrice();
                    }
                }
                rmSlobDTO.setPrice( price );
                rmSlobDTO.setTotalSlobValue( 0.0 );
                rmSlobDTO.setRscDtReasonId( getReasonIdFromSlobReason( rscItGrossConsumptionDTO.getRscMtItemId(), rscIotRmLatestSlobReasonDTOS ) );
                rmSlobDTO.setRscMtPPheaderId( ppHeaderId );
                rmSlobDTO.setRscItRMGrossConsumptionId( rscItGrossConsumptionDTO.getId() );
                rmSlobDTO.setTotalConsumptionQuantity( rscItGrossConsumptionDTO.getTotalValue() );
                rmSlobDTO.setRscDtItemCode( rscItGrossConsumptionDTO.getItemCode() );
                rscIotRmSlobDTO.add( rmSlobDTO );
            }
        }
        rscIotRmSlobRepository.saveAll( rscIotRmSlobMapper.toEntity( rscIotRmSlobDTO ) );
    }

    private RscIotRmSlobDTO getDetailsIfItemPresentInExpiryMap(RscIotRmSlobDTO rmSlobDTO, RscIotRmStockDTO rscIotRmStockDTO) {
        if (Optional.ofNullable( rscIotRmStockDTO.getItemCode() ).isPresent()) {
            rmSlobDTO.setRscDtItemCode( rscIotRmStockDTO.getItemCode() );
        }
        if (Optional.ofNullable( rscIotRmStockDTO.getQuantity() ).isPresent()) {
            rmSlobDTO.setExpiryStockQuantity( rscIotRmStockDTO.getQuantity() );
            rmSlobDTO.setExpirySlobValue( getStockValue( rscIotRmStockDTO ) );
        }
        if (Optional.ofNullable( rscIotRmStockDTO.getStdPrice() ).isPresent()) {
            rmSlobDTO.setStdPrice( rscIotRmStockDTO.getStdPrice() );
        }
        if (Optional.ofNullable( rscIotRmStockDTO.getMapPrice() ).isPresent()) {
            rmSlobDTO.setMapPrice( rscIotRmStockDTO.getMapPrice() );
        }
        return rmSlobDTO;
    }

    private RscIotRmSlobDTO getDetailsIfItemPresentInMesMap(RscIotRmSlobDTO rmSlobDTO, RscIotRmStockDTO rscIotRmStockDTO) {
        if (Optional.ofNullable( rscIotRmStockDTO.getItemCode() ).isPresent()) {
            rmSlobDTO.setRscDtItemCode( rscIotRmStockDTO.getItemCode() );
        }
        if (Optional.ofNullable( rscIotRmStockDTO.getStdPrice() ).isPresent()) {
            rmSlobDTO.setStdPrice( rscIotRmStockDTO.getStdPrice() );
        }
        if (Optional.ofNullable( rscIotRmStockDTO.getMapPrice() ).isPresent()) {
            rmSlobDTO.setMapPrice( rscIotRmStockDTO.getMapPrice() );
        }
        if (Optional.ofNullable( rscIotRmStockDTO.getQuantity() ).isPresent()) {
            rmSlobDTO.setStockQuantity( rscIotRmStockDTO.getQuantity() );
            rmSlobDTO.setBalanceStock( rscIotRmStockDTO.getQuantity() );
            rmSlobDTO.setStockValue( getStockValue( rscIotRmStockDTO ) );
            if (Optional.ofNullable( rmSlobDTO.getTotalConsumptionQuantity() ).isPresent()) {
                if (rscIotRmStockDTO.getQuantity() - rmSlobDTO.getTotalConsumptionQuantity() > 0) {
                    rmSlobDTO.setBalanceStock( rscIotRmStockDTO.getQuantity() - rmSlobDTO.getTotalConsumptionQuantity() );
                    rmSlobDTO.setSlobType( SlobType.SL );
                } else {
                    rmSlobDTO.setBalanceStock( 0.0 );
                }
            } else {
                rmSlobDTO.setSlobType( SlobType.OB );
            }
        }
        return rmSlobDTO;
    }

    private Double getSlobValue(Double expirySlobValue, RscIotRmSlobDTO rmSlobDTO) {
        if (Optional.ofNullable( rmSlobDTO.getPrice() ).isPresent()) {
            return DoubleUtils.round( rmSlobDTO.getBalanceStock() * rmSlobDTO.getPrice() + expirySlobValue );
        } else if (expirySlobValue != 0.00) {
            return DoubleUtils.round( expirySlobValue );
        } else {
            return 0.0;
        }
    }

    private Double getPriceValue(RscIotRmSlobDTO rmSlobDTO) {
        if (Optional.ofNullable( rmSlobDTO.getMapPrice() ).isPresent()) {
            return rmSlobDTO.getMapPrice();
        } else if (Optional.ofNullable( rmSlobDTO.getStdPrice() ).isPresent()) {
            return rmSlobDTO.getStdPrice();
        } else {
            return null;
        }
    }

    private Long getReasonIdFromSlobReason(Long rscMtItemId, List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS) {
        List<RscIotRmLatestSlobReasonDTO> iotRmLatestSlobReasonDTO = rscIotRmLatestSlobReasonDTOS.stream().filter( rscIotRmLatestSlobReasonDTO -> rscIotRmLatestSlobReasonDTO.getRscMtItemId().equals( rscMtItemId ) ).collect( Collectors.toList() );
        if (iotRmLatestSlobReasonDTO.size() == 0) {
            return null;
        } else {
            RscIotRmLatestSlobReasonDTO rscIotRmLatestSlobReasonDTO = iotRmLatestSlobReasonDTO.get( 0 );
            return rscIotRmLatestSlobReasonDTO.getRscDtReasonId();
        }
    }

    @Override
    public void saveAllSlobs(List<RscIotRmSlobDTO> rscIotRMSlobDTOList) {
        List<RscIotRmSlobDTO> obj1 = new ArrayList<>();
        List<RscIotRmSlobDTO> obj2 = new ArrayList<>();
        for (RscIotRmSlobDTO rscIotRmSlobDTO : rscIotRMSlobDTOList) {
            if (isValid( rscIotRmSlobDTO )) {
                obj1.add( rscIotRmSlobDTO );
            } else {
                obj2.add( rscIotRmSlobDTO );
            }
        }
        obj1.sort( Comparator.comparingLong( obj -> Long.parseLong( obj.getRscDtItemCode() ) ) );
        obj2.sort( Comparator.comparing( obj -> obj.getRscDtItemCode() ) );
        obj1.addAll( obj2 );
        rscIotRmSlobRepository.saveAll( rscIotRmSlobMapper.toEntity( obj1 ) );
    }

    private Boolean isValid(RscIotRmSlobDTO rscIotRmSlobDTO) {
        try {
            Long.parseLong( rscIotRmSlobDTO.getRscDtItemCode() );
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    @Override
    public void calculateRscItRMSlobSummary(Long rscMtPPHeaderId, Double newSitValue) {
        Optional<RscMtPPheaderDTO> pPheaderDTO = rscMtPpheaderService.findOne( rscMtPPHeaderId );
        if (pPheaderDTO.isPresent()) {
            if (rscIotRmSlobSummaryService.countByRscMtPPheaderId( rscMtPPHeaderId ) == 0) {
                RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = new RscIotRmSlobSummaryDTO();
                List<RscIotRmSlobDTO> rscItRMSlobList = getRscItRMSlobList( rscMtPPHeaderId );
                Double totalStockValue;
                Double sitValue;
                if (Optional.ofNullable( newSitValue ).isPresent()) {
                    sitValue = newSitValue;
                } else {
                    sitValue = 0.0;
                }
                Double totalStockSitValue;
                Double totalSlobValue;
                Double totalSlowItemValue;
                Double totalObsoleteExpiryValue;
                Double totalObsoleteNonExpiryValue;
                Double totalObsoleteItemValue;
                Double slowItemPercentage;
                Double obsoleteItemPercentage;
                Double stockQualityPercentage;
                Double obsoleteExpiryValuePercentage;
                Double obsoleteNonExpiryValuePercentage;
                Double stockQualityValue;
                Double slobSum;
                Double totalSlobQuantityValue;
                Double totalMidNightStockValue;
                Double totalExpiredStockValue;
                Double totalConsumptionQuantity;
                Double[] grossConsumptionTotalArray = new Double[12];
                for (int i = 0; i < grossConsumptionTotalArray.length; i++) {
                    grossConsumptionTotalArray[i] = 0.0;
                }
                if (Optional.ofNullable( rscItRMSlobList ).isPresent()) {
                    totalStockValue = 0.0;
                    totalSlowItemValue = 0.0;
                    totalObsoleteExpiryValue = 0.0;
                    totalObsoleteNonExpiryValue = 0.0;
                    totalObsoleteItemValue = 0.0;
                    slobSum = 0.0;
                    totalMidNightStockValue = 0.0;
                    totalExpiredStockValue = 0.0;
                    totalConsumptionQuantity = 0.0;
                    totalSlobQuantityValue = 0.0;
                    for (RscIotRmSlobDTO rscItRMSlobLists : rscItRMSlobList) {
                        if (rscItRMSlobLists.getTotalStockValue() != null) {
                            totalStockValue += rscItRMSlobLists.getTotalStockValue();
                        }
                        if (rscItRMSlobLists.getSlobValue() != null) {
                            slobSum += rscItRMSlobLists.getSlobValue();
                        }
                        if (rscItRMSlobLists.getStockQuantity() != null) {
                            totalMidNightStockValue += rscItRMSlobLists.getStockQuantity();
                        }
                        if (rscItRMSlobLists.getExpiryStockQuantity() != null) {
                            totalExpiredStockValue += rscItRMSlobLists.getExpiryStockQuantity();
                        }
                        if (rscItRMSlobLists.getSlobQuantity() != null) {
                            totalSlobQuantityValue += rscItRMSlobLists.getSlobQuantity();
                        }
                        if (Optional.ofNullable( rscItRMSlobLists.getSlobType() ).isPresent()) {
                            if (rscItRMSlobLists.getSlobType().equals( SlobType.SL )) {
                                if (rscItRMSlobLists.getSlobValue() != null) {
                                    totalSlowItemValue += rscItRMSlobLists.getSlowSlobValue();
                                }
                            } else if (rscItRMSlobLists.getSlobType().equals( SlobType.OB )) {
                                if (rscItRMSlobLists.getSlobValue() != null) {
                                    totalObsoleteNonExpiryValue += rscItRMSlobLists.getObSlobValue();
                                }
                                if (rscItRMSlobLists.getSlobValue() != null && rscItRMSlobLists.getExpirySlobValue() != null) {
                                    totalObsoleteItemValue += rscItRMSlobLists.getSlobValue() + rscItRMSlobLists.getExpirySlobValue();
                                }
                            }
                        }
                        if (Optional.ofNullable( rscItRMSlobLists.getExpirySlobValue() ).isPresent()) {
                            totalObsoleteExpiryValue += rscItRMSlobLists.getExpirySlobValue();
                        }
                        if (Optional.ofNullable( rscItRMSlobLists.getTotalConsumptionQuantity() ).isPresent()) {
                            totalConsumptionQuantity += rscItRMSlobLists.getTotalConsumptionQuantity();
                            grossConsumptionTotalArray = updateGrossConsumptionArray( grossConsumptionTotalArray, rscItRMSlobLists.getRscItRMGrossConsumptionId() );
                        }
                    }
                    totalSlobValue = slobSum;
                    totalStockSitValue = totalStockValue + sitValue;
                    totalObsoleteItemValue = totalObsoleteExpiryValue + totalObsoleteNonExpiryValue;
                    slowItemPercentage = (totalSlowItemValue / totalStockValue) * 100;
                    obsoleteItemPercentage = (totalObsoleteItemValue / totalStockValue) * 100;
                    obsoleteExpiryValuePercentage = (totalObsoleteExpiryValue / totalStockValue) * 100;
                    obsoleteNonExpiryValuePercentage = (totalObsoleteNonExpiryValue / totalStockValue) * 100;
                    stockQualityPercentage = 100 - (slowItemPercentage + obsoleteItemPercentage);
//                    stockQualityValue = (totalStockValue * stockQualityPercentage) / 100;
                    stockQualityValue = totalStockValue - totalSlobValue;
                    rscIotRmSlobSummaryDTO.setRscMtPPheaderId( rscMtPPHeaderId );
                    rscIotRmSlobSummaryDTO.setSitValue( sitValue );
                    rscIotRmSlobSummaryDTO.setTotalStockValue( DoubleUtils.round( totalStockValue ) );
                    rscIotRmSlobSummaryDTO.setTotalStockSitValue( DoubleUtils.round( totalStockSitValue ) );
                    rscIotRmSlobSummaryDTO.setTotalSlobValue( DoubleUtils.round( totalSlobValue ) );
                    rscIotRmSlobSummaryDTO.setTotalSlowItemValue( DoubleUtils.round( totalSlowItemValue ) );
                    rscIotRmSlobSummaryDTO.setTotalObsoleteItemValue( DoubleUtils.round( totalObsoleteItemValue ) );
                    rscIotRmSlobSummaryDTO.setTotalObsoleteItemValue( DoubleUtils.round( totalObsoleteItemValue ) );
                    rscIotRmSlobSummaryDTO.setSlowItemPercentage( DoubleUtils.getFormattedValue( slowItemPercentage ) );
                    rscIotRmSlobSummaryDTO.setObsoleteItemPercentage( DoubleUtils.getFormattedValue( obsoleteItemPercentage ) );
                    rscIotRmSlobSummaryDTO.setStockQualityPercentage( DoubleUtils.getFormattedValue( stockQualityPercentage ) );
                    rscIotRmSlobSummaryDTO.setObsoleteExpiryValuePercentage( DoubleUtils.getFormattedValue( obsoleteExpiryValuePercentage ) );
                    rscIotRmSlobSummaryDTO.setObsoleteNonExpiryValuePercentage( DoubleUtils.getFormattedValue( obsoleteNonExpiryValuePercentage ) );
                    rscIotRmSlobSummaryDTO.setStockQualityValue( DoubleUtils.round( stockQualityValue ) );
                    rscIotRmSlobSummaryDTO.setTotalObsoleteExpiryValue( DoubleUtils.round( totalObsoleteExpiryValue ) );
                    rscIotRmSlobSummaryDTO.setTotalObsoleteNonExpiryValue( DoubleUtils.round( totalObsoleteNonExpiryValue ) );
                    rscIotRmSlobSummaryDTO.setTotalMidNightStockValue( DoubleUtils.round( totalMidNightStockValue ) );
                    rscIotRmSlobSummaryDTO.setTotalExpiredStockValue( DoubleUtils.round( totalExpiredStockValue ) );
                    rscIotRmSlobSummaryDTO.setTotalFinalStockValue( DoubleUtils.round( totalMidNightStockValue + totalExpiredStockValue ) );
                    rscIotRmSlobSummaryDTO.setTotalConsumptionQuantity( DoubleUtils.round( totalConsumptionQuantity ) );
                    rscIotRmSlobSummaryDTO.setTotalSlobQuantityValue( DoubleUtils.round( totalSlobQuantityValue ) );
                    rscIotRmSlobSummaryDTO = getGrossConsumptionAllMonthsTotalValue( rscIotRmSlobSummaryDTO, grossConsumptionTotalArray );

                }
                save( rscIotRmSlobSummaryDTO );
            }
        }
    }

    private Double[] updateGrossConsumptionArray(Double[] grossConsumptionTotalArray, Long grossConsumptionId) {
        RscItGrossConsumptionDTO rscItGrossConsumptionDTO = rscItRMGrossConsumptionService.findByGrossConsumptionId( grossConsumptionId );
        grossConsumptionTotalArray[0] += rscItGrossConsumptionDTO.getM1Value();
        grossConsumptionTotalArray[1] += rscItGrossConsumptionDTO.getM2Value();
        grossConsumptionTotalArray[2] += rscItGrossConsumptionDTO.getM3Value();
        grossConsumptionTotalArray[3] += rscItGrossConsumptionDTO.getM4Value();
        grossConsumptionTotalArray[4] += rscItGrossConsumptionDTO.getM5Value();
        grossConsumptionTotalArray[5] += rscItGrossConsumptionDTO.getM6Value();
        grossConsumptionTotalArray[6] += rscItGrossConsumptionDTO.getM7Value();
        grossConsumptionTotalArray[7] += rscItGrossConsumptionDTO.getM8Value();
        grossConsumptionTotalArray[8] += rscItGrossConsumptionDTO.getM9Value();
        grossConsumptionTotalArray[9] += rscItGrossConsumptionDTO.getM10Value();
        grossConsumptionTotalArray[10] += rscItGrossConsumptionDTO.getM11Value();
        grossConsumptionTotalArray[11] += rscItGrossConsumptionDTO.getM12Value();
        return grossConsumptionTotalArray;
    }

    private RscIotRmSlobSummaryDTO getGrossConsumptionAllMonthsTotalValue(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO, Double[] grossConsumptionTotalArray) {
        rscIotRmSlobSummaryDTO.setM1TotalQuantity( grossConsumptionTotalArray[0] );
        rscIotRmSlobSummaryDTO.setM2TotalQuantity( grossConsumptionTotalArray[1] );
        rscIotRmSlobSummaryDTO.setM3TotalQuantity( grossConsumptionTotalArray[2] );
        rscIotRmSlobSummaryDTO.setM4TotalQuantity( grossConsumptionTotalArray[3] );
        rscIotRmSlobSummaryDTO.setM5TotalQuantity( grossConsumptionTotalArray[4] );
        rscIotRmSlobSummaryDTO.setM6TotalQuantity( grossConsumptionTotalArray[5] );
        rscIotRmSlobSummaryDTO.setM7TotalQuantity( grossConsumptionTotalArray[6] );
        rscIotRmSlobSummaryDTO.setM8TotalQuantity( grossConsumptionTotalArray[7] );
        rscIotRmSlobSummaryDTO.setM9TotalQuantity( grossConsumptionTotalArray[8] );
        rscIotRmSlobSummaryDTO.setM10TotalQuantity( grossConsumptionTotalArray[9] );
        rscIotRmSlobSummaryDTO.setM11TotalQuantity( grossConsumptionTotalArray[10] );
        rscIotRmSlobSummaryDTO.setM12TotalQuantity( grossConsumptionTotalArray[11] );
        return rscIotRmSlobSummaryDTO;
    }

    private List<RscIotRmSlobDTO> getRscItRMSlobList(Long rscMtPPHeaderId) {
        return rscIotRmSlobMapper.toDto( rscIotRmSlobRepository.findAllByRscMtPPheaderId( rscMtPPHeaderId ) );
    }

    @Override
    public void save(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        rscIotRmSlobSummaryService.saveRmSlobSummary( rscIotRmSlobSummaryDTO );
    }

    private Double getStockValue(RscIotRmStockDTO rscIotRmStockDTO) {
        if (rscIotRmStockDTO.getMapPrice() != null) {
            return rscIotRmStockDTO.getQuantity() * rscIotRmStockDTO.getMapPrice();
        } else {
            return 0.0;
        }
    }

    @Override
    public RMSlobDetailsDTO getAllRscIotRmSlob(Long rscMtPPHeaderId) {
        RMSlobDetailsDTO rMSlobDetailsDTO = new RMSlobDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( rscMtPPHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            rMSlobDetailsDTO.setMpsId( rscMtPPheaderDTO.get().getId() );
            rMSlobDetailsDTO.setMpsDate( rscMtPPheaderDTO.get().getMpsDate() );
            rMSlobDetailsDTO.setMpsName( rscMtPPheaderDTO.get().getMpsName() );
            rMSlobDetailsDTO.setCurrentMonthValue( DateUtils.getCurrentMonthName( rscMtPPheaderDTO.get().getMpsDate().minusMonths( 1 ) ) );
            rMSlobDetailsDTO.setPreviousMonthValue( DateUtils.getCurrentMonthName( rscMtPPheaderDTO.get().getMpsDate().minusMonths( 2 ) ) );
            RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId( rscMtPPHeaderId );
            if (Optional.ofNullable( rscIotRmSlobSummaryDTO ).isPresent()) {
                rMSlobDetailsDTO = getRmSlobSummaryValues( rscIotRmSlobSummaryDTO, rMSlobDetailsDTO );
            }
            Long previousMonthsMpsId = rscMtPpheaderService.getPreviousMonthsFirstCutMpsId( rscMtPPheaderDTO.get().getId() );
            if (Optional.ofNullable( previousMonthsMpsId ).isPresent()) {
                RscIotRmSlobSummaryDTO previousMonthsSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId( previousMonthsMpsId );
                if (Optional.ofNullable( previousMonthsSummaryDTO ).isPresent()) {
                    rMSlobDetailsDTO = getPreviousMonthsDetails( previousMonthsSummaryDTO, rMSlobDetailsDTO );
                }
            }
            List<RmSlobDTO> rscIotRmSlobList = rmSlobMapper.toDto( rscIotRmSlobRepository.findAllByRscMtPPheaderId( rscMtPPHeaderId ).stream().filter( rscIotRmSlob -> rscIotRmSlob.getTotalSlobValue() != null ).sorted( Comparator.comparing( RscIotRmSlob::getTotalSlobValue ).reversed() ).collect( Collectors.toList() ) );
            if (Optional.ofNullable( rscIotRmSlobList ).isPresent()) {
                Map<Long, RscIotRmSlobDTO> rmSlobMap = getPreviousMPSRMSlobMap( rscMtPPHeaderId );
                if (Optional.ofNullable( rmSlobMap ).isPresent()) {

                    for (RmSlobDTO rmSlobDTO : rscIotRmSlobList) {
                        rmSlobDTO.setPreviousMonthsSlobValue( getSlobValueByItemId( rmSlobMap, rmSlobDTO.getRscMtItemId() ) );
                        rmSlobDTO.setPreviousMonthsObsoleteExpiryValue( getObsoleteExpiryValueByItemId( rmSlobMap, rmSlobDTO.getRscMtItemId() ) );
                        rmSlobDTO.setPreviousMonthsObsoleteNonExpiryValue( getObsoleteNonExpiryValueByItemId( rmSlobMap, rmSlobDTO.getRscMtItemId() ) );
                        rmSlobDTO.setPreviousMonthsSlowMovingValue( getSlowMovingValueByItemId( rmSlobMap, rmSlobDTO.getRscMtItemId() ) );
                    }
                }
                rMSlobDetailsDTO.setRmSlobs( rscIotRmSlobList );
            }
        }
        return rMSlobDetailsDTO;
    }

    private RMSlobDetailsDTO getPreviousMonthsDetails(RscIotRmSlobSummaryDTO previousMonthsSummaryDTO, RMSlobDetailsDTO rMSlobDetailsDTO) {
        rMSlobDetailsDTO.setPreviousMonthsTotalSlobValue( previousMonthsSummaryDTO.getTotalSlobValue() );
        rMSlobDetailsDTO.setPreviousMonthsTotalObsoleteExpiryValue( previousMonthsSummaryDTO.getTotalObsoleteExpiryValue() );
        rMSlobDetailsDTO.setPreviousMonthsTotalObsoleteExpiryValue( previousMonthsSummaryDTO.getTotalObsoleteExpiryValue() );
        rMSlobDetailsDTO.setPreviousMonthsTotalObsoleteNonExpiryValue( previousMonthsSummaryDTO.getTotalObsoleteNonExpiryValue() );
        rMSlobDetailsDTO.setPreviousMonthsTotalSlowItemValue( previousMonthsSummaryDTO.getTotalSlowItemValue() );
        return rMSlobDetailsDTO;
    }

    private RMSlobDetailsDTO getRmSlobSummaryValues(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO, RMSlobDetailsDTO rMSlobDetailsDTO) {
        rMSlobDetailsDTO.setTotalStockValue( rscIotRmSlobSummaryDTO.getTotalStockValue() );
        rMSlobDetailsDTO.setTotalSlobQuantityValue( rscIotRmSlobSummaryDTO.getTotalSlobQuantityValue() );
        rMSlobDetailsDTO.setTotalObsoleteExpiryValue( rscIotRmSlobSummaryDTO.getTotalObsoleteExpiryValue() );
        rMSlobDetailsDTO.setTotalObsoleteNonExpiryValue( rscIotRmSlobSummaryDTO.getTotalObsoleteNonExpiryValue() );
        rMSlobDetailsDTO.setTotalSlowItemValue( rscIotRmSlobSummaryDTO.getTotalSlowItemValue() );
        rMSlobDetailsDTO.setSitValue( rscIotRmSlobSummaryDTO.getSitValue() );
        rMSlobDetailsDTO.setTotalSlobValue( rscIotRmSlobSummaryDTO.getTotalSlobValue() );
        rMSlobDetailsDTO.setTotalMidNightStockValue( rscIotRmSlobSummaryDTO.getTotalMidNightStockValue() );
        rMSlobDetailsDTO.setTotalExpiredStockValue( rscIotRmSlobSummaryDTO.getTotalExpiredStockValue() );
        rMSlobDetailsDTO.setTotalFinalStockValue( rscIotRmSlobSummaryDTO.getTotalFinalStockValue() );
        rMSlobDetailsDTO.setTotalConsumptionQuantity( rscIotRmSlobSummaryDTO.getTotalConsumptionQuantity() );
        rMSlobDetailsDTO.setM1TotalQuantity( rscIotRmSlobSummaryDTO.getM1TotalQuantity() );
        rMSlobDetailsDTO.setM2TotalQuantity( rscIotRmSlobSummaryDTO.getM2TotalQuantity() );
        rMSlobDetailsDTO.setM3TotalQuantity( rscIotRmSlobSummaryDTO.getM3TotalQuantity() );
        rMSlobDetailsDTO.setM4TotalQuantity( rscIotRmSlobSummaryDTO.getM4TotalQuantity() );
        rMSlobDetailsDTO.setM5TotalQuantity( rscIotRmSlobSummaryDTO.getM5TotalQuantity() );
        rMSlobDetailsDTO.setM6TotalQuantity( rscIotRmSlobSummaryDTO.getM6TotalQuantity() );
        rMSlobDetailsDTO.setM7TotalQuantity( rscIotRmSlobSummaryDTO.getM7TotalQuantity() );
        rMSlobDetailsDTO.setM8TotalQuantity( rscIotRmSlobSummaryDTO.getM8TotalQuantity() );
        rMSlobDetailsDTO.setM9TotalQuantity( rscIotRmSlobSummaryDTO.getM9TotalQuantity() );
        rMSlobDetailsDTO.setM10TotalQuantity( rscIotRmSlobSummaryDTO.getM10TotalQuantity() );
        rMSlobDetailsDTO.setM11TotalQuantity( rscIotRmSlobSummaryDTO.getM11TotalQuantity() );
        rMSlobDetailsDTO.setM12TotalQuantity( rscIotRmSlobSummaryDTO.getM12TotalQuantity() );
        return rMSlobDetailsDTO;
    }

    private Double getSlowMovingValueByItemId(Map<Long, RscIotRmSlobDTO> rmSlobMap, Long itemId) {
        if (Optional.ofNullable( rmSlobMap.get( itemId ) ).isPresent()) {
            return rmSlobMap.get( itemId ).getSlowSlobValue();
        } else {
            return null;
        }
    }

    private Double getObsoleteNonExpiryValueByItemId(Map<Long, RscIotRmSlobDTO> rmSlobMap, Long itemId) {
        if (Optional.ofNullable( rmSlobMap.get( itemId ) ).isPresent()) {
            return rmSlobMap.get( itemId ).getObSlobValue();
        } else {
            return null;
        }
    }

    private Double getObsoleteExpiryValueByItemId(Map<Long, RscIotRmSlobDTO> rmSlobMap, Long itemId) {
        if (Optional.ofNullable( rmSlobMap.get( itemId ) ).isPresent()) {
            return rmSlobMap.get( itemId ).getExpirySlobValue();
        } else {
            return null;
        }
    }

    private Double getSlobValueByItemId(Map<Long, RscIotRmSlobDTO> rmSlobMap, Long itemId) {
        if (Optional.ofNullable( rmSlobMap.get( itemId ) ).isPresent()) {
            return rmSlobMap.get( itemId ).getSlobValue();
        } else {
            return 0.0;
        }
    }

    private Map<Long, RscIotRmSlobDTO> getPreviousMPSRMSlobMap(Long mpsId) {
        Long prevMpsId = rscMtPpheaderService.getPreviousMonthsFirstCutMpsId( mpsId );
        if (Optional.ofNullable( prevMpsId ).isPresent()) {
            Map<Long, RscIotRmSlobDTO> rmSlobMap = new HashMap<>();
            for (RscIotRmSlobDTO rscIotRmSlobDTO : findAllRMSlobsByRscMtPpHeaderId( prevMpsId )) {
                rmSlobMap.put( rscIotRmSlobDTO.getRscMtItemId(), rscIotRmSlobDTO );
            }
            return rmSlobMap;
        }
        return null;
    }

    @Override
    public List<RscIotRmSlobDTO> findAllRMSlobsByRscMtPpHeaderId(Long rscMtPPHeaderId) {
        return rscIotRmSlobMapper.toDto( rscIotRmSlobRepository.findAllByRscMtPPheaderId( rscMtPPHeaderId ) );
    }

    @Override
    public SlobTotalConsumptionDetailsDTO getTotalConsumption(Long ppHeaderId, Long rscMtItemId) {
        return rscItRMGrossConsumptionService.getRmSlobTotalConsumptionDetailsDTO( ppHeaderId, rscMtItemId );
    }

    @Override
    public SlobSummaryDetailsDTO getRmSlobSummaryDetails(Long ppHeaderId) {
        SlobSummaryDetailsDTO rmSlobSummaryDetails = new SlobSummaryDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            rmSlobSummaryDetails.setMpsId( rscMtPPheaderDTO.get().getId() );
            rmSlobSummaryDetails.setMpsDate( rscMtPPheaderDTO.get().getMpsDate() );
            rmSlobSummaryDetails.setMpsName( rscMtPPheaderDTO.get().getMpsName() );
            RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId( ppHeaderId );
            if (Optional.ofNullable( rscIotRmSlobSummaryDTO ).isPresent()) {
                SlobSummaryFormulaDetailsDTO rmSlobSummaryFormulaDetails = new SlobSummaryFormulaDetailsDTO();
                rmSlobSummaryFormulaDetails.setTotalStockSitValueDTO( getTotalStockSitValue( rscIotRmSlobSummaryDTO ) );
                rmSlobSummaryFormulaDetails.setTotalSlowItemValueDTO( getTotalSlowItemValue( rscIotRmSlobSummaryDTO ) );
                rmSlobSummaryFormulaDetails.setTotalObsoleteItemValueDTO( getTotalObsoleteItemValue( rscIotRmSlobSummaryDTO ) );
                rmSlobSummaryFormulaDetails.setTotalStockQualityValueDTO( getTotalStockQualityValue( rscIotRmSlobSummaryDTO ) );
                rmSlobSummaryFormulaDetails.setTotalSlobValue( rscIotRmSlobSummaryDTO.getTotalSlobValue() );
                rmSlobSummaryFormulaDetails.setTotalStockValue( rscIotRmSlobSummaryDTO.getTotalStockValue() );
                rmSlobSummaryFormulaDetails.setSitValue( rscIotRmSlobSummaryDTO.getSitValue() );
                rmSlobSummaryFormulaDetails.setCurrentMonthsTotalSlobValue( rscIotRmSlobSummaryDTO.getTotalSlobValue() );
                rmSlobSummaryFormulaDetails.setPrevMonthsTotalSlobValue( null );
                if (Optional.ofNullable( rmSlobSummaryFormulaDetails.getPrevMonthsTotalSlobValue() ).isPresent()) {
                    rmSlobSummaryFormulaDetails.setInvValue( rmSlobSummaryFormulaDetails.getCurrentMonthsTotalSlobValue() - rmSlobSummaryFormulaDetails.getPrevMonthsTotalSlobValue() );
                }
                rmSlobSummaryFormulaDetails.setCurrentMonth( getCurrentMonthName( rscMtPPheaderDTO.get().getMpsDate() ) );
                rmSlobSummaryFormulaDetails.setPrevMonth( getPrevMonthName( rscMtPPheaderDTO.get().getMpsDate() ) );

                rmSlobSummaryFormulaDetails.setAddProvValue( rscIotRmSlobSummaryDTO.getTotalStockValue() );
                rmSlobSummaryDetails.setSlobSummaryFormulaDetailsDTO( rmSlobSummaryFormulaDetails );
            }
        }
        return rmSlobSummaryDetails;
    }

    private String getCurrentMonthName(LocalDate mpsDate) {
        return mpsDate.getMonth() + " " + (mpsDate.getYear() % 100);
    }

    private String getPrevMonthName(LocalDate mpsDate) {
        return mpsDate.plusMonths( -1 ).getMonth() + " " + (mpsDate.getYear() % 100);
    }

    private TotalStockSitValueDTO getTotalStockSitValue(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        TotalStockSitValueDTO totalStockSitValueDTO = new TotalStockSitValueDTO();
        totalStockSitValueDTO.setTotalStockSitFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalStockSitValue() ) + " = " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getSitValue() ) + " + " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalStockValue() ) );
        totalStockSitValueDTO.setTotalStockSitValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalStockSitValue() ) );
        totalStockSitValueDTO.setTotalStockSitValueFormula( MaterialCategoryConstants.TOTAL_STOCK_SIT_VALUE_FORMULA );
        return totalStockSitValueDTO;
    }

    private TotalSlowItemValueDTO getTotalSlowItemValue(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        TotalSlowItemValueDTO totalSlowItemValueDTO = new TotalSlowItemValueDTO();
        totalSlowItemValueDTO.setTotalSlowItemValueFormula( MaterialCategoryConstants.TOTAL_SLOW_ITEM_VALUE_FORMULA );
        totalSlowItemValueDTO.setSlowItemPercentageFormula( MaterialCategoryConstants.SLOW_ITEM_PERCENTAGE_FORMULA );
        totalSlowItemValueDTO.setTotalSlowItemFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalSlowItemValue() ) + " = " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalSlowItemValue() ) );
        totalSlowItemValueDTO.setTotalSlowItemValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalSlowItemValue() ) );
        totalSlowItemValueDTO.setSlowItemPercentageFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getSlowItemPercentage() ) + " = " + " ( " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalSlowItemValue() ) + " / " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalStockSitValue() ) + " ) " + " * " + 100 );
        totalSlowItemValueDTO.setSlowItemPercentage( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getSlowItemPercentage() ) );
        return totalSlowItemValueDTO;
    }

    private TotalObsoleteItemValueDTO getTotalObsoleteItemValue(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        TotalObsoleteItemValueDTO totalObsoleteItemValueDTO = new TotalObsoleteItemValueDTO();
        totalObsoleteItemValueDTO.setTotalObsoleteItemValueFormula( MaterialCategoryConstants.TOTAL_OBSOLETE_ITEM_VALUE_FORMULA );
        totalObsoleteItemValueDTO.setObsoleteItemPercentageFormula( MaterialCategoryConstants.OBSOLETE_ITEM_PERCENTAGE_FORMULA );
        totalObsoleteItemValueDTO.setTotalObsoleteItemFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalObsoleteItemValue() ) + " = " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalObsoleteItemValue() ) );
        totalObsoleteItemValueDTO.setTotalObsoleteItemValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalObsoleteItemValue() ) );
        totalObsoleteItemValueDTO.setObsoleteItemPercentageFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getObsoleteItemPercentage() ) + " = " + " ( " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalObsoleteItemValue() ) + " / " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalStockSitValue() ) + " ) " + " * " + 100 );
        totalObsoleteItemValueDTO.setObsoleteItemPercentage( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getObsoleteItemPercentage() ) );
        return totalObsoleteItemValueDTO;
    }

    private TotalStockQualityValueDTO getTotalStockQualityValue(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        TotalStockQualityValueDTO totalStockQualityValueDTO = new TotalStockQualityValueDTO();
        totalStockQualityValueDTO.setTotalStockQualityValueFormula( MaterialCategoryConstants.TOTAL_STOCK_QUALITY_VALUE_FORMULA );
        totalStockQualityValueDTO.setStockQualityPercentageFormula( MaterialCategoryConstants.STOCK_QUALITY_PERCENTAGE_FORMULA );
        totalStockQualityValueDTO.setTotalStockQualityFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getStockQualityValue() ) + " = " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getTotalStockSitValue() ) + " * " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getStockQualityPercentage() ) );
        totalStockQualityValueDTO.setStockQuantity( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getStockQualityValue() ) );
        totalStockQualityValueDTO.setStockQualityPercentageFormulaValue( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getStockQualityPercentage() ) + " = " + 100 + " - " + " ( " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getSlowItemPercentage() ) + " + " + DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getObsoleteItemPercentage() ) + " )" );
        totalStockQualityValueDTO.setStockQualityPercentage( DoubleUtils.getFormattedValue( rscIotRmSlobSummaryDTO.getStockQualityPercentage() ) );
        return totalStockQualityValueDTO;
    }

    @Override
    public RMSlobDetailsDTO getNullMapPriceDetails(Long rscMtPPHeaderId) {
        RMSlobDetailsDTO rmSlobDetailsDTO = new RMSlobDetailsDTO();
        Optional<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findOne( rscMtPPHeaderId );
        if (rscMtPPheaderDTO.isPresent()) {
            rmSlobDetailsDTO.setMpsId( rscMtPPheaderDTO.get().getId() );
            rmSlobDetailsDTO.setMpsDate( rscMtPPheaderDTO.get().getMpsDate() );
            rmSlobDetailsDTO.setMpsName( rscMtPPheaderDTO.get().getMpsName() );
            rmSlobDetailsDTO.setRmSlobs( getRmSlobDetails( rscMtPPHeaderId ) );
        }
        return rmSlobDetailsDTO;
    }

    private List<RmSlobDTO> getRmSlobDetails(Long rscMtPPHeaderId) {
        List<RmSlobDTO> rscIotRmSlobDTOS = rmSlobMapper.toDto( rscIotRmSlobRepository.findAllByRscMtPPheaderIdAndPriceIsNull( rscMtPPHeaderId ) );
        List<RmSlobDTO> rmSlobDTOList = new ArrayList<>();
        if (Optional.ofNullable( rscIotRmSlobDTOS ).isPresent()) {
            rmSlobDTOList.addAll( rscIotRmSlobDTOS );
        }
        return rmSlobDTOList;
    }

    private Integer getTotalCoverDays(RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO) {
        return rscIotRmCoverDaysSummaryDTO.getMonth1CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth2CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth3CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth4CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth5CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth6CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth7CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth8CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth9CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth10CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth11CoverDays() + rscIotRmCoverDaysSummaryDTO.getMonth12CoverDays();
    }

    @Override
    public ExcelExportFiles rmSlobExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile( RmSlobExporter.rmSlobToExcel( getAllRscIotRmSlob( rscMtPPheaderDTO.getId() ), monthDetailDTO, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.RM_SLOB + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public List<RscDtReasonDTO> getAllRmSlobsReasons(Long rscMtPPHeaderId) {
        return rscDtReasonService.findAll().stream().filter( rscDtReasonDTO -> rscDtReasonDTO.getReasonType().equals( MaterialCategoryConstants.RM_SLOB_TAG ) || rscDtReasonDTO.getReasonType().equals( MaterialCategoryConstants.NONE_TAG ) ).collect( Collectors.toList() );
    }

    /* @Override
     public List<RmSlobDTO> saveAllRmSlobs(List<RmSlobDTO> rmSlobDTOList, Double sitValue, Long ppHeaderId) {
         List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS = rscIotRmLatestSlobReasonService.findAll();
         RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId(ppHeaderId);
         if (Optional.ofNullable(rmSlobDTOList).isPresent() && rmSlobDTOList.size() > 0) {
             rmSlobDTOList.forEach(rmSlobDTO -> {
                 saveRscIotRmLatestSlob(rmSlobDTO, rscIotRmLatestSlobReasonDTOS);
             });
         }
         List<RscIotRmSlob> rscItPMSlobs = rscIotRmSlobRepository.saveAll(rmSlobMapper.toEntity(rmSlobDTOList));
         if (Optional.ofNullable(rscIotRmSlobSummaryDTO).isPresent()) {
             if (!rscIotRmSlobSummaryDTO.getSitValue().equals(sitValue)) {
                 rscIotRmSlobSummaryService.deleteById(ppHeaderId);
                 calculateRscItRMSlobSummary(ppHeaderId, sitValue);
                 exportRmSlobSummary(ppHeaderId);
             } else {
                 rscIotRmSlobSummaryService.deleteById(ppHeaderId);
                 calculateRscItRMSlobSummary(ppHeaderId, null);
                 exportRmSlobSummary(ppHeaderId);
             }
         }
         recalculationAndExcelCreationOnUpdateService.recalculateCoverDaysAndSummary(ppHeaderId);
         return rmSlobMapper.toDto(rscItPMSlobs);
     }

     private void saveRscIotRmLatestSlob(RmSlobDTO rmSlobDTO, List<RscIotRmLatestSlobReasonDTO> rscIotRmLatestSlobReasonDTOS) {
         List<RscIotRmLatestSlobReasonDTO> iotRmLatestSlobReasonDTOS = rscIotRmLatestSlobReasonDTOS.stream().filter(rscIotPmLatestSlobReasonDTO -> rscIotPmLatestSlobReasonDTO.getRscMtItemId().equals(rmSlobDTO.getRscMtItemId())).collect(Collectors.toList());
         if (iotRmLatestSlobReasonDTOS.size() == 0) {
             RscIotRmLatestSlobReasonDTO rscIotRmLatestSlobReasonDTO = new RscIotRmLatestSlobReasonDTO();
             rscIotRmLatestSlobReasonDTO.setRscMtItemId(rmSlobDTO.getRscMtItemId());
             rscIotRmLatestSlobReasonDTO.setRscDtReasonId(rmSlobDTO.getReasonId());
             rscIotRmLatestSlobReasonDTO.setRemark(rmSlobDTO.getRemark());
             rscIotRmLatestSlobReasonService.saveOne(rscIotRmLatestSlobReasonDTO);
         } else {
             rscIotRmLatestSlobReasonDTOS.forEach(rscIotRmLatestSlobReasonDTO -> {
                 RscIotRmLatestSlobReasonDTO rmLatestSlobReasonDTO = new RscIotRmLatestSlobReasonDTO();
                 rmLatestSlobReasonDTO.setId(rscIotRmLatestSlobReasonDTO.getId());
                 rmLatestSlobReasonDTO.setRscMtItemId(rscIotRmLatestSlobReasonDTO.getRscMtItemId());
                 if (rmSlobDTO.getReasonId() != null) {
                     rmLatestSlobReasonDTO.setRscDtReasonId(rmSlobDTO.getReasonId());
                 }
                 if (rmSlobDTO.getRemark() != null) {
                     rmLatestSlobReasonDTO.setRemark(rmSlobDTO.getRemark());
                 }
                 rscIotRmLatestSlobReasonService.saveOne(rmLatestSlobReasonDTO);
             });
         }
     }
 */
    @Override
    public SlobSummaryDTO getRmSlobSummary(Long ppHeaderId) {
        SlobSummaryDTO slobSummaryDTO = new SlobSummaryDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId ).orElse( null );
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent()) {
            slobSummaryDTO.setMpsId( rscMtPPheaderDTO.getId() );
            slobSummaryDTO.setMpsDate( rscMtPPheaderDTO.getMpsDate() );
            slobSummaryDTO.setMpsName( rscMtPPheaderDTO.getMpsName() );

            slobSummaryDTO.setCurrentMonthName( slobSummaryUtil.getPreviousMonthName( rscMtPPheaderDTO.getMpsDate() ) );
            slobSummaryDTO.setPreviousMonthName( slobSummaryUtil.getPreviousSecondMonthName( rscMtPPheaderDTO.getMpsDate() ) );
            RscIotRmSlobSummaryDTO currentMonthRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId( ppHeaderId );
            if (Optional.ofNullable( currentMonthRmSlobSummaryDTO ).isPresent()) {
                Long previousMonthMpsId = rscMtPpheaderService.getPreviousMonthsFirstCutMpsId( ppHeaderId );
                if (Optional.ofNullable( previousMonthMpsId ).isPresent()) {
                    RscIotRmSlobSummaryDTO previousMonthRmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId( previousMonthMpsId );
                    if (Optional.ofNullable( previousMonthRmSlobSummaryDTO ).isPresent()) {
                        slobSummaryDTO.setTotalObsoleteItemValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalObsoleteItemValue(), previousMonthRmSlobSummaryDTO.getTotalObsoleteItemValue() ) );
                        slobSummaryDTO.setTotalObsoleteExpiryValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalObsoleteExpiryValue(), previousMonthRmSlobSummaryDTO.getTotalObsoleteExpiryValue() ) );
                        slobSummaryDTO.setTotalObsoleteNonExpiryValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalObsoleteNonExpiryValue(), previousMonthRmSlobSummaryDTO.getTotalObsoleteNonExpiryValue() ) );
                        slobSummaryDTO.setTotalSlowItemValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalSlowItemValue(), previousMonthRmSlobSummaryDTO.getTotalSlowItemValue() ) );
                        slobSummaryDTO.setTotalSlobValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalSlobValue(), previousMonthRmSlobSummaryDTO.getTotalSlobValue() ) );
                        slobSummaryDTO.setTotalStockValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalStockValue(), previousMonthRmSlobSummaryDTO.getTotalStockValue() ) );
                        slobSummaryDTO.setObsoleteItemPercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getObsoleteItemPercentage(), previousMonthRmSlobSummaryDTO.getObsoleteItemPercentage() ) );
                        slobSummaryDTO.setSlowItemPercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getSlowItemPercentage(), previousMonthRmSlobSummaryDTO.getSlowItemPercentage() ) );
                        slobSummaryDTO.setStockQualityPercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getStockQualityPercentage(), previousMonthRmSlobSummaryDTO.getStockQualityPercentage() ) );
                        slobSummaryDTO.setStockQualityValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getStockQualityValue(), previousMonthRmSlobSummaryDTO.getStockQualityValue() ) );
                        slobSummaryDTO.setObsoleteExpiryValuePercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getObsoleteExpiryValuePercentage(), previousMonthRmSlobSummaryDTO.getObsoleteExpiryValuePercentage() ) );
                        slobSummaryDTO.setObsoleteNonExpiryValuePercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getObsoleteNonExpiryValuePercentage(), previousMonthRmSlobSummaryDTO.getObsoleteNonExpiryValuePercentage() ) );
                    }
                } else {
                    slobSummaryDTO.setTotalObsoleteItemValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalObsoleteItemValue(), 0.0 ) );
                    slobSummaryDTO.setTotalObsoleteExpiryValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalObsoleteExpiryValue(), 0.0 ) );
                    slobSummaryDTO.setTotalObsoleteNonExpiryValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalObsoleteNonExpiryValue(), 0.0 ) );
                    slobSummaryDTO.setTotalSlobValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalSlobValue(), 0.0 ) );
                    slobSummaryDTO.setTotalSlowItemValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalSlowItemValue(), 0.0 ) );
                    slobSummaryDTO.setTotalStockValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getTotalStockValue(), 0.0 ) );
                    slobSummaryDTO.setObsoleteItemPercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getObsoleteItemPercentage(), 0.0 ) );
                    slobSummaryDTO.setSlowItemPercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getSlowItemPercentage(), 0.0 ) );
                    slobSummaryDTO.setStockQualityPercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getStockQualityPercentage(), 0.0 ) );
                    slobSummaryDTO.setStockQualityValueDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getStockQualityValue(), 0.0 ) );
                    slobSummaryDTO.setObsoleteExpiryValuePercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getObsoleteExpiryValuePercentage(), 0.0 ) );
                    slobSummaryDTO.setObsoleteNonExpiryValuePercentageDetails( slobSummaryUtil.getSlobSummaryValueDetails( currentMonthRmSlobSummaryDTO.getObsoleteNonExpiryValuePercentage(), 0.0 ) );
                }
            }
        }
        return slobSummaryDTO;
    }

    public ExcelExportFiles exportRmSlobSummary(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobSummaryDTO slobSummaryDTO = getRmSlobSummary( rscMtPPheaderDTO.getId() );
        excelExportFiles.setFile( RmSlobSummaryExporter.rmSlobSummaryDetailsToExcel( slobSummaryDTO, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.RM_SLOB_SUMMARY + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public SlobMovementDetailsDTO getRmSlobMovement() {
        SlobMovementDetailsDTO slobMovementDetailsDTO = new SlobMovementDetailsDTO();
        List<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && rscMtPPheaderDTO.size() > 0) {
            slobMovementDetailsDTO.setMpsId(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getId());
            slobMovementDetailsDTO.setMpsDate(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsDate());
            slobMovementDetailsDTO.setMpsName(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsName());
            slobMovementDetailsDTO.setSlobMovementDTOList( slobMovementsUtil.getSlobMovementList( "RM_SLOB" ) );
        }
        return slobMovementDetailsDTO;
    }

    @Override
    public SlobInventoryQualityIndexDetailsDTO getRmSlobInventoryQualityIndex() {
        SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO = new SlobInventoryQualityIndexDetailsDTO();
        List<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent() && rscMtPPheaderDTO.size() > 0) {
            slobInventoryQualityIndexDetailsDTO.setMpsId(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getId());
            slobInventoryQualityIndexDetailsDTO.setMpsDate(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsDate());
            slobInventoryQualityIndexDetailsDTO.setMpsName(rscMtPPheaderDTO.get(rscMtPPheaderDTO.size() - 1).getMpsName());
            slobInventoryQualityIndexDetailsDTO.setSlobInventoryQualityIndexDTOList( slobInventoryQualityIndexUtil.getSlobInventoryQualityIndexList( "RM_SLOB" ) );
        }
        return slobInventoryQualityIndexDetailsDTO;
    }
    @Override
    public ExcelExportFiles exportRmSlobMovementExporter(RscMtPPheaderDTO rscMtPPheaderDTO ) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobMovementDetailsDTO  slobMovementDetailsDTO  = getRmSlobMovement();
        excelExportFiles.setFile( RmSlobMovementExporter.rmslobMovementToExcelList( slobMovementDetailsDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_SLOB_MOVEMENT + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }
    @Override
    public SlobSummaryDashboardDTO getRmSlobSummaryDashboardDTO(Long ppHeaderId) {
        SlobSummaryDashboardDTO slobSummaryDashboardDTO = new SlobSummaryDashboardDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne( ppHeaderId ).orElse( null );
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent()) {
            slobSummaryDashboardDTO = rscIotRmSlobSummaryService.getRmSlobSummaryDashboard( ppHeaderId );
        }
        return slobSummaryDashboardDTO;
    }
    @Override
    public ExcelExportFiles exportRmSlobInventoryQualityIndexExporter(RscMtPPheaderDTO rscMtPPheaderDTO ) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobInventoryQualityIndexDetailsDTO  slobInventoryQualityIndexDetailsDTO  = getRmSlobInventoryQualityIndex();
        excelExportFiles.setFile( RmSlobInventoryQualityExporter.rmSlobInventoryQualityIndexDetailsDTOToExcelList( slobInventoryQualityIndexDetailsDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.RM_INVENTORY_QUALITY_INDEX  + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }
    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotRmSlobRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}