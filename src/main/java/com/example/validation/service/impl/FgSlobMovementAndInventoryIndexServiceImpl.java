package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.SlobInventoryQualityIndexDetailsDTO;
import com.example.validation.dto.SlobMovementDetailsDTO;
import com.example.validation.service.FgSlobMovementAndInventoryIndexService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.SlobInventoryQualityIndexUtil;
import com.example.validation.util.SlobMovementsUtil;
import com.example.validation.util.excel_export.FgSlobInventoryQualityExporter;
import com.example.validation.util.excel_export.FgSlobMovementExporter;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class FgSlobMovementAndInventoryIndexServiceImpl implements FgSlobMovementAndInventoryIndexService {
    private final RscMtPpheaderService rscMtPpheaderService;
    private final SlobMovementsUtil slobMovementsUtil;
    private final SlobInventoryQualityIndexUtil slobInventoryQualityIndexUtil;

    public FgSlobMovementAndInventoryIndexServiceImpl(RscMtPpheaderService rscMtPpheaderService, SlobMovementsUtil slobMovementsUtil, SlobInventoryQualityIndexUtil slobInventoryQualityIndexUtil) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.slobMovementsUtil = slobMovementsUtil;
        this.slobInventoryQualityIndexUtil = slobInventoryQualityIndexUtil;
    }

    @Override
    public SlobMovementDetailsDTO getFgSlobMovement() {
        SlobMovementDetailsDTO slobMovementDetailsDTO = new SlobMovementDetailsDTO();
        List<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent() && rscMtPPheaderDTO.size() > 0) {
            slobMovementDetailsDTO.setMpsId( rscMtPPheaderDTO.get( rscMtPPheaderDTO.size() - 1 ).getId() );
            slobMovementDetailsDTO.setMpsDate( rscMtPPheaderDTO.get( rscMtPPheaderDTO.size() - 1 ).getMpsDate() );
            slobMovementDetailsDTO.setMpsName( rscMtPPheaderDTO.get( rscMtPPheaderDTO.size() - 1 ).getMpsName() );
            slobMovementDetailsDTO.setSlobMovementDTOList( slobMovementsUtil.getSlobMovementList( "FG_SLOB" ) );
        }
        return slobMovementDetailsDTO;
    }

    @Override
    public ExcelExportFiles exportFgSlobMovementExporter(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobMovementDetailsDTO slobMovementDetailsDTO = getFgSlobMovement();
        excelExportFiles.setFile( FgSlobMovementExporter.fgslobMovementToExcelList( slobMovementDetailsDTO, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.FG_SLOB_MOVEMENT + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }

    @Override
    public SlobInventoryQualityIndexDetailsDTO getFgSlobInventoryQualityIndex() {
        SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO = new SlobInventoryQualityIndexDetailsDTO();
        List<RscMtPPheaderDTO> rscMtPPheaderDTO = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        if (Optional.ofNullable( rscMtPPheaderDTO ).isPresent() && rscMtPPheaderDTO.size() > 0) {
            slobInventoryQualityIndexDetailsDTO.setMpsId( rscMtPPheaderDTO.get( rscMtPPheaderDTO.size() - 1 ).getId() );
            slobInventoryQualityIndexDetailsDTO.setMpsDate( rscMtPPheaderDTO.get( rscMtPPheaderDTO.size() - 1 ).getMpsDate() );
            slobInventoryQualityIndexDetailsDTO.setMpsName( rscMtPPheaderDTO.get( rscMtPPheaderDTO.size() - 1 ).getMpsName() );
            slobInventoryQualityIndexDetailsDTO.setSlobInventoryQualityIndexDTOList( slobInventoryQualityIndexUtil.getSlobInventoryQualityIndexList( "FG_SLOB" ) );
        }
        return slobInventoryQualityIndexDetailsDTO;
    }

    @Override
    public ExcelExportFiles exportFgSlobInventoryQualityIndexExporter(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO = getFgSlobInventoryQualityIndex();
        excelExportFiles.setFile( FgSlobInventoryQualityExporter.fgSlobInventoryQualityIndexDetailsDTOToExcelList( slobInventoryQualityIndexDetailsDTO, rscMtPPheaderDTO.getMpsDate() ) );
        excelExportFiles.setFileName( ExcelFileNameConstants.FG_INVENTORY_QUALITY_INDEX + ExcelFileNameConstants.Extension );
        return excelExportFiles;
    }
}
