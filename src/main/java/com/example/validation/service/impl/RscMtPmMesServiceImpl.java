package com.example.validation.service.impl;

import com.example.validation.dto.IndividualValuesMesDTO;
import com.example.validation.dto.RscMtPmMesDTO;
import com.example.validation.mapper.IndividualValuesPmMesMapper;
import com.example.validation.mapper.RscMtPmMesMapper;
import com.example.validation.repository.RscMtPmMesRepository;
import com.example.validation.service.RscMtPmMesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
public class RscMtPmMesServiceImpl implements RscMtPmMesService {
    private final RscMtPmMesMapper rscMtPmMesMapper;
    private final RscMtPmMesRepository rscMtPmMesRepository;
    private final IndividualValuesPmMesMapper individualValuesPmMesMapper;

    public RscMtPmMesServiceImpl(RscMtPmMesRepository rscMtPmMesRepository, RscMtPmMesMapper rscMtPmMesMapper, IndividualValuesPmMesMapper individualValuesPmMesMapper) {
        this.rscMtPmMesRepository = rscMtPmMesRepository;
        this.rscMtPmMesMapper = rscMtPmMesMapper;
        this.individualValuesPmMesMapper = individualValuesPmMesMapper;
    }

    @Override
    public List<RscMtPmMesDTO> findAll() {
        return rscMtPmMesMapper.toDto(rscMtPmMesRepository.findAll());
    }

    @Override
    public List<RscMtPmMesDTO> findAllByStockDate(LocalDate stockDate) {
        return rscMtPmMesMapper.toDto(rscMtPmMesRepository.findAllByStockDate(stockDate));
    }

    @Override
    public List<IndividualValuesMesDTO> findAllIndividualValueDTOByRscMtItemId(Long rscMtItemId) {
        return individualValuesPmMesMapper.toDto(rscMtPmMesRepository.findByRscMtItemId(rscMtItemId));
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscMtPmMesRepository.deleteAllByStockDate(stockDate);
    }
}