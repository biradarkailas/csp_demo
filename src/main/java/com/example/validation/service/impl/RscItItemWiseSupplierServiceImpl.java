package com.example.validation.service.impl;

import com.example.validation.dto.RscDtSupplierDTO;
import com.example.validation.dto.RscItItemWiseSupplierDTO;
import com.example.validation.mapper.RscItItemWiseSupplierMapper;
import com.example.validation.mapper.SupplierMaterialsMapper;
import com.example.validation.repository.RscItItemWiseSupplierRepository;
import com.example.validation.service.RscItItemWiseSupplierService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.ListUtils;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class RscItItemWiseSupplierServiceImpl implements RscItItemWiseSupplierService {
    private final RscItItemWiseSupplierRepository rscItItemWiseSupplierRepository;
    private final RscItItemWiseSupplierMapper rscItItemWiseSupplierMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final SupplierMaterialsMapper supplierMaterialsMapper;

    public RscItItemWiseSupplierServiceImpl(RscItItemWiseSupplierRepository rscItItemWiseSupplierRepository, RscItItemWiseSupplierMapper rscItItemWiseSupplierMapper, RscMtPpheaderService rscMtPpheaderService, SupplierMaterialsMapper supplierMaterialsMapper) {
        this.rscItItemWiseSupplierRepository = rscItItemWiseSupplierRepository;
        this.rscItItemWiseSupplierMapper = rscItItemWiseSupplierMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.supplierMaterialsMapper = supplierMaterialsMapper;
    }

    @Override
    public RscItItemWiseSupplierDTO findOneByRscMtItemIdAndAllocation(Long rscMtItemId) {
        List<RscItItemWiseSupplierDTO> rscItItemWiseSupplierDTOs = rscItItemWiseSupplierMapper.toDto(rscItItemWiseSupplierRepository.findAllByRscMtItemId(rscMtItemId));
        if (Optional.ofNullable(rscItItemWiseSupplierDTOs).isPresent()) {
            // TODO repplace 100 to 1 allocation condition
            Optional<RscItItemWiseSupplierDTO> optionalRscItItemWiseSupplierDTO = rscItItemWiseSupplierDTOs.stream().
                    filter(rscItItemWiseSupplierDTO -> rscItItemWiseSupplierDTO.getAllocationPercentage() == 1)
                    .findAny();
            if (optionalRscItItemWiseSupplierDTO.isPresent())
                return optionalRscItItemWiseSupplierDTO.get();
        }
        return null;
    }

    @Override
    public List<RscItItemWiseSupplierDTO> findByRscMtItemId(Long rscMtItemId, Long ppHeaderId) {
        LocalDate mpsDate = rscMtPpheaderService.findOne(ppHeaderId).get().getMpsDate();
        List<RscItItemWiseSupplierDTO> rscItItemWiseSupplierDTOs = rscItItemWiseSupplierMapper.toDto(rscItItemWiseSupplierRepository.findAllByRscMtItemId(rscMtItemId));
        return rscItItemWiseSupplierDTOs.stream()
                .filter(rscItItemWiseSupplierDTO -> isValidFromDate(mpsDate, rscItItemWiseSupplierDTO.getApplicableFrom())
                        && isValidToDate(mpsDate, rscItItemWiseSupplierDTO.getApplicableTo()))
                .filter(rscItItemWiseSupplierDTO -> rscItItemWiseSupplierDTO.getAllocationPercentage() > 0)
                .collect(Collectors.toList());
    }

    private boolean isValidToDate(LocalDate mpsDate, LocalDate toDate) {
        if (toDate != null && mpsDate != null) {
            return toDate.isAfter(mpsDate);
        }
        return true;
    }

    private boolean isValidFromDate(LocalDate mpsDate, LocalDate fromDate) {
        if (fromDate != null && mpsDate != null) {
            return fromDate.isBefore(mpsDate.plusMonths(11));
        }
        return true;
    }

    @Override
    public List<RscDtSupplierDTO> getSuppliersListByCategory(List<String> category) {
        return supplierMaterialsMapper.toDto(rscItItemWiseSupplierRepository
                .findAllByRscMtItemRscDtItemCategoryDescriptionIn(category)).stream().filter(ListUtils.distinctByKey(rscDtSupplierDTO -> rscDtSupplierDTO.getId())).collect(Collectors.toList());
    }
}