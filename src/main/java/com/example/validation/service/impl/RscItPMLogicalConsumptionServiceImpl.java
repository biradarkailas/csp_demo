package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.mapper.PMLogicalConsumptionMapper;
import com.example.validation.mapper.RscItPMLogicalConsumptionMapper;
import com.example.validation.repository.RscItPMLogicalConsumptionRepository;
import com.example.validation.service.RscItPMLogicalConsumptionService;
import com.example.validation.service.RscMtPpdetailsService;
import com.example.validation.util.consumption.ConsumptionUtil;
import com.example.validation.util.excel_export.PmLogicalConsumptionExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Service
@Transactional
public class RscItPMLogicalConsumptionServiceImpl implements RscItPMLogicalConsumptionService {
    private final RscItPMLogicalConsumptionRepository rscItPmLogicalConsumptionRepository;
    private final RscItPMLogicalConsumptionMapper rscItPmLogicalConsumptionMapper;
    private final PMLogicalConsumptionMapper pmLogicalConsumptionMapper;
    private final ConsumptionUtil consumptionUtil;
    private final RscMtPpdetailsService rscMtPpdetailsService;

    public RscItPMLogicalConsumptionServiceImpl(RscItPMLogicalConsumptionRepository rscItPmLogicalConsumptionRepository, RscItPMLogicalConsumptionMapper rscItPmLogicalConsumptionMapper, PMLogicalConsumptionMapper pmLogicalConsumptionMapper, ConsumptionUtil consumptionUtil, RscMtPpdetailsService rscMtPpdetailsService) {
        this.rscItPmLogicalConsumptionRepository = rscItPmLogicalConsumptionRepository;
        this.rscItPmLogicalConsumptionMapper = rscItPmLogicalConsumptionMapper;
        this.pmLogicalConsumptionMapper = pmLogicalConsumptionMapper;
        this.consumptionUtil = consumptionUtil;
        this.rscMtPpdetailsService = rscMtPpdetailsService;
    }

    @Override
    public List<LogicalConsumptionMainDTO> getPMLogicalConsumptionByFG(Long ppHeaderId, Long fgMtItemId) {
        return pmLogicalConsumptionMapper.toDto(rscItPmLogicalConsumptionRepository.findAllByRscMtPPheaderIdAndRscMtItemMpsParentId(ppHeaderId, fgMtItemId));
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAll() {
        return rscItPmLogicalConsumptionMapper.toDto(rscItPmLogicalConsumptionRepository.findAll());
    }

    @Override
    public List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId) {
        return rscItPmLogicalConsumptionMapper.toDto(rscItPmLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void saveAll(List<RscItLogicalConsumptionDTO> pmRscItLogicalConsumptionDTOS, Long ppHeaderId) {
        if (rscItPmLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            pmRscItLogicalConsumptionDTOS.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemChildCode));
            pmRscItLogicalConsumptionDTOS.sort(Comparator.comparing(RscItLogicalConsumptionDTO::getRscMtItemParentCode));
            rscItPmLogicalConsumptionRepository.saveAll(rscItPmLogicalConsumptionMapper.toEntity(pmRscItLogicalConsumptionDTOS));
        }
    }

    @Override
    public Long count(Long ppHeaderId) {
        return rscItPmLogicalConsumptionRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public Set<Long> getDistinctPMMtItemIds() {
        return consumptionUtil.getDistinctLCMtItemId(rscItPmLogicalConsumptionMapper.toDto(rscItPmLogicalConsumptionRepository.findAll()));
    }

    @Override
    public ExcelExportFiles getPmLogicalConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, Map<Long, ItemDetailDTO> mpsPlanDetailMap) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        excelExportFiles.setFile(PmLogicalConsumptionExporter.logicalConsumptionsToExcel(getPMLogicalConsumption(rscMtPPheaderDTO.getId()), mpsPlanDetailMap, monthDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.PM_LOGICAL_CONSUMPTION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<LogicalConsumptionMainDTO> getPMLogicalConsumption(Long ppHeaderId) {
        return pmLogicalConsumptionMapper.toDto(rscItPmLogicalConsumptionRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscItPmLogicalConsumptionRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}