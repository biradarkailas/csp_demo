package com.example.validation.service.impl;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscIotFGStockDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.slob.FGSlobDTO;
import com.example.validation.dto.slob.FGSlobTotalValueDetailDTO;
import com.example.validation.dto.slob.RscIotFGSlobDTO;
import com.example.validation.mapper.FGSlobMapper;
import com.example.validation.mapper.RscIotFGSlobMapper;
import com.example.validation.repository.RscIotFGSlobRepository;
import com.example.validation.service.RscIotFGSlobService;
import com.example.validation.service.RscIotFGStockService;
import com.example.validation.util.FGSlobUtil;
import com.example.validation.util.excel_export.FgSlobSaturationExporter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class RscIotFGSlobServiceImpl implements RscIotFGSlobService {
    private final RscIotFGSlobRepository rscIotFGSlobRepository;
    private final RscIotFGSlobMapper rscIotFGSlobMapper;
    private final RscIotFGStockService rscIotFGStockService;
    private final FGSlobUtil fgSlobUtil;
    private final FGSlobMapper fgSlobMapper;

    public RscIotFGSlobServiceImpl(RscIotFGSlobRepository rscIotFGSlobRepository, RscIotFGSlobMapper rscIotFGSlobMapper, RscIotFGStockService rscIotFGStockService, FGSlobUtil fgSlobUtil, FGSlobMapper fgSlobMapper) {
        this.rscIotFGSlobRepository = rscIotFGSlobRepository;
        this.rscIotFGSlobMapper = rscIotFGSlobMapper;
        this.rscIotFGStockService = rscIotFGStockService;
        this.fgSlobUtil = fgSlobUtil;
        this.fgSlobMapper = fgSlobMapper;
    }

    @Override
    public void createFGSlob(Long ppHeaderId) {
        if (rscIotFGSlobRepository.countByRscMtPPHeaderId(ppHeaderId) == 0) {
            List<RscIotFGSlobDTO> rscIotFGSlobDTOs = new ArrayList<>();
            for (RscIotFGStockDTO rscIotFGStockDTO : rscIotFGStockService.findAllByStockTypeTagAndMPS(MaterialCategoryConstants.FG_MES_TAG, ppHeaderId)) {
                if (Optional.ofNullable(rscIotFGStockDTO).isPresent())
                    rscIotFGSlobDTOs.add(fgSlobUtil.getRscIotFGSlobDTO(rscIotFGStockDTO, ppHeaderId));
            }
            rscIotFGSlobRepository.saveAll(rscIotFGSlobMapper.toEntity(rscIotFGSlobDTOs));
        }
    }

    @Override
    public List<RscIotFGSlobDTO> getRscIotFGSlobDTO(Long ppHeaderId) {
        return rscIotFGSlobMapper.toDto(rscIotFGSlobRepository.findAllByRscMtPPHeaderId(ppHeaderId));
    }

    @Override
    public FGSlobTotalValueDetailDTO getFGSlobTotalValueDetail(Long ppHeaderId) {
        return fgSlobUtil.getFGSlobTotalValueDetailDTO(rscIotFGSlobMapper.toDto(rscIotFGSlobRepository.findAllByRscMtPPHeaderId(ppHeaderId)), ppHeaderId);
    }

    @Override
    public ExcelExportFiles fgSlobExport(RscMtPPheaderDTO rscMtPPheaderDTO) {
        ExcelExportFiles excelExportFiles = new ExcelExportFiles();
        List<RscIotFGSlobDTO> rscIotFGSlobDTOList = rscIotFGSlobMapper.toDto(rscIotFGSlobRepository.findAllByRscMtPPHeaderId(rscMtPPheaderDTO.getId()));
        FGSlobTotalValueDetailDTO fgSlobTotalValueDetailDTO = fgSlobUtil.getFGSlobTotalValueDetailDTO(rscIotFGSlobDTOList, rscMtPPheaderDTO.getId());
        excelExportFiles.setFile(FgSlobSaturationExporter.slobSaturationToExcel(rscIotFGSlobDTOList, fgSlobTotalValueDetailDTO, rscMtPPheaderDTO.getMpsDate()));
        excelExportFiles.setFileName(ExcelFileNameConstants.FG_SLOB_SATURATION + rscMtPPheaderDTO.getMpsDate() + ExcelFileNameConstants.Extension);
        return excelExportFiles;
    }

    @Override
    public List<FGSlobDTO> getAllFGSlobDTO(Long ppHeaderId) {
        return fgSlobMapper.toDto(rscIotFGSlobRepository.findAllByRscMtPPHeaderId(ppHeaderId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFGSlobRepository.deleteAllByRscMtPPHeaderId(ppHeaderId);
    }

}