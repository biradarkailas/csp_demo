package com.example.validation.service.impl;

import com.example.validation.domain.PartyTypeUser;
import com.example.validation.repository.PartyTypeUserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PartyTypeUserService {
    @Autowired
    PartyTypeUserRepo partyTypeUserRepo;

    @Autowired
    PasswordEncoder passwordEncoder;

    public PartyTypeUser save(PartyTypeUser partyTypeUser) {
        partyTypeUser.setPassword(passwordEncoder.encode(partyTypeUser.getPassword()));
        return partyTypeUserRepo.save(partyTypeUser);
    }

    public List<PartyTypeUser> findAll() {
        return partyTypeUserRepo.findAll();
    }

    public Optional<PartyTypeUser> findById(Long id) {
        return partyTypeUserRepo.findById(id);
    }

    public PartyTypeUser findByUsername(String username) {
        return partyTypeUserRepo.findByUsername(username);
    }

    public List<PartyTypeUser> findAllByAccountEnable(boolean enable) {
        return partyTypeUserRepo.findAllByEnabled(enable);
    }
}
