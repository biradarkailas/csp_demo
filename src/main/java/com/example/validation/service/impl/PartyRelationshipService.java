package com.example.validation.service.impl;

import com.example.validation.domain.Party;
import com.example.validation.domain.PartyRelationship;
import com.example.validation.repository.PartyRelationshipRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PartyRelationshipService {
    @Autowired
    PartyRelationshipRepo partyRelationshipRepo;

    public PartyRelationship save(PartyRelationship partyRelationship) {
        return partyRelationshipRepo.save(partyRelationship);
    }

    public PartyRelationship findByFirstParty(Party firstParty) {
        return partyRelationshipRepo.findByFirstParty(firstParty);
    }
}
