package com.example.validation.service.impl;

import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import com.example.validation.dto.PmCoverDaysSummaryDTO;
import com.example.validation.dto.RscIotPmCoverDaysSummaryDTO;
import com.example.validation.mapper.PmCoverDaysSummaryDetailsMapper;
import com.example.validation.mapper.PmCoverDaysSummaryMapper;
import com.example.validation.mapper.RscIotPmCoverDaysSummaryMapper;
import com.example.validation.repository.RscIotPmCoverDaysSummaryRepository;
import com.example.validation.service.RscIotPmCoverDaysSummaryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class RscIotPmCoverDaysSummaryServiceImpl implements RscIotPmCoverDaysSummaryService {
    private final RscIotPmCoverDaysSummaryRepository rscIotPmCoverDaysSummaryRepository;
    private final RscIotPmCoverDaysSummaryMapper rscIotPmCoverDaysSummaryMapper;
    private final PmCoverDaysSummaryMapper pmCoverDaysSummaryMapper;
    private final PmCoverDaysSummaryDetailsMapper pmCoverDaysSummaryDetailsMapper;

    public RscIotPmCoverDaysSummaryServiceImpl(RscIotPmCoverDaysSummaryRepository rscIotPmCoverDaysSummaryRepository, RscIotPmCoverDaysSummaryMapper rscIotPmCoverDaysSummaryMapper, PmCoverDaysSummaryMapper pmCoverDaysSummaryMapper, PmCoverDaysSummaryDetailsMapper pmCoverDaysSummaryDetailsMapper) {
        this.rscIotPmCoverDaysSummaryRepository = rscIotPmCoverDaysSummaryRepository;
        this.rscIotPmCoverDaysSummaryMapper = rscIotPmCoverDaysSummaryMapper;
        this.pmCoverDaysSummaryMapper = pmCoverDaysSummaryMapper;
        this.pmCoverDaysSummaryDetailsMapper = pmCoverDaysSummaryDetailsMapper;
    }

    @Override
    public void saveCoverDaysSummary(RscIotPmCoverDaysSummaryDTO rscIotPmCoverDaysSummaryDTO) {
        rscIotPmCoverDaysSummaryRepository.save(rscIotPmCoverDaysSummaryMapper.toEntity(rscIotPmCoverDaysSummaryDTO));
    }

    @Override
    public PmCoverDaysSummaryDTO getPmCoverDaysSummaryDetails(Long ppHeaderId) {
        return pmCoverDaysSummaryMapper.toDto(rscIotPmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public CoverDaysSummaryDetailsDTO getPmCoverDaysSummaryAllDetails(Long ppHeaderId) {
        return pmCoverDaysSummaryDetailsMapper.toDto(rscIotPmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public RscIotPmCoverDaysSummaryDTO getPmCoverDaysSummary(Long ppHeaderId) {
        return rscIotPmCoverDaysSummaryMapper.toDto(rscIotPmCoverDaysSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<RscIotPmCoverDaysSummaryDTO> getAllPmCoverDays(Long ppHeaderId) {
        return rscIotPmCoverDaysSummaryMapper.toDto(rscIotPmCoverDaysSummaryRepository.findAll());
    }

    @Override
    public long countByRscMtPPheaderId(Long ppHeaderId) {
        return rscIotPmCoverDaysSummaryRepository.countByRscMtPPheaderId(ppHeaderId);
    }

    @Override
    public void deleteAllById(Long ppHeaderId) {
        rscIotPmCoverDaysSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
