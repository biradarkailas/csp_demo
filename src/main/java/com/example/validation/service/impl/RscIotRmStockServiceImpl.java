package com.example.validation.service.impl;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.domain.RscIotRmStock;
import com.example.validation.dto.RmStockItemCodeDTO;
import com.example.validation.dto.RscIotRmStockDTO;
import com.example.validation.mapper.RmStockItemCodeMapper;
import com.example.validation.mapper.RscIotRmStockMapper;
import com.example.validation.repository.RscIotRmStockRepository;
import com.example.validation.service.RscIotRmStockService;
import com.example.validation.util.DateUtils;
import com.example.validation.util.ListUtils;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotRmStockServiceImpl implements RscIotRmStockService {
    private final RscIotRmStockRepository rscIotRmStockRepository;
    private final RscIotRmStockMapper rscIotRmStockMapper;
    private final RmStockItemCodeMapper rmStockItemCodeMapper;

    public RscIotRmStockServiceImpl(RscIotRmStockRepository rscIotRmStockRepository, RscIotRmStockMapper rscIotRmStockMapper, RmStockItemCodeMapper rmStockItemCodeMapper) {
        this.rscIotRmStockRepository = rscIotRmStockRepository;
        this.rscIotRmStockMapper = rscIotRmStockMapper;
        this.rmStockItemCodeMapper = rmStockItemCodeMapper;
    }

    @Override
    public List<RscIotRmStockDTO> findByRscMtItemIdAndMPSDate(Long rscMtItemId, LocalDate mpsDate) {
        List<RscIotRmStock> allByRscMtItemId = rscIotRmStockRepository.findAllByRscMtItemId(rscMtItemId);
        System.out.println(allByRscMtItemId);
        List<RscIotRmStockDTO> rscIotRmStockDTOList = rscIotRmStockMapper.toDto(allByRscMtItemId);
        System.out.println("\n\n");
        System.out.println(rscIotRmStockDTOList);
        return rscIotRmStockDTOList.stream().filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate, rscIotRmStockDTO.getStockDate())).collect(Collectors.toList());
    }

    @Override
    public List<RscIotRmStockDTO> findByRscMtItemId(Long rscMtItemId, LocalDate mpsDate) {
        // return getMpsDateWiseRMStockDTOs(mpsDate, rscIotRmStockMapper.toDto(rscIotRmStockRepository.findAllByRscMtItemId(rscMtItemId)));
        return rscIotRmStockMapper.toDto(rscIotRmStockRepository.findAllByRscMtItemId(rscMtItemId));
    }

    private List<RscIotRmStockDTO> getMpsDateWiseRMStockDTOs(LocalDate mpsDate, List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        return rscIotRmStockDTOs.stream().filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(rscIotRmStockDTO.getStockDate(), mpsDate)).collect(Collectors.toList());
    }

    @Override
    public List<RscIotRmStockDTO> findAll() {
        return rscIotRmStockMapper.toDto(rscIotRmStockRepository.findAll());
    }

    @Override
    public List<RscIotRmStockDTO> findAllRmStocksByStockTypeTag(List<String> stockTypeList) {
        return rscIotRmStockMapper.toDto(rscIotRmStockRepository.findAllByRscDtStockTypeTagIn(stockTypeList));
    }

    @Override
    public List<RmStockItemCodeDTO> findAllDistinctByRscMtItem() {
        return rmStockItemCodeMapper.toDto(rscIotRmStockRepository.findAll()).stream().filter(ListUtils.distinctByKey(rscIotRmStockDTO -> rscIotRmStockDTO.getRscMtItemId())).collect(Collectors.toList());
    }

    @Override
    public void deleteAll(LocalDate stockDate) {
        rscIotRmStockRepository.deleteAllByStockDate(stockDate);
    }

    @Override
    public List findDataByMpsDateAndStockType(LocalDate mpsDate, String stockType, Integer pageNo, Integer pageSize) {
        Pageable paging = PageRequest.of(pageNo, pageSize);
        String date = mpsDate.getYear() + "" + mpsDate.getMonth().getValue();
        List stockData = new ArrayList<>();
        if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJ)) {
            stockData = rscIotRmStockRepository.findByMpsDateAndStockType(date, stockType, paging);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES)) {
            stockData = rscIotRmStockRepository.findMesByMpsDateAndStockType(date, stockType, paging);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD)) {
            stockData = rscIotRmStockRepository.findRtdByMpsDateAndStockType(date, stockType, paging);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM)) {
            stockData = rscIotRmStockRepository.findRmBulkOpRmByMpsDateAndStockType(date, stockType, paging);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other)) {
            stockData = rscIotRmStockRepository.findRmTransferStockByMpsDateAndStockType(date, stockType, paging);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION)) {
            stockData = rscIotRmStockRepository.findRmRejectionStockByMpsDateAndStockType(date, stockType, paging);
        }
        if (stockData.size() > 0) {
            return stockData;
        } else {
            return new ArrayList<>();
        }
    }

    @Override
    public List findDataByMpsDateAndStockType(LocalDate mpsDate, String stockType) {
        String date = mpsDate.getYear() + "" + mpsDate.getMonth().getValue();
        List stockData = new ArrayList<>();
        if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJ)) {
            stockData = rscIotRmStockRepository.findByMpsDateAndStockType(date, stockType);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES)) {
            stockData = rscIotRmStockRepository.findMesByMpsDateAndStockType(date, stockType);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD)) {
            stockData = rscIotRmStockRepository.findRtdByMpsDateAndStockType(date, stockType);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM)) {
            stockData = rscIotRmStockRepository.findRmBulkOpRmByMpsDateAndStockType(date, stockType);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other)) {
            stockData = rscIotRmStockRepository.findRmTransferStockByMpsDateAndStockType(date, stockType);
        } else if (stockType.equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION)) {
            stockData = rscIotRmStockRepository.findRmRejectionStockByMpsDateAndStockType(date, stockType);
        }
        if (stockData.size() > 0) {
            return stockData;
        } else {
            return new ArrayList<>();
        }
    }


    //    Mes New Api with optimization
    @Override
    public HashMap<Long, RscIotRmStockDTO> findAllByStockDateAndStockType(LocalDate stockDate, String stockType) {
        HashMap<Long, RscIotRmStockDTO> rmStockDTOHashMap = new HashMap<>();
        List<RscIotRmStockDTO> rscIotRmStockDTOList = rscIotRmStockMapper.toDto(rscIotRmStockRepository.findAllByStockDateBetweenAndRscDtStockTypeTag(stockDate.with(TemporalAdjusters.firstDayOfMonth()), stockDate.with(TemporalAdjusters.lastDayOfMonth()), stockType));
        rscIotRmStockDTOList.forEach(rscIotRmStockDTO -> {
            rmStockDTOHashMap.put(rscIotRmStockDTO.getRscMtItemId(), rscIotRmStockDTO);
        });
        return rmStockDTOHashMap;
    }
}