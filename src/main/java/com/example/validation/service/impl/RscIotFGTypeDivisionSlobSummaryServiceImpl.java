package com.example.validation.service.impl;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.FgTypesAndDivisionsDTO;
import com.example.validation.dto.FgTypesDTO;
import com.example.validation.dto.RscDtFGTypeDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.slob.*;
import com.example.validation.mapper.*;
import com.example.validation.repository.RscIotFGTypeDivisionSlobSummaryRepository;
import com.example.validation.service.RscDtFGTypeService;
import com.example.validation.service.RscIotFGSlobService;
import com.example.validation.service.RscIotFGTypeDivisionSlobSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import com.example.validation.util.FGSlobSummaryUtil;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class RscIotFGTypeDivisionSlobSummaryServiceImpl implements RscIotFGTypeDivisionSlobSummaryService {
    private final RscIotFGTypeDivisionSlobSummaryRepository rscIotFGTypeDivisionSlobSummaryRepository;
    private final RscIotFGTypeDivisionSlobSummaryMapper rscIotFGTypeDivisionSlobSummaryMapper;
    private final DivisionWiseFGSlobSummaryDTOMapper divisionWiseFGSlobSummaryDTOMapper;
    private final RscIotFGSlobService rscIotFGSlobService;
    private final FGSlobSummaryUtil fgSlobSummaryUtil;
    private final DivisionWiseFGSlobSummaryDashboardDTOMapper divisionWiseFGSlobSummaryDashboardDTOMapper;
    private final RscDtFGTypeService rscDtFGTypeService;
    private final DivisionWiseFGSlobSummaryIQDashboardMapper divisionWiseFGSlobSummaryIQDashboardMapper;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final FgTypeDivisionMapper fgTypeDivisionMapper;
    private final FGTypeAndDivisionSlobSummaryDashboardMapper fgTypeAndDivisionSlobSummaryDashboardMapper;

    public RscIotFGTypeDivisionSlobSummaryServiceImpl(RscIotFGTypeDivisionSlobSummaryRepository rscIotFGTypeDivisionSlobSummaryRepository, RscIotFGTypeDivisionSlobSummaryMapper rscIotFGTypeDivisionSlobSummaryMapper, DivisionWiseFGSlobSummaryDTOMapper divisionWiseFGSlobSummaryDTOMapper, RscIotFGSlobService rscIotFGSlobService, FGSlobSummaryUtil fgSlobSummaryUtil, DivisionWiseFGSlobSummaryDashboardDTOMapper divisionWiseFGSlobSummaryDashboardDTOMapper, RscDtFGTypeService rscDtFGTypeService, DivisionWiseFGSlobSummaryIQDashboardMapper divisionWiseFGSlobSummaryIQDashboardMapper, RscMtPpheaderService rscMtPpheaderService, FgTypeDivisionMapper fgTypeDivisionMapper, FGTypeAndDivisionSlobSummaryDashboardMapper fgTypeAndDivisionSlobSummaryDashboardMapper) {
        this.rscIotFGTypeDivisionSlobSummaryRepository = rscIotFGTypeDivisionSlobSummaryRepository;
        this.rscIotFGTypeDivisionSlobSummaryMapper = rscIotFGTypeDivisionSlobSummaryMapper;
        this.divisionWiseFGSlobSummaryDTOMapper = divisionWiseFGSlobSummaryDTOMapper;
        this.rscIotFGSlobService = rscIotFGSlobService;
        this.fgSlobSummaryUtil = fgSlobSummaryUtil;
        this.divisionWiseFGSlobSummaryDashboardDTOMapper = divisionWiseFGSlobSummaryDashboardDTOMapper;
        this.rscDtFGTypeService = rscDtFGTypeService;
        this.divisionWiseFGSlobSummaryIQDashboardMapper = divisionWiseFGSlobSummaryIQDashboardMapper;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.fgTypeDivisionMapper = fgTypeDivisionMapper;
        this.fgTypeAndDivisionSlobSummaryDashboardMapper = fgTypeAndDivisionSlobSummaryDashboardMapper;
    }

    @Override
    public void createFGTypeDivisionSlobSummary(Long ppHeaderId) {
        if (rscIotFGTypeDivisionSlobSummaryRepository.countByRscMtPPheaderId(ppHeaderId) == 0) {
            List<RscIotFGTypeDivisionSlobSummaryDTO> rscIotFGTypeDivisionSlobSummaryDTOs = new ArrayList<>();
            List<FGSlobDTO> fgSlobDTOs = rscIotFGSlobService.getAllFGSlobDTO(ppHeaderId);
            if (Optional.ofNullable(fgSlobDTOs).isPresent()) {
                rscIotFGTypeDivisionSlobSummaryDTOs.addAll(fgSlobSummaryUtil.getRscIotFGTypeDivisionSlobSummaryDTOs(ppHeaderId, rscDtFGTypeService.getRscDtFGTypeIdByType(MaterialCategoryConstants.YFG_TYPE), getFGTypeSlobDTOs(fgSlobDTOs, true)));
                rscIotFGTypeDivisionSlobSummaryDTOs.addAll(fgSlobSummaryUtil.getRscIotFGTypeDivisionSlobSummaryDTOs(ppHeaderId, rscDtFGTypeService.getRscDtFGTypeIdByType(MaterialCategoryConstants.ZRM_TYPE), getFGTypeSlobDTOs(fgSlobDTOs, false)));
            }
            rscIotFGTypeDivisionSlobSummaryRepository.saveAll(rscIotFGTypeDivisionSlobSummaryMapper.toEntity(rscIotFGTypeDivisionSlobSummaryDTOs));
        }
    }

    private List<FGSlobDTO> getFGTypeSlobDTOs(List<FGSlobDTO> fgSlobDTOs, Boolean isFGItem) {
        if (isFGItem) {
            return fgSlobDTOs.stream()
                    .filter(fgSlobDTO -> fgSlobDTO.getItemCategory().equals(MaterialCategoryConstants.FG_MATERIAL))
                    .collect(Collectors.toList());
        } else {
            return fgSlobDTOs.stream()
                    .filter(fgSlobDTO -> !fgSlobDTO.getItemCategory().equals(MaterialCategoryConstants.FG_MATERIAL))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<RscIotFGTypeDivisionSlobSummaryDTO> findAll(Long ppHeaderId) {
        return rscIotFGTypeDivisionSlobSummaryMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderId(ppHeaderId));
    }

    @Override
    public List<DivisionWiseFGSlobSummaryDTO> findAllByFGTypeId(Long ppHeaderId, Long fgTypeId) {
        return divisionWiseFGSlobSummaryDTOMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeId(ppHeaderId, fgTypeId));
    }

    @Override
    public DivisionWiseFGSlobSummaryDTO findAllByFGTypeAndDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId) {
        return divisionWiseFGSlobSummaryDTOMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeIdAndRscDtDivisionId(ppHeaderId, fgTypeId, divisionId));
    }

    @Override
    public List<DivisionWiseFGSlobSummaryDashboardDTO> getDivisionFGSlobSummaryDashboardDTO(Long ppHeaderId, Long fgTypeId) {
        return divisionWiseFGSlobSummaryDashboardDTOMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeId(ppHeaderId, fgTypeId));
    }

    @Override
    public List<DivisionWiseFGSlobSummaryIQDashboardDTO> findAllDivisionWiseFGSlobSummaryIQDashboardDTO(Long ppHeaderId, Long fgTypeId) {
        return divisionWiseFGSlobSummaryIQDashboardMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeId(ppHeaderId, fgTypeId));
    }

    @Override
    public FgTypesAndDivisionsDTO getFgTypesAndDivisionsData(Long ppHeaderId) {
        FgTypesAndDivisionsDTO fgTypesAndDivisionsDTO = new FgTypesAndDivisionsDTO();
        RscMtPPheaderDTO rscMtPPheaderDTO = rscMtPpheaderService.findOne(ppHeaderId).orElse(null);
        if (Optional.ofNullable(rscMtPPheaderDTO).isPresent()) {
            List<RscIotFGTypeDivisionSlobSummaryDTO> fgTypeDivisionSlobSummaryDTOS = findAll(ppHeaderId);
            List<RscDtFGTypeDTO> fgTypeDTOS = rscDtFGTypeService.findAll();
            List<FgTypesDTO> fgTypesDTOList = new ArrayList<>();
            fgTypeDTOS.forEach(rscDtFGTypeDTO -> {
                if (rscDtFGTypeDTO.getType().equals(MaterialCategoryConstants.ALL)) {
                    fgTypesAndDivisionsDTO.setId(rscDtFGTypeDTO.getId());
                    fgTypesAndDivisionsDTO.setName(rscDtFGTypeDTO.getType());
                } else {
                    Optional<RscIotFGTypeDivisionSlobSummaryDTO> iotFGTypeDivisionSlobSummaryDTO = fgTypeDivisionSlobSummaryDTOS.stream().filter(rscIotFGTypeDivisionSlobSummaryDTO -> rscIotFGTypeDivisionSlobSummaryDTO.getFgTypeId().equals(rscDtFGTypeDTO.getId()) && rscIotFGTypeDivisionSlobSummaryDTO.getRscMtPPheaderId().equals(ppHeaderId)).findAny();
                    if (iotFGTypeDivisionSlobSummaryDTO.isPresent()) {
                        fgTypesDTOList.add(getFgTypeWithDivsions(rscDtFGTypeDTO, ppHeaderId));
                    }
                }
            });
            fgTypesAndDivisionsDTO.setChilds(fgTypesDTOList);
        }
        return fgTypesAndDivisionsDTO;
    }

    private FgTypesDTO getFgTypeWithDivsions(RscDtFGTypeDTO rscDtFGTypeDTO, Long ppHeaderId) {
        FgTypesDTO fgTypesDTO = new FgTypesDTO();
        fgTypesDTO.setId(rscDtFGTypeDTO.getId());
        fgTypesDTO.setName(rscDtFGTypeDTO.getType());
        fgTypesDTO.setUniqueName(MaterialCategoryConstants.ALL + "-" + rscDtFGTypeDTO.getType());
        fgTypesDTO.setChilds(getDivisionDTOList(ppHeaderId, rscDtFGTypeDTO));
        return fgTypesDTO;
    }

    private List<FgTypesDTO> getDivisionDTOList(Long ppHeaderId, RscDtFGTypeDTO rscDtFGTypeDTO) {
        List<FgTypesDTO> rscDtDivisionDTOS = new ArrayList<>();
        rscDtDivisionDTOS.addAll(fgTypeDivisionMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeId(ppHeaderId, rscDtFGTypeDTO.getId())));
        rscDtDivisionDTOS.forEach(fgTypesDTO -> {
            fgTypesDTO.setParentId(rscDtFGTypeDTO.getId());
            fgTypesDTO.setUniqueName(rscDtFGTypeDTO.getType() + "-" + fgTypesDTO.getName());
        });
        return rscDtDivisionDTOS;
    }

    @Override
    public FGSlobSummaryDashboardDTO findByFGTypeAndDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId) {
        return fgTypeAndDivisionSlobSummaryDashboardMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeIdAndRscDtDivisionId(ppHeaderId, fgTypeId, divisionId));
    }

    @Override
    public RscIotFGTypeDivisionSlobSummaryDTO findByDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId) {
        return rscIotFGTypeDivisionSlobSummaryMapper.toDto(rscIotFGTypeDivisionSlobSummaryRepository.findAllByRscMtPPheaderIdAndRscDtFGTypeIdAndRscDtDivisionId(ppHeaderId, fgTypeId, divisionId));
    }

    @Override
    public void deleteAll(Long ppHeaderId) {
        rscIotFGTypeDivisionSlobSummaryRepository.deleteAllByRscMtPPheaderId(ppHeaderId);
    }
}
