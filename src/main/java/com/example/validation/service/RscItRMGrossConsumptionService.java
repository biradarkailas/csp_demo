package com.example.validation.service;

import com.example.validation.dto.*;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;

import java.util.List;

public interface RscItRMGrossConsumptionService {
    void saveAll(List<RscItGrossConsumptionDTO> rmRscItGrossConsumptionDTOs, Long ppHeaderId);

    GrossConsumptionMainDTO getAllRMGrossConsumption(Long ppHeaderId);

    List<GrossConsumptionDTO> getRmGrossConsumption(Long ppHeaderId);

    List<RscItGrossConsumptionDTO> getRscItGrossConsumptionDTO(Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllRMGrossConsumptionByRscMtPPHeaderId(Long ppHeaderId);

    ExcelExportFiles getRmGrossConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long rscMtItemId);

    SlobTotalConsumptionDetailsDTO getRmSlobTotalConsumptionDetailsDTO(Long ppHeaderId, Long rscMtItemId);

    Long count(Long ppHeaderId);

    List<ConsumptionPivotDTO> findAllByRscMtPPHeaderId(Long ppHeaderId);

    RscItGrossConsumptionDTO findByGrossConsumptionId(Long grossConsumptionId);

    void deleteAll(Long ppHeaderId);
}