package com.example.validation.service;

import com.example.validation.dto.RscIitMpsForecastDTO;

import java.util.List;

public interface RscIitMpsForecastService {
    RscIitMpsForecastDTO findByFGItemIdAndPPHeaderId(Long fgItemId, Long ppHeaderId);

    List<RscIitMpsForecastDTO> findByBpiCodeAndPPHeaderId(String bpiCode, Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}