package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;

import java.util.List;
import java.util.Map;

public interface RscItRMLogicalConsumptionService {
    void saveAll(List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOS, Long ppHeaderId);

    void saveAllUnique(List<RscItLogicalConsumptionDTO> rmRscItLogicalConsumptionDTOS, Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getRMLogicalConsumptionByFG(Long ppHeaderId, Long bulkItemId);

    List<RscItLogicalConsumptionDTO> findAllLogicalConsumptionDTOsByItemCategory(String itemDescription);

    List<RscItLogicalConsumptionDTO> findAll();

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

    ExcelExportFiles getRmLogicalConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO,   Map<Long, GrossConsumptionDTO>  grossConsumptionMap );

    ExcelExportFiles getBulkLogicalConsumptionExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, Map<Long, ItemDetailDTO> mpsPlanDetailMap);

    Long count(Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getRMLogicalConsumption(Long ppHeaderId);

    void deleteConsumption(Long ppHeaderId);
}