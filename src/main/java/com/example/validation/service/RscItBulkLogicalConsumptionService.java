package com.example.validation.service;

import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;

import java.util.List;

public interface RscItBulkLogicalConsumptionService {

    void saveAll(List<RscItLogicalConsumptionDTO> bulkRscItLogicalConsumptionDTOs, Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getBulkLogicalConsumptionByFG(Long ppHeaderId, Long fgMtItemId);

    List<RscItLogicalConsumptionDTO> findAllLogicalConsumptionDTOsByItemCategory(String itemDescription);

    List<RscItLogicalConsumptionDTO> findAll();

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderId(Long ppHeaderId);

    Long count(Long ppHeaderId);

    List<LogicalConsumptionMainDTO> getBulkLogicalConsumption(Long ppHeaderId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPheaderIdAndItemId(Long ppHeaderId, Long itemId);

    void deleteAll(Long ppHeaderId);
}