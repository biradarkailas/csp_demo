package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtMouldDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface RscDtMouldService {
    List<RscDtMouldDTO> findAll();

    List<RscDtMouldDTO> findPmMouldWithPagination(Integer pageNo, Integer pageSize);

    ExcelExportFiles pmMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO);
}