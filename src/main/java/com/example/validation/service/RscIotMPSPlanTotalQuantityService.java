package com.example.validation.service;

import com.example.validation.dto.*;
import com.example.validation.specification.RscMtPPDetailsCriteria;

public interface RscIotMPSPlanTotalQuantityService {
    MPSDeltaAnalysisMainDTO getFilteredMPSDeltaAnalysis(RscMtPPDetailsCriteria criteria);

    ExcelExportFiles exportPmMPSTriangleAnalysisExporter(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO, MpsPlanMainDTO mpsPlanMainDTO);
}