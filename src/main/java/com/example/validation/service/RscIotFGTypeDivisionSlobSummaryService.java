package com.example.validation.service;

import com.example.validation.dto.FgTypesAndDivisionsDTO;
import com.example.validation.dto.slob.*;

import java.util.List;

public interface RscIotFGTypeDivisionSlobSummaryService {
    void createFGTypeDivisionSlobSummary(Long ppHeaderId);

    List<RscIotFGTypeDivisionSlobSummaryDTO> findAll(Long ppHeaderId);

    List<DivisionWiseFGSlobSummaryDTO> findAllByFGTypeId(Long ppHeaderId, Long fgTypeId);

    DivisionWiseFGSlobSummaryDTO findAllByFGTypeAndDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId);

    List<DivisionWiseFGSlobSummaryIQDashboardDTO> findAllDivisionWiseFGSlobSummaryIQDashboardDTO(Long ppHeaderId, Long fgTypeId);

    List<DivisionWiseFGSlobSummaryDashboardDTO> getDivisionFGSlobSummaryDashboardDTO(Long ppHeaderId, Long fgTypeId);

    FgTypesAndDivisionsDTO getFgTypesAndDivisionsData(Long ppHeaderId);

    FGSlobSummaryDashboardDTO findByFGTypeAndDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId);

    RscIotFGTypeDivisionSlobSummaryDTO findByDivisionId(Long ppHeaderId, Long fgTypeId, Long divisionId);

    void deleteAll(Long ppHeaderId);
}