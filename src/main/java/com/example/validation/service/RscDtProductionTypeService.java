package com.example.validation.service;

import com.example.validation.dto.RscDtProductionTypeDTO;

import java.util.List;

public interface RscDtProductionTypeService {
    List<RscDtProductionTypeDTO> findAll();
}
