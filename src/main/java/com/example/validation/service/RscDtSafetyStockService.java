package com.example.validation.service;

import com.example.validation.dto.RscDtSafetyStockDTO;

import java.util.List;

public interface RscDtSafetyStockService {
    List<RscDtSafetyStockDTO> findAll();
}
