package com.example.validation.service;

import com.example.validation.dto.RscIotFGStockDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscIotFGStockService {
    List<RscIotFGStockDTO> findAllByStockTypeTagAndMPS(String stockTypeTag, Long ppHeaderId);

    void deleteAll(LocalDate stockDate);
}