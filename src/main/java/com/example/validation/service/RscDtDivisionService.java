package com.example.validation.service;

import com.example.validation.dto.RscDtDivisionDTO;

import java.util.List;

public interface RscDtDivisionService {
    List<RscDtDivisionDTO> findAll();
}
