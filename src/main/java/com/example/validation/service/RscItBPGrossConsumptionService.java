package com.example.validation.service;

import com.example.validation.dto.ItemCodeDetailDTO;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;

import java.util.List;

public interface RscItBPGrossConsumptionService {
    void saveAll(List<RscItGrossConsumptionDTO> bpRscItGrossConsumptionDTOs, Long ppHeaderId);

    List<GrossConsumptionDTO> getAllBPGrossConsumption(Long ppHeaderId);

    List<ItemCodeDetailDTO> getBPItemCodeDetailDTO(Long ppHeaderId);

    List<RscItGrossConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId);

    RscItGrossConsumptionDTO findByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId);

    Long count(Long ppHeaderId);

    List<Long> findAllItemIdByRscMtPPHeaderId(Long ppHeaderId);

    void deleteByRscMtPPheaderIdAndRscMtItemId(Long ppHeaderId, Long bpItemId);

    void save(RscItGrossConsumptionDTO rscItGrossConsumptionDTO);

    void deleteAll(Long ppHeaderId);
}