package com.example.validation.service;

import com.example.validation.dto.RscDtItemCategoryDTO;

import java.util.List;

public interface RscDtItemCategoryService {
    List<RscDtItemCategoryDTO> findAll();
}
