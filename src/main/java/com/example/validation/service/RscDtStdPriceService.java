package com.example.validation.service;

import com.example.validation.dto.RscDtStdPriceDTO;

import java.util.List;

public interface RscDtStdPriceService {
    List<RscDtStdPriceDTO> findAll();
}
