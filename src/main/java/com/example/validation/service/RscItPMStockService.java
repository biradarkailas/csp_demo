package com.example.validation.service;

import com.example.validation.dto.PmStockItemCodeDTO;
import com.example.validation.dto.RscItPMStockDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscItPMStockService {
    List<RscItPMStockDTO> findByRscMtItemId(Long rscMtItemId, LocalDate mpsDate);

    List<RscItPMStockDTO> findAll();

    List<PmStockItemCodeDTO> findAllDistinctByRscMtItem();

    List<RscItPMStockDTO> findAllRscItPmStock();

    List<RscItPMStockDTO> findAllRscItPmStockByStockDate(LocalDate mpsDate);

    List<RscItPMStockDTO> findAllRscItPmStockByStockTypeTag(List<String> stockTypeList);

    void deleteAll(LocalDate stockDate);
}