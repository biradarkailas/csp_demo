package com.example.validation.service;

import java.time.LocalDate;

public interface DeleteOldPlansService {
    void deleteAllOldPlans(LocalDate mpsDate);
}