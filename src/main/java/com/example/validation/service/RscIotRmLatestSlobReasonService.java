package com.example.validation.service;

import com.example.validation.dto.RscIotRmLatestSlobReasonDTO;

import java.util.List;

public interface RscIotRmLatestSlobReasonService {
    List<RscIotRmLatestSlobReasonDTO> findAll();

    void saveOne(RscIotRmLatestSlobReasonDTO rmLatestSlobReasonDTO);
}
