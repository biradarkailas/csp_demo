package com.example.validation.service;

import com.example.validation.dto.RscMtFgMesDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtFgMesService {
    List<RscMtFgMesDTO> findAll();

    void deleteAll(LocalDate stockDate);
}
