package com.example.validation.service;

import com.example.validation.dto.RscDtMaterialSubCategoryDTO;

import java.util.List;

public interface RscDtMaterialSubCategoryService {
    List<RscDtMaterialSubCategoryDTO> findAll();

}
