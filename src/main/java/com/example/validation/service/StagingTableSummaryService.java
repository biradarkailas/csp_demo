package com.example.validation.service;

import com.example.validation.dto.StagingTableSummaryDTO;

import java.time.LocalDate;
import java.util.List;

public interface StagingTableSummaryService {

    void save(StagingTableSummaryDTO stagingTableSummaryDTO);

    void saveAll(List<StagingTableSummaryDTO> stagingTableSummaryList);

    List<StagingTableSummaryDTO> findAllByTableNames(List<String> sanitizeTableNames);

    List<StagingTableSummaryDTO> getAllStagingTableSummary(List<String> tableNames);

    List<StagingTableSummaryDTO> findAllByTableNamesAndIsValidate(List<String> sanitizeTableNames);

    Boolean isValidStagingTableSummaryCount();

    List<String> findAllTableNamesByValidate();

    Boolean isValidStagingTableSummaryByTableName(String tableName);

    List<StagingTableSummaryDTO> findAllByIsActive();

    StagingTableSummaryDTO findByIsActiveAndTableName(String tableName);

    void inActiveAllStagingTableSummary();

    void deleteAll(LocalDate mpsDate);
}