package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.PurchasePlanCoverDaysEquationDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCoverDaysDTO;

import java.util.List;

public interface RscIotRMPurchasePlanCoverDaysService {
    void calculateRMPurchasePlanCoverDays(Long ppHeaderId);

    List<RscIotRMPurchasePlanCoverDaysDTO> getAllRMPurchasePlanCoverDays(Long ppHeaderId);

    List<PurchasePlanCoverDaysEquationDTO> getPurchasePlanCoverDaysEquationDTOs(Long ppHeaderId);

    ExcelExportFiles rmPurchaseCoverdaysExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    ExcelExportFiles rmPurchaseStockEquationExporte(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void deleteAll(Long ppHeaderId);
}
