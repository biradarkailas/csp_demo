package com.example.validation.service;

public interface GrossConsumptionService {

    void createWIPGrossConsumption(Long ppHeaderId);

    void createGrossConsumption(Long ppHeaderId);

    void createBPGrossConsumption(Long ppHeaderId);

    void createRMGrossConsumption(Long ppHeaderId);

    void createSubBaseGrossConsumption(Long ppHeaderId);
}