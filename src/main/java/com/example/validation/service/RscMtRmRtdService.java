package com.example.validation.service;

import com.example.validation.dto.IndividualValuesRtdDTO;
import com.example.validation.dto.RscMtRmRtdDto;
import com.example.validation.dto.RscMtRmRtdExportDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscMtRmRtdService {
    List<RscMtRmRtdDto> findAllByRscMtItemId(Long rscMtItemId);

    List<IndividualValuesRtdDTO> findAllIndividualValueDTORscRtdItemId(Long rscMtItemId);

    List<RscMtRmRtdExportDTO> findAll();

    void deleteAll(LocalDate receiptDate);
}