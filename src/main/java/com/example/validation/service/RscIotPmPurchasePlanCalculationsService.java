package com.example.validation.service;

import com.example.validation.dto.supply.PmSupplyCalculationsDTO;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanDTO;

import java.util.List;

public interface RscIotPmPurchasePlanCalculationsService {
    Boolean createPMSupplyCalculations(Long ppHeaderId);

    List<RscIotPMPurchasePlanDTO> getPMSupplyCalculationsDTOByPPheaderId(Long ppHeaderId);

    PmSupplyCalculationsDTO getPMSupplyCalculations(Long rscMtItemId, Long ppHeaderId);

    List<PurchasePlanCalculationsMainDTO> findAll(Long ppHeaderId);

    void deleteAll(Long ppHeaderId);
}