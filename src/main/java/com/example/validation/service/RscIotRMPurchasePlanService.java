package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.RMPurchasePlanMainDTO;

public interface RscIotRMPurchasePlanService {
    RMPurchasePlanMainDTO getRMPurchasePlan(Long ppHeaderId);

    Boolean createRMPurchasePlan(Long ppHeaderId);

//    List<RMPurchasePlanDTO> saveAll(List<RMPurchasePlanDTO> rmPurchasePlanDTOs);

    ExcelExportFiles getRmPurchasePlanExport(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void deleteAll(Long ppHeaderId);
}