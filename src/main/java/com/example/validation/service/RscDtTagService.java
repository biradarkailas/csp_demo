package com.example.validation.service;

import com.example.validation.dto.RscDtTagDTO;

import java.util.List;

public interface RscDtTagService {
    List<RscDtTagDTO> findAll();
}
