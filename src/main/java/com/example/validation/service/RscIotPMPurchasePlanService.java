package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.supply.PurchasePlanMainDTO;

public interface RscIotPMPurchasePlanService {
    Boolean createPMPurchasePlan(Long ppHeaderId);

    /*List<PurchasePlanDTO> saveAll(List<PurchasePlanDTO> purchasePlanDTOs);*/

    PurchasePlanMainDTO getPMOriginalPurchasePlan(Long ppHeaderId);

    PurchasePlanMainDTO getPMLatestPurchasePlan(Long ppHeaderId);

    ExcelExportFiles purchasePlanToExcel(RscMtPPheaderDTO rscMtPPheaderDTO, MonthDetailDTO monthDetailDTO);

    void deleteAll(Long ppHeaderId);
}