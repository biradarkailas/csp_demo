package com.example.validation.service;

import com.example.validation.dto.slob.*;

import java.util.List;

public interface RscIotFGTypeSlobSummaryService {

    void createTypeWiseFGSlobSummary(Long ppHeaderId);

    List<RscIotFGTypeSlobSummaryDTO> findAll(Long ppHeaderId);

    List<TypeWiseFGSlobSummaryDTO> findAllMaterialWiseFGSlobSummaryDTO(Long ppHeaderId);

    List<TypeWiseFGSlobDeviationDTO> getDivisionWiseFGSlobDeviation(Long ppHeaderId);

    MaterialWiseFGSlobSummaryDashboardDTO getMaterialWiseFGSlobSummaryDashboardDTO(Long ppHeaderId);

    List<DivisionWiseFGSlobSummaryIQDashboardDTO> findAllMaterialWiseFGSlobSummaryIQDashboardDTO(Long ppHeaderId);

    RscIotFGTypeSlobSummaryDTO findByPPHeaderIdAndTypeId(Long ppHeaderId, Long typeId);

    DivisionFGSlobSummaryDashboardDTO getDivisionFGSlobSummaryDashboardDTO(Long ppHeaderId, Long fgTypeId);

    FGSlobSummaryDashboardDTO findSlobSummaryByPPHeaderIdAndTypeId(Long ppHeaderId, Long typeId);

    void deleteAll(Long ppHeaderId);
}