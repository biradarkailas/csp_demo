package com.example.validation.service;

import com.example.validation.dto.BulkOpRmExportDTO;
import com.example.validation.dto.IndividualValuesBulkOpRmDTO;
import com.example.validation.dto.RscIitBulkRMStockDTO;
import com.example.validation.dto.RscIitRmBulkOpRmDTO;

import java.time.LocalDate;
import java.util.List;

public interface RscIitRmBulkOpRmService {
    List<RscIitRmBulkOpRmDTO> findAllByRscMtItemId(Long rscMtItemId);

    List<IndividualValuesBulkOpRmDTO> findAllIndivisualValueDTORscMtItemId(Long rscMtItemId);

    List<RscIitBulkRMStockDTO> findAllByStockDate(LocalDate mpsDate);

    void deleteAll(LocalDate stockDate);
}