package com.example.validation.service;

import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;

import java.util.List;
import java.util.Set;

public interface RscItSubBaseLogicalConsumptionService {

    void saveAll(List<RscItLogicalConsumptionDTO> subBaseRscItLogicalConsumptionDTOs);

    Long count(Long ppHeaderId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderId(Long ppHeaderId);

    Set<Long> findAllItemIdByRscMtPPHeaderId(Long ppHeaderId);

    List<RscItLogicalConsumptionDTO> findAllByRscMtPPHeaderIdAndChildItemId(Long ppHeaderId, Long subBaseItemId);

    void deleteByRscMtPPheaderIdAndRscMtParentItemId(Long ppHeaderId, Long bpParentItemId);

    void deleteAll(Long ppHeaderId);
}