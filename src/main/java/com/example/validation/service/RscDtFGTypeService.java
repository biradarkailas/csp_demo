package com.example.validation.service;

import com.example.validation.dto.RscDtFGTypeDTO;

import java.util.List;

public interface RscDtFGTypeService {
    Long getRscDtFGTypeIdByType(String type);

    List<RscDtFGTypeDTO> findAll();

    RscDtFGTypeDTO findById(Long id);
}