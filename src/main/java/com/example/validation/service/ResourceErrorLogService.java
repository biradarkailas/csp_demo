package com.example.validation.service;

import com.example.validation.dto.ResourceErrorLogDTO;

import java.util.List;

public interface ResourceErrorLogService {
    List<ResourceErrorLogDTO> findAll();

    void deleteAll();
}
