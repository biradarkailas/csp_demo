package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.MaterialMasterDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface MaterialMastersService {
    List<MaterialMasterDTO> getMaterialMastersListOfPackingMaterial();

    List<MaterialMasterDTO> getMaterialMastersListOfRawMaterial();

    List<MaterialMasterDTO> getMaterialMastersListOfFgMaterial();

    List<MaterialMasterDTO> getMaterialMastersListOfBulkMaterial();

    List<MaterialMasterDTO> getMaterialMastersListOfSubBasesMaterial();

    List<MaterialMasterDTO> getMaterialMastersListOfWipMaterial();

    ExcelExportFiles fgMaterialMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles fgWipMaterialMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles rmMaterialMasterDataExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles rmBulkMaterialMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles rmSubBasesMaterialMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    ExcelExportFiles pmMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    List<MaterialMasterDTO> findMaterialMastersListOfPackingMaterialWithPagination(Integer pageNo, Integer pageSize);

    List<MaterialMasterDTO> findMaterialMastersListOfRawMaterialWithPagination(Integer pageNo, Integer pageSize);

    List<MaterialMasterDTO> findMaterialMastersListOfFgMaterialWithPagination(Integer pageNo, Integer pageSize);

    List<MaterialMasterDTO> findMaterialMastersListOfBulkMaterialWithPagination(Integer pageNo, Integer pageSize);

    List<MaterialMasterDTO> findMaterialMastersListOfSubBasesMaterialWithPagination(Integer pageNo, Integer pageSize);

    List<MaterialMasterDTO> findMaterialMastersListOfWipMaterialWithPagination(Integer pageNo, Integer pageSize);
}
