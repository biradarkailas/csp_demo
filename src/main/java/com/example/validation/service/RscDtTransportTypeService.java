package com.example.validation.service;

import com.example.validation.dto.RscDtTransportTypeDTO;

import java.util.List;

public interface RscDtTransportTypeService {
    List<RscDtTransportTypeDTO> findAll();
}
