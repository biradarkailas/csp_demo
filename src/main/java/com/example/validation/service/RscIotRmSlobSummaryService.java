package com.example.validation.service;

import com.example.validation.dto.RscIotRmSlobSummaryDTO;
import com.example.validation.dto.SlobSummaryDashboardDTO;

public interface RscIotRmSlobSummaryService {
    void saveRmSlobSummary(RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO);

    RscIotRmSlobSummaryDTO findByRscMtPPheaderId(Long rscMtPPHeaderId);

    Long countByRscMtPPheaderId(Long rscMtPPHeaderId);

    void deleteById(Long ppHeaderId);

    SlobSummaryDashboardDTO getRmSlobSummaryDashboard(Long ppHeaderId);
}
