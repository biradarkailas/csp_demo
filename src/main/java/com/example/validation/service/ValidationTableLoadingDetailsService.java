package com.example.validation.service;

import com.example.validation.dto.ValidationTableLoadingDetailsDTO;

import java.util.List;

public interface ValidationTableLoadingDetailsService {
    List<ValidationTableLoadingDetailsDTO> findAll();

    void deleteAll();
}
