package com.example.validation.service;

import com.example.validation.dto.ExcelExportFiles;
import com.example.validation.dto.RscDtItemTypeDTO;
import com.example.validation.dto.RscMtPPheaderDTO;

import java.util.List;

public interface MaterialCategoryMasterService {
    List<RscDtItemTypeDTO> getMaterialCategoryListOfPackingMaterial();

    ExcelExportFiles pmMasterDataMouldExport(RscMtPPheaderDTO rscMtPPheaderDTO);

    List getMaterialCategoryListOfPackingMaterialWithPagination(Integer pageNo, Integer pageSize);
}
