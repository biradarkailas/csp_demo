package com.example.validation.service;

import com.example.validation.dto.RscMtOtifPmReceiptsHeaderDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface RscMtOtifPmReceiptsHeaderService {
    Optional<RscMtOtifPmReceiptsHeaderDTO> findOne(Long id);

    RscMtOtifPmReceiptsHeaderDTO findByOtifDate(LocalDate otifDate);

    RscMtOtifPmReceiptsHeaderDTO getLatestReceiptHeaderId(LocalDate mpsDate);

    List<LocalDate> getPreviousMonthAllOtifDate();

    void deleteAll(List<Long> ids);

    List<Long> getAllIds(LocalDate otifDate);
}
