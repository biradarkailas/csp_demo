package com.example.validation.service;

import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.dto.supply.RMPurchasePlanDTO;

import java.util.List;

public interface RecalculationAndExcelCreationOfPurchasePlanService {
    List<PurchasePlanDTO> saveAll(List<PurchasePlanDTO> purchasePlanDTOs);

    List<RMPurchasePlanDTO> saveAllRmPurchasePlan(List<RMPurchasePlanDTO> rmPurchasePlanDTOs);

    String createPmPurchasePlanExportOnUpdate(Long ppHeaderId);

    String createRmPurchasePlanExportOnUpdate(Long ppHeaderId);
}
