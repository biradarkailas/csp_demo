package com.example.validation.service;

import com.example.validation.dto.FileUploadStatusDTO;
import com.example.validation.dto.MpsPlanViewDto;
import com.example.validation.dto.RscIitIdpStatusDTO;
import com.example.validation.util.enums.ModuleName;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.time.LocalDate;
import java.util.List;

public interface RscIitIdpStatusService {

    List<RscIitIdpStatusDTO> findRscIitIdpStatusByName(String name);

    Boolean mpsNameExists(String name);

    void save(RscIitIdpStatusDTO rscIitIdpStatusDTO);

    RscIitIdpStatusDTO findIdpStatusDTOByMpsNameAndFileName(String mpsName, String fileName);

    MpsPlanViewDto saveAll(MpsPlanViewDto mpsPlanViewDto);

    MpsPlanViewDto getCurrentMpsPlan();

    Boolean isPlanAvailable();

    FileUploadStatusDTO uploadFile(MultipartFile file, HttpSession session, Long idpStatusId);

    MpsPlanViewDto uploadedMpsPlan(String mpsName);

    ModuleName isValidPlan(String mpsName);

    List<RscIitIdpStatusDTO> findAllRscIitIdpStatusByActive();

    MpsPlanViewDto updateStatusOfFiles(List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs);

    MpsPlanViewDto saveOtifDate(MpsPlanViewDto mpsPlanViewDto);

    MpsPlanViewDto updateRscIitIdpStatus(MpsPlanViewDto mpsPlanViewDto);

    void deleteAll(LocalDate mpsDate);

    Boolean mpsNameExistsForEdit(String name);

    void updateAllFilesModifiedDate(Long idpStatusId);

    MpsPlanViewDto saveExecutionStatus(MpsPlanViewDto mpsPlanViewDto);

    LocalDate getMpsDateByMpsName(String name);

    List<RscIitIdpStatusDTO> findRscIitIdpStatusByNameAndDate(String name, LocalDate mpsDate);

}