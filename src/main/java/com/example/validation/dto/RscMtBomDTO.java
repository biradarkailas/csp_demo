package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtBomDTO {
    private Long id;
    private int materialBatchNumber;
    private Double linkQuantity;
    private double wastePercent;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
    private boolean active;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Long rscMtItemParentId;
    private String parentItemCode;
    private String parentItemDescription;
    private Long rscMtItemChildId;
    private String childItemCode;
    private String parentItemCategory;
    private Long mpsId;
}
