package com.example.validation.dto.slob;

import lombok.Data;

import java.util.List;

@Data
public class MaterialWiseFGSlobSummaryDashboardDTO {
    private Double totalSlobValue;
    private Double totalSLValue;
    private Double totalOBValue;
    private List<TypeWiseFGSlobSummaryDashboardDTO> typeWiseFGSlobSummaryDashboardDTOs;
}