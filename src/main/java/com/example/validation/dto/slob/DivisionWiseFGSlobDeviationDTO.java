package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class DivisionWiseFGSlobDeviationDTO {
    private String divisionName;
    private FGSlobDeviationDTO divisionWiseDeviationDTO;
}