package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class RscIotFGTypeSlobSummaryDTO {
    private Long id;
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
    private Double slobItemPercentage;
    private Double stockQualityPercentage;
    private Long fgTypeId;
    private String fgType;
    private Long rscMtPPheaderId;
    private Double stockQualityValue;

}