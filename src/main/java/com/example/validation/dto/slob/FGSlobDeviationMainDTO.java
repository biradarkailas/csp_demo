package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class FGSlobDeviationMainDTO {
    private FGSlobDeviationDTO obDeviationDTO;
    private FGSlobDeviationDTO slDeviationDTO;
    private FGSlobDeviationDTO slobDeviationDTO;
    private FGSlobDeviationDTO stockDeviationDTO;
    private FGSlobDeviationDTO obPercentageDeviationDTO;
    private FGSlobDeviationDTO slPercentageDeviationDTO;
    private FGSlobDeviationDTO stockPercentageDeviationDTO;
}
