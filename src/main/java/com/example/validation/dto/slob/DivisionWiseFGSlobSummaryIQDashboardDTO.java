package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class DivisionWiseFGSlobSummaryIQDashboardDTO {
    private Long typeId;
    private String typeName;
    private Double iqValue;
    private SlobValueDTO slobValues;
}