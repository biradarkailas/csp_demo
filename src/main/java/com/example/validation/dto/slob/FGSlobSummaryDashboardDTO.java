package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class FGSlobSummaryDashboardDTO {
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSLValue;
    private Double totalOBValue;
    private Double iqValue;
    private Double obPercentage;
    private Double slPercentage;
    private Double iqPercentage;
}