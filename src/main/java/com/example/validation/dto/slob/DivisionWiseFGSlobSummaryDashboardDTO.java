package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class DivisionWiseFGSlobSummaryDashboardDTO {
    private String type;
    private Double slobValue;
    private Double slValue;
    private Double obValue;
}