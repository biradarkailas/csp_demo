package com.example.validation.dto.slob;

import lombok.Data;

import java.util.List;

@Data
public class TypeWiseFGSlobDeviationDTO {
    private Long fgTypeId;
    private String fgType;
    private FGSlobDeviationDTO typeDeviationDTO;
    private List<DivisionWiseFGSlobDeviationDTO> divisionWiseFGSlobSummaryDTOs;
}