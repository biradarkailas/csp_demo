package com.example.validation.dto.slob;

import com.example.validation.util.enums.SlobType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class FGSlobDTO {
    private Double stockValue;
    private Double slobValue;
    private Long rscMtItemId;
    private Long itemDivisionId;
    private String itemCategory;
    private Long rscMtPPHeaderId;

    @Enumerated(EnumType.STRING)
    private SlobType slobType;
}