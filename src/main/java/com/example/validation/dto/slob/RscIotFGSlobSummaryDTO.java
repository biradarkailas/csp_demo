package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class RscIotFGSlobSummaryDTO {
    private Long id;
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
    private Double slobItemPercentage;
    private Double stockQualityPercentage;
    private Long rscMtPPheaderId;
    private Double stockQualityValue;
}