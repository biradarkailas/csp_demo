package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class RscIotFGTypeDivisionSlobSummaryDTO {
    private Long id;
    private Long fgTypeId;
    private String fgType;
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double stockQualityPercentage;
    private Long rscDtDivisionId;
    private String divisionName;
    private Long rscMtPPheaderId;
    private Double stockQualityValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
}