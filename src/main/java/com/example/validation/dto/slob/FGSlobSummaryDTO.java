package com.example.validation.dto.slob;

import lombok.Data;

import java.util.List;

@Data
public class FGSlobSummaryDTO {
    private Long rscMtPPHeaderId;
    private String mpsName;
    private String currentMonthValue;
    private String previousMonthValue;
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
    private Double stockQualityPercentage;
    private Double slobIQ;
    private List<TypeWiseFGSlobSummaryDTO> typeWiseFGSlobSummaryDTOS;
}