package com.example.validation.dto.slob;

import lombok.Data;

import java.util.List;

@Data
public class DivisionFGSlobSummaryIQDashboardDTO {
    private Long materialTypeId;
    private String materialType;
    private Double iqValue;
    private List<DivisionWiseFGSlobSummaryIQDashboardDTO> divisionWiseFGSlobSummaryIQDashboardDTOs;
}