package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class FGSlobValuesDTO {
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalSlowValue;
    private Double totalObsoleteValue;
}