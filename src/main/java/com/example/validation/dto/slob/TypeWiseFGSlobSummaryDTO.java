package com.example.validation.dto.slob;

import lombok.Data;

import java.util.List;

@Data
public class TypeWiseFGSlobSummaryDTO {
    private Long fgTypeId;
    private String fgType;
    private Double totalTypeStockValue;
    private Double totalTypeSlobValue;
    private Double totalTypeSLValue;
    private Double totalTypeOBValue;
    private Double materialIQ;
    private List<DivisionWiseFGSlobSummaryDTO> divisionWiseFGSlobSummaryDTOs;
}