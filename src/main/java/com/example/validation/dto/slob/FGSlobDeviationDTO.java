package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class FGSlobDeviationDTO {
    private Double currentMonthSlobValue;
    private Double previousMonthSlobValue;
    private Double deviationValue;
    private String deviationStatus;
}