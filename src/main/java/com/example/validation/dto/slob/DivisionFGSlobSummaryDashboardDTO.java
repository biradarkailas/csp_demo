package com.example.validation.dto.slob;

import lombok.Data;

import java.util.List;

@Data
public class DivisionFGSlobSummaryDashboardDTO {
    private Long fgTypeId;
    private String fgType;
    private List<DivisionWiseFGSlobSummaryDashboardDTO> divisionWiseFGSlobSummaryDashboardDTOs;
}