package com.example.validation.dto.slob;

import lombok.Data;

import java.time.LocalDate;

@Data
public class FGSlobTotalValueDetailDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double slTypeTotalValue;
    private Double obTypeTotalValue;
}