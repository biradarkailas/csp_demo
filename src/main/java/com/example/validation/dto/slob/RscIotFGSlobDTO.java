package com.example.validation.dto.slob;

import com.example.validation.util.enums.SlobType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Data
public class RscIotFGSlobDTO {
    private Long id;
    private Double mapPrice;
    private Double stockQuantity;
    private Double stockValue;
    private Double slobQuantity;
    private Double slobValue;
    private Double totalPlanQuantity;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscMtPPHeaderId;
    private String mpsName;
    private LocalDate mpsDate;

    @Enumerated(EnumType.STRING)
    private SlobType slobType;
}