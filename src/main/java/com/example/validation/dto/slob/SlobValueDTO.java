package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class SlobValueDTO {
    private Double stockValue;
    private Double slobValue;
}