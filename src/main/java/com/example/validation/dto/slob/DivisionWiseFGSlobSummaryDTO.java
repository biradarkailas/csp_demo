package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class DivisionWiseFGSlobSummaryDTO {
    private Double stockValue;
    private Double slobValue;
    private Double slValue;
    private Double obValue;
    private Long rscDtDivisionId;
    private String divisionName;
}