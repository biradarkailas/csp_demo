package com.example.validation.dto.slob;

import lombok.Data;

@Data
public class TypeWiseFGSlobSummaryDashboardDTO {
    private Long fgTypeId;
    private String fgType;
    private Double slobValue;
    private Double slValue;
    private Double obValue;
}