package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class IndividualValuesBulkOpRmDTO {
    private LocalDate stockDate;
    private Double value;
    private String code;
    private String description;
    private String lotNumber;
}
