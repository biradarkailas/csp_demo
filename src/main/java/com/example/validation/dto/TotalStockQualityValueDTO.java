package com.example.validation.dto;

import lombok.Data;

@Data
public class TotalStockQualityValueDTO {
    private String totalStockQualityValueFormula;
    private String totalStockQualityFormulaValue;
    private Double stockQuantity;
    private String stockQualityPercentageFormula;
    private String stockQualityPercentageFormulaValue;
    private Double stockQualityPercentage;
}
