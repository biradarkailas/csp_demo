package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtItemCategoryDTO {
    private Long id;
    private String category;
    private String description;
}
