package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtCountryDTO {
    private Long id;
    private String name;
}

