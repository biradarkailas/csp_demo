package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtSkidsDTO {
    private Long id;
    private String name;
    private double shifts;
    private double batchesPerShift;
    private double hrsPerShift;
}
