package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscMtRmBulkOpRmDTO {
    private Long id;
    private LocalDate date;
    private Double quantity;
    private String lotNumber;
    private String location;
    private String stockStatus;
    private String documentNumber;
    private String country;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
}