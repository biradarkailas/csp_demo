package com.example.validation.dto;

import lombok.Data;

@Data
public class ItemDetailsDTO {
    private Long itemId;
    private String itemCode;
    private String itemDescription;
}