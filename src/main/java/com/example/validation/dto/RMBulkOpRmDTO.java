package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RMBulkOpRmDTO {
    private String lotNumber;
    private Double quantity;
    private String stockStatus;
    private LocalDate date;
}
