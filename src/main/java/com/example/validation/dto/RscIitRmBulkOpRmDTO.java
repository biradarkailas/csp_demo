package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIitRmBulkOpRmDTO {
    private Long id;
    private Double quantity;
    private LocalDate date;
    private Long rscMtBulkItemId;
    private String bulkItemCode;
    private String bulkItemDescription;
    private Long rscMtRmItemId;
    private String rmItemCode;
    private String rmItemDescription;
}
