package com.example.validation.dto;

import com.example.validation.dto.slob.FGSlobSummaryDashboardDTO;
import lombok.Data;

@Data
public class FGSlobSummaryWithFgTypeAndDivisionDTO {
    private Long fgTypeId;
    private String fgType;
    private Long divisionId;
    private String divisionName;
    private FGSlobSummaryDashboardDTO fgSlobSummaryDashboardDTO;
}
