package com.example.validation.dto;

import lombok.Data;

@Data
public class MonthDetailDTO {
    MonthNameDTO month1;
    MonthNameDTO month2;
    MonthNameDTO month3;
    MonthNameDTO month4;
    MonthNameDTO month5;
    MonthNameDTO month6;
    MonthNameDTO month7;
    MonthNameDTO month8;
    MonthNameDTO month9;
    MonthNameDTO month10;
    MonthNameDTO month11;
    MonthNameDTO month12;
}