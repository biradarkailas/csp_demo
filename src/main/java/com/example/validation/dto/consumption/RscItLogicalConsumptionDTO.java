package com.example.validation.dto.consumption;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscItLogicalConsumptionDTO {
    private Long id;
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private Double totalValue;
    private Long rscMtItemMpsParentId;
    private String rscMtItemMpsParentCode;
    private String rscMItemMpsParentCodeDescription;
    private Long rscMtItemParentId;
    private String rscMtItemParentCode;
    private String rscMtItemParentCodeDescription;
    private Long rscMtItemChildId;
    private String rscMtItemChildCode;
    private String mtItemChildCodeDescription;
    private Double childItemCoverDays;
    private Long rscMtPPheaderId;
    private String mpsName;
    private LocalDate mpsDate;
    private Long rscMtBomId;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
}