package com.example.validation.dto.consumption;

import com.example.validation.dto.MonthValueDetailDTO;
import lombok.Data;

import java.time.LocalDate;

@Data
public class LogicalConsumptionMainDTO {
    private Long parentId;
    private String parentCode;
    private String parentDescription;
    private Long childId;
    private String childCode;
    private String childDescription;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
    private Double linkQuantity;
    private Double wastePercent;
    private Double totalValue;
    private MonthValueDetailDTO logicalConsumptionMonthValues;
}