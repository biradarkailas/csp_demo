package com.example.validation.dto.consumption;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class GrossConsumptionMainDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private List<GrossConsumptionDTO> itemCodes;
}