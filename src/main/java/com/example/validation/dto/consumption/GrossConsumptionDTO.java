package com.example.validation.dto.consumption;

import com.example.validation.dto.MonthValueDetailDTO;
import lombok.Data;

@Data
public class GrossConsumptionDTO {
    private Long materialId;
    private String materialCode;
    private String materialDescription;
    private Double totalValue;
    private String category;
    private int priority;
    private Long itemWiseSupplierId;
    private String supplierName;
    private String supplierCode;
    private MonthValueDetailDTO grossConsumptionMonthValues;
}