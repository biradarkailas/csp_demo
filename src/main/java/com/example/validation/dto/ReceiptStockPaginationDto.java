package com.example.validation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ReceiptStockPaginationDto {
    private String itemCode;
    private String itemDescription;
    private Long rscMtItemId;
    private Double totalReceiptStock;
    private Double lotWiseReceiptStock;
    private String lotNumber;
    private String receiptNumber;
    private LocalDate receiptDate;
    private String supplierName;
    private String poNumber;
}
