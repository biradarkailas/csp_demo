package com.example.validation.dto;

import lombok.Data;

@Data
public class MonthNamesDTO {
    private String month1Value;
    private String month2Value;
    private String month3Value;
    private String month4Value;
    private String month5Value;
    private String month6Value;
    private String month7Value;
    private String month8Value;
    private String month9Value;
    private String month10Value;
    private String month11Value;
    private String month12Value;
}
