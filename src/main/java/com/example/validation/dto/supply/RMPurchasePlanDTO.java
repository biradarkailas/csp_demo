package com.example.validation.dto.supply;

import com.example.validation.dto.MonthValueDetailDTO;
import lombok.Data;

@Data
public class RMPurchasePlanDTO {
    private Long id;
    private Long mpsId;
    private Long materialId;
    private String materialCode;
    private String materialDescription;
    private Long rscIitRMItemWiseSupplierDetailsId;
    private Long rscItRMGrossConsumptionId;
    private String supplierName;
    private String supplierCode;
    private String category;
    private Double packSize;
    private Double moqValue;
    private String halalComments;
    private String otherComments;
    private Double totalValue;
    private String orderType;
    private String poRemark;
    private String containerType;
    private String description;
    private Double allocation;
    private MonthValueDetailDTO supplyMonthValues;
    private OriginalRMPurchasePlanDTO originalRMPurchasePlan;
}
