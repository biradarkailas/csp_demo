package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class MonthWiseSupplyDTO {
    private Double monthEndStock;
    private Double stockAfterConsumption;
    private Double receiptTillDate;
    private Double closingStock;
    private Double finalSupply;
    private Double consumptionAtCurrentMonth;
    private Double finalSupplySuggestedCurrentMonth;
    private Double nextMonthOpeningStock;
    private Double coverDays;
    private Double openPoStock;
}
