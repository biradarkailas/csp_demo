package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class RMPurchasePlanMainDTO {
    private String mpsName;
    private LocalDate mpsDate;
    private List<RMPurchasePlanDTO> itemCodes;
}