package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotRMPurchasePlanCalculationsDTO {
    private Long id;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscMtPPheaderId;
    private LocalDate mpsDate;
    private Long rscItRMGrossConsumptionId;
    private Long itemWiseSupplierDetailsId;
    private MonthWiseRMPurchasePlanDTO month1PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month2PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month3PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month4PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month5PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month6PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month7PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month8PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month9PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month10PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month11PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month12PurchasePlanDTO;
}