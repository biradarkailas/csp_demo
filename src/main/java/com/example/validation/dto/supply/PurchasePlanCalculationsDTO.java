package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class PurchasePlanCalculationsDTO {
    private Double monthEndStock;
    private Double consumption;
    private Double finalSupply;
    private Double priority;
}
