package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotPMPurchasePlanCalculationsDTO {
    private Long id;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscMtPPheaderId;
    private LocalDate mpsDate;
    private Long rscItPMGrossConsumptionId;
    private Long rscItItemWiseSupplierId;
    private int priority;
    private Month1SupplyDTO month1SupplyDTO;
    private MonthWiseSupplyDTO month2SupplyDTO;
    private MonthWiseSupplyDTO month3SupplyDTO;
    private MonthWiseSupplyDTO month4SupplyDTO;
    private MonthWiseSupplyDTO month5SupplyDTO;
    private MonthWiseSupplyDTO month6SupplyDTO;
    private MonthWiseSupplyDTO month7SupplyDTO;
    private MonthWiseSupplyDTO month8SupplyDTO;
    private MonthWiseSupplyDTO month9SupplyDTO;
    private MonthWiseSupplyDTO month10SupplyDTO;
    private MonthWiseSupplyDTO month11SupplyDTO;
    private MonthWiseSupplyDTO month12SupplyDTO;
}