package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class PmSupplyCalculationsDTO {
    private String materialCode;
    private String materialDescription;
    private String supplierName;
    private Double safetyStock;
    private Double coverDays;
    private Double moq;
    private Double techSeries;
    private Month1SupplyDTO month1SupplyDTO;
    private MonthWiseSupplyDTO month2SupplyDTO;
    private MonthWiseSupplyDTO month3SupplyDTO;
    private MonthWiseSupplyDTO month4SupplyDTO;
    private MonthWiseSupplyDTO month5SupplyDTO;
    private MonthWiseSupplyDTO month6SupplyDTO;
    private MonthWiseSupplyDTO month7SupplyDTO;
    private MonthWiseSupplyDTO month8SupplyDTO;
    private MonthWiseSupplyDTO month9SupplyDTO;
    private MonthWiseSupplyDTO month10SupplyDTO;
    private MonthWiseSupplyDTO month11SupplyDTO;
    private MonthWiseSupplyDTO month12SupplyDTO;
}
