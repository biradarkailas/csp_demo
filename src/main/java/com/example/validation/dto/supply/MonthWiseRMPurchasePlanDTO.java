package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class MonthWiseRMPurchasePlanDTO {
    private Double openingStock;
    private Double stockAfterConsumption;
    private Double openPoStock;
    private Double closingStock;
    private Double suggestedPurchasePlan;
    private Double consumptionAtCurrentMonth;
    private Double obsoleteStock;
    private Double nextMonthOpeningStock;
    private Double coverDays;
}