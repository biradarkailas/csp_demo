package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class Month1SupplyDTO {
    private Double monthEndStock;
    private Double stockAfterConsumption;
    private Double receiptTillDate;
    private Double closingStock;
    private Double suggestedSupply;
    private Double onHandStock;
    private Double prioritySupply;
    private Double finalSupply;
    private Double consumptionAtCurrentMonth;
    private Double finalSupplySuggestedCurrentMonth;
    private Double nextMonthOpeningStock;
    private Double coverDays;
    private Double openPoStock;
}