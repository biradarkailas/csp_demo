package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PurchasePlanCoverDaysEquationDTO {
    private Long id;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscMtPPheaderId;
    private LocalDate mpsDate;
    private StockEquationDTO m1StockEquationDTO;
    private StockEquationDTO m2StockEquationDTO;
    private StockEquationDTO m3StockEquationDTO;
    private StockEquationDTO m4StockEquationDTO;
    private StockEquationDTO m5StockEquationDTO;
    private StockEquationDTO m6StockEquationDTO;
    private StockEquationDTO m7StockEquationDTO;
    private StockEquationDTO m8StockEquationDTO;
    private StockEquationDTO m9StockEquationDTO;
    private StockEquationDTO m10StockEquationDTO;
    private StockEquationDTO m11StockEquationDTO;
    private StockEquationDTO m12StockEquationDTO;
}