package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class PurchasePlanMainDTO {
    private String mpsName;
    private LocalDate mpsDate;
    private List<PurchasePlanDTO> itemCodes;
}