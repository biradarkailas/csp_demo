package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotRMPurchasePlanDTO {
    private Long id;
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private String poRemark;
    private String description;
    private String containerType;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscMtPPheaderId;
    private LocalDate mpsDate;
    private Long rscIitRMItemWiseSupplierDetailsId;
    private Long rscMtSupplierId;
    private Long rscItRMGrossConsumptionId;
}