package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotPMPurchasePlanDTO {
    private Long id;
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private Double rtdValue;
    private int priority;
    private Long rscMtItemId;
    private LocalDate launchDate;
    private String itemCode;
    private String itemDescription;
    private Double coverDays;
    private Long rscMtPPheaderId;
    private LocalDate mpsDate;
    private Long rscItItemWiseSupplierId;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
    private Long rscDtPMRemarkId;
    private Long rscDtSupplierId;
    private String poRemark;
    private Long rscMtSupplierId;
}