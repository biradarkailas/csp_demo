package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class RmPurchasePlanCalculationDTO {
    private String materialCode;
    private String materialDescription;
    private String supplierName;
    private Double safetyStock;
    private Double coverDays;
    private Double moq;
    private Double packSize;
    private MonthWiseRMPurchasePlanDTO month1PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month2PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month3PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month4PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month5PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month6PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month7PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month8PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month9PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month10PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month11PurchasePlanDTO;
    private MonthWiseRMPurchasePlanDTO month12PurchasePlanDTO;
}
