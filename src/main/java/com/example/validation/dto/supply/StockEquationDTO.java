package com.example.validation.dto.supply;

import lombok.Data;

@Data
public class StockEquationDTO {
    private Double stock;
    private Double order;
    private Double coverDays;
}
