package com.example.validation.dto.supply;

import com.example.validation.dto.MonthValueDetailDTO;
import com.example.validation.dto.RscIotOriginalMonthValueslDTO;
import lombok.Data;

@Data
public class PurchasePlanDTO {
    private Long id;
    private Long mpsId;
    private Long materialId;
    private String materialCode;
    private String materialDescription;
    private Long rscDtPMRemarkId;
    private String remarkName;
    private Long rscItItemWiseSupplierId;
    private Long rscMtItemId;
    private Double moqValue;
    private Double stockValue;
    private String supplierName;
    private String supplierCode;
    private Double totalValue;
    private Double rtdValue;
    private int priority;
    private MonthValueDetailDTO supplyMonthValues;
    private RscIotOriginalMonthValueslDTO originalMonthValues;
    private Long mouldId;
    private String mould;
    private Double capacity;
}