package com.example.validation.dto.supply;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PurchasePlanCalculationsMainDTO {
    private Long id;
    private Long rscMtItemId;
    private String itemCode;
    private Long rscMtPPheaderId;
    private LocalDate mpsDate;
    private Double coverDays;
    private PurchasePlanCalculationsDTO month1SupplyDTO;
    private PurchasePlanCalculationsDTO month2SupplyDTO;
    private PurchasePlanCalculationsDTO month3SupplyDTO;
    private PurchasePlanCalculationsDTO month4SupplyDTO;
    private PurchasePlanCalculationsDTO month5SupplyDTO;
    private PurchasePlanCalculationsDTO month6SupplyDTO;
    private PurchasePlanCalculationsDTO month7SupplyDTO;
    private PurchasePlanCalculationsDTO month8SupplyDTO;
    private PurchasePlanCalculationsDTO month9SupplyDTO;
    private PurchasePlanCalculationsDTO month10SupplyDTO;
    private PurchasePlanCalculationsDTO month11SupplyDTO;
    private PurchasePlanCalculationsDTO month12SupplyDTO;
}