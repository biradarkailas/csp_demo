package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscMtPmMesDTO {
    private LocalDate stockDate;
    private double quantity;
    private String lotNumber;
    private String location;
    private String stockStatus;
    private String documentNumber;
    private String site;
    private LocalDate createdDate;
    private LocalDate modifiedDate;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
}
