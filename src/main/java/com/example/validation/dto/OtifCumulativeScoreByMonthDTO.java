package com.example.validation.dto;

import lombok.Data;

@Data
public class OtifCumulativeScoreByMonthDTO {
    private String monthValue;
    private Double actualReceiptPercentage;
    private Double scoreByActualReceipt;
}
