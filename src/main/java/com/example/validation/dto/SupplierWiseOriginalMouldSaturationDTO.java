package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class SupplierWiseOriginalMouldSaturationDTO {
    private String supplierName;
    private String mould;
    private Double capacity;
    private Double month1Total;
    private Double month2Total;
    private Double month3Total;
    private Double month4Total;
    private Double month5Total;
    private Double month6Total;
    private Double month7Total;
    private Double month8Total;
    private Double month9Total;
    private Double month10Total;
    private Double month11Total;
    private Double month12Total;
    private List<ItemWiseOriginalMouldSaturationDTO> itemWiseOriginalMouldSaturationDTOList;
}
