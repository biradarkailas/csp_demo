package com.example.validation.dto;

import com.example.validation.util.enums.SaturationStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;

@Data
public class RscIotFgLineSaturationSummaryDTO {
    private Long id;
    private Double avgSaturationPercentage;
    @Enumerated(EnumType.STRING)
    private SaturationStatus saturationStatus;
    private LocalDate lineSummaryDate;
    private Long rscDtLinesId;
    private String lineName;
    private Long rscMtPPheaderId;
}
