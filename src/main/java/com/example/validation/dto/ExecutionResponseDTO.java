package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ExecutionResponseDTO {
    private String message;
    private Boolean isSuccess;
    private String moduleName;
    private String errorMessage;
    private LocalDateTime createdDateTime;
    private String url;
}
