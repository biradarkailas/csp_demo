package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtTestTaskDTO {
    private long id;
    private String testTaskName;
    private String testTableName;
}
