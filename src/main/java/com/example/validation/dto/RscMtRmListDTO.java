package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtRmListDTO {
    private Long id;
    private Double quantity;
    private String lotNumber;
    private LocalDate expirationDate;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private LocalDate mpsDate;
}
