package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtSitDTO {
    private Long id;
    private double sitPrice;
}
