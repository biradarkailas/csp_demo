package com.example.validation.dto;

import lombok.Data;

@Data
public class ExcelFiledetailsDTO {
    private Long id;
    private String fileName;
    private String sheetName;
    private int sheetIndex;
}
