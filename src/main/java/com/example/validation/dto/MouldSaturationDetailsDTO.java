package com.example.validation.dto;

import lombok.Data;

@Data
public class MouldSaturationDetailsDTO {
    private Long rscDtMouldId;
    private String mouldName;
    private Double avgSaturationPercentage;
}
