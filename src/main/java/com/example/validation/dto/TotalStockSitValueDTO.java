package com.example.validation.dto;

import lombok.Data;

@Data
public class TotalStockSitValueDTO {
    private String totalStockSitValueFormula;
    private String totalStockSitFormulaValue;
    private Double totalStockSitValue;
}
