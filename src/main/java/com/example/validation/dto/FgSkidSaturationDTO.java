package com.example.validation.dto;

import lombok.Data;

@Data
public class FgSkidSaturationDTO {
    private RscIotFgSkidSaturationDTO rscIotFgSkidSaturationDTO;
    private String skidName;
}
