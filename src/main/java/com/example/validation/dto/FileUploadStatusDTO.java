package com.example.validation.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class FileUploadStatusDTO {
    String fileName;
    Boolean status;
    String errorDescription;
}