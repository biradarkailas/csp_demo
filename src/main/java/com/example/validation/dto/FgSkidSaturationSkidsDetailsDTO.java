package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class FgSkidSaturationSkidsDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private String highSaturationRange;
    private String mediumSaturationRange;
    private String lowSaturationRange;
    List<FgSkidSaturationSkidsDTO> highSaturationRangeSkidsDTOList;
    List<FgSkidSaturationSkidsDTO> mediumSaturationRangeSkidsDTOList;
    List<FgSkidSaturationSkidsDTO> lowSaturationRangeSkidsDTOList;
    private Integer highSaturationRangeListCount;
    private Integer mediumSaturationRangeListCount;
    private Integer lowSaturationRangeListCount;
    private Integer totalCount;
}
