package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class IndividualStockTypeValueDTO {
    private Double quantity;
    private LocalDate stockDate;
}
