package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtReasonTypeDTO {
    private String name;
}
