package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIitRMItemWiseSupplierDetailsDTO {
    private Long id;
    private String halalComments;
    private String otherComments;
    private Double safetyStock;
    private Long rscItItemWiseSupplierId;
    private String supplierName;
    private Double moqValue;
    private Double packSize;
    private Double allocation;
    private Long rscIitSupplierWiseTransportId;
    private String orderType;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
}