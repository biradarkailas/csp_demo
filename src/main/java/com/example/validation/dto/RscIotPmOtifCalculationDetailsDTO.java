package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class RscIotPmOtifCalculationDetailsDTO {
    private Long rscDtSupplierId;
    private String supplierName;
    private String supplierCode;
    private List<PmOtifCalculationDetailsDTO> pmOtifCalculationDetailsDTO;
    private Double receiptPercentage;
    private Integer score;
}
