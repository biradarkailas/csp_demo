package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class FgSaturationDeltaAnalysisDTO {
    private List<DeltaAnalysisDetailsDTO> saturationDetailsList;
    private Double[] finalSaturationDetails;
    private Double[] deviationDetails;
}
