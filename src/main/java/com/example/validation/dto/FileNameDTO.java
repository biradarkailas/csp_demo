package com.example.validation.dto;

import lombok.Data;

@Data
public class FileNameDTO {
    Long id;
    String fileName;
    Long folderId;
    String folderName;
    String extension;
    Boolean isRequired;
}