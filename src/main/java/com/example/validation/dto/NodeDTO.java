package com.example.validation.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class NodeDTO {

    private String name;
    private Double linkQuantity;
    Double wastePercentage;
    List<NodeDTO> childs = new ArrayList<>();
}