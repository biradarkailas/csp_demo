package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class IndividualValuesRtdDTO {
    private String lotNumber;
    private LocalDate receiptDate;
    private String supplierName;
    private String poNumber;
    private Double value;
}
