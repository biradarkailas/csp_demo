package com.example.validation.dto;

import lombok.Data;

@Data
public class ItemWiseOriginalMouldSaturationDTO {
    private String code;
    private String description;
    private MonthValueDTO month1;
    private MonthValueDTO month2;
    private MonthValueDTO month3;
    private MonthValueDTO month4;
    private MonthValueDTO month5;
    private MonthValueDTO month6;
    private MonthValueDTO month7;
    private MonthValueDTO month8;
    private MonthValueDTO month9;
    private MonthValueDTO month10;
    private MonthValueDTO month11;
    private MonthValueDTO month12;
}
