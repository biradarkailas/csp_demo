package com.example.validation.dto;

import lombok.Data;

@Data
public class RmStockItemCodeDTO {
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Double safetyStock;
    private Double openingStock;
    private MesDTO rmMes;
    private RtdDTO rmRtd;
    private BulkOpRmDTO rmBulkOpRm;
    private OtherStockDTO rmTransferStock;
    private OtherStockDTO rmRejection;
}
