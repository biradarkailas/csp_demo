package com.example.validation.dto;

import com.example.validation.domain.*;
import lombok.Data;

@Data
public class RscIotOriginalMouldSaturationDTO {
    private Long m1Value;
    private Long m2Value;
    private Long m3Value;
    private Long m4Value;
    private Long m5Value;
    private Long m6Value;
    private Long m7Value;
    private Long m8Value;
    private Long m9Value;
    private Long m10Value;
    private Long m11Value;
    private Long m12Value;
    private String mould;
    private Long rscDtMouldId;
    private Long rscMtPPheaderId;
}
