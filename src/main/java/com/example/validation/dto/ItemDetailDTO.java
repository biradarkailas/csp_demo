package com.example.validation.dto;

import lombok.Data;

@Data
public class ItemDetailDTO {
    private Long id;
    private String code;
    private String description;
    private Double total;
    private boolean priority;
    private String productionType;
    private String lineName;
    private String skidName;
    private MonthValueDetailDTO monthValues;
    private ItemOtherDetailDTO otherInfo;
    private String fla;
    private String divisionName;
    private String brandName;
    private String signatureName;
    private String materialCategoryName;
    private String materialSubCategoryName;
}