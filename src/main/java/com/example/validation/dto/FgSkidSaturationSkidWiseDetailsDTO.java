package com.example.validation.dto;

import com.example.validation.util.enums.SaturationStatus;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

@Data
public class FgSkidSaturationSkidWiseDetailsDTO {
    private Long rscDtSkidsId;
    private String skidName;
    @Enumerated(EnumType.STRING)
    private SaturationStatus saturationStatus;
    private Double avgReceiptPercentage;
    List<RscIotFgSkidSaturationDTO> rscIotFgSkidSaturationDTOList;
}
