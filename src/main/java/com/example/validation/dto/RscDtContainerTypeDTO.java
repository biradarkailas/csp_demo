package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtContainerTypeDTO {
    Long id;
    String typeName;
}
