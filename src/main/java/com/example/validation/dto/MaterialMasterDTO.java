package com.example.validation.dto;

import lombok.Data;

@Data
public class MaterialMasterDTO {
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtMoqId;
    private Double moqValue;
    private Long rscDtTechnicalSeriesId;
    private Double seriesValue;
    private Long rscDtSafetyStockId;
    private Double stockValue;
    private Double mapPrice;
    private Double stdPrice;
}