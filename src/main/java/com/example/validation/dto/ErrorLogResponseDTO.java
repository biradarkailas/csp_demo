package com.example.validation.dto;

import lombok.Data;

@Data
public class ErrorLogResponseDTO {
    private Long id;
    private String fileName;
    private String fieldName;
    private String errorDescription;
    private String errorType;
}