package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtSafetyStockDTO {
    private Long id;
    private Double stockValue;
}
