package com.example.validation.dto;

import lombok.Data;

@Data
public class CoverDaysDTO {
    private Integer month1CoverDays;
    private Integer month2CoverDays;
    private Integer month3CoverDays;
    private Integer month4CoverDays;
    private Integer month5CoverDays;
    private Integer month6CoverDays;
    private Integer month7CoverDays;
    private Integer month8CoverDays;
    private Integer month9CoverDays;
    private Integer month10CoverDays;
    private Integer month11CoverDays;
    private Integer month12CoverDays;
}
