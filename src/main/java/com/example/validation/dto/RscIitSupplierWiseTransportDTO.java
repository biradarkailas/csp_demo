package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIitSupplierWiseTransportDTO {
    private Long id;
    private Long rscItItemWiseSupplierId;
    private String supplierName;
    private Long rscDtSailingDaysId;
    private Double sailingDays;
    private Long rscDtTransportModeId;
    private String rscDtTransportModeCode;
    private Long rscDtTransportTypeId;
    private String transportTypeFullName;
}