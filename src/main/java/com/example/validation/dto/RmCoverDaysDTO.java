package com.example.validation.dto;

import lombok.Data;

@Data
public class RmCoverDaysDTO {
    private Long id;
    private Long rscMtItemId;
    private Long rscMtPPheaderId;
    private Long rscItRMGrossConsumptionId;
    private String code;
    private String description;
    private String category;
    private Double stockQuantity;
    private Double stockValue;
    private Double stdPrice;
    private Double map;
    private Double rate;
    private Double allMonthsConsumptionValue;
    private Double consumptionPercentage;
    private Double aggregatedPercentage;
    private String className;
    private CoverDaysMonthDetailsDTO CoverDaysMonthDetailsDTO;
}
