package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtPMRemarkDTO {
    private Long id;
    private String name;
    private String aliasName;}
