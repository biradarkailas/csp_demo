package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class PmOtifCalculationSupplierWiseListDTO {
    private Long rscDtSupplierId;
    private String supplierCode;
    private String supplierName;
    private Double avgReceiptPercentage;
    private Integer avgScore;
    List<RscIotPmOtifCalculationDTO> rscIotPmOtifCalculationDTOList;
}
