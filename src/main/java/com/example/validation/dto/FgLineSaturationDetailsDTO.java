package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class FgLineSaturationDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private RscIitMpsWorkingDaysDTO rscIitMpsWorkingDaysDTO;
    private List<FgLineSaturationDTO> fgLineSaturationDTO;
}
