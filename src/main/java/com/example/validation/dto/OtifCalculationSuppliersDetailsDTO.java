package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class OtifCalculationSuppliersDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;

    private String ReceiptRangeForScore4;
    private String ReceiptRangeForScore3;
    private String ReceiptRangeForScore2;
    private String ReceiptRangeForScore1;
    private String ReceiptRangeForScore0;

    List<OtifCalculationSuppliersDTO> score4ReceiptRangeSuppliersDTOList;
    List<OtifCalculationSuppliersDTO> score3ReceiptRangeSuppliersDTOList;
    List<OtifCalculationSuppliersDTO> score2ReceiptRangeSuppliersDTOList;
    List<OtifCalculationSuppliersDTO> score1ReceiptRangeSuppliersDTOList;
    List<OtifCalculationSuppliersDTO> score0ReceiptRangeSuppliersDTOList;

    private Integer score4ReceiptRangeListCount;
    private Integer score3ReceiptRangeListCount;
    private Integer score2ReceiptRangeListCount;
    private Integer score1ReceiptRangeListCount;
    private Integer score0ReceiptRangeListCount;

    private Integer totalCount;
}
