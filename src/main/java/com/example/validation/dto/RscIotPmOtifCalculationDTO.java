package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscIotPmOtifCalculationDTO {
    private Long id;
    private LocalDate otifCalcDate;
    private Long rscIotPMLatestPurchasePlanId;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Double currentMonthOrderQty;
    private Double prevMonthOrderQty;
    private Long rscMtPPheaderId;
    private Long rscDtSupplierId;
    private LocalDate receiptHeaderDate;
    private Double totalOrderQty;
    private Double totalReceivedQty;
    private Double balanceQty;
    private Double receiptPercentage;
    private Integer score;
    private LocalDateTime createdDate;
}
