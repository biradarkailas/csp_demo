package com.example.validation.dto;

import lombok.Data;

@Data
public class ItemOtherDetailDTO {
    private Long skidsId;
    private Long productionTypeId;
    private Long linesId;
    private Long divisionId;
    private Long brandId;
    private Long signatureId;
    private Long materialCategoryId;
    private Long materialSubCategoryId;
}