package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PmStockItemCodeDTO {
    private Long id;
    private String code;
    private String description;
    private Double safetyStock;
    private Double openingStock;
    private LocalDate stockDate;
    private MesDTO mes;
    private RtdDTO rtd;
}
