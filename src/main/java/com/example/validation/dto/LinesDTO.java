package com.example.validation.dto;

import lombok.Data;

@Data
public class LinesDTO {
    private Long id;
    private String lineName;
}
