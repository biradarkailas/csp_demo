package com.example.validation.dto;

import lombok.Data;

@Data
public class MonthValueDTO {
    private Double value;
}