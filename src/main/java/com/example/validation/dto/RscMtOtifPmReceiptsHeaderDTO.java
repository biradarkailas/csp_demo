package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtOtifPmReceiptsHeaderDTO {
    private Long id;
    private LocalDate otifDate;
    private boolean active;
    private LocalDate createdDate;
    private LocalDate modifiedDate;
}
