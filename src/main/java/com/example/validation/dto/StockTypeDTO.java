package com.example.validation.dto;

import lombok.Data;

@Data
public class StockTypeDTO {
    private MesDTO rmMes;
    private RtdDTO rmRtd;
    private BulkOpRmDTO rmBulkOpRm;
    private OtherStockDTO rmTransferStock;
    private OtherStockDTO rmRejection;
}
