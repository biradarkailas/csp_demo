package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotPmSlobSummaryDTO {
    private Long id;
    private Long rscMtPPheaderId;
    private Double totalStockValue;
    private Double sitValue;
    private Double totalStockSitValue;
    private Double totalSlobValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double slowItemPercentage;
    private Double obsoleteItemPercentage;
    private Double stockQualityPercentage;
    private Double stockQuantity;
}
