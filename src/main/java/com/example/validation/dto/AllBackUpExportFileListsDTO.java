package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class AllBackUpExportFileListsDTO {
    private BackupPlanList planZipList;
    private List<String> pmOtifZipList;
    private List<String> allModuleZipList;
}
