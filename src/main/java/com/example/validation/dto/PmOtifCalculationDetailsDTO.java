package com.example.validation.dto;

import lombok.Data;

@Data
public class PmOtifCalculationDetailsDTO {
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Double currentMonthOrderQty;
    private Double prevMonthOrderQty;
    private Double totalOrderQty;
    private Double totalReceivedQty;
    private Double balanceQty;
    private Double receiptPercentage;
    private Integer score;
}
