package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscItPMStockDTO {
    private Long id;
    private LocalDate stockDate;
    private Double quantity;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtStockTypeId;
    private String stockTypeTag;
    private String stockTypeAlias;
    private Double mapPrice;
    private Double stdPrice;
    private LocalDate arrivalDateForPo;
}