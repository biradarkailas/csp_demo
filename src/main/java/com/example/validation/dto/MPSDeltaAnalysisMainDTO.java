package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class MPSDeltaAnalysisMainDTO {
    private List<Double> m1Values;
    private List<Double> m2Values;
    private List<Double> m3Values;
    private List<Double> m4Values;
    private List<Double> m5Values;
    private List<Double> m6Values;
    private List<Double> m7Values;
    private List<Double> m8Values;
    private List<Double> m9Values;
    private List<Double> m10Values;
    private List<Double> m11Values;
    private List<Double> m12Values;
    private List<Long> finalValues;
    private List<Double> deviationValues;
}