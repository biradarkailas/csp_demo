package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class FgLineSaturationDTO {
    private RscIotFgLineSaturationDTO rscIotFgLineSaturationDTOS;
    private String lineName;
}
