package com.example.validation.dto;

import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import lombok.Data;

import java.util.List;

@Data
public class LogicalConsumptionExportDTO {
    private RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO;
    private RscMtBomDTO rscMtBomDTO;
    private List<ItemDetailDTO> itemDetailDTOList;
}
