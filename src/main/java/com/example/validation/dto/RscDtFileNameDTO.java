package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtFileNameDTO {
    Long id;
    String fileName;
    Long folderId;
    String folderName;
    String extension;
    Boolean fcRequired;
    Boolean vdRequired;
    Boolean psRequired;
}