package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtItemCodeDTO {
    private Long id;
    private String code;
    private String description;
}
