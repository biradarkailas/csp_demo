package com.example.validation.dto;

import lombok.Data;

@Data
public class SlobTotalConsumptionDetailsDTO {
    private Long rscMtItemId;
    private Long rscMtPPheaderId;
    private MonthValueDetailDTO grossConsumptionMonthValues;
    private Double totalValue;
}
