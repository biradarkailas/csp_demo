package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SlobSummaryDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private SlobSummaryFormulaDetailsDTO slobSummaryFormulaDetailsDTO;
}
