package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtFGTypeDTO {
    private Long id;
    private String type;
}