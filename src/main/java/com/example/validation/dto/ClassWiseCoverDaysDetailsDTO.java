package com.example.validation.dto;

import lombok.Data;

@Data
public class ClassWiseCoverDaysDetailsDTO {
    private Long rscMtPPHeaderId;
    private ClassWiseDetailsDTO classACoverDaysConsumptionDTO;
    private ClassWiseDetailsDTO classBCoverDaysConsumptionDTO;
    private ClassWiseDetailsDTO classCCoverDaysConsumptionDTO;
    private CoverDaysConsumptionDTO totalCoverDaysConsumptionDTO;
}
