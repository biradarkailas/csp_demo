package com.example.validation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RmTransferStockPaginationDTO {
    private String itemCode;
    private String itemDescription;
    private LocalDate stockDate;
    private Long rscMtItemId;
    private Double quantity;
    private Double lotWiseStock;
}
