package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtFolderNameDTO {
    Long id;
    String folderName;
}
