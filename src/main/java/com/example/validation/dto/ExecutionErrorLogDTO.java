package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ExecutionErrorLogDTO {
    private Long id;
    private String methodName;
    private String errorDescription;
    private LocalDateTime createdDate;
    private Long rscMtPPheaderId;
}
