package com.example.validation.dto;

import com.example.validation.domain.BaseIdEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.time.LocalDate;

@EqualsAndHashCode(callSuper = true)
@Data
public class RscIotMPSPlanTotalQuantityDTO extends BaseIdEntity {
    private Double m1TotalValue;
    private Double m2TotalValue;
    private Double m3TotalValue;
    private Double m4TotalValue;
    private Double m5TotalValue;
    private Double m6TotalValue;
    private Double m7TotalValue;
    private Double m8TotalValue;
    private Double m9TotalValue;
    private Double m10TotalValue;
    private Double m11TotalValue;
    private Double m12TotalValue;
    private String mpsPlanName;
    private int mpsPlanSequence;
    private Long mpsId;
    private LocalDate mpsDate;
    private String mpsName;
}