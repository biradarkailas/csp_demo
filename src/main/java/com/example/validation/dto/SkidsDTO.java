package com.example.validation.dto;

import lombok.Data;

@Data
public class SkidsDTO {
    private Long id;
    private String skidName;
}
