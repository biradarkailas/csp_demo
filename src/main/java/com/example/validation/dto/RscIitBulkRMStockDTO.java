package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIitBulkRMStockDTO {
    private Double bulkQuantity;
    private Double rmQuantity;
    private Long bulkItemId;
    private String bulkItemCode;
    private String bulkItemDescription;
    private Long rmItemId;
    private String rmItemCode;
    private String rmItemDescription;
}