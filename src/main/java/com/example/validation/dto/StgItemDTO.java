package com.example.validation.dto;

import lombok.Data;

@Data
public class StgItemDTO {
    private String itemCode;
    private String code;
    private String elevenDigitCode;
}
