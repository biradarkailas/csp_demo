package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PMRtdDTO {
    private String lotNumber;
    private LocalDate receiptDate;
    private String supplierName;
    private String poNumber;
    private Double value;
    private String supplierLotNumber;
}