package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtMaterialCategoryDTO {
    private Long id;
    private String name;
}
