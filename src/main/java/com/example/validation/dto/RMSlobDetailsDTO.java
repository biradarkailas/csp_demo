package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class RMSlobDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double totalObsoleteExpiryValue;
    private Double totalObsoleteNonExpiryValue;
    private Double totalSlowItemValue;
    private Double sitValue;
    private String currentMonthValue;
    private String previousMonthValue;
    private Double totalMidNightStockValue;
    private Double totalExpiredStockValue;
    private Double totalFinalStockValue;
    private Double previousMonthsTotalSlobValue;
    private Double previousMonthsTotalObsoleteExpiryValue;
    private Double previousMonthsTotalObsoleteNonExpiryValue;
    private Double previousMonthsTotalSlowItemValue;
    private Double m1TotalQuantity;
    private Double m2TotalQuantity;
    private Double m3TotalQuantity;
    private Double m4TotalQuantity;
    private Double m5TotalQuantity;
    private Double m6TotalQuantity;
    private Double m7TotalQuantity;
    private Double m8TotalQuantity;
    private Double m9TotalQuantity;
    private Double m10TotalQuantity;
    private Double m11TotalQuantity;
    private Double m12TotalQuantity;
    private Double totalConsumptionQuantity;
    private Double totalSlobQuantityValue;
    private List<RmSlobDTO> rmSlobs;
}
