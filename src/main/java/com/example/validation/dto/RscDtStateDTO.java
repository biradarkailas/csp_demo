package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtStateDTO {
    private Long id;
    private String stateName;
    private Long rscDtCountryId;
}
