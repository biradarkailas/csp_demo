package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtItemTypeDTO {
    private Long id;
    private String itemTypeDescription;
}
