package com.example.validation.dto;

import lombok.Data;

@Data
public class StockTypesDTO {
    private MesDTO rmMesDTO;
    private RtdDTO rmRtdDTO;
    private BulkOpRmDTO bulkOpRmDTO;
}
