package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtItemDTO {
    private Long id;
    private LocalDate launchDate;
    private boolean active;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Long rscDtItemCodeId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtItemCategoryId;
    private String itemCategoryName;
    private String itemCategoryDescription;
    private Long rscDtItemTypeId;
    private String itemTypeName;
    private Long rscDtCoverDaysId;
    private Double coverDays;
    private Long rscDtUomId;
    private String uomName;
    private Long rscDtDivisionId;
    private String divisionName;
    private Long rscDtSafetyStockId;
    private Double stockValue;
    private Long rscDtItemClassId;
    private String itemClassName;
    private Long rscDtTechnicalSeriesId;
    private Double seriesValue;
    private Long rscDtMoqId;
    private Double moqValue;
    private Long rscDtMapId;
    private Double mapPrice;
    private Double stdPrice;
}
