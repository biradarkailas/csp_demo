package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIitMpsForecastDTO {
    private Long id;
    private String division;
    private String sixDigitCode;
    private String eightDigitCode;
    private String elevenDigitCode;
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private Double m13Value;
    private Double m14Value;
    private Long rscMtItemId;
    private Long rscMtPPHeaderId;
}