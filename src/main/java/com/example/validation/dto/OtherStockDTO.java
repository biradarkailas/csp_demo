package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class OtherStockDTO {
    private String name;
    private String aliasName;
    private Double totalValue;
    private List<IndividualStockTypeValueDTO> individualStockTypeValueDTOList;
}
