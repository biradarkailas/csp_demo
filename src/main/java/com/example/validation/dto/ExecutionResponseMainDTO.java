package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class ExecutionResponseMainDTO {
    private LocalDate mpsDate;
    private String mpsName;
    private List<ExecutionResponseDTO> executionResponseDTOs;
}
