package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotFgLineSaturationDTO {
    private Long id;
    private Long rscMtPPheaderId;
    private Long rscDtLinesId;
    private Double m1TotalMpsPlanQty;
    private Double m2TotalMpsPlanQty;
    private Double m3TotalMpsPlanQty;
    private Double m4TotalMpsPlanQty;
    private Double m5TotalMpsPlanQty;
    private Double m6TotalMpsPlanQty;
    private Double m7TotalMpsPlanQty;
    private Double m8TotalMpsPlanQty;
    private Double m9TotalMpsPlanQty;
    private Double m10TotalMpsPlanQty;
    private Double m11TotalMpsPlanQty;
    private Double m12TotalMpsPlanQty;
    private Double month1Capacity;
    private Double month2Capacity;
    private Double month3Capacity;
    private Double month4Capacity;
    private Double month5Capacity;
    private Double month6Capacity;
    private Double month7Capacity;
    private Double month8Capacity;
    private Double month9Capacity;
    private Double month10Capacity;
    private Double month11Capacity;
    private Double month12Capacity;
    private Double m1LineSaturation;
    private Double m2LineSaturation;
    private Double m3LineSaturation;
    private Double m4LineSaturation;
    private Double m5LineSaturation;
    private Double m6LineSaturation;
    private Double m7LineSaturation;
    private Double m8LineSaturation;
    private Double m9LineSaturation;
    private Double m10LineSaturation;
    private Double m11LineSaturation;
    private Double m12LineSaturation;
}
