package com.example.validation.dto;

import com.example.validation.util.enums.SlobType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class RscItPMSlobDTO {
    private Long id;
    private Long rscMtItemId;
    private String rscDtItemCode;
    private Long rscMtPPHeaderId;
    private Double mapPrice;
    private Double stdPrice;
    private Double price;
    private Double midNightStockQuantity;
    private Double stockQuantity;
    private Double stockValue;
    private Double slobQuantity;
    private Double slobValue;
    private Long rscItItemWiseSupplierId;
    private Double totalConsumptionQuantity;
    @Enumerated(EnumType.STRING)
    private SlobType slobType;
    private Long rscDtReasonId;
    private String rscDtReasonName;
    private String remark;
}
