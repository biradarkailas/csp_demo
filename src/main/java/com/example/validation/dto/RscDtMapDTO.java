package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtMapDTO {
    private Long id;
    private Double mapPrice;
}
