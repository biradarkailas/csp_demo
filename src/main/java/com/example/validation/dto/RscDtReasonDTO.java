package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtReasonDTO {
    private Long id;
    private String reason;
    private String description;
    private Long rscDtReasonTypeId;
    private String reasonType;
}
