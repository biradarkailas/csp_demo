package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ResourceTableUploadSummaryDTO {
    private Long id;
    private String tableName;
    private String procedureName;
    private LocalDateTime startLoadDate;
    private LocalDateTime endLoadDate;
    private int recordCount;
    private Boolean isLoadSuccess;
}

