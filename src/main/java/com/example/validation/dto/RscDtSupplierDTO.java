package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtSupplierDTO {
    private Long id;
    private String supplierName;
    private String supplierCode;
    private boolean active;
}
