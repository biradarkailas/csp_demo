package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class IndividualValuesMesDTO {
    private LocalDate stockDate;
    private String stockStatus;
    private String lotNumber;
    private LocalDate expirationDate;
    private Double value;
}