package com.example.validation.dto;

import lombok.Data;

@Data
public class MPSDeviationDTO {
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private Double m13Value;
    private Double m14Value;
    private Double m15Value;
    private Double m16Value;
    private Double m17Value;
    private Double m18Value;
    private Double m19Value;
    private Double m20Value;
    private Double m21Value;
    private Double m22Value;
    private Double m23Value;
}
