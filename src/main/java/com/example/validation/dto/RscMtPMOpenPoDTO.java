package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscMtPMOpenPoDTO {
    private Long id;
    private String poNumber;
    private LocalDate poDate;
    private Double orderedQuantity;
    private Double receivedQuantity;
    private LocalDate deliveryDate;
    private Integer paymentMode;
    private LocalDate dueDate;
    private Double rate;
    private String currency;
    private LocalDate sailingDate;
    private LocalDate landingDate;
    private Long rscMtItemId;
    private Long rscMtSupplierId;
    private Long mpsId;
}