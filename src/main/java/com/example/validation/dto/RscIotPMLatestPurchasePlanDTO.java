package com.example.validation.dto;

import com.example.validation.domain.RscItItemWiseSupplier;
import com.example.validation.domain.RscMtItem;
import com.example.validation.domain.RscMtPPheader;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Data
public class RscIotPMLatestPurchasePlanDTO {
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private RscMtItem rscMtItem;
    private RscMtPPheader rscMtPPheader;
    private RscItItemWiseSupplier rscItItemWiseSupplier;
}
