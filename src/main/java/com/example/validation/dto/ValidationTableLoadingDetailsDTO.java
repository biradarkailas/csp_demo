package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ValidationTableLoadingDetailsDTO {
    private Long id;
    private String inputFileName;
    private String procedureName;
    private boolean isValidated;
    private LocalDateTime startLoadDate;
    private LocalDateTime endLoadDate;
    private int recordCount;
}