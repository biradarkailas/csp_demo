package com.example.validation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RmBulkOpRmStockPaginationDto {
    private String bulkCode;
    private String bulkDescription;
    private String rmCode;
    private String rmDescription;
    private Long bulkItemId;
    private Long rmItemId;
    private Double bulkTotalQuantity;
    private Double bulkWiseRmQuantity;
}
