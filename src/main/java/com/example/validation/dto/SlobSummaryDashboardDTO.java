package com.example.validation.dto;

import lombok.Data;

@Data
public class SlobSummaryDashboardDTO {
    private Double totalStockValue;
    private Double totalSlobValue;
    private Double obValue;
    private Double slValue;
    private Double inventoryQualityValue;
    private Double obPercentage;
    private Double slPercentage;
    private Double iqPercentage;
}