package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class PMSlobDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private Double totalStockValue;
    private Double totalSlowItemValue;
    private Double totalObsoleteItemValue;
    private Double sitValue;
    private List<PmSlobDTO> pmSlobs;
}
