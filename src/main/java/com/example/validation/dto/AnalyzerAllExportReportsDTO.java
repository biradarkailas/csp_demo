package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class AnalyzerAllExportReportsDTO {
    private List<String> fgFileList;
    private List<String> pmFileList;
    private List<String> rmFileList;
    private List<String> pmOtifFileList;
    private List<String> fgAnnualFileList;
    private List<String> pmAnnualFileList;
    private List<String> rmAnnualFileList;
}
