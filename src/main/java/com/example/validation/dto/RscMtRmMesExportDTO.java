package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtRmMesExportDTO {
    private LocalDate stockDate;
    private Double quantity;
    private String lotNumber;
    private String location;
    private String stockStatus;
    private String documentNumber;
    private String site;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Double safetyStock;
}
