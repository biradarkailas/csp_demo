package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class MouldSaturationDashboardDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private String highSaturationRange;
    private String mediumSaturationRange;
    private String lowSaturationRange;
    List<MouldSaturationDetailsDTO> highSaturationRangeMouldsDTOList;
    List<MouldSaturationDetailsDTO> mediumSaturationRangeMouldsDTOList;
    List<MouldSaturationDetailsDTO> lowSaturationRangeMouldsDTOList;
    private Integer highSaturationRangeListCount;
    private Integer mediumSaturationRangeListCount;
    private Integer lowSaturationRangeListCount;
    private Integer totalCount;
}
