package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class FgTypesDTO {
    private Long id;
    private String name;
    private Long parentId;
    private String uniqueName;
    private String cssClass = "nodeColor";
    private List<FgTypesDTO> childs;
}
