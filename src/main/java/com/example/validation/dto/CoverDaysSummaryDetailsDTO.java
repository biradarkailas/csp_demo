package com.example.validation.dto;

import lombok.Data;

@Data
public class CoverDaysSummaryDetailsDTO {
    private Long rscMtPPHeaderId;
    private CoverDaysInMonthDTO coverDaysInMonthDTO;
    private CoverDaysConsumptionDTO coverDaysConsumptionDTO;
    private CoverDaysDTO coverDaysDTO;
    private Integer totalCoverDays;
    private Integer totalDaysOfMonths;
    private Integer totalCoverDaysWithSit;
    private Double totalStockValue;
    private Double sitValue;
    private Double totalStockSitValue;
    private String openingMonthValue;
  /*  private Double classATotalStockValue;
    private Integer classATotalCoverDays;
    private Double classBTotalStockValue;
    private Integer classBTotalCoverDays;
    private Double classCTotalStockValue;
    private Integer classCTotalCoverDays;*/
}
