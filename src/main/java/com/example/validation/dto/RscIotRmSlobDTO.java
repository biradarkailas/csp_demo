package com.example.validation.dto;

import com.example.validation.util.enums.SlobType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class RscIotRmSlobDTO {
    private Long id;
    private Long rscMtPPheaderId;
    private Long rscMtItemId;
    private String rscDtItemCode;
    private Long rscItItemWiseSupplierId;
    private Long rscItRMGrossConsumptionId;
    private Double mapPrice;
    private Double stdPrice;
    private Double price;
    private Double stockQuantity;
    private Double stockValue;
    private Double expiryStockQuantity;
    private Double expirySlobValue;
    private Double totalStockQuantity;
    private Double totalStockValue;
    private Double totalConsumptionQuantity;
    private Double slobQuantity;
    private Double slobValue;
    private Double balanceStock;
    private Double totalSlobValue;
    @Enumerated(EnumType.STRING)
    private SlobType slobType;
    private Double obSlobValue;
    private Double slowSlobValue;
    private Double totalObValue;
    private Long rscDtReasonId;
    private String remark;
}