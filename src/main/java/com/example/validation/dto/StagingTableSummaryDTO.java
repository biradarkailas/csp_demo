package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class StagingTableSummaryDTO {
    private Long id;
    private String tableName;
    private boolean isStgLoaded;
    private boolean isSanitize;
    private boolean isValidate;
    private boolean isActive;
    private boolean isRscLoaded;
    private LocalDateTime startStgLoadDate;
    private LocalDateTime endStgLoadDate;
    private LocalDateTime startRscLoadDate;
    private LocalDateTime endRscLoadDate;
    private LocalDateTime startValidateDate;
    private LocalDateTime endValidateDate;
    private int recordCount;
    private LocalDate mpsDate;
}
