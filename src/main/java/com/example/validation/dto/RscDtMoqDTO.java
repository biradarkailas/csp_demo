package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtMoqDTO {
    private Long id;
    private double moqValue;
}
