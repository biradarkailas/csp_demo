package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtSailingDaysDTO {
    private Long id;
    private double sailingDays;
}
