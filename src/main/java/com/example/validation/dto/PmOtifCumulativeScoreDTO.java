package com.example.validation.dto;

import lombok.Data;

@Data
public class PmOtifCumulativeScoreDTO {
    private Long id;
    private Long supplierId;
    private String supplierName;
    private Double averagePercentage;
    private Double maxPercentage;
    private Double minPercentage;
    private Double score;
}
