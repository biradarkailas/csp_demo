package com.example.validation.dto;

import com.example.validation.util.enums.ModuleName;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscIitIdpStatusDTO {
    private Long id;
    private String mpsName;
    private LocalDate mpsDate;
    private Boolean uploadStatus;
    private Boolean validationStatus;
    private Boolean resourceStatus;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Boolean isActive;
    private Boolean isRequired;
    private Long fileNameId;
    private String fileName;
    private String extension;
    private Long folderNameId;
    private String folderName;
    private Long rscDtTagId;
    private String tagName;
    private String tagDescription;
    private LocalDate otifDate;
    private Boolean isExecutionDone;
}
