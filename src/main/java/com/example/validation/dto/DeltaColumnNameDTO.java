package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class DeltaColumnNameDTO {
    List<String> columnNames;
    List<String> rowNames;
}