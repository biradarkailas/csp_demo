package com.example.validation.dto;

import lombok.Data;

@Data
public class AnalyzerExportFileDTO {
    private ExcelExportFiles excelExportFiles;
    private String mediaType;
}
