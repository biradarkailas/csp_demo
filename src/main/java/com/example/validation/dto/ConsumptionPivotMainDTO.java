package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class ConsumptionPivotMainDTO {
    private Long mpsId;
    private String mpsName;
    private List<ConsumptionPivotDTO> consumptionPivotDTOs;
}