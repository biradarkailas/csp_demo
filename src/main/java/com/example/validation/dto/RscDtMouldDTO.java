package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtMouldDTO {
    private Long id;
    private String mould;
    private String childMould;
    private double noOfCavities;
    private double perCavity;
    private double totalCapacity;
    private boolean active;
}
