package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RMOpenPoDTO {
    private String poNumber;
    private LocalDate poDate;
    private double orderedQuantity;
    private LocalDate landingDate;
}
