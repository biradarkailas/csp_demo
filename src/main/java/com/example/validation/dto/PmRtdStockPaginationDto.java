package com.example.validation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PmRtdStockPaginationDto {
    private String itemCode;
    private String itemDescription;
    private LocalDate receiptDate;
    private Long rscMtItemId;
    private Double totalReceiptStock;
    private Double lotWiseReceiptStock;
    private String lotNumber;
    private String receiptNumber;
    private String supplierName;
    private String poNumber;
}