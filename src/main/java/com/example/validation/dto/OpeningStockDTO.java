package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class OpeningStockDTO {
    private Double value;
    private StockTypeDTO stockTypes;
}
