package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class FgTypesAndDivisionsDTO {
    private Long id;
    private String name;
    private Long parentId;
  private String cssClass = "nodeColor";
    private String uniqueName= "ALL";
    private List<FgTypesDTO> childs;
}
