package com.example.validation.dto;

import lombok.Data;

@Data
public class ItemCodeDetailDTO {
    private Long itemId;
    private String itemCode;
    private String itemDescription;
    private OpeningStockDTO openingStockDTO;
    private String formula;
    private String formulaValue;
}
