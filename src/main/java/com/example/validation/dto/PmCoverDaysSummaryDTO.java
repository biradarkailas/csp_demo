package com.example.validation.dto;

import lombok.Data;

@Data
public class PmCoverDaysSummaryDTO {
    private Long rscMtPPHeaderId;
    private Double month1ConsumptionTotalValue;
    private Double month2ConsumptionTotalValue;
    private Double month3ConsumptionTotalValue;
    private Double month4ConsumptionTotalValue;
    private Double month5ConsumptionTotalValue;
    private Double month6ConsumptionTotalValue;
    private Double month7ConsumptionTotalValue;
    private Double month8ConsumptionTotalValue;
    private Double month9ConsumptionTotalValue;
    private Double month10ConsumptionTotalValue;
    private Double month11ConsumptionTotalValue;
    private Double month12ConsumptionTotalValue;
}
