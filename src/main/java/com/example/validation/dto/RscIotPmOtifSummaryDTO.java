package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotPmOtifSummaryDTO {
    private LocalDate otifSummaryDate;
    private Long rscDtSupplierId;
    private String supplierName;
    private String supplierCode;
    private Long rscMtPPheaderId;
    private LocalDate receiptHeaderDate;
    private Double avgReceiptPercentage;
    private Integer avgScore;
}
