package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class PMMesDTO {
    private LocalDate stockDate;
    private String stockStatus;
    private String lotNumber;
    private double quantity;
}
