package com.example.validation.dto;

import lombok.Data;

@Data
public class TotalSlowItemValueDTO {
    private String totalSlowItemValueFormula;
    private String totalSlowItemFormulaValue;
    private Double totalSlowItemValue;
    private String slowItemPercentageFormula;
    private String slowItemPercentageFormulaValue;
    private Double slowItemPercentage;
}
