package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtTechnicalSeriesDTO {
    private Long id;
    private double seriesValue;
}
