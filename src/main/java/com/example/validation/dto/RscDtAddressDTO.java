package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtAddressDTO {
    private Long id;
    private String addressLine1;
    private String addressLine2;
    private String area;
    private String pincode;
    private Long rscDtCityId;
}
