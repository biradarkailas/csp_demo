package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class LinesDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private List<LinesDTO> linesDTOList;
}
