package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class OtifCumulativeScoreAnalysisDTO {
    private List<OtifCumulativeScoreByMonthDTO> otifCumulativeScoreByMonthDTOS;
    private Double averageReceiptPercentage;
    private Double scoreByAverageReceipt;
}

