package com.example.validation.dto;

import com.example.validation.config.constants.MaterialCategoryConstants;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class PmStockDetailDTO {
    private String mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private List<PmStockDetailDTO.PmStockDetailItemCode> itemCodes;

    @Data
    public class PmStockDetailItemCode {
        Long id;
        String code;
        String description;
        String formula = MaterialCategoryConstants.PM_STOCK_DETAIL_FORMULA;
        StringBuilder formulaValue;
        OpeningStock openingStock;

        @Data
        public class OpeningStock {
            Double value;
            StockType stockType;

            @Data
            public class StockType {
                Mes mes;
                Rtd rtd;

                @Data
                public class Mes {
                    String name;
                    String aliasName;
                    Double totalValue;
                    List<MesIndividualValue> individualValues;

                    @Data
                    public class MesIndividualValue {
                        LocalDate stockDate;
                        String stockStatus;
                        Double value;
                        String lotNumber;
                    }
                }

                @Data
                public class Rtd {
                    String name;
                    String aliasName;
                    Double totalValue;
                    List<RtdIndividualValue> individualValues;

                    @Data
                    public class RtdIndividualValue {
                        LocalDate receiptDate;
                        String supplierName;
                        Double value;
                        String lotNumber;
                        String supplierLotNumber;
                        String poNumber;
                    }
                }
            }
        }
    }
}
