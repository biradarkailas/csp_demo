package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class BackupPlanList {
    private List<String> firstCutFileList;
    private List<String> validatedFileList;

}
