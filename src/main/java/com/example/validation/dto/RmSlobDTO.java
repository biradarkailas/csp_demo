package com.example.validation.dto;

import com.example.validation.util.enums.SlobType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class RmSlobDTO {
    private Long id;
    private Long rscMtItemId;
    private Long rscMtPPheaderId;
    private Long rscItItemWiseSupplierId;
    private Long rscItRMGrossConsumptionId;
    private String code;
    private String description;
    private String supplier;
    private Double midNightStock;
    private Double expiredStock;
    private Double finalStock;
    private Double totalConsumptionQuantity;
    private Double slobQuantity;
    private Double map;
    private Double stdPrice;
    private Double price;
    private Double stockValue;
    private Double totalStockValue;
    private Double slobValue;
    private Double balanceStock;
    private Double totalSlobValue;
    @Enumerated(EnumType.STRING)
    private SlobType slobType;
    private Double obsoleteExpiryValue;
    private Double obsoleteNonExpiryValue;
    private Double slowMovingValue;
    private Double totalObValue;
    private Long reasonId;
    private String reason;
    private String remark;
    private MonthValuesDTO monthValuesDTO;
    private Double PreviousMonthsSlobValue;
    private Double PreviousMonthsObsoleteExpiryValue;
    private Double PreviousMonthsObsoleteNonExpiryValue;
    private Double PreviousMonthsSlowMovingValue;
}
