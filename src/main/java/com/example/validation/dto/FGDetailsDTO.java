package com.example.validation.dto;

import lombok.Data;

@Data
public class FGDetailsDTO {
    private String fgCode;
    private String fgDescription;
}