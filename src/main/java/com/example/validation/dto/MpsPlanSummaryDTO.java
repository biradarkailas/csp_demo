package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class MpsPlanSummaryDTO {
    private Long ppHeaderId;
    private List divisionList;
    private List signatureList;
    private List brandList;
    private List category1List;
    private List category2List;
    private List linesList;

}
