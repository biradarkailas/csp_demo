package com.example.validation.dto;

import com.example.validation.domain.RscDtMould;
import com.example.validation.domain.RscMtItem;
import lombok.Data;

import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Data
public class RscItItemMouldDTO {
    private LocalDateTime createdDate;
    private java.time.LocalDateTime modifiedDate;
    private Long rscMtItemId;
    private Long rscDtMouldId;
}
