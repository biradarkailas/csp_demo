package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtStdPriceDTO {
    private Long id;
    private double stdPrice;
}
