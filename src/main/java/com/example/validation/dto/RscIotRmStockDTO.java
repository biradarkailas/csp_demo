package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotRmStockDTO {
    private Long id;
    private Double quantity;
    private LocalDate stockDate;
    private LocalDate arrivalDateForPo;
    private LocalDate mpsDate;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtStockTypeId;
    private String stockTypeTag;
    private String stockTypeAliasName;
    private Double mapPrice;
    private Double stdPrice;
}
