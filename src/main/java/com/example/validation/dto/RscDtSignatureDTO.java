package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtSignatureDTO {
    private Long id;
    private String name;
}
