package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotFGStockDTO {
    private Long id;
    private Double quantity;
    private LocalDate stockDate;
    private Long rscMtItemId;
    private String itemCode;
    private String itemCategoryDescription;
    private Long rscDtStockTypeId;
    private String stockTypeTag;
    private String stockTypeAlias;
    private Double mapPrice;
}