package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RscMtSupplierDTO {
    private Long id;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
}
