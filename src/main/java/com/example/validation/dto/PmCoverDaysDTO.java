package com.example.validation.dto;

import lombok.Data;

@Data
public class PmCoverDaysDTO {
    private Long id;
    private Long rscMtItemId;
    private Long rscMtPPheaderId;
    private Long rscItItemWiseSupplierId;
    private Long rscItPMGrossConsumptionId;
    private String code;
    private String description;
    private String supplier;
    private String category;
    private Double stockQuantity;
    private Double stockValue;
    private Double stdPrice;
    private Double map;
    private Double rate;
    private CoverDaysMonthDetailsDTO coverDaysMonthDetailsDTO;
}
