package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class DeltaAnalysisDetailsDTO {
    private String saturationMonth;
    private List<Double> saturationList;
}
