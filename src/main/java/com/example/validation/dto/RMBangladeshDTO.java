package com.example.validation.dto;

import lombok.Data;

@Data
public class RMBangladeshDTO {
    private Double quantity;
}
