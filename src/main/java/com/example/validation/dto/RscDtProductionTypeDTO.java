package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtProductionTypeDTO {
    private Long id;
    private String name;
}
