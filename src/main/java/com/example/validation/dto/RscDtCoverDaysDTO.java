package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtCoverDaysDTO {
    private Long id;
    private double coverDays;
}
