package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class SlobSummaryDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private String currentMonthName;
    private String previousMonthName;
    private SlobSummaryValueDetailsDTO totalObsoleteItemValueDetails;
    private SlobSummaryValueDetailsDTO totalObsoleteExpiryValueDetails;
    private SlobSummaryValueDetailsDTO totalObsoleteNonExpiryValueDetails;
    private SlobSummaryValueDetailsDTO totalSlowItemValueDetails;
    private SlobSummaryValueDetailsDTO totalSlobValueDetails;
    private SlobSummaryValueDetailsDTO totalStockValueDetails;
    private SlobSummaryValueDetailsDTO stockQualityValueDetails;
    private SlobSummaryValueDetailsDTO slowItemPercentageDetails;
    private SlobSummaryValueDetailsDTO obsoleteItemPercentageDetails;
    private SlobSummaryValueDetailsDTO stockQualityPercentageDetails;
    private SlobSummaryValueDetailsDTO obsoleteExpiryValuePercentageDetails;
    private SlobSummaryValueDetailsDTO obsoleteNonExpiryValuePercentageDetails;
}
