package com.example.validation.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class SapTableNameDTO implements Serializable {
    private Long id;
    private String tableName;
    private String tableColumns;
    private boolean isActive;
}
