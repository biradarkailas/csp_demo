package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtItem2DTO {
    private Long id;
    private String bpiCode;
    private String baseCode;
}