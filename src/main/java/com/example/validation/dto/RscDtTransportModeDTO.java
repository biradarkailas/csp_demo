package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtTransportModeDTO {
    private Long id;
    private String code;
    private String description;
}
