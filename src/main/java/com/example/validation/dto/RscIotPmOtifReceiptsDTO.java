package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscIotPmOtifReceiptsDTO {
    private Long id;
    private LocalDate receiptDate;
    private Long rscMtItemId;
    private Long rscDtSupplierId;
    private Long rscMtOtifPmReceiptsHeaderId;
    private Double quantity;
    private LocalDate createdDate;
    private LocalDate modifiedDate;
}
