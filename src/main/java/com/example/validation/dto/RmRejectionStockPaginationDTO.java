package com.example.validation.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RmRejectionStockPaginationDTO {
    private String code;
    private String description;
    private LocalDate dateOfStock;
    private Long mtItemId;
    private Double stockQuantity;
    private Double lotWiseStockQuantity;
}
