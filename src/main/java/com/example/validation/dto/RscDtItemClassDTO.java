package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtItemClassDTO {
    private Long id;
    private String aliasName;
    private String fullName;
}
