package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtContactDetailsDTO {
    private Long id;
    private String mobileNo1;
    private String mobileNo2;
    private String landlineNo1;
    private String landlineNo2;
    private String fax;
    private String emailId1;
    private String emailId2;
}
