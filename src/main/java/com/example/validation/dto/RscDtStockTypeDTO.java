package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtStockTypeDTO {
    private Long id;
    private String aliasName;
    private String fullName;
    private String tag;
}
