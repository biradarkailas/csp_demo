package com.example.validation.dto;

import lombok.Data;

@Data
public class MonthNameDTO {
    String monthName;
    String monthNameAlias;
}
