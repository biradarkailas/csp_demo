package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtTransportTypeDTO {
    private Long id;
    private String aliasName;
    private String fullName;
}
