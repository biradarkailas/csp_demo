package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class OtifCumulativeSuppliersDetailsDTO {
    private List<PmOtifCumulativeScoreDTO> score4SuppliersList;
    private List<PmOtifCumulativeScoreDTO> score3SuppliersList;
    private List<PmOtifCumulativeScoreDTO> score2SuppliersList;
    private List<PmOtifCumulativeScoreDTO> score1SuppliersList;
    private List<PmOtifCumulativeScoreDTO> score0SuppliersList;
}
