package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscMtPmOtifReceiptsDTO {
    private Long id;
    private Long rscMtItemId;
    private Long  rscDtSupplierId;
    private Long  rscMtOtifPmReceiptsHeaderId;
    private LocalDate receiptDate;
    private Double quantity;
    private String receiptNumber;
    private String orderNumber;
    private String packingSlipNumber;
    private String supplierLotNumber;
    private String lotNumber;
    private String poNumber;
    private LocalDate createdDate;
    private LocalDate modifiedDate;

}
