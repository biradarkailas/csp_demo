package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtCityDTO {
    private Long id;
    private String name;
    private Long rscDtStateId;
}
