package com.example.validation.dto;

import com.example.validation.util.enums.ModuleName;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtPPheaderDTO {
    private Long id;
    private String mpsName;
    private LocalDate mpsDate;
    private Boolean active;
    private LocalDate otifDate;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private String tagId;
    private String tagName;
    private String tagDescription;
    private Boolean isMpsBackup;
    private Boolean isExecutionDone;
    private Boolean isBackUpDone;

    @Enumerated(EnumType.STRING)
    private ModuleName moduleName;

}