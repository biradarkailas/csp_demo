package com.example.validation.dto;

import com.example.validation.util.enums.SlobType;
import lombok.Data;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Data
public class PmSlobDTO {
    private Long id;
    private Long rscMtItemId;
    private Long rscMtPPheaderId;
    private Long rscItItemWiseSupplierId;
    private String code;
    private String description;
    private String supplier;
    private String category;
    private Double stock;
    private Double map;
    private Double stdPrice;
    private Double price;
    private Double value;
    private Double totalStock;
    private Double slobQuantity;
    private Double slobValue;
    @Enumerated(EnumType.STRING)
    private SlobType slobType;
    private String conclusion;
    private Long reasonId;
    private String reason;
    private String remark;

}
