package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtBrandDTO {
    private Long id;
    private String name;
}
