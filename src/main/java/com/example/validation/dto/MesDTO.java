package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class MesDTO {
    private String name;
    private String aliasName;
    private Double totalValue;
    private List<IndividualValuesMesDTO> individualValuesMesDTOList;
}