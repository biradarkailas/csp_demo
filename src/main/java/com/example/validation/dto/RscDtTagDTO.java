package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtTagDTO {
    private Long id;
    private String name;
    private String description;
    private Boolean isBackup;
}
