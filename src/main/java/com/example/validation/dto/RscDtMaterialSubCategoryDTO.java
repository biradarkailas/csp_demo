package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtMaterialSubCategoryDTO {
    private Long id;
    private String name;
}
