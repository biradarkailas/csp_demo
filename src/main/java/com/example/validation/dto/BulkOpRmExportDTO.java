package com.example.validation.dto;

import lombok.Data;

@Data
public class BulkOpRmExportDTO {
    private Double quantity;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Double safetyStock;
}
