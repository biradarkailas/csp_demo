package com.example.validation.dto;

import lombok.Data;

@Data
public class SlobMovementDTO {
    public Double obValue;
    public Double slValue;
    public Double slobValue;
    private String monthValue;
}
