package com.example.validation.dto;

import lombok.Data;

@Data
public class TotalObsoleteItemValueDTO {
    private String totalObsoleteItemValueFormula;
    private String totalObsoleteItemFormulaValue;
    private Double totalObsoleteItemValue;
    private String obsoleteItemPercentageFormula;
    private String obsoleteItemPercentageFormulaValue;
    private Double obsoleteItemPercentage;
}
