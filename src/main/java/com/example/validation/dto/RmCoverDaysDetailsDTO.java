package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.List;
@Data
public class RmCoverDaysDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private List<RmCoverDaysDTO> rmCoverDaysDTO;
}
