package com.example.validation.dto;

import lombok.Data;

@Data
public class SlobInventoryQualityIndexDTO {
    public Double slobPercentage;
    public Double inventoryQualityPercentage;
    public Double stockValue;
    public Double slobValue;
    public Double inventoryQualityValue;
    private String monthValue;
}
