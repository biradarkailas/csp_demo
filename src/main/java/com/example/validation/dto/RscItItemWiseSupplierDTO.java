package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscItItemWiseSupplierDTO {
    private Long id;
    private LocalDate applicableFrom;
    private LocalDate applicableTo;
    private Double allocationPercentage;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtTechnicalSeriesId;
    private Double seriesValue;
    private Long rscDtMoqId;
    private Double moqValue;
    private Long rscMtSupplierId;
    private String supplierName;
    private String supplierCode;
    private Long rscDtContainerTypeId;
    private String containerType;
}