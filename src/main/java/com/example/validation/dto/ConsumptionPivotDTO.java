package com.example.validation.dto;

import lombok.Data;

import java.util.List;

@Data
public class ConsumptionPivotDTO {
    private Long childItemId;
    private String childItemCode;
    private String childItemDescription;
    private List<ConsumptionItemDetailsDTO> consumptionItemDetailsDTOs;
    private Double m1TotalValue;
    private Double m2TotalValue;
    private Double m3TotalValue;
    private Double m4TotalValue;
    private Double m5TotalValue;
    private Double m6TotalValue;
    private Double m7TotalValue;
    private Double m8TotalValue;
    private Double m9TotalValue;
    private Double m10TotalValue;
    private Double m11TotalValue;
    private Double m12TotalValue;
    private Double totalValue;
}