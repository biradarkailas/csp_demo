package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotFgSkidSaturationDTO {
    private Long id;
    private Long rscMtPPheaderId;
    private Long rscDtSkidsId;
    private Double m1TotalRatioOfPlanQtyWithTs;
    private Double m2TotalRatioOfPlanQtyWithTs;
    private Double m3TotalRatioOfPlanQtyWithTs;
    private Double m4TotalRatioOfPlanQtyWithTs;
    private Double m5TotalRatioOfPlanQtyWithTs;
    private Double m6TotalRatioOfPlanQtyWithTs;
    private Double m7TotalRatioOfPlanQtyWithTs;
    private Double m8TotalRatioOfPlanQtyWithTs;
    private Double m9TotalRatioOfPlanQtyWithTs;
    private Double m10TotalRatioOfPlanQtyWithTs;
    private Double m11TotalRatioOfPlanQtyWithTs;
    private Double m12TotalRatioOfPlanQtyWithTs;
    private Double month1Capacity;
    private Double month2Capacity;
    private Double month3Capacity;
    private Double month4Capacity;
    private Double month5Capacity;
    private Double month6Capacity;
    private Double month7Capacity;
    private Double month8Capacity;
    private Double month9Capacity;
    private Double month10Capacity;
    private Double month11Capacity;
    private Double month12Capacity;
    private Double m1SkidSaturation;
    private Double m2SkidSaturation;
    private Double m3SkidSaturation;
    private Double m4SkidSaturation;
    private Double m5SkidSaturation;
    private Double m6SkidSaturation;
    private Double m7SkidSaturation;
    private Double m8SkidSaturation;
    private Double m9SkidSaturation;
    private Double m10SkidSaturation;
    private Double m11SkidSaturation;
    private Double m12SkidSaturation;
}
