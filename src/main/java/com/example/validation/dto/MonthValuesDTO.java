package com.example.validation.dto;

import lombok.Data;

@Data
public class MonthValuesDTO {
    private Double month1Value;
    private Double month2Value;
    private Double month3Value;
    private Double month4Value;
    private Double month5Value;
    private Double month6Value;
    private Double month7Value;
    private Double month8Value;
    private Double month9Value;
    private Double month10Value;
    private Double month11Value;
    private Double month12Value;
}
