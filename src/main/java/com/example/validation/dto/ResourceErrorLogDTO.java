package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ResourceErrorLogDTO {
    private Long id;
    private String procedureName;
    private String fieldName;
    private String errorDescription;
    private LocalDateTime createdDate;
}
