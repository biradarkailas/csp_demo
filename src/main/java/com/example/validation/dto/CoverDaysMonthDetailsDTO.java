package com.example.validation.dto;

import lombok.Data;

@Data
public class CoverDaysMonthDetailsDTO {
    private Double month1ConsumptionQuantity;
    private Double month2ConsumptionQuantity;
    private Double month3ConsumptionQuantity;
    private Double month4ConsumptionQuantity;
    private Double month5ConsumptionQuantity;
    private Double month6ConsumptionQuantity;
    private Double month7ConsumptionQuantity;
    private Double month8ConsumptionQuantity;
    private Double month9ConsumptionQuantity;
    private Double month10ConsumptionQuantity;
    private Double month11ConsumptionQuantity;
    private Double month12ConsumptionQuantity;
    private Double month1Value;
    private Double month2Value;
    private Double month3Value;
    private Double month4Value;
    private Double month5Value;
    private Double month6Value;
    private Double month7Value;
    private Double month8Value;
    private Double month9Value;
    private Double month10Value;
    private Double month11Value;
    private Double month12Value;
}
