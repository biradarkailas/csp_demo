package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

@Data
public class OtifCalculationDetailsDTO {
    private Long mpsId;
    private String mpsName;
    private LocalDate mpsDate;
    private String currentMonth;
    private String previousMonth;
    private LocalDate otifDate;
    private List<RscIotPmOtifCalculationDetailsDTO> otifCalculationDetailsDTO;
}
