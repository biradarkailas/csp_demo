package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class RscMtPPdetailsDTO {
    private Long id;
    private Double m1Idealvalue;
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private Double total;
    private boolean priority;
    private Long rscMtPPheaderId;
    private String mpsName;
    private LocalDate mpsDate;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtProductionTypeId;
    private String productionType;
    private Long rscDtLinesId;
    private String lineName;
    private Long rscDtSkidsId;
    private String skidName;
    private Long rscDtDivisionId;
    private String divisionName;
    private Long rscDtBrandId;
    private String brandName;
    private Long rscDtSignatureId;
    private String signatureName;
    private Long rscDtMaterialCategoryId;
    private String materialCategoryName;
    private Long rscDtMaterialSubCategoryId;
    private String materialSubCategoryName;
}
