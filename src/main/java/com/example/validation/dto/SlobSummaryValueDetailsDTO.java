package com.example.validation.dto;

import lombok.Data;

@Data
public class SlobSummaryValueDetailsDTO {
    private Double currentMonthValue;
    private Double previousMonthValue;
    private Double deviationTotal;
    private String deviationStatus;

}
