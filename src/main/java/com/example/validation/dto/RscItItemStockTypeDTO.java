package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class RscItItemStockTypeDTO {
    private Long id;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
}
