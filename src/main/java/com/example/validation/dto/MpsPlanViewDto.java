package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class MpsPlanViewDto {
    private String mpsName;
    private String mpsDateStr;
    private LocalDate mpsDate;
    private Long rscDtTagId;
    private String tagName;
    private String tagDescription;
    private Boolean enable;
    private Boolean pmOTIFEnable;
    private Boolean editable;
    private LocalDateTime modifiedDate;
    private List<RscIitIdpStatusDTO> rscIitIdpStatusDTOs;
    private LocalDate otifDate;
    private Boolean isExecutionDone;
    private Boolean executionEnable;
}