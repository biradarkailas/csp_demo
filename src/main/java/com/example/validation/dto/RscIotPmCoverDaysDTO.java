package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotPmCoverDaysDTO {
    private Long id;
    private Long rscMtItemId;
    private Long rscMtPPHeaderId;
    private Long rscItItemWiseSupplierId;
    private Long rscItPMGrossConsumptionId;
    private Double stdPrice;
    private Double map;
    private Double rate;
    private Double stockQuantity;
    private Double stockValue;
    private Double month1ConsumptionValue;
    private Double month2ConsumptionValue;
    private Double month3ConsumptionValue;
    private Double month4ConsumptionValue;
    private Double month5ConsumptionValue;
    private Double month6ConsumptionValue;
    private Double month7ConsumptionValue;
    private Double month8ConsumptionValue;
    private Double month9ConsumptionValue;
    private Double month10ConsumptionValue;
    private Double month11ConsumptionValue;
    private Double month12ConsumptionValue;
}
