package com.example.validation.dto;

import lombok.Data;

@Data
public class OtifCalculationSuppliersDTO {
    private Long rscDtSupplierId;
    private String supplierName;
    private Double receiptPercentage;
}
