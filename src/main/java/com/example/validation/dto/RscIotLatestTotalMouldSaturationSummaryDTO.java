package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotLatestTotalMouldSaturationSummaryDTO {
    private Long id;
    private Long rscMtPPheaderId;
    private Long rscIotLatestTotalMouldSaturationId;
    private Double threeMonthsAverage;
    private Double sixMonthsAverage;
    private Double twelveMonthsAverage;
}
