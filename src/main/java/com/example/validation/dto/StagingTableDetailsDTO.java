package com.example.validation.dto;

import lombok.Data;

@Data
public class StagingTableDetailsDTO {
    private Long id;
    private String csvFileName;
    private String tableName;
    private boolean hasHeader;
    private int executionSequence;
    private String validationProcedureName;
    private int resourceSequence;
    private String resourceProcedureName;
}
