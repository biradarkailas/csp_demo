package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotPmLatestSlobReasonDTO {
    private Long id;
    private Long rscMtItemId;
    private Long rscDtReasonId;
    private String remark;
}
