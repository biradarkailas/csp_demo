package com.example.validation.dto;

import lombok.Data;

@Data
public class LatestPurchasePlanDTO {
    private Long id;
    private Long rscMtPPheaderId;
    private Long mouldId;
    private String mould;
    private Double m1Value;
    private Double m2Value;
    private Double m3Value;
    private Double m4Value;
    private Double m5Value;
    private Double m6Value;
    private Double m7Value;
    private Double m8Value;
    private Double m9Value;
    private Double m10Value;
    private Double m11Value;
    private Double m12Value;
    private Double rtdValue;
    private Double capacity;
}
