package com.example.validation.dto;


import lombok.Data;

@Data
public class RscDtUomDTO {
    private Long id;
    private String aliasName;
    private String fullName;
}
