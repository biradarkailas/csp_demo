package com.example.validation.dto;

import lombok.Data;

@Data
public class RscIotOriginalMonthValueslDTO {
    MonthValueDTO month1;
    MonthValueDTO month2;
    MonthValueDTO month3;
    MonthValueDTO month4;
    MonthValueDTO month5;
    MonthValueDTO month6;
    MonthValueDTO month7;
    MonthValueDTO month8;
    MonthValueDTO month9;
    MonthValueDTO month10;
    MonthValueDTO month11;
    MonthValueDTO month12;
    private Double totalValue;
}