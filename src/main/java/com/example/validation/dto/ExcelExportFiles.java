package com.example.validation.dto;

import lombok.Data;

import java.io.InputStream;

@Data
public class ExcelExportFiles {
    private InputStream file;
    private String fileName;
}
