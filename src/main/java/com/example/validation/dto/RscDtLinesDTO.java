package com.example.validation.dto;

import lombok.Data;

@Data
public class RscDtLinesDTO {
    private Long id;
    private String name;
    private Double capacityPerShift;
    private Double shifts;
    private Double speed;
    private Double gUpTime;
    private Double modPerLinePerShift;
    private Double hrsPerShift;
}
