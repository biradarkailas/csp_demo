package com.example.validation.dto;

import lombok.Data;

@Data
public class ResourceKeyValueDTO {
    private Long id;
    private String resourceKey;
    private String resourceValue;
    private Boolean isDelete;
}
