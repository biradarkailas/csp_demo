package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtPmMesRejectedStockDTO {
    private Long id;
    private Long rscMtItemId;
    private LocalDate stockDate;
    private Double quantity;
    private String lotNumber;
    private String location;
    private String stockStatus;
    private String documentNumber;
    private String country;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;

}
