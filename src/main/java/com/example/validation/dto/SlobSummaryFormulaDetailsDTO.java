package com.example.validation.dto;

import lombok.Data;

@Data
public class SlobSummaryFormulaDetailsDTO {
    private TotalStockSitValueDTO totalStockSitValueDTO;
    private TotalSlowItemValueDTO totalSlowItemValueDTO;
    private TotalObsoleteItemValueDTO totalObsoleteItemValueDTO;
    private TotalStockQualityValueDTO totalStockQualityValueDTO;
    private Double totalSlobValue;
    private Double totalStockValue;
    private Double sitValue;
    private String currentMonth;
    private String prevMonth;
    private Double currentMonthsTotalSlobValue;
    private Double prevMonthsTotalSlobValue;
    private Double addProvValue;
    private Double invValue;

}