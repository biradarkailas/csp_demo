package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtRmRtdExportDTO {
    private LocalDate receiptDate;
    private double quantity;
    private String receiptNumber;
    private String orderNumber;
    private String packingSlipNumber;
    private String supplierLotNumber;
    private String lotNumber;
    private String poNumber;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
    private Long rscMtItemId;
    private String itemCode;
    private String itemDescription;
    private Long rscDtSupplierId;
    private String supplierName;
    private Double safetyStock;
}
