package com.example.validation.dto;

import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
public class RscMtRmOtherStocksDTO {
    private Long id;
    private Double quantity;
    private LocalDate stock_date;
    private LocalDateTime createdDate;
    private LocalDateTime modifiedDate;
}
