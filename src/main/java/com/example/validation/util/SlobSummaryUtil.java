package com.example.validation.util;

import com.example.validation.dto.SlobSummaryValueDetailsDTO;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

@Component
public class SlobSummaryUtil {
    private final FGSlobSummaryUtil fgSlobSummaryUtil;

    public SlobSummaryUtil(FGSlobSummaryUtil fgSlobSummaryUtil) {
        this.fgSlobSummaryUtil = fgSlobSummaryUtil;
    }

    public SlobSummaryValueDetailsDTO getSlobSummaryValueDetails(Double currentMonthValue, Double previousMonthValue) {
        SlobSummaryValueDetailsDTO summaryValueDetailsDTO = new SlobSummaryValueDetailsDTO();
        summaryValueDetailsDTO.setCurrentMonthValue(currentMonthValue);
        summaryValueDetailsDTO.setPreviousMonthValue(previousMonthValue);
        Double deviationValue = getDeviationValue(currentMonthValue, previousMonthValue);
        summaryValueDetailsDTO.setDeviationTotal(deviationValue);
        summaryValueDetailsDTO.setDeviationStatus(fgSlobSummaryUtil.getDeviationStatus(deviationValue));
        return summaryValueDetailsDTO;
    }

    private Double getDeviationValue(Double currentMonthValue, Double previousMonthValue) {
        return MathUtils.sub(currentMonthValue, previousMonthValue);
    }

    public String getCurrentMonthName(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate) + " " + mpsDate.getYear() % 100 + " Total";
    }

    public String getPreviousMonthName(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate.minusMonths(1)) + " " + mpsDate.minusMonths(1).getYear() % 100 + " Total";
    }
    public String getPreviousSecondMonthName(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate.minusMonths(2)) + " " + mpsDate.minusMonths(2).getYear() % 100 + " Total";
    }
}
