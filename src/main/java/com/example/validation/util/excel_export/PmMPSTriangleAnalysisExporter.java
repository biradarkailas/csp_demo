package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.DeltaColumnNameDTO;
import com.example.validation.dto.MPSDeltaAnalysisMainDTO;
import com.example.validation.specification.RscMtPPDetailsCriteria;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmMPSTriangleAnalysisExporter {
    public static ByteArrayInputStream pmMPSTriangleAnalysisToExcelList(RscMtPPDetailsCriteria criteria, MPSDeltaAnalysisMainDTO mpsDeltaAnalysisMainDTO, LocalDate mpsDate, DeltaColumnNameDTO deltaColumnNameDTO) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.MPS_Triangle_Analysis );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printDataRows( criteria, mpsDeltaAnalysisMainDTO, deltaColumnNameDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex <= deltaColumnNameDTO.getColumnNames().size(); columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    public static void printDataRows(RscMtPPDetailsCriteria criteria, MPSDeltaAnalysisMainDTO mpsDeltaAnalysisMainDTO, DeltaColumnNameDTO deltaColumnNameDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( mpsDeltaAnalysisMainDTO ).isPresent()) {
            if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getDeviationValues() ).isPresent())) {
                int rowIdx1 = 2;
                int rowIdx2 = 0;
                int rowIdx3 = 1;
                Row rowHeader = sheet.createRow( rowIdx2 );
                Row rowMonthHeader = sheet.createRow( rowIdx3 );
                Row row1 = sheet.createRow( rowIdx1++ );
                Row row2 = sheet.createRow( rowIdx1++ );
                Row row3 = sheet.createRow( rowIdx1++ );
                Row row4 = sheet.createRow( rowIdx1++ );
                Row row5 = sheet.createRow( rowIdx1++ );
                Row row6 = sheet.createRow( rowIdx1++ );
                Row row7 = sheet.createRow( rowIdx1++ );
                Row row8 = sheet.createRow( rowIdx1++ );
                Row row9 = sheet.createRow( rowIdx1++ );
                Row row10 = sheet.createRow( rowIdx1++ );
                Row row11 = sheet.createRow( rowIdx1++ );
                Row row12 = sheet.createRow( rowIdx1++ );
                Row row13 = sheet.createRow( rowIdx1++ );
                Row row14 = sheet.createRow( rowIdx1 );
                rowHeader.createCell( 0 ).setCellValue( "MPS Triangle Analysis" );
                rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                sheet.addMergedRegion( new CellRangeAddress( 0, 0, 0, 2 ) );
                rowMonthHeader.createCell( 0 ).setCellValue( "Month" );
                rowMonthHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );

                for (int cellValue = 0, cellNo = 1; cellValue < deltaColumnNameDTO.getColumnNames().size(); cellValue++, cellNo++) {
                    if ((Optional.ofNullable( deltaColumnNameDTO.getColumnNames() ).isPresent())) {
                        rowMonthHeader.createCell( cellNo ).setCellValue( deltaColumnNameDTO.getColumnNames().get( cellValue ) );
                        rowMonthHeader.getCell( cellNo ).setCellStyle( allCellStyles.get( 1 ) );
                    } else {
                        rowMonthHeader.createCell( cellNo ).setCellValue( " " );
                        rowMonthHeader.getCell( cellNo ).setCellStyle( allCellStyles.get( 1 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM1Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM1Values().get( cellValue ) ).isPresent())) {
                            row1.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM1Values().get( cellValue ) );
                            row1.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row1.createCell( cellNo ).setCellValue( " " );
                            row1.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row1.createCell( cellNo ).setCellValue( " " );
                        row1.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM2Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM2Values().get( cellValue ) ).isPresent())) {
                            row2.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM2Values().get( cellValue ) );
                            row2.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row2.createCell( cellNo ).setCellValue( " " );
                            row2.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row2.createCell( cellNo ).setCellValue( " " );
                        row2.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM3Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM3Values().get( cellValue ) ).isPresent())) {
                            row3.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM3Values().get( cellValue ) );
                            row3.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row3.createCell( cellNo ).setCellValue( " " );
                            row3.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row3.createCell( cellNo ).setCellValue( " " );
                        row3.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM4Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM4Values().get( cellValue ) ).isPresent())) {
                            row4.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM4Values().get( cellValue ) );
                            row4.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row4.createCell( cellNo ).setCellValue( " " );
                            row4.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row4.createCell( cellNo ).setCellValue( " " );
                        row4.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM5Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM5Values().get( cellValue ) ).isPresent())) {
                            row5.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM5Values().get( cellValue ) );
                            row5.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row5.createCell( cellNo ).setCellValue( " " );
                            row5.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row5.createCell( cellNo ).setCellValue( " " );
                        row5.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM6Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM6Values().get( cellValue ) ).isPresent())) {
                            row6.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM6Values().get( cellValue ) );
                            row6.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row6.createCell( cellNo ).setCellValue( " " );
                            row6.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row6.createCell( cellNo ).setCellValue( " " );
                        row6.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM7Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM7Values().get( cellValue ) ).isPresent())) {
                            row7.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM7Values().get( cellValue ) );
                            row7.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row7.createCell( cellNo ).setCellValue( " " );
                            row7.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row7.createCell( cellNo ).setCellValue( " " );
                        row7.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM8Values() ).isPresent())) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM8Values().get( cellValue ) ).isPresent())) {
                            row8.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM8Values().get( cellValue ) );
                            row8.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row8.createCell( cellNo ).setCellValue( " " );
                            row8.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row8.createCell( cellNo ).setCellValue( " " );
                        row8.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM9Values() ).isPresent()) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM9Values().get( cellValue ) ).isPresent())) {
                            row9.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM9Values().get( cellValue ) );
                            row9.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row9.createCell( cellNo ).setCellValue( " " );
                            row9.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row9.createCell( cellNo ).setCellValue( " " );
                        row9.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM10Values() ).isPresent()) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM10Values().get( cellValue ) ).isPresent())) {
                            row10.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM10Values().get( cellValue ) );
                            row10.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row10.createCell( cellNo ).setCellValue( " " );
                            row10.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row10.createCell( cellNo ).setCellValue( " " );
                        row10.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM11Values() ).isPresent()) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM11Values().get( cellValue ) ).isPresent())) {
                            row11.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM11Values().get( cellValue ) );
                            row11.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row11.createCell( cellNo ).setCellValue( " " );
                            row11.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row11.createCell( cellNo ).setCellValue( " " );
                        row11.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM12Values() ).isPresent()) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getM12Values().get( cellValue ) ).isPresent())) {
                            row12.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getM12Values().get( cellValue ) );
                            row12.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row12.createCell( cellNo ).setCellValue( " " );
                            row12.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row12.createCell( cellNo ).setCellValue( " " );
                        row12.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( mpsDeltaAnalysisMainDTO.getFinalValues() ).isPresent()) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getFinalValues().get( cellValue ) ).isPresent())) {
                            row13.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getFinalValues().get( cellValue ) );
                            row13.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row13.createCell( cellNo ).setCellValue( " " );
                            row13.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row13.createCell( cellNo ).setCellValue( " " );
                        row13.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( mpsDeltaAnalysisMainDTO.getDeviationValues() ).isPresent()) {
                        if ((Optional.ofNullable( mpsDeltaAnalysisMainDTO.getDeviationValues().get( cellValue ) ).isPresent())) {
                            row14.createCell( cellNo ).setCellValue( mpsDeltaAnalysisMainDTO.getDeviationValues().get( cellValue ) );
                            row14.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                        } else {
                            row14.createCell( cellNo ).setCellValue( " " );
                            row14.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    } else {
                        row14.createCell( cellNo ).setCellValue( " " );
                        row14.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
                for (int cellValue = 0; cellValue < 14; cellValue++) {
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row1.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 2 ) );
                        row1.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 0 ).setCellValue( " " );
                        row1.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row2.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 3 ) );
                        row2.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 0 ).setCellValue( " " );
                        row2.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row3.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 4 ) );
                        row3.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row3.createCell( 0 ).setCellValue( " " );
                        row3.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row4.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 5 ) );
                        row4.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row4.createCell( 0 ).setCellValue( " " );
                        row4.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row5.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 6 ) );
                        row5.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row5.createCell( 0 ).setCellValue( " " );
                        row5.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row6.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 7 ) );
                        row6.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row6.createCell( 0 ).setCellValue( " " );
                        row6.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row7.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 8 ) );
                        row7.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row7.createCell( 0 ).setCellValue( " " );
                        row7.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row8.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 9 ) );
                        row8.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row8.createCell( 0 ).setCellValue( " " );
                        row8.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row9.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 10 ) );
                        row9.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row9.createCell( 0 ).setCellValue( " " );
                        row9.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row10.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 11 ) );
                        row10.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row10.createCell( 0 ).setCellValue( " " );
                        row10.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row11.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 12 ) );
                        row11.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row11.createCell( 0 ).setCellValue( " " );
                        row11.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row12.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 13 ) );
                        row12.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row12.createCell( 0 ).setCellValue( " " );
                        row12.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row13.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 0 ) );
                        row13.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row13.createCell( 0 ).setCellValue( " " );
                        row13.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if ((Optional.ofNullable( deltaColumnNameDTO.getRowNames() ).isPresent())) {
                        row14.createCell( 0 ).setCellValue( deltaColumnNameDTO.getRowNames().get( 1 ) );
                        row14.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row14.createCell( 0 ).setCellValue( " " );
                        row14.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );

        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}


