package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.consumption.LogicalConsumptionMainDTO;
import com.example.validation.util.ExcelExportUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class PmLogicalConsumptionExporter {
    public static ByteArrayInputStream logicalConsumptionsToExcel(List<LogicalConsumptionMainDTO> logicalConsumptionMainDTOS, Map<Long, ItemDetailDTO> mpsPlanDetailMap, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_LOGICAL_CONSUMPTION + mpsDate.toString() );
            sheet.createFreezePane( 1, 2 );
            printDataRows( logicalConsumptionMainDTOS, mpsPlanDetailMap, sheet, allCellStyles );
            printAdditionalDataRow( sheet, allCellStyles );
            List<String> headerNames = getHeader( monthDetailDTO );
            printHeaderRow( sheet, workbook, headerNames );
            for (int columnIndex = 0; columnIndex < headerNames.size(); columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static void printAdditionalDataRow(Sheet sheet, List<CellStyle> allCellStyles) {
        Row additionalDataRow = sheet.createRow( 0 );
        additionalDataRow.createCell( 8 ).setCellValue( "FG Quantity" );
        sheet.addMergedRegion( new CellRangeAddress( 0, 0, 8, 20 ) );
        additionalDataRow.getCell( 8 ).setCellStyle( allCellStyles.get( 6 ) );
        additionalDataRow.createCell( 21 ).setCellValue( "PM Consumption" );
        sheet.addMergedRegion( new CellRangeAddress( 0, 0, 21, 33 ) );
        additionalDataRow.getCell( 21 ).setCellStyle( allCellStyles.get( 6 ) );
        additionalDataRow.createCell( 33 ).setCellValue( "" );
        additionalDataRow.getCell( 33 ).setCellStyle( allCellStyles.get( 0 ) );
    }

    private static List<String> getHeader(MonthDetailDTO monthDetailDTO) {
        List<String> headerNames = new ArrayList<>();
        headerNames.add( "FG Code" );
        headerNames.add( "FG Description" );
        headerNames.add( "PM Code" );
        headerNames.add( "PM Description" );
        headerNames.add( "Link Qty" );
        headerNames.add( "Loss %" );
        headerNames.add( "From Date" );
        headerNames.add( "To Date" );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth1().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth2().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth3().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth4().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth5().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth6().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth7().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth8().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth9().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth10().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth11().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth12().getMonthNameAlias() ) );
        headerNames.add( "Total" );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth1().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth2().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth3().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth4().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth5().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth6().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth7().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth8().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth9().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth10().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth11().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth12().getMonthNameAlias() ) );
        headerNames.add( "Total" );
        return headerNames;
    }

    private static void printHeaderRow(Sheet sheet, Workbook workbook, List<String> headerNames) {
        Row headerRow = sheet.createRow( 1 );
        for (int col = 0; col < headerNames.size(); col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( headerNames.get( col ) );
            cell.setCellStyle( ExcelExportUtil.getHeaderCellStyle( workbook ) );
        }
    }

    public static void printDataRows(List<LogicalConsumptionMainDTO> logicalConsumptionMainDTOS, Map<Long, ItemDetailDTO> mpsPlanDetailMap, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 2;
        if (!mpsPlanDetailMap.isEmpty()) {
            if (Optional.ofNullable( logicalConsumptionMainDTOS ).isPresent()) {
                for (LogicalConsumptionMainDTO logicalConsumptionMainDTO : logicalConsumptionMainDTOS) {
                    ItemDetailDTO itemDetailDTO = mpsPlanDetailMap.get( logicalConsumptionMainDTO.getParentId() );
                    Row row1 = sheet.createRow( rowIdx++ );
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getParentCode() ).isPresent()) {
                        String materialCodeInStringFormat = logicalConsumptionMainDTO.getParentCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row1.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                            row1.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
                        } catch (NumberFormatException ex) {
                            row1.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                            row1.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                        }
                    } else {
                        row1.createCell( 0 ).setCellValue( " " );
                        row1.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getParentDescription() ).isPresent()) {
                        row1.createCell( 1 ).setCellValue( logicalConsumptionMainDTO.getParentDescription() );
                        row1.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row1.createCell( 1 ).setCellValue( " " );
                        row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getChildCode() ).isPresent()) {
                        String materialCodeInStringFormat = logicalConsumptionMainDTO.getChildCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row1.createCell( 2 ).setCellValue( materialCodeInNumberFormat );
                            row1.getCell( 2 ).setCellStyle( allCellStyles.get( 3 ) );
                        } catch (NumberFormatException ex) {
                            row1.createCell( 2 ).setCellValue( materialCodeInStringFormat );
                            row1.getCell( 2 ).setCellStyle( allCellStyles.get( 2 ) );
                        }
                    } else {
                        row1.createCell( 2 ).setCellValue( " " );
                        row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getChildDescription() ).isPresent()) {
                        row1.createCell( 3 ).setCellValue( logicalConsumptionMainDTO.getChildDescription() );
                        row1.getCell( 3 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row1.createCell( 3 ).setCellValue( " " );
                        row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLinkQuantity() ).isPresent()) {
                        row1.createCell( 4 ).setCellValue( logicalConsumptionMainDTO.getLinkQuantity() );
                        row1.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row1.createCell( 4 ).setCellValue( " " );
                        row1.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getWastePercent() ).isPresent()) {
                        row1.createCell( 5 ).setCellValue( logicalConsumptionMainDTO.getWastePercent() );
                        row1.getCell( 5 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row1.createCell( 5 ).setCellValue( " " );
                        row1.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getApplicableFrom() ).isPresent()) {
                        row1.createCell( 6 ).setCellValue( logicalConsumptionMainDTO.getApplicableFrom() );
                        row1.getCell( 6 ).setCellStyle( allCellStyles.get( 5 ) );
                    } else {
                        row1.createCell( 6 ).setCellValue( " " );
                        row1.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getApplicableTo() ).isPresent()) {
                        row1.createCell( 7 ).setCellValue( logicalConsumptionMainDTO.getApplicableTo() );
                        row1.getCell( 7 ).setCellStyle( allCellStyles.get( 5 ) );
                    } else {
                        row1.createCell( 7 ).setCellValue( " " );
                        row1.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth1().getValue() ).isPresent()) {
                        row1.createCell( 8 ).setCellValue( itemDetailDTO.getMonthValues().getMonth1().getValue() );
                        row1.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 8 ).setCellValue( " " );
                        row1.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth2().getValue() ).isPresent()) {
                        row1.createCell( 9 ).setCellValue( itemDetailDTO.getMonthValues().getMonth2().getValue() );
                        row1.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 9 ).setCellValue( " " );
                        row1.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth3().getValue() ).isPresent()) {
                        row1.createCell( 10 ).setCellValue( itemDetailDTO.getMonthValues().getMonth3().getValue() );
                        row1.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 10 ).setCellValue( " " );
                        row1.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth4().getValue() ).isPresent()) {
                        row1.createCell( 11 ).setCellValue( itemDetailDTO.getMonthValues().getMonth4().getValue() );
                        row1.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 11 ).setCellValue( " " );
                        row1.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth5().getValue() ).isPresent()) {
                        row1.createCell( 12 ).setCellValue( itemDetailDTO.getMonthValues().getMonth5().getValue() );
                        row1.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 12 ).setCellValue( " " );
                        row1.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth6().getValue() ).isPresent()) {
                        row1.createCell( 13 ).setCellValue( itemDetailDTO.getMonthValues().getMonth6().getValue() );
                        row1.getCell( 13 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 13 ).setCellValue( " " );
                        row1.getCell( 13 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth7().getValue() ).isPresent()) {
                        row1.createCell( 14 ).setCellValue( itemDetailDTO.getMonthValues().getMonth7().getValue() );
                        row1.getCell( 14 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 14 ).setCellValue( " " );
                        row1.getCell( 14 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth8().getValue() ).isPresent()) {
                        row1.createCell( 15 ).setCellValue( itemDetailDTO.getMonthValues().getMonth8().getValue() );
                        row1.getCell( 15 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 15 ).setCellValue( " " );
                        row1.getCell( 15 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth9().getValue() ).isPresent()) {
                        row1.createCell( 16 ).setCellValue( itemDetailDTO.getMonthValues().getMonth9().getValue() );
                        row1.getCell( 16 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 16 ).setCellValue( " " );
                        row1.getCell( 16 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth10().getValue() ).isPresent()) {
                        row1.createCell( 17 ).setCellValue( itemDetailDTO.getMonthValues().getMonth10().getValue() );
                        row1.getCell( 17 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 17 ).setCellValue( " " );
                        row1.getCell( 17 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth11().getValue() ).isPresent()) {
                        row1.createCell( 18 ).setCellValue( itemDetailDTO.getMonthValues().getMonth11().getValue() );
                        row1.getCell( 18 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 18 ).setCellValue( " " );
                        row1.getCell( 18 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getMonthValues().getMonth12().getValue() ).isPresent()) {
                        row1.createCell( 19 ).setCellValue( itemDetailDTO.getMonthValues().getMonth12().getValue() );
                        row1.getCell( 19 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 19 ).setCellValue( " " );
                        row1.getCell( 19 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( itemDetailDTO.getTotal() ).isPresent()) {
                        row1.createCell( 20 ).setCellValue( itemDetailDTO.getTotal() );
                        row1.getCell( 20 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 20 ).setCellValue( " " );
                        row1.getCell( 20 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth1().getValue() ).isPresent()) {
                        row1.createCell( 21 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth1().getValue() );
                        row1.getCell( 21 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 21 ).setCellValue( " " );
                        row1.getCell( 21 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth2().getValue() ).isPresent()) {
                        row1.createCell( 22 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth2().getValue() );
                        row1.getCell( 22 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 22 ).setCellValue( " " );
                        row1.getCell( 22 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth3().getValue() ).isPresent()) {
                        row1.createCell( 23 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth3().getValue() );
                        row1.getCell( 23 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 23 ).setCellValue( " " );
                        row1.getCell( 23 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth4().getValue() ).isPresent()) {
                        row1.createCell( 24 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth4().getValue() );
                        row1.getCell( 24 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 24 ).setCellValue( " " );
                        row1.getCell( 24 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth5().getValue() ).isPresent()) {
                        row1.createCell( 25 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth5().getValue() );
                        row1.getCell( 25 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 25 ).setCellValue( " " );
                        row1.getCell( 25 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth6().getValue() ).isPresent()) {
                        row1.createCell( 26 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth6().getValue() );
                        row1.getCell( 26 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 26 ).setCellValue( " " );
                        row1.getCell( 26 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth7().getValue() ).isPresent()) {
                        row1.createCell( 27 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth7().getValue() );
                        row1.getCell( 27 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 27 ).setCellValue( " " );
                        row1.getCell( 27 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth8().getValue() ).isPresent()) {
                        row1.createCell( 28 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth8().getValue() );
                        row1.getCell( 28 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 28 ).setCellValue( " " );
                        row1.getCell( 28 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth9().getValue() ).isPresent()) {
                        row1.createCell( 29 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth9().getValue() );
                        row1.getCell( 29 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 29 ).setCellValue( " " );
                        row1.getCell( 29 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth10().getValue() ).isPresent()) {
                        row1.createCell( 30 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth10().getValue() );
                        row1.getCell( 30 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 30 ).setCellValue( " " );
                        row1.getCell( 30 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth11().getValue() ).isPresent()) {
                        row1.createCell( 31 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth11().getValue() );
                        row1.getCell( 31 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 31 ).setCellValue( " " );
                        row1.getCell( 31 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth12().getValue() ).isPresent()) {
                        row1.createCell( 32 ).setCellValue( logicalConsumptionMainDTO.getLogicalConsumptionMonthValues().getMonth12().getValue() );
                        row1.getCell( 32 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 32 ).setCellValue( " " );
                        row1.getCell( 32 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( logicalConsumptionMainDTO.getTotalValue() ).isPresent()) {
                        row1.createCell( 33 ).setCellValue( logicalConsumptionMainDTO.getTotalValue() );
                        row1.getCell( 33 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( 33 ).setCellValue( " " );
                        row1.getCell( 33 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
