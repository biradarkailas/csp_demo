package com.example.validation.util.excel_export;

import com.example.validation.dto.ExecutionResponseDTO;
import com.example.validation.util.ExcelExportUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class ExecutionResponseExporter {
  public static ByteArrayInputStream createExecutionResponseExcel(
      String mpsDate, List<ExecutionResponseDTO> executionResponseDTOs) {
    try (XSSFWorkbook workbook = new XSSFWorkbook();
        ByteArrayOutputStream out = new ByteArrayOutputStream(); ) {
      XSSFSheet sheet = workbook.createSheet("ErrorLog_" + mpsDate);
      List<CellStyle> allCellStyles = getAllCellStyles(workbook);
      List<String> headerNames = getHeader();
      printHeaderRow(sheet, workbook, headerNames);
      printDataRows(executionResponseDTOs, sheet, allCellStyles);
      for (int columnIndex = 0; columnIndex < headerNames.size(); columnIndex++) {
        sheet.autoSizeColumn(columnIndex);
      }
      workbook.write(out);
      return new ByteArrayInputStream(out.toByteArray());
    } catch (IOException e) {
      throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
    }
  }

  private static List<String> getHeader() {
    List<String> headerNames = new ArrayList<>();
    headerNames.add("Date Time");
    // headerNames.add("URL");
    headerNames.add("Module Name");
    headerNames.add("Error");
    return headerNames;
  }

  private static void printHeaderRow(Sheet sheet, Workbook workbook, List<String> headerNames) {
    Row headerRow = sheet.createRow(0);
    for (int col = 0; col < headerNames.size(); col++) {
      Cell cell = headerRow.createCell(col);
      cell.setCellValue(headerNames.get(col));
      cell.setCellStyle(ExcelExportUtil.getHeaderCellStyle(workbook));
    }
  }

  private static void printDataRows(
      List<ExecutionResponseDTO> executionResponseDTOs,
      Sheet sheet,
      List<CellStyle> allCellStyles) {
    int rowIdx = 1;
    if ((Optional.ofNullable(executionResponseDTOs).isPresent())) {
      for (ExecutionResponseDTO executionResponseDTO : executionResponseDTOs) {
        Row row = sheet.createRow(rowIdx++);
        if (Optional.ofNullable(executionResponseDTO.getCreatedDateTime()).isPresent()) {
          row.createCell(0).setCellValue(executionResponseDTO.getCreatedDateTime());
          row.getCell(0).setCellStyle(allCellStyles.get(3));
        } else {
          row.createCell(0).setCellValue(" ");
          row.getCell(0).setCellStyle(allCellStyles.get(0));
        }
        /*if (Optional.ofNullable(executionResponseDTO.getUrl()).isPresent()) {
            row.createCell(1).setCellValue(executionResponseDTO.getUrl());
            row.getCell(1).setCellStyle(allCellStyles.get(2));
        } else {
            row.createCell(1).setCellValue(" ");
            row.getCell(1).setCellStyle(allCellStyles.get(0));
        }*/
        if (Optional.ofNullable(executionResponseDTO.getModuleName()).isPresent()) {
          row.createCell(1).setCellValue(executionResponseDTO.getModuleName());
          row.getCell(1).setCellStyle(allCellStyles.get(2));
        } else {
          row.createCell(1).setCellValue(" ");
          row.getCell(1).setCellStyle(allCellStyles.get(0));
        }
        if (Optional.ofNullable(executionResponseDTO.getErrorMessage()).isPresent()) {
          row.createCell(2).setCellValue(executionResponseDTO.getErrorMessage());
          row.getCell(2).setCellStyle(allCellStyles.get(2));
          row.getCell(2).getCellStyle().setWrapText(true);
        } else {
          row.createCell(2).setCellValue(" ");
          row.getCell(2).setCellStyle(allCellStyles.get(0));
        }
      }
    }
  }

  private static List<CellStyle> getAllCellStyles(Workbook workbook) {
    List<CellStyle> allCellStyleList = new ArrayList<>();
    List<Font> allFonts = getAllFonts(workbook);
    CellStyle blankCellStyle = workbook.createCellStyle();
    setBorderStyle(blankCellStyle);
    allCellStyleList.add(blankCellStyle);
    CellStyle headerCellStyle = workbook.createCellStyle();
    Font headerFont = allFonts.get(1);
    headerFont.setBold(true);
    headerFont.setColor(IndexedColors.WHITE.index);
    headerCellStyle.setFont(headerFont);
    headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
    headerCellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.index);
    setBorderStyle(headerCellStyle);
    headerCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
    headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
    allCellStyleList.add(headerCellStyle);
    CellStyle stringDataCellStyle = workbook.createCellStyle();
    stringDataCellStyle.setFont(allFonts.get(0));
    setBorderStyle(stringDataCellStyle);
    stringDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
    allCellStyleList.add(stringDataCellStyle);
    CellStyle dateDataCellStyle = workbook.createCellStyle();
    dateDataCellStyle.setFont(allFonts.get(0));
    setBorderStyle(dateDataCellStyle);
    dateDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy h:mm"));
    allCellStyleList.add(dateDataCellStyle);
    CellStyle generalDataCellStyle = workbook.createCellStyle();
    generalDataCellStyle.setFont(allFonts.get(0));
    setBorderStyle(generalDataCellStyle);
    generalDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
    allCellStyleList.add(generalDataCellStyle);
    return allCellStyleList;
  }

  private static List<Font> getAllFonts(Workbook workbook) {
    List<Font> allFontList = new ArrayList<>();
    Font size10Font = workbook.createFont();
    size10Font.setFontName("Calibri");
    size10Font.setFontHeightInPoints((short) 10);
    allFontList.add(size10Font);
    Font size11Font = workbook.createFont();
    size11Font.setFontName("Calibri");
    size11Font.setFontHeightInPoints((short) 11);
    allFontList.add(size11Font);
    return allFontList;
  }

  private static void setBorderStyle(CellStyle cellStyle) {
    cellStyle.setBorderTop(BorderStyle.THIN);
    cellStyle.setBorderRight(BorderStyle.THIN);
    cellStyle.setBorderBottom(BorderStyle.THIN);
    cellStyle.setBorderLeft(BorderStyle.THIN);
  }
}
