package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.SlobSummaryDetailsDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmCoverDaysSummaryExporter {
    public static ByteArrayInputStream pmCoverDaysSummaryToExcel(SlobSummaryDetailsDTO pmSlobSummaryDetails, CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_SLOB_COVER_DAYS_SUMMARY + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow( monthDetailDTO, sheet, allCellStyles.get( 1 ) );
            printDataRows( pmSlobSummaryDetails, coverDaysSummaryDetailsDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader( monthDetailDTO ).length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static void printHeaderRow(MonthDetailDTO monthDetailDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 3 );
        String[] header = getHeader( monthDetailDTO );
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static String[] getHeader(MonthDetailDTO monthDetailDTO) {
        String m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value;
        if (Optional.ofNullable( monthDetailDTO.getMonth1().getMonthNameAlias() ).isPresent()) {
            m1Value = monthDetailDTO.getMonth1().getMonthNameAlias();
        } else {
            m1Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth2().getMonthNameAlias() ).isPresent()) {
            m2Value = monthDetailDTO.getMonth2().getMonthNameAlias();
        } else {
            m2Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth3().getMonthNameAlias() ).isPresent()) {
            m3Value = monthDetailDTO.getMonth3().getMonthNameAlias();
        } else {
            m3Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth4().getMonthNameAlias() ).isPresent()) {
            m4Value = monthDetailDTO.getMonth4().getMonthNameAlias();
        } else {
            m4Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth5().getMonthNameAlias() ).isPresent()) {
            m5Value = monthDetailDTO.getMonth5().getMonthNameAlias();
        } else {
            m5Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth6().getMonthNameAlias() ).isPresent()) {
            m6Value = monthDetailDTO.getMonth6().getMonthNameAlias();
        } else {
            m6Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth7().getMonthNameAlias() ).isPresent()) {
            m7Value = monthDetailDTO.getMonth7().getMonthNameAlias();
        } else {
            m7Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth8().getMonthNameAlias() ).isPresent()) {
            m8Value = monthDetailDTO.getMonth8().getMonthNameAlias();
        } else {
            m8Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth9().getMonthNameAlias() ).isPresent()) {
            m9Value = monthDetailDTO.getMonth9().getMonthNameAlias();
        } else {
            m9Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth10().getMonthNameAlias() ).isPresent()) {
            m10Value = monthDetailDTO.getMonth10().getMonthNameAlias();
        } else {
            m10Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth11().getMonthNameAlias() ).isPresent()) {
            m11Value = monthDetailDTO.getMonth11().getMonthNameAlias();
        } else {
            m11Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth12().getMonthNameAlias() ).isPresent()) {
            m12Value = monthDetailDTO.getMonth12().getMonthNameAlias();
        } else {
            m12Value = "";
        }
        return new String[]{"Month", m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value};
    }

    private static void printDataRows(SlobSummaryDetailsDTO pmSlobSummaryDetails, CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( coverDaysSummaryDetailsDTO ).isPresent()) {
            if ((Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO() ).isPresent()) && (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO() ).isPresent())
                    && (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO() ).isPresent())) {
                int rowIdx1 = 4;
                int rowIdx2 = 0;
                int rowIdx3 = 1;
                Row rowHeader = sheet.createRow( rowIdx2 );
                Row rowValue = sheet.createRow( rowIdx3 );
                Row row1 = sheet.createRow( rowIdx1++ );
                Row row2 = sheet.createRow( rowIdx1++ );
                Row row3 = sheet.createRow( rowIdx1 );
                rowHeader.createCell( 0 ).setCellValue( "Total Cover Days Count" );
                rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getTotalCoverDays() ).isPresent()) {
                    rowValue.createCell( 0 ).setCellValue( coverDaysSummaryDetailsDTO.getTotalCoverDays() );
                } else {
                    rowValue.createCell( 0 ).setCellValue( "" );
                }
                rowValue.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
                rowHeader.createCell( 1 ).setCellValue( "Total Stock Value With Sit" );
                sheet.addMergedRegion( new CellRangeAddress( 0, 0, 1, 2 ) );
                rowHeader.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                rowHeader.getCell( 1 ).setCellStyle( allCellStyles.get( 1 ) );
                if ((Optional.ofNullable( coverDaysSummaryDetailsDTO.getTotalStockSitValue() ).isPresent())) {
                    rowValue.createCell( 1 ).setCellValue( coverDaysSummaryDetailsDTO.getTotalStockSitValue().longValue() );
                } else {
                    rowValue.createCell( 1 ).setCellValue( "" );
                }
                rowValue.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                rowValue.createCell( 2 ).setCellValue( "" );
                rowValue.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                sheet.addMergedRegion( new CellRangeAddress( 1, 1, 1, 2 ) );
                row1.createCell( 0 ).setCellValue( "Days In Month" );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row2.createCell( 0 ).setCellValue( "Consumption" );
                row2.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row3.createCell( 0 ).setCellValue( "Cover Days" );
                row3.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth1CoverDays() ).isPresent()) {
                    row1.createCell( 1 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth1CoverDays() );
                    row1.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 1 ).setCellValue( " " );
                    row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth2CoverDays() ).isPresent()) {
                    row1.createCell( 2 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth2CoverDays() );
                    row1.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 2 ).setCellValue( " " );
                    row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth3CoverDays() ).isPresent()) {
                    row1.createCell( 3 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth3CoverDays() );
                    row1.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 3 ).setCellValue( " " );
                    row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth4CoverDays() ).isPresent()) {
                    row1.createCell( 4 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth4CoverDays() );
                    row1.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 4 ).setCellValue( " " );
                    row1.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth5CoverDays() ).isPresent()) {
                    row1.createCell( 5 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth5CoverDays() );
                    row1.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 5 ).setCellValue( " " );
                    row1.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth6CoverDays() ).isPresent()) {
                    row1.createCell( 6 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth6CoverDays() );
                    row1.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 6 ).setCellValue( " " );
                    row1.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth7CoverDays() ).isPresent()) {
                    row1.createCell( 7 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth7CoverDays() );
                    row1.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 7 ).setCellValue( " " );
                    row1.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth8CoverDays() ).isPresent()) {
                    row1.createCell( 8 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth8CoverDays() );
                    row1.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 8 ).setCellValue( " " );
                    row1.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth9CoverDays() ).isPresent()) {
                    row1.createCell( 9 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth9CoverDays() );
                    row1.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 9 ).setCellValue( " " );
                    row1.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth10CoverDays() ).isPresent()) {
                    row1.createCell( 10 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth10CoverDays() );
                    row1.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 10 ).setCellValue( " " );
                    row1.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth11CoverDays() ).isPresent()) {
                    row1.createCell( 11 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth11CoverDays() );
                    row1.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 11 ).setCellValue( " " );
                    row1.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth12CoverDays() ).isPresent()) {
                    row1.createCell( 12 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysInMonthDTO().getMonth12CoverDays() );
                    row1.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 12 ).setCellValue( " " );
                    row1.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 1 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 1 ).setCellValue( " " );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 2 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 2 ).setCellValue( " " );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 3 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() );
                    row2.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 3 ).setCellValue( " " );
                    row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 4 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() );
                    row2.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 4 ).setCellValue( " " );
                    row2.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 5 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() );
                    row2.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 5 ).setCellValue( " " );
                    row2.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 6 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() );
                    row2.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 6 ).setCellValue( " " );
                    row2.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 7 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() );
                    row2.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 7 ).setCellValue( " " );
                    row2.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 8 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() );
                    row2.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 8 ).setCellValue( " " );
                    row2.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 9 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() );
                    row2.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 9 ).setCellValue( " " );
                    row2.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 10 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() );
                    row2.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 10 ).setCellValue( " " );
                    row2.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 11 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() );
                    row2.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 11 ).setCellValue( " " );
                    row2.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() ).isPresent()) {
                    row2.createCell( 12 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() );
                    row2.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 12 ).setCellValue( " " );
                    row2.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth1CoverDays() ).isPresent()) {
                    row3.createCell( 1 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth1CoverDays() );
                    row3.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 1 ).setCellValue( " " );
                    row3.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth2CoverDays() ).isPresent()) {
                    row3.createCell( 2 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth2CoverDays() );
                    row3.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 2 ).setCellValue( " " );
                    row3.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth3CoverDays() ).isPresent()) {
                    row3.createCell( 3 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth3CoverDays() );
                    row3.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 3 ).setCellValue( " " );
                    row3.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth4CoverDays() ).isPresent()) {
                    row3.createCell( 4 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth4CoverDays() );
                    row3.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 4 ).setCellValue( " " );
                    row3.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth5CoverDays() ).isPresent()) {
                    row3.createCell( 5 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth5CoverDays() );
                    row3.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 5 ).setCellValue( " " );
                    row3.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth6CoverDays() ).isPresent()) {
                    row3.createCell( 6 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth6CoverDays() );
                    row3.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 6 ).setCellValue( " " );
                    row3.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth7CoverDays() ).isPresent()) {
                    row3.createCell( 7 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth7CoverDays() );
                    row3.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 7 ).setCellValue( " " );
                    row3.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth8CoverDays() ).isPresent()) {
                    row3.createCell( 8 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth8CoverDays() );
                    row3.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 8 ).setCellValue( " " );
                    row3.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth9CoverDays() ).isPresent()) {
                    row3.createCell( 9 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth9CoverDays() );
                    row3.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 9 ).setCellValue( " " );
                    row3.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth10CoverDays() ).isPresent()) {
                    row3.createCell( 10 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth10CoverDays() );
                    row3.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 10 ).setCellValue( " " );
                    row3.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth11CoverDays() ).isPresent()) {
                    row3.createCell( 11 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth11CoverDays() );
                    row3.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 11 ).setCellValue( " " );
                    row3.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth12CoverDays() ).isPresent()) {
                    row3.createCell( 12 ).setCellValue( coverDaysSummaryDetailsDTO.getCoverDaysDTO().getMonth12CoverDays() );
                    row3.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 12 ).setCellValue( " " );
                    row3.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                }
            }
        }
    }

    private static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );

        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}


