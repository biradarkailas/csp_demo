package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ConsumptionItemDetailsDTO;
import com.example.validation.dto.ConsumptionPivotDTO;
import com.example.validation.dto.ConsumptionPivotMainDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.util.ExcelExportUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RmPivotConsumptionExporter {
    public static ByteArrayInputStream rmPivotConsumptionToExcel(ConsumptionPivotMainDTO consumptionPivotMainDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.Rm_Pivot_Consumption + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            sheet.createFreezePane( 1, 1 );
            printDataRows( consumptionPivotMainDTO, sheet, allCellStyles );
            List<String> headerNames = getHeader( monthDetailDTO );
            printHeaderRow( sheet, workbook, headerNames );
            for (int columnIndex = 0; columnIndex < headerNames.size(); columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }


    private static List<String> getHeader(MonthDetailDTO monthDetailDTO) {
        List<String> headerNames = new ArrayList<>();
        headerNames.add( "RM Code" );
        headerNames.add( "Bulk" );
        headerNames.add( "Bulk Description" );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth1().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth2().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth3().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth4().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth5().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth6().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth7().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth8().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth9().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth10().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth11().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth12().getMonthNameAlias() ) );
        headerNames.add( "Total Value" );
        headerNames.add( "FG Details" );
        return headerNames;
    }

    private static void printHeaderRow(Sheet sheet, Workbook workbook, List<String> headerNames) {
        Row headerRow = sheet.createRow( 0 );
        for (int col = 0; col < headerNames.size(); col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( headerNames.get( col ) );
            cell.setCellStyle( ExcelExportUtil.getHeaderCellStyle( workbook ) );
        }
    }


    private static void printDataRows(ConsumptionPivotMainDTO consumptionPivotMainDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 1;
        Row row2 = sheet.createRow( rowIdx );
        if (Optional.ofNullable( consumptionPivotMainDTO.getConsumptionPivotDTOs() ).isPresent()) {
            for (ConsumptionPivotDTO consumptionPivotDTO : consumptionPivotMainDTO.getConsumptionPivotDTOs()) {
                if (Optional.ofNullable( consumptionPivotDTO.getConsumptionItemDetailsDTOs() ).isPresent()) {
                    for (ConsumptionItemDetailsDTO consumptionItemDetailsDTO : consumptionPivotDTO.getConsumptionItemDetailsDTOs()) {
                        row2 = sheet.createRow( rowIdx++ );
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getChildItemCode() ).isPresent()) {
                            String materialCodeInStringFormat = consumptionItemDetailsDTO.getChildItemCode();
                            try {
                                long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                                row2.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                                row2.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
                            } catch (NumberFormatException ex) {
                                row2.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                                row2.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                            }
                        } else {
                            row2.createCell( 0 ).setCellValue( " " );
                            row2.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getParentItemCode() ).isPresent()) {
                            String materialCodeInStringFormat = consumptionItemDetailsDTO.getParentItemCode();
                            try {
                                long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                                row2.createCell( 1 ).setCellValue( materialCodeInNumberFormat );
                                row2.getCell( 1 ).setCellStyle( allCellStyles.get( 3 ) );
                            } catch (NumberFormatException ex) {
                                row2.createCell( 1 ).setCellValue( materialCodeInStringFormat );
                            }
                            row2.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                        } else {
                            row2.createCell( 1 ).setCellValue( " " );
                            row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getParentItemDescription() ).isPresent()) {
                            row2.createCell( 2 ).setCellValue( consumptionItemDetailsDTO.getParentItemDescription() );
                            row2.getCell( 2 ).setCellStyle( allCellStyles.get( 2 ) );
                        } else {
                            row2.createCell( 2 ).setCellValue( " " );
                            row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM1Value() ).isPresent()) {
                            row2.createCell( 3 ).setCellValue( consumptionItemDetailsDTO.getM1Value() );
                            row2.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 3 ).setCellValue( " " );
                            row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM2Value() ).isPresent()) {
                            row2.createCell( 4 ).setCellValue( consumptionItemDetailsDTO.getM2Value() );
                            row2.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 4 ).setCellValue( " " );
                            row2.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM3Value() ).isPresent()) {
                            row2.createCell( 5 ).setCellValue( consumptionItemDetailsDTO.getM3Value() );
                            row2.getCell( 5 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 5 ).setCellValue( " " );
                            row2.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM4Value() ).isPresent()) {
                            row2.createCell( 6 ).setCellValue( consumptionItemDetailsDTO.getM4Value() );
                            row2.getCell( 6 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 6 ).setCellValue( " " );
                            row2.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM5Value() ).isPresent()) {
                            row2.createCell( 7 ).setCellValue( consumptionItemDetailsDTO.getM5Value() );
                            row2.getCell( 7 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 7 ).setCellValue( " " );
                            row2.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM6Value() ).isPresent()) {
                            row2.createCell( 8 ).setCellValue( consumptionItemDetailsDTO.getM6Value() );
                            row2.getCell( 8 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 8 ).setCellValue( " " );
                            row2.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM7Value() ).isPresent()) {
                            row2.createCell( 9 ).setCellValue( consumptionItemDetailsDTO.getM7Value() );
                            row2.getCell( 9 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 9 ).setCellValue( " " );
                            row2.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM8Value() ).isPresent()) {
                            row2.createCell( 10 ).setCellValue( consumptionItemDetailsDTO.getM8Value() );
                            row2.getCell( 10 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 10 ).setCellValue( " " );
                            row2.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM9Value() ).isPresent()) {
                            row2.createCell( 11 ).setCellValue( consumptionItemDetailsDTO.getM9Value() );
                            row2.getCell( 11 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 11 ).setCellValue( " " );
                            row2.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM10Value() ).isPresent()) {
                            row2.createCell( 12 ).setCellValue( consumptionItemDetailsDTO.getM10Value() );
                            row2.getCell( 12 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 12 ).setCellValue( " " );
                            row2.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM11Value() ).isPresent()) {
                            row2.createCell( 13 ).setCellValue( consumptionItemDetailsDTO.getM11Value() );
                            row2.getCell( 13 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 13 ).setCellValue( " " );
                            row2.getCell( 13 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getM12Value() ).isPresent()) {
                            row2.createCell( 14 ).setCellValue( consumptionItemDetailsDTO.getM12Value() );
                            row2.getCell( 14 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 14 ).setCellValue( " " );
                            row2.getCell( 14 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getTotalValue() ).isPresent()) {
                            row2.createCell( 15 ).setCellValue( consumptionItemDetailsDTO.getTotalValue() );
                            row2.getCell( 15 ).setCellStyle( allCellStyles.get( 7 ) );
                        } else {
                            row2.createCell( 15 ).setCellValue( " " );
                            row2.getCell( 15 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                        if (Optional.ofNullable( consumptionItemDetailsDTO.getFgItemDetails() ).isPresent()) {
                            row2.createCell( 16 ).setCellValue( consumptionItemDetailsDTO.getFgItemDetails() );
                            row2.getCell( 16 ).setCellStyle( allCellStyles.get( 2 ) );
                        } else {
                            row2.createCell( 16 ).setCellValue( " " );
                            row2.getCell( 16 ).setCellStyle( allCellStyles.get( 0 ) );
                        }
                    }
                    row2 = sheet.createRow( rowIdx++ );
                    if (Optional.ofNullable( consumptionPivotDTO.getChildItemCode() ).isPresent()) {
                        String materialCodeInStringFormat = consumptionPivotDTO.getChildItemCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row2.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                            row2.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
                        } catch (NumberFormatException ex) {
                            row2.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                            row2.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                        }
                    } else {
                        row2.createCell( 0 ).setCellValue( " " );
                        row2.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    row2.createCell( 1 ).setCellValue( " " );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    row2.createCell( 2 ).setCellValue( "Total" );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 8 ) );
                    if (Optional.ofNullable( consumptionPivotDTO.getM1TotalValue() ).isPresent()) {
                        row2.createCell( 3 ).setCellValue( consumptionPivotDTO.getM1TotalValue() );
                        row2.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 3 ).setCellValue( " " );
                        row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM2TotalValue() ).isPresent()) {
                        row2.createCell( 4 ).setCellValue( consumptionPivotDTO.getM2TotalValue() );
                        row2.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 4 ).setCellValue( " " );
                        row2.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM3TotalValue() ).isPresent()) {
                        row2.createCell( 5 ).setCellValue( consumptionPivotDTO.getM3TotalValue() );
                        row2.getCell( 5 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 5 ).setCellValue( " " );
                        row2.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM4TotalValue() ).isPresent()) {
                        row2.createCell( 6 ).setCellValue( consumptionPivotDTO.getM4TotalValue() );
                        row2.getCell( 6 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 6 ).setCellValue( " " );
                        row2.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM5TotalValue() ).isPresent()) {
                        row2.createCell( 7 ).setCellValue( consumptionPivotDTO.getM5TotalValue() );
                        row2.getCell( 7 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 7 ).setCellValue( " " );
                        row2.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM6TotalValue() ).isPresent()) {
                        row2.createCell( 8 ).setCellValue( consumptionPivotDTO.getM6TotalValue() );
                        row2.getCell( 8 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 8 ).setCellValue( " " );
                        row2.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM7TotalValue() ).isPresent()) {
                        row2.createCell( 9 ).setCellValue( consumptionPivotDTO.getM7TotalValue() );
                        row2.getCell( 9 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 9 ).setCellValue( " " );
                        row2.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM8TotalValue() ).isPresent()) {
                        row2.createCell( 10 ).setCellValue( consumptionPivotDTO.getM8TotalValue() );
                        row2.getCell( 10 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 10 ).setCellValue( " " );
                        row2.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM9TotalValue() ).isPresent()) {
                        row2.createCell( 11 ).setCellValue( consumptionPivotDTO.getM9TotalValue() );
                        row2.getCell( 11 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 11 ).setCellValue( " " );
                        row2.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM10TotalValue() ).isPresent()) {
                        row2.createCell( 12 ).setCellValue( consumptionPivotDTO.getM10TotalValue() );
                        row2.getCell( 12 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 12 ).setCellValue( " " );
                        row2.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM11TotalValue() ).isPresent()) {
                        row2.createCell( 13 ).setCellValue( consumptionPivotDTO.getM11TotalValue() );
                        row2.getCell( 13 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 13 ).setCellValue( " " );
                        row2.getCell( 13 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getM12TotalValue() ).isPresent()) {
                        row2.createCell( 14 ).setCellValue( consumptionPivotDTO.getM12TotalValue() );
                        row2.getCell( 14 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 14 ).setCellValue( " " );
                        row2.getCell( 14 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( consumptionPivotDTO.getTotalValue() ).isPresent()) {
                        row2.createCell( 15 ).setCellValue( consumptionPivotDTO.getTotalValue() );
                        row2.getCell( 15 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row2.createCell( 15 ).setCellValue( " " );
                        row2.getCell( 15 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}

