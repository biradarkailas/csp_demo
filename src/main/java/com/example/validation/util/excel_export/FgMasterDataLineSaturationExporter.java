package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.RscDtLinesDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FgMasterDataLineSaturationExporter {
    public static ByteArrayInputStream masterDatalineSaturationToExcel(List<RscDtLinesDTO> rscDtLinesDTOS, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.FG_Master_Data_LINE_SATURATION + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            sheet.createFreezePane( 1, 1 );
            printHeaderRow( sheet, allCellStyles.get( 1 ) );
            printDataRows( rscDtLinesDTOS, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static String[] getHeader() {
        return new String[]{"Name", "Capacity Per Shift", "Shifts", "Speed", "G Up Time", "Mod Per Line Per Shift", "Hrs Per Shift"};
    }

    private static void printHeaderRow(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 0 );
        String[] header = getHeader();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(List<RscDtLinesDTO> rscDtLinesDTOS, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 1;
        if (Optional.ofNullable( rscDtLinesDTOS ).isPresent()) {
            for (RscDtLinesDTO rscDtLinesDTO : rscDtLinesDTOS) {
                Row row = sheet.createRow( rowIdx++ );
                if (Optional.ofNullable( rscDtLinesDTO.getName() ).isPresent()) {
                    String format = "General";
                    String materialCodeInStringFormat = rscDtLinesDTO.getName();
                    try {
                        long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                        row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 3 ) );
                    } catch (NumberFormatException ex) {
                        row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 5 ) );
                    }
                } else {
                    row.createCell( 0 ).setCellValue( " " );
                    row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtLinesDTO.getCapacityPerShift() ).isPresent()) {
                    row.createCell( 1 ).setCellValue( rscDtLinesDTO.getCapacityPerShift() );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 6 ) );
                } else {
                    row.createCell( 1 ).setCellValue( " " );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtLinesDTO.getShifts() ).isPresent()) {
                    row.createCell( 2 ).setCellValue( rscDtLinesDTO.getShifts() );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 2 ).setCellValue( " " );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtLinesDTO.getSpeed() ).isPresent()) {
                    row.createCell( 3 ).setCellValue( rscDtLinesDTO.getSpeed() );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 3 ).setCellValue( " " );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }

                if (Optional.ofNullable( rscDtLinesDTO.getGUpTime() ).isPresent()) {
                    row.createCell( 4 ).setCellValue( rscDtLinesDTO.getGUpTime() );
                    row.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                } else {
                    row.createCell( 4 ).setCellValue( " " );
                    row.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtLinesDTO.getModPerLinePerShift() ).isPresent()) {
                    row.createCell( 5 ).setCellValue( rscDtLinesDTO.getModPerLinePerShift() );
                    row.getCell( 5 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 5 ).setCellValue( " " );
                    row.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtLinesDTO.getHrsPerShift() ).isPresent()) {
                    row.createCell( 6 ).setCellValue( rscDtLinesDTO.getHrsPerShift() );
                    row.getCell( 6 ).setCellStyle( allCellStyles.get( 6 ) );
                } else {
                    row.createCell( 6 ).setCellValue( " " );
                    row.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );

        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );

        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1


        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//2

        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( numberDataCellStyle1 );//3

        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//4

        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//5

        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//6
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
