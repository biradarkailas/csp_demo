package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.RscDtSkidsDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FgMasterDataSkidSaturationExporter {

    public static ByteArrayInputStream MasterDataSkidSaturationToExcel(List<RscDtSkidsDTO> rscDtSkidsDTOS, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.FG_Master_Data_SkidSaturation + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            sheet.createFreezePane( 1, 1 );
            printHeaderRow( sheet, allCellStyles.get( 1 ) );
            printDataRows( rscDtSkidsDTOS, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }

    }

    private static String[] getHeader() {
        return new String[]{"Name", "Shifts", "Batches Per Shift", "Hrs Per Shift"};
    }

    private static void printHeaderRow(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 0 );
        String[] header = getHeader();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(List<RscDtSkidsDTO> rscDtSkidsDTOS, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 1;
        if (Optional.ofNullable( rscDtSkidsDTOS ).isPresent()) {
            for (RscDtSkidsDTO rscDtSkidsDTO : rscDtSkidsDTOS) {
                Row row = sheet.createRow( rowIdx++ );
                if (Optional.ofNullable( rscDtSkidsDTO.getName() ).isPresent()) {
                    String format = "General";
                    String materialCodeInStringFormat = rscDtSkidsDTO.getName();
                    try {
                        long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                        row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 3 ) );
                    } catch (NumberFormatException ex) {
                        row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 5 ) );
                    }
                } else {
                    row.createCell( 0 ).setCellValue( " " );
                    row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtSkidsDTO.getShifts() ).isPresent()) {
                    row.createCell( 1 ).setCellValue( rscDtSkidsDTO.getShifts() );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 1 ).setCellValue( " " );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtSkidsDTO.getBatchesPerShift() ).isPresent()) {
                    row.createCell( 2 ).setCellValue( rscDtSkidsDTO.getBatchesPerShift() );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 3 ) );
                } else {
                    row.createCell( 2 ).setCellValue( " " );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscDtSkidsDTO.getHrsPerShift() ).isPresent()) {
                    row.createCell( 3 ).setCellValue( rscDtSkidsDTO.getHrsPerShift() );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 3 ) );
                } else {
                    row.createCell( 3 ).setCellValue( " " );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );

        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0

        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1


        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//2

        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( numberDataCellStyle1 );//3

        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//4

        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//5

        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}

