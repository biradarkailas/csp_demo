package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.FgLineSaturationDTO;
import com.example.validation.dto.FgLineSaturationDetailsDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.util.ExcelExportUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FgLineSaturationExporter {

    public static ByteArrayInputStream lineSaturationToExcel(FgLineSaturationDetailsDTO fgLineSaturationDetailsDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.FG_LINE_SATURATION + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            sheet.createFreezePane( 1, 1 );
            ;
            printDataRows( fgLineSaturationDetailsDTO, sheet, allCellStyles );
            List<String> headerNames = getHeader( monthDetailDTO );
            printHeaderRow( sheet, workbook, headerNames );
            for (int columnIndex = 0; columnIndex < headerNames.size(); columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }

    }


    private static List<String> getHeader(MonthDetailDTO monthDetailDTO) {
        List<String> headerNames = new ArrayList<>();
        headerNames.add( "Lines" );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth1().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth2().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth3().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth4().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth5().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth6().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth7().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth8().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth9().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth10().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth11().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth12().getMonthNameAlias() ) );

        return headerNames;
    }

    private static void printHeaderRow(Sheet sheet, Workbook workbook, List<String> headerNames) {
        Row headerRow = sheet.createRow( 0 );
        for (int col = 0; col < headerNames.size(); col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( headerNames.get( col ) );
            cell.setCellStyle( ExcelExportUtil.getHeaderCellStyle( workbook ) );
        }
    }

    private static void printDataRows(FgLineSaturationDetailsDTO fgLineSaturationDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 1;
        if (Optional.ofNullable( fgLineSaturationDetailsDTO.getFgLineSaturationDTO() ).isPresent()) {
            for (FgLineSaturationDTO fgLineSaturationDTO : fgLineSaturationDetailsDTO.getFgLineSaturationDTO()) {
                Row row = sheet.createRow( rowIdx++ );
                if (Optional.ofNullable( fgLineSaturationDTO.getLineName() ).isPresent()) {
                    String format = "General";
                    String materialCodeInStringFormat = fgLineSaturationDTO.getLineName();
                    try {
                        long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                        row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 3 ) );
                    } catch (NumberFormatException ex) {
                        row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                    }
                } else {
                    row.createCell( 0 ).setCellValue( " " );
                    row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM1LineSaturation() ).isPresent()) {
                    row.createCell( 1 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM1LineSaturation() / 100 );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 1 ).setCellValue( " " );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM2LineSaturation() ).isPresent()) {
                    row.createCell( 2 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM2LineSaturation() / 100 );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 2 ).setCellValue( " " );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM3LineSaturation() ).isPresent()) {
                    row.createCell( 3 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM3LineSaturation() / 100 );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 3 ).setCellValue( " " );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM4LineSaturation() ).isPresent()) {
                    row.createCell( 4 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM4LineSaturation() / 100 );
                    row.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 4 ).setCellValue( " " );
                    row.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM5LineSaturation() ).isPresent()) {
                    row.createCell( 5 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM5LineSaturation() / 100 );
                    row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 5 ).setCellValue( " " );
                    row.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM6LineSaturation() ).isPresent()) {
                    row.createCell( 6 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM6LineSaturation() / 100 );
                    row.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 6 ).setCellValue( " " );
                    row.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM7LineSaturation() ).isPresent()) {
                    row.createCell( 7 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM7LineSaturation() / 100 );
                    row.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 7 ).setCellValue( " " );
                    row.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM8LineSaturation() ).isPresent()) {
                    row.createCell( 8 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM8LineSaturation() / 100 );
                    row.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 8 ).setCellValue( " " );
                    row.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM9LineSaturation() ).isPresent()) {
                    row.createCell( 9 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM9LineSaturation() / 100 );
                    row.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 9 ).setCellValue( " " );
                    row.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM10LineSaturation() ).isPresent()) {
                    row.createCell( 10 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM10LineSaturation() / 100 );
                    row.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 10 ).setCellValue( " " );
                    row.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM11LineSaturation() ).isPresent()) {
                    row.createCell( 11 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM11LineSaturation() / 100 );
                    row.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 11 ).setCellValue( " " );
                    row.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM12LineSaturation() ).isPresent()) {
                    row.createCell( 12 ).setCellValue( fgLineSaturationDTO.getRscIotFgLineSaturationDTOS().getM12LineSaturation() / 100 );
                    row.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 12 ).setCellValue( " " );
                    row.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                for (int col = 1; col < 13; col++) {
                    Cell cell1 = row.getCell( col );
                    if (cell1.getNumericCellValue() > 0.69)
                        cell1.setCellStyle( allCellStyles.get( 7 ) );
                }
            }
        }
    }

    private static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );

        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );

        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );

        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );

        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );

        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );

        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );

        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );

        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        Font headerFont1 = allFonts.get( 1 );
        headerFont1.setColor( IndexedColors.WHITE.index );
        percentageDataCellStyle.setFont( headerFont );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        percentageDataCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        percentageDataCellStyle.setFillForegroundColor( IndexedColors.RED1.index );
        allCellStyleList.add( percentageDataCellStyle );

        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();

        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );

        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
