package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.PMSlobDetailsDTO;
import com.example.validation.dto.PmSlobDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmSlobExporter {
    public static ByteArrayInputStream pmSlobToExcelList(PMSlobDetailsDTO pmSlobDetailsDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_SLOB + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow1( sheet, allCellStyles.get( 1 ) );
            printHeaderRow2( sheet, allCellStyles.get( 1 ) );
            printDataRows( pmSlobDetailsDTO, sheet, allCellStyles );
            sheet.createFreezePane( 1, 4 );
            for (int columnIndex = 0; columnIndex < getHeader().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            for (int columnIndex = 0; columnIndex < getHeaderStock().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static String[] getHeader() {
        return new String[]{"PM Code", "PM Description", "Supplier", "Category", "Stock Quantity", "Consumption", "Map", "Std Price", "SLOB Quantity", "Rate", "Stock Value", "SLOB Value", "Slow/OB", "Reason", "Remark"};
    }

    private static String[] getHeaderStock() {
        return new String[]{"Total Stock Value", "Total Slow Value", "Total Obsolete Value", "SIT"};
    }

    private static void printHeaderRow1(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 3 );
        String[] header = getHeader();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printHeaderRow2(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 0 );
        String[] header = getHeaderStock();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }


    private static void printDataRows(PMSlobDetailsDTO pmSlobDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( pmSlobDetailsDTO ).isPresent()) {
            Row row1 = sheet.createRow( 1 );
            if (Optional.ofNullable( pmSlobDetailsDTO.getTotalStockValue() ).isPresent()) {
                row1.createCell( 0 ).setCellValue( pmSlobDetailsDTO.getTotalStockValue() );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 0 ).setCellValue( " " );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( pmSlobDetailsDTO.getTotalSlowItemValue() ).isPresent()) {
                row1.createCell( 1 ).setCellValue( pmSlobDetailsDTO.getTotalSlowItemValue() );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 1 ).setCellValue( " " );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( pmSlobDetailsDTO.getTotalObsoleteItemValue() ).isPresent()) {
                row1.createCell( 2 ).setCellValue( pmSlobDetailsDTO.getTotalObsoleteItemValue() );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 2 ).setCellValue( " " );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( pmSlobDetailsDTO.getSitValue() ).isPresent()) {
                row1.createCell( 3 ).setCellValue( pmSlobDetailsDTO.getSitValue() );
                row1.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 3 ).setCellValue( " " );
                row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
            }
        }
        int rowIdx1 = 4;
        if ((Optional.ofNullable( pmSlobDetailsDTO ).isPresent()) && (Optional.ofNullable( pmSlobDetailsDTO.getPmSlobs() ).isPresent())) {
            for (PmSlobDTO pmSlobDTO : pmSlobDetailsDTO.getPmSlobs()) {
                if (Optional.ofNullable( pmSlobDTO ).isPresent()) {
                    Row row = sheet.createRow( rowIdx1++ );
                    if (Optional.ofNullable( pmSlobDTO.getCode() ).isPresent()) {
                        String materialCodeInStringFormat = pmSlobDTO.getCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
                        } catch (NumberFormatException ex) {
                            row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                        }
                    } else {
                        row.createCell( 0 ).setCellValue( " " );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getDescription() ).isPresent()) {
                        row.createCell( 1 ).setCellValue( pmSlobDTO.getDescription() );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 1 ).setCellValue( " " );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getSupplier() ).isPresent()) {
                        row.createCell( 2 ).setCellValue( pmSlobDTO.getSupplier() );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 2 ).setCellValue( " " );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getCategory() ).isPresent()) {
                        row.createCell( 3 ).setCellValue( pmSlobDTO.getCategory() );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 3 ).setCellValue( " " );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getStock() ).isPresent()) {
                        row.createCell( 4 ).setCellValue( pmSlobDTO.getStock() );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 4 ).setCellValue( " " );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getTotalStock() ).isPresent()) {
                        row.createCell( 5 ).setCellValue( pmSlobDTO.getTotalStock() );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 5 ).setCellValue( " " );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getMap() ).isPresent()) {
                        row.createCell( 6 ).setCellValue( pmSlobDTO.getMap() );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 6 ).setCellValue( " " );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getStdPrice() ).isPresent()) {
                        row.createCell( 7 ).setCellValue( pmSlobDTO.getStdPrice() );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 7 ).setCellValue( " " );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getSlobQuantity() ).isPresent()) {
                        row.createCell( 8 ).setCellValue( pmSlobDTO.getSlobQuantity() );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 8 ).setCellValue( " " );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getPrice() ).isPresent()) {
                        row.createCell( 9 ).setCellValue( pmSlobDTO.getPrice() );
                        row.getCell( 9 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 9 ).setCellValue( " " );
                        row.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getValue() ).isPresent()) {
                        row.createCell( 10 ).setCellValue( pmSlobDTO.getValue() );
                        row.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 10 ).setCellValue( " " );
                        row.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getSlobValue() ).isPresent()) {
                        row.createCell( 11 ).setCellValue( pmSlobDTO.getSlobValue() );
                        row.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 11 ).setCellValue( " " );
                        row.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getSlobType() ).isPresent()) {
                        row.createCell( 12 ).setCellValue( pmSlobDTO.getSlobType().toString() );
                        row.getCell( 12 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 12 ).setCellValue( " " );
                        row.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getReason() ).isPresent()) {
                        row.createCell( 13 ).setCellValue( pmSlobDTO.getReason() );
                        row.getCell( 13 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 13 ).setCellValue( " " );
                        row.getCell( 13 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmSlobDTO.getRemark() ).isPresent()) {
                        row.createCell( 14 ).setCellValue( pmSlobDTO.getRemark() );
                        row.getCell( 14 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 14 ).setCellValue( " " );
                        row.getCell( 14 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
