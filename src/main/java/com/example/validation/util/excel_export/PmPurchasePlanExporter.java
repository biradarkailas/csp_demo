package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.supply.PurchasePlanDTO;
import com.example.validation.dto.supply.PurchasePlanMainDTO;
import com.example.validation.util.ExcelExportUtil;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmPurchasePlanExporter {
    public static ByteArrayInputStream purchasePlanToExcel(PurchasePlanMainDTO purchasePlanDTOS, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_PURCHASE_PLAN + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            List<String> headerNames = getHeader( monthDetailDTO );
            printAdditionalDataRow( sheet, allCellStyles );
            printHeaderRow( sheet, workbook, headerNames );
            printDataRows( purchasePlanDTOS, sheet, allCellStyles );
            sheet.createFreezePane( 1, 2 );
            for (int columnIndex = 0; columnIndex < headerNames.size(); columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static void printAdditionalDataRow(Sheet sheet, List<CellStyle> allCellStyles) {
        Row additionalDataRow = sheet.createRow( 0 );
        additionalDataRow.createCell( 0 ).setCellValue( "Schedule Division :" );
        additionalDataRow.getCell( 0 ).setCellStyle( allCellStyles.get( 6 ) );
        additionalDataRow.createCell( 5 ).setCellValue( "Confirmed Schedule" );
        additionalDataRow.getCell( 5 ).setCellStyle( allCellStyles.get( 6 ) );
        additionalDataRow.createCell( 7 ).setCellValue( "Tentative Schedule" );
        additionalDataRow.getCell( 7 ).setCellStyle( allCellStyles.get( 6 ) );
        additionalDataRow.createCell( 16 ).setCellValue( "" );
        additionalDataRow.getCell( 16 ).setCellStyle( allCellStyles.get( 0 ) );
        sheet.addMergedRegion( new CellRangeAddress( 0, 0, 0, 4 ) );
        sheet.addMergedRegion( new CellRangeAddress( 0, 0, 5, 6 ) );
        sheet.addMergedRegion( new CellRangeAddress( 0, 0, 7, 16 ) );
    }

    private static List<String> getHeader(MonthDetailDTO monthDetailDTO) {
        List<String> headerNames = new ArrayList<>();
        headerNames.add( "PM Code" );
        headerNames.add( "PM Description" );
        headerNames.add( "Supplier" );
        headerNames.add( "Supplier Code" );
        headerNames.add( "RTD" );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth1().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth2().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth3().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth4().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth5().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth6().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth7().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth8().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth9().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth10().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth11().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth12().getMonthNameAlias() ) );
        headerNames.add( "Total Value" );
        headerNames.add( "Priority" );
        headerNames.add( "Remark" );
        headerNames.add( "SS" );
        headerNames.add( "MOQ" );
        return headerNames;
    }

    private static void printHeaderRow(Sheet sheet, Workbook workbook, List<String> headerNames) {
        Row headerRow = sheet.createRow( 1 );
        for (int col = 0; col < headerNames.size(); col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( headerNames.get( col ) );
            cell.setCellStyle( ExcelExportUtil.getHeaderCellStyle( workbook ) );
        }
    }

    private static void printDataRows(PurchasePlanMainDTO purchasePlanDTOS, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 2;
        if ((Optional.ofNullable( purchasePlanDTOS ).isPresent()) && (Optional.ofNullable( purchasePlanDTOS.getItemCodes() ).isPresent())) {
            for (PurchasePlanDTO purchasePlanDTO : purchasePlanDTOS.getItemCodes()) {
                Row row = sheet.createRow( rowIdx++ );
                if (Optional.ofNullable( purchasePlanDTO.getMaterialCode() ).isPresent()) {
                    String materialCodeInStringFormat = purchasePlanDTO.getMaterialCode();
                    try {
                        long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                        row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
                    } catch (NumberFormatException ex) {
                        row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                    }
                } else {
                    row.createCell( 0 ).setCellValue( " " );
                    row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getMaterialDescription() ).isPresent()) {
                    row.createCell( 1 ).setCellValue( purchasePlanDTO.getMaterialDescription() );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 1 ).setCellValue( " " );
                    row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplierName() ).isPresent()) {
                    row.createCell( 2 ).setCellValue( purchasePlanDTO.getSupplierName() );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 2 ).setCellValue( " " );
                    row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplierCode() ).isPresent()) {
                    String format = "General";
                    String materialCodeInStringFormat = purchasePlanDTO.getSupplierCode();
                    try {
                        long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                        row.createCell( 3 ).setCellValue( materialCodeInNumberFormat );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 3 ) );
                    } catch (NumberFormatException ex) {
                        row.createCell( 3 ).setCellValue( materialCodeInStringFormat );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 2 ) );
                    }
                } else {
                    row.createCell( 3 ).setCellValue( " " );
                    row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getRtdValue() ).isPresent()) {
                    row.createCell( 4 ).setCellValue( purchasePlanDTO.getRtdValue() );
                    row.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row.createCell( 4 ).setCellValue( " " );
                    row.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth1().getValue() ).isPresent()) {
                    row.createCell( 5 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth1().getValue() );
                    row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 5 ).setCellValue( " " );
                    row.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth2().getValue() ).isPresent()) {
                    row.createCell( 6 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth2().getValue() );
                    row.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 6 ).setCellValue( " " );
                    row.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth3().getValue() ).isPresent()) {
                    row.createCell( 7 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth3().getValue() );
                    row.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 7 ).setCellValue( " " );
                    row.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth4().getValue() ).isPresent()) {
                    row.createCell( 8 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth4().getValue() );
                    row.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 8 ).setCellValue( " " );
                    row.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth5().getValue() ).isPresent()) {
                    row.createCell( 9 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth5().getValue() );
                    row.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 9 ).setCellValue( " " );
                    row.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth6().getValue() ).isPresent()) {
                    row.createCell( 10 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth6().getValue() );
                    row.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 10 ).setCellValue( " " );
                    row.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth7().getValue() ).isPresent()) {
                    row.createCell( 11 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth7().getValue() );
                    row.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 11 ).setCellValue( " " );
                    row.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth8().getValue() ).isPresent()) {
                    row.createCell( 12 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth8().getValue() );
                    row.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 12 ).setCellValue( " " );
                    row.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth9().getValue() ).isPresent()) {
                    row.createCell( 13 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth9().getValue() );
                    row.getCell( 13 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 13 ).setCellValue( " " );
                    row.getCell( 13 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth10().getValue() ).isPresent()) {
                    row.createCell( 14 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth10().getValue() );
                    row.getCell( 14 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 14 ).setCellValue( " " );
                    row.getCell( 14 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth11().getValue() ).isPresent()) {
                    row.createCell( 15 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth11().getValue() );
                    row.getCell( 15 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 15 ).setCellValue( " " );
                    row.getCell( 15 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getSupplyMonthValues().getMonth12().getValue() ).isPresent()) {
                    row.createCell( 16 ).setCellValue( purchasePlanDTO.getSupplyMonthValues().getMonth12().getValue() );
                    row.getCell( 16 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 16 ).setCellValue( " " );
                    row.getCell( 16 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getTotalValue() ).isPresent()) {
                    row.createCell( 17 ).setCellValue( purchasePlanDTO.getTotalValue() );
                    row.getCell( 17 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 17 ).setCellValue( " " );
                    row.getCell( 17 ).setCellStyle( allCellStyles.get( 0 ) );
                }

                if (purchasePlanDTO.getPriority() == 1) {
                    row.createCell( 18 ).setCellValue( "priority" );
                    row.getCell( 18 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 18 ).setCellValue( " " );
                    row.getCell( 18 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getRemarkName() ).isPresent()) {
                    row.createCell( 19 ).setCellValue( purchasePlanDTO.getRemarkName() );
                    row.getCell( 19 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row.createCell( 19 ).setCellValue( " " );
                    row.getCell( 19 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getStockValue() ).isPresent()) {
                    row.createCell( 20 ).setCellValue( purchasePlanDTO.getStockValue() );
                    row.getCell( 20 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 20 ).setCellValue( " " );
                    row.getCell( 20 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( purchasePlanDTO.getMoqValue() ).isPresent()) {
                    row.createCell( 21 ).setCellValue( purchasePlanDTO.getMoqValue() );
                    row.getCell( 21 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row.createCell( 21 ).setCellValue( " " );
                    row.getCell( 21 ).setCellStyle( allCellStyles.get( 0 ) );
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
