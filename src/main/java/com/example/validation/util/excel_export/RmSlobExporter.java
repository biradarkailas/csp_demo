package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.RMSlobDetailsDTO;
import com.example.validation.dto.RmSlobDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RmSlobExporter {
    public static ByteArrayInputStream rmSlobToExcel(RMSlobDetailsDTO rmSlobDetailsDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.RM_SLOB + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow1( rmSlobDetailsDTO, monthDetailDTO, sheet, allCellStyles.get( 1 ) );
            printDataRows( rmSlobDetailsDTO, sheet, allCellStyles );
            printAdditionalDataRow( rmSlobDetailsDTO, sheet, allCellStyles );
            sheet.createFreezePane( 1, 2 );
            for (int columnIndex = 0; columnIndex < getHeader( rmSlobDetailsDTO, monthDetailDTO ).length; columnIndex++) {
                if (columnIndex != 28 && columnIndex != 27 && columnIndex != 29 && columnIndex != 30 && columnIndex != 2 && columnIndex != 3)
                    sheet.autoSizeColumn( columnIndex );
            }
            sheet.setColumnWidth( 2, 3500 );
            sheet.setColumnWidth( 3, 3200 );
            sheet.setColumnWidth( 27, 3200 );
            sheet.setColumnWidth( 28, 3200 );
            sheet.setColumnWidth( 29, 3500 );
            sheet.setColumnWidth( 30, 3500 );
            sheet.setColumnWidth( 30, 3000 );
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static void printAdditionalDataRow(RMSlobDetailsDTO rmSlobDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        Row additionalDataRow1 = sheet.createRow( 0 );
        if (Optional.ofNullable( rmSlobDetailsDTO ).isPresent()) {
            sheet.setColumnWidth( 28, 4200 );
            sheet.setColumnWidth( 29, 4200 );
            sheet.setColumnWidth( 30, 4200 );
            sheet.setColumnWidth( 31, 4200 );
            sheet.setColumnWidth( 33, 4200 );
            additionalDataRow1.createCell( 25 ).setCellValue( "SLOB Value" );
            sheet.addMergedRegion( new CellRangeAddress( 0, 0, 25, 27 ) );
            additionalDataRow1.getCell( 25 ).setCellStyle( allCellStyles.get( 8 ) );
            additionalDataRow1.createCell( 28 ).setCellValue( "Obsolete Expired Value" );
            sheet.addMergedRegion( new CellRangeAddress( 0, 0, 28, 30 ) );
            additionalDataRow1.getCell( 28 ).setCellStyle( allCellStyles.get( 8 ) );
            additionalDataRow1.createCell( 31 ).setCellValue( "Obsolete Non Expired Value" );
            sheet.addMergedRegion( new CellRangeAddress( 0, 0, 31, 33 ) );
            additionalDataRow1.getCell( 31 ).setCellStyle( allCellStyles.get( 8 ) );
            additionalDataRow1.createCell( 34 ).setCellValue( "Slow Moving Value" );
            sheet.addMergedRegion( new CellRangeAddress( 0, 0, 34, 36 ) );
            additionalDataRow1.getCell( 34 ).setCellStyle( allCellStyles.get( 8 ) );
            for (int col = 25; col < 37; col++) {
                if (col != 25 && col != 28 && col != 31 && col != 34) {
                    additionalDataRow1.createCell( col ).setCellValue( "" );
                    additionalDataRow1.getCell( col ).setCellStyle( allCellStyles.get( 8 ) );
                }
            }
        }
    }

    private static String[] getHeader(RMSlobDetailsDTO rmSlobDetailsDTO, MonthDetailDTO monthDetailDTO) {
        String m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value, currentMonthValue, previousMonthValue;
        if (Optional.ofNullable( monthDetailDTO.getMonth1().getMonthNameAlias() ).isPresent()) {
            m1Value = monthDetailDTO.getMonth1().getMonthNameAlias();
        } else {
            m1Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth2().getMonthNameAlias() ).isPresent()) {
            m2Value = monthDetailDTO.getMonth2().getMonthNameAlias();
        } else {
            m2Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth3().getMonthNameAlias() ).isPresent()) {
            m3Value = monthDetailDTO.getMonth3().getMonthNameAlias();
        } else {
            m3Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth4().getMonthNameAlias() ).isPresent()) {
            m4Value = monthDetailDTO.getMonth4().getMonthNameAlias();
        } else {
            m4Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth5().getMonthNameAlias() ).isPresent()) {
            m5Value = monthDetailDTO.getMonth5().getMonthNameAlias();
        } else {
            m5Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth6().getMonthNameAlias() ).isPresent()) {
            m6Value = monthDetailDTO.getMonth6().getMonthNameAlias();
        } else {
            m6Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth7().getMonthNameAlias() ).isPresent()) {
            m7Value = monthDetailDTO.getMonth7().getMonthNameAlias();
        } else {
            m7Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth8().getMonthNameAlias() ).isPresent()) {
            m8Value = monthDetailDTO.getMonth8().getMonthNameAlias();
        } else {
            m8Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth9().getMonthNameAlias() ).isPresent()) {
            m9Value = monthDetailDTO.getMonth9().getMonthNameAlias();
        } else {
            m9Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth10().getMonthNameAlias() ).isPresent()) {
            m10Value = monthDetailDTO.getMonth10().getMonthNameAlias();
        } else {
            m10Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth11().getMonthNameAlias() ).isPresent()) {
            m11Value = monthDetailDTO.getMonth11().getMonthNameAlias();
        } else {
            m11Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth12().getMonthNameAlias() ).isPresent()) {
            m12Value = monthDetailDTO.getMonth12().getMonthNameAlias();
        } else {
            m12Value = "";
        }

        if (Optional.ofNullable( rmSlobDetailsDTO ).isPresent()) {
            String currentMonthName, previousMonthName;
            if (Optional.ofNullable( rmSlobDetailsDTO.getCurrentMonthValue() ).isPresent()) {
                currentMonthValue = rmSlobDetailsDTO.getCurrentMonthValue();
            } else {
                currentMonthValue = "";
            }
            if (Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthValue() ).isPresent()) {
                previousMonthValue = rmSlobDetailsDTO.getPreviousMonthValue();
            } else {
                previousMonthValue = "";
            }
            return new String[]{"RM Code", "RM Description", "Midnight Stock", "Expired Stock", "Final Stock", m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value, "Total Consumption", "Balance Stock", "MAP", "Std.Price", "SLOB Qty", "Rate", "Stock Value", "SLOB Type", currentMonthValue, previousMonthValue, "Diff", currentMonthValue, previousMonthValue, "Diff", currentMonthValue, previousMonthValue, "Diff", currentMonthValue, previousMonthValue, "Diff", "Total SLOB Value", "Reason", "Remark"};
        } else

            return new String[]{"RM Code", "RM Description", "Midnight Stock", "Expired Stock", "Final Stock", m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value, "Total Consumption", "Balance Stock", "MAP", "Std.Price", "SLOB Qty", "Rate", "Stock Value", "SLOB Type", "", "", "", "", "", "", "", "", "", "", "", "", "Total SLOB Value", "Reason", "Remark"};
    }

    private static void printHeaderRow1(RMSlobDetailsDTO rmSlobDetailsDTO, MonthDetailDTO monthDetailDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 1 );
        String[] header = getHeader( rmSlobDetailsDTO, monthDetailDTO );
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(RMSlobDetailsDTO rmSlobDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( rmSlobDetailsDTO ).isPresent()) {
            int rowIdx1 = 2;
            if (Optional.ofNullable( rmSlobDetailsDTO.getRmSlobs() ).isPresent()) {
                for (RmSlobDTO rmSlobs : rmSlobDetailsDTO.getRmSlobs()) {
                    Row row = sheet.createRow( rowIdx1++ );
                    if (Optional.ofNullable( rmSlobs.getCode() ).isPresent()) {
                        String format = "General";
                        String materialCodeInStringFormat = rmSlobs.getCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 12 ) );
                        } catch (NumberFormatException ex) {
                            row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 11 ) );
                        }
                    } else {
                        row.createCell( 0 ).setCellValue( 0 );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getDescription() ).isPresent()) {
                        row.createCell( 1 ).setCellValue( rmSlobs.getDescription() );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 1 ).setCellValue( " " );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getMidNightStock() ).isPresent()) {
                        row.createCell( 2 ).setCellValue( rmSlobs.getMidNightStock() );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 2 ).setCellValue( 0 );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getExpiredStock() ).isPresent()) {
                        row.createCell( 3 ).setCellValue( rmSlobs.getExpiredStock() );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 3 ).setCellValue( 0 );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getFinalStock() ).isPresent()) {
                        row.createCell( 4 ).setCellValue( rmSlobs.getFinalStock() );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 4 ).setCellValue( 0 );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth1Value() ).isPresent()) {
                            row.createCell( 5 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth1Value() );
                            row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 5 ).setCellValue( 0 );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth2Value() ).isPresent()) {
                            row.createCell( 6 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth2Value().longValue() );
                            row.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 6 ).setCellValue( 0 );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth3Value() ).isPresent()) {
                            row.createCell( 7 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth3Value() );
                            row.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 7 ).setCellValue( 0 );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth4Value() ).isPresent()) {
                            row.createCell( 8 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth4Value() );
                            row.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 8 ).setCellValue( 0 );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth5Value() ).isPresent()) {
                            row.createCell( 9 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth5Value().longValue() );
                            row.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 9 ).setCellValue( 0 );
                        row.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth6Value() ).isPresent()) {
                            row.createCell( 10 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth6Value() );
                            row.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 10 ).setCellValue( 0 );
                        row.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth7Value() ).isPresent()) {
                            row.createCell( 11 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth7Value() );
                            row.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 11 ).setCellValue( 0 );
                        row.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth8Value() ).isPresent()) {
                            row.createCell( 12 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth8Value() );
                            row.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 12 ).setCellValue( 0 );
                        row.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth9Value() ).isPresent()) {
                            row.createCell( 13 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth9Value() );
                            row.getCell( 13 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 13 ).setCellValue( 0 );
                        row.getCell( 13 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth10Value() ).isPresent()) {
                            row.createCell( 14 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth10Value() );
                            row.getCell( 14 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 14 ).setCellValue( 0 );
                        row.getCell( 14 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth11Value() ).isPresent()) {
                            row.createCell( 15 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth11Value() );
                            row.getCell( 15 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 15 ).setCellValue( 0 );
                        row.getCell( 15 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    try {
                        if (Optional.ofNullable( rmSlobs.getMonthValuesDTO().getMonth12Value() ).isPresent()) {
                            row.createCell( 16 ).setCellValue( rmSlobs.getMonthValuesDTO().getMonth12Value() );
                            row.getCell( 16 ).setCellStyle( allCellStyles.get( 4 ) );
                        }
                    } catch (NullPointerException e) {
                        row.createCell( 16 ).setCellValue( 0 );
                        row.getCell( 16 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getTotalConsumptionQuantity() ).isPresent()) {
                        row.createCell( 17 ).setCellValue( rmSlobs.getTotalConsumptionQuantity() );
                        row.getCell( 17 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 17 ).setCellValue( 0 );
                        row.getCell( 17 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getBalanceStock() ).isPresent()) {
                        row.createCell( 18 ).setCellValue( rmSlobs.getBalanceStock() );
                        row.getCell( 18 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 18 ).setCellValue( 0 );
                        row.getCell( 18 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getMap() ).isPresent()) {
                        row.createCell( 19 ).setCellValue( rmSlobs.getMap() );
                        row.getCell( 19 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 19 ).setCellValue( 0 );
                        row.getCell( 19 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getStdPrice() ).isPresent()) {
                        row.createCell( 20 ).setCellValue( rmSlobs.getStdPrice() );
                        row.getCell( 20 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 20 ).setCellValue( 0 );
                        row.getCell( 20 ).setCellStyle( allCellStyles.get( 3 ) );
                    }

                    if (Optional.ofNullable( rmSlobs.getSlobQuantity() ).isPresent()) {
                        row.createCell( 21 ).setCellValue( rmSlobs.getSlobQuantity() );
                        row.getCell( 21 ).setCellStyle( allCellStyles.get( 0 ) );
                    } else {
                        row.createCell( 21 ).setCellValue( 0 );
                        row.getCell( 21 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getPrice() ).isPresent()) {
                        row.createCell( 22 ).setCellValue( rmSlobs.getPrice() );
                        row.getCell( 22 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 22 ).setCellValue( 0 );
                        row.getCell( 22 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getTotalStockValue() ).isPresent()) {
                        row.createCell( 23 ).setCellValue( rmSlobs.getTotalStockValue() );
                        row.getCell( 23 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 23 ).setCellValue( 0 );
                        row.getCell( 23 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getSlobType() ).isPresent()) {
                        row.createCell( 24 ).setCellValue( rmSlobs.getSlobType().toString() );
                        row.getCell( 24 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 24 ).setCellValue( " " );
                        row.getCell( 24 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    double slobValue = 0;
                    if (Optional.ofNullable( rmSlobs.getSlobValue() ).isPresent()) {
                        slobValue = rmSlobs.getSlobValue();
                        row.createCell( 25 ).setCellValue( rmSlobs.getSlobValue() );
                        row.getCell( 25 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        slobValue = 0;
                        row.createCell( 25 ).setCellValue( 0 );
                        row.getCell( 25 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    double previousMonthsSlobValue = 0;
                    if (Optional.ofNullable( rmSlobs.getPreviousMonthsSlobValue() ).isPresent()) {
                        previousMonthsSlobValue = rmSlobs.getPreviousMonthsSlobValue();
                        row.createCell( 26 ).setCellValue( rmSlobs.getPreviousMonthsSlobValue() );
                        row.getCell( 26 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        previousMonthsSlobValue = 0;
                        row.createCell( 26 ).setCellValue( 0 );
                        row.getCell( 26 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    if ((Optional.ofNullable( rmSlobs.getPreviousMonthsSlobValue() ).isPresent()) || (Optional.ofNullable( rmSlobs.getSlobValue() ).isPresent())) {
                        row.createCell( 27 ).setCellValue( slobValue - previousMonthsSlobValue );
                        row.getCell( 27 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else {
                        row.createCell( 27 ).setCellValue( 0 );
                        row.getCell( 27 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getObsoleteExpiryValue() ).isPresent()) {
                        row.createCell( 28 ).setCellValue( rmSlobs.getObsoleteExpiryValue() );
                        row.getCell( 28 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 28 ).setCellValue( 0 );
                        row.getCell( 28 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getPreviousMonthsObsoleteExpiryValue() ).isPresent()) {
                        row.createCell( 29 ).setCellValue( rmSlobs.getPreviousMonthsObsoleteExpiryValue() );
                        row.getCell( 29 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 29 ).setCellValue( 0 );
                        row.getCell( 29 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if ((Optional.ofNullable( rmSlobs.getPreviousMonthsObsoleteExpiryValue() ).isPresent()) && (Optional.ofNullable( rmSlobs.getObsoleteExpiryValue() ).isPresent())) {
                        row.createCell( 30 ).setCellValue( rmSlobs.getObsoleteExpiryValue() - rmSlobs.getPreviousMonthsObsoleteExpiryValue() );
                        row.getCell( 30 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else if ((Optional.ofNullable( rmSlobs.getObsoleteExpiryValue() ).isPresent())) {
                        row.createCell( 30 ).setCellValue(rmSlobs.getObsoleteExpiryValue() - 0 );
                        row.getCell( 30 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else if ((Optional.ofNullable( rmSlobs.getPreviousMonthsObsoleteExpiryValue() ).isPresent())) {
                        row.createCell( 30 ).setCellValue( 0 - rmSlobs.getPreviousMonthsObsoleteExpiryValue() );
                        row.getCell( 30 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else {
                        row.createCell( 30 ).setCellValue( 0 );
                        row.getCell( 30 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getObsoleteNonExpiryValue() ).isPresent()) {
                        row.createCell( 31 ).setCellValue( rmSlobs.getObsoleteNonExpiryValue() );
                        row.getCell( 31 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 31 ).setCellValue( 0 );
                        row.getCell( 31 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getPreviousMonthsObsoleteNonExpiryValue() ).isPresent()) {
                        row.createCell( 32 ).setCellValue( rmSlobs.getPreviousMonthsObsoleteNonExpiryValue() );
                        row.getCell( 32 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 32 ).setCellValue( 0 );
                        row.getCell( 32 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if ((Optional.ofNullable( rmSlobs.getObsoleteNonExpiryValue() ).isPresent()) && (Optional.ofNullable( rmSlobs.getPreviousMonthsObsoleteNonExpiryValue() ).isPresent())) {
                        row.createCell( 33 ).setCellValue( rmSlobs.getObsoleteNonExpiryValue() - rmSlobs.getPreviousMonthsObsoleteNonExpiryValue() );
                        row.getCell( 33 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else if ((Optional.ofNullable( rmSlobs.getObsoleteNonExpiryValue() ).isPresent())) {
                        row.createCell( 33 ).setCellValue( rmSlobs.getObsoleteNonExpiryValue() - 0 );
                        row.getCell( 33 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else if ((Optional.ofNullable( rmSlobs.getPreviousMonthsObsoleteNonExpiryValue() ).isPresent())) {
                        row.createCell( 33 ).setCellValue(0 - rmSlobs.getPreviousMonthsObsoleteNonExpiryValue() );
                        row.getCell( 33 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else {
                        row.createCell( 33 ).setCellValue( 0 );
                        row.getCell( 33 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getSlowMovingValue() ).isPresent()) {
                        row.createCell( 34 ).setCellValue( rmSlobs.getSlowMovingValue() );
                        row.getCell( 34 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 34 ).setCellValue( 0 );
                        row.getCell( 34 ).setCellStyle( allCellStyles.get( 4 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getPreviousMonthsSlowMovingValue() ).isPresent()) {
                        row.createCell( 35 ).setCellValue( rmSlobs.getPreviousMonthsSlowMovingValue() );
                        row.getCell( 35 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 35 ).setCellValue( 0 );
                        row.getCell( 35 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if ((Optional.ofNullable( rmSlobs.getSlowMovingValue() ).isPresent()) && (Optional.ofNullable( rmSlobs.getPreviousMonthsSlowMovingValue() ).isPresent())) {
                        row.createCell( 36 ).setCellValue( rmSlobs.getSlowMovingValue() - rmSlobs.getPreviousMonthsSlowMovingValue() );
                        row.getCell( 36 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else if ((Optional.ofNullable( rmSlobs.getSlowMovingValue() ).isPresent())) {
                        row.createCell( 36 ).setCellValue( rmSlobs.getSlowMovingValue() - 0 );
                        row.getCell( 36 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else if ((Optional.ofNullable( rmSlobs.getPreviousMonthsSlowMovingValue() ).isPresent())) {
                        row.createCell( 36 ).setCellValue(0 - rmSlobs.getPreviousMonthsSlowMovingValue() );
                        row.getCell( 36 ).setCellStyle( allCellStyles.get( 3 ) );
                    } else {
                        row.createCell( 36 ).setCellValue( 0 );
                        row.getCell( 36 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getTotalSlobValue() ).isPresent()) {
                        row.createCell( 37 ).setCellValue( rmSlobs.getTotalSlobValue() );
                        row.getCell( 37 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 37 ).setCellValue( 0 );
                        row.getCell( 37 ).setCellStyle( allCellStyles.get( 3 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getReason() ).isPresent()) {
                        row.createCell( 38 ).setCellValue( rmSlobs.getReason() );
                        row.getCell( 38 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 38 ).setCellValue( " " );
                        row.getCell( 38 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rmSlobs.getRemark() ).isPresent()) {
                        row.createCell( 39 ).setCellValue( rmSlobs.getRemark() );
                        row.getCell( 39 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 39 ).setCellValue( " " );
                        row.getCell( 39 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
            Row additionalDataRow = sheet.createRow( rowIdx1++ );
            additionalDataRow.createCell( 0 ).setCellValue( "SUM" );
            additionalDataRow.getCell( 0 ).setCellStyle( allCellStyles.get( 14 ) );
            additionalDataRow.createCell( 1 ).setCellValue( " " );
            additionalDataRow.getCell( 1 ).setCellStyle( allCellStyles.get( 14 ) );

            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalMidNightStockValue() ).isPresent()) {
                additionalDataRow.createCell( 2 ).setCellValue( rmSlobDetailsDTO.getTotalMidNightStockValue() );
                additionalDataRow.getCell( 2 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 2 ).setCellValue( 0 );
                additionalDataRow.getCell( 2 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalExpiredStockValue() ).isPresent()) {
                additionalDataRow.createCell( 3 ).setCellValue( rmSlobDetailsDTO.getTotalExpiredStockValue() );
                additionalDataRow.getCell( 3 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 3 ).setCellValue( 0 );
                additionalDataRow.getCell( 3 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalFinalStockValue() ).isPresent()) {
                additionalDataRow.createCell( 4 ).setCellValue( rmSlobDetailsDTO.getTotalFinalStockValue() );
                additionalDataRow.getCell( 4 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 4 ).setCellValue( 0 );
                additionalDataRow.getCell( 4 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM1TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 5 ).setCellValue( rmSlobDetailsDTO.getM1TotalQuantity() );
                    additionalDataRow.getCell( 5 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 5 ).setCellValue( 0 );
                additionalDataRow.getCell( 5 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM2TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 6 ).setCellValue( rmSlobDetailsDTO.getM2TotalQuantity().longValue() );
                    additionalDataRow.getCell( 6 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 6 ).setCellValue( 0 );
                additionalDataRow.getCell( 6 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM3TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 7 ).setCellValue( rmSlobDetailsDTO.getM3TotalQuantity() );
                    additionalDataRow.getCell( 7 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 7 ).setCellValue( 0 );
                additionalDataRow.getCell( 7 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM4TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 8 ).setCellValue( rmSlobDetailsDTO.getM4TotalQuantity() );
                    additionalDataRow.getCell( 8 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 8 ).setCellValue( 0 );
                additionalDataRow.getCell( 8 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM5TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 9 ).setCellValue( rmSlobDetailsDTO.getM5TotalQuantity().longValue() );
                    additionalDataRow.getCell( 9 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 9 ).setCellValue( 0 );
                additionalDataRow.getCell( 9 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM6TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 10 ).setCellValue( rmSlobDetailsDTO.getM6TotalQuantity() );
                    additionalDataRow.getCell( 10 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 10 ).setCellValue( 0 );
                additionalDataRow.getCell( 10 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM7TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 11 ).setCellValue( rmSlobDetailsDTO.getM7TotalQuantity() );
                    additionalDataRow.getCell( 11 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 11 ).setCellValue( 0 );
                additionalDataRow.getCell( 11 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM8TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 12 ).setCellValue( rmSlobDetailsDTO.getM8TotalQuantity() );
                    additionalDataRow.getCell( 12 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 12 ).setCellValue( 0 );
                additionalDataRow.getCell( 12 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM9TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 13 ).setCellValue( rmSlobDetailsDTO.getM9TotalQuantity() );
                    additionalDataRow.getCell( 13 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 13 ).setCellValue( 0 );
                additionalDataRow.getCell( 13 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM10TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 14 ).setCellValue( rmSlobDetailsDTO.getM10TotalQuantity() );
                    additionalDataRow.getCell( 14 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 14 ).setCellValue( 0 );
                additionalDataRow.getCell( 14 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM11TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 15 ).setCellValue( rmSlobDetailsDTO.getM11TotalQuantity() );
                    additionalDataRow.getCell( 15 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 15 ).setCellValue( 0 );
                additionalDataRow.getCell( 15 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            try {
                if (Optional.ofNullable( rmSlobDetailsDTO.getM12TotalQuantity() ).isPresent()) {
                    additionalDataRow.createCell( 16 ).setCellValue( rmSlobDetailsDTO.getM12TotalQuantity() );
                    additionalDataRow.getCell( 16 ).setCellStyle( allCellStyles.get( 13 ) );
                }
            } catch (NullPointerException e) {
                additionalDataRow.createCell( 16 ).setCellValue( 0 );
                additionalDataRow.getCell( 16 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalConsumptionQuantity() ).isPresent()) {
                additionalDataRow.createCell( 17 ).setCellValue( rmSlobDetailsDTO.getTotalConsumptionQuantity() );
                additionalDataRow.getCell( 17 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 17 ).setCellValue( 0 );
                additionalDataRow.getCell( 17 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            additionalDataRow.createCell( 18 ).setCellValue( " " );
            additionalDataRow.getCell( 18 ).setCellStyle( allCellStyles.get( 14 ) );
            additionalDataRow.createCell( 19 ).setCellValue( " " );
            additionalDataRow.getCell( 19 ).setCellStyle( allCellStyles.get( 14 ) );
            additionalDataRow.createCell( 20 ).setCellValue( " " );
            additionalDataRow.getCell( 20 ).setCellStyle( allCellStyles.get( 14 ) );
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalSlobQuantityValue() ).isPresent()) {
                additionalDataRow.createCell( 21 ).setCellValue( rmSlobDetailsDTO.getTotalSlobQuantityValue() );
                additionalDataRow.getCell( 21 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 21 ).setCellValue( " " );
                additionalDataRow.getCell( 21 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            additionalDataRow.createCell( 22 ).setCellValue( " " );
            additionalDataRow.getCell( 22 ).setCellStyle( allCellStyles.get( 14 ) );
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalStockValue() ).isPresent()) {
                additionalDataRow.createCell( 23 ).setCellValue( rmSlobDetailsDTO.getTotalStockValue() );
                additionalDataRow.getCell( 23 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 23 ).setCellValue( 0 );
                additionalDataRow.getCell( 23 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            additionalDataRow.createCell( 24 ).setCellValue( " " );
            additionalDataRow.getCell( 24 ).setCellStyle( allCellStyles.get( 14 ) );
            double totalSlobValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalSlobValue() ).isPresent()) {
                totalSlobValue = rmSlobDetailsDTO.getTotalSlobValue();
                additionalDataRow.createCell( 25 ).setCellValue( rmSlobDetailsDTO.getTotalSlobValue() );
                additionalDataRow.getCell( 25 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                totalSlobValue = 0;
                additionalDataRow.createCell( 25 ).setCellValue( 0 );
                additionalDataRow.getCell( 25 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            double previousMonthsTotalSlobValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalSlobValue() ).isPresent()) {
                previousMonthsTotalSlobValue = rmSlobDetailsDTO.getPreviousMonthsTotalSlobValue();
                additionalDataRow.createCell( 26 ).setCellValue( rmSlobDetailsDTO.getPreviousMonthsTotalSlobValue() );
                additionalDataRow.getCell( 26 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                previousMonthsTotalSlobValue = 0;
                additionalDataRow.createCell( 26 ).setCellValue( 0 );
                additionalDataRow.getCell( 26 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            if ((Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalSlobValue() ).isPresent()) || (Optional.ofNullable( rmSlobDetailsDTO.getTotalSlobValue() ).isPresent())) {
                additionalDataRow.createCell( 27 ).setCellValue( totalSlobValue - previousMonthsTotalSlobValue );
                additionalDataRow.getCell( 27 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 27 ).setCellValue( 0 );
                additionalDataRow.getCell( 27 ).setCellStyle( allCellStyles.get( 13 ) );
            }

            double totalObsoleteExpiryValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalObsoleteExpiryValue() ).isPresent()) {
                totalObsoleteExpiryValue = rmSlobDetailsDTO.getTotalObsoleteExpiryValue();
                additionalDataRow.createCell( 28 ).setCellValue( rmSlobDetailsDTO.getTotalObsoleteExpiryValue() );
                additionalDataRow.getCell( 28 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                totalObsoleteExpiryValue = 0;
                additionalDataRow.createCell( 28 ).setCellValue( 0 );
                additionalDataRow.getCell( 28 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            double previousMonthsTotalObsoleteExpiryValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteExpiryValue() ).isPresent()) {
                previousMonthsTotalObsoleteExpiryValue = rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteExpiryValue();
                additionalDataRow.createCell( 29 ).setCellValue( rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteExpiryValue() );
                additionalDataRow.getCell( 29 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                previousMonthsTotalObsoleteExpiryValue = 0;
                additionalDataRow.createCell( 29 ).setCellValue( 0 );
                additionalDataRow.getCell( 29 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            if ((Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteExpiryValue() ).isPresent()) || (Optional.ofNullable( rmSlobDetailsDTO.getTotalObsoleteExpiryValue() ).isPresent())) {
                additionalDataRow.createCell( 30 ).setCellValue( totalObsoleteExpiryValue - previousMonthsTotalObsoleteExpiryValue );
                additionalDataRow.getCell( 30 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 30 ).setCellValue( " " );
                additionalDataRow.getCell( 30 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            double totalObsoleteNonExpiryValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalObsoleteNonExpiryValue() ).isPresent()) {
                totalObsoleteNonExpiryValue = rmSlobDetailsDTO.getTotalObsoleteNonExpiryValue();
                additionalDataRow.createCell( 31 ).setCellValue( rmSlobDetailsDTO.getTotalObsoleteNonExpiryValue() );
                additionalDataRow.getCell( 31 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                totalObsoleteNonExpiryValue = 0;
                additionalDataRow.createCell( 31 ).setCellValue( 0 );
                additionalDataRow.getCell( 31 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            double previousMonthsTotalObsoleteNonExpiryValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteNonExpiryValue() ).isPresent()) {
                previousMonthsTotalObsoleteNonExpiryValue = rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteNonExpiryValue();
                additionalDataRow.createCell( 32 ).setCellValue( rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteNonExpiryValue() );
                additionalDataRow.getCell( 32 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                previousMonthsTotalObsoleteNonExpiryValue = 0;
                additionalDataRow.createCell( 32 ).setCellValue( 0 );
                additionalDataRow.getCell( 32 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            if ((Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalObsoleteNonExpiryValue() ).isPresent()) || (Optional.ofNullable( rmSlobDetailsDTO.getTotalObsoleteNonExpiryValue() ).isPresent())) {
                additionalDataRow.createCell( 33 ).setCellValue( totalObsoleteNonExpiryValue - previousMonthsTotalObsoleteNonExpiryValue );
                additionalDataRow.getCell( 33 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 33 ).setCellValue( " " );
                additionalDataRow.getCell( 33 ).setCellStyle( allCellStyles.get( 14 ) );
            }
            double totalSlowItemValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalSlowItemValue() ).isPresent()) {
                totalSlowItemValue = rmSlobDetailsDTO.getTotalSlowItemValue();
                additionalDataRow.createCell( 34 ).setCellValue( rmSlobDetailsDTO.getTotalSlowItemValue() );
                additionalDataRow.getCell( 34 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                totalSlowItemValue = 0;
                additionalDataRow.createCell( 34 ).setCellValue( 0 );
                additionalDataRow.getCell( 34 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            double previousMonthsTotalSlowItemValue = 0;
            if (Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalSlowItemValue() ).isPresent()) {
                previousMonthsTotalSlowItemValue = rmSlobDetailsDTO.getPreviousMonthsTotalSlowItemValue();
                additionalDataRow.createCell( 35 ).setCellValue( rmSlobDetailsDTO.getPreviousMonthsTotalSlowItemValue() );
                additionalDataRow.getCell( 35 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                previousMonthsTotalSlowItemValue = 0;
                additionalDataRow.createCell( 35 ).setCellValue( 0 );
                additionalDataRow.getCell( 35 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            if ((Optional.ofNullable( rmSlobDetailsDTO.getTotalSlowItemValue() ).isPresent()) || (Optional.ofNullable( rmSlobDetailsDTO.getPreviousMonthsTotalSlowItemValue() ).isPresent())) {
                additionalDataRow.createCell( 36 ).setCellValue( totalSlowItemValue - previousMonthsTotalSlowItemValue );
                additionalDataRow.getCell( 36 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 36 ).setCellValue( 0 );
                additionalDataRow.getCell( 36 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            if (Optional.ofNullable( rmSlobDetailsDTO.getTotalSlobValue() ).isPresent()) {
                additionalDataRow.createCell( 37 ).setCellValue( rmSlobDetailsDTO.getTotalSlobValue() );
                additionalDataRow.getCell( 37 ).setCellStyle( allCellStyles.get( 13 ) );
            } else {
                additionalDataRow.createCell( 37 ).setCellValue( 0 );
                additionalDataRow.getCell( 37 ).setCellStyle( allCellStyles.get( 13 ) );
            }
            additionalDataRow.createCell( 38 ).setCellValue( " " );
            additionalDataRow.getCell( 38 ).setCellStyle( allCellStyles.get( 14 ) );
            additionalDataRow.createCell( 39 ).setCellValue( " " );
            additionalDataRow.getCell( 39 ).setCellStyle( allCellStyles.get( 14 ) );
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        CellStyle commaSeparatedNumberNegDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//15
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}

