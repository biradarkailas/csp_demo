package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.slob.FGSlobTotalValueDetailDTO;
import com.example.validation.dto.slob.RscIotFGSlobDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FgSlobSaturationExporter {
    public static ByteArrayInputStream slobSaturationToExcel(List<RscIotFGSlobDTO> rscIotFGSlobDTOList, FGSlobTotalValueDetailDTO fgSlobTotalValueDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.FG_SLOB_SATURATION + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow1( sheet, allCellStyles.get( 1 ) );
            printHeaderRow2( sheet, allCellStyles.get( 1 ) );
            sheet.createFreezePane( 1, 4 );
            printDataRows( rscIotFGSlobDTOList, fgSlobTotalValueDetailDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            for (int columnIndex = 0; columnIndex < getHeaderStock().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static String[] getHeader() {
        return new String[]{"FG Code", "Description", "Stock Qty", "Map", "Stock Value", "Total Plan Qty", "Slob Qty", "Slob Value", "SL/OB"};
    }

    private static String[] getHeaderStock() {
        return new String[]{"Total Stock Value", "Total Slob Value ", "Total Slow Item Value", "Slob OB Item Value"};
    }

    private static void printHeaderRow1(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 3 );
        String[] header = getHeader();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printHeaderRow2(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 0 );
        String[] header = getHeaderStock();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(List<RscIotFGSlobDTO> rscIotFGSlobDTOList, FGSlobTotalValueDetailDTO fgSlobTotalValueDetailDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 1;
        if (Optional.ofNullable( fgSlobTotalValueDetailDTO ).isPresent()) {
            Row row1 = sheet.createRow( rowIdx );
            if (Optional.ofNullable( fgSlobTotalValueDetailDTO.getTotalStockValue() ).isPresent()) {
                row1.createCell( 0 ).setCellValue( fgSlobTotalValueDetailDTO.getTotalStockValue() );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 0 ).setCellValue( " " );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( fgSlobTotalValueDetailDTO.getTotalSlobValue() ).isPresent()) {
                row1.createCell( 1 ).setCellValue( fgSlobTotalValueDetailDTO.getTotalSlobValue() );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 1 ).setCellValue( " " );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( fgSlobTotalValueDetailDTO.getSlTypeTotalValue() ).isPresent()) {
                row1.createCell( 2 ).setCellValue( fgSlobTotalValueDetailDTO.getSlTypeTotalValue() );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 2 ).setCellValue( " " );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( fgSlobTotalValueDetailDTO.getObTypeTotalValue() ).isPresent()) {
                row1.createCell( 3 ).setCellValue( fgSlobTotalValueDetailDTO.getObTypeTotalValue() );
                row1.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 3 ).setCellValue( " " );
                row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
            }
        }
        int rowIdx1 = 4;
        if (Optional.ofNullable( rscIotFGSlobDTOList ).isPresent()) {
            for (RscIotFGSlobDTO rscIotFGSlobDTO : rscIotFGSlobDTOList) {
                if (Optional.ofNullable( rscIotFGSlobDTO ).isPresent()) {
                    Row row = sheet.createRow( rowIdx1++ );
                    if (Optional.ofNullable( rscIotFGSlobDTO.getItemCode() ).isPresent()) {
                        String materialCodeInStringFormat = rscIotFGSlobDTO.getItemCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 3 ) );
                        } catch (NumberFormatException ex) {
                            row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                        }
                    } else {
                        row.createCell( 0 ).setCellValue( " " );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getItemDescription() ).isPresent()) {
                        row.createCell( 1 ).setCellValue( rscIotFGSlobDTO.getItemDescription() );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 1 ).setCellValue( " " );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getStockQuantity() ).isPresent()) {
                        row.createCell( 2 ).setCellValue( rscIotFGSlobDTO.getStockQuantity() );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 2 ).setCellValue( " " );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getMapPrice() ).isPresent()) {
                        row.createCell( 3 ).setCellValue( rscIotFGSlobDTO.getMapPrice() );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                    } else {
                        row.createCell( 3 ).setCellValue( " " );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getStockValue() ).isPresent()) {
                        row.createCell( 4 ).setCellValue( rscIotFGSlobDTO.getStockValue() );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 4 ).setCellValue( " " );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getTotalPlanQuantity() ).isPresent()) {
                        row.createCell( 5 ).setCellValue( rscIotFGSlobDTO.getTotalPlanQuantity() );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 5 ).setCellValue( " " );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getSlobQuantity() ).isPresent()) {
                        row.createCell( 6 ).setCellValue( rscIotFGSlobDTO.getSlobQuantity() );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 6 ).setCellValue( " " );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getSlobValue() ).isPresent()) {
                        row.createCell( 7 ).setCellValue( rscIotFGSlobDTO.getSlobValue() );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 7 ).setCellValue( " " );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( rscIotFGSlobDTO.getSlobType() ).isPresent()) {
                        row.createCell( 8 ).setCellValue( rscIotFGSlobDTO.getSlobType().toString() );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row.createCell( 8 ).setCellValue( " " );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
