package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.slob.FGSlobDeviationMainDTO;
import com.example.validation.dto.slob.FGSlobSummaryDTO;
import com.example.validation.dto.slob.TypeWiseFGSlobDeviationDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FgSlobSummaryExporter {
    public static ByteArrayInputStream fgSlobDetailsToExcelList(FGSlobDeviationMainDTO fgSlobDeviationMainDTO, List<TypeWiseFGSlobDeviationDTO> typeWiseFGSlobDeviationDTO, FGSlobSummaryDTO fgSlobSummaryDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook(); ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            XSSFSheet sheet = workbook.createSheet(ExcelFileNameConstants.FG_SLOB_SUMMARY + mpsDate.toString());
            List<CellStyle> allCellStyles = getAllCellStyles(workbook);
            printHeaderOverAllDeviation(fgSlobSummaryDTO, sheet, allCellStyles.get(1));
            printHeaderRowDivisionwiseDeviation(sheet, allCellStyles.get(1));
            printHeaderSummaryDetails(sheet, allCellStyles.get(1));
            printDataRows(fgSlobDeviationMainDTO, typeWiseFGSlobDeviationDTO, fgSlobSummaryDTO, sheet, allCellStyles);
            for (int columnIndex = 0; columnIndex < getHeaderOverAllDeviation(fgSlobSummaryDTO).length; columnIndex++) {
                sheet.autoSizeColumn(columnIndex);
            }
            for (int columnIndex = 0; columnIndex < getHeaderDivisionwiseDeviation().length; columnIndex++) {
                sheet.autoSizeColumn(columnIndex);
            }
            for (int columnIndex = 0; columnIndex < getHeaderSummaryDetails().length; columnIndex++) {
                sheet.autoSizeColumn(columnIndex);
            }
            workbook.write(out);
            return new ByteArrayInputStream(out.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("fail to import data to Excel file: " + e.getMessage());
        }
    }

    private static String[] getHeaderOverAllDeviation(FGSlobSummaryDTO fgSlobSummaryDTO) {
        if (Optional.ofNullable(fgSlobSummaryDTO).isPresent()) {
            String currentMonthName, previousMonthName;
            if (Optional.ofNullable(fgSlobSummaryDTO.getCurrentMonthValue()).isPresent()) {
                currentMonthName = fgSlobSummaryDTO.getCurrentMonthValue();
            } else {
                currentMonthName = "";
            }
            if (Optional.ofNullable(fgSlobSummaryDTO.getPreviousMonthValue()).isPresent()) {
                previousMonthName = fgSlobSummaryDTO.getPreviousMonthValue();
            } else {
                previousMonthName = "";
            }

            return new String[]{"", currentMonthName, previousMonthName, "Deviation Total", "Deviation Status"};
        } else return new String[]{"", "", "", "Deviation Total", "Deviation Status"};
    }

    private static String[] getHeaderDivisionwiseDeviation() {
        return new String[]{"Material Type ", "Division", "Current Slob", "Previous Slob", "Deviation", "Deviation Status"};
    }

    private static String[] getHeaderSummaryDetails() {
        return new String[]{"Material Type ", "Division", "OB Value", "SM Value", "Current Slob", "Total Stock Value", "IQ"};
    }

    private static void printHeaderOverAllDeviation(FGSlobSummaryDTO fgSlobSummaryDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow(1);
        String[] header = getHeaderOverAllDeviation(fgSlobSummaryDTO);
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(header[col]);
            cell.setCellStyle(headerCellStyle);
        }
    }

    private static void printHeaderRowDivisionwiseDeviation(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow(11);
        String[] header = getHeaderDivisionwiseDeviation();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(header[col]);
            cell.setCellStyle(headerCellStyle);
        }
    }

    private static void printHeaderSummaryDetails(Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow(23);
        String[] header = getHeaderSummaryDetails();
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell(col);
            cell.setCellValue(header[col]);
            cell.setCellStyle(headerCellStyle);
        }
    }

    private static void printDataRows(FGSlobDeviationMainDTO fgSlobDeviationMainDTO, List<TypeWiseFGSlobDeviationDTO> typeWiseFGSlobDeviationDTO, FGSlobSummaryDTO fgSlobSummaryDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable(fgSlobDeviationMainDTO).isPresent()) {
            if ((Optional.ofNullable(fgSlobDeviationMainDTO.getObDeviationDTO()).isPresent()) && (Optional.ofNullable(fgSlobDeviationMainDTO.getSlDeviationDTO()).isPresent()) && (Optional.ofNullable(fgSlobDeviationMainDTO.getSlobDeviationDTO()).isPresent()) && (Optional.ofNullable(fgSlobDeviationMainDTO.getStockDeviationDTO()).isPresent()) && (Optional.ofNullable(fgSlobDeviationMainDTO.getSlPercentageDeviationDTO()).isPresent()) && (Optional.ofNullable(fgSlobDeviationMainDTO.getObPercentageDeviationDTO()).isPresent()) && (Optional.ofNullable(fgSlobDeviationMainDTO.getStockPercentageDeviationDTO()).isPresent())) {
                int rowIdx1 = 2;
                int rowIdx = 0;
                Row row0 = sheet.createRow(rowIdx);
                Row row1 = sheet.createRow(rowIdx1++);
                Row row2 = sheet.createRow(rowIdx1++);
                Row row3 = sheet.createRow(rowIdx1++);
                Row row4 = sheet.createRow(rowIdx1++);
                Row row5 = sheet.createRow(rowIdx1++);
                Row row6 = sheet.createRow(rowIdx1++);
                Row row7 = sheet.createRow(rowIdx1);
                row0.createCell(0).setCellValue("OverAll Deviation");
                row0.getCell(0).setCellStyle(allCellStyles.get(9));
                row1.createCell(0).setCellValue("OB Value");
                row1.getCell(0).setCellStyle(allCellStyles.get(2));
                row2.createCell(0).setCellValue("SM Value");
                row2.getCell(0).setCellStyle(allCellStyles.get(2));
                row3.createCell(0).setCellValue("Current SLOB");
                row3.getCell(0).setCellStyle(allCellStyles.get(2));
                row4.createCell(0).setCellValue("Total Stock Value");
                row4.getCell(0).setCellStyle(allCellStyles.get(2));
                row5.createCell(0).setCellValue("OB %");
                row5.getCell(0).setCellStyle(allCellStyles.get(2));
                row6.createCell(0).setCellValue("SM %");
                row6.getCell(0).setCellStyle(allCellStyles.get(2));
                row7.createCell(0).setCellValue("Stock Quality %");
                row7.getCell(0).setCellStyle(allCellStyles.get(2));

                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row1.createCell(1).setCellValue(fgSlobDeviationMainDTO.getObDeviationDTO().getCurrentMonthSlobValue().longValue());
                    row1.getCell(1).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(1).setCellValue(" ");
                    row1.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row2.createCell(1).setCellValue(fgSlobDeviationMainDTO.getSlDeviationDTO().getCurrentMonthSlobValue().longValue());
                    row2.getCell(1).setCellStyle(allCellStyles.get(4));
                } else {
                    row2.createCell(1).setCellValue(" ");
                    row2.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlobDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row3.createCell(1).setCellValue(fgSlobDeviationMainDTO.getSlobDeviationDTO().getCurrentMonthSlobValue().longValue());
                    row3.getCell(1).setCellStyle(allCellStyles.get(4));
                } else {
                    row3.createCell(1).setCellValue(" ");
                    row3.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row4.createCell(1).setCellValue(fgSlobDeviationMainDTO.getStockDeviationDTO().getCurrentMonthSlobValue().longValue());
                    row4.getCell(1).setCellStyle(allCellStyles.get(4));
                } else {
                    row4.createCell(1).setCellValue(" ");
                    row4.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row5.createCell(1).setCellValue((fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getCurrentMonthSlobValue()));
                    row5.getCell(1).setCellStyle(allCellStyles.get(7));
                } else {
                    row5.createCell(1).setCellValue(" ");
                    row5.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row6.createCell(1).setCellValue((fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getCurrentMonthSlobValue()));
                    row6.getCell(1).setCellStyle(allCellStyles.get(7));
                } else {
                    row6.createCell(1).setCellValue(" ");
                    row6.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row7.createCell(1).setCellValue((fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getCurrentMonthSlobValue()));
                    row7.getCell(1).setCellStyle(allCellStyles.get(7));
                } else {
                    row7.createCell(1).setCellValue(" ");
                    row7.getCell(1).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row1.createCell(2).setCellValue(fgSlobDeviationMainDTO.getObDeviationDTO().getPreviousMonthSlobValue().longValue());
                    row1.getCell(2).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(2).setCellValue(" ");
                    row1.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row2.createCell(2).setCellValue(fgSlobDeviationMainDTO.getSlDeviationDTO().getPreviousMonthSlobValue().longValue());
                    row2.getCell(2).setCellStyle(allCellStyles.get(4));
                } else {
                    row2.createCell(2).setCellValue(" ");
                    row2.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlobDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row3.createCell(2).setCellValue(fgSlobDeviationMainDTO.getSlobDeviationDTO().getPreviousMonthSlobValue().longValue());
                    row3.getCell(2).setCellStyle(allCellStyles.get(4));
                } else {
                    row3.createCell(2).setCellValue(" ");
                    row3.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row4.createCell(2).setCellValue(fgSlobDeviationMainDTO.getStockDeviationDTO().getPreviousMonthSlobValue().longValue());
                    row4.getCell(2).setCellStyle(allCellStyles.get(4));
                } else {
                    row4.createCell(2).setCellValue(" ");
                    row4.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row5.createCell(2).setCellValue((fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getPreviousMonthSlobValue()));
                    row5.getCell(2).setCellStyle(allCellStyles.get(7));
                } else {
                    row5.createCell(2).setCellValue(" ");
                    row5.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row6.createCell(2).setCellValue((fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getPreviousMonthSlobValue()));
                    row6.getCell(2).setCellStyle(allCellStyles.get(7));
                } else {
                    row6.createCell(2).setCellValue(" ");
                    row6.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row7.createCell(2).setCellValue((fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getPreviousMonthSlobValue()));
                    row7.getCell(2).setCellStyle(allCellStyles.get(7));
                } else {
                    row7.createCell(2).setCellValue(" ");
                    row7.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObDeviationDTO().getDeviationValue()).isPresent()) {
                    row1.createCell(3).setCellValue(fgSlobDeviationMainDTO.getObDeviationDTO().getDeviationValue().longValue());
                    row1.getCell(3).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(3).setCellValue(" ");
                    row1.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlDeviationDTO().getDeviationValue()).isPresent()) {
                    row2.createCell(3).setCellValue(fgSlobDeviationMainDTO.getSlDeviationDTO().getDeviationValue().longValue());
                    row2.getCell(3).setCellStyle(allCellStyles.get(4));
                } else {
                    row2.createCell(3).setCellValue(" ");
                    row2.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlobDeviationDTO().getDeviationValue()).isPresent()) {
                    row3.createCell(3).setCellValue(fgSlobDeviationMainDTO.getSlobDeviationDTO().getDeviationValue().longValue());
                    row3.getCell(3).setCellStyle(allCellStyles.get(4));
                } else {
                    row3.createCell(3).setCellValue(" ");
                    row3.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockDeviationDTO().getDeviationValue()).isPresent()) {
                    row4.createCell(3).setCellValue(fgSlobDeviationMainDTO.getStockDeviationDTO().getDeviationValue().longValue());
                    row4.getCell(3).setCellStyle(allCellStyles.get(4));
                } else {
                    row4.createCell(3).setCellValue(" ");
                    row4.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getDeviationValue()).isPresent()) {
                    row5.createCell(3).setCellValue((fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getDeviationValue()));
                    row5.getCell(3).setCellStyle(allCellStyles.get(7));
                } else {
                    row5.createCell(3).setCellValue(" ");
                    row5.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getDeviationValue()).isPresent()) {
                    row6.createCell(3).setCellValue((fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getDeviationValue()));
                    row6.getCell(3).setCellStyle(allCellStyles.get(7));
                } else {
                    row6.createCell(3).setCellValue(" ");
                    row6.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getDeviationValue()).isPresent()) {
                    row7.createCell(3).setCellValue((fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getDeviationValue()));
                    row7.getCell(3).setCellStyle(allCellStyles.get(7));
                    row7.getCell(3).setCellStyle(allCellStyles.get(7));
                } else {
                    row7.createCell(3).setCellValue(" ");
                    row7.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getObDeviationDTO().getDeviationStatus() == "NA")
                        row1.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getObDeviationDTO().getDeviationStatus() == "UP")
                        row1.createCell(4).setCellValue(10);
                    else row1.createCell(4).setCellValue(0);
                } else {
                    row1.createCell(4).setCellValue(" ");
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getSlDeviationDTO().getDeviationStatus() == "NA")
                        row2.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getSlDeviationDTO().getDeviationStatus() == "UP")
                        row2.createCell(4).setCellValue(10);
                    else row2.createCell(4).setCellValue(0);
                } else {
                    row2.createCell(4).setCellValue(" ");
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlobDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getSlobDeviationDTO().getDeviationStatus() == "NA")
                        row3.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getSlobDeviationDTO().getDeviationStatus() == "UP")
                        row3.createCell(4).setCellValue(10);
                    else row3.createCell(4).setCellValue(0);
                } else {
                    row3.createCell(4).setCellValue(" ");
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getStockDeviationDTO().getDeviationStatus() == "NA")
                        row4.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getStockDeviationDTO().getDeviationStatus() == "UP")
                        row4.createCell(4).setCellValue(10);
                    else row4.createCell(4).setCellValue(0);
                } else {
                    row4.createCell(4).setCellValue(" ");
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getDeviationStatus() == "NA")
                        row5.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getObPercentageDeviationDTO().getDeviationStatus() == "UP")
                        row5.createCell(4).setCellValue(10);
                    else row5.createCell(4).setCellValue(0);
                } else {
                    row5.createCell(4).setCellValue(" ");
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getDeviationStatus() == "NA")
                        row6.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getSlPercentageDeviationDTO().getDeviationStatus() == "UP")
                        row6.createCell(4).setCellValue(10);
                    else row6.createCell(4).setCellValue(0);
                } else {
                    row6.createCell(4).setCellValue(" ");
                }
                if (Optional.ofNullable(fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getDeviationStatus() == "NA")
                        row7.createCell(4).setCellValue("No change");
                    else if (fgSlobDeviationMainDTO.getStockPercentageDeviationDTO().getDeviationStatus() == "UP")
                        row7.createCell(4).setCellValue(10);
                    else row7.createCell(4).setCellValue(0);
                } else {
                    row7.createCell(4).setCellValue(" ");
                }
                row1.getCell(4).setCellStyle(allCellStyles.get(6));
                row2.getCell(4).setCellStyle(allCellStyles.get(6));
                row3.getCell(4).setCellStyle(allCellStyles.get(6));
                row4.getCell(4).setCellStyle(allCellStyles.get(6));
                row5.getCell(4).setCellStyle(allCellStyles.get(6));
                row6.getCell(4).setCellStyle(allCellStyles.get(6));
                row7.getCell(4).setCellStyle(allCellStyles.get(6));
                SheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
                CellRangeAddress[] regions = new CellRangeAddress[]{CellRangeAddress.valueOf("E3:E9")};
                ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule(IconMultiStateFormatting.IconSet.GYR_3_ARROW);
                rule3.getMultiStateFormatting().setIconOnly(true);
                IconMultiStateFormatting im3 = rule3.getMultiStateFormatting();
                im3.setIconOnly(true);
                im3.getThresholds()[0].setRangeType(ConditionalFormattingThreshold.RangeType.NUMBER);
                im3.getThresholds()[1].setValue(3d);
                im3.getThresholds()[1].setRangeType(ConditionalFormattingThreshold.RangeType.NUMBER);
                im3.getThresholds()[1].setValue(3d);
                im3.getThresholds()[2].setRangeType(ConditionalFormattingThreshold.RangeType.NUMBER);
                im3.getThresholds()[2].setValue(7d);
                sheetCF.addConditionalFormatting(regions, rule3);
            }
        }
        if (Optional.ofNullable(typeWiseFGSlobDeviationDTO).isPresent()) {
            int rowIdx2 = 12;
            int rowIdx = 10;
            Row row0 = sheet.createRow(rowIdx);
            row0.createCell(0).setCellValue("Division wise Deviation");
            row0.getCell(0).setCellStyle(allCellStyles.get(9));
            for (int l = 0; l < typeWiseFGSlobDeviationDTO.size(); l++) {
                Row row1 = sheet.createRow(rowIdx2++);
                if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getFgType()).isPresent()) {
                    row1.createCell(0).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getFgType());
                    row1.getCell(0).setCellStyle(allCellStyles.get(2));
                } else {
                    row1.createCell(0).setCellValue(" ");
                    row1.getCell(0).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                    row1.createCell(2).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getCurrentMonthSlobValue());
                    row1.getCell(2).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(2).setCellValue(" ");
                    row1.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                    row1.createCell(3).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getPreviousMonthSlobValue());
                    row1.getCell(3).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(3).setCellValue(" ");
                    row1.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getDeviationValue()).isPresent()) {
                    row1.createCell(4).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getDeviationValue());
                    row1.getCell(4).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(4).setCellValue(" ");
                    row1.getCell(4).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getDeviationStatus()).isPresent()) {
                    if (typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getDeviationStatus() == "NA") {
                        row1.createCell(5).setCellValue("No change");
                        row1.getCell(5).setCellStyle(allCellStyles.get(6));
                    } else if (typeWiseFGSlobDeviationDTO.get(l).getTypeDeviationDTO().getDeviationStatus() == "UP") {
                        row1.createCell(5).setCellValue(10);
                        row1.getCell(5).setCellStyle(allCellStyles.get(6));
                    } else {
                        row1.createCell(5).setCellValue(0);
                        row1.getCell(5).setCellStyle(allCellStyles.get(6));
                    }
                } else {
                    row1.createCell(5).setCellValue(" ");
                    row1.getCell(5).setCellStyle(allCellStyles.get(6));
                }
                for (int m = 0; m < typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().size(); m++) {
                    Row row2 = sheet.createRow(rowIdx2++);
                    row2.createCell(0).setCellValue(" ");
                    row2.getCell(0).setCellStyle(allCellStyles.get(0));
                    if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionName()).isPresent()) {
                        row2.createCell(1).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionName());
                        row2.getCell(1).setCellStyle(allCellStyles.get(2));
                    } else {
                        row2.createCell(1).setCellValue(" ");
                        row2.getCell(1).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getCurrentMonthSlobValue()).isPresent()) {
                        row2.createCell(2).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getCurrentMonthSlobValue());
                        row2.getCell(2).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(2).setCellValue(" ");
                        row2.getCell(2).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getPreviousMonthSlobValue()).isPresent()) {
                        row2.createCell(3).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getPreviousMonthSlobValue());
                        row2.getCell(3).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(3).setCellValue(" ");
                        row2.getCell(3).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getDeviationValue()).isPresent()) {
                        row2.createCell(4).setCellValue(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getDeviationValue());
                        row2.getCell(4).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(4).setCellValue(" ");
                        row2.getCell(4).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getDeviationStatus()).isPresent()) {
                        if (typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getDeviationStatus() == "NA") {
                            row2.createCell(5).setCellValue("No change");
                            row2.getCell(5).setCellStyle(allCellStyles.get(6));
                        } else if (typeWiseFGSlobDeviationDTO.get(l).getDivisionWiseFGSlobSummaryDTOs().get(m).getDivisionWiseDeviationDTO().getDeviationStatus() == "UP") {
                            row2.createCell(5).setCellValue(10);
                            row2.getCell(5).setCellStyle(allCellStyles.get(6));
                        } else {
                            row2.createCell(5).setCellValue(0);
                            row2.getCell(5).setCellStyle(allCellStyles.get(6));
                        }
                    } else {
                        row2.createCell(5).setCellValue(" ");
                        row2.getCell(5).setCellStyle(allCellStyles.get(6));
                    }
                }
            }
            SheetConditionalFormatting sheetCF1 = sheet.getSheetConditionalFormatting();
            CellRangeAddress[] regions1 = new CellRangeAddress[]{CellRangeAddress.valueOf("F13:F21")};
            ConditionalFormattingRule rule4 = sheetCF1.createConditionalFormattingRule(IconMultiStateFormatting.IconSet.GYR_3_ARROW);
            rule4.getMultiStateFormatting().setIconOnly(true);
            IconMultiStateFormatting im4 = rule4.getMultiStateFormatting();
            im4.setIconOnly(true);
            im4.getThresholds()[0].setRangeType(ConditionalFormattingThreshold.RangeType.NUMBER);
            im4.getThresholds()[1].setValue(3d);
            im4.getThresholds()[1].setRangeType(ConditionalFormattingThreshold.RangeType.NUMBER);
            im4.getThresholds()[1].setValue(3d);
            im4.getThresholds()[2].setRangeType(ConditionalFormattingThreshold.RangeType.NUMBER);
            im4.getThresholds()[2].setValue(7d);
            sheetCF1.addConditionalFormatting(regions1, rule4);
        }
        if (Optional.ofNullable(fgSlobSummaryDTO).isPresent()) {
            int rowIdx2 = 24;
            int rowIdx = 22;
            Row row0 = sheet.createRow(rowIdx);
            row0.createCell(0).setCellValue("Summary Details");
            row0.getCell(0).setCellStyle(allCellStyles.get(9));
            for (int j = 0; j < fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().size(); j++) {
                Row row1 = sheet.createRow(rowIdx2++);
                if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getFgType()).isPresent()) {
                    row1.createCell(0).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getFgType());
                    row1.getCell(0).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(0).setCellValue(" ");
                    row1.getCell(0).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeOBValue()).isPresent()) {
                    row1.createCell(2).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeOBValue());
                    row1.getCell(2).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(2).setCellValue(" ");
                    row1.getCell(2).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeSLValue()).isPresent()) {
                    row1.createCell(3).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeSLValue());
                    row1.getCell(3).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(3).setCellValue(" ");
                    row1.getCell(3).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeSlobValue()).isPresent()) {
                    row1.createCell(4).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeSlobValue());
                    row1.getCell(4).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(4).setCellValue(" ");
                    row1.getCell(4).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeStockValue()).isPresent()) {
                    row1.createCell(5).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getTotalTypeStockValue());
                    row1.getCell(5).setCellStyle(allCellStyles.get(4));
                } else {
                    row1.createCell(5).setCellValue(" ");
                    row1.getCell(5).setCellStyle(allCellStyles.get(0));
                }
                if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getMaterialIQ()).isPresent()) {
                    row1.createCell(6).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getMaterialIQ() / 100);
                    row1.getCell(6).setCellStyle(allCellStyles.get(8));
                } else {
                    row1.createCell(6).setCellValue(" ");
                    row1.getCell(6).setCellStyle(allCellStyles.get(0));
                }
                for (int k = 0; k < typeWiseFGSlobDeviationDTO.get(j).getDivisionWiseFGSlobSummaryDTOs().size(); k++) {
                    Row row2 = sheet.createRow(rowIdx2++);
                    row2.createCell(0).setCellValue(" ");
                    row2.getCell(0).setCellStyle(allCellStyles.get(0));
                    if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getDivisionName()).isPresent()) {
                        row2.createCell(1).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getDivisionName());
                        row2.getCell(1).setCellStyle(allCellStyles.get(2));
                    } else {
                        row2.createCell(1).setCellValue(" ");
                        row2.getCell(1).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getObValue()).isPresent()) {
                        row2.createCell(2).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getObValue());
                        row2.getCell(2).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(2).setCellValue(" ");
                        row2.getCell(2).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getSlValue()).isPresent()) {
                        row2.createCell(3).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getSlValue());
                        row2.getCell(3).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(3).setCellValue(" ");
                        row2.getCell(3).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getSlobValue()).isPresent()) {
                        row2.createCell(4).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getSlobValue());
                        row2.getCell(4).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(4).setCellValue(" ");
                        row2.getCell(4).setCellStyle(allCellStyles.get(0));
                    }
                    if (Optional.ofNullable(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getStockValue()).isPresent()) {
                        row2.createCell(5).setCellValue(fgSlobSummaryDTO.getTypeWiseFGSlobSummaryDTOS().get(j).getDivisionWiseFGSlobSummaryDTOs().get(k).getStockValue());
                        row2.getCell(5).setCellStyle(allCellStyles.get(4));
                    } else {
                        row2.createCell(5).setCellValue(" ");
                        row2.getCell(5).setCellStyle(allCellStyles.get(0));
                    }
                    row2.createCell(6).setCellValue(" ");
                    row2.getCell(6).setCellStyle(allCellStyles.get(0));
                }
            }
            Row row3 = sheet.createRow(rowIdx2);
            row3.createCell(0).setCellValue("Total");
            row3.getCell(0).setCellStyle(allCellStyles.get(0));
            row3.createCell(1).setCellValue(" ");
            row3.getCell(1).setCellStyle(allCellStyles.get(0));
            if (Optional.ofNullable(fgSlobSummaryDTO.getTotalObsoleteItemValue()).isPresent()) {
                row3.createCell(2).setCellValue(fgSlobSummaryDTO.getTotalObsoleteItemValue());
                row3.getCell(2).setCellStyle(allCellStyles.get(4));
            } else {
                row3.createCell(2).setCellValue(" ");
                row3.getCell(2).setCellStyle(allCellStyles.get(0));
            }
            if (Optional.ofNullable(fgSlobSummaryDTO.getTotalSlowItemValue()).isPresent()) {
                row3.createCell(3).setCellValue(fgSlobSummaryDTO.getTotalSlowItemValue());
                row3.getCell(3).setCellStyle(allCellStyles.get(4));
            } else {
                row3.createCell(3).setCellValue(" ");
                row3.getCell(3).setCellStyle(allCellStyles.get(0));
            }
            if (Optional.ofNullable(fgSlobSummaryDTO.getTotalSlobValue()).isPresent()) {
                row3.createCell(4).setCellValue(fgSlobSummaryDTO.getTotalSlobValue());
                row3.getCell(4).setCellStyle(allCellStyles.get(4));
            } else {
                row3.createCell(4).setCellValue(" ");
                row3.getCell(4).setCellStyle(allCellStyles.get(0));
            }
            if (Optional.ofNullable(fgSlobSummaryDTO.getTotalStockValue()).isPresent()) {
                row3.createCell(5).setCellValue(fgSlobSummaryDTO.getTotalStockValue());
                row3.getCell(5).setCellStyle(allCellStyles.get(4));
            } else {
                row3.createCell(5).setCellValue(" ");
                row3.getCell(5).setCellStyle(allCellStyles.get(0));
            }
            if (Optional.ofNullable(fgSlobSummaryDTO.getSlobIQ()).isPresent()) {
                row3.createCell(6).setCellValue(fgSlobSummaryDTO.getSlobIQ() / 100);
                row3.getCell(6).setCellStyle(allCellStyles.get(8));
            } else {
                row3.createCell(6).setCellValue(" ");
                row3.getCell(6).setCellStyle(allCellStyles.get(0));
            }
        }
    }

    private static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts(workbook);
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle(blankCellStyle);
        allCellStyleList.add(blankCellStyle);
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get(1);
        headerFont.setBold(true);
        headerFont.setColor(IndexedColors.WHITE.index);
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.index);
        setBorderStyle(headerCellStyle);
        headerCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        allCellStyleList.add(headerCellStyle);
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(stringDataCellStyle);
        stringDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
        allCellStyleList.add(stringDataCellStyle);
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(numberDataCellStyle);
        numberDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));
        allCellStyleList.add(numberDataCellStyle);
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(commaSeparatedNumberDataCellStyle);
        commaSeparatedNumberDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0"));
        allCellStyleList.add(commaSeparatedNumberDataCellStyle);
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(dateDataCellStyle);
        dateDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("m/d/yy"));
        allCellStyleList.add(dateDataCellStyle);
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(additionalDataCellStyle);
        additionalDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
        additionalDataCellStyle.setAlignment(HorizontalAlignment.CENTER);
        allCellStyleList.add(additionalDataCellStyle);
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(decimaNumberDataCellStyle);
        decimaNumberDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("#,##0.00"));
        allCellStyleList.add(decimaNumberDataCellStyle);
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont(allFonts.get(0));
        setBorderStyle(percentageDataCellStyle);
        percentageDataCellStyle.setDataFormat(workbook.createDataFormat().getFormat("0.00%"));
        allCellStyleList.add(percentageDataCellStyle);
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont(allFonts.get(2));
        setBorderStyle(stringDataCellStyle1);
        stringDataCellStyle1.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
        allCellStyleList.add(stringDataCellStyle1);
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName("Calibri");
        size10Font.setFontHeightInPoints((short) 10);
        allFontList.add(size10Font);
        Font size11Font = workbook.createFont();
        size11Font.setFontName("Calibri");
        size11Font.setFontHeightInPoints((short) 11);
        allFontList.add(size11Font);
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName("Calibri");
        sizeFont.setColor(IndexedColors.BLACK.index);
        sizeFont.setBold(true);
        sizeFont.setFontHeightInPoints((short) 11);
        allFontList.add(sizeFont);
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
    }
}
