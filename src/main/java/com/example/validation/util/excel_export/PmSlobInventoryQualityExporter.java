package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.SlobInventoryQualityIndexDetailsDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmSlobInventoryQualityExporter {
    public static ByteArrayInputStream pmSlobInventoryQualityIndexDetailsDTOToExcelList(SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_INVENTORY_QUALITY_INDEX + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printDataRows( slobInventoryQualityIndexDetailsDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < 12; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    public static void printDataRows(SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO ).isPresent()) {
            if ((Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getMpsDate() ).isPresent())) {
                int rowIdx1 = 2;
                int rowIdx2 = 0;
                Row rowHeader = sheet.createRow( rowIdx2 );
                Row row = sheet.createRow( rowIdx1++ );
                Row row1 = sheet.createRow( rowIdx1++ );
                Row row2 = sheet.createRow( rowIdx1++ );
                Row row3 = sheet.createRow( rowIdx1++ );
                Row row4 = sheet.createRow( rowIdx1++ );
                Row row5 = sheet.createRow( rowIdx1 );
                rowHeader.createCell( 0 ).setCellValue( "Inventory Quality Index" );
                rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                sheet.addMergedRegion( new CellRangeAddress( 0, 0, 0, 2 ) );
                row.createCell( 0 ).setCellValue( "Month" );
                row.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row1.createCell( 0 ).setCellValue( "SLOB [%]" );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row2.createCell( 0 ).setCellValue( "IQ[%]" );
                row2.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row3.createCell( 0 ).setCellValue( "Stock Value (Million)" );
                row3.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row4.createCell( 0 ).setCellValue( "SLOB Value (Million)" );
                row4.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
                row5.createCell( 0 ).setCellValue( "IQ Value (Million)" );
                row5.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );

                for (int cellValue = 0, cellNo = 1; cellValue < 12; cellValue++, cellNo++) {
                    if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).getMonthValue() ).isPresent()) {
                        row.createCell( cellNo ).setCellValue( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).getMonthValue() );
                        row.getCell( cellNo ).setCellStyle( allCellStyles.get( 1 ) );
                    } else {
                        row.createCell( cellNo ).setCellValue( " " );
                        row.getCell( cellNo ).setCellStyle( allCellStyles.get( 1 ) );
                    }
                    if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).slobPercentage ).isPresent()) {
                        row1.createCell( cellNo ).setCellValue( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).slobPercentage );
                        row1.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row1.createCell( cellNo ).setCellValue( " " );
                        row1.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).inventoryQualityPercentage ).isPresent()) {
                        row2.createCell( cellNo ).setCellValue( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).inventoryQualityPercentage );
                        row2.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( cellNo ).setCellValue( " " );
                        row2.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).stockValue ).isPresent()) {
                        row3.createCell( cellNo ).setCellValue( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).stockValue );
                        row3.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row3.createCell( cellNo ).setCellValue( " " );
                        row3.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).slobValue ).isPresent()) {
                        row4.createCell( cellNo ).setCellValue( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).slobValue );
                        row4.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row4.createCell( cellNo ).setCellValue( " " );
                        row4.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).inventoryQualityValue ).isPresent()) {
                        row5.createCell( cellNo ).setCellValue( slobInventoryQualityIndexDetailsDTO.getSlobInventoryQualityIndexDTOList().get( cellValue ).inventoryQualityValue );
                        row5.getCell( cellNo ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row5.createCell( cellNo ).setCellValue( " " );
                        row5.getCell( cellNo ).setCellStyle( allCellStyles.get( 0 ) );
                    }

                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );

        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}


