package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.SlobSummaryDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmSlobSummaryExporter {
    public static ByteArrayInputStream pmSlobDetailsToExcelList(SlobSummaryDTO slobSummaryDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_SLOB_SUMMARY + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow( slobSummaryDTO, sheet, allCellStyles.get( 1 ) );
            printDataRows( slobSummaryDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader( slobSummaryDTO ).length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static String[] getHeader(SlobSummaryDTO slobSummaryDTO) {
        if (Optional.ofNullable( slobSummaryDTO ).isPresent()) {
            String currentMonthName, previousMonthName;
            if (Optional.ofNullable( slobSummaryDTO.getCurrentMonthName() ).isPresent()) {
                currentMonthName = slobSummaryDTO.getCurrentMonthName();
            } else {
                currentMonthName = "";
            }
            if (Optional.ofNullable( slobSummaryDTO.getPreviousMonthName() ).isPresent()) {
                previousMonthName = slobSummaryDTO.getPreviousMonthName();
            } else {
                previousMonthName = "";
            }
            return new String[]{"", currentMonthName, previousMonthName, "Deviation Total", "Deviation Status"};
        } else {
            return new String[]{"", "", "", "Deviation Total", "Deviation Status"};
        }
    }

    private static void printHeaderRow(SlobSummaryDTO slobSummaryDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 1 );
        String[] header = getHeader( slobSummaryDTO );
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(SlobSummaryDTO slobSummaryDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( slobSummaryDTO ).isPresent()) {
            if ((Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails() ).isPresent())) {
                int rowIdx = 0;
                int rowIdx1 = 2;
                Row row0 = sheet.createRow( rowIdx );
                Row row1 = sheet.createRow( rowIdx1++ );
                Row row2 = sheet.createRow( rowIdx1++ );
                Row row3 = sheet.createRow( rowIdx1++ );
                Row row4 = sheet.createRow( rowIdx1++ );
                Row row5 = sheet.createRow( rowIdx1++ );
                Row row6 = sheet.createRow( rowIdx1++ );
                Row row7 = sheet.createRow( rowIdx1++ );
                Row row8 = sheet.createRow( rowIdx1 );
                row0.createCell( 0 ).setCellValue( "SLOB Summary" );
                row0.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row1.createCell( 0 ).setCellValue( "OB Value" );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row2.createCell( 0 ).setCellValue( "SM Value" );
                row2.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row3.createCell( 0 ).setCellValue( "Current SLOB" );
                row3.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row4.createCell( 0 ).setCellValue( "Total Stock Value" );
                row4.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row5.createCell( 0 ).setCellValue( "Stock Quality Value Details" );
                row5.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row6.createCell( 0 ).setCellValue( "OB %" );
                row6.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row7.createCell( 0 ).setCellValue( "SM %" );
                row7.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row8.createCell( 0 ).setCellValue( "Stock Quality %" );
                row8.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails().getCurrentMonthValue() ).isPresent()) {
                    row1.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalObsoleteItemValueDetails().getCurrentMonthValue() );
                    row1.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 1 ).setCellValue( " " );
                    row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails().getCurrentMonthValue() ).isPresent()) {
                    row2.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalSlowItemValueDetails().getCurrentMonthValue() );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 1 ).setCellValue( " " );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails().getCurrentMonthValue() ).isPresent()) {
                    row3.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalSlobValueDetails().getCurrentMonthValue() );
                    row3.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 1 ).setCellValue( " " );
                    row3.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails().getCurrentMonthValue() ).isPresent()) {
                    row4.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalStockValueDetails().getCurrentMonthValue() );
                    row4.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row4.createCell( 1 ).setCellValue( " " );
                    row4.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails().getCurrentMonthValue() ).isPresent()) {
                    row5.createCell( 1 ).setCellValue( slobSummaryDTO.getStockQualityValueDetails().getCurrentMonthValue() );
                    row5.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row5.createCell( 1 ).setCellValue( " " );
                    row5.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails().getCurrentMonthValue() ).isPresent()) {
                    row6.createCell( 1 ).setCellValue( (slobSummaryDTO.getObsoleteItemPercentageDetails().getCurrentMonthValue() ) );
                    row6.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row6.createCell( 1 ).setCellValue( " " );
                    row6.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails().getCurrentMonthValue() ).isPresent()) {
                    row7.createCell( 1 ).setCellValue( (slobSummaryDTO.getSlowItemPercentageDetails().getCurrentMonthValue() ) );
                    row7.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row7.createCell( 1 ).setCellValue( " " );
                    row7.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails().getCurrentMonthValue() ).isPresent()) {
                    row8.createCell( 1 ).setCellValue( (slobSummaryDTO.getStockQualityPercentageDetails().getCurrentMonthValue()) );
                    row8.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row8.createCell( 1 ).setCellValue( " " );
                    row8.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails().getPreviousMonthValue() ).isPresent()) {
                    row1.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalObsoleteItemValueDetails().getPreviousMonthValue() );
                    row1.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 2 ).setCellValue( " " );
                    row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails().getPreviousMonthValue() ).isPresent()) {
                    row2.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalSlowItemValueDetails().getPreviousMonthValue() );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 2 ).setCellValue( " " );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails().getPreviousMonthValue() ).isPresent()) {
                    row3.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalSlobValueDetails().getPreviousMonthValue() );
                    row3.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 2 ).setCellValue( " " );
                    row3.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails().getPreviousMonthValue() ).isPresent()) {
                    row4.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalStockValueDetails().getPreviousMonthValue() );
                    row4.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row4.createCell( 2 ).setCellValue( " " );
                    row4.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails().getPreviousMonthValue() ).isPresent()) {
                    row5.createCell( 2 ).setCellValue( slobSummaryDTO.getStockQualityValueDetails().getPreviousMonthValue() );
                    row5.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row5.createCell( 2 ).setCellValue( " " );
                    row5.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails().getPreviousMonthValue() ).isPresent()) {
                    row6.createCell( 2 ).setCellValue( (slobSummaryDTO.getObsoleteItemPercentageDetails().getPreviousMonthValue() ) );
                    row6.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row6.createCell( 2 ).setCellValue( " " );
                    row6.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails().getPreviousMonthValue() ).isPresent()) {
                    row7.createCell( 2 ).setCellValue( (slobSummaryDTO.getSlowItemPercentageDetails().getPreviousMonthValue() ) );
                    row7.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row7.createCell( 2 ).setCellValue( " " );
                    row7.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails().getPreviousMonthValue() ).isPresent()) {
                    row8.createCell( 2 ).setCellValue( (slobSummaryDTO.getStockQualityPercentageDetails().getPreviousMonthValue() ) );
                    row8.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row8.createCell( 2 ).setCellValue( " " );
                    row8.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationTotal() ).isPresent()) {
                    row1.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationTotal() );
                    row1.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 3 ).setCellValue( " " );
                    row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationTotal() ).isPresent()) {
                    row2.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationTotal() );
                    row2.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 3 ).setCellValue( " " );
                    row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails().getDeviationTotal() ).isPresent()) {
                    row3.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalSlobValueDetails().getDeviationTotal() );
                    row3.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 3 ).setCellValue( " " );
                    row3.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails().getDeviationTotal() ).isPresent()) {
                    row4.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalStockValueDetails().getDeviationTotal() );
                    row4.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row4.createCell( 3 ).setCellValue( " " );
                    row4.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails().getDeviationTotal() ).isPresent()) {
                    row5.createCell( 3 ).setCellValue( slobSummaryDTO.getStockQualityValueDetails().getDeviationTotal() );
                    row5.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row5.createCell( 3 ).setCellValue( " " );
                    row5.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationTotal() ).isPresent()) {
                    row6.createCell( 3 ).setCellValue( (slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationTotal() ) );
                    row6.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row6.createCell( 3 ).setCellValue( " " );
                    row6.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails().getDeviationTotal() ).isPresent()) {
                    row7.createCell( 3 ).setCellValue( (slobSummaryDTO.getSlowItemPercentageDetails().getDeviationTotal() ) );
                    row7.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row7.createCell( 3 ).setCellValue( " " );
                    row7.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails().getDeviationTotal() ).isPresent()) {
                    row8.createCell( 3 ).setCellValue( (slobSummaryDTO.getStockQualityPercentageDetails().getDeviationTotal()) );
                    row8.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row8.createCell( 3 ).setCellValue( " " );
                    row8.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationStatus() == "NA")
                        row1.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationStatus() == "UP")
                        row1.createCell( 4 ).setCellValue( 10 );
                    else
                        row1.createCell( 4 ).setCellValue( 0 );
                } else {
                    row1.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationStatus() == "NA")
                        row2.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationStatus() == "UP")
                        row2.createCell( 4 ).setCellValue( 10 );
                    else
                        row2.createCell( 4 ).setCellValue( 0 );
                } else {
                    row2.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getTotalSlobValueDetails().getDeviationStatus() == "NA")
                        row3.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalSlobValueDetails().getDeviationStatus() == "UP")
                        row3.createCell( 4 ).setCellValue( 10 );
                    else
                        row3.createCell( 4 ).setCellValue( 0 );
                } else {
                    row3.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getTotalStockValueDetails().getDeviationStatus() == "NA")
                        row4.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalStockValueDetails().getDeviationStatus() == "UP")
                        row4.createCell( 4 ).setCellValue( 10 );
                    else
                        row4.createCell( 4 ).setCellValue( 0 );
                } else {
                    row4.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getStockQualityValueDetails().getDeviationStatus() == "NA")
                        row5.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getStockQualityValueDetails().getDeviationStatus() == "UP")
                        row5.createCell( 4 ).setCellValue( 10 );
                    else
                        row5.createCell( 4 ).setCellValue( 0 );
                } else {
                    row5.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationStatus() == "NA")
                        row6.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationStatus() == "UP")
                        row6.createCell( 4 ).setCellValue( 10 );
                    else
                        row6.createCell( 4 ).setCellValue( 0 );
                } else {
                    row6.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getSlowItemPercentageDetails().getDeviationStatus() == "NA")
                        row7.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getSlowItemPercentageDetails().getDeviationStatus() == "UP")
                        row7.createCell( 4 ).setCellValue( 10 );
                    else
                        row7.createCell( 4 ).setCellValue( 0 );
                } else {
                    row7.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails().getDeviationStatus() ).isPresent()) {
                    if (slobSummaryDTO.getStockQualityPercentageDetails().getDeviationStatus() == "NA")
                        row8.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getStockQualityPercentageDetails().getDeviationStatus() == "UP")
                        row8.createCell( 4 ).setCellValue( 10 );
                    else
                        row8.createCell( 4 ).setCellValue( 0 );
                } else {
                    row8.createCell( 4 ).setCellValue( "" );
                }
                row1.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row2.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row3.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row4.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row5.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row6.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row7.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row8.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
            }
            SheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
            CellRangeAddress[] regions = new CellRangeAddress[]{CellRangeAddress.valueOf( "E3:E11" )};
            ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule( IconMultiStateFormatting.IconSet.GYR_3_ARROW );
            rule3.getMultiStateFormatting().setIconOnly( true );
            IconMultiStateFormatting im3 = rule3.getMultiStateFormatting();
            im3.setIconOnly( true );
            im3.getThresholds()[0].setRangeType( ConditionalFormattingThreshold.RangeType.NUMBER );
            im3.getThresholds()[1].setValue( 3d );
            im3.getThresholds()[1].setRangeType( ConditionalFormattingThreshold.RangeType.NUMBER );
            im3.getThresholds()[1].setValue( 3d );
            im3.getThresholds()[2].setRangeType( ConditionalFormattingThreshold.RangeType.NUMBER );
            im3.getThresholds()[2].setValue( 7d );
            sheetCF.addConditionalFormatting( regions, rule3 );
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( workbook.createDataFormat().getFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}