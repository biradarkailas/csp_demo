package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ItemDetailDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.MpsPlanMainDTO;
import com.example.validation.util.ExcelExportUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmMpsPlanExporter {
    public static ByteArrayInputStream mpsPlanToExcel(MpsPlanMainDTO mpsPlanMainDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_MPS_PLAN + mpsDate.toString() );
            sheet.createFreezePane( 1, 1 );
            List<String> headerNames = getHeader( monthDetailDTO );
            printHeaderRow( sheet, workbook, headerNames );
            printDataRows( mpsPlanMainDTO, sheet, workbook );
            for (int columnIndex = 0; columnIndex < headerNames.size(); columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static List<String> getHeader(MonthDetailDTO monthDetailDTO) {
        List<String> headerNames = new ArrayList<>();
        headerNames.add( "FG Code" );
        headerNames.add( "FG Description" );
        headerNames.add( "Division" );
        headerNames.add( "Signature" );
        headerNames.add( "Brand" );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth1().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth2().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth3().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth4().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth5().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth6().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth7().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth8().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth9().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth10().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth11().getMonthNameAlias() ) );
        headerNames.add( ExcelExportUtil.getMonthName( monthDetailDTO.getMonth12().getMonthNameAlias() ) );
        headerNames.add( "Total" );
        headerNames.add( "Material Category" );
        headerNames.add( "Material Sub Category" );
        headerNames.add( "Lines" );
        headerNames.add( "Skids" );
        return headerNames;
    }

    private static void printHeaderRow(Sheet sheet, Workbook workbook, List<String> headerNames) {
        Row headerRow = sheet.createRow( 0 );
        for (int col = 0; col < headerNames.size(); col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( headerNames.get( col ) );
            cell.setCellStyle( ExcelExportUtil.getHeaderCellStyle( workbook ) );
        }
    }

    private static void printDataRows(MpsPlanMainDTO mpsPlanMainDTO, Sheet sheet, Workbook workbook) {
        int rowIdx = 1;
        if (Optional.ofNullable( mpsPlanMainDTO.getBpiCodes() ).isPresent()) {
            for (ItemDetailDTO itemDetailDTO : mpsPlanMainDTO.getBpiCodes()) {
                if (Optional.ofNullable( itemDetailDTO ).isPresent()) {
                    Row row = sheet.createRow( rowIdx++ );
                    ExcelExportUtil.createStringToNumberValueCell( workbook, itemDetailDTO.getCode(), row, 0 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getDescription(), row, 1 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getDivisionName(), row, 2 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getSignatureName(), row, 3 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getBrandName(), row, 4 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth1().getValue(), row, 5 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth2().getValue(), row, 6 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth3().getValue(), row, 7 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth4().getValue(), row, 8 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth5().getValue(), row, 9 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth6().getValue(), row, 10 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth7().getValue(), row, 11 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth8().getValue(), row, 12 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth9().getValue(), row, 13 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth10().getValue(), row, 14 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth11().getValue(), row, 15 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getMonthValues().getMonth12().getValue(), row, 16 );
                    ExcelExportUtil.createNumberCell( workbook, itemDetailDTO.getTotal(), row, 17 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getMaterialCategoryName(), row, 18 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getMaterialSubCategoryName(), row, 19 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getLineName(), row, 20 );
                    ExcelExportUtil.createStringCell( workbook, itemDetailDTO.getSkidName(), row, 21 );
                }
            }
        }
    }
}