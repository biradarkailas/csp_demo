package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.SlobSummaryDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RmSlobSummaryExporter {
    public static ByteArrayInputStream rmSlobSummaryDetailsToExcel(SlobSummaryDTO slobSummaryDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.RM_SLOB_SUMMARY + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow( slobSummaryDTO, sheet, allCellStyles.get( 1 ) );
            printDataRows( slobSummaryDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader( slobSummaryDTO ).length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static String[] getHeader(SlobSummaryDTO slobSummaryDTO) {
        if (Optional.ofNullable( slobSummaryDTO ).isPresent()) {
            String currentMonthName, previousMonthName;
            if (Optional.ofNullable( slobSummaryDTO.getCurrentMonthName() ).isPresent()) {
                currentMonthName = slobSummaryDTO.getCurrentMonthName();
            } else {
                currentMonthName = "";
            }
            if (Optional.ofNullable( slobSummaryDTO.getPreviousMonthName() ).isPresent()) {
                previousMonthName = slobSummaryDTO.getPreviousMonthName();
            } else {
                previousMonthName = "";
            }
            return new String[]{"", currentMonthName, previousMonthName, "Deviation Total", "Deviation Status"};
        } else {
            return new String[]{"", "", "", "Deviation Total", "Deviation Status"};
        }
    }

    private static void printHeaderRow(SlobSummaryDTO slobSummaryDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 1 );
        String[] header = getHeader( slobSummaryDTO );
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(SlobSummaryDTO slobSummaryDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if (Optional.ofNullable( slobSummaryDTO ).isPresent()) {
            if ((Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getObsoleteExpiryValuePercentageDetails() ).isPresent())
                    && (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails() ).isPresent()) && (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteExpiryValueDetails() ).isPresent())) {
                int rowIdx = 0;
                int rowIdx1 = 2;
                Row row0 = sheet.createRow( rowIdx );
                Row row1 = sheet.createRow( rowIdx1++ );
                Row row2 = sheet.createRow( rowIdx1++ );
                Row row3 = sheet.createRow( rowIdx1++ );
                Row row4 = sheet.createRow( rowIdx1++ );
                Row row5 = sheet.createRow( rowIdx1++ );
                Row row6 = sheet.createRow( rowIdx1++ );
                Row row7 = sheet.createRow( rowIdx1++ );
                Row row8 = sheet.createRow( rowIdx1++ );
                Row row9 = sheet.createRow( rowIdx1++ );
                Row row10 = sheet.createRow( rowIdx1++ );
                Row row11 = sheet.createRow( rowIdx1++ );
                Row row12 = sheet.createRow( rowIdx1 );
                row0.createCell( 0 ).setCellValue( "SLOB Summary" );
                row0.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row1.createCell( 0 ).setCellValue( "OB Value" );
                row1.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row2.createCell( 0 ).setCellValue( "SM Value" );
                row2.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row3.createCell( 0 ).setCellValue( "Current SLOB" );
                row3.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row4.createCell( 0 ).setCellValue( "Total Stock Value" );
                row4.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row5.createCell( 0 ).setCellValue( "Stock Quality Value Details" );
                row5.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row6.createCell( 0 ).setCellValue( "OB %" );
                row6.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row7.createCell( 0 ).setCellValue( "SM %" );
                row7.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row8.createCell( 0 ).setCellValue( "Stock Quality %" );
                row8.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row9.createCell( 0 ).setCellValue( "OB Expiry Value " );
                row9.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row10.createCell( 0 ).setCellValue( "OB Non Expiry Value" );
                row10.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row11.createCell( 0 ).setCellValue( " OB Expiry %" );
                row11.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                row12.createCell( 0 ).setCellValue( "OB Non Expiry %" );
                row12.getCell( 0 ).setCellStyle( allCellStyles.get( 2 ) );
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails() ).isPresent()) {
                    row1.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalObsoleteItemValueDetails().getCurrentMonthValue() );
                    row1.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 1 ).setCellValue( " " );
                    row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent()) {
                    row2.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalSlowItemValueDetails().getCurrentMonthValue() );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 1 ).setCellValue( " " );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails() ).isPresent()) {
                    row3.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalSlobValueDetails().getCurrentMonthValue() );
                    row3.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 1 ).setCellValue( " " );
                    row3.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails() ).isPresent()) {
                    row4.createCell( 1 ).setCellValue( slobSummaryDTO.getTotalStockValueDetails().getCurrentMonthValue() );
                    row4.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row4.createCell( 1 ).setCellValue( " " );
                    row4.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails() ).isPresent()) {
                    row5.createCell( 1 ).setCellValue( slobSummaryDTO.getStockQualityValueDetails().getCurrentMonthValue() );
                    row5.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row5.createCell( 1 ).setCellValue( " " );
                    row5.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails() ).isPresent()) {
                    row6.createCell( 1 ).setCellValue( (slobSummaryDTO.getObsoleteItemPercentageDetails().getCurrentMonthValue() ) );
                    row6.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row6.createCell( 1 ).setCellValue( " " );
                    row6.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails() ).isPresent()) {
                    row7.createCell( 1 ).setCellValue( (slobSummaryDTO.getSlowItemPercentageDetails().getCurrentMonthValue()) );
                    row7.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row7.createCell( 1 ).setCellValue( " " );
                    row7.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails() ).isPresent()) {
                    row8.createCell( 1 ).setCellValue( (slobSummaryDTO.getStockQualityPercentageDetails().getCurrentMonthValue() ) );
                    row8.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row8.createCell( 1 ).setCellValue( " " );
                    row8.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteExpiryValueDetails() ).isPresent()) {
                    row9.createCell( 1 ).setCellValue( (slobSummaryDTO.getTotalObsoleteExpiryValueDetails().getCurrentMonthValue()) );
                    row9.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row9.createCell( 1 ).setCellValue( " " );
                    row9.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails() ).isPresent()) {
                    row10.createCell( 1 ).setCellValue( (slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails().getCurrentMonthValue()) );
                    row10.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row10.createCell( 1 ).setCellValue( " " );
                    row10.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteExpiryValuePercentageDetails() ).isPresent()) {
                    row11.createCell( 1 ).setCellValue( (slobSummaryDTO.getObsoleteExpiryValuePercentageDetails().getCurrentMonthValue() ) );
                    row11.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row11.createCell( 1 ).setCellValue( " " );
                    row11.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails() ).isPresent()) {
                    row12.createCell( 1 ).setCellValue( (slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails().getCurrentMonthValue()) );
                    row12.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row12.createCell( 1 ).setCellValue( " " );
                    row12.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }

                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails() ).isPresent()) {
                    row1.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalObsoleteItemValueDetails().getPreviousMonthValue() );
                    row1.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 2 ).setCellValue( " " );
                    row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent()) {
                    row2.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalSlowItemValueDetails().getPreviousMonthValue() );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 2 ).setCellValue( " " );
                    row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails() ).isPresent()) {
                    row3.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalSlobValueDetails().getPreviousMonthValue() );
                    row3.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 2 ).setCellValue( " " );
                    row3.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails() ).isPresent()) {
                    row4.createCell( 2 ).setCellValue( slobSummaryDTO.getTotalStockValueDetails().getPreviousMonthValue() );
                    row4.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row4.createCell( 2 ).setCellValue( " " );
                    row4.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails() ).isPresent()) {
                    row5.createCell( 2 ).setCellValue( slobSummaryDTO.getStockQualityValueDetails().getPreviousMonthValue() );
                    row5.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row5.createCell( 2 ).setCellValue( " " );
                    row5.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails() ).isPresent()) {
                    row6.createCell( 2 ).setCellValue( (slobSummaryDTO.getObsoleteItemPercentageDetails().getPreviousMonthValue() ) );
                    row6.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row6.createCell( 2 ).setCellValue( " " );
                    row6.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails() ).isPresent()) {
                    row7.createCell( 2 ).setCellValue( (slobSummaryDTO.getSlowItemPercentageDetails().getPreviousMonthValue() ) );
                    row7.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row7.createCell( 2 ).setCellValue( " " );
                    row7.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails() ).isPresent()) {
                    row8.createCell( 2 ).setCellValue( (slobSummaryDTO.getStockQualityPercentageDetails().getPreviousMonthValue() ) );
                    row8.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row8.createCell( 2 ).setCellValue( " " );
                    row8.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteExpiryValueDetails() ).isPresent()) {
                    row9.createCell( 2 ).setCellValue( (slobSummaryDTO.getTotalObsoleteExpiryValueDetails().getPreviousMonthValue()) );
                    row9.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row9.createCell( 2 ).setCellValue( " " );
                    row9.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails() ).isPresent()) {
                    row10.createCell( 2 ).setCellValue( (slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails().getPreviousMonthValue()) );
                    row10.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row10.createCell( 2 ).setCellValue( " " );
                    row10.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteExpiryValuePercentageDetails() ).isPresent()) {
                    row11.createCell( 2 ).setCellValue( (slobSummaryDTO.getObsoleteExpiryValuePercentageDetails().getPreviousMonthValue() ) );
                    row11.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row11.createCell( 2 ).setCellValue( " " );
                    row11.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails() ).isPresent()) {
                    row12.createCell( 2 ).setCellValue( (slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails().getPreviousMonthValue() ) );
                    row12.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row12.createCell( 2 ).setCellValue( " " );
                    row12.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                }

                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails() ).isPresent()) {
                    row1.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationTotal() );
                    row1.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row1.createCell( 3 ).setCellValue( " " );
                    row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent()) {
                    row2.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationTotal() );
                    row2.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row2.createCell( 3 ).setCellValue( " " );
                    row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlobValueDetails() ).isPresent()) {
                    row3.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalSlobValueDetails().getDeviationTotal() );
                    row3.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row3.createCell( 3 ).setCellValue( " " );
                    row3.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails() ).isPresent()) {
                    row4.createCell( 3 ).setCellValue( slobSummaryDTO.getTotalStockValueDetails().getDeviationTotal() );
                    row4.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row4.createCell( 3 ).setCellValue( " " );
                    row4.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails() ).isPresent()) {
                    row5.createCell( 3 ).setCellValue( slobSummaryDTO.getStockQualityValueDetails().getDeviationTotal() );
                    row5.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row5.createCell( 3 ).setCellValue( " " );
                    row5.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails() ).isPresent()) {
                    row6.createCell( 3 ).setCellValue( (slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationTotal()) );
                    row6.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row6.createCell( 3 ).setCellValue( " " );
                    row6.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails() ).isPresent()) {
                    row7.createCell( 3 ).setCellValue( (slobSummaryDTO.getSlowItemPercentageDetails().getDeviationTotal() ) );
                    row7.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row7.createCell( 3 ).setCellValue( " " );
                    row7.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails() ).isPresent()) {
                    row8.createCell( 3 ).setCellValue( (slobSummaryDTO.getStockQualityPercentageDetails().getDeviationTotal() ) );
                    row8.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row8.createCell( 3 ).setCellValue( " " );
                    row8.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteExpiryValueDetails() ).isPresent()) {
                    row9.createCell( 3 ).setCellValue( (slobSummaryDTO.getTotalObsoleteExpiryValueDetails().getDeviationTotal()) );
                    row9.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row9.createCell( 3 ).setCellValue( " " );
                    row9.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails() ).isPresent()) {
                    row10.createCell( 3 ).setCellValue( (slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails().getDeviationTotal()) );
                    row10.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                } else {
                    row10.createCell( 3 ).setCellValue( " " );
                    row10.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteExpiryValuePercentageDetails() ).isPresent()) {
                    row11.createCell( 3 ).setCellValue( (slobSummaryDTO.getObsoleteExpiryValuePercentageDetails().getDeviationTotal() ) );
                    row11.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row11.createCell( 3 ).setCellValue( " " );
                    row11.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails() ).isPresent()) {
                    row12.createCell( 3 ).setCellValue( (slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails().getDeviationTotal()) );
                    row12.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
                } else {
                    row12.createCell( 3 ).setCellValue( " " );
                    row12.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteItemValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationStatus() == "NA")
                        row1.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalObsoleteItemValueDetails().getDeviationStatus() == "UP")
                        row1.createCell( 4 ).setCellValue( 10 );
                    else
                        row1.createCell( 4 ).setCellValue( 0 );
                } else {
                    row1.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationStatus() == "NA")
                        row2.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalSlowItemValueDetails().getDeviationStatus() == "UP")
                        row2.createCell( 4 ).setCellValue( 10 );
                    else
                        row2.createCell( 4 ).setCellValue( 0 );
                } else {
                    row2.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalSlowItemValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getTotalSlobValueDetails().getDeviationStatus() == "NA")
                        row3.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalSlobValueDetails().getDeviationStatus() == "UP")
                        row3.createCell( 4 ).setCellValue( 10 );
                    else
                        row3.createCell( 4 ).setCellValue( 0 );
                } else {
                    row3.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalStockValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getTotalStockValueDetails().getDeviationStatus() == "NA")
                        row4.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalStockValueDetails().getDeviationStatus() == "UP")
                        row4.createCell( 4 ).setCellValue( 10 );
                    else
                        row4.createCell( 4 ).setCellValue( 0 );
                } else {
                    row4.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getStockQualityValueDetails().getDeviationStatus() == "NA")
                        row5.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getStockQualityValueDetails().getDeviationStatus() == "UP")
                        row5.createCell( 4 ).setCellValue( 10 );
                    else
                        row5.createCell( 4 ).setCellValue( 0 );
                } else {
                    row5.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteItemPercentageDetails() ).isPresent()) {
                    if (slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationStatus() == "NA")
                        row6.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getObsoleteItemPercentageDetails().getDeviationStatus() == "UP")
                        row6.createCell( 4 ).setCellValue( 10 );
                    else
                        row6.createCell( 4 ).setCellValue( 0 );
                } else {
                    row6.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getSlowItemPercentageDetails() ).isPresent()) {
                    if (slobSummaryDTO.getSlowItemPercentageDetails().getDeviationStatus() == "NA")
                        row7.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getSlowItemPercentageDetails().getDeviationStatus() == "UP")
                        row7.createCell( 4 ).setCellValue( 10 );
                    else
                        row7.createCell( 4 ).setCellValue( 0 );
                } else {
                    row7.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getStockQualityPercentageDetails() ).isPresent()) {
                    if (slobSummaryDTO.getStockQualityPercentageDetails().getDeviationStatus() == "NA")
                        row8.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getStockQualityPercentageDetails().getDeviationStatus() == "UP")
                        row8.createCell( 4 ).setCellValue( 10 );
                    else
                        row8.createCell( 4 ).setCellValue( 0 );
                } else {
                    row8.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteExpiryValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getTotalObsoleteExpiryValueDetails().getDeviationStatus() == "NA")
                        row9.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalObsoleteExpiryValueDetails().getDeviationStatus() == "UP")
                        row9.createCell( 4 ).setCellValue( 10 );
                    else
                        row9.createCell( 4 ).setCellValue( 0 );
                } else {
                    row9.createCell( 4 ).setCellValue( "" );
                }

                if (Optional.ofNullable( slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails() ).isPresent()) {
                    if (slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails().getDeviationStatus() == "NA")
                        row10.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getTotalObsoleteNonExpiryValueDetails().getDeviationStatus() == "UP")
                        row10.createCell( 4 ).setCellValue( 10 );
                    else
                        row10.createCell( 4 ).setCellValue( 0 );
                } else {
                    row10.createCell( 4 ).setCellValue( "" );
                }

                if (Optional.ofNullable( slobSummaryDTO.getObsoleteExpiryValuePercentageDetails() ).isPresent()) {
                    if (slobSummaryDTO.getObsoleteExpiryValuePercentageDetails().getDeviationStatus() == "NA")
                        row11.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getObsoleteExpiryValuePercentageDetails().getDeviationStatus() == "UP")
                        row11.createCell( 4 ).setCellValue( 10 );
                    else
                        row11.createCell( 4 ).setCellValue( 0 );
                } else {
                    row11.createCell( 4 ).setCellValue( "" );
                }
                if (Optional.ofNullable( slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails() ).isPresent()) {
                    if (slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails().getDeviationStatus() == "NA")
                        row12.createCell( 4 ).setCellValue( "No change" );
                    else if (slobSummaryDTO.getObsoleteNonExpiryValuePercentageDetails().getDeviationStatus() == "UP")
                        row12.createCell( 4 ).setCellValue( 10 );
                    else
                        row12.createCell( 4 ).setCellValue( 0 );
                } else {
                    row12.createCell( 4 ).setCellValue( "" );
                }
                row1.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row2.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row3.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row4.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row5.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row6.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row7.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row8.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row9.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row10.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row11.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
                row12.getCell( 4 ).setCellStyle( allCellStyles.get( 6 ) );
            }
            SheetConditionalFormatting sheetCF = sheet.getSheetConditionalFormatting();
            CellRangeAddress[] regions = new CellRangeAddress[]{CellRangeAddress.valueOf( "E3:E15" )};
            ConditionalFormattingRule rule3 = sheetCF.createConditionalFormattingRule( IconMultiStateFormatting.IconSet.GYR_3_ARROW );
            rule3.getMultiStateFormatting().setIconOnly( true );
            IconMultiStateFormatting im3 = rule3.getMultiStateFormatting();
            im3.setIconOnly( true );
            im3.getThresholds()[0].setRangeType( ConditionalFormattingThreshold.RangeType.NUMBER );
            im3.getThresholds()[1].setValue( 3d );
            im3.getThresholds()[1].setRangeType( ConditionalFormattingThreshold.RangeType.NUMBER );
            im3.getThresholds()[1].setValue( 3d );
            im3.getThresholds()[2].setRangeType( ConditionalFormattingThreshold.RangeType.NUMBER );
            im3.getThresholds()[2].setValue( 7d );
            sheetCF.addConditionalFormatting( regions, rule3 );
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        CellStyle commaSeparatedPercentageDataCellStyle = workbook.createCellStyle();
        commaSeparatedPercentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedPercentageDataCellStyle );
        commaSeparatedPercentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( commaSeparatedPercentageDataCellStyle );//15
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}
