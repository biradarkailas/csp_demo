package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.OtifCalculationDetailsDTO;
import com.example.validation.dto.PmOtifCalculationDetailsDTO;
import com.example.validation.dto.RscIotPmOtifCalculationDetailsDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class PmOtifSummaryExporter {
    public static ByteArrayInputStream pmOtifSummaryToExcel(OtifCalculationDetailsDTO otifCalculationDetailsDTO, MonthDetailDTO monthDetailDTO, LocalDate otifDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.PM_Otif_Summary + otifDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printAdditionalDataRow( sheet, allCellStyles );
            sheet.createFreezePane( 1, 1 );
            printHeaderRow( otifCalculationDetailsDTO, sheet, allCellStyles.get( 1 ) );
            printDataRows( otifCalculationDetailsDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < getHeader( otifCalculationDetailsDTO ).length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static void printAdditionalDataRow(Sheet sheet, List<CellStyle> allCellStyles) {
        Row additionalDataRow = sheet.createRow( 0 );
        additionalDataRow.createCell( 4 ).setCellValue( "Confirmed Schedule" );
        additionalDataRow.getCell( 4 ).setCellStyle( allCellStyles.get( 9 ) );
        sheet.addMergedRegion( new CellRangeAddress( 0, 0, 4, 5 ) );

    }

    private static String[] getHeader(OtifCalculationDetailsDTO otifCalculationDetailsDTO) {
        if (Optional.ofNullable( otifCalculationDetailsDTO ).isPresent()) {
            String CurrentMonth, PreviousMonth;
            if (Optional.ofNullable( otifCalculationDetailsDTO.getCurrentMonth() ).isPresent()) {
                CurrentMonth = otifCalculationDetailsDTO.getCurrentMonth();
            } else {
                CurrentMonth = "";
            }
            if (Optional.ofNullable( otifCalculationDetailsDTO.getPreviousMonth() ).isPresent()) {
                PreviousMonth = otifCalculationDetailsDTO.getPreviousMonth();
            } else {
                PreviousMonth = "";
            }
            return new String[]{"Supplier Code", "Supplier Name", "Item Code", "Item Description", PreviousMonth, CurrentMonth, "Total Order Qty", "Total Receipt Qty", "Balance", "Receipt %", "Score"};

        } else
            return new String[]{"Supplier Code", "Supplier Name", "Item Code", "Item Description", "", "", "Total Order Qty", "Total Receipt Qty", "Balance", "Receipt %", "Score"};
    }

    private static void printHeaderRow(OtifCalculationDetailsDTO otifCalculationDetailsDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 1 );
        String[] header = getHeader( otifCalculationDetailsDTO );
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(OtifCalculationDetailsDTO otifCalculationDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 2;
        Row row2 = sheet.createRow( rowIdx );
        if ((Optional.ofNullable( otifCalculationDetailsDTO ).isPresent()) && (Optional.ofNullable( otifCalculationDetailsDTO.getOtifCalculationDetailsDTO() ).isPresent())) {
            for (RscIotPmOtifCalculationDetailsDTO rscIotPmOtifCalculationDetailsDTO : otifCalculationDetailsDTO.getOtifCalculationDetailsDTO()) {
                row2 = sheet.createRow( rowIdx++ );
                if (Optional.ofNullable( rscIotPmOtifCalculationDetailsDTO.getSupplierCode() ).isPresent()) {
                    String materialCodeInStringFormat = rscIotPmOtifCalculationDetailsDTO.getSupplierCode();
                    try {
                        long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                        row2.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                        row2.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
                    } catch (NumberFormatException ex) {
                        row2.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                        row2.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                    }
                } else {
                    row2.createCell( 0 ).setCellValue( " " );
                    row2.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscIotPmOtifCalculationDetailsDTO.getSupplierName() ).isPresent()) {
                    row2.createCell( 1 ).setCellValue( rscIotPmOtifCalculationDetailsDTO.getSupplierName() );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 2 ) );
                } else {
                    row2.createCell( 1 ).setCellValue( " " );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                row2.createCell( 2 ).setCellValue( " " );
                row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                row2.createCell( 3 ).setCellValue( " " );
                row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                row2.createCell( 4 ).setCellValue( " " );
                row2.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                row2.createCell( 5 ).setCellValue( " " );
                row2.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                row2.createCell( 6 ).setCellValue( " " );
                row2.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                row2.createCell( 7 ).setCellValue( " " );
                row2.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                row2.createCell( 8 ).setCellValue( " " );
                row2.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                if (Optional.ofNullable( rscIotPmOtifCalculationDetailsDTO.getReceiptPercentage() ).isPresent()) {
                    row2.createCell( 9 ).setCellValue( rscIotPmOtifCalculationDetailsDTO.getReceiptPercentage() );
                    row2.getCell( 9 ).setCellStyle( allCellStyles.get( 9 ) );
                } else {
                    row2.createCell( 9 ).setCellValue( " " );
                    row2.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                if (Optional.ofNullable( rscIotPmOtifCalculationDetailsDTO.getScore() ).isPresent()) {
                    row2.createCell( 10 ).setCellValue( rscIotPmOtifCalculationDetailsDTO.getScore() );
                    row2.getCell( 10 ).setCellStyle( allCellStyles.get( 9 ) );
                } else {
                    row2.createCell( 10 ).setCellValue( " " );
                    row2.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                }
                for (PmOtifCalculationDetailsDTO pmOtifCalculationDetailsDTO : rscIotPmOtifCalculationDetailsDTO.getPmOtifCalculationDetailsDTO()) {
                    row2 = sheet.createRow( rowIdx++ );
                    row2.createCell( 0 ).setCellValue( " " );
                    row2.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    row2.createCell( 1 ).setCellValue( " " );
                    row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getItemCode() ).isPresent()) {
                        String materialCodeInStringFormat = pmOtifCalculationDetailsDTO.getItemCode();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row2.createCell( 2 ).setCellValue( materialCodeInNumberFormat );
                            row2.getCell( 2 ).setCellStyle( allCellStyles.get( 9 ) );
                        } catch (NumberFormatException ex) {
                            row2.createCell( 2 ).setCellValue( materialCodeInStringFormat );
                            row2.getCell( 2 ).setCellStyle( allCellStyles.get( 8 ) );
                        }
                    } else {
                        row2.createCell( 2 ).setCellValue( " " );
                        row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getItemDescription() ).isPresent()) {
                        row2.createCell( 3 ).setCellValue( pmOtifCalculationDetailsDTO.getItemDescription() );
                        row2.getCell( 3 ).setCellStyle( allCellStyles.get( 2 ) );
                    } else {
                        row2.createCell( 3 ).setCellValue( " " );
                        row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                    }

                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getPrevMonthOrderQty() ).isPresent()) {
                        row2.createCell( 4 ).setCellValue( pmOtifCalculationDetailsDTO.getPrevMonthOrderQty() );
                        row2.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 4 ).setCellValue( " " );
                        row2.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getCurrentMonthOrderQty() ).isPresent()) {
                        row2.createCell( 5 ).setCellValue( pmOtifCalculationDetailsDTO.getCurrentMonthOrderQty() );
                        row2.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 5 ).setCellValue( " " );
                        row2.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getTotalOrderQty() ).isPresent()) {
                        row2.createCell( 6 ).setCellValue( pmOtifCalculationDetailsDTO.getTotalOrderQty() );
                        row2.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 6 ).setCellValue( " " );
                        row2.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getTotalReceivedQty() ).isPresent()) {
                        row2.createCell( 7 ).setCellValue( pmOtifCalculationDetailsDTO.getTotalReceivedQty() );
                        row2.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 7 ).setCellValue( " " );
                        row2.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getBalanceQty() ).isPresent()) {
                        row2.createCell( 8 ).setCellValue( pmOtifCalculationDetailsDTO.getBalanceQty() );
                        row2.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 8 ).setCellValue( " " );
                        row2.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getReceiptPercentage() ).isPresent()) {
                        row2.createCell( 9 ).setCellValue( pmOtifCalculationDetailsDTO.getReceiptPercentage() );
                        row2.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 9 ).setCellValue( " " );
                        row2.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( pmOtifCalculationDetailsDTO.getScore() ).isPresent()) {
                        row2.createCell( 10 ).setCellValue( pmOtifCalculationDetailsDTO.getScore() );
                        row2.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row2.createCell( 10 ).setCellValue( " " );
                        row2.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                }
            }
        }
    }

    public static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );//0
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );//1
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );//2
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );//3
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );//4
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );//5
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );//6
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );//7
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );//8
        CellStyle numberDataCellStyle1 = workbook.createCellStyle();
        numberDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle1 );
        numberDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle1 );//9
        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        percentageDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( percentageDataCellStyle );//10
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );//11
        CellStyle numberDataCellStyle2 = workbook.createCellStyle();
        numberDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( numberDataCellStyle2 );
        numberDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle2 );//12
        CellStyle bottomHeaderCellStyle = workbook.createCellStyle();
        Font bottomHeader = allFonts.get( 4 );
        bottomHeader.setBold( true );
        bottomHeader.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyle.setFont( bottomHeader );
        bottomHeaderCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyle.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyle );
        bottomHeaderCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( bottomHeaderCellStyle );//13
        CellStyle bottomHeaderCellStyleGeneral = workbook.createCellStyle();
        Font bottomHeader1 = allFonts.get( 4 );
        bottomHeader1.setBold( true );
        bottomHeader1.setColor( IndexedColors.BLACK.index );
        bottomHeaderCellStyleGeneral.setFont( bottomHeader1 );
        bottomHeaderCellStyleGeneral.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        bottomHeaderCellStyleGeneral.setFillForegroundColor( IndexedColors.GREY_25_PERCENT.index );
        setBorderStyle( bottomHeaderCellStyleGeneral );
        bottomHeaderCellStyleGeneral.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( bottomHeaderCellStyleGeneral );//14
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );//0
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );//1
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );//2
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont1 );//3
        Font bottomHeadersizeFont = workbook.createFont();
        bottomHeadersizeFont.setFontName( "Calibri" );
        bottomHeadersizeFont.setColor( IndexedColors.BLACK.index );
        bottomHeadersizeFont.setBold( true );
        bottomHeadersizeFont.setFontHeightInPoints( (short) 12 );
        allFontList.add( bottomHeadersizeFont );//4
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}


