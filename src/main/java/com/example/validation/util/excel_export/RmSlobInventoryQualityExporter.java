package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.SlobInventoryQualityIndexDetailsDTO;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

public class RmSlobInventoryQualityExporter extends PmSlobInventoryQualityExporter {
    public static ByteArrayInputStream rmSlobInventoryQualityIndexDetailsDTOToExcelList(SlobInventoryQualityIndexDetailsDTO slobInventoryQualityIndexDetailsDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.RM_INVENTORY_QUALITY_INDEX + mpsDate.toString() );
            List<CellStyle> allCellStyles = PmSlobInventoryQualityExporter.getAllCellStyles( workbook );
            PmSlobInventoryQualityExporter.printDataRows( slobInventoryQualityIndexDetailsDTO, sheet, allCellStyles );
            for (int columnIndex = 0; columnIndex < 12; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }
}


