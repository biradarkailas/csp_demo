package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.FgSkidSaturationDTO;
import com.example.validation.dto.FgSkidSaturationDetailsDTO;
import com.example.validation.dto.MonthDetailDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class FgSkidSaturationExporter {
    public static ByteArrayInputStream skidSaturationToExcel(FgSkidSaturationDetailsDTO fgLineSaturationDetailsDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (Workbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            Sheet sheet = workbook.createSheet( ExcelFileNameConstants.FG_SKID_SATURATION + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printHeaderRow( monthDetailDTO, sheet, allCellStyles.get( 1 ) );
            printDataRows( fgLineSaturationDetailsDTO, sheet, allCellStyles );
            sheet.createFreezePane( 1, 1 );
            for (int columnIndex = 0; columnIndex < getHeader( monthDetailDTO ).length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }

    private static String[] getHeader(MonthDetailDTO monthDetailDTO) {
        String m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value;
        if (Optional.ofNullable( monthDetailDTO.getMonth1().getMonthNameAlias() ).isPresent()) {
            m1Value = monthDetailDTO.getMonth1().getMonthNameAlias();
        } else {
            m1Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth2().getMonthNameAlias() ).isPresent()) {
            m2Value = monthDetailDTO.getMonth2().getMonthNameAlias();
        } else {
            m2Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth3().getMonthNameAlias() ).isPresent()) {
            m3Value = monthDetailDTO.getMonth3().getMonthNameAlias();
        } else {
            m3Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth4().getMonthNameAlias() ).isPresent()) {
            m4Value = monthDetailDTO.getMonth4().getMonthNameAlias();
        } else {
            m4Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth5().getMonthNameAlias() ).isPresent()) {
            m5Value = monthDetailDTO.getMonth5().getMonthNameAlias();
        } else {
            m5Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth6().getMonthNameAlias() ).isPresent()) {
            m6Value = monthDetailDTO.getMonth6().getMonthNameAlias();
        } else {
            m6Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth7().getMonthNameAlias() ).isPresent()) {
            m7Value = monthDetailDTO.getMonth7().getMonthNameAlias();
        } else {
            m7Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth8().getMonthNameAlias() ).isPresent()) {
            m8Value = monthDetailDTO.getMonth8().getMonthNameAlias();
        } else {
            m8Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth9().getMonthNameAlias() ).isPresent()) {
            m9Value = monthDetailDTO.getMonth9().getMonthNameAlias();
        } else {
            m9Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth10().getMonthNameAlias() ).isPresent()) {
            m10Value = monthDetailDTO.getMonth10().getMonthNameAlias();
        } else {
            m10Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth11().getMonthNameAlias() ).isPresent()) {
            m11Value = monthDetailDTO.getMonth11().getMonthNameAlias();
        } else {
            m11Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth12().getMonthNameAlias() ).isPresent()) {
            m12Value = monthDetailDTO.getMonth12().getMonthNameAlias();
        } else {
            m12Value = "";
        }
        return new String[]{"Skids", m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value,};
    }

    private static void printHeaderRow(MonthDetailDTO monthDetailDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row headerRow = sheet.createRow( 0 );
        String[] header = getHeader( monthDetailDTO );
        for (int col = 0; col < header.length; col++) {
            Cell cell = headerRow.createCell( col );
            cell.setCellValue( header[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(FgSkidSaturationDetailsDTO fgLineSaturationDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        int rowIdx = 1;
        if (Optional.ofNullable( fgLineSaturationDetailsDTO.getFgSkidSaturationDTO() ).isPresent()) {
            for (FgSkidSaturationDTO fgSkidSaturationDTO : fgLineSaturationDetailsDTO.getFgSkidSaturationDTO()) {
                if (Optional.ofNullable( fgSkidSaturationDTO ).isPresent()) {
                    Row row = sheet.createRow( rowIdx++ );
                    if (Optional.ofNullable( fgSkidSaturationDTO.getSkidName() ).isPresent()) {
                        String format = "General";
                        String materialCodeInStringFormat = fgSkidSaturationDTO.getSkidName();
                        try {
                            long materialCodeInNumberFormat = Long.parseLong( materialCodeInStringFormat );
                            row.createCell( 0 ).setCellValue( materialCodeInNumberFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 3 ) );
                        } catch (NumberFormatException ex) {
                            row.createCell( 0 ).setCellValue( materialCodeInStringFormat );
                            row.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
                        }

                    } else {
                        row.createCell( 0 ).setCellValue( " " );
                        row.getCell( 0 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM1SkidSaturation() ).isPresent()) {
                        row.createCell( 1 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM1SkidSaturation() / 100 );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 1 ).setCellValue( " " );
                        row.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM2SkidSaturation() ).isPresent()) {
                        row.createCell( 2 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM2SkidSaturation() / 100 );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 2 ).setCellValue( " " );
                        row.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM3SkidSaturation() ).isPresent()) {
                        row.createCell( 3 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM3SkidSaturation() / 100 );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 3 ).setCellValue( " " );
                        row.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM4SkidSaturation() ).isPresent()) {
                        row.createCell( 4 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM4SkidSaturation() / 100 );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 4 ).setCellValue( " " );
                        row.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM5SkidSaturation() ).isPresent()) {
                        row.createCell( 5 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM5SkidSaturation() / 100 );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 5 ).setCellValue( " " );
                        row.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM6SkidSaturation() ).isPresent()) {
                        row.createCell( 6 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM6SkidSaturation() / 100 );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 6 ).setCellValue( " " );
                        row.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM7SkidSaturation() ).isPresent()) {
                        row.createCell( 7 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM7SkidSaturation() / 100 );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 7 ).setCellValue( " " );
                        row.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM8SkidSaturation() ).isPresent()) {
                        row.createCell( 8 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM8SkidSaturation() / 100 );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 8 ).setCellValue( " " );
                        row.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM9SkidSaturation() ).isPresent()) {
                        row.createCell( 9 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM9SkidSaturation() / 100 );
                        row.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 9 ).setCellValue( " " );
                        row.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM10SkidSaturation() ).isPresent()) {
                        row.createCell( 10 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM10SkidSaturation() / 100 );
                        row.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 10 ).setCellValue( " " );
                        row.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM11SkidSaturation() ).isPresent()) {
                        row.createCell( 11 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM11SkidSaturation() / 100 );
                        row.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 11 ).setCellValue( " " );
                        row.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
                    }
                    if (Optional.ofNullable( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM12SkidSaturation() ).isPresent()) {
                        row.createCell( 12 ).setCellValue( fgSkidSaturationDTO.getRscIotFgSkidSaturationDTO().getM12SkidSaturation() / 100 );
                        row.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
                    } else {
                        row.createCell( 12 ).setCellValue( " " );
                        row.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
                    }

                    for (int col = 1; col < 13; col++) {
                        Cell cell1 = row.getCell( col );
                        if (cell1.getNumericCellValue() > 0.69)
                            cell1.setCellStyle( allCellStyles.get( 7 ) );
                        ;
                    }
                }
            }
        }
    }

    private static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );

        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );

        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );

        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );

        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 2 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );

        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );

        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );

        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );

        CellStyle percentageDataCellStyle = workbook.createCellStyle();
        Font headerFont1 = allFonts.get( 1 );
        headerFont1.setColor( IndexedColors.WHITE.index );
        percentageDataCellStyle.setFont( headerFont );
        setBorderStyle( percentageDataCellStyle );
        percentageDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0%" ) );
        percentageDataCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        percentageDataCellStyle.setFillForegroundColor( IndexedColors.RED1.index );
        allCellStyleList.add( percentageDataCellStyle );

        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle1 );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();

        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );

        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );

        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 10 );
        allFontList.add( sizeFont );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}


