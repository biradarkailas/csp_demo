package com.example.validation.util.excel_export;

import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.ClassWiseCoverDaysDetailsDTO;
import com.example.validation.dto.CoverDaysSummaryDetailsDTO;
import com.example.validation.dto.MonthDetailDTO;
import com.example.validation.dto.SlobSummaryDetailsDTO;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class RmCoverDaysSummaryExporter {
    public static ByteArrayInputStream rmCoverDaysSummaryToExcel(SlobSummaryDetailsDTO slobSummaryDetailsDTO, CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO, ClassWiseCoverDaysDetailsDTO classWiseCoverDaysDetailsDTO, MonthDetailDTO monthDetailDTO, LocalDate mpsDate) {
        try (XSSFWorkbook workbook = new XSSFWorkbook();
             ByteArrayOutputStream out = new ByteArrayOutputStream();) {
            XSSFSheet sheet = workbook.createSheet( ExcelFileNameConstants.RM_SLOB_COVER_DAYS_SUMMARY + mpsDate.toString() );
            List<CellStyle> allCellStyles = getAllCellStyles( workbook );
            printclassBaseHeaderRow( monthDetailDTO, sheet, allCellStyles.get( 1 ) );
            printClassBaseStockHeaderRow( sheet, allCellStyles.get( 1 ) );
            printDataRows( classWiseCoverDaysDetailsDTO, slobSummaryDetailsDTO, coverDaysSummaryDetailsDTO, sheet, allCellStyles );
           /* for (int columnIndex = 0; columnIndex < getHeader(monthDetailDTO).length; columnIndex++) {
                sheet.autoSizeColumn(columnIndex);
            }*/
            for (int columnIndex = 0; columnIndex < getClassBaseHeaderHeader( monthDetailDTO ).length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            for (int columnIndex = 0; columnIndex < getHeaderBaseStockHeaderDetails().length; columnIndex++) {
                sheet.autoSizeColumn( columnIndex );
            }
            workbook.write( out );
            return new ByteArrayInputStream( out.toByteArray() );
        } catch (IOException e) {
            throw new RuntimeException( "fail to import data to Excel file: " + e.getMessage() );
        }
    }


    private static String[] getClassBaseHeaderHeader(MonthDetailDTO monthDetailDTO) {
        String m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value;
        if (Optional.ofNullable( monthDetailDTO.getMonth1().getMonthNameAlias() ).isPresent()) {
            m1Value = monthDetailDTO.getMonth1().getMonthNameAlias();
        } else {
            m1Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth2().getMonthNameAlias() ).isPresent()) {
            m2Value = monthDetailDTO.getMonth2().getMonthNameAlias();
        } else {
            m2Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth3().getMonthNameAlias() ).isPresent()) {
            m3Value = monthDetailDTO.getMonth3().getMonthNameAlias();
        } else {
            m3Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth4().getMonthNameAlias() ).isPresent()) {
            m4Value = monthDetailDTO.getMonth4().getMonthNameAlias();
        } else {
            m4Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth5().getMonthNameAlias() ).isPresent()) {
            m5Value = monthDetailDTO.getMonth5().getMonthNameAlias();
        } else {
            m5Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth6().getMonthNameAlias() ).isPresent()) {
            m6Value = monthDetailDTO.getMonth6().getMonthNameAlias();
        } else {
            m6Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth7().getMonthNameAlias() ).isPresent()) {
            m7Value = monthDetailDTO.getMonth7().getMonthNameAlias();
        } else {
            m7Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth8().getMonthNameAlias() ).isPresent()) {
            m8Value = monthDetailDTO.getMonth8().getMonthNameAlias();
        } else {
            m8Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth9().getMonthNameAlias() ).isPresent()) {
            m9Value = monthDetailDTO.getMonth9().getMonthNameAlias();
        } else {
            m9Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth10().getMonthNameAlias() ).isPresent()) {
            m10Value = monthDetailDTO.getMonth10().getMonthNameAlias();
        } else {
            m10Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth11().getMonthNameAlias() ).isPresent()) {
            m11Value = monthDetailDTO.getMonth11().getMonthNameAlias();
        } else {
            m11Value = "";
        }
        if (Optional.ofNullable( monthDetailDTO.getMonth12().getMonthNameAlias() ).isPresent()) {
            m12Value = monthDetailDTO.getMonth12().getMonthNameAlias();
        } else {
            m12Value = "";
        }
        return new String[]{"Class", m1Value, m2Value, m3Value, m4Value, m5Value, m6Value, m7Value, m8Value, m9Value, m10Value, m11Value, m12Value,};
    }

    private static String[] getHeaderBaseStockHeaderDetails() {
        return new String[]{"Class", "Stock", "CoverDay"};
    }


    private static void printclassBaseHeaderRow(MonthDetailDTO monthDetailDTO, Sheet sheet, CellStyle headerCellStyle) {
        Row classBaseHeaderRow = sheet.createRow( 5 );
        String[] classBaseheader = getClassBaseHeaderHeader( monthDetailDTO );
        for (int col = 0; col < classBaseheader.length; col++) {
            Cell cell = classBaseHeaderRow.createCell( col );
            cell.setCellValue( classBaseheader[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printClassBaseStockHeaderRow(Sheet sheet, CellStyle headerCellStyle) {
        Row classBaseStockHeaderRow = sheet.createRow( 12 );
        String[] classBaseStockHeader = getHeaderBaseStockHeaderDetails();
        for (int col = 0; col < classBaseStockHeader.length; col++) {
            Cell cell = classBaseStockHeaderRow.createCell( col );
            cell.setCellValue( classBaseStockHeader[col] );
            cell.setCellStyle( headerCellStyle );
        }
    }

    private static void printDataRows(ClassWiseCoverDaysDetailsDTO classWiseCoverDaysDetailsDTO, SlobSummaryDetailsDTO slobSummaryDetailsDTO, CoverDaysSummaryDetailsDTO coverDaysSummaryDetailsDTO, Sheet sheet, List<CellStyle> allCellStyles) {
        if ((Optional.ofNullable( coverDaysSummaryDetailsDTO ).isPresent()) && (Optional.ofNullable( slobSummaryDetailsDTO ).isPresent())) {
            int rowIdx0 = 0;
            int rowIdx2 = 1;
            int rowIdx3 = 2;
            Row rowHeader = sheet.createRow( rowIdx2 );
            Row rowValue = sheet.createRow( rowIdx3 );
            Row row0 = sheet.createRow( rowIdx0 );
            for (int i = 1; i <= 9; ++i) {
                Cell cell = rowValue.createCell( i );
                cell.setCellStyle( allCellStyles.get( 4 ) );
            }
            row0.createCell( 0 ).setCellValue( "OverAll CoverDays" );
            row0.getCell( 0 ).setCellStyle( allCellStyles.get( 9 ) );
            rowHeader.createCell( 0 ).setCellValue( "Opening Value in MNs" );
            rowHeader.getCell( 0 ).setCellStyle( allCellStyles.get( 1 ) );
            if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getTotalStockValue() ).isPresent()) {
                rowValue.createCell( 0 ).setCellValue( coverDaysSummaryDetailsDTO.getTotalStockValue() / 1000000 );
                rowValue.getCell( 0 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                rowValue.createCell( 0 ).setCellValue( " " );
                rowValue.getCell( 0 ).setCellStyle( allCellStyles.get( 4 ) );
            }
            rowHeader.createCell( 1 ).setCellValue( "CoverDays" );
            rowHeader.getCell( 1 ).setCellStyle( allCellStyles.get( 1 ) );
            if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getTotalCoverDays() ).isPresent()) {
                rowValue.createCell( 1 ).setCellValue( coverDaysSummaryDetailsDTO.getTotalCoverDays().longValue() );
                rowValue.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                rowValue.createCell( 1 ).setCellValue( "" );
                rowValue.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
            }
            rowHeader.createCell( 2 ).setCellValue( "SIT in MNs" );
            rowHeader.getCell( 2 ).setCellStyle( allCellStyles.get( 1 ) );
            if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getSitValue() ).isPresent()) {
                rowValue.createCell( 2 ).setCellValue( coverDaysSummaryDetailsDTO.getSitValue() / 1000000 );
                rowValue.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                rowValue.createCell( 2 ).setCellValue( " " );
                rowValue.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            }
            rowHeader.createCell( 3 ).setCellValue( "Opening Value With SIT in MNs" );
            rowHeader.getCell( 3 ).setCellStyle( allCellStyles.get( 1 ) );
            if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getTotalStockSitValue() ).isPresent()) {
                rowValue.createCell( 3 ).setCellValue( coverDaysSummaryDetailsDTO.getTotalStockSitValue() / 1000000 );
                rowValue.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                rowValue.createCell( 3 ).setCellValue( "" );
                rowValue.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
            }
            sheet.addMergedRegion( new CellRangeAddress( 1, 1, 3, 6 ) );
            sheet.addMergedRegion( new CellRangeAddress( 2, 2, 3, 6 ) );
            rowHeader.createCell( 7 ).setCellValue( "CoverDays with SIT" );
            rowHeader.getCell( 7 ).setCellStyle( allCellStyles.get( 1 ) );
            sheet.addMergedRegion( new CellRangeAddress( 1, 1, 7, 9 ) );
            sheet.addMergedRegion( new CellRangeAddress( 2, 2, 7, 9 ) );
            if (Optional.ofNullable( coverDaysSummaryDetailsDTO.getTotalCoverDaysWithSit() ).isPresent()) {
                rowValue.createCell( 7 ).setCellValue( coverDaysSummaryDetailsDTO.getTotalCoverDaysWithSit().longValue() );
                rowValue.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                rowValue.createCell( 8 ).setCellValue( "" );
                rowValue.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
            }
        }
        if ((Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO() ).isPresent()) && (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO() ).isPresent())
                && (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO() ).isPresent())) {
            int ClassBaseRowIdx = 6;
            int ClassBaseRowIdx1 = 4;
            Row row0 = sheet.createRow( ClassBaseRowIdx1 );
            Row row1 = sheet.createRow( ClassBaseRowIdx++ );
            Row row2 = sheet.createRow( ClassBaseRowIdx++ );
            Row row3 = sheet.createRow( ClassBaseRowIdx++ );
            Row row4 = sheet.createRow( ClassBaseRowIdx );
            row0.createCell( 0 ).setCellValue( "Class Based Consumption" );
            row0.getCell( 0 ).setCellStyle( allCellStyles.get( 10 ) );
            row1.createCell( 0 ).setCellValue( "A" );
            row1.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
            row2.createCell( 0 ).setCellValue( "B" );
            row2.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
            row3.createCell( 0 ).setCellValue( "C" );
            row3.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
            row4.createCell( 0 ).setCellValue( "Total Consumption" );
            row4.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() / 1000000 );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 1 ).setCellValue( " " );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() / 1000000 );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 2 ).setCellValue( " " );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 3 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() / 1000000 );
                row1.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 3 ).setCellValue( " " );
                row1.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 4 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() / 1000000 );
                row1.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 4 ).setCellValue( " " );
                row1.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 5 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() / 1000000 );
                row1.getCell( 5 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 5 ).setCellValue( " " );
                row1.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 6 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() / 1000000 );
                row1.getCell( 6 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 6 ).setCellValue( " " );
                row1.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 7 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() / 1000000 );
                row1.getCell( 7 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 7 ).setCellValue( " " );
                row1.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 8 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() / 1000000 );
                row1.getCell( 8 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 8 ).setCellValue( " " );
                row1.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 9 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() / 1000000 );
                row1.getCell( 9 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 9 ).setCellValue( " " );
                row1.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 10 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() / 1000000 );
                row1.getCell( 10 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 10 ).setCellValue( " " );
                row1.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 11 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() / 1000000 );
                row1.getCell( 11 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 11 ).setCellValue( " " );
                row1.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() ).isPresent()) {
                row1.createCell( 12 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() / 1000000 );
                row1.getCell( 12 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 12 ).setCellValue( " " );
                row1.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() / 1000000 );
                row2.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 1 ).setCellValue( " " );
                row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() / 1000000 );
                row2.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 2 ).setCellValue( " " );
                row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 3 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() / 1000000 );
                row2.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 3 ).setCellValue( " " );
                row2.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 4 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() / 1000000 );
                row2.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 4 ).setCellValue( " " );
                row2.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 5 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() / 1000000 );
                row2.getCell( 5 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 5 ).setCellValue( " " );
                row2.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 6 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() / 1000000 );
                row2.getCell( 6 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 6 ).setCellValue( " " );
                row2.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 7 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() / 1000000 );
                row2.getCell( 7 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 7 ).setCellValue( " " );
                row2.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 8 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() / 1000000 );
                row2.getCell( 8 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 8 ).setCellValue( " " );
                row2.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 9 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() / 1000000 );
                row2.getCell( 9 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 9 ).setCellValue( " " );
                row2.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 10 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() / 1000000 );
                row2.getCell( 10 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 10 ).setCellValue( " " );
                row2.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 11 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() / 1000000 );
                row2.getCell( 11 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 11 ).setCellValue( " " );
                row2.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() ).isPresent()) {
                row2.createCell( 12 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() / 1000000 );
                row2.getCell( 12 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 12 ).setCellValue( " " );
                row2.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() / 1000000 );
                row3.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 1 ).setCellValue( " " );
                row3.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() / 1000000 );
                row3.getCell( 2 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 2 ).setCellValue( " " );
                row3.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 3 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() / 1000000 );
                row3.getCell( 3 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 3 ).setCellValue( " " );
                row3.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 4 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() / 1000000 );
                row3.getCell( 4 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 4 ).setCellValue( " " );
                row3.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 5 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() / 1000000 );
                row3.getCell( 5 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 5 ).setCellValue( " " );
                row3.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 6 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() / 1000000 );
                row3.getCell( 6 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 6 ).setCellValue( " " );
                row3.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 7 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() / 1000000 );
                row3.getCell( 7 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 7 ).setCellValue( " " );
                row3.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 8 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() / 1000000 );
                row3.getCell( 8 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 8 ).setCellValue( " " );
                row3.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 9 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() / 1000000 );
                row3.getCell( 9 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 9 ).setCellValue( " " );
                row3.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 10 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() / 1000000 );
                row3.getCell( 10 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 10 ).setCellValue( " " );
                row3.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 11 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() / 1000000 );
                row3.getCell( 11 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 11 ).setCellValue( " " );
                row3.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() ).isPresent()) {
                row3.createCell( 12 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() / 1000000 );
                row3.getCell( 12 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 12 ).setCellValue( " " );
                row3.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth1ConsumptionTotalValue() / 1000000 );
                row4.getCell( 1 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 1 ).setCellValue( " " );
                row4.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth2ConsumptionTotalValue() / 1000000 );
                row4.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 2 ).setCellValue( " " );
                row4.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 3 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth3ConsumptionTotalValue() / 1000000 );
                row4.getCell( 3 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 3 ).setCellValue( " " );
                row4.getCell( 3 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 4 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth4ConsumptionTotalValue() / 1000000 );
                row4.getCell( 4 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 4 ).setCellValue( " " );
                row4.getCell( 4 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 5 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth5ConsumptionTotalValue() / 1000000 );
                row4.getCell( 5 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 5 ).setCellValue( " " );
                row4.getCell( 5 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 6 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth6ConsumptionTotalValue() / 1000000 );
                row4.getCell( 6 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 6 ).setCellValue( " " );
                row4.getCell( 6 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 7 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth7ConsumptionTotalValue() / 1000000 );
                row4.getCell( 7 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 7 ).setCellValue( " " );
                row4.getCell( 7 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 8 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth8ConsumptionTotalValue() / 1000000 );
                row4.getCell( 8 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 8 ).setCellValue( " " );
                row4.getCell( 8 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 9 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth9ConsumptionTotalValue() / 1000000 );
                row4.getCell( 9 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 9 ).setCellValue( " " );
                row4.getCell( 9 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 10 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth10ConsumptionTotalValue() / 1000000 );
                row4.getCell( 10 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 10 ).setCellValue( " " );
                row4.getCell( 10 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() ).isPresent()) {
                row4.createCell( 11 ).setCellValue( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth11ConsumptionTotalValue() / 1000000 );
                row4.getCell( 11 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 11 ).setCellValue( " " );
                row4.getCell( 11 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() ).isPresent()) {
                double Month12Consumption = classWiseCoverDaysDetailsDTO.getTotalCoverDaysConsumptionDTO().getMonth12ConsumptionTotalValue() / 1000000;
                row4.createCell( 12 ).setCellValue( Month12Consumption );
                row4.getCell( 12 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row4.createCell( 12 ).setCellValue( " " );
                row4.getCell( 12 ).setCellStyle( allCellStyles.get( 0 ) );
            }
        }
        int ClassBaseStockRowIdx = 13;
        int ClassBaseStockRowIdx1 = 11;
        Row row0 = sheet.createRow( ClassBaseStockRowIdx1 );
        Row row1 = sheet.createRow( ClassBaseStockRowIdx++ );
        Row row2 = sheet.createRow( ClassBaseStockRowIdx++ );
        Row row3 = sheet.createRow( ClassBaseStockRowIdx );
        row0.createCell( 0 ).setCellValue( "Class Based Stock & CoverDays" );
        row0.getCell( 0 ).setCellStyle( allCellStyles.get( 10 ) );
        row1.createCell( 0 ).setCellValue( "A" );
        row1.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
        row2.createCell( 0 ).setCellValue( "B" );
        row2.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );
        row3.createCell( 0 ).setCellValue( "C" );
        row3.getCell( 0 ).setCellStyle( allCellStyles.get( 8 ) );

        if ((Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO() ).isPresent()) && (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO() ).isPresent())
                && (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO() ).isPresent())) {
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getTotalStockValue() ).isPresent()) {
                row1.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getTotalStockValue() / 1000000 );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row1.createCell( 1 ).setCellValue( " " );
                row1.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getTotalCoverDays() ).isPresent()) {
                row1.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassACoverDaysConsumptionDTO().getTotalCoverDays() );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row1.createCell( 2 ).setCellValue( " " );
                row1.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getTotalStockValue() ).isPresent()) {
                row2.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getTotalStockValue() / 1000000 );
                row2.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row2.createCell( 1 ).setCellValue( " " );
                row2.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getTotalCoverDays() ).isPresent()) {
                row2.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassBCoverDaysConsumptionDTO().getTotalCoverDays() );
                row2.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row2.createCell( 2 ).setCellValue( " " );
                row2.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getTotalStockValue() ).isPresent()) {
                row3.createCell( 1 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getTotalStockValue() / 1000000 );
                row3.getCell( 1 ).setCellStyle( allCellStyles.get( 7 ) );
            } else {
                row3.createCell( 1 ).setCellValue( " " );
                row3.getCell( 1 ).setCellStyle( allCellStyles.get( 0 ) );
            }
            if (Optional.ofNullable( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getTotalCoverDays() ).isPresent()) {
                row3.createCell( 2 ).setCellValue( classWiseCoverDaysDetailsDTO.getClassCCoverDaysConsumptionDTO().getTotalCoverDays() );
                row3.getCell( 2 ).setCellStyle( allCellStyles.get( 4 ) );
            } else {
                row3.createCell( 2 ).setCellValue( " " );
                row3.getCell( 2 ).setCellStyle( allCellStyles.get( 0 ) );
            }
        }
    }

    private static List<CellStyle> getAllCellStyles(Workbook workbook) {
        List<CellStyle> allCellStyleList = new ArrayList<>();
        List<Font> allFonts = getAllFonts( workbook );
        CellStyle blankCellStyle = workbook.createCellStyle();
        setBorderStyle( blankCellStyle );
        allCellStyleList.add( blankCellStyle );
        CellStyle headerCellStyle = workbook.createCellStyle();
        Font headerFont = allFonts.get( 1 );
        headerFont.setBold( true );
        headerFont.setColor( IndexedColors.WHITE.index );
        headerCellStyle.setFont( headerFont );
        headerCellStyle.setFillPattern( FillPatternType.SOLID_FOREGROUND );
        headerCellStyle.setFillForegroundColor( IndexedColors.DARK_BLUE.index );
        setBorderStyle( headerCellStyle );
        headerCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        headerCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( headerCellStyle );
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( stringDataCellStyle );
        stringDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle );
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( numberDataCellStyle );
        numberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "0" ) );
        allCellStyleList.add( numberDataCellStyle );
        CellStyle commaSeparatedNumberDataCellStyle = workbook.createCellStyle();
        commaSeparatedNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( commaSeparatedNumberDataCellStyle );
        commaSeparatedNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0" ) );
        allCellStyleList.add( commaSeparatedNumberDataCellStyle );
        CellStyle dateDataCellStyle = workbook.createCellStyle();
        dateDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( dateDataCellStyle );
        dateDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "m/d/yy" ) );
        allCellStyleList.add( dateDataCellStyle );
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( additionalDataCellStyle );
        additionalDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        additionalDataCellStyle.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( additionalDataCellStyle );
        CellStyle decimaNumberDataCellStyle = workbook.createCellStyle();
        decimaNumberDataCellStyle.setFont( allFonts.get( 0 ) );
        setBorderStyle( decimaNumberDataCellStyle );
        decimaNumberDataCellStyle.setDataFormat( HSSFDataFormat.getBuiltinFormat( "#,##0.00" ) );
        allCellStyleList.add( decimaNumberDataCellStyle );
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont( allFonts.get( 2 ) );
        setBorderStyle( stringDataCellStyle1 );
        stringDataCellStyle1.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        stringDataCellStyle1.setAlignment( HorizontalAlignment.CENTER );
        allCellStyleList.add( stringDataCellStyle1 );
        CellStyle stringDataCellStyle2 = workbook.createCellStyle();
        stringDataCellStyle2.setFont( allFonts.get( 3 ) );
        setBorderStyle( stringDataCellStyle2 );
        stringDataCellStyle2.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle2 );
        CellStyle stringDataCellStyle3 = workbook.createCellStyle();
        stringDataCellStyle3.setFont( allFonts.get( 4 ) );
        stringDataCellStyle3.setDataFormat( HSSFDataFormat.getBuiltinFormat( "General" ) );
        allCellStyleList.add( stringDataCellStyle3 );
        return allCellStyleList;
    }

    private static List<Font> getAllFonts(Workbook workbook) {
        List<Font> allFontList = new ArrayList<>();
        Font size10Font = workbook.createFont();
        size10Font.setFontName( "Calibri" );
        size10Font.setFontHeightInPoints( (short) 10 );
        allFontList.add( size10Font );
        Font size11Font = workbook.createFont();
        size11Font.setFontName( "Calibri" );
        size11Font.setFontHeightInPoints( (short) 11 );
        allFontList.add( size11Font );
        Font sizeFont = workbook.createFont();
        sizeFont.setFontName( "Calibri" );
        sizeFont.setColor( IndexedColors.BLACK.index );
        sizeFont.setBold( true );
        sizeFont.setFontHeightInPoints( (short) 11 );
        allFontList.add( sizeFont );
        Font sizeFont1 = workbook.createFont();
        sizeFont1.setFontName( "Calibri" );
        sizeFont1.setColor( IndexedColors.BLACK.index );
        sizeFont1.setBold( true );
        sizeFont1.setFontHeightInPoints( (short) 11 );
        allFontList.add( sizeFont1 );
        Font sizeFont2 = workbook.createFont();
        sizeFont2.setFontName( "Calibri" );
        sizeFont2.setColor( IndexedColors.BLACK.index );
        sizeFont2.setFontHeightInPoints( (short) 11 );
        allFontList.add( sizeFont2 );
        return allFontList;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop( BorderStyle.THIN );
        cellStyle.setBorderRight( BorderStyle.THIN );
        cellStyle.setBorderBottom( BorderStyle.THIN );
        cellStyle.setBorderLeft( BorderStyle.THIN );
    }
}



