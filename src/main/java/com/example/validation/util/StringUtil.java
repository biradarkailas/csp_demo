package com.example.validation.util;

import com.example.validation.config.constants.ApplicationConstants;
import org.apache.commons.lang.StringUtils;

import java.util.*;

public class StringUtil {

    public static Set<String> lineSeparatedStringToList(String rsyncResponse) {
        int countOfRegex = StringUtils.countMatches(rsyncResponse, "\n");
        if (countOfRegex < 3) {
            return null;
        } else {
            String[] lines = rsyncResponse.split("\n");
            List<String> arrayList = new LinkedList<>(Arrays.asList(lines));
            arrayList.remove(0);
            arrayList.remove(arrayList.size() - 1);
            arrayList.remove(arrayList.size() - 1);
            arrayList.remove(arrayList.size() - 1);
            return new HashSet<>(arrayList);
        }
    }

    public static Boolean isEqual(String result) {
        return result.equals(ApplicationConstants.TRUE);
    }
}