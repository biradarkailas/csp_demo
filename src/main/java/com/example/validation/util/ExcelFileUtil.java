package com.example.validation.util;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.dto.StagingTableDetailsDTO;
import com.example.validation.util.helper.JdbcTemplateHelper;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author Kailas Biradar
 */
@Component
public class ExcelFileUtil {
    private final JdbcTemplateHelper jdbcTemplateHelper;

    public ExcelFileUtil(JdbcTemplateHelper jdbcTemplateHelper) {
        this.jdbcTemplateHelper = jdbcTemplateHelper;
    }

    public boolean loadCsvFile(StagingTableDetailsDTO stagingTableDetails) {
        if (stagingTableDetails.isHasHeader()) {
            return jdbcTemplateHelper.loadData(stagingTableDetails.getTableName(), getFilePath(stagingTableDetails.getCsvFileName()));
        } else {
            return jdbcTemplateHelper.loadDataWithoutHeader(stagingTableDetails.getTableName(), getFilePath(stagingTableDetails.getCsvFileName()));
        }
    }

    private String getFilePath(String fileName) {
        return getSourcePath() + ApplicationConstants.FORWARD_SLASH + fileName;
    }

    public String getSourcePath() {
        return ApplicationConstants.SOURCE_PATH;
    }

    public List<String> getCsvFiles(Set<String> files) {
        return files.stream()
                .filter(fileName -> fileName.endsWith(".csv") || fileName.endsWith(".CSV"))
                .collect(Collectors.toList());
    }

    public List<String> getExcelFiles(Set<String> files) {
        return files.stream()
                .filter(fileName -> fileName.endsWith(".xlsm") || fileName.endsWith(".xls") || fileName.endsWith(".xlsx"))
                .collect(Collectors.toList());
    }
}