package com.example.validation.util;

import com.example.validation.config.constants.ExcelConstants;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.ss.usermodel.*;

import java.util.Optional;

public class ExcelExportUtil {

    private static Font getFontProperty(Workbook workbook, String fontName, int fontSize) {
        Font font = workbook.createFont();
        font.setFontName(fontName);
        font.setFontHeightInPoints((short) fontSize);
        return font;
    }

    private static Font getFontBoldColorProperty(Workbook workbook, String fontName, short colorIndex, int fontSize) {
        Font font = workbook.createFont();
        font.setColor(colorIndex);
        font.setBold(true);
        font.setFontName(fontName);
        font.setFontHeightInPoints((short) fontSize);
        return font;
    }

    private static void setBorderStyle(CellStyle cellStyle) {
        cellStyle.setBorderTop(BorderStyle.THIN);
        cellStyle.setBorderRight(BorderStyle.THIN);
        cellStyle.setBorderBottom(BorderStyle.THIN);
        cellStyle.setBorderLeft(BorderStyle.THIN);
    }

    private static CellStyle getBlankCellStyle(Workbook workbook) {
        CellStyle blankCellStyle = workbook.createCellStyle();
        ExcelExportUtil.setBorderStyle(blankCellStyle);
        return blankCellStyle;
    }

    private static CellStyle getCellStyle(Workbook workbook, String formatter) {
        CellStyle stringDataCellStyle = workbook.createCellStyle();
        stringDataCellStyle.setFont(ExcelExportUtil.getFontProperty(workbook, ExcelConstants.CALIBRI, 10));
        ExcelExportUtil.setBorderStyle(stringDataCellStyle);
        stringDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(formatter));
        return stringDataCellStyle;
    }

    public static CellStyle getCenterAlignmentCellStyle(Workbook workbook, String formatter) {
        CellStyle additionalDataCellStyle = workbook.createCellStyle();
        additionalDataCellStyle.setFont(ExcelExportUtil.getFontProperty(workbook, ExcelConstants.CALIBRI, 10));
        ExcelExportUtil.setBorderStyle(additionalDataCellStyle);
        additionalDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat(formatter));
        additionalDataCellStyle.setAlignment(HorizontalAlignment.CENTER);
        return additionalDataCellStyle;
    }

    public static CellStyle getHeaderCellStyle(Workbook workbook) {
        CellStyle headerCellStyle = workbook.createCellStyle();
        headerCellStyle.setFont(ExcelExportUtil.getFontBoldColorProperty(workbook, ExcelConstants.CALIBRI, IndexedColors.WHITE.index, 11));
        headerCellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        headerCellStyle.setFillForegroundColor(IndexedColors.DARK_BLUE.index);
        ExcelExportUtil.setBorderStyle(headerCellStyle);
        headerCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("General"));
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        return headerCellStyle;
    }

    private static CellStyle getBoldFontCellStyle(Workbook workbook, String formatter) {
        CellStyle stringDataCellStyle1 = workbook.createCellStyle();
        stringDataCellStyle1.setFont(ExcelExportUtil.getFontBoldColorProperty(workbook, ExcelConstants.CALIBRI, IndexedColors.BLACK.index, 10));
        ExcelExportUtil.setBorderStyle(stringDataCellStyle1);
        stringDataCellStyle1.setDataFormat(HSSFDataFormat.getBuiltinFormat(formatter));
        return stringDataCellStyle1;
    }


    public static CellStyle getNumberDataCellStyle(Workbook workbook) {
        CellStyle numberDataCellStyle = workbook.createCellStyle();
        numberDataCellStyle.setFont(ExcelExportUtil.getFontBoldColorProperty(workbook, ExcelConstants.CALIBRI, IndexedColors.BLACK.index, 10));
        ExcelExportUtil.setBorderStyle(numberDataCellStyle);
        numberDataCellStyle.setDataFormat(HSSFDataFormat.getBuiltinFormat("0"));
        return numberDataCellStyle;
    }

    public static void createStringToNumberValueCell(Workbook workbook, String itemCode, Row row, int cellNumber) {
        if (Optional.ofNullable(itemCode).isPresent()) {
            try {
                row.createCell(cellNumber).setCellValue(Long.parseLong(itemCode));
                row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getBoldFontCellStyle(workbook, "0"));
            } catch (NumberFormatException ex) {
                row.createCell(cellNumber).setCellValue(itemCode);
                row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getBoldFontCellStyle(workbook, "General"));
            }
        } else {
            row.createCell(cellNumber).setCellValue(" ");
            row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getBlankCellStyle(workbook));
        }
    }

    public static void createStringCell(Workbook workbook, String cellValue, Row row, int cellNumber) {
        if (Optional.ofNullable(cellValue).isPresent()) {
            row.createCell(cellNumber).setCellValue(cellValue);
            row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getCellStyle(workbook, "General"));
        } else {
            row.createCell(cellNumber).setCellValue(" ");
            row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getBlankCellStyle(workbook));
        }
    }

    public static void createNumberCell(Workbook workbook, Double cellValue, Row row, int cellNumber) {
        if (Optional.ofNullable(cellValue).isPresent()) {
            row.createCell(cellNumber).setCellValue(cellValue);
            row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getCellStyle(workbook, "#,##0"));
        } else {
            row.createCell(cellNumber).setCellValue(" ");
            row.getCell(cellNumber).setCellStyle(ExcelExportUtil.getBlankCellStyle(workbook));
        }
    }

    public static String getMonthName(String headerName) {
        if (Optional.ofNullable(headerName).isPresent()) {
            return headerName;
        } else {
            return "";
        }
    }
}