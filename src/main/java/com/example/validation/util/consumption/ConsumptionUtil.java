package com.example.validation.util.consumption;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscMtBomDTO;
import com.example.validation.dto.RscMtPPdetailsDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.consumption.GrossConsumptionDTO;
import com.example.validation.dto.consumption.GrossConsumptionMainDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.service.RscMtItemService;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.MathUtils;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class ConsumptionUtil {
    private final RscMtItemService rscMtItemService;

    public ConsumptionUtil(RscMtItemService rscMtItemService) {
        this.rscMtItemService = rscMtItemService;
    }

    public RscItLogicalConsumptionDTO getLogicalConsumptionDTO(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO parentItemRscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO = new RscItLogicalConsumptionDTO();
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            rscItLogicalConsumptionDTO.setRscMtPPheaderId(bulkRscItGrossConsumptionDTO.getRscMtPPheaderId());
            rscItLogicalConsumptionDTO.setRscMtItemMpsParentId(bulkRscItGrossConsumptionDTO.getRscMtItemId());
            rscItLogicalConsumptionDTO.setMpsDate(bulkRscItGrossConsumptionDTO.getMpsDate());
        } else {
            if (Optional.ofNullable(rscMtPPdetailsDTO).isPresent()) {
                rscItLogicalConsumptionDTO.setRscMtPPheaderId(rscMtPPdetailsDTO.getRscMtPPheaderId());
                rscItLogicalConsumptionDTO.setRscMtItemMpsParentId(rscMtPPdetailsDTO.getRscMtItemId());
                rscItLogicalConsumptionDTO.setMpsDate(rscMtPPdetailsDTO.getMpsDate());
            } else {
                rscItLogicalConsumptionDTO.setRscMtPPheaderId(parentItemRscItLogicalConsumptionDTO.getRscMtPPheaderId());
                rscItLogicalConsumptionDTO.setRscMtItemMpsParentId(parentItemRscItLogicalConsumptionDTO.getRscMtItemMpsParentId());
                rscItLogicalConsumptionDTO.setMpsDate(parentItemRscItLogicalConsumptionDTO.getMpsDate());
            }
        }
        rscItLogicalConsumptionDTO.setRscMtBomId(rscMtBomDTO.getId());
        rscItLogicalConsumptionDTO.setRscMtItemParentId(rscMtBomDTO.getRscMtItemParentId());
        rscItLogicalConsumptionDTO.setRscMtItemParentCode(rscMtBomDTO.getParentItemCode());
        rscItLogicalConsumptionDTO.setRscMtItemChildId(rscMtBomDTO.getRscMtItemChildId());
        rscItLogicalConsumptionDTO.setRscMtItemChildCode(rscMtBomDTO.getChildItemCode());
        rscItLogicalConsumptionDTO.setM1Value(LogicalConsumptionMonthUtil.getM1LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM2Value(LogicalConsumptionMonthUtil.getM2LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM3Value(LogicalConsumptionMonthUtil.getM3LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM4Value(LogicalConsumptionMonthUtil.getM4LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM5Value(LogicalConsumptionMonthUtil.getM5LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM6Value(LogicalConsumptionMonthUtil.getM6LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM7Value(LogicalConsumptionMonthUtil.getM7LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM8Value(LogicalConsumptionMonthUtil.getM8LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM9Value(LogicalConsumptionMonthUtil.getM9LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM10Value(LogicalConsumptionMonthUtil.getM10LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM11Value(LogicalConsumptionMonthUtil.getM11LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setM12Value(LogicalConsumptionMonthUtil.getM12LogicalConsumption(rscMtPPdetailsDTO, parentItemRscItLogicalConsumptionDTO, rscMtBomDTO, bulkRscItGrossConsumptionDTO, isRMItem, isPM));
        rscItLogicalConsumptionDTO.setTotalValue(getTotalLogicalConsumptionValue(rscItLogicalConsumptionDTO, isPM));
        return rscItLogicalConsumptionDTO;
    }

    private Double getTotalLogicalConsumptionValue(RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, Boolean isPM) {
        double totalValue = rscItLogicalConsumptionDTO.getM1Value()
                + rscItLogicalConsumptionDTO.getM2Value()
                + rscItLogicalConsumptionDTO.getM3Value()
                + rscItLogicalConsumptionDTO.getM4Value()
                + rscItLogicalConsumptionDTO.getM5Value()
                + rscItLogicalConsumptionDTO.getM6Value()
                + rscItLogicalConsumptionDTO.getM7Value()
                + rscItLogicalConsumptionDTO.getM8Value()
                + rscItLogicalConsumptionDTO.getM9Value()
                + rscItLogicalConsumptionDTO.getM10Value()
                + rscItLogicalConsumptionDTO.getM11Value()
                + rscItLogicalConsumptionDTO.getM12Value();
        if (isPM)
            return DoubleUtils.round(totalValue);
        return DoubleUtils.getFormattedValue(totalValue);
    }

    public boolean isFGMaterialItem(Long rscMtItemId) {
        return rscMtItemService.findById(rscMtItemId).getItemCategoryDescription().equals(MaterialCategoryConstants.FG_MATERIAL);
    }

    public boolean isBulkMaterialItem(Long rscMtItemId) {
        return rscMtItemService.findById(rscMtItemId).getItemCategoryDescription().equals(MaterialCategoryConstants.BULK_MATERIAL);
    }

    public boolean isRawMaterialItem(Long rscMtItemId) {
        return rscMtItemService.findById(rscMtItemId).getItemCategoryDescription().equals(MaterialCategoryConstants.RAW_MATERIAL);
    }

    public boolean isSubBasesItem(Long rscMtItemId) {
        return rscMtItemService.findById(rscMtItemId).getItemCategoryDescription().equals(MaterialCategoryConstants.SUB_BASES);
    }

    public boolean isPackingMaterialItem(Long rscMtItemId) {
        return rscMtItemService.findById(rscMtItemId).getItemCategoryDescription().equals(MaterialCategoryConstants.PACKING_MATERIAL);
    }

    public boolean isWIPMaterialItem(Long rscMtItemId) {
        return rscMtItemService.findById(rscMtItemId).getItemCategoryDescription().equals(MaterialCategoryConstants.WIP_MATERIAL);
    }

    public Set<Long> getDistinctLCMtItemId(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOS) {
        return rscItLogicalConsumptionDTOS.stream().map(RscItLogicalConsumptionDTO::getRscMtItemChildId).collect(Collectors.toSet());
    }

    public GrossConsumptionMainDTO getGrossConsumptionMainDTO(RscMtPPheaderDTO rscMtPPHeaderDTO, List<GrossConsumptionDTO> grossConsumptionDTOs) {
        GrossConsumptionMainDTO grossConsumptionMainDTO = new GrossConsumptionMainDTO();
            grossConsumptionMainDTO.setMpsId(rscMtPPHeaderDTO.getId());
            grossConsumptionMainDTO.setMpsName(rscMtPPHeaderDTO.getMpsName());
            grossConsumptionMainDTO.setMpsDate(rscMtPPHeaderDTO.getMpsDate());
            grossConsumptionMainDTO.setItemCodes(grossConsumptionDTOs);
        return grossConsumptionMainDTO;
    }

    public List<RscItLogicalConsumptionDTO> getUniqueLogicalConsumptionDTOs(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs) {
        Map<String, RscItLogicalConsumptionDTO> uniqueRscItLogicalConsumptionDTOMap = new HashMap<>();
        for (RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO : rscItLogicalConsumptionDTOs) {
            String key = rscItLogicalConsumptionDTO.getRscMtItemParentId() + "-" + rscItLogicalConsumptionDTO.getRscMtItemChildId();
            if (uniqueRscItLogicalConsumptionDTOMap.containsKey(key)) {
                uniqueRscItLogicalConsumptionDTOMap.put(key, getAddedLogicalConsumptionDTO(rscItLogicalConsumptionDTO, uniqueRscItLogicalConsumptionDTOMap, key));
            } else
                uniqueRscItLogicalConsumptionDTOMap.put(key, rscItLogicalConsumptionDTO);
        }
        return new ArrayList<>(uniqueRscItLogicalConsumptionDTOMap.values());
    }

    private Boolean isUniqueChildItemList(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOList) {
        List<Long> childItemList = rscItLogicalConsumptionDTOList.stream().map(rscItLogicalConsumptionDTO -> rscItLogicalConsumptionDTO.getRscMtItemChildId()).collect(Collectors.toList());
        Set<Long> uniqueChildItemSet = new HashSet<>(childItemList);
        if (uniqueChildItemSet.size() < childItemList.size())
            return false;
        return true;
    }

    private RscItLogicalConsumptionDTO getAddedLogicalConsumptionDTO(RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, Map<String, RscItLogicalConsumptionDTO> uniqueRscItLogicalConsumptionDTOMap, String key) {
        rscItLogicalConsumptionDTO.setM1Value(MathUtils.add(rscItLogicalConsumptionDTO.getM1Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM1Value()));
        rscItLogicalConsumptionDTO.setM2Value(MathUtils.add(rscItLogicalConsumptionDTO.getM2Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM2Value()));
        rscItLogicalConsumptionDTO.setM3Value(MathUtils.add(rscItLogicalConsumptionDTO.getM3Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM3Value()));
        rscItLogicalConsumptionDTO.setM4Value(MathUtils.add(rscItLogicalConsumptionDTO.getM4Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM4Value()));
        rscItLogicalConsumptionDTO.setM5Value(MathUtils.add(rscItLogicalConsumptionDTO.getM5Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM5Value()));
        rscItLogicalConsumptionDTO.setM6Value(MathUtils.add(rscItLogicalConsumptionDTO.getM6Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM6Value()));
        rscItLogicalConsumptionDTO.setM7Value(MathUtils.add(rscItLogicalConsumptionDTO.getM7Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM7Value()));
        rscItLogicalConsumptionDTO.setM8Value(MathUtils.add(rscItLogicalConsumptionDTO.getM8Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM8Value()));
        rscItLogicalConsumptionDTO.setM9Value(MathUtils.add(rscItLogicalConsumptionDTO.getM9Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM9Value()));
        rscItLogicalConsumptionDTO.setM10Value(MathUtils.add(rscItLogicalConsumptionDTO.getM10Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM10Value()));
        rscItLogicalConsumptionDTO.setM11Value(MathUtils.add(rscItLogicalConsumptionDTO.getM11Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM11Value()));
        rscItLogicalConsumptionDTO.setM12Value(MathUtils.add(rscItLogicalConsumptionDTO.getM12Value(), uniqueRscItLogicalConsumptionDTOMap.get(key).getM12Value()));
        rscItLogicalConsumptionDTO.setTotalValue(MathUtils.add(rscItLogicalConsumptionDTO.getTotalValue(), uniqueRscItLogicalConsumptionDTOMap.get(key).getTotalValue()));
        return rscItLogicalConsumptionDTO;
    }
}