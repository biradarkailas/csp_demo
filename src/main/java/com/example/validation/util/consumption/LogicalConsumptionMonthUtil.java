package com.example.validation.util.consumption;

import com.example.validation.dto.RscMtBomDTO;
import com.example.validation.dto.RscMtPPdetailsDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.util.DateUtils;
import com.example.validation.util.FormulasUtil;

import java.time.LocalDate;
import java.util.Optional;

class LogicalConsumptionMonthUtil {
    private static Double quantity;

    static Double getM1LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM1Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 0);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM1Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 0);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM1Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 0);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM2LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM2Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 1);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM2Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 1);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM2Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 1);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM3LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM3Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 2);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM3Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 2);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM3Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 2);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM4LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM4Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 3);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM4Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 3);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM4Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 3);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM5LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM5Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 4);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM5Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 4);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM5Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 4);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM6LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM6Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 5);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM6Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 5);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM6Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 5);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM7LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM7Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 6);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM7Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 6);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM7Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 6);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM8LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM8Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 7);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM8Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 7);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM8Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 7);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM9LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM9Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 8);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM9Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 8);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM9Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 8);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM10LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM10Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 9);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM10Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 9);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM10Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 9);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM11LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM11Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 10);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM11Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 10);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM11Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 10);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    static Double getM12LogicalConsumption(RscMtPPdetailsDTO rscMtPPdetailsDTO, RscItLogicalConsumptionDTO rscItLogicalConsumptionDTO, RscMtBomDTO rscMtBomDTO, RscItGrossConsumptionDTO bulkRscItGrossConsumptionDTO, boolean isRMItem, boolean isPM) {
        if (Optional.ofNullable(bulkRscItGrossConsumptionDTO).isPresent()) {
            quantity = getValidMonthQuantity(bulkRscItGrossConsumptionDTO.getM12Value(), bulkRscItGrossConsumptionDTO.getMpsDate(), rscMtBomDTO, 11);
        } else {
            if (Optional.ofNullable(rscItLogicalConsumptionDTO).isPresent()) {
                quantity = getValidMonthQuantity(rscItLogicalConsumptionDTO.getM12Value(), rscItLogicalConsumptionDTO.getMpsDate(), rscMtBomDTO, 11);
            } else {
                quantity = getValidMonthQuantity(rscMtPPdetailsDTO.getM12Value(), rscMtPPdetailsDTO.getMpsDate(), rscMtBomDTO, 11);
            }
        }
        return getLogicalConsumption(quantity, rscMtBomDTO, isRMItem, isPM);
    }

    private static Double getValidMonthQuantity(Double quantityValue, LocalDate mpsDate, RscMtBomDTO rscMtBomDTO, int monthNumber) {
        if (mpsDate != null) {
            if (DateUtils.checkEndDateValid(rscMtBomDTO.getApplicableTo(), mpsDate.plusMonths(monthNumber)) &&
                    DateUtils.checkStartDateValid(rscMtBomDTO.getApplicableFrom(), mpsDate.plusMonths(monthNumber))) {
                return quantityValue;
            }
        }
        return 0.0;
    }

    private static Double getLogicalConsumption(Double quantity, RscMtBomDTO rscMtBomDTO, boolean isRMItem, boolean isPM) {
        if (isRMItem)
            return FormulasUtil.rmLogicalConsumption(quantity, rscMtBomDTO);
        /*if (isPM)
            return DoubleUtils.round(FormulasUtil.logicalConsumption(quantity, rscMtBomDTO));*/
        return FormulasUtil.logicalConsumption(quantity, rscMtBomDTO);
    }
}