package com.example.validation.util.consumption;

import com.example.validation.dto.RscMtItemDTO;
import com.example.validation.dto.consumption.RscItLogicalConsumptionDTO;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.FormulasUtil;

import java.util.List;
import java.util.Optional;

public class GrossConsumptionMonthUtil {

    public static Double getM1GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM1Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM2GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM2Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM3GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM3Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM4GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM4Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM5GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM5Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM6GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM6Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM7GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM7Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM8GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM8Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM9GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM9Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM10GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM10Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    public static Double getM11GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM11Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }


    public static Double getM12GrossConsumption(List<RscItLogicalConsumptionDTO> rscItLogicalConsumptionDTOs, RscMtItemDTO rscMtItemDTO, boolean isBulk, boolean isPM) {
        Double totalQuantity = rscItLogicalConsumptionDTOs.stream()
                .map(RscItLogicalConsumptionDTO::getM12Value)
                .reduce(0.0, Double::sum);
        if (isPM)
            return DoubleUtils.round(totalQuantity);
        return getGrossQuantity(isBulk, rscMtItemDTO, totalQuantity);
    }

    private static Double getGrossQuantity(boolean isBulk, RscMtItemDTO rscMtItemDTO, Double totalQuantity) {
        if (isBulk) {
            return FormulasUtil.getValidQuantity(getMoqValue(rscMtItemDTO), getSeriesValue(rscMtItemDTO), totalQuantity);
        }
        return DoubleUtils.getFormattedValue(totalQuantity);
    }

    private static Double getSeriesValue(RscMtItemDTO rscMtItemDTO) {
        if (Optional.ofNullable(rscMtItemDTO.getSeriesValue()).isPresent())
            return rscMtItemDTO.getSeriesValue();
        return 0.0;
    }

    private static Double getMoqValue(RscMtItemDTO rscMtItemDTO) {
        if (Optional.ofNullable(rscMtItemDTO.getMoqValue()).isPresent())
            return rscMtItemDTO.getMoqValue();
        return 0.0;
    }
}