package com.example.validation.util;

import com.example.validation.repository.ErrorLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Component
public class ValidationProcedureUtils {
    private final Logger log = LoggerFactory.getLogger(ValidationProcedureUtils.class);
    private final ErrorLogRepository errorLogRepository;

    public ValidationProcedureUtils(ErrorLogRepository errorLogRepository) {
        this.errorLogRepository = errorLogRepository;
    }

    public boolean itemValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.itemValidation());
        } catch (Exception e) {
            log.info("Item validation SQL Exception");
        }
        return false;
    }

    public boolean mpsValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.mpsValidation());
        } catch (Exception e) {
            log.info("mps SQL Exception");
        }
        return false;
    }

    public boolean workingDaysDataValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.workingDaysValidation());
        } catch (Exception e) {
            log.info("Working Days Data SQL Exception");
        }
        return false;
    }

    public boolean bomValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.bomValidation());
        } catch (Exception e) {
            log.info("bom SQL Exception");
        }
        return false;
    }

    public boolean mouldMasterValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.mouldMasterValidation());
        } catch (Exception e) {
            log.info("mould_master SQL Exception");
        }
        return false;
    }

    public boolean pmMaterialMasterValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.pmMaterialMasterValidation());
        } catch (Exception e) {
            log.info("pm_material_master SQL Exception");
        }
        return false;
    }

    public boolean rmMaterialMasterValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.rmMaterialMasterValidation());
        } catch (Exception e) {
            log.info("rm_material_master SQL Exception");
        }
        return false;
    }

    public boolean openPoValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.openPoValidation());
        } catch (Exception e) {
            log.info("open_po SQL Exception");
        }
        return false;
    }

    public boolean inStdValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.inStdValidation());
        } catch (Exception e) {
            log.info("in_std SQL Exception");
        }
        return false;
    }

    public boolean midNightStockValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.midNightStockValidation());
        } catch (Exception e) {
            log.info("mid_night_stock SQL Exception");
        }
        return false;
    }

    public boolean usListGenValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.usListGenValidation());
        } catch (Exception e) {
            log.info("us_listgen SQL Exception");
        }
        return false;
    }

    public boolean technicalSizeValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.technicalSizeValidation());
        } catch (Exception e) {
            log.info("technical_size SQL Exception");
        }
        return false;
    }

    public boolean linesValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.linesValidation());
        } catch (Exception e) {
            log.info("lines SQL Exception");
        }
        return false;
    }

    public boolean skidsValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.skidsValidation());
        } catch (Exception e) {
            log.info("skids SQL Exception");
        }
        return false;
    }

    public boolean stdPriceValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.stdPriceValidation());
        } catch (Exception e) {
            log.info("std Price SQL Exception");
        }
        return false;
    }

    public boolean receiptsTillDateValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.receiptsTillDateValidation());
        } catch (Exception e) {
            log.info("Receipts Till Date SQL Exception");
        }
        return false;
    }

    public boolean otherLocationStockValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.otherLocationStockValidation());
        } catch (Exception e) {
            log.info("Other Location Stock SQL Exception");
        }
        return false;
    }

    public boolean physicalRejectionValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.physicalRejectionValidation());
        } catch (Exception e) {
            log.info("Physical Rejection SQL Exception");
        }
        return false;
    }

    public boolean item2Validation() {
        try {
            return StringUtil.isEqual(errorLogRepository.item2Validation());
        } catch (Exception e) {
            log.info("item2 SQL Exception");
        }
        return false;
    }

    public boolean forecastMpsValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.forecastMpsValidation());
        } catch (Exception e) {
            log.info("Forecast MPS SQL Exception");
        }
        return false;
    }

    public boolean mapValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.mapValidation());
        } catch (Exception e) {
            log.info("MAP SQL Exception");
        }
        return false;
    }

    public boolean pmOtifValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.pmOtifValidation());
        } catch (Exception e) {
            log.info("PM OTIF SQL Exception");
        }
        return false;
    }

    public boolean item2DataValidation() {
        try {
            return StringUtil.isEqual(errorLogRepository.item2DataValidation());
        } catch (Exception e) {
            log.info("Item 2 data SQL Exception");
        }
        return false;
    }
}
