package com.example.validation.util.supply;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.dto.supply.PurchasePlanCalculationsDTO;
import com.example.validation.dto.supply.PurchasePlanCalculationsMainDTO;
import com.example.validation.dto.supply.RscIotPMPurchasePlanCoverDaysDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCoverDaysDTO;
import com.example.validation.util.DoubleUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;

@Component
public class PurchasePlanCoverDaysUtil {
    public RscIotPMPurchasePlanCoverDaysDTO getRscIotPMPurchasePlanCoverDaysDTO(PurchasePlanCalculationsMainDTO pmPurchasePlanCalculationsDTO) {
        RscIotPMPurchasePlanCoverDaysDTO rscIotPMPurchasePlanCoverDaysDTO = new RscIotPMPurchasePlanCoverDaysDTO();
        rscIotPMPurchasePlanCoverDaysDTO.setRscMtPPheaderId(pmPurchasePlanCalculationsDTO.getRscMtPPheaderId());
        rscIotPMPurchasePlanCoverDaysDTO.setRscMtItemId(pmPurchasePlanCalculationsDTO.getRscMtItemId());
        rscIotPMPurchasePlanCoverDaysDTO.setPmPurchasePlanCalculationsId(pmPurchasePlanCalculationsDTO.getId());
        Map<Integer, PurchasePlanCalculationsDTO> purchasePlanCalculationsDTOMap = getAllMonthConsumption(pmPurchasePlanCalculationsDTO);
        rscIotPMPurchasePlanCoverDaysDTO.setM1Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.ZERO));
        rscIotPMPurchasePlanCoverDaysDTO.setM2Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.ONE));
        rscIotPMPurchasePlanCoverDaysDTO.setM3Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.TWO));
        rscIotPMPurchasePlanCoverDaysDTO.setM4Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.THREE));
        rscIotPMPurchasePlanCoverDaysDTO.setM5Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.FOUR));
        rscIotPMPurchasePlanCoverDaysDTO.setM6Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.FIVE));
        rscIotPMPurchasePlanCoverDaysDTO.setM7Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.SIX));
        rscIotPMPurchasePlanCoverDaysDTO.setM8Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.SEVEN));
        rscIotPMPurchasePlanCoverDaysDTO.setM9Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.EIGHT));
        rscIotPMPurchasePlanCoverDaysDTO.setM10Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.NINE));
        rscIotPMPurchasePlanCoverDaysDTO.setM11Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.TEN));
        rscIotPMPurchasePlanCoverDaysDTO.setM12Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, pmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.ELEVEN));
        return rscIotPMPurchasePlanCoverDaysDTO;
    }

    public RscIotRMPurchasePlanCoverDaysDTO getRscIotRMPurchasePlanCoverDaysDTO(PurchasePlanCalculationsMainDTO rmPurchasePlanCalculationsDTO) {
        RscIotRMPurchasePlanCoverDaysDTO rscIotRMPurchasePlanCoverDaysDTO = new RscIotRMPurchasePlanCoverDaysDTO();
        rscIotRMPurchasePlanCoverDaysDTO.setRscMtPPheaderId(rmPurchasePlanCalculationsDTO.getRscMtPPheaderId());
        rscIotRMPurchasePlanCoverDaysDTO.setRscMtItemId(rmPurchasePlanCalculationsDTO.getRscMtItemId());
        rscIotRMPurchasePlanCoverDaysDTO.setRmPurchasePlanCalculationsId(rmPurchasePlanCalculationsDTO.getId());
        Map<Integer, PurchasePlanCalculationsDTO> purchasePlanCalculationsDTOMap = getAllMonthConsumption(rmPurchasePlanCalculationsDTO);
        rscIotRMPurchasePlanCoverDaysDTO.setM1Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.ZERO));
        rscIotRMPurchasePlanCoverDaysDTO.setM2Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.ONE));
        rscIotRMPurchasePlanCoverDaysDTO.setM3Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.TWO));
        rscIotRMPurchasePlanCoverDaysDTO.setM4Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.THREE));
        rscIotRMPurchasePlanCoverDaysDTO.setM5Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.FOUR));
        rscIotRMPurchasePlanCoverDaysDTO.setM6Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.FIVE));
        rscIotRMPurchasePlanCoverDaysDTO.setM7Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.SIX));
        rscIotRMPurchasePlanCoverDaysDTO.setM8Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.SEVEN));
        rscIotRMPurchasePlanCoverDaysDTO.setM9Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.EIGHT));
        rscIotRMPurchasePlanCoverDaysDTO.setM10Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.NINE));
        rscIotRMPurchasePlanCoverDaysDTO.setM11Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.TEN));
        rscIotRMPurchasePlanCoverDaysDTO.setM12Value(getMonthWiseCoverDays(purchasePlanCalculationsDTOMap, rmPurchasePlanCalculationsDTO.getMpsDate(), ApplicationConstants.ELEVEN));
        return rscIotRMPurchasePlanCoverDaysDTO;
    }

    private Double getMonthWiseCoverDays(Map<Integer, PurchasePlanCalculationsDTO> purchasePlanCalculationsDTOMap, LocalDate mpsDate, Integer monthNumber) {
        Double totalOrderStock = getTotalOrderStock(purchasePlanCalculationsDTOMap.get(monthNumber));
        return getCoverDays(totalOrderStock, purchasePlanCalculationsDTOMap, mpsDate, monthNumber, ApplicationConstants.DOUBLE_ZERO);
    }

    private Double getCoverDays(Double totalOrderStock, Map<Integer, PurchasePlanCalculationsDTO> purchasePlanCalculationsDTOMap, LocalDate mpsDate, Integer monthNumber, Double coverDays) {
        if (monthNumber < ApplicationConstants.TWELVE) {
            Double currentMonthDays = getCurrentMonthDays(mpsDate.plusMonths(monthNumber));
            Double currentMonthConsumption = purchasePlanCalculationsDTOMap.get(monthNumber).getConsumption();
            Double remainingOrderStock = getRemainingOrderStock(totalOrderStock, currentMonthConsumption);
            if (remainingOrderStock > ApplicationConstants.ZERO) {
                Double totalCoverDays = getTotalCoverDays(coverDays, currentMonthDays);
                return getCoverDays(remainingOrderStock, purchasePlanCalculationsDTOMap, mpsDate, monthNumber + ApplicationConstants.ONE, totalCoverDays);
            }
            return getFinalCoverDays(coverDays, totalOrderStock, currentMonthConsumption, currentMonthDays);
        }
        return DoubleUtils.round(coverDays);
    }

    private Double getFinalCoverDays(Double coverDays, Double totalOrderStock, Double currentMonthConsumption, Double currentMonthDays) {
        Double finalCoverDays = coverDays + calculateCoverDays(totalOrderStock, currentMonthConsumption, currentMonthDays);
        if (finalCoverDays > ApplicationConstants.ONE_YEAR_DAYS_COUNT) {
            return ApplicationConstants.ONE_YEAR_DAYS_COUNT;
        } else if (finalCoverDays < ApplicationConstants.ZERO) {
            return ApplicationConstants.DOUBLE_ZERO;
        }
        return DoubleUtils.round(finalCoverDays);
    }

    double calculateCoverDays(Double totalOrderStock, Double currentMonthConsumption, Double currentMonthDays) {
        return (totalOrderStock / currentMonthConsumption) * currentMonthDays;
    }

    private Double getCurrentMonthDays(LocalDate currentMonth) {
        return Double.valueOf(currentMonth.lengthOfMonth());
    }

    private Double getTotalCoverDays(Double coverDays, Double currentMonthCoverDays) {
        return coverDays + currentMonthCoverDays;
    }

    private Double getRemainingOrderStock(Double totalOrderStock, Double currentMonthConsumption) {
        return totalOrderStock - currentMonthConsumption;
    }

    private Double getTotalOrderStock(PurchasePlanCalculationsDTO pmPurchasePlanCalculationsDTO) {
        return pmPurchasePlanCalculationsDTO.getMonthEndStock() + pmPurchasePlanCalculationsDTO.getFinalSupply();
    }

    private Map<Integer, PurchasePlanCalculationsDTO> getAllMonthConsumption(PurchasePlanCalculationsMainDTO pmPurchasePlanCalculationsDTO) {
        Map<Integer, PurchasePlanCalculationsDTO> purchasePlanCalculationsDTOMap = new HashMap<>();
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.ZERO, pmPurchasePlanCalculationsDTO.getMonth1SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.ONE, pmPurchasePlanCalculationsDTO.getMonth2SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.TWO, pmPurchasePlanCalculationsDTO.getMonth3SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.THREE, pmPurchasePlanCalculationsDTO.getMonth4SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.FOUR, pmPurchasePlanCalculationsDTO.getMonth5SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.FIVE, pmPurchasePlanCalculationsDTO.getMonth6SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.SIX, pmPurchasePlanCalculationsDTO.getMonth7SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.SEVEN, pmPurchasePlanCalculationsDTO.getMonth8SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.EIGHT, pmPurchasePlanCalculationsDTO.getMonth9SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.NINE, pmPurchasePlanCalculationsDTO.getMonth10SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.TEN, pmPurchasePlanCalculationsDTO.getMonth11SupplyDTO());
        purchasePlanCalculationsDTOMap.put(ApplicationConstants.ELEVEN, pmPurchasePlanCalculationsDTO.getMonth12SupplyDTO());
        return purchasePlanCalculationsDTOMap;
    }
}