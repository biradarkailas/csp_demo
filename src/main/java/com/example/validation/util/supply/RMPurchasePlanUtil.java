package com.example.validation.util.supply;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscIitRMItemWiseSupplierDetailsDTO;
import com.example.validation.dto.RscIotRmStockDTO;
import com.example.validation.dto.RscItItemWiseSupplierDTO;
import com.example.validation.dto.RscMtRmOpenPoDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.supply.MonthWiseRMPurchasePlanDTO;
import com.example.validation.dto.supply.RscIotRMPurchasePlanCalculationsDTO;
import com.example.validation.service.RscIitRMItemWiseSupplierDetailsService;
import com.example.validation.service.RscIotRmStockService;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class RMPurchasePlanUtil {
    private final RscIotRmStockService rscIotRmStockService;
    private final RscIitRMItemWiseSupplierDetailsService rscIitRMItemWiseSupplierDetailsService;

    public RMPurchasePlanUtil(RscIotRmStockService rscIotRmStockService, RscIitRMItemWiseSupplierDetailsService rscIitRMItemWiseSupplierDetailsService) {
        this.rscIotRmStockService = rscIotRmStockService;
        this.rscIitRMItemWiseSupplierDetailsService = rscIitRMItemWiseSupplierDetailsService;
    }

    public RscIotRMPurchasePlanCalculationsDTO getRMPurchasePlanCalculationsDTO(RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO) {
        RscIotRMPurchasePlanCalculationsDTO rscIotRMPurchasePlanCalculationsDTO = new RscIotRMPurchasePlanCalculationsDTO();
        if (Optional.ofNullable(rscItItemWiseSupplierDTO).isPresent()) {
            RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO = rscIitRMItemWiseSupplierDetailsService.findByItemWiseSupplierId(rscItItemWiseSupplierDTO.getId());
            if (Optional.ofNullable(rscIitRMItemWiseSupplierDetailsDTO).isPresent()) {
                Map<Integer, MonthWiseRMPurchasePlanDTO> monthWiseRMPurchasePlanDTOMap = getMonthWiseRMPurchasePlanDTOMap(rscItGrossConsumptionDTO, rscIitRMItemWiseSupplierDetailsDTO);
                rscIotRMPurchasePlanCalculationsDTO.setRscMtItemId(rscItGrossConsumptionDTO.getRscMtItemId());
                rscIotRMPurchasePlanCalculationsDTO.setRscItRMGrossConsumptionId(rscItGrossConsumptionDTO.getId());
                rscIotRMPurchasePlanCalculationsDTO.setRscMtPPheaderId(rscItGrossConsumptionDTO.getRscMtPPheaderId());
                rscIotRMPurchasePlanCalculationsDTO.setItemWiseSupplierDetailsId(rscIitRMItemWiseSupplierDetailsDTO.getId());
                rscIotRMPurchasePlanCalculationsDTO.setMonth1PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.ONE));
                rscIotRMPurchasePlanCalculationsDTO.setMonth2PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.TWO));
                rscIotRMPurchasePlanCalculationsDTO.setMonth3PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.THREE));
                rscIotRMPurchasePlanCalculationsDTO.setMonth4PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.FOUR));
                rscIotRMPurchasePlanCalculationsDTO.setMonth5PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.FIVE));
                rscIotRMPurchasePlanCalculationsDTO.setMonth6PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.SIX));
                rscIotRMPurchasePlanCalculationsDTO.setMonth7PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.SEVEN));
                rscIotRMPurchasePlanCalculationsDTO.setMonth8PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.EIGHT));
                rscIotRMPurchasePlanCalculationsDTO.setMonth9PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.NINE));
                rscIotRMPurchasePlanCalculationsDTO.setMonth10PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.TEN));
                rscIotRMPurchasePlanCalculationsDTO.setMonth11PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.ELEVEN));
                rscIotRMPurchasePlanCalculationsDTO.setMonth12PurchasePlanDTO(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.TWELVE));
                return rscIotRMPurchasePlanCalculationsDTO;
            }
        }
        return null;
    }

    private Map<Integer, MonthWiseRMPurchasePlanDTO> getMonthWiseRMPurchasePlanDTOMap(RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO) {
        Map<Integer, MonthWiseRMPurchasePlanDTO> monthWiseRMPurchasePlanDTOMap = new HashMap<>();
        List<RscIotRmStockDTO> rscIotRmStockDTOs = rscIotRmStockService.findByRscMtItemId(rscItGrossConsumptionDTO.getRscMtItemId(), rscItGrossConsumptionDTO.getMpsDate());
        Double openingStock = getMonth1OpeningStock(rscIotRmStockDTOs, rscItGrossConsumptionDTO);
        Map<Integer, Double> monthWiseObsoleteValue = getObsoleteValueMap(rscIotRmStockDTOs, rscItGrossConsumptionDTO);
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.ONE, RMPurchasePlanMonthUtil.getMonth1PurchasePlan(openingStock, rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.TWO, RMPurchasePlanMonthUtil.getMonth2PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.ONE)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.THREE, RMPurchasePlanMonthUtil.getMonth3PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.TWO)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.FOUR, RMPurchasePlanMonthUtil.getMonth4PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.THREE)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.FIVE, RMPurchasePlanMonthUtil.getMonth5PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.FOUR)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.SIX, RMPurchasePlanMonthUtil.getMonth6PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.FIVE)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.SEVEN, RMPurchasePlanMonthUtil.getMonth7PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.SIX)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.EIGHT, RMPurchasePlanMonthUtil.getMonth8PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.SEVEN)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.NINE, RMPurchasePlanMonthUtil.getMonth9PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.EIGHT)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.TEN, RMPurchasePlanMonthUtil.getMonth10PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.NINE)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.ELEVEN, RMPurchasePlanMonthUtil.getMonth11PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.TEN)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        monthWiseRMPurchasePlanDTOMap.put(ApplicationConstants.TWELVE, RMPurchasePlanMonthUtil.getMonth12PurchasePlan(getOpeningStock(monthWiseRMPurchasePlanDTOMap.get(ApplicationConstants.ELEVEN)), rscItGrossConsumptionDTO, rscIotRmStockDTOs, rscIitRMItemWiseSupplierDetailsDTO, monthWiseObsoleteValue));
        return monthWiseRMPurchasePlanDTOMap;
    }

    private Map<Integer, Double> getObsoleteValueMap(List<RscIotRmStockDTO> rscIotRmStockDTOs, RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        return getActualExpiryStockMap(getCumulativeExpiryMap(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs), getConsumptionBeforeMap(rscItGrossConsumptionDTO));
    }

    private Map<Integer, Double> getActualExpiryStockMap(Map<Integer, Double> cumulativeExpiryMap, Map<Integer, Double> consumptionBeforeMap) {
        Map<Integer, Double> actualExpiryMap = new HashMap<>();
        for (int monthNumber = 1; monthNumber <= 12; monthNumber++) {
            actualExpiryMap.put(monthNumber, getActualExpiryStockValue(cumulativeExpiryMap.get(monthNumber), consumptionBeforeMap.get(monthNumber), getPreviousAllMonthSum(actualExpiryMap)));
        }
        return actualExpiryMap;
    }

    private Double getPreviousAllMonthSum(Map<Integer, Double> actualExpiryMap) {
        if (actualExpiryMap.isEmpty()) {
            return 0.0;
        }
        return actualExpiryMap.values().stream().reduce(0.0, Double::sum);
    }

    private Double getActualExpiryStockValue(Double cumulativeExpiryValue, Double consumptionBeforeValue, Double previousAllMonthValue) {
        Double result = cumulativeExpiryValue - consumptionBeforeValue - previousAllMonthValue;
        if (result < 0) {
            return 0.0;
        }
        return result;
    }

    private Map<Integer, Double> getConsumptionBeforeMap(RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        Map<Integer, Double> consumptionBeforeMap = new HashMap<>();
        Map<Integer, Double> allMonthConsumptionMap = PMPurchasePlanMonthUtil.getAllMonthMap(rscItGrossConsumptionDTO);
        for (int monthNumber = 0; monthNumber < 12; monthNumber++) {
            consumptionBeforeMap.put(monthNumber + 1, getConsumptionBeforeValue(consumptionBeforeMap, allMonthConsumptionMap, monthNumber));
        }
        return consumptionBeforeMap;
    }

    private Double getConsumptionBeforeValue(Map<Integer, Double> consumptionBeforeMap, Map<Integer, Double> allMonthConsumptionMap, int previousMonthNumber) {
        if (consumptionBeforeMap.isEmpty()) {
            return 0.0;
        }
        return consumptionBeforeMap.get(previousMonthNumber) + allMonthConsumptionMap.get(previousMonthNumber);
    }

    private Map<Integer, Double> getCumulativeExpiryMap(LocalDate mpsDate, List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        Map<Integer, Double> obsoleteValueMap = new HashMap<>();
        for (int monthNumber = 0; monthNumber < 12; monthNumber++) {
            obsoleteValueMap.put(monthNumber + 1, getObsoleteValue(mpsDate, rscIotRmStockDTOs, monthNumber, getPreviousMonthValue(obsoleteValueMap, monthNumber)));
        }
        return obsoleteValueMap;
    }

    private Double getObsoleteValue(LocalDate mpsDate, List<RscIotRmStockDTO> rscIotRmStockDTOs, int monthNumber, Double cumulativeExpiry) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = getFilteredExpiryStock(mpsDate, rscIotRmStockDTOs, monthNumber);
        if (optionalRscIotRmStockDTO.isPresent()) {
            return optionalRscIotRmStockDTO.get().getQuantity() + cumulativeExpiry;
        }
        return cumulativeExpiry;
    }

    private Optional<RscIotRmStockDTO> getFilteredExpiryStock(LocalDate mpsDate, List<RscIotRmStockDTO> rscIotRmStockDTOs, int monthNumber) {
        return rscIotRmStockDTOs.stream()
                .filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_EXPIRED))
                .filter(rscIotRmStockDTO -> rscIotRmStockDTO.getMpsDate().equals(mpsDate))
                .filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate.plusMonths(monthNumber), rscIotRmStockDTO.getStockDate()))
                .findAny();
    }

    private Double getPreviousMonthValue(Map<Integer, Double> obsoleteValueMap, int previousMonthNumber) {
        if (obsoleteValueMap.isEmpty()) {
            return 0.0;
        }
        return obsoleteValueMap.get(previousMonthNumber);
    }

    private Double getOpeningStock(MonthWiseRMPurchasePlanDTO previousMonthPurchasePlan) {
        return DoubleUtils.getFormattedValue(previousMonthPurchasePlan.getNextMonthOpeningStock());
    }

    private Double getMonth1OpeningStock(List<RscIotRmStockDTO> rscIotRmStockDTOs, RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        List<RscIotRmStockDTO> filteredRscIotRmStockDTOs = getValidRMStockByMPSDate(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs);
        return getOpeningStockValue(getPurchasePlanMap(filteredRscIotRmStockDTOs));
    }

    private Double getOpeningStockValue(Map<String, Double> purchasePlanMap) {
        return DoubleUtils.getFormattedValue(purchasePlanMap.get(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES)
                + purchasePlanMap.get(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD)
                + purchasePlanMap.get(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM)
                + purchasePlanMap.get(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other)
                - purchasePlanMap.get(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION));
    }

    private Map<String, Double> getPurchasePlanMap(List<RscIotRmStockDTO> filteredRscIotRmStockDTOs) {
        Map<String, Double> purchasePlanMap = new HashMap<>();
        purchasePlanMap.put(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES, getRMMES(filteredRscIotRmStockDTOs));
        purchasePlanMap.put(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD, getRMRTD(filteredRscIotRmStockDTOs));
        purchasePlanMap.put(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION, getRMRejection(filteredRscIotRmStockDTOs));
        purchasePlanMap.put(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM, getRMBulkOpRm(filteredRscIotRmStockDTOs));
        purchasePlanMap.put(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other, getRMTransfer(filteredRscIotRmStockDTOs));
        return purchasePlanMap;
    }

    private Double getRMMES(List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = rscIotRmStockDTOs.stream().filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_MES)).findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }

    private Double getRMRTD(List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = rscIotRmStockDTOs.stream().filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_RTD)).findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }

    private Double getRMRejection(List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = rscIotRmStockDTOs.stream().filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_REJECTION)).findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }

    private Double getRMBulkOpRm(List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = rscIotRmStockDTOs.stream().filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_BULK_OP_RM)).findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }

    private Double getRMTransfer(List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = rscIotRmStockDTOs.stream().filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_Other)).findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }

    private List<RscIotRmStockDTO> getValidRMStockByMPSDate(LocalDate mpsDate, List<RscIotRmStockDTO> rscIotRmStockDTOs) {
        return rscIotRmStockDTOs.stream()
                .filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate, rscIotRmStockDTO.getStockDate()))
                .collect(Collectors.toList());
    }

    public String getPORemarkValue(List<RscMtRmOpenPoDTO> rscMtRmOpenPoDTOs) {
        StringBuilder poRemark = new StringBuilder(100);
        String prefix = "";
        for (RscMtRmOpenPoDTO rscMtRmOpenPoDTO : rscMtRmOpenPoDTOs) {
            poRemark.append(prefix);
            prefix = "; ";
            poRemark.append(getPORemarkString(rscMtRmOpenPoDTO, getTotalQuantity(rscMtRmOpenPoDTO)));
        }
        return poRemark.toString();
    }

    private String getPORemarkString(RscMtRmOpenPoDTO rscMtRmOpenPoDTO, int quantity) {
        return "PO : " + rscMtRmOpenPoDTO.getPoNumber() + " / " + quantity + " KG";
    }

    private int getTotalQuantity(RscMtRmOpenPoDTO rscMtRmOpenPoDTO) {
        return (int) (rscMtRmOpenPoDTO.getOrderedQuantity() - rscMtRmOpenPoDTO.getReceivedQuantity());
    }
}