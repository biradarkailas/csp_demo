package com.example.validation.util.supply;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscIitRMItemWiseSupplierDetailsDTO;
import com.example.validation.dto.RscIotRmStockDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.supply.MonthWiseRMPurchasePlanDTO;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.FormulasUtil;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class RMPurchasePlanMonthUtil {

    static MonthWiseRMPurchasePlanDTO getMonth1PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM1Value(), rscItGrossConsumptionDTO.getM2Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 0);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(1), obsoleteValueMap.get(2), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate());
    }

    static MonthWiseRMPurchasePlanDTO getMonth2PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM2Value(), rscItGrossConsumptionDTO.getM3Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 1);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(2), obsoleteValueMap.get(3), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(1));
    }

    static MonthWiseRMPurchasePlanDTO getMonth3PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM3Value(), rscItGrossConsumptionDTO.getM4Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 2);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(3), obsoleteValueMap.get(4), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(2));
    }

    static MonthWiseRMPurchasePlanDTO getMonth4PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM4Value(), rscItGrossConsumptionDTO.getM5Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 3);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(4), obsoleteValueMap.get(5), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(3));
    }

    static MonthWiseRMPurchasePlanDTO getMonth5PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM5Value(), rscItGrossConsumptionDTO.getM6Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 4);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(5), obsoleteValueMap.get(6), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(4));
    }

    static MonthWiseRMPurchasePlanDTO getMonth6PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM6Value(), rscItGrossConsumptionDTO.getM7Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 5);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(6), obsoleteValueMap.get(7), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(5));
    }

    static MonthWiseRMPurchasePlanDTO getMonth7PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM7Value(), rscItGrossConsumptionDTO.getM8Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 6);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(7), obsoleteValueMap.get(8), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(6));
    }

    static MonthWiseRMPurchasePlanDTO getMonth8PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM8Value(), rscItGrossConsumptionDTO.getM9Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 7);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(8), obsoleteValueMap.get(9), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(7));
    }

    static MonthWiseRMPurchasePlanDTO getMonth9PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM9Value(), rscItGrossConsumptionDTO.getM10Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 8);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(9), obsoleteValueMap.get(10), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(8));
    }

    static MonthWiseRMPurchasePlanDTO getMonth10PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM10Value(), rscItGrossConsumptionDTO.getM11Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 9);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(10), obsoleteValueMap.get(11), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(9));
    }

    static MonthWiseRMPurchasePlanDTO getMonth11PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM11Value(), rscItGrossConsumptionDTO.getM12Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 10);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(11), obsoleteValueMap.get(12), rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(10));
    }

    static MonthWiseRMPurchasePlanDTO getMonth12PurchasePlan(Double openingStock, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, List<RscIotRmStockDTO> rscIotRmStockDTOs, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO, Map<Integer, Double> obsoleteValueMap) {
        Map<String, Double> purchasePlanMap = getRMPurchasePlanMap(openingStock, rscItGrossConsumptionDTO.getM12Value(), rscItGrossConsumptionDTO.getM12Value(), rscIitRMItemWiseSupplierDetailsDTO);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscIotRmStockDTOs, 11);
        return getMonthWiseRMPurchasePlanDTO(purchasePlanMap, openPO, obsoleteValueMap.get(12), 0.0, rscIitRMItemWiseSupplierDetailsDTO.getApplicableFrom(), rscIitRMItemWiseSupplierDetailsDTO.getApplicableTo(), rscItGrossConsumptionDTO.getMpsDate().plusMonths(11));
    }

    private static MonthWiseRMPurchasePlanDTO getMonthWiseRMPurchasePlanDTO(Map<String, Double> purchasePlanMap, Double openPO, Double currentObsoleteValue, Double nextMonthObsoleteValue, LocalDate fromDate, LocalDate toDate, LocalDate mpsDate) {
        MonthWiseRMPurchasePlanDTO monthWiseRMPurchasePlanDTO = new MonthWiseRMPurchasePlanDTO();
        Double stockAfterConsumption = getStockAfterConsumption(purchasePlanMap, currentObsoleteValue);
        Double closingStock = getClosingStock(purchasePlanMap, openPO, stockAfterConsumption, nextMonthObsoleteValue);
        Double suggestedOrder = getSuggestedOrder(purchasePlanMap, closingStock);
        Double finalSuggestedOrder = getFinalSuggestedOrder(purchasePlanMap, suggestedOrder, fromDate, toDate, mpsDate);
        Double nextMonthConsumption = getNextMonthConsumption(stockAfterConsumption, openPO, suggestedOrder);
        monthWiseRMPurchasePlanDTO.setObsoleteStock(currentObsoleteValue);
        monthWiseRMPurchasePlanDTO.setOpeningStock(purchasePlanMap.get(MaterialCategoryConstants.OPENING_STOCK));
        monthWiseRMPurchasePlanDTO.setStockAfterConsumption(stockAfterConsumption);
        monthWiseRMPurchasePlanDTO.setClosingStock(closingStock);
        monthWiseRMPurchasePlanDTO.setOpenPoStock(openPO);
        monthWiseRMPurchasePlanDTO.setSuggestedPurchasePlan(DoubleUtils.round(finalSuggestedOrder));
        monthWiseRMPurchasePlanDTO.setNextMonthOpeningStock(nextMonthConsumption);
        return monthWiseRMPurchasePlanDTO;
    }

    private static Double getNextMonthConsumption(Double stockAfterConsumption, Double openPO, Double finalSuggestedOrder) {
        return stockAfterConsumption + openPO + finalSuggestedOrder;
    }

    private static Double getStockAfterConsumption(Map<String, Double> purchasePlanMap, Double obsoleteValue) {
        return purchasePlanMap.get(MaterialCategoryConstants.OPENING_STOCK) - purchasePlanMap.get(MaterialCategoryConstants.CURRENT_MONTH) - obsoleteValue;
    }

    private static Double getClosingStock(Map<String, Double> purchasePlanMap, Double openPO, Double stockAfterConsumption, Double nextMonthObsoleteValue) {
        return stockAfterConsumption + openPO - purchasePlanMap.get(MaterialCategoryConstants.SAFETY_STOCK) - nextMonthObsoleteValue;
    }

    private static Double getFinalSuggestedOrder(Map<String, Double> purchasePlanMap, Double suggestedOrder, LocalDate fromDate, LocalDate toDate, LocalDate mpsDate) {
        if (DateUtils.checkStartDateValid(fromDate, mpsDate) && DateUtils.checkEndDateValid(toDate, mpsDate)) {
            return suggestedOrder * purchasePlanMap.get(MaterialCategoryConstants.ALLOCATION);
        }
        return 0.0;
    }


    private static Double getSuggestedOrder(Map<String, Double> purchasePlanMap, Double closingStock) {
        Double nextMonthConsumption = purchasePlanMap.get(MaterialCategoryConstants.NEXT_MONTH);
        double calculatedValue = closingStock - nextMonthConsumption;
        if (nextMonthConsumption == 0 && purchasePlanMap.get(MaterialCategoryConstants.SAFETY_STOCK) == 0) //if next month consumption and safety stock are 0 then suggested order also 0.
            return 0.0;
        else {
            if (calculatedValue >= 0)
                return 0.0;
            else
                return FormulasUtil.getValidQuantity(purchasePlanMap.get(MaterialCategoryConstants.MOQ), purchasePlanMap.get(MaterialCategoryConstants.PACK_SIZE), Math.ceil(Math.abs(calculatedValue)));
        }
    }

    private static Map<String, Double> getRMPurchasePlanMap(Double openingStock, Double currentMonthConsumption, Double nextMonthConsumption, RscIitRMItemWiseSupplierDetailsDTO rscIitRMItemWiseSupplierDetailsDTO) {
        Map<String, Double> purchasePlanMap = new HashMap<>();
        purchasePlanMap.put(MaterialCategoryConstants.OPENING_STOCK, openingStock);
        purchasePlanMap.put(MaterialCategoryConstants.CURRENT_MONTH, currentMonthConsumption);
        purchasePlanMap.put(MaterialCategoryConstants.NEXT_MONTH, nextMonthConsumption);
        purchasePlanMap.put(MaterialCategoryConstants.SAFETY_STOCK, rscIitRMItemWiseSupplierDetailsDTO.getSafetyStock());
        purchasePlanMap.put(MaterialCategoryConstants.PACK_SIZE, rscIitRMItemWiseSupplierDetailsDTO.getPackSize());
        purchasePlanMap.put(MaterialCategoryConstants.MOQ, rscIitRMItemWiseSupplierDetailsDTO.getMoqValue());
        purchasePlanMap.put(MaterialCategoryConstants.ALLOCATION, rscIitRMItemWiseSupplierDetailsDTO.getAllocation());
        return purchasePlanMap;
    }

    private static Double getValidOpenPO(LocalDate mpsDate, List<RscIotRmStockDTO> rscIotRmStockDTOs, int monthNumber) {
        Optional<RscIotRmStockDTO> optionalRscIotRmStockDTO = rscIotRmStockDTOs.stream()
                .filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_RM_OPEN_PO))
                .filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate, rscIotRmStockDTO.getStockDate()))
                .filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate.plusMonths(monthNumber), rscIotRmStockDTO.getArrivalDateForPo()))
                .findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }
}