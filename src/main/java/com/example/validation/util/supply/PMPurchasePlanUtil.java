package com.example.validation.util.supply;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.*;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.supply.*;
import com.example.validation.service.*;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class PMPurchasePlanUtil {
    private final RscMtItemService rscMtItemService;
    private final RscItItemWiseSupplierService rscItItemWiseSupplierService;
    private final RscItPMStockService rscItPMStockService;
    private final RscDtPMRemarkService rscDtPMRemarkService;
    private final RscItPMGrossConsumptionService rscItPMGrossConsumptionService;
    private final RscMtPpheaderService rscMtPpheaderService;

    public PMPurchasePlanUtil(RscMtItemService rscMtItemService, RscItItemWiseSupplierService rscItItemWiseSupplierService, RscItPMStockService rscItPMStockService, RscDtPMRemarkService rscDtPMRemarkService, RscItPMGrossConsumptionService rscItPMGrossConsumptionService, RscMtPpheaderService rscMtPpheaderService) {
        this.rscMtItemService = rscMtItemService;
        this.rscItItemWiseSupplierService = rscItItemWiseSupplierService;
        this.rscItPMStockService = rscItPMStockService;
        this.rscDtPMRemarkService = rscDtPMRemarkService;
        this.rscItPMGrossConsumptionService = rscItPMGrossConsumptionService;
        this.rscMtPpheaderService = rscMtPpheaderService;
    }

    public Long getRscDtPMRemarkId(RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO) {
        if (isEndOfLifeRemark(rscIotPMPurchasePlanDTO)) {
            return rscDtPMRemarkService.findRscDtPMRemarkIdByAliasName(MaterialCategoryConstants.END_OF_LIFE);
        } else {
            /*if (isLaunchRemark(rscIotPMPurchasePlanDTO)) {
                return rscDtPMRemarkService.findRscDtPMRemarkIdByAliasName(MaterialCategoryConstants.LAUNCH);
            }*/
            return rscDtPMRemarkService.findRscDtPMRemarkIdByAliasName(MaterialCategoryConstants.NONE);
        }
    }

    public RscIotPMPurchasePlanCalculationsDTO getRscIotPmSupplyCalculationsDTO(RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        RscIotPMPurchasePlanCalculationsDTO rscIotPMPurchasePlanCalculationsDTO = new RscIotPMPurchasePlanCalculationsDTO();
        RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO = rscItItemWiseSupplierService.findOneByRscMtItemIdAndAllocation(rscItGrossConsumptionDTO.getRscMtItemId());
        if (Optional.ofNullable(rscItItemWiseSupplierDTO).isPresent()) {
            rscIotPMPurchasePlanCalculationsDTO.setRscMtItemId(rscItGrossConsumptionDTO.getRscMtItemId());
            rscIotPMPurchasePlanCalculationsDTO.setRscMtPPheaderId(rscItGrossConsumptionDTO.getRscMtPPheaderId());
            rscIotPMPurchasePlanCalculationsDTO.setRscItPMGrossConsumptionId(rscItGrossConsumptionDTO.getId());
            rscIotPMPurchasePlanCalculationsDTO.setPriority(rscItGrossConsumptionDTO.getPriority());
            RscMtItemDTO rscMtItemDTO = rscMtItemService.findById(rscItGrossConsumptionDTO.getRscMtItemId());
            Optional<RscMtPPheaderDTO> mtPPHeaderDTO = rscMtPpheaderService.findOne(rscItGrossConsumptionDTO.getRscMtPPheaderId());
            List<RscItPMStockDTO> rscItPMStockDTOs = rscItPMStockService.findByRscMtItemId(rscItGrossConsumptionDTO.getRscMtItemId(), mtPPHeaderDTO.get().getMpsDate());
            Month1SupplyDTO month1SupplyDTO = PMPurchasePlanMonthUtil.getMonth1Supply(rscItPMStockDTOs, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO);
            MonthWiseSupplyDTO month2SupplyDTO = PMPurchasePlanMonthUtil.getMonth2Supply(month1SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month3SupplyDTO = PMPurchasePlanMonthUtil.getMonth3Supply(month2SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month4SupplyDTO = PMPurchasePlanMonthUtil.getMonth4Supply(month3SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month5SupplyDTO = PMPurchasePlanMonthUtil.getMonth5Supply(month4SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month6SupplyDTO = PMPurchasePlanMonthUtil.getMonth6Supply(month5SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month7SupplyDTO = PMPurchasePlanMonthUtil.getMonth7Supply(month6SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month8SupplyDTO = PMPurchasePlanMonthUtil.getMonth8Supply(month7SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month9SupplyDTO = PMPurchasePlanMonthUtil.getMonth9Supply(month8SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month10SupplyDTO = PMPurchasePlanMonthUtil.getMonth10Supply(month9SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month11SupplyDTO = PMPurchasePlanMonthUtil.getMonth11Supply(month10SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            MonthWiseSupplyDTO month12SupplyDTO = PMPurchasePlanMonthUtil.getMonth12Supply(month11SupplyDTO, rscItGrossConsumptionDTO, rscMtItemDTO, rscItItemWiseSupplierDTO, rscItPMStockDTOs);
            rscIotPMPurchasePlanCalculationsDTO.setMonth1SupplyDTO(month1SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth2SupplyDTO(month2SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth3SupplyDTO(month3SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth4SupplyDTO(month4SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth5SupplyDTO(month5SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth6SupplyDTO(month6SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth7SupplyDTO(month7SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth8SupplyDTO(month8SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth9SupplyDTO(month9SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth10SupplyDTO(month10SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth11SupplyDTO(month11SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setMonth12SupplyDTO(month12SupplyDTO);
            rscIotPMPurchasePlanCalculationsDTO.setRscItItemWiseSupplierId(rscItItemWiseSupplierDTO.getId());
            return rscIotPMPurchasePlanCalculationsDTO;
        }
        return null;
    }


    public PurchasePlanMainDTO getSupplyMainDTO(RscMtPPheaderDTO rscMtPPHeaderDTO, List<PurchasePlanDTO> purchasePlanDTOS) {
        PurchasePlanMainDTO purchasePlanMainDTO = new PurchasePlanMainDTO();
        if (Optional.ofNullable(rscMtPPHeaderDTO).isPresent()) {
            purchasePlanMainDTO.setMpsName(rscMtPPHeaderDTO.getMpsName());
            purchasePlanMainDTO.setMpsDate(rscMtPPHeaderDTO.getMpsDate());
            purchasePlanMainDTO.setItemCodes(purchasePlanDTOS);
        }
        return purchasePlanMainDTO;
    }

    private boolean isLaunchRemark(RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO) {
        LocalDate launchDate = rscIotPMPurchasePlanDTO.getLaunchDate();
        LocalDate mpsDate = rscIotPMPurchasePlanDTO.getMpsDate();
        if (Optional.ofNullable(launchDate).isPresent())
            if (launchDate.isAfter(mpsDate.minusDays(ApplicationConstants.ONE)))
                return launchDate.getMonth().equals(mpsDate.getMonth()) || launchDate.getMonth().equals(mpsDate.getMonth().plus(ApplicationConstants.ONE));
        return false;
    }

    private boolean isEndOfLifeRemark(RscIotPMPurchasePlanDTO rscIotPMPurchasePlanDTO) {
        RscItGrossConsumptionDTO rscItGrossConsumptionDTO = rscItPMGrossConsumptionService.findByRscMtPPheaderIdAndRscMtItemId(rscIotPMPurchasePlanDTO.getRscMtPPheaderId(), rscIotPMPurchasePlanDTO.getRscMtItemId());
        if (Optional.ofNullable(rscItGrossConsumptionDTO).isPresent())
            return (get2MonthTotalValue(rscItGrossConsumptionDTO) > 0) && (get10MonthTotalValue(rscItGrossConsumptionDTO) == 0);
        return false;
    }

    private Double get10MonthTotalValue(RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        return rscItGrossConsumptionDTO.getM3Value()
                + rscItGrossConsumptionDTO.getM4Value()
                + rscItGrossConsumptionDTO.getM5Value()
                + rscItGrossConsumptionDTO.getM6Value()
                + rscItGrossConsumptionDTO.getM7Value()
                + rscItGrossConsumptionDTO.getM8Value()
                + rscItGrossConsumptionDTO.getM9Value()
                + rscItGrossConsumptionDTO.getM10Value()
                + rscItGrossConsumptionDTO.getM11Value()
                + rscItGrossConsumptionDTO.getM12Value();
    }

    private Double get2MonthTotalValue(RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        return rscItGrossConsumptionDTO.getM1Value() + rscItGrossConsumptionDTO.getM2Value();
    }

    public String getPORemarkValue(List<RscMtPMOpenPoDTO> rscMtPMOpenPoDTOs) {
        StringBuilder poRemark = new StringBuilder(100);
        String prefix = "";
        for (RscMtPMOpenPoDTO rscMtPMOpenPoDTO : rscMtPMOpenPoDTOs) {
            poRemark.append(prefix);
            prefix = "; ";
            poRemark.append(getPORemarkString(rscMtPMOpenPoDTO, getTotalQuantity(rscMtPMOpenPoDTO)));
        }
        return poRemark.toString();
    }

    private String getPORemarkString(RscMtPMOpenPoDTO rscMtPMOpenPoDTO, int quantity) {
        return "PO : " + rscMtPMOpenPoDTO.getPoNumber() + " / " + quantity + " KG";
    }

    private int getTotalQuantity(RscMtPMOpenPoDTO rscMtPMOpenPoDTO) {
        return (int) (rscMtPMOpenPoDTO.getOrderedQuantity() - rscMtPMOpenPoDTO.getReceivedQuantity());
    }
}