package com.example.validation.util.supply;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscItItemWiseSupplierDTO;
import com.example.validation.dto.RscItPMStockDTO;
import com.example.validation.dto.RscMtItemDTO;
import com.example.validation.dto.consumption.RscItGrossConsumptionDTO;
import com.example.validation.dto.supply.Month1SupplyDTO;
import com.example.validation.dto.supply.MonthWiseSupplyDTO;
import com.example.validation.util.DateUtils;
import com.example.validation.util.DoubleUtils;
import com.example.validation.util.FormulasUtil;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

class PMPurchasePlanMonthUtil {

    static Month1SupplyDTO getMonth1Supply(List<RscItPMStockDTO> rscItPMStockDTOs, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO) {
        Double mesQuantity = getPMStockQuantityByType(rscItPMStockDTOs, MaterialCategoryConstants.MES_STOCK_TYPE);
        Double rtdQuantity = getPMStockQuantityByType(rscItPMStockDTOs, MaterialCategoryConstants.RTD_STOCK_TYPE);
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 0);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(mesQuantity, rtdQuantity, rscItGrossConsumptionDTO.getM1Value(), rscItGrossConsumptionDTO.getM2Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonth1SupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 1);
            Map<String, Double> supplyMap = getSupplyMap(mesQuantity, rtdQuantity, rscItGrossConsumptionDTO.getM1Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonth1SupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth2Supply(Month1SupplyDTO month1SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 1);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month1SupplyDTO.getClosingStock(), month1SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM2Value(), rscItGrossConsumptionDTO.getM3Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 2);
            Map<String, Double> supplyMap = getSupplyMap(month1SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM2Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth3Supply(MonthWiseSupplyDTO month2SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 2);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month2SupplyDTO.getClosingStock(), month2SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM3Value(), rscItGrossConsumptionDTO.getM4Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 3);
            Map<String, Double> supplyMap = getSupplyMap(month2SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM3Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth4Supply(MonthWiseSupplyDTO month3SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 3);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month3SupplyDTO.getClosingStock(), month3SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM4Value(), rscItGrossConsumptionDTO.getM5Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 4);
            Map<String, Double> supplyMap = getSupplyMap(month3SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM4Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth5Supply(MonthWiseSupplyDTO month4SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 4);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month4SupplyDTO.getClosingStock(), month4SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM5Value(), rscItGrossConsumptionDTO.getM6Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 5);
            Map<String, Double> supplyMap = getSupplyMap(month4SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM5Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth6Supply(MonthWiseSupplyDTO month5SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 5);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month5SupplyDTO.getClosingStock(), month5SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM6Value(), rscItGrossConsumptionDTO.getM7Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 6);
            Map<String, Double> supplyMap = getSupplyMap(month5SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM6Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth7Supply(MonthWiseSupplyDTO month6SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 6);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month6SupplyDTO.getClosingStock(), month6SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM7Value(), rscItGrossConsumptionDTO.getM8Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 7);
            Map<String, Double> supplyMap = getSupplyMap(month6SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM7Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth8Supply(MonthWiseSupplyDTO month7SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 7);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month7SupplyDTO.getClosingStock(), month7SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM8Value(), rscItGrossConsumptionDTO.getM9Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 8);
            Map<String, Double> supplyMap = getSupplyMap(month7SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM8Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth9Supply(MonthWiseSupplyDTO month8SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 8);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month8SupplyDTO.getClosingStock(), month8SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM9Value(), rscItGrossConsumptionDTO.getM10Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 9);
            Map<String, Double> supplyMap = getSupplyMap(month8SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM9Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth10Supply(MonthWiseSupplyDTO month9SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 9);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month9SupplyDTO.getClosingStock(), month9SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM10Value(), rscItGrossConsumptionDTO.getM11Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 10);
            Map<String, Double> supplyMap = getSupplyMap(month9SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM10Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth11Supply(MonthWiseSupplyDTO month10SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 10);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month10SupplyDTO.getClosingStock(), month10SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM11Value(), rscItGrossConsumptionDTO.getM12Value(), rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 11);
            Map<String, Double> supplyMap = getSupplyMap(month10SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM11Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    static MonthWiseSupplyDTO getMonth12Supply(MonthWiseSupplyDTO month11SupplyDTO, RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO, List<RscItPMStockDTO> rscItPMStockDTOs) {
        Double openPO = getValidOpenPO(rscItGrossConsumptionDTO.getMpsDate(), rscItPMStockDTOs, 11);
        if (rscMtItemDTO.getCoverDays() == 30) {
            Map<String, Double> supplyMap = getSupplyMap(month11SupplyDTO.getClosingStock(), month11SupplyDTO.getFinalSupply(), rscItGrossConsumptionDTO.getM12Value(), 0.0, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getMonthWiseSupplyDTO(supplyMap, openPO);
        } else {
            Double nextMonthConsumption = getValidNextMonthConsumption(rscItGrossConsumptionDTO, rscMtItemDTO, 12);
            Map<String, Double> supplyMap = getSupplyMap(month11SupplyDTO.getNextMonthOpeningStock(), 0.0, rscItGrossConsumptionDTO.getM12Value(), nextMonthConsumption, rscMtItemDTO, rscItItemWiseSupplierDTO);
            return getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        }
    }

    private static Map<String, Double> getSupplyMap(Double mesQuantity, Double rtdQuantity, Double currentMonth, Double nextMonth, RscMtItemDTO rscMtItemDTO, RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO) {
        Map<String, Double> supplyMap = new HashMap<>();
        supplyMap.put(MaterialCategoryConstants.MES_STOCK_TYPE, mesQuantity);
        supplyMap.put(MaterialCategoryConstants.RTD_STOCK_TYPE, rtdQuantity);
        supplyMap.put(MaterialCategoryConstants.CURRENT_MONTH, currentMonth);
        supplyMap.put(MaterialCategoryConstants.NEXT_MONTH, nextMonth);
        supplyMap.put(MaterialCategoryConstants.STOCK_VALUE, getValidStockValue(rscMtItemDTO));
        supplyMap.put(MaterialCategoryConstants.MOQ, getValidMoqValue(rscItItemWiseSupplierDTO));
        supplyMap.put(MaterialCategoryConstants.TS, getValidTSValue(rscItItemWiseSupplierDTO));
        return supplyMap;
    }

    private static Double getValidStockValue(RscMtItemDTO rscMtItemDTO) {
        if (Optional.ofNullable(rscMtItemDTO.getStockValue()).isPresent())
            return rscMtItemDTO.getStockValue();
        else
            return 0.0;
    }

    private static Double getValidMoqValue(RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO) {
        if (Optional.ofNullable(rscItItemWiseSupplierDTO).isPresent())
            return rscItItemWiseSupplierDTO.getMoqValue();
        else
            return 0.0;
    }

    private static Double getValidTSValue(RscItItemWiseSupplierDTO rscItItemWiseSupplierDTO) {
        if (Optional.ofNullable(rscItItemWiseSupplierDTO).isPresent())
            return rscItItemWiseSupplierDTO.getSeriesValue();
        else
            return 0.0;
    }

    private static MonthWiseSupplyDTO getMonthWiseSupplyDTO(Map<String, Double> supplyMap, Double openPO) {
        MonthWiseSupplyDTO monthWiseSupplyDTO = new MonthWiseSupplyDTO();
        Double stockAfterConsumption = getStockAfterConsumption(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH)) + openPO;
        Double closingStock = getClosingStock(supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE), stockAfterConsumption);
        Double suggestedOrder = getSuggestedOrder(supplyMap.get(MaterialCategoryConstants.NEXT_MONTH), closingStock, supplyMap.get(MaterialCategoryConstants.STOCK_VALUE), supplyMap.get(MaterialCategoryConstants.MOQ), supplyMap.get(MaterialCategoryConstants.TS));
        monthWiseSupplyDTO.setMonthEndStock(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE));
        monthWiseSupplyDTO.setReceiptTillDate(supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE));
        monthWiseSupplyDTO.setStockAfterConsumption(stockAfterConsumption);
        monthWiseSupplyDTO.setClosingStock(closingStock);
        monthWiseSupplyDTO.setFinalSupply(suggestedOrder);
        monthWiseSupplyDTO.setOpenPoStock(openPO);
        return monthWiseSupplyDTO;
    }

    private static Month1SupplyDTO getMonth1SupplyDTO(Map<String, Double> supplyMap, Double openPO) {
        Month1SupplyDTO month1SupplyDTO = new Month1SupplyDTO();
        Double stockAfterConsumption = getStockAfterConsumption(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH)) + openPO;
        Double closingStock = getClosingStock(supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE), stockAfterConsumption);
        Double suggestedOrder = getSuggestedOrder(supplyMap.get(MaterialCategoryConstants.NEXT_MONTH), closingStock, supplyMap.get(MaterialCategoryConstants.STOCK_VALUE), supplyMap.get(MaterialCategoryConstants.MOQ), supplyMap.get(MaterialCategoryConstants.TS));
        Double onHandStock = getOnHandStock(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE));
        Double priority = getPriorityValue(onHandStock, supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH), supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.STOCK_VALUE), supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.MOQ), supplyMap.get(MaterialCategoryConstants.TS));
        Double nextMonthOpeningStock = getNextMonthOpeningStock(stockAfterConsumption, priority);
        month1SupplyDTO.setFinalSupply(getActualSuggestedOrder(suggestedOrder, nextMonthOpeningStock, supplyMap.get(MaterialCategoryConstants.NEXT_MONTH), supplyMap.get(MaterialCategoryConstants.MOQ), supplyMap.get(MaterialCategoryConstants.TS), supplyMap.get(MaterialCategoryConstants.STOCK_VALUE)));
        month1SupplyDTO.setClosingStock(nextMonthOpeningStock);
        month1SupplyDTO.setMonthEndStock(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE));
        month1SupplyDTO.setOnHandStock(onHandStock);
        month1SupplyDTO.setPrioritySupply(priority);
        month1SupplyDTO.setReceiptTillDate(supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE));
        month1SupplyDTO.setStockAfterConsumption(stockAfterConsumption);
        month1SupplyDTO.setSuggestedSupply(suggestedOrder);
        month1SupplyDTO.setOpenPoStock(openPO);
        return month1SupplyDTO;
    }

    private static Double getActualSuggestedOrder(Double suggestedOrder, Double nextMonthOpeningStock, Double nextMonthConsumption, Double moqValue, Double seriesValue, Double safetyStock) {
        if (suggestedOrder > nextMonthConsumption) {
            if (nextMonthConsumption < nextMonthOpeningStock) {
                return 0.0;
            } else {
                if ((nextMonthConsumption - nextMonthOpeningStock) <= moqValue) {
                    return moqValue;
                } else {
                    return FormulasUtil.getValidQuantity(moqValue, seriesValue, (nextMonthConsumption + safetyStock - nextMonthOpeningStock));
                }
            }
        } else {
            return suggestedOrder;
        }
    }

    /*
    =IF((IF(C14-(C8+B3)>0,0,(C8+B3)-C14))>0,CEILING((C11+C8-C14+B3),B2),C11)
    =IF((IF(C14-(C8+B3)>0,0,(C8+B3)-C14))>0,IF((C11+C8-C14+B3)<B1,B1,CEILING((C11+C8-C14+B3),B2)),IF(C11>B1,C11,CEILING(C11,B1)))
     */
    private static Double getPriorityValue(Double onHandStock, Double grossConsumptionQuantity, Double mesQuantity, Double safetyStock, Double rtdQuantity, Double moqValue, Double seriesValue) {
        double priorityOne = onHandStock - (grossConsumptionQuantity + safetyStock);
        double priorityTwo = (grossConsumptionQuantity + safetyStock) - onHandStock;
        double priorityThree = rtdQuantity + grossConsumptionQuantity - onHandStock + safetyStock;
        if (priorityOne >= 0 && rtdQuantity == 0) {
            return 0.0;
        } else if (priorityTwo > 0 && (priorityTwo > rtdQuantity || priorityThree > rtdQuantity)) {
            return FormulasUtil.getValidQuantity(moqValue, seriesValue, priorityThree);
        } else {
            if (rtdQuantity > moqValue)
                return rtdQuantity;
            else
                return FormulasUtil.getValidQuantity(moqValue, seriesValue, rtdQuantity);
        }
    }

    private static Double getOnHandStock(Double mesQuantity, Double rtdQuantity) {
        return mesQuantity + rtdQuantity;
    }

    private static Double getSuggestedOrder(Double nextMonthConsumption, Double closingStock, Double safetyStock, Double moqValue, Double seriesValue) {
        Double orderOne = (nextMonthConsumption - closingStock) + safetyStock;
        if (orderOne <= 0.0) {
            return 0.0;
        } else {
            return FormulasUtil.getValidQuantity(moqValue, seriesValue, orderOne);
        }
    }

    private static Double getStockAfterConsumption(Double mesQuantity, Double grossConsumptionQuantity) {
        return DoubleUtils.getFormattedValue(mesQuantity - grossConsumptionQuantity);
    }

    private static Double getClosingStock(Double rtdQuantity, Double stockAfterConsumption) {
        return DoubleUtils.getFormattedValue(stockAfterConsumption + rtdQuantity);
    }

    private static Double getOtherClosingStock(Double suggestedOrder, Double mesQuantity, Double currentMonthConsumption) {
        return DoubleUtils.getFormattedValue(suggestedOrder + mesQuantity - currentMonthConsumption);
    }

    private static Double getNextMonthOpeningStock(Double stockAfterConsumption, Double priority) {
        return DoubleUtils.getFormattedValue(stockAfterConsumption + priority);
    }

    public static Map<Integer, Double> getAllMonthMap(RscItGrossConsumptionDTO rscItGrossConsumptionDTO) {
        Map<Integer, Double> allMonthConsumption = new HashMap<>();
        allMonthConsumption.put(1, rscItGrossConsumptionDTO.getM1Value());
        allMonthConsumption.put(2, rscItGrossConsumptionDTO.getM2Value());
        allMonthConsumption.put(3, rscItGrossConsumptionDTO.getM3Value());
        allMonthConsumption.put(4, rscItGrossConsumptionDTO.getM4Value());
        allMonthConsumption.put(5, rscItGrossConsumptionDTO.getM5Value());
        allMonthConsumption.put(6, rscItGrossConsumptionDTO.getM6Value());
        allMonthConsumption.put(7, rscItGrossConsumptionDTO.getM7Value());
        allMonthConsumption.put(8, rscItGrossConsumptionDTO.getM8Value());
        allMonthConsumption.put(9, rscItGrossConsumptionDTO.getM9Value());
        allMonthConsumption.put(10, rscItGrossConsumptionDTO.getM10Value());
        allMonthConsumption.put(11, rscItGrossConsumptionDTO.getM11Value());
        allMonthConsumption.put(12, rscItGrossConsumptionDTO.getM12Value());
        return allMonthConsumption;
    }

    private static Month1SupplyDTO getOtherMonth1SupplyDTO(Map<String, Double> supplyMap, Double openPO) {
        Month1SupplyDTO month1SupplyDTO = new Month1SupplyDTO();
        MonthWiseSupplyDTO monthWiseSupplyDTO = getOtherMonthWiseSupplyDTO(supplyMap, openPO);
        month1SupplyDTO.setClosingStock(monthWiseSupplyDTO.getClosingStock());
        month1SupplyDTO.setMonthEndStock(monthWiseSupplyDTO.getMonthEndStock());
        month1SupplyDTO.setStockAfterConsumption(monthWiseSupplyDTO.getStockAfterConsumption());
        month1SupplyDTO.setReceiptTillDate(monthWiseSupplyDTO.getReceiptTillDate());
        Double nextMonthOpeningStock = monthWiseSupplyDTO.getNextMonthOpeningStock();
        month1SupplyDTO.setOpenPoStock(openPO);
        if (monthWiseSupplyDTO.getReceiptTillDate() > monthWiseSupplyDTO.getFinalSupply()) {
            month1SupplyDTO.setFinalSupply(monthWiseSupplyDTO.getReceiptTillDate());
            nextMonthOpeningStock = getNextMonthOpeningStock(monthWiseSupplyDTO.getReceiptTillDate(), supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH));
        } else {
            month1SupplyDTO.setFinalSupply(monthWiseSupplyDTO.getFinalSupply());
        }
        month1SupplyDTO.setNextMonthOpeningStock(nextMonthOpeningStock);
        return month1SupplyDTO;
    }

    private static MonthWiseSupplyDTO getOtherMonthWiseSupplyDTO(Map<String, Double> supplyMap, Double openPO) {
        MonthWiseSupplyDTO monthWiseSupplyDTO = new MonthWiseSupplyDTO();
        Double stockAfterConsumption = getStockAfterConsumption(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH)) + openPO;
        Double suggestedOrder = getSuggestedOrder(supplyMap.get(MaterialCategoryConstants.NEXT_MONTH), supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.STOCK_VALUE), supplyMap.get(MaterialCategoryConstants.MOQ), supplyMap.get(MaterialCategoryConstants.TS));
        Double closingStock = getOtherClosingStock(suggestedOrder, supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH));
        Double nextMonthOpeningStock = getNextMonthOpeningStock(suggestedOrder, supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE), supplyMap.get(MaterialCategoryConstants.CURRENT_MONTH));
        monthWiseSupplyDTO.setMonthEndStock(supplyMap.get(MaterialCategoryConstants.MES_STOCK_TYPE));
        monthWiseSupplyDTO.setReceiptTillDate(supplyMap.get(MaterialCategoryConstants.RTD_STOCK_TYPE));
        monthWiseSupplyDTO.setStockAfterConsumption(stockAfterConsumption);
        monthWiseSupplyDTO.setClosingStock(closingStock);
        monthWiseSupplyDTO.setFinalSupply(suggestedOrder);
        monthWiseSupplyDTO.setNextMonthOpeningStock(nextMonthOpeningStock);
        monthWiseSupplyDTO.setOpenPoStock(openPO);
        return monthWiseSupplyDTO;
    }

    private static Double getNextMonthOpeningStock(Double suggestedOrder, Double openingStock, Double currentMonthConsumption) {
        return suggestedOrder + openingStock - currentMonthConsumption;
    }

    private static Double getValidNextMonthConsumption(RscItGrossConsumptionDTO rscItGrossConsumptionDTO, RscMtItemDTO rscMtItemDTO, int currentMonthIndex) {
        Map<Integer, Double> allMonthConsumption = getAllMonthMap(rscItGrossConsumptionDTO);
        Double coverMonthConsumption = 0.0;
        Double coverDays = rscMtItemDTO.getCoverDays();
        int lastMonthIndex = ((int) (coverDays / 30)) + currentMonthIndex - 1;
        for (int i = currentMonthIndex; i <= lastMonthIndex; i++) {
            if (i <= 12) {
                coverMonthConsumption += allMonthConsumption.get(i);
            }
        }
        return coverMonthConsumption + getCoverDaysConsumption(coverDays, allMonthConsumption.get(lastMonthIndex + 1));
    }

    private static Double getCoverDaysConsumption(Double coverDays, Double lastMonthConsumption) {
        if (Optional.ofNullable(lastMonthConsumption).isPresent()) {
            double remainder = coverDays % 30;
            if (remainder > 0) {
                double remainingDays = 30 / remainder;
                return DoubleUtils.getFormattedValue(lastMonthConsumption / remainingDays);
            }
        }
        return 0.0;
    }

    private static Double getPMStockQuantityByType(List<RscItPMStockDTO> rscItPMStockDTOs, String stockType) {
        for (RscItPMStockDTO rscItPMStockDTO : rscItPMStockDTOs) {
            if (Optional.ofNullable(rscItPMStockDTO).isPresent()) {
                if (rscItPMStockDTO.getStockTypeAlias().equals(stockType)) {
                    return rscItPMStockDTO.getQuantity();
                }
            }
        }
        return 0.0;
    }

    private static Double getValidOpenPO(LocalDate mpsDate, List<RscItPMStockDTO> rscItPMStockDTOs, int monthNumber) {
        Optional<RscItPMStockDTO> optionalRscIotRmStockDTO = rscItPMStockDTOs.stream()
                .filter(rscIotRmStockDTO -> rscIotRmStockDTO.getStockTypeTag().equals(MaterialCategoryConstants.STOCK_TYPE_TAG_PM_OPEN_PO))
                .filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate, rscIotRmStockDTO.getStockDate()))
                .filter(rscIotRmStockDTO -> DateUtils.isEqualMonthAndYear(mpsDate.plusMonths(monthNumber), rscIotRmStockDTO.getArrivalDateForPo()))
                .findAny();
        if (optionalRscIotRmStockDTO.isPresent())
            return optionalRscIotRmStockDTO.get().getQuantity();
        return 0.0;
    }
}