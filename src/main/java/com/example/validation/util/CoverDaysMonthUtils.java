package com.example.validation.util;

import com.example.validation.dto.*;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class CoverDaysMonthUtils {
    public List getCoverDaysCalculation(Double totalStockSitValue, Double[] monthsConsumptionTotalValue, Integer[] monthsCoverDays, Optional<RscMtPPheaderDTO> pPheaderDTO) {
        List coverDaysCalculatedList = new ArrayList();
        if (pPheaderDTO.isPresent()) {
            LocalDate mpsDate = pPheaderDTO.get().getMpsDate();
            if (totalStockSitValue != null) {
                boolean prevMonthsConsumptionStatus = true;
                Double stockAfterMonth1Consumption = getStockAfterMonthConsumption(totalStockSitValue, monthsConsumptionTotalValue[0]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(totalStockSitValue, prevMonthsConsumptionStatus);
                monthsCoverDays[0] = getMonthsCoverDays(totalStockSitValue, monthsConsumptionTotalValue[0], stockAfterMonth1Consumption, mpsDate, prevMonthsConsumptionStatus);
                Double stockAfterMonth2Consumption = getStockAfterMonthConsumption(stockAfterMonth1Consumption, monthsConsumptionTotalValue[1]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth1Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[1] = getMonthsCoverDays(stockAfterMonth1Consumption, monthsConsumptionTotalValue[1], stockAfterMonth2Consumption, mpsDate.plusMonths(1), prevMonthsConsumptionStatus);
                Double stockAfterMonth3Consumption = getStockAfterMonthConsumption(stockAfterMonth2Consumption, monthsConsumptionTotalValue[2]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth2Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[2] = getMonthsCoverDays(stockAfterMonth2Consumption, monthsConsumptionTotalValue[2], stockAfterMonth3Consumption, mpsDate.plusMonths(2), prevMonthsConsumptionStatus);
                Double stockAfterMonth4Consumption = getStockAfterMonthConsumption(stockAfterMonth3Consumption, monthsConsumptionTotalValue[3]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth3Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[3] = getMonthsCoverDays(stockAfterMonth3Consumption, monthsConsumptionTotalValue[3], stockAfterMonth4Consumption, mpsDate.plusMonths(3), prevMonthsConsumptionStatus);
                Double stockAfterMonth5Consumption = getStockAfterMonthConsumption(stockAfterMonth4Consumption, monthsConsumptionTotalValue[4]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth4Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[4] = getMonthsCoverDays(stockAfterMonth4Consumption, monthsConsumptionTotalValue[4], stockAfterMonth5Consumption, mpsDate.plusMonths(4), prevMonthsConsumptionStatus);
                Double stockAfterMonth6Consumption = getStockAfterMonthConsumption(stockAfterMonth5Consumption, monthsConsumptionTotalValue[5]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth5Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[5] = getMonthsCoverDays(stockAfterMonth5Consumption, monthsConsumptionTotalValue[5], stockAfterMonth6Consumption, mpsDate.plusMonths(5), prevMonthsConsumptionStatus);
                Double stockAfterMonth7Consumption = getStockAfterMonthConsumption(stockAfterMonth6Consumption, monthsConsumptionTotalValue[6]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth6Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[6] = getMonthsCoverDays(stockAfterMonth6Consumption, monthsConsumptionTotalValue[6], stockAfterMonth7Consumption, mpsDate.plusMonths(6), prevMonthsConsumptionStatus);
                Double stockAfterMonth8Consumption = getStockAfterMonthConsumption(stockAfterMonth7Consumption, monthsConsumptionTotalValue[7]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth7Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[7] = getMonthsCoverDays(stockAfterMonth7Consumption, monthsConsumptionTotalValue[7], stockAfterMonth8Consumption, mpsDate.plusMonths(7), prevMonthsConsumptionStatus);
                Double stockAfterMonth9Consumption = getStockAfterMonthConsumption(stockAfterMonth8Consumption, monthsConsumptionTotalValue[8]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth8Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[8] = getMonthsCoverDays(stockAfterMonth8Consumption, monthsConsumptionTotalValue[8], stockAfterMonth9Consumption, mpsDate.plusMonths(8), prevMonthsConsumptionStatus);
                Double stockAfterMonth10Consumption = getStockAfterMonthConsumption(stockAfterMonth9Consumption, monthsConsumptionTotalValue[9]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth9Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[9] = getMonthsCoverDays(stockAfterMonth9Consumption, monthsConsumptionTotalValue[9], stockAfterMonth10Consumption, mpsDate.plusMonths(9), prevMonthsConsumptionStatus);
                Double stockAfterMonth11Consumption = getStockAfterMonthConsumption(stockAfterMonth10Consumption, monthsConsumptionTotalValue[10]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth10Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[10] = getMonthsCoverDays(stockAfterMonth10Consumption, monthsConsumptionTotalValue[10], stockAfterMonth11Consumption, mpsDate.plusMonths(10), prevMonthsConsumptionStatus);
                Double stockAfterMonth12Consumption = getStockAfterMonthConsumption(stockAfterMonth11Consumption, monthsConsumptionTotalValue[11]);
                prevMonthsConsumptionStatus = checkPreviousMonthConsumption(stockAfterMonth11Consumption, prevMonthsConsumptionStatus);
                monthsCoverDays[11] = getMonthsCoverDays(stockAfterMonth11Consumption, monthsConsumptionTotalValue[11], stockAfterMonth12Consumption, mpsDate.plusMonths(11), prevMonthsConsumptionStatus);
            }
            coverDaysCalculatedList.add(totalStockSitValue);
            coverDaysCalculatedList.add(monthsCoverDays);
        }
        return coverDaysCalculatedList;
    }

    private static boolean checkPreviousMonthConsumption(Double lastMonthStockAfterMonthConsumption, boolean prevMonthsConsumptionStatus) {
        if (prevMonthsConsumptionStatus) if (lastMonthStockAfterMonthConsumption >= 0) return true;
        return false;
    }

    private static Integer getMonthsCoverDays(Double lastMonthStockAfterMonthConsumption, Double currentMonthsConsumptionTotalValue, Double currentMonthStockAfterMonthConsumption, LocalDate mpsDate, boolean prevMonthsConsumptionStatus) {
        if (prevMonthsConsumptionStatus) {
            if (currentMonthStockAfterMonthConsumption >= 0) {
                return DateUtils.getDaysOfMonth(mpsDate);
            } else {
                return getMonthCoverDaysValue(lastMonthStockAfterMonthConsumption, currentMonthsConsumptionTotalValue);
            }
        }
        return 0;
    }

    private static double getStockAfterMonthConsumption(Double lastMonthStockAfterMonthConsumption, Double currentMonthsConsumptionTotalValue) {
        return lastMonthStockAfterMonthConsumption - currentMonthsConsumptionTotalValue;
    }

    private static Integer getMonthCoverDaysValue(Double totalStockSitValue, Double monthsConsumptionTotalValue) {
        return ((int) Math.round((totalStockSitValue / monthsConsumptionTotalValue) * 30));
    }

    public CoverDaysInMonthDTO getCoverDaysOfAllMonths(LocalDate mpsDate) {
        CoverDaysInMonthDTO coverDaysInMonthDTO = new CoverDaysInMonthDTO();
        coverDaysInMonthDTO.setMonth1CoverDays(DateUtils.getDaysOfMonth(mpsDate));
        coverDaysInMonthDTO.setMonth2CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(1)));
        coverDaysInMonthDTO.setMonth3CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(2)));
        coverDaysInMonthDTO.setMonth4CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(3)));
        coverDaysInMonthDTO.setMonth5CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(4)));
        coverDaysInMonthDTO.setMonth6CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(5)));
        coverDaysInMonthDTO.setMonth7CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(6)));
        coverDaysInMonthDTO.setMonth8CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(7)));
        coverDaysInMonthDTO.setMonth9CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(8)));
        coverDaysInMonthDTO.setMonth10CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(9)));
        coverDaysInMonthDTO.setMonth11CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(10)));
        coverDaysInMonthDTO.setMonth12CoverDays(DateUtils.getDaysOfMonth(mpsDate.plusMonths(11)));
        return coverDaysInMonthDTO;
    }

    public Integer getTotalDaysOfAllMonths(LocalDate mpsDate) {
        Integer totalDaysOfMonths = 0;
        for (int i = 0; i < 12; i++) {
            totalDaysOfMonths += DateUtils.getDaysOfMonth(mpsDate.plusMonths(i));
        }
        return totalDaysOfMonths;
    }

    public RscIotRmCoverDaysSummaryDTO getRmCoverDaysWhenSitValueZero(List<RscIotRmCoverDaysDTO> rscItRmCoverDaysLists, RscIotRmCoverDaysSummaryDTO rscIotRmCoverDaysSummaryDTO, Optional<RscMtPPheaderDTO> pPheaderDTO, Double sitValue, RscIotRmSlobSummaryDTO rscIotRmSlobSummaryDTO) {
        Double totalStockValue = 0.0;
        Double totalStockSitValue = 0.0;
        Double[] monthsConsumptionTotalValue = new Double[13];
        for (int i = 0; i < monthsConsumptionTotalValue.length; i++) {
            monthsConsumptionTotalValue[i] = 0.0;
        }
        Integer[] monthsCoverDays = new Integer[12];
        for (int i = 0; i < monthsCoverDays.length; i++) {
            monthsCoverDays[i] = 0;
        }
        for (RscIotRmCoverDaysDTO rscItRmCoverDays : rscItRmCoverDaysLists) {
            totalStockValue += getStockValue(rscItRmCoverDays);
            monthsConsumptionTotalValue[0] += rscItRmCoverDays.getMonth1ConsumptionValue();
            monthsConsumptionTotalValue[1] += rscItRmCoverDays.getMonth2ConsumptionValue();
            monthsConsumptionTotalValue[2] += rscItRmCoverDays.getMonth3ConsumptionValue();
            monthsConsumptionTotalValue[3] += rscItRmCoverDays.getMonth4ConsumptionValue();
            monthsConsumptionTotalValue[4] += rscItRmCoverDays.getMonth5ConsumptionValue();
            monthsConsumptionTotalValue[5] += rscItRmCoverDays.getMonth6ConsumptionValue();
            monthsConsumptionTotalValue[6] += rscItRmCoverDays.getMonth7ConsumptionValue();
            monthsConsumptionTotalValue[7] += rscItRmCoverDays.getMonth8ConsumptionValue();
            monthsConsumptionTotalValue[8] += rscItRmCoverDays.getMonth9ConsumptionValue();
            monthsConsumptionTotalValue[9] += rscItRmCoverDays.getMonth10ConsumptionValue();
            monthsConsumptionTotalValue[10] += rscItRmCoverDays.getMonth11ConsumptionValue();
            monthsConsumptionTotalValue[11] += rscItRmCoverDays.getMonth12ConsumptionValue();
            monthsConsumptionTotalValue[12] += rscItRmCoverDays.getAllMonthsConsumptionValue();
        }
        if (totalStockValue != null) {
            totalStockSitValue = totalStockValue + sitValue;
        }
        List coverDaysCalculatedList = getCoverDaysCalculation(totalStockSitValue, monthsConsumptionTotalValue, monthsCoverDays, pPheaderDTO);
        totalStockSitValue = (Double) coverDaysCalculatedList.get(0);
        monthsCoverDays = (Integer[]) coverDaysCalculatedList.get(1);
        rscIotRmCoverDaysSummaryDTO.setTotalStockValue(DoubleUtils.round(totalStockValue));
        rscIotRmCoverDaysSummaryDTO.setSitValue(DoubleUtils.round(sitValue));
        rscIotRmCoverDaysSummaryDTO.setTotalStockSitValue(DoubleUtils.round(totalStockSitValue));
        rscIotRmCoverDaysSummaryDTO.setMonth1ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[0]));
        rscIotRmCoverDaysSummaryDTO.setMonth2ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[1]));
        rscIotRmCoverDaysSummaryDTO.setMonth3ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[2]));
        rscIotRmCoverDaysSummaryDTO.setMonth4ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[3]));
        rscIotRmCoverDaysSummaryDTO.setMonth5ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[4]));
        rscIotRmCoverDaysSummaryDTO.setMonth6ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[5]));
        rscIotRmCoverDaysSummaryDTO.setMonth7ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[6]));
        rscIotRmCoverDaysSummaryDTO.setMonth8ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[7]));
        rscIotRmCoverDaysSummaryDTO.setMonth9ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[8]));
        rscIotRmCoverDaysSummaryDTO.setMonth10ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[9]));
        rscIotRmCoverDaysSummaryDTO.setMonth11ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[10]));
        rscIotRmCoverDaysSummaryDTO.setMonth12ConsumptionTotalValue(DoubleUtils.round(monthsConsumptionTotalValue[11]));
        rscIotRmCoverDaysSummaryDTO.setAllMonthsTotalConsumptionValue(DoubleUtils.round(monthsConsumptionTotalValue[12]));
        rscIotRmCoverDaysSummaryDTO.setMonth1CoverDays(monthsCoverDays[0]);
        rscIotRmCoverDaysSummaryDTO.setMonth2CoverDays(monthsCoverDays[1]);
        rscIotRmCoverDaysSummaryDTO.setMonth3CoverDays(monthsCoverDays[2]);
        rscIotRmCoverDaysSummaryDTO.setMonth4CoverDays(monthsCoverDays[3]);
        rscIotRmCoverDaysSummaryDTO.setMonth5CoverDays(monthsCoverDays[4]);
        rscIotRmCoverDaysSummaryDTO.setMonth6CoverDays(monthsCoverDays[5]);
        rscIotRmCoverDaysSummaryDTO.setMonth7CoverDays(monthsCoverDays[6]);
        rscIotRmCoverDaysSummaryDTO.setMonth8CoverDays(monthsCoverDays[7]);
        rscIotRmCoverDaysSummaryDTO.setMonth9CoverDays(monthsCoverDays[8]);
        rscIotRmCoverDaysSummaryDTO.setMonth10CoverDays(monthsCoverDays[9]);
        rscIotRmCoverDaysSummaryDTO.setMonth11CoverDays(monthsCoverDays[10]);
        rscIotRmCoverDaysSummaryDTO.setMonth12CoverDays(monthsCoverDays[11]);
        if (sitValue == 0.0) {
            rscIotRmCoverDaysSummaryDTO.setTotalCoverDaysWithoutSit(getCoverDaysTotal(monthsCoverDays));
        }
        rscIotRmCoverDaysSummaryDTO.setTotalCoverDaysWithSit(getCoverDaysTotal(monthsCoverDays));
        return rscIotRmCoverDaysSummaryDTO;
    }

    private Integer getCoverDaysTotal(Integer[] monthsCoverDays) {
        Integer coverDaysTotal = 0;
        for (int i = 0; i < monthsCoverDays.length; i++) {
            coverDaysTotal += monthsCoverDays[i];
        }
        return coverDaysTotal;
    }

    private Double getStockValue(RscIotRmCoverDaysDTO rscItRmCoverDaysDTO) {
        if (rscItRmCoverDaysDTO.getStockValue() != null) {
            return rscItRmCoverDaysDTO.getStockValue();
        }
        return 0.0;
    }
}
