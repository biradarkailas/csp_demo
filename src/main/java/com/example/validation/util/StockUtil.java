package com.example.validation.util;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.IndividualValuesMesDTO;
import com.example.validation.dto.IndividualValuesRtdDTO;
import com.example.validation.dto.MesDTO;
import com.example.validation.dto.RtdDTO;

import java.time.LocalDate;
import java.util.List;
import java.util.stream.Collectors;

public class StockUtil {
    public static List<IndividualValuesMesDTO> getIndividualMesValuesList(List<IndividualValuesMesDTO> individualValuesMesDTOList, LocalDate mpsDate) {
        return individualValuesMesDTOList.stream()
                .filter(individualValuesMesDTO -> DateUtils.isEqualMonthAndYear(individualValuesMesDTO.getStockDate(), mpsDate)
                        && !individualValuesMesDTO.getStockStatus().equals("R")).collect(Collectors.toList());
    }

    public static List<IndividualValuesRtdDTO> getIndividualRtdValuesList(List<IndividualValuesRtdDTO> indivisualValuesRtdDTOList, LocalDate mpsDate) {
        return indivisualValuesRtdDTOList.stream()
                .filter(individualValuesRtdDTO -> DateUtils.isEqualMonthAndYear(individualValuesRtdDTO.getReceiptDate(), mpsDate))
                .collect(Collectors.toList());
    }

    private static Double getRscMesTotalQuantity(List<IndividualValuesMesDTO> individualMesValuesList, LocalDate mpsDate) {
        double totalQuantityMes = 0;
        for (IndividualValuesMesDTO indivisualValuesMesDTO : individualMesValuesList) {
            boolean isDateMatches = DateUtils.isEqualMonthAndYear(indivisualValuesMesDTO.getStockDate(), mpsDate);
            if (isDateMatches) {
                totalQuantityMes += indivisualValuesMesDTO.getValue();
            }
        }
        return DoubleUtils.getFormattedValue(totalQuantityMes);
    }

    private static Double getRscMtRmRtdTotalQuantity(List<IndividualValuesRtdDTO> individualRtdValuesList, LocalDate mpsDate) {
        double totalQuantityRtd = 0.0;
        for (IndividualValuesRtdDTO indivisualValuesRtdDTO : individualRtdValuesList) {
            boolean isDateMatches = DateUtils.isEqualMonthAndYear(indivisualValuesRtdDTO.getReceiptDate(), mpsDate);
            if (isDateMatches) {
                totalQuantityRtd += indivisualValuesRtdDTO.getValue();
            }
        }
        return DoubleUtils.getFormattedValue(totalQuantityRtd);
    }

    private static MesDTO setMesStockType(Double totalValue, List<IndividualValuesMesDTO> indivisualValuesMesDTOList) {
        MesDTO rmMesDTO = new MesDTO();
        rmMesDTO.setName(MaterialCategoryConstants.MONTH_END_STOCK);
        rmMesDTO.setAliasName(MaterialCategoryConstants.MES_STOCK_TYPE);
        rmMesDTO.setTotalValue(totalValue);
        rmMesDTO.setIndividualValuesMesDTOList(indivisualValuesMesDTOList);
        return rmMesDTO;
    }

    private static RtdDTO setRtdStockType(Double totalValue, List<IndividualValuesRtdDTO> indivisualValuesRtdDTO) {
        RtdDTO rmRtdDTO = new RtdDTO();
        rmRtdDTO.setName(MaterialCategoryConstants.RECEIPT_TILL_DATE);
        rmRtdDTO.setAliasName(MaterialCategoryConstants.RTD_STOCK_TYPE);
        rmRtdDTO.setTotalValue(totalValue);
        rmRtdDTO.setIndividualValuesRtdDTOList(indivisualValuesRtdDTO);
        return rmRtdDTO;
    }

    public static MesDTO getMesStockType(List<IndividualValuesMesDTO> individualMesValuesList, LocalDate mpsDate) {
        if (individualMesValuesList.size() != 0) {
            Double totalQuantityMes = StockUtil.getRscMesTotalQuantity(individualMesValuesList, mpsDate);
            return setMesStockType(totalQuantityMes, individualMesValuesList);
        } else {
            return null;
        }
    }

    public static RtdDTO getRtdStockType(List<IndividualValuesRtdDTO> individualValuesRtdDTOList, LocalDate mpsDate) {
        if (individualValuesRtdDTOList.size() != 0) {
            Double totalQuantityRtd = StockUtil.getRscMtRmRtdTotalQuantity(individualValuesRtdDTOList, mpsDate);
            return StockUtil.setRtdStockType(totalQuantityRtd, individualValuesRtdDTOList);
        } else {
            return null;
        }
    }
}
