package com.example.validation.util;

public class MathUtils {

    public static <T extends Number> Double multiply(T firstValue, T secondValue) {
        return DoubleUtils.getFormattedValue(firstValue.doubleValue() * secondValue.doubleValue());
    }

    public static <T extends Number> Double add(T firstValue, T secondValue) {
        return DoubleUtils.getFormattedValue(firstValue.doubleValue() + secondValue.doubleValue());
    }

    public static <T extends Number> Double sub(T firstValue, T secondValue) {
        return DoubleUtils.getFormattedValue(firstValue.doubleValue() - secondValue.doubleValue());
    }

    public static <T extends Number> Double divide(T firstValue, T secondValue) {
        return DoubleUtils.getFormattedValue(firstValue.doubleValue() / secondValue.doubleValue());
    }
}