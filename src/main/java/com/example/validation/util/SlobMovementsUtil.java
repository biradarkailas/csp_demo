package com.example.validation.util;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscIotPmSlobSummaryDTO;
import com.example.validation.dto.RscIotRmSlobSummaryDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.SlobMovementDTO;
import com.example.validation.dto.slob.RscIotFGSlobSummaryDTO;
import com.example.validation.service.RscIotFGSlobSummaryService;
import com.example.validation.service.RscIotPmSlobSummaryService;
import com.example.validation.service.RscIotRmSlobSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SlobMovementsUtil {
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;
    private final RscIotPmSlobSummaryService rscIotPmSlobSummaryService;
    private final RscIotFGSlobSummaryService rscIotFGSlobSummaryService;

    public SlobMovementsUtil(RscMtPpheaderService rscMtPpheaderService, RscIotRmSlobSummaryService rscIotRmSlobSummaryService, RscIotPmSlobSummaryService rscIotPmSlobSummaryService, RscIotFGSlobSummaryService rscIotFGSlobSummaryService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
        this.rscIotPmSlobSummaryService = rscIotPmSlobSummaryService;
        this.rscIotFGSlobSummaryService = rscIotFGSlobSummaryService;
    }

    public List<SlobMovementDTO> getSlobMovementList(String slobTag) {
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        boolean isPlanValidated = true;
        int monthNo = 0;
        List<SlobMovementDTO> slobMovementDTOList = new ArrayList<>();
        if (Optional.ofNullable(pPheaderDTOList).isPresent()&&pPheaderDTOList.size()>0) {
            while (monthNo < pPheaderDTOList.size() && isPlanValidated) {
                slobMovementDTOList.add(getSlobMovement(pPheaderDTOList.get(monthNo), slobTag));
                monthNo++;
            }
            isPlanValidated = false;
        }
        if (!isPlanValidated) {
            while (monthNo < 12) {
                SlobMovementDTO slobMovementDTO = new SlobMovementDTO();
                slobMovementDTO.setMonthValue(getSlobMovementMonthValue(pPheaderDTOList.get(0).getMpsDate().plusMonths(monthNo)));
                slobMovementDTOList.add(slobMovementDTO);
                monthNo++;
            }
        }
        return slobMovementDTOList;
    }

    private SlobMovementDTO getSlobMovement(RscMtPPheaderDTO rscMtPPheaderDTO, String slobTag) {
        SlobMovementDTO slobMovementDTO = new SlobMovementDTO();
        if (slobTag.equals(MaterialCategoryConstants.PM_SLOB_TAG)) {
            RscIotPmSlobSummaryDTO pmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(pmSlobSummaryDTO).isPresent()) {
                slobMovementDTO.setObValue(pmSlobSummaryDTO.getTotalObsoleteItemValue());
                slobMovementDTO.setSlValue(pmSlobSummaryDTO.getTotalSlowItemValue());
                slobMovementDTO.setMonthValue(getSlobMovementMonthValue(rscMtPPheaderDTO.getMpsDate()));
                slobMovementDTO.setSlobValue(pmSlobSummaryDTO.getTotalSlobValue());
            }
        } else if (slobTag.equals(MaterialCategoryConstants.RM_SLOB_TAG)) {
            RscIotRmSlobSummaryDTO rmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(rmSlobSummaryDTO).isPresent()) {
                slobMovementDTO.setObValue(rmSlobSummaryDTO.getTotalObsoleteItemValue());
                slobMovementDTO.setSlValue(rmSlobSummaryDTO.getTotalSlowItemValue());
                slobMovementDTO.setSlobValue(rmSlobSummaryDTO.getTotalSlobValue());
                slobMovementDTO.setMonthValue(getSlobMovementMonthValue(rscMtPPheaderDTO.getMpsDate()));
            }
        } else if (slobTag.equals(MaterialCategoryConstants.FG_SLOB_TAG)) {
            RscIotFGSlobSummaryDTO fgSlobSummaryDTO = rscIotFGSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(fgSlobSummaryDTO).isPresent()) {
                slobMovementDTO.setSlValue(fgSlobSummaryDTO.getTotalSlowItemValue());
                slobMovementDTO.setObValue(fgSlobSummaryDTO.getTotalObsoleteItemValue());
                slobMovementDTO.setSlobValue(fgSlobSummaryDTO.getTotalSlobValue());
                slobMovementDTO.setMonthValue(getSlobMovementMonthValue(rscMtPPheaderDTO.getMpsDate()));
            }
        }
        return slobMovementDTO;
    }

    private String getSlobMovementMonthValue(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate) + "-" + mpsDate.getYear() % 100;
    }
}
