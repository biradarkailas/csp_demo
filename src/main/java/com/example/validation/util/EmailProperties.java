package com.example.validation.util;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Getter
@Component
public class EmailProperties {
    @Value("${email.signIn.link}")
    private String signInLink;
}
