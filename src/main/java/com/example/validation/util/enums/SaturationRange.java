package com.example.validation.util.enums;

public enum SaturationRange {
    HIGH_SATURATION_RANGE_MIN_COUNT(70),
    MEDIUM_SATURATION_RANGE_MIN_COUNT(40),
    MEDIUM_SATURATION_RANGE_MAX_COUNT(69),
    LOW_SATURATION_RANGE_MAX_COUNT(40);

    int count;

    SaturationRange(int count) {
        this.count = count;
    }

    public int getNumVal() {
        return count;
    }
}