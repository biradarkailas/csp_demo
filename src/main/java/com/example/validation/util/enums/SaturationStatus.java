package com.example.validation.util.enums;

public enum SaturationStatus {
    High, Medium, Low
}
