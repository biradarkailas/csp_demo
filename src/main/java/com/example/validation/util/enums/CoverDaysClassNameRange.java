package com.example.validation.util.enums;

public enum CoverDaysClassNameRange {
    CLASS_A_RANGE_MAX_COUNT(80),
    CLASS_B_RANGE_MIN_COUNT(81),
    CLASS_B_RANGE_MAX_COUNT(95),
    CLASS_C_RANGE_MIN_COUNT(96);

    int count;
    CoverDaysClassNameRange(int count) {
        this.count = count;
    }

    public int getNumVal() {
        return count;
    }
}
