package com.example.validation.util.enums;

public enum PartyOrganizationType {
    ENTERPRISE, ORGANIZATION, DEFAULT
}
