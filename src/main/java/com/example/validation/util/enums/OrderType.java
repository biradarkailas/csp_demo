package com.example.validation.util.enums;

public enum OrderType {
    ConfirmPO, MSS
}