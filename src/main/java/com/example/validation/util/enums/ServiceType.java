package com.example.validation.util.enums;

public enum ServiceType {
    FF, CHA, SUP, CFS, CBW, LP, NONE
}
