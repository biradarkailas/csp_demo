package com.example.validation.util.enums;

public enum SupplierWiseReceiptRange {
    RECEIPT_RANGE_MIN_COUNT_FOR_SCORE4(98),

    RECEIPT_RANGE_MIN_COUNT_FOR_SCORE3(95),
    RECEIPT_RANGE_MAX_COUNT_FOR_SCORE3(98),

    RECEIPT_RANGE_MIN_COUNT_FOR_SCORE2(90),
    RECEIPT_RANGE_MAX_COUNT_FOR_SCORE2(95),

    RECEIPT_RANGE_MIN_COUNT_FOR_SCORE1(80),
    RECEIPT_RANGE_MAX_COUNT_FOR_SCORE1(90),

    RECEIPT_RANGE_MAX_COUNT_FOR_SCORE0(80);

    int count;

    SupplierWiseReceiptRange(int count) {
        this.count = count;
    }

    public int getNumVal() {
        return count;
    }

}
