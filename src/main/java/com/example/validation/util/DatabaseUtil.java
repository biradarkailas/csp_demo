package com.example.validation.util;

import com.example.validation.config.constants.ApplicationConstants;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class DatabaseUtil {
  public static void backup() {
    try {
      String[] command = getCommand();
      Process runtimeProcess = Runtime.getRuntime().exec(command);
      int processComplete = runtimeProcess.waitFor();
      // return processComplete == 0;
    } catch (IOException e) {
      e.printStackTrace();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  @NotNull
  private static String[] getCommand() {
    if (ApplicationConstants.OS.equals("WIN")) {
      return new String[] {
        getDBPath(),
        "--user=" + ApplicationConstants.DB_USER_NAME,
        "--password=" + ApplicationConstants.DB_PASSWORD,
        "--routines",
        "--result-file=" + getFileName(),
        ApplicationConstants.DB_NAME
      };
    } else {
      return new String[] {
        getDBPath(),
        "--user=" + ApplicationConstants.DB_USER_NAME,
        "--password=" + ApplicationConstants.DB_PASSWORD,
        "--routines",
        ApplicationConstants.DB_NAME,
        "> " + getFileName(),
      };
    }
  }

  private static String getDBPath() {
    if (ApplicationConstants.OS.equals("WIN")) {
      return ApplicationConstants.WINDOWS_DB_PATH + "mysqldump";
    } else {
      return "mysqldump --database";
    }
  }

  private static String getFileName() {
    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
    LocalDateTime now = LocalDateTime.now();
    if (ApplicationConstants.OS.equals("WIN")) {
      FileOperation.createDirectoryIfNotExits(ApplicationConstants.WINDOWS_DB_BACKUP_PATH);
      return ApplicationConstants.WINDOWS_DB_BACKUP_PATH + "CSP_DB_" + dtf.format(now) + ".sql";
    } else {
      FileOperation.createDirectoryIfNotExits(ApplicationConstants.LINUX_DB_BACKUP_PATH);
      return ApplicationConstants.LINUX_DB_BACKUP_PATH + "CSP_DB_" + dtf.format(now) + ".sql";
    }
  }
}
