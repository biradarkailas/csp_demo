package com.example.validation.util;

import com.example.validation.config.constants.MapKeyConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscIitMpsForecastDTO;
import com.example.validation.dto.RscIotFGStockDTO;
import com.example.validation.dto.RscMtPPdetailsDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.slob.FGSlobTotalValueDetailDTO;
import com.example.validation.dto.slob.RscIotFGSlobDTO;
import com.example.validation.service.*;
import com.example.validation.util.enums.SlobType;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class FGSlobUtil {
    private final RscMtPpdetailsService rscMtPpdetailsService;
    private final RscMtBomService rscMtBomService;
    private final RscIitMpsForecastService rscIitMpsForecastService;
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscDtItem2Service rscDtItem2Service;

    public FGSlobUtil(RscMtPpdetailsService rscMtPpdetailsService, RscMtBomService rscMtBomService, RscIitMpsForecastService rscIitMpsForecastService, RscMtPpheaderService rscMtPpheaderService, RscDtItem2Service rscDtItem2Service) {
        this.rscMtPpdetailsService = rscMtPpdetailsService;
        this.rscMtBomService = rscMtBomService;
        this.rscIitMpsForecastService = rscIitMpsForecastService;
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscDtItem2Service = rscDtItem2Service;
    }

    public RscIotFGSlobDTO getRscIotFGSlobDTO(RscIotFGStockDTO rscIotFGStockDTO, Long ppHeaderId) {
        Double validTotalMpsQuantity = getValidTotalPlanQuantity(ppHeaderId, rscIotFGStockDTO);
        Double mapValue = getValidMapPrice(rscIotFGStockDTO);
        Double slobQuantity = getValidSlobQuantity(rscIotFGStockDTO.getQuantity(), validTotalMpsQuantity);
        RscIotFGSlobDTO rscIotFGSlobDTO = new RscIotFGSlobDTO();
        rscIotFGSlobDTO.setRscMtItemId(rscIotFGStockDTO.getRscMtItemId());
        rscIotFGSlobDTO.setRscMtPPHeaderId(ppHeaderId);
        rscIotFGSlobDTO.setMapPrice(mapValue);
        rscIotFGSlobDTO.setStockQuantity(rscIotFGStockDTO.getQuantity());
        rscIotFGSlobDTO.setStockValue(getValidStockValue(rscIotFGStockDTO.getQuantity(), mapValue));
        rscIotFGSlobDTO.setTotalPlanQuantity(validTotalMpsQuantity);
        rscIotFGSlobDTO.setSlobQuantity(slobQuantity);
        rscIotFGSlobDTO.setSlobValue(getValidSlobValue(slobQuantity, mapValue));
        rscIotFGSlobDTO.setSlobType(getValidSlobType(slobQuantity, rscIotFGStockDTO.getQuantity()));
        return rscIotFGSlobDTO;
    }

    private Double getValidMapPrice(RscIotFGStockDTO rscIotFGStockDTO) {
        return Optional.ofNullable(rscIotFGStockDTO.getMapPrice())
                .orElse(1.0);
    }

    private Double getValidStockValue(Double stockQuantity, Double mapValue) {
        return DoubleUtils.round(MathUtils.multiply(stockQuantity, mapValue));
    }

    private Double getValidTotalPlanQuantity(Long ppHeaderId, RscIotFGStockDTO rscIotFGStockDTO) {
        return DoubleUtils.round(getTotalPlanQuantity(ppHeaderId, rscIotFGStockDTO.getRscMtItemId(), rscIotFGStockDTO.getItemCode(), rscIotFGStockDTO.getItemCategoryDescription()));
    }

    private Double getTotalPlanQuantity(Long ppHeaderId, Long fgItemId, String fgItemCode, String fgCategoryDescription) {
        if (fgCategoryDescription.equals(MaterialCategoryConstants.FG_MATERIAL)) {
            return getForecastSixMonthQuantitySum(ppHeaderId, fgItemId, fgItemCode);
        } else {
            return getMPSSixMonthQuantitySum(ppHeaderId, fgItemId);
        }
    }

    private Double getForecastSixMonthQuantitySum(Long ppHeaderId, Long fgItemId, String fgItemCode) {
        RscIitMpsForecastDTO rscIitMpsForecastDTO = rscIitMpsForecastService.findByFGItemIdAndPPHeaderId(fgItemId, ppHeaderId);
        if (Optional.ofNullable(rscIitMpsForecastDTO).isPresent()) {
            return DoubleUtils.getFormattedValue(getSixMonthForecastValue(rscIitMpsForecastDTO));
        } else {
            String bpiCode = rscDtItem2Service.get8DigitCode(fgItemCode);
            if (Optional.ofNullable(bpiCode).isPresent()) {
                List<RscIitMpsForecastDTO> mpsForecastDTOs = rscIitMpsForecastService.findByBpiCodeAndPPHeaderId(bpiCode, ppHeaderId);
                if (Optional.ofNullable(mpsForecastDTOs).isPresent()) {
                    return DoubleUtils.getFormattedValue(getSixMonthForecastValueSum(mpsForecastDTOs));
                }
            }
            return 0.0;
        }
    }

    private double getSixMonthForecastValue(RscIitMpsForecastDTO rscIitMpsForecastDTO) {
        return rscIitMpsForecastDTO.getM1Value()
                + rscIitMpsForecastDTO.getM2Value()
                + rscIitMpsForecastDTO.getM3Value()
                + rscIitMpsForecastDTO.getM4Value()
                + rscIitMpsForecastDTO.getM5Value()
                + rscIitMpsForecastDTO.getM6Value();
    }

    private double getSixMonthForecastValueSum(List<RscIitMpsForecastDTO> rscIitMpsForecastDTOs) {
        Double sum = 0.0;
        for (RscIitMpsForecastDTO rscIitMpsForecastDTO : rscIitMpsForecastDTOs) {
            sum = sum + getSixMonthForecastValue(rscIitMpsForecastDTO);
        }
        return sum;
    }

    private Double getMPSSixMonthQuantitySum(Long ppHeaderId, Long fgItemId) {
        RscMtPPdetailsDTO rscMtPPdetailsDTO = rscMtPpdetailsService.findOneRscMtPPdetailsDTO(ppHeaderId, fgItemId);
        if (Optional.ofNullable(rscMtPPdetailsDTO).isPresent()) {
            return DoubleUtils.getFormattedValue(getSixMonthSum(rscMtPPdetailsDTO));
        } else {
            List<RscMtPPdetailsDTO> rscMtPPdetailsDTOs = rscMtPpdetailsService.findAllRscMtPPdetailsDTO(ppHeaderId, getFGItemId(fgItemId, ppHeaderId));
            if (Optional.ofNullable(rscMtPPdetailsDTOs).isPresent()) {
                return DoubleUtils.getFormattedValue(rscMtPPdetailsDTOs.stream().map(this::getSixMonthSum).reduce(0.0, Double::sum));
            }
            return 0.0;
        }
    }

    private Double getSixMonthSum(RscMtPPdetailsDTO rscMtPPdetailsDTO) {
        return rscMtPPdetailsDTO.getM1Value()
                + rscMtPPdetailsDTO.getM2Value()
                + rscMtPPdetailsDTO.getM3Value()
                + rscMtPPdetailsDTO.getM4Value()
                + rscMtPPdetailsDTO.getM5Value()
                + rscMtPPdetailsDTO.getM6Value();
    }

    private List<Long> getFGItemId(Long fgItemId, Long ppHeaderId) {
        return rscMtBomService.findFGItemIdByRscMtItemChildId(fgItemId, ppHeaderId);
    }

    private Double getValidSlobQuantity(Double stockQuantity, Double totalMpsQuantity) {
        Double slobQuantity = DoubleUtils.round(MathUtils.sub(stockQuantity, totalMpsQuantity));
        if (slobQuantity < 0)
            return null;
        return slobQuantity;
    }

    private Double getValidSlobValue(Double slobQuantity, Double mapValue) {
        if (Optional.ofNullable(slobQuantity).isPresent())
            return DoubleUtils.round(MathUtils.multiply(slobQuantity, mapValue));
        return null;
    }

    private SlobType getValidSlobType(Double slobQuantity, Double stockQuantity) {
        if (Optional.ofNullable(slobQuantity).isPresent()) {
            return getSlobType(slobQuantity, stockQuantity);
        }
        return null;
    }

    private SlobType getSlobType(Double slobQuantity, Double stockQuantity) {
        if (slobQuantity < stockQuantity)
            return SlobType.SL;
        else if (slobQuantity > 0)
            return SlobType.OB;
        return null;
    }

    public FGSlobTotalValueDetailDTO getFGSlobTotalValueDetailDTO(List<RscIotFGSlobDTO> rscIotFGSlobDTOs, Long ppHeaderId) {
        Map<String, Double> fgSlobTotalValues = new HashMap<>();
        if (Optional.ofNullable(rscIotFGSlobDTOs).isPresent()) {
            for (RscIotFGSlobDTO rscIotFGSlobDTO : rscIotFGSlobDTOs) {
                String fgSlobTypeKey = getFGSlobTypeKey(rscIotFGSlobDTO);
                if (fgSlobTotalValues.isEmpty()) {
                    fgSlobTotalValues.put(MapKeyConstants.Total_Stock_Value, getTotalValue(0.0, rscIotFGSlobDTO.getStockValue()));
                    fgSlobTotalValues.put(MapKeyConstants.Total_Slob_Value, getTotalValue(0.0, rscIotFGSlobDTO.getSlobValue()));
                    fgSlobTotalValues.put(MapKeyConstants.SL_Type_Total_Value, getTotalValue(0.0, rscIotFGSlobDTO.getSlobValue()));
                    fgSlobTotalValues.put(MapKeyConstants.OB_Type_Total_Value, getTotalValue(0.0, rscIotFGSlobDTO.getSlobValue()));
                } else {
                    fgSlobTotalValues.put(MapKeyConstants.Total_Stock_Value, getTotalValue(fgSlobTotalValues.get(MapKeyConstants.Total_Stock_Value), rscIotFGSlobDTO.getStockValue()));
                    fgSlobTotalValues.put(MapKeyConstants.Total_Slob_Value, getTotalValue(fgSlobTotalValues.get(MapKeyConstants.Total_Slob_Value), rscIotFGSlobDTO.getSlobValue()));
                    if (Optional.ofNullable(fgSlobTypeKey).isPresent())
                        fgSlobTotalValues.put(fgSlobTypeKey, getTotalValue(fgSlobTotalValues.get(fgSlobTypeKey), rscIotFGSlobDTO.getSlobValue()));
                }
            }
        }
        return getFGSlobTotalValueDetailDTO(fgSlobTotalValues, ppHeaderId);
    }

    private String getFGSlobTypeKey(RscIotFGSlobDTO rscIotFGSlobDTO) {
        if (Optional.ofNullable(rscIotFGSlobDTO.getSlobType()).isPresent()) {
            if (rscIotFGSlobDTO.getSlobType().name().equals(MaterialCategoryConstants.SL_SLOB_TYPE)) {
                return MapKeyConstants.SL_Type_Total_Value;
            } else {
                return MapKeyConstants.OB_Type_Total_Value;
            }
        }
        return null;
    }

    private FGSlobTotalValueDetailDTO getFGSlobTotalValueDetailDTO(Map<String, Double> fgSlobTotalValues, Long ppHeaderId) {
        FGSlobTotalValueDetailDTO fgSlobTotalValueDetailDTO = new FGSlobTotalValueDetailDTO();
        Optional<RscMtPPheaderDTO> optionalRscMtPPHeaderDTO = rscMtPpheaderService.findOne(ppHeaderId);
        if (optionalRscMtPPHeaderDTO.isPresent()) {
            fgSlobTotalValueDetailDTO.setMpsId(optionalRscMtPPHeaderDTO.get().getId());
            fgSlobTotalValueDetailDTO.setMpsDate(optionalRscMtPPHeaderDTO.get().getMpsDate());
            fgSlobTotalValueDetailDTO.setMpsName(optionalRscMtPPHeaderDTO.get().getMpsName());
            fgSlobTotalValueDetailDTO.setTotalStockValue(fgSlobTotalValues.get(MapKeyConstants.Total_Stock_Value));
            fgSlobTotalValueDetailDTO.setTotalSlobValue(fgSlobTotalValues.get(MapKeyConstants.Total_Slob_Value));
            fgSlobTotalValueDetailDTO.setSlTypeTotalValue(fgSlobTotalValues.get(MapKeyConstants.SL_Type_Total_Value));
            fgSlobTotalValueDetailDTO.setObTypeTotalValue(fgSlobTotalValues.get(MapKeyConstants.OB_Type_Total_Value));
        }
        return fgSlobTotalValueDetailDTO;
    }

    private Double getTotalValue(Double totalValue, Double value) {
        if (Optional.ofNullable(value).isPresent() && Optional.ofNullable(totalValue).isPresent())
            return MathUtils.add(totalValue, value);
        return totalValue;
    }
}