package com.example.validation.util;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.slob.FGSlobDTO;
import com.example.validation.dto.slob.FGSlobDeviationDTO;
import com.example.validation.dto.slob.RscIotFGTypeDivisionSlobSummaryDTO;
import com.example.validation.dto.slob.TypeWiseFGSlobSummaryDashboardDTO;
import com.example.validation.util.enums.SlobType;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;

@Component
public class FGSlobSummaryUtil {

    public List<RscIotFGTypeDivisionSlobSummaryDTO> getRscIotFGTypeDivisionSlobSummaryDTOs(Long ppHeaderId, Long fgTypeId, List<FGSlobDTO> fgSlobDTOs) {
        List<RscIotFGTypeDivisionSlobSummaryDTO> rscIotFGTypeDivisionSlobSummaryDTOs = new ArrayList<>();
        for (Long divisionId : getDivisionIds(fgSlobDTOs)) {
            if (Optional.ofNullable(divisionId).isPresent())
                rscIotFGTypeDivisionSlobSummaryDTOs.add(getRscIotFGTypeDivisionSlobSummaryDTO(ppHeaderId, fgTypeId, divisionId, fgSlobDTOs));
        }
        return rscIotFGTypeDivisionSlobSummaryDTOs;
    }

    private Set<Long> getDivisionIds(List<FGSlobDTO> fgSlobDTOs) {
        return fgSlobDTOs.stream().map(FGSlobDTO::getItemDivisionId).collect(Collectors.toSet());
    }

    private RscIotFGTypeDivisionSlobSummaryDTO getRscIotFGTypeDivisionSlobSummaryDTO(Long ppHeaderId, Long fgTypeId, Long divisionId, List<FGSlobDTO> fgSlobDTOs) {
        RscIotFGTypeDivisionSlobSummaryDTO rscIotFGTypeDivisionSlobSummaryDTO = new RscIotFGTypeDivisionSlobSummaryDTO();
        List<FGSlobDTO> fgSlobDTOsByDivision = getFGSlobDTOByDivisionId(divisionId, fgSlobDTOs);
        List<FGSlobDTO> filteredFGSlobDTOs = getFGSlobDTOBySlobTypeIsNotNull(fgSlobDTOsByDivision);
        rscIotFGTypeDivisionSlobSummaryDTO.setFgTypeId(fgTypeId);
        rscIotFGTypeDivisionSlobSummaryDTO.setRscDtDivisionId(divisionId);
        rscIotFGTypeDivisionSlobSummaryDTO.setRscMtPPheaderId(ppHeaderId);
        Double totalSlobValue = getTotalSlobValue(filteredFGSlobDTOs);
        Double totalStockValue = getTotalStockValue(fgSlobDTOsByDivision);
        rscIotFGTypeDivisionSlobSummaryDTO.setTotalSlobValue(totalSlobValue);
        rscIotFGTypeDivisionSlobSummaryDTO.setTotalStockValue(totalStockValue);
        rscIotFGTypeDivisionSlobSummaryDTO.setTotalSlowItemValue(getTotalSLValue(filteredFGSlobDTOs));
        rscIotFGTypeDivisionSlobSummaryDTO.setTotalObsoleteItemValue(getTotalOBValue(filteredFGSlobDTOs));
        rscIotFGTypeDivisionSlobSummaryDTO.setStockQualityPercentage(getStockQualityPercentage(getSlobPercentageValue(totalSlobValue, totalStockValue)));
        rscIotFGTypeDivisionSlobSummaryDTO.setStockQualityValue(getStockQualityValue(totalStockValue, rscIotFGTypeDivisionSlobSummaryDTO.getStockQualityPercentage()));
        rscIotFGTypeDivisionSlobSummaryDTO.setObsoleteItemPercentage(getItemPercentageValue(rscIotFGTypeDivisionSlobSummaryDTO.getTotalObsoleteItemValue(), rscIotFGTypeDivisionSlobSummaryDTO.getTotalStockValue()));
        rscIotFGTypeDivisionSlobSummaryDTO.setSlowItemPercentage(getItemPercentageValue(rscIotFGTypeDivisionSlobSummaryDTO.getTotalSlowItemValue(), rscIotFGTypeDivisionSlobSummaryDTO.getTotalStockValue()));
        return rscIotFGTypeDivisionSlobSummaryDTO;
    }

    private List<FGSlobDTO> getFGSlobDTOByDivisionId(Long divisionId, List<FGSlobDTO> fgSlobDTOs) {
        return fgSlobDTOs.stream()
                .filter(fgSlobDTO -> fgSlobDTO.getItemDivisionId() != null)
                .filter(fgSlobDTO -> fgSlobDTO.getItemDivisionId().equals(divisionId))
                .collect(Collectors.toList());
    }

    private List<FGSlobDTO> getFGSlobDTOBySlobTypeIsNotNull(List<FGSlobDTO> fgSlobDTOs) {
        return fgSlobDTOs.stream()
                .filter(fgSlobDTO -> fgSlobDTO.getSlobType() != null)
                .collect(Collectors.toList());
    }

    public Double getDashboardTotalSlobValue(List<TypeWiseFGSlobSummaryDashboardDTO> divisionWiseFGSlobSummaryDashboardDTOs) {
        return divisionWiseFGSlobSummaryDashboardDTOs.stream().mapToDouble(TypeWiseFGSlobSummaryDashboardDTO::getSlobValue).sum();
    }

    public Double getDashboardTotalSLValue(List<TypeWiseFGSlobSummaryDashboardDTO> divisionWiseFGSlobSummaryDashboardDTOs) {
        return divisionWiseFGSlobSummaryDashboardDTOs.stream().mapToDouble(TypeWiseFGSlobSummaryDashboardDTO::getSlValue).sum();
    }

    public Double getDashboardTotalOBValue(List<TypeWiseFGSlobSummaryDashboardDTO> divisionWiseFGSlobSummaryDashboardDTOs) {
        return divisionWiseFGSlobSummaryDashboardDTOs.stream().mapToDouble(TypeWiseFGSlobSummaryDashboardDTO::getObValue).sum();
    }

    private Double getTotalSlobValue(List<FGSlobDTO> fgSlobDTOsByDivision) {
        return fgSlobDTOsByDivision.stream().mapToDouble(FGSlobDTO::getSlobValue).sum();
    }

    private Double getTotalStockValue(List<FGSlobDTO> fgSlobDTOsByDivision) {
        return fgSlobDTOsByDivision.stream().mapToDouble(FGSlobDTO::getStockValue).sum();
    }

    private Double getTotalSLValue(List<FGSlobDTO> fgSlobDTOsByDivision) {
        return fgSlobDTOsByDivision.stream()
                .filter(fgSlobDTO -> fgSlobDTO.getSlobType().equals(SlobType.SL))
                .mapToDouble(FGSlobDTO::getSlobValue).sum();
    }

    private Double getTotalOBValue(List<FGSlobDTO> fgSlobDTOsByDivision) {
        return fgSlobDTOsByDivision.stream()
                .filter(fgSlobDTO -> fgSlobDTO.getSlobType().equals(SlobType.OB))
                .mapToDouble(FGSlobDTO::getSlobValue).sum();
    }

    public Double getPercentageValue(Map<String, Double> typeWiseSlobMap, String keyName) {
        return MathUtils.divide(typeWiseSlobMap.get(keyName), typeWiseSlobMap.get(MaterialCategoryConstants.STOCK));
    }

    public Double getSlobPercentageValue(Double slobValue, Double stockValue) {
        return MathUtils.divide(slobValue, stockValue);
    }

    public Double getStockQualityPercentage(Double slobPercentage) {
        return 100 - slobPercentage;
    }


    public Double getValidTotalValue(Map<String, Double> fgSlobSummaryMap, Double totalValue, String keyName) {
        if (fgSlobSummaryMap.keySet().contains(keyName)) {
            return fgSlobSummaryMap.get(keyName) + totalValue;
        }
        return totalValue;
    }

    public FGSlobDeviationDTO getFGSlobDeviationDTO(Double currentMonthValue, Double previousMonthValue) {
        FGSlobDeviationDTO fgSlobDeviationDTO = new FGSlobDeviationDTO();
        fgSlobDeviationDTO.setCurrentMonthSlobValue(currentMonthValue);
        fgSlobDeviationDTO.setPreviousMonthSlobValue(previousMonthValue);
        Double deviationValue = getDeviationValue(currentMonthValue, previousMonthValue);
        fgSlobDeviationDTO.setDeviationValue(deviationValue);
        fgSlobDeviationDTO.setDeviationStatus(getDeviationStatus(deviationValue));
        return fgSlobDeviationDTO;
    }

    private Double getDeviationValue(Double currentMonthValueO, Double previousMonthValue) {
        return MathUtils.sub(currentMonthValueO, previousMonthValue);
    }

    public String getDeviationStatus(Double deviationValue) {
        if (deviationValue > 0) {
            return ApplicationConstants.DEVIATION_STATUS_UP;
        } else if (deviationValue < 0) {
            return ApplicationConstants.DEVIATION_STATUS_DOWN;
        } else {
            return ApplicationConstants.DEVIATION_STATUS_NA;
        }
    }

    public Double getStockQualityValue(Double totalStockValue, Double stockQualityPercentage) {
        return DoubleUtils.round(MathUtils.divide(MathUtils.multiply(totalStockValue, stockQualityPercentage), 100));
    }

    private Double getItemPercentageValue(Double slobValue, Double totalStockValue) {
        return MathUtils.multiply(MathUtils.divide(slobValue, totalStockValue), 100);
    }
}