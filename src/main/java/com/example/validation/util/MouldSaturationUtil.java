package com.example.validation.util;

public class MouldSaturationUtil {
    public static Double getMonth1MouldSaturation(Double month1Value, Double month2Value, Double workingDays, Double rtdValue, Double capacity) {
        if (month1Value != null && month2Value != null && rtdValue != null && capacity != null && workingDays != null) {
            return DoubleUtils.round(((month1Value + month2Value - rtdValue) / (capacity * workingDays)) * 100);
        }
        return 0.0;
    }

    public static Double getMonthWiseMouldSaturation(Double month1Value, Double workingDays, Double capacity) {
        if (month1Value != null && capacity != null && workingDays != null) {
            return DoubleUtils.round((month1Value / (capacity * workingDays)) * 100);
        }
        return 0.0;
    }

}
