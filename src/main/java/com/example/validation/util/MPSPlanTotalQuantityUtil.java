package com.example.validation.util;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.dto.MPSDeltaAnalysisMainDTO;
import com.example.validation.dto.RscMtPPdetailsDTO;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class MPSPlanTotalQuantityUtil {
    public List<Double> getMonthWiseMPSPlanTotalQuantityDTO(List<RscMtPPdetailsDTO> rscMtPPDetailsDTOs) {
        List<Double> mpsPlanTotalQuantities = new ArrayList<>();
        Map<Integer, Double> mpsPlanTotalQuantityMap = getMPSPlanTotalQuantityMap(rscMtPPDetailsDTOs);
        for (int i = 1; i <= 12; i++) {
            mpsPlanTotalQuantities.add(mpsPlanTotalQuantityMap.get(i));
        }
        return mpsPlanTotalQuantities;
    }

    private Map<Integer, Double> getMPSPlanTotalQuantityMap(List<RscMtPPdetailsDTO> rscMtPPDetailsDTOs) {
        Map<Integer, Double> allMonthQuantityMap = new HashMap<>();
        if (rscMtPPDetailsDTOs.size() > 0) {
            for (RscMtPPdetailsDTO rscMtPPdetailsDTO : rscMtPPDetailsDTOs) {
                if (allMonthQuantityMap.isEmpty()) {
                    allMonthQuantityMap.put(1, rscMtPPdetailsDTO.getM1Value());
                    allMonthQuantityMap.put(2, rscMtPPdetailsDTO.getM2Value());
                    allMonthQuantityMap.put(3, rscMtPPdetailsDTO.getM3Value());
                    allMonthQuantityMap.put(4, rscMtPPdetailsDTO.getM4Value());
                    allMonthQuantityMap.put(5, rscMtPPdetailsDTO.getM5Value());
                    allMonthQuantityMap.put(6, rscMtPPdetailsDTO.getM6Value());
                    allMonthQuantityMap.put(7, rscMtPPdetailsDTO.getM7Value());
                    allMonthQuantityMap.put(8, rscMtPPdetailsDTO.getM8Value());
                    allMonthQuantityMap.put(9, rscMtPPdetailsDTO.getM9Value());
                    allMonthQuantityMap.put(10, rscMtPPdetailsDTO.getM10Value());
                    allMonthQuantityMap.put(11, rscMtPPdetailsDTO.getM11Value());
                    allMonthQuantityMap.put(12, rscMtPPdetailsDTO.getM12Value());
                } else {
                    allMonthQuantityMap.put(1, allMonthQuantityMap.get(1) + rscMtPPdetailsDTO.getM1Value());
                    allMonthQuantityMap.put(2, allMonthQuantityMap.get(2) + rscMtPPdetailsDTO.getM2Value());
                    allMonthQuantityMap.put(3, allMonthQuantityMap.get(3) + rscMtPPdetailsDTO.getM3Value());
                    allMonthQuantityMap.put(4, allMonthQuantityMap.get(4) + rscMtPPdetailsDTO.getM4Value());
                    allMonthQuantityMap.put(5, allMonthQuantityMap.get(5) + rscMtPPdetailsDTO.getM5Value());
                    allMonthQuantityMap.put(6, allMonthQuantityMap.get(6) + rscMtPPdetailsDTO.getM6Value());
                    allMonthQuantityMap.put(7, allMonthQuantityMap.get(7) + rscMtPPdetailsDTO.getM7Value());
                    allMonthQuantityMap.put(8, allMonthQuantityMap.get(8) + rscMtPPdetailsDTO.getM8Value());
                    allMonthQuantityMap.put(9, allMonthQuantityMap.get(9) + rscMtPPdetailsDTO.getM9Value());
                    allMonthQuantityMap.put(10, allMonthQuantityMap.get(10) + rscMtPPdetailsDTO.getM10Value());
                    allMonthQuantityMap.put(11, allMonthQuantityMap.get(11) + rscMtPPdetailsDTO.getM11Value());
                    allMonthQuantityMap.put(12, allMonthQuantityMap.get(12) + rscMtPPdetailsDTO.getM12Value());
                }
            }
        } else {
            allMonthQuantityMap.put(1, 0.0);
            allMonthQuantityMap.put(2, 0.0);
            allMonthQuantityMap.put(3, 0.0);
            allMonthQuantityMap.put(4, 0.0);
            allMonthQuantityMap.put(5, 0.0);
            allMonthQuantityMap.put(6, 0.0);
            allMonthQuantityMap.put(7, 0.0);
            allMonthQuantityMap.put(8, 0.0);
            allMonthQuantityMap.put(9, 0.0);
            allMonthQuantityMap.put(10, 0.0);
            allMonthQuantityMap.put(11, 0.0);
            allMonthQuantityMap.put(12, 0.0);
        }
        return allMonthQuantityMap;
    }

    public MPSDeltaAnalysisMainDTO getMPSDeltaAnalysisMainDTO(Map<Integer, List<Double>> mpsPlanTotalQuantityMap, int mpsCount) {
        MPSDeltaAnalysisMainDTO mpsPSDeltaAnalysisMainDTO = new MPSDeltaAnalysisMainDTO();
        mpsPSDeltaAnalysisMainDTO.setM1Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(1), 1));
        mpsPSDeltaAnalysisMainDTO.setM2Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(2), 2));
        mpsPSDeltaAnalysisMainDTO.setM3Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(3), 3));
        mpsPSDeltaAnalysisMainDTO.setM4Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(4), 4));
        mpsPSDeltaAnalysisMainDTO.setM5Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(5), 5));
        mpsPSDeltaAnalysisMainDTO.setM6Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(6), 6));
        mpsPSDeltaAnalysisMainDTO.setM7Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(7), 7));
        mpsPSDeltaAnalysisMainDTO.setM8Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(8), 8));
        mpsPSDeltaAnalysisMainDTO.setM9Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(9), 9));
        mpsPSDeltaAnalysisMainDTO.setM10Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(10), 10));
        mpsPSDeltaAnalysisMainDTO.setM11Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(11), 11));
        mpsPSDeltaAnalysisMainDTO.setM12Values(getMonthWiseMPSDeltaAnalysisMainDTO(mpsPlanTotalQuantityMap.get(12), 12));
        Map<String, Map<Integer, Double>> bothMPSPlanValueMap = getBothMPSPlanMap(mpsPlanTotalQuantityMap, mpsCount);
        mpsPSDeltaAnalysisMainDTO.setFinalValues(getMPSFinalPlanDTO(bothMPSPlanValueMap.get(ApplicationConstants.FINAL)));
        mpsPSDeltaAnalysisMainDTO.setDeviationValues(getMPSDeviationDTO(bothMPSPlanValueMap.get(ApplicationConstants.PREVIOUS), bothMPSPlanValueMap.get(ApplicationConstants.FINAL)));
        return mpsPSDeltaAnalysisMainDTO;
    }

    private List<Double> getMonthWiseMPSDeltaAnalysisMainDTO(List<Double> mpsPlanTotalQuantities, int monthNumber) {
        if (Optional.ofNullable(mpsPlanTotalQuantities).isPresent()) {
            List<Double> allMPSPlanValues = new ArrayList<>();
            for (int i = 1; i < 24; i++) {
                if (i >= monthNumber && i <= monthNumber + 11)
                    allMPSPlanValues.add(mpsPlanTotalQuantities.get(i - monthNumber));
                else
                    allMPSPlanValues.add(null);
            }
            return allMPSPlanValues;
        }
        return null;
    }

    private List<Long> getMPSFinalPlanDTO(Map<Integer, Double> mpsFinalPlanDTOMap) {
        List<Long> finalPlanValues = new ArrayList<>();
        for (int i = 1; i <= 23; i++) {
            Double value = mpsFinalPlanDTOMap.get(i);
            if (Optional.ofNullable(value).isPresent())
                finalPlanValues.add(Math.round(value));
            else
                finalPlanValues.add(null);
        }
        return finalPlanValues;
    }

    private Map<String, Map<Integer, Double>> getBothMPSPlanMap(Map<Integer, List<Double>> mpsPlanTotalQuantityMap, int mpsCount) {
        Map<String, Map<Integer, Double>> bothMPSPlanValueMap = new HashMap<>();
        Map<Integer, Double> mpsFinalPlanValueMap = new HashMap<>();
        Map<Integer, Double> mpsPreviousPlanValueMap = new HashMap<>();
        int key = 0;
        for (int monthIndex = 1; monthIndex <= 23; monthIndex++) {
            if (monthIndex < mpsCount) {
                mpsFinalPlanValueMap.put(monthIndex, mpsPlanTotalQuantityMap.get(monthIndex).get(0));
                mpsPreviousPlanValueMap.put(monthIndex, mpsPlanTotalQuantityMap.get(1).get(monthIndex - 1));
            } else {
                if (key < 12) {
                    mpsFinalPlanValueMap.put(monthIndex, mpsPlanTotalQuantityMap.get(mpsCount).get(key));
                    mpsPreviousPlanValueMap.put(monthIndex, getValidValue(mpsPlanTotalQuantityMap.get(1), monthIndex - 1));
                } else {
                    mpsFinalPlanValueMap.put(monthIndex, null);
                    mpsPreviousPlanValueMap.put(monthIndex, null);
                }
                key++;
            }
        }
        bothMPSPlanValueMap.put(ApplicationConstants.FINAL, mpsFinalPlanValueMap);
        bothMPSPlanValueMap.put(ApplicationConstants.PREVIOUS, mpsPreviousPlanValueMap);
        return bothMPSPlanValueMap;
    }

    private Double getValidValue(List<Double> values, int monthNumber) {
        if (monthNumber < 12) {
            return values.get(monthNumber);
        }
        return null;
    }

    private int getValidMPSCount(int mpsCount) {
        if (mpsCount == 0)
            return mpsCount + 1;
        return mpsCount;
    }

    private List<Double> getMPSDeviationDTO(Map<Integer, Double> mpsPreviousPlanValueMap, Map<Integer, Double> mpsFinalPlanValueMap) {
        List<Double> mpsDeviationValues = new ArrayList<>();
        for (int i = 1; i <= 23; i++) {
            mpsDeviationValues.add(getDeviationValue(mpsPreviousPlanValueMap.get(i), mpsFinalPlanValueMap.get(i)));
        }
        return mpsDeviationValues;
    }

    private Double getDeviationValue(Double previousMonthValue, Double finalValue) {
        if (Optional.ofNullable(finalValue).isPresent()) {
            if (Optional.ofNullable(previousMonthValue).isPresent()) {
                Double result = (finalValue - previousMonthValue) / previousMonthValue;
                return DoubleUtils.getFormattedValue(result) * 100;
            }
            return 0.0;
        }
        return null;
    }
}