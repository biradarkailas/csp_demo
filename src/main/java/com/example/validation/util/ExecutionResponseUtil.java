package com.example.validation.util;

import com.example.validation.dto.ExecutionResponseDTO;

import java.time.LocalDateTime;

public class ExecutionResponseUtil {
    public static ExecutionResponseDTO getExecutionResponseDTO(String moduleName, String message, Boolean isSuccess) {
        ExecutionResponseDTO executionResponseDTO = new ExecutionResponseDTO();
        executionResponseDTO.setIsSuccess(isSuccess);
        executionResponseDTO.setMessage(message);
        executionResponseDTO.setModuleName(moduleName);
        executionResponseDTO.setCreatedDateTime(LocalDateTime.now());
        if (!isSuccess) {
            executionResponseDTO.setErrorMessage("Data execution has failed while calculating " + moduleName + " \nPlease re-create the plan after correcting data");
        }
        return executionResponseDTO;
    }
}