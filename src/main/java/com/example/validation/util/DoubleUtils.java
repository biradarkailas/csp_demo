package com.example.validation.util;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class DoubleUtils {
    public static Double getFormattedValue(Double value) {
        DecimalFormat formatter = new DecimalFormat("#0.0000");
        formatter.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return Double.valueOf(formatter.format(value));
    }

    public static Double round(Double value) {
        return (double) Math.round(value);
    }

    public static Double getTwoDecimalFormattedValue(Double value) {
        DecimalFormat formatter = new DecimalFormat("#0.00");
        formatter.setDecimalFormatSymbols(DecimalFormatSymbols.getInstance(Locale.ENGLISH));
        return Double.valueOf(formatter.format(value));
    }
}