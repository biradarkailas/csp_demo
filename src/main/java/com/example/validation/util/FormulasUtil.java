package com.example.validation.util;

import com.example.validation.dto.RscMtBomDTO;

public class FormulasUtil {

    public static Double logicalConsumption(Double quantity, RscMtBomDTO rscMtBomDTO) {
        return DoubleUtils.getFormattedValue(quantity * rscMtBomDTO.getLinkQuantity() * (1 + rscMtBomDTO.getWastePercent() / 100));
    }

    public static Double rmLogicalConsumption(Double quantity, RscMtBomDTO rscMtBomDTO) {
        return DoubleUtils.getFormattedValue((quantity * rscMtBomDTO.getLinkQuantity() * (1 + rscMtBomDTO.getWastePercent() / 100)) / 100);
    }

    public static Double getValidQuantity(Double moqValue, Double seriesValue, Double totalQuantity) {
        if (totalQuantity <= moqValue) {
            return DoubleUtils.getFormattedValue(moqValue);
        } else {
            return getQuantityTechnicalSeriesWise(totalQuantity, seriesValue);
        }
    }

    private static Double getQuantityTechnicalSeriesWise(Double quantity, Double technicalSeriesValue) {
        int quotient = (int) (quantity / technicalSeriesValue);
        Double remainder = quantity % technicalSeriesValue;
        if (remainder == 0) {
            return DoubleUtils.getFormattedValue(quantity);
        }
        return DoubleUtils.getFormattedValue(technicalSeriesValue * (quotient + 1));
    }
}