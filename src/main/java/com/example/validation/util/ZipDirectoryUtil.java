package com.example.validation.util;


import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.config.constants.ExcelFileNameConstants;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.RscIitIdpStatusService;
import com.example.validation.service.RscMtOtifPmReceiptsHeaderService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@EnableScheduling
@Component
public class ZipDirectoryUtil {
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService;
    private final RscIitIdpStatusService rscIitIdpStatusService;

    public ZipDirectoryUtil(RscMtPpheaderService rscMtPpheaderService, RscMtOtifPmReceiptsHeaderService rscMtOtifPmReceiptsHeaderService, RscIitIdpStatusService rscIitIdpStatusService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscMtOtifPmReceiptsHeaderService = rscMtOtifPmReceiptsHeaderService;
        this.rscIitIdpStatusService = rscIitIdpStatusService;
    }

    @Scheduled(cron = "0 0 0 * * ?")
    public void createZip() throws IOException {
        List<RscMtPPheaderDTO> previousMonthsPPHeaderDTOByActive = rscMtPpheaderService.findAllPreviousMonthsPPHeaderByActive();
        if (Optional.ofNullable(previousMonthsPPHeaderDTOByActive).isPresent()&&!previousMonthsPPHeaderDTOByActive.isEmpty()) {
            List<LocalDate> previousMonthAllOtifDate = rscMtOtifPmReceiptsHeaderService.getPreviousMonthAllOtifDate();
            for (RscMtPPheaderDTO rscMtPPheaderDTO : previousMonthsPPHeaderDTOByActive) {
                if (rscMtPPheaderDTO.getTagName().equals("FC") || rscMtPPheaderDTO.getTagName().equals("VD")) {
                    zipPlansDirectory(rscMtPPheaderDTO);
                }
            }
            if (Optional.ofNullable(previousMonthAllOtifDate).isPresent()&&!previousMonthAllOtifDate.isEmpty()) {
                // for (LocalDate otifDate : currentMonthAllOtifDate) {
                zipOTIFDirectory(previousMonthAllOtifDate.get(0));
                //  }
            }
        }
        rscMtPpheaderService.updateIsBackupDone();
    }

    private void zipPlansDirectory(RscMtPPheaderDTO rscMtPpHeaderDTO) throws IOException {
        File fileToZip = new File(getPlanInputPath(rscMtPpHeaderDTO));
        String outputPath = getPlanOutputPath(rscMtPpHeaderDTO);
        FileOutputStream fos = new FileOutputStream(outputPath);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        zipFile(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fos.close();
    }

    private void zipOTIFDirectory(LocalDate otifDate) throws IOException {
        File fileToZip = new File(getOTIFInputPath(otifDate));
        String outputPath = getOTIFOutputPath(otifDate);
        FileOutputStream fos = new FileOutputStream(outputPath);
        ZipOutputStream zipOut = new ZipOutputStream(fos);
        zipFile(fileToZip, fileToZip.getName(), zipOut);
        zipOut.close();
        fos.close();
    }

    private String getPlanOutputPath(RscMtPPheaderDTO rscMtPpHeaderDTO) {
        String basePath = ApplicationConstants.BACKUP_PATH +
                String.valueOf(rscMtPpHeaderDTO.getMpsDate().getYear()) + "/" +
                String.valueOf(rscMtPpHeaderDTO.getMpsDate().getMonth()) + "/";
        String path = basePath + "Plans" + "/" + getTagName(rscMtPpHeaderDTO.getTagName());
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return path + "/" + rscMtPpHeaderDTO.getMpsName() + "-" + rscMtPpHeaderDTO.getMpsDate().getYear() + ".zip";
    }

    private String getOTIFOutputPath(LocalDate otifDate) {
        String basePath = ApplicationConstants.BACKUP_PATH +
                String.valueOf(otifDate.getYear()) + "/" +
                String.valueOf(otifDate.getMonth()) + "/";
        String path = basePath + "Analysis/Month/PM/Otif/";
        File file = new File(path);
        if (!file.exists()) {
            file.mkdirs();
        }
        return path + getOTIFZipName(otifDate) + ".zip";
    }

    private String getOTIFZipName(LocalDate otifDate) {
        //return otifDate.getDayOfMonth() + DateUtils.getMonthValue(otifDate) + "-" + otifDate.getYear();
        return DateUtils.getMonthValue(otifDate) + "_OTIF";
    }

    private String getTagName(String tagName) {
        return tagName.equals("FC") ? "first cut" : "validated";
    }

    private String getPlanInputPath(RscMtPPheaderDTO rscMtPpHeaderDTO) {
        String basePath = ExcelFileNameConstants.Save_All_Exports_Path +
                String.valueOf(rscMtPpHeaderDTO.getMpsDate().getYear()) + "/" +
                String.valueOf(rscMtPpHeaderDTO.getMpsDate().getMonth()) + "/";
        return basePath + "Plans" + "/" + rscMtPpHeaderDTO.getMpsDate().getDayOfMonth() + "/" + rscMtPpHeaderDTO.getMpsName();
    }

    private String getOTIFInputPath(LocalDate otifDate) {
        String basePath = ExcelFileNameConstants.Save_All_Exports_Path +
                String.valueOf(otifDate.getYear()) + "/" +
                String.valueOf(otifDate.getMonth()) + "/";
        // return basePath + "Analysis/Month/PM/Otif/" + otifDate.getDayOfMonth();
        return basePath + "Analysis/Month/PM/Otif/";
    }

    private void zipFile(File fileToZip, String fileName, ZipOutputStream zipOut) throws IOException {
        if (fileToZip.isDirectory()) {
            if (fileName.endsWith("/")) {
                zipOut.putNextEntry(new ZipEntry(fileName));
                zipOut.closeEntry();
            } else {
                zipOut.putNextEntry(new ZipEntry(fileName + "/"));
                zipOut.closeEntry();
            }
            File[] children = fileToZip.listFiles();
            for (File childFile : children) {
                zipFile(childFile, fileName + "/" + childFile.getName(), zipOut);
            }
        }
        if (fileToZip.isFile()) {
            FileInputStream fis = new FileInputStream(fileToZip);
            ZipEntry zipEntry = new ZipEntry(fileName);
            zipOut.putNextEntry(zipEntry);
            byte[] bytes = new byte[1024];
            int length;
            while ((length = fis.read(bytes)) >= 0) {
                zipOut.write(bytes, 0, length);
            }
            fis.close();
        }
    }
}