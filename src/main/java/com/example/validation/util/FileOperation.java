package com.example.validation.util;

import com.example.validation.config.constants.ApplicationConstants;
import com.example.validation.dto.ExcelFiledetailsDTO;
import com.example.validation.dto.FileUploadStatusDTO;
import com.example.validation.dto.RscIitIdpStatusDTO;
import com.example.validation.service.ExcelFileDetailsService;
import org.apache.commons.io.FileUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.util.ZipSecureFile;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class FileOperation {
    private static Logger log = LoggerFactory.getLogger(FileOperation.class);
    private final ExcelFileDetailsService excelFileDetailsService;

    public FileOperation(ExcelFileDetailsService excelFileDetailsService) {
        this.excelFileDetailsService = excelFileDetailsService;
    }

    public static void createDirectoryIfNotExits(String url) {
        Path path = Paths.get(url);
        try {
            if (!Files.exists(path)) {
                Files.createDirectories(path);
            }
        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public static String createFileNameWithTimestamp(String name, String extension) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(ApplicationConstants.FILE_NAME_DATE_TIME_FORMATTER);
        LocalDateTime now = LocalDateTime.now();
        return name + dtf.format(now) + extension;
    }

    public static void deleteNonEmptyDir(File dir, boolean keepFolder) {
        if (dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                deleteNonEmptyDir(new File(dir, children[i]), false);
            }
        }
        if (!keepFolder) dir.delete();
    }

    public static String[] getFilesInDirectory(File dir) {
        if (dir.isDirectory()) {
            return dir.list();
        }
        return null;
    }

    public static void moveFiles(String sourcePath, String destinationPath) {
        try {
            createDirectoryIfNotExits(destinationPath);
            Files.walk(Paths.get(sourcePath)).forEach(source -> {
                try {
                    Path destination = Paths.get(destinationPath, source.toString().substring(sourcePath.length()));
                    Files.move(source, destination.toAbsolutePath(), StandardCopyOption.REPLACE_EXISTING);
                } catch (IOException e) {
                    log.info("Checked Exception : Directory Not Empty but replaced required files");
                }
            });
        } catch (IOException e) {
            log.info("Failed to move the files");
        }
    }

    public void moveDirectory(File source, File target) {
        try {
            FileUtils.moveDirectoryToDirectory(source, target, true);
        } catch (IOException e) {
            log.info("Failed to move the Directory");
        }
    }

    public FileUploadStatusDTO uploadFiles(MultipartFile file) {
        if (file != null) {
            ZipSecureFile.setMinInflateRatio(0);
            String name = file.getOriginalFilename();
            File directory = new File(ApplicationConstants.SOURCE_PATH);
            directory.mkdir();
            Path rootLocation = Paths.get(ApplicationConstants.SOURCE_PATH);
            System.out.println("rootLocation  ==  " + rootLocation);
            List<ExcelFiledetailsDTO> excelFileDetailsDTOs = excelFileDetailsService.getExcelFiledetailsDTOs(name);
            try {
                InputStream inputStream = file.getInputStream();
                if (!excelFileDetailsDTOs.isEmpty()) {
                    Workbook workbook = getWorkbook(name, inputStream);
                    for (ExcelFiledetailsDTO excelFiledetailsDTO : excelFileDetailsDTOs) {
                        String sheetName = workbook.getSheetName(excelFiledetailsDTO.getSheetIndex());
                        if (!sheetName.equals(excelFiledetailsDTO.getSheetName())) {
                            workbook.setSheetName(excelFiledetailsDTO.getSheetIndex(), excelFiledetailsDTO.getSheetName());
                        }
                    }
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    workbook.write(baos);
                    workbook.close();
                    InputStream updatedInputStream = new ByteArrayInputStream(baos.toByteArray());
                    return getFileUploadStatusDTO(name, rootLocation, updatedInputStream);
                } else {
                    return getFileUploadStatusDTO(name, rootLocation, inputStream);
                }
            } catch (Exception exception) {
                System.out.println("Excel SheetName Update Exception " + exception.getMessage());
            }
        }
        return null;
    }

    private Workbook getWorkbook(String name, InputStream inputStream) throws IOException {
        if (getExtension(name).equalsIgnoreCase("xls")) {
            return new HSSFWorkbook(inputStream);
        } else if (getExtension(name).equalsIgnoreCase("xlsx")) {
            return new XSSFWorkbook(inputStream);
        } else {
            return null;
        }
    }

    private String getExtension(String fileName) {
        int index = fileName.lastIndexOf('.');
        if (index > 0) return fileName.substring(index + 1);
        return "";
    }

    private FileUploadStatusDTO getFileUploadStatusDTO(String name, Path rootLocation, InputStream inputStream) {
        try {
            Files.copy(inputStream, rootLocation.resolve(name), StandardCopyOption.REPLACE_EXISTING);
            return FileUploadStatusDTO.builder().fileName(name).status(true).errorDescription("Successfully Uploaded").build();
        } catch (Exception exception) {
            System.out.println("error while uploading file catch:: " + exception.getMessage());
            return FileUploadStatusDTO.builder().fileName(name).status(false).errorDescription(exception.getMessage()).build();
        }
    }

    public void deleteFile(RscIitIdpStatusDTO file) {
        if (file != null) {
            String name = file.getFileName();
            Path rootLocation = Paths.get(ApplicationConstants.SOURCE_PATH);
            File tempFile = new File(rootLocation + "/" + name + "." + file.getExtension());
            if (tempFile.exists()) {
                tempFile.delete();
            }
        }
    }

    public void deleteDirectory() {
        File directoryPath = new File(ApplicationConstants.SOURCE_PATH);
        try {
            FileUtils.deleteDirectory(directoryPath);
        } catch (Exception exception) {
            System.out.println("data directory not deleted");
        }
    }
}