package com.example.validation.util;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.TextStyle;
import java.util.Locale;
import java.util.Optional;

public class DateUtils {

    public static Boolean checkEndDateValid(LocalDate endDate, LocalDate mpsDate) {
        if (endDate != null) {
            if (endDate.getYear() >= mpsDate.getYear()) {
                if (endDate.getYear() == mpsDate.getYear()) {
                    if (endDate.getMonthValue() >= mpsDate.getMonthValue()) {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            return false;
        }
        return true;
    }

    public static Boolean checkStartDateValid(LocalDate startDate, LocalDate mpsDate) {
        if (startDate != null) {
            if (startDate.getYear() <= mpsDate.getYear()) {
                if (startDate.getYear() == mpsDate.getYear()) {
                    if (startDate.getMonthValue() <= mpsDate.getMonthValue()) {
                        return true;
                    }
                    return false;
                }
                return true;
            }
            return false;
        }
        return true;
    }

    public static boolean isEqualMonthAndYear(LocalDate firstDate, LocalDate secondDate) {
        if (Optional.ofNullable(secondDate).isPresent()) if (firstDate.getYear() == secondDate.getYear())
            if (firstDate.getMonthValue() == secondDate.getMonthValue()) return true;
        return false;
    }

    public static boolean isDateInBetweenStartDateAndEndDate(LocalDate startDate, LocalDate endDate, LocalDate date) {
        return date.isAfter(startDate) && date.isBefore(endDate);
    }

    public static Integer getDaysOfMonth(LocalDate mpsDate) {
        YearMonth yearMonthObject = YearMonth.of(mpsDate.getYear(), mpsDate.getMonthValue());
        return yearMonthObject.lengthOfMonth();
    }

    public static String getMonthValue(LocalDate mpsDate) {
        return mpsDate.getMonth().getDisplayName(TextStyle.SHORT, Locale.US);
    }

    public static String getPrevMonthName(LocalDate mpsDate) {
        return mpsDate.minusMonths(1).getMonth().getDisplayName(TextStyle.SHORT, Locale.US);
    }
    public static String getNextMonthName(LocalDate mpsDate) {
        return mpsDate.plusMonths(1).getMonth().getDisplayName(TextStyle.SHORT, Locale.US);
    }
    public static String getCurrentMonthName(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate) + "-" + mpsDate.getYear() % 100;
    }

    public static String getPreviousMonthName(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate.minusMonths(1)) + "-" + mpsDate.minusMonths(1).getYear() % 100;
    }

    public static LocalDate getLastDateOfMonth(LocalDate mpsDate) {
        YearMonth thisYearMonth = YearMonth.of(mpsDate.getYear(), mpsDate.getMonthValue());
        return thisYearMonth.atEndOfMonth();
    }

    public static String getCurrentMonthYearName(LocalDate mpsDate) {
        return mpsDate.getMonth().getDisplayName(TextStyle.FULL, Locale.US) + "-" + mpsDate.getYear();
    }
}