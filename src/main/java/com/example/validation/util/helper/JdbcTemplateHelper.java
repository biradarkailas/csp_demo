package com.example.validation.util.helper;

import com.example.validation.config.constants.ApplicationConstants;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Component;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author Kailas Biradar
 */
@Component
public class JdbcTemplateHelper {
    private final JdbcTemplate jdbcTemplate;
    private final NamedParameterJdbcTemplate namedparameterJdbcTemplate;

    public JdbcTemplateHelper(NamedParameterJdbcTemplate namedparameterJdbcTemplate, JdbcTemplate jdbcTemplate) {
        this.namedparameterJdbcTemplate = namedparameterJdbcTemplate;
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean loadData(String tableName, String filePath) {
        try {
            namedparameterJdbcTemplate.execute("TRUNCATE TABLE " + tableName, PreparedStatement::execute);
            namedparameterJdbcTemplate.execute("LOAD DATA INFILE \'" + filePath + "\' IGNORE\n" +
                    "INTO TABLE " + tableName + "\n" +
                    "CHARACTER SET latin1 \n" +
                    "FIELDS TERMINATED BY ','\n" +
                    "ENCLOSED BY '\"'\n" +
                    "OPTIONALLY ENCLOSED BY '\"'\n" +
                    "LINES TERMINATED BY '\\r\\n'\n" +
                    "IGNORE 1 ROWS", PreparedStatement::execute);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean loadDataWithoutHeader(String tableName, String filePath) {
        try {
            namedparameterJdbcTemplate.execute("TRUNCATE TABLE " + tableName, PreparedStatement::execute);
            namedparameterJdbcTemplate.execute("LOAD DATA INFILE \'" + filePath + "\' IGNORE\n" +
                            "INTO TABLE " + tableName + "\n" +
                            "CHARACTER SET latin1 \n" +
                            "FIELDS TERMINATED BY ','\n" +
                            "ENCLOSED BY '\"'\n" +
                            "OPTIONALLY ENCLOSED BY '\"'\n" +
                            "LINES TERMINATED BY '\\r\\n'\n",
                    PreparedStatement::execute);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean batchInsertData(String insertQuery, List<List<Object>> rows) {
        jdbcTemplate.batchUpdate(insertQuery, new BatchPreparedStatementSetter() {
            @Override
            public void setValues(PreparedStatement preparedStatement, int i) throws SQLException {
                int columnIndex = ApplicationConstants.ONE;
                List<Object> rowData = rows.get(i);
                for (Object cellValue : rowData) {
                    preparedStatement.setString(columnIndex++, cellValue.toString());
                }
            }

            @Override
            public int getBatchSize() {
                return rows.size();
            }
        });
        return true;
    }

    public int getRecordCountByTableName(String tableName) {
        return jdbcTemplate.queryForObject(ApplicationConstants.COUNT_QUERY + tableName, Integer.class);
    }
}
