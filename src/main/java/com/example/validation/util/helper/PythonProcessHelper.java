package com.example.validation.util.helper;

import com.example.validation.config.constants.ApplicationConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class PythonProcessHelper {
    private final Logger log = LoggerFactory.getLogger(PythonProcessHelper.class);

    public PythonProcessHelper() {
    }

    public List<String> excelToCsv(String excelFileName, String sheetNames) throws IOException {
        List<String> result = new ArrayList<>();
        String response;
        String[] cmd;
        if (ApplicationConstants.OS.equals("WIN")) {
            cmd = new String[5];
            cmd[0] = ApplicationConstants.PYTHON_WIN;
            cmd[1] = getPythonFilePath();
            cmd[2] = getSourcePath();
            cmd[3] = excelFileName;
            cmd[4] = sheetNames;
            System.out.println("Python Windows Part called");
        } else {
            cmd = new String[5];
            cmd[0] = ApplicationConstants.PYTHON_LIN;
            cmd[1] = getPythonFilePath();
            cmd[2] = getSourcePath();
            cmd[3] = excelFileName;
            cmd[4] = sheetNames;
            System.out.println("Python linux Part called");
        }
        Runtime runtime = Runtime.getRuntime();
        Process process = runtime.exec(cmd);
        BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
        while ((response = in.readLine()) != null) {
            result.addAll(getListByPythonResponse(response));
        }
        return result;
    }

    private String getDirectoryName(String excelFileName) {
        return "";
    }

    private String getSourcePath() {
        return ApplicationConstants.SOURCE_PATH + ApplicationConstants.FORWARD_SLASH;
    }

    private List<String> getListByPythonResponse(String pythonResponse) {
        return Arrays.asList(pythonResponse.split(", "));
    }

    private String getPythonFilePath() {
        try {
            File file = ResourceUtils.getFile(ApplicationConstants.PYTHON_FILE_PATH);
            //File file = new File(getClass().getClassLoader().getResource(ApplicationConstants.PYTHON_FILE_PATH).getFile());
            return file.getAbsolutePath();
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }
}