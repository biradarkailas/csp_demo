package com.example.validation.util;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.DeltaAnalysisDetailsDTO;
import com.example.validation.dto.RscIotFgLineSaturationDTO;
import com.example.validation.dto.RscIotFgSkidSaturationDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.service.RscIotFgLineSaturationService;
import com.example.validation.service.RscIotFgSkidSaturationService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SaturationDeltaAnalysisUtils {
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotFgLineSaturationService rscIotFgLineSaturationService;
    private final RscIotFgSkidSaturationService rscIotFgSkidSaturationService;

    public SaturationDeltaAnalysisUtils(RscMtPpheaderService rscMtPpheaderService, RscIotFgLineSaturationService rscIotFgLineSaturationService, RscIotFgSkidSaturationService rscIotFgSkidSaturationService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotFgLineSaturationService = rscIotFgLineSaturationService;
        this.rscIotFgSkidSaturationService = rscIotFgSkidSaturationService;
    }

    public List<DeltaAnalysisDetailsDTO> getSaturationDetails(Long id, RscMtPPheaderDTO rscMtPPheaderDTO, String saturationTag) {
        List<DeltaAnalysisDetailsDTO> deltaAnalysisDetailsDTOS = new ArrayList<>();
        boolean isPlanValidated = true;
        Long mpsId;
        int monthNo = 0;
        while (monthNo < 12 && isPlanValidated) {
            if (saturationTag.equals(MaterialCategoryConstants.LINE_SATURATION_TAG)) {
                deltaAnalysisDetailsDTOS.add(getDeltaAnalysisDetails(rscMtPPheaderDTO, id, monthNo));
            } else if (saturationTag.equals(MaterialCategoryConstants.SKID_SATURATION_TAG)) {
                deltaAnalysisDetailsDTOS.add(getSkidDeltaAnalysisDetails(rscMtPPheaderDTO, id, monthNo));
            }
            mpsId = rscMtPpheaderService.getNextMonthMpsId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(mpsId).isPresent()) {
                isPlanValidated = true;
                rscMtPPheaderDTO = rscMtPpheaderService.findOne(mpsId).orElse(null);
            } else {
                isPlanValidated = false;
            }
            monthNo++;
        }
        return deltaAnalysisDetailsDTOS;
    }

    private DeltaAnalysisDetailsDTO getDeltaAnalysisDetails(RscMtPPheaderDTO rscMtPPheaderDTO, Long rscDtLinesId, int monthNo) {
        DeltaAnalysisDetailsDTO deltaAnalysisDetailsDTO = new DeltaAnalysisDetailsDTO();
        deltaAnalysisDetailsDTO.setSaturationMonth(getSaturationMonth(rscMtPPheaderDTO.getMpsDate()));
        List<RscIotFgLineSaturationDTO> lineDetails = rscIotFgLineSaturationService.findAllByLinesIdAndRscMtPPheaderId(rscDtLinesId, rscMtPPheaderDTO.getId());
        List saturationList = new ArrayList(23);
        Double[] lineSaturation = getSaturationArray(lineDetails.get(0));
        int j = 0;
        for (int i = 0; i < 23; i++) {
            if (i < monthNo) {
                saturationList.add(null);
            } else {
                if (j < 12) {
                    saturationList.add(i, lineSaturation[j++]);
                } else {
                    saturationList.add(null);
                }
            }
        }
        deltaAnalysisDetailsDTO.setSaturationList(saturationList);
        return deltaAnalysisDetailsDTO;
    }

    private Double[] getSaturationArray(RscIotFgLineSaturationDTO rscIotFgLineSaturationDTO) {
        Double[] lineSaturation = new Double[12];
        lineSaturation[0] = rscIotFgLineSaturationDTO.getM1LineSaturation();
        lineSaturation[1] = rscIotFgLineSaturationDTO.getM2LineSaturation();
        lineSaturation[2] = rscIotFgLineSaturationDTO.getM3LineSaturation();
        lineSaturation[3] = rscIotFgLineSaturationDTO.getM4LineSaturation();
        lineSaturation[4] = rscIotFgLineSaturationDTO.getM5LineSaturation();
        lineSaturation[5] = rscIotFgLineSaturationDTO.getM6LineSaturation();
        lineSaturation[6] = rscIotFgLineSaturationDTO.getM7LineSaturation();
        lineSaturation[7] = rscIotFgLineSaturationDTO.getM8LineSaturation();
        lineSaturation[8] = rscIotFgLineSaturationDTO.getM9LineSaturation();
        lineSaturation[9] = rscIotFgLineSaturationDTO.getM10LineSaturation();
        lineSaturation[10] = rscIotFgLineSaturationDTO.getM11LineSaturation();
        lineSaturation[11] = rscIotFgLineSaturationDTO.getM12LineSaturation();
        return lineSaturation;
    }

    private String getSaturationMonth(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate) + " Saturation";
    }

    public Double[] getFinalSaturationDetails(List<DeltaAnalysisDetailsDTO> saturationDetailsList) {
        Double[] finalSaturationArray = new Double[23];
        for (int i = 0; i < 23; i++) {
            if (i < saturationDetailsList.size()) {
                finalSaturationArray[i] = saturationDetailsList.get(i).getSaturationList().get(i);
            } else {
                finalSaturationArray[i] = saturationDetailsList.get(saturationDetailsList.size() - 1).getSaturationList().get(i);
            }
        }
        return finalSaturationArray;
    }


    public Double[] getDeviationDetails(Double[] finalSaturation, List<DeltaAnalysisDetailsDTO> saturationDetailsList) {
        Double[] deviationDetails = new Double[23];
        deviationDetails[0] = 0.0;
        for (int i = 1; i < 23; i++) {
            if (i <saturationDetailsList.size()) {
                deviationDetails[i] = getDeviation(finalSaturation[i], saturationDetailsList.get(0).getSaturationList().get(i));
            } else {
                if (finalSaturation[i] != null) {
                    if (saturationDetailsList.size() > 1 && saturationDetailsList.get(0).getSaturationList().get(i) != null) {
                        deviationDetails[i] = getDeviation(finalSaturation[i], saturationDetailsList.get(0).getSaturationList().get(i));
                    } else {
                        deviationDetails[i] = 0.0;
                    }
                }
            }
        }
        return deviationDetails;
    }

    private Double getDeviation(Double finalSaturation, Double saturation) {
        if (saturation == 0) {
            return 0.0;
        } else {
            return DoubleUtils.getFormattedValue(((finalSaturation - saturation) / saturation) * 100);
        }
    }

    private DeltaAnalysisDetailsDTO getSkidDeltaAnalysisDetails(RscMtPPheaderDTO rscMtPPheaderDTO, Long rscDtSkidId, int monthNo) {
        DeltaAnalysisDetailsDTO deltaAnalysisDetailsDTO = new DeltaAnalysisDetailsDTO();
        deltaAnalysisDetailsDTO.setSaturationMonth(getSaturationMonth(rscMtPPheaderDTO.getMpsDate()));
        List<RscIotFgSkidSaturationDTO> lineDetails = rscIotFgSkidSaturationService.findAllBySkidsIdAndRscMtPPheaderId(rscDtSkidId, rscMtPPheaderDTO.getId());
        List saturationList = new ArrayList(23);
        Double[] skidSaturation = getSkidSaturationArray(lineDetails.get(0));
        int j = 0;
        for (int i = 0; i < 23; i++) {
            if (i < monthNo || j == 12) {
                saturationList.add(null);
            } else if (j < 12) {
                saturationList.add(i, skidSaturation[j++]);
            }
        }
        deltaAnalysisDetailsDTO.setSaturationList(saturationList);
        return deltaAnalysisDetailsDTO;
    }

    private Double[] getSkidSaturationArray(RscIotFgSkidSaturationDTO rscIotFgSkidSaturationDTO) {
        Double[] skidSaturation = new Double[12];
        skidSaturation[0] = rscIotFgSkidSaturationDTO.getM1SkidSaturation();
        skidSaturation[1] = rscIotFgSkidSaturationDTO.getM2SkidSaturation();
        skidSaturation[2] = rscIotFgSkidSaturationDTO.getM3SkidSaturation();
        skidSaturation[3] = rscIotFgSkidSaturationDTO.getM4SkidSaturation();
        skidSaturation[4] = rscIotFgSkidSaturationDTO.getM5SkidSaturation();
        skidSaturation[5] = rscIotFgSkidSaturationDTO.getM6SkidSaturation();
        skidSaturation[6] = rscIotFgSkidSaturationDTO.getM7SkidSaturation();
        skidSaturation[7] = rscIotFgSkidSaturationDTO.getM8SkidSaturation();
        skidSaturation[8] = rscIotFgSkidSaturationDTO.getM9SkidSaturation();
        skidSaturation[9] = rscIotFgSkidSaturationDTO.getM10SkidSaturation();
        skidSaturation[10] = rscIotFgSkidSaturationDTO.getM11SkidSaturation();
        skidSaturation[11] = rscIotFgSkidSaturationDTO.getM12SkidSaturation();
        return skidSaturation;
    }

}
