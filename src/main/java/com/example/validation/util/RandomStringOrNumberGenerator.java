package com.example.validation.util;

import org.springframework.stereotype.Component;

import java.util.Random;

@Component
public class RandomStringOrNumberGenerator {
    public synchronized String generateRandomString(int numberOfCharacters) {
        String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder temp = new StringBuilder();
        Random rnd = new Random();
        while (temp.length() < numberOfCharacters) { // length of the random string.
            int index = (int) (rnd.nextFloat() * CHARS.length());
            temp.append(CHARS.charAt(index));
        }
        return temp.toString();
    }

    public synchronized String generateRandomNumber(int numberOfCharacters) {
        StringBuilder sb = new StringBuilder(numberOfCharacters);
        Random rnd = new Random();
        for(int i=0; i < numberOfCharacters; i++)
            sb.append((char)('0' + rnd.nextInt(10)));
        return sb.toString();
    }
}
