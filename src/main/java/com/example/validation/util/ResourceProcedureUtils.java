package com.example.validation.util;

import com.example.validation.repository.ErrorLogRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component
public class ResourceProcedureUtils {
    private final Logger log = LoggerFactory.getLogger(ResourceProcedureUtils.class);
    private final ErrorLogRepository errorLogRepository;

    public ResourceProcedureUtils(ErrorLogRepository errorLogRepository) {
        this.errorLogRepository = errorLogRepository;
    }

    public boolean srcPostMps() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostMps());
        } catch (Exception e) {
            log.info("Post MPS SQL Exception");
        }
        return false;
    }

    public boolean srcPostWorkingDays() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostWorkingDays());
        } catch (Exception e) {
            log.info("Post Working Days SQL Exception");
        }
        return false;
    }

    public boolean srcPostBOM() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostBOM());
        } catch (Exception e) {
            log.info("Post BOM SQL Exception");
        }
        return false;
    }

    public boolean srcPostItemData() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostItemData());
        } catch (Exception e) {
            log.info("Post Item Data SQL Exception");
        }
        return false;
    }

    public boolean srcPostStdPriceData() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostStdPriceData());
        } catch (Exception e) {
            log.info("Post STD Price SQL Exception");
        }
        return false;
    }

    public boolean srcPostPMMaterial() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostPMMaterial());
        } catch (Exception e) {
            log.info("Post PM Material SQL Exception");
        }
        return false;
    }

    public boolean srcPostMouldData() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostMouldData());
        } catch (Exception e) {
            log.info("Post Mould Data SQL Exception");
        }
        return false;
    }

    public boolean srcPostItemTechnicalSize() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostItemTechnicalSize());
        } catch (Exception e) {
            log.info("Post Item Technical Size SQL Exception");
        }
        return false;
    }

    public boolean srcPostRMMaterial() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostRMMaterial());
        } catch (Exception e) {
            log.info("Post RM Material SQL Exception");
        }
        return false;
    }

    public boolean srcPostLinesData() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostLinesData());
        } catch (Exception e) {
            log.info("Post Lines SQL Exception");
        }
        return false;
    }

    public boolean srcPostSkidsData() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostSkidsData());
        } catch (Exception e) {
            log.info("Post Skids SQL Exception");
        }
        return false;
    }

    public boolean srcPostMAP() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostMAP());
        } catch (Exception e) {
            log.info("Post MAP SQL Exception");
        }
        return false;
    }

    public boolean srcPostMidNightStock() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostMidNightStock());
        } catch (Exception e) {
            log.info("Post Mid Night Stock SQL Exception");
        }
        return false;
    }

    public boolean srcPostReceipt() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostReceipt());
        } catch (Exception e) {
            log.info("Post Receipt SQL Exception");
        }
        return false;
    }

    public boolean srcPostOtherLocationStock() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostOtherLocationStock());
        } catch (Exception e) {
            log.info("Post Other Location Stock SQL Exception");
        }
        return false;
    }

    public boolean srcPostPhysicalRejection() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostPhysicalRejection());
        } catch (Exception e) {
            log.info("Post Physical Rejection SQL Exception");
        }
        return false;
    }

    public boolean srcPostOpenPOData() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostOpenPOData());
        } catch (Exception e) {
            log.info("Post OpenPO Data SQL Exception");
        }
        return false;
    }

    public boolean srcPostMESRejInStd() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostMESRejInStd());
        } catch (Exception e) {
            log.info("Post MES Rej InStd SQL Exception");
        }
        return false;
    }

    public boolean srcPostRMListGen() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostRMListGen());
        } catch (Exception e) {
            log.info("Post RM ListGen SQL Exception");
        }
        return false;
    }

    public boolean srcPostItem2() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostItem2());
        } catch (Exception e) {
            log.info("Post Item2 SQL Exception");
        }
        return false;
    }

    public boolean srcPostForecastMPS() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostForecastMPS());
        } catch (Exception e) {
            log.info("Post ForecastMPS SQL Exception");
        }
        return false;
    }

    public boolean srcPostPMOtif() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostPMOtif());
        } catch (Exception e) {
            log.info("Post PM OTIF SQL Exception");
        }
        return false;
    }

    public boolean srcPostItem2Data() {
        try {
            return StringUtil.isEqual(errorLogRepository.srcPostItem2Data());
        } catch (Exception e) {
            log.info("Item 2 Data SQL Exception");
        }
        return false;
    }
}
