package com.example.validation.util;

import com.example.validation.config.constants.MaterialCategoryConstants;
import com.example.validation.dto.RscIotPmSlobSummaryDTO;
import com.example.validation.dto.RscIotRmSlobSummaryDTO;
import com.example.validation.dto.RscMtPPheaderDTO;
import com.example.validation.dto.SlobInventoryQualityIndexDTO;
import com.example.validation.dto.slob.RscIotFGSlobSummaryDTO;
import com.example.validation.service.RscIotFGSlobSummaryService;
import com.example.validation.service.RscIotPmSlobSummaryService;
import com.example.validation.service.RscIotRmSlobSummaryService;
import com.example.validation.service.RscMtPpheaderService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
public class SlobInventoryQualityIndexUtil {
    private final RscMtPpheaderService rscMtPpheaderService;
    private final RscIotRmSlobSummaryService rscIotRmSlobSummaryService;
    private final RscIotPmSlobSummaryService rscIotPmSlobSummaryService;
    private final RscIotFGSlobSummaryService rscIotFGSlobSummaryService;

    public SlobInventoryQualityIndexUtil(RscMtPpheaderService rscMtPpheaderService, RscIotRmSlobSummaryService rscIotRmSlobSummaryService, RscIotPmSlobSummaryService rscIotPmSlobSummaryService, RscIotFGSlobSummaryService rscIotFGSlobSummaryService) {
        this.rscMtPpheaderService = rscMtPpheaderService;
        this.rscIotRmSlobSummaryService = rscIotRmSlobSummaryService;
        this.rscIotPmSlobSummaryService = rscIotPmSlobSummaryService;
        this.rscIotFGSlobSummaryService = rscIotFGSlobSummaryService;
    }

    public List<SlobInventoryQualityIndexDTO> getSlobInventoryQualityIndexList(String slobTag) {
        List<RscMtPPheaderDTO> pPheaderDTOList = rscMtPpheaderService.findAllPpHeaderListSortedBasedOnMpsDateAndMpsFirstCut();
        boolean isPlanValidated = true;
        int monthNo = 0;
        List<SlobInventoryQualityIndexDTO> slobInventoryQualityIndexDTOList = new ArrayList<>();
        if (Optional.ofNullable(pPheaderDTOList).isPresent()&&pPheaderDTOList.size()>0) {
            while (monthNo < pPheaderDTOList.size() && isPlanValidated) {
                slobInventoryQualityIndexDTOList.add(getslobInventoryQualityIndex(pPheaderDTOList.get(monthNo), slobTag));
                monthNo++;
            }
            isPlanValidated = false;
        }
        if (!isPlanValidated) {
            while (monthNo < 12) {
                SlobInventoryQualityIndexDTO slobInventoryQualityIndexDTO = new SlobInventoryQualityIndexDTO();
                slobInventoryQualityIndexDTO.setMonthValue(getSlobInventoryQualityMonthValue(pPheaderDTOList.get(0).getMpsDate().plusMonths(monthNo)));
                slobInventoryQualityIndexDTOList.add(slobInventoryQualityIndexDTO);
                monthNo++;
            }
        }
        return slobInventoryQualityIndexDTOList;
    }

    private String getSlobInventoryQualityMonthValue(LocalDate mpsDate) {
        return DateUtils.getMonthValue(mpsDate) + "-" + mpsDate.getYear() % 100;
    }

    private SlobInventoryQualityIndexDTO getslobInventoryQualityIndex(RscMtPPheaderDTO rscMtPPheaderDTO, String slobTag) {
        SlobInventoryQualityIndexDTO slobInventoryQualityIndexDTO = new SlobInventoryQualityIndexDTO();
        if (slobTag.equals(MaterialCategoryConstants.PM_SLOB_TAG)) {
            RscIotPmSlobSummaryDTO pmSlobSummaryDTO = rscIotPmSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(pmSlobSummaryDTO).isPresent()) {
                slobInventoryQualityIndexDTO.setInventoryQualityPercentage(pmSlobSummaryDTO.getStockQualityPercentage());
                slobInventoryQualityIndexDTO.setSlobPercentage(getSlobPercentage(slobInventoryQualityIndexDTO.getInventoryQualityPercentage()));
                slobInventoryQualityIndexDTO.setMonthValue(getSlobInventoryQualityIndexMonthValue(rscMtPPheaderDTO));
                slobInventoryQualityIndexDTO.setStockValue(getValueInMillions(pmSlobSummaryDTO.getTotalStockValue()));
                slobInventoryQualityIndexDTO.setSlobValue(getValueInMillions(pmSlobSummaryDTO.getTotalSlobValue()));
                slobInventoryQualityIndexDTO.setInventoryQualityValue(getValueInMillions(pmSlobSummaryDTO.getStockQuantity()));

            }
        } else if (slobTag.equals(MaterialCategoryConstants.RM_SLOB_TAG)) {
            RscIotRmSlobSummaryDTO rmSlobSummaryDTO = rscIotRmSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(rmSlobSummaryDTO).isPresent()) {
                slobInventoryQualityIndexDTO.setInventoryQualityPercentage(rmSlobSummaryDTO.getStockQualityPercentage());
                slobInventoryQualityIndexDTO.setSlobPercentage(getSlobPercentage(slobInventoryQualityIndexDTO.getInventoryQualityPercentage()));
                slobInventoryQualityIndexDTO.setMonthValue(getSlobInventoryQualityIndexMonthValue(rscMtPPheaderDTO));
                slobInventoryQualityIndexDTO.setStockValue(getValueInMillions(rmSlobSummaryDTO.getTotalStockValue()));
                slobInventoryQualityIndexDTO.setSlobValue(getValueInMillions(rmSlobSummaryDTO.getTotalSlobValue()));
                slobInventoryQualityIndexDTO.setInventoryQualityValue(getValueInMillions(rmSlobSummaryDTO.getStockQualityValue()));
            }
        } else if (slobTag.equals(MaterialCategoryConstants.FG_SLOB_TAG)) {
            RscIotFGSlobSummaryDTO fgSlobSummaryDTO = rscIotFGSlobSummaryService.findByRscMtPPheaderId(rscMtPPheaderDTO.getId());
            if (Optional.ofNullable(fgSlobSummaryDTO).isPresent()) {
                slobInventoryQualityIndexDTO.setInventoryQualityPercentage(fgSlobSummaryDTO.getStockQualityPercentage());
                slobInventoryQualityIndexDTO.setSlobPercentage(getSlobPercentage(slobInventoryQualityIndexDTO.getInventoryQualityPercentage()));
                slobInventoryQualityIndexDTO.setMonthValue(getSlobInventoryQualityIndexMonthValue(rscMtPPheaderDTO));
                slobInventoryQualityIndexDTO.setStockValue(getValueInMillions(fgSlobSummaryDTO.getTotalStockValue()));
                slobInventoryQualityIndexDTO.setSlobValue(getValueInMillions(fgSlobSummaryDTO.getTotalSlobValue()));
                slobInventoryQualityIndexDTO.setInventoryQualityValue(getValueInMillions(getInventoryQualityValueForFg(fgSlobSummaryDTO.getTotalStockValue(), fgSlobSummaryDTO.getStockQualityPercentage())));
            }
        }
        return slobInventoryQualityIndexDTO;
    }

    private Double getSlobPercentage(Double inventoryPercentage) {
        return DoubleUtils.getFormattedValue(100 - inventoryPercentage);
    }

    private Double getInventoryQualityValueForFg(Double totalStockValue, Double stockQualityPercentage) {
        return DoubleUtils.round(totalStockValue * stockQualityPercentage / 100);
    }


    private Double getValueInMillions(Double value) {
        return DoubleUtils.round(value / 1000000) ;
    }

    private String getSlobInventoryQualityIndexMonthValue(RscMtPPheaderDTO rscMtPPheaderDTO) {
        return DateUtils.getMonthValue(rscMtPPheaderDTO.getMpsDate()) + "-" + rscMtPPheaderDTO.getMpsDate().getYear() % 100;
    }
}
