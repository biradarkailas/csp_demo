import pandas as pd
import sys
 
def convertToCsv(path,directoryName,fileName,sheetNames):

    sheetNameList = sheetNames.split(", ")
    extension = ".csv"
    csvNameList = []
    directoryPath = " "
    if(len(directoryName) == 0):
        directoryPath = path
    else:
        directoryPath = path + directoryName

    for i in range(len(sheetNameList)):
        data_xls = pd.read_excel(directoryPath + fileName, sheetNameList[i], dtype=str, index_col=None)
        data_xls.to_csv(directoryPath + sheetNameList[i] + extension, encoding='utf-8', index=False, line_terminator='\r\n')
        csvNameList.append(directoryName + sheetNameList[i] + extension)
    result = ', '.join(csvNameList)
    print(result)

path = sys.argv[1]
directoryName = ""
fileName = sys.argv[2]
sheetNames = sys.argv[3]

convertToCsv(path,directoryName,fileName,sheetNames)
