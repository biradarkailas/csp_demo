USE `metnet_loreal_test_sv`;
DROP function IF EXISTS `fun_dash_replace_and_comma_remove`;

USE `metnet_loreal_test_sv`;
DROP function IF EXISTS `metnet_loreal_test_sv`.`fun_dash_replace_and_comma_remove`;
;

DELIMITER $$
USE `metnet_loreal_test_sv`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `fun_dash_replace_and_comma_remove`(column_name VARCHAR(20)) RETURNS varchar(500) CHARSET utf8mb4
BEGIN
	DECLARE str_replaced_field VARCHAR(500);
	SET str_replaced_field= REPLACE(REPLACE(column_name, CHAR(45), 0),
	CHAR(44),'');
	RETURN str_replaced_field;
END$$

DELIMITER ;
;