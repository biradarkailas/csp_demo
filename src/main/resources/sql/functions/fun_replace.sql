USE `metnet_loreal_test_sv`;
DROP function IF EXISTS `fun_replace`;

USE `metnet_loreal_test_sv`;
DROP function IF EXISTS `metnet_loreal_test_sv`.`fun_replace`;

DELIMITER $$
USE `metnet_loreal_test_sv`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `fun_replace`(column_name VARCHAR(50)) RETURNS varchar(500) CHARSET utf8mb4
BEGIN
DECLARE str_trimmed_field VARCHAR(500);
SET str_trimmed_field= REPLACE(REPLACE(REPLACE(REPLACE(TRIM(column_name), CHAR(9), ''),
CHAR(10),
''),
CHAR(11),
''),
CHAR(13),
'');

RETURN str_trimmed_field;
END$$

DELIMITER ;