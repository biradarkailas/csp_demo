CREATE TABLE test_table
(
    ID int NOT NULL PRIMARY KEY,
    file_name varchar(255),
    field_name varchar(255),
    error_description varchar(255),
    created_date varchar(255)
);