DELIMITER $$
USE `validation`$$
DROP PROCEDURE `export_error_log`$$
CREATE PROCEDURE export_error_log(IN file_path VARCHAR(100), OUT isSuccess varchar(10))
BEGIN
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
DECLARE str_path VARCHAR(100) DEFAULT NULL;
GET DIAGNOSTICS CONDITION 1 @SQLSTATE = RETURNED_SQLSTATE,
@errno = MYSQL_ERRNO, @TEXT = MESSAGE_TEXT;
SET @full_error = CONCAT("ERROR ", @errno, " (", @SQLSTATE, "): ", @TEXT);
CALL prc_error_log('prc_Remove_Spaces','',substr(@full_error,1,200));
SELECT SUBSTR(@full_error, 1, 300) ;
set isSuccess='false';
END;
	SET @str_path=file_path;
	SET @QUERY = CONCAT("
	SELECT 'Id', 'File Name', 'Field Name', 'Description', 'Created Date' FROM error_log
	UNION
	SELECT id, file_name, field_name, error_description, created_date
	FROM
	`error_log`
	INTO OUTFILE '",@str_path,"'
	FIELDS TERMINATED BY ','
	LINES TERMINATED BY '\\n'");
	PREPARE stmt FROM @QUERY;
	EXECUTE stmt;
	DEALLOCATE PREPARE stmt;
    set isSuccess='true';
    COMMIT;
END $$
DELIMITER ;