DELIMITER $$
USE `validation`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `prc_sanitize_data`(IN stmt_name varchar (64),OUT isSuccess varchar(5))
BEGIN
DECLARE EXIT HANDLER FOR SQLEXCEPTION
BEGIN
	GET DIAGNOSTICS CONDITION 1 @SQLSTATE = RETURNED_SQLSTATE,
	@errno = MYSQL_ERRNO, @TEXT = MESSAGE_TEXT;
	SET @full_error = CONCAT("ERROR ", @errno, " (", @SQLSTATE, "): ", @TEXT);

    CALL prc_error_log('prc_sanitize_data','',substr(@full_error,1,200));

    SELECT SUBSTR(@full_error, 1, 300);
	set isSuccess='false';
END;
set isSuccess='true';
CASE WHEN stmt_name='stg_bill_of_material' THEN

	Update stmt_name SET
		FG_CODE = FUN_REPLACE(FG_CODE),COMPONENT_CODE = FUN_REPLACE(COMPONENT_CODE),LINK_QUANTITY = FUN_REPLACE(LINK_QUANTITY),
		WASTE_PERCENT = FUN_REPLACE(WASTE_PERCENT),APPLICABLE_FROM = FUN_REPLACE(APPLICABLE_FROM),APPLICABLE_TO = FUN_REPLACE(APPLICABLE_TO);
	COMMIT;

	WHEN stmt_name='stg_mps' THEN
	Update stg_mps SET
		MPS_DIV=fun_replace(MPS_DIV),11_digit_CODE = FUN_REPLACE(11_digit_CODE),BPI_CODE = FUN_REPLACE(BPI_CODE),
		R_P = FUN_REPLACE(R_P),ABC = FUN_REPLACE(ABC),Cover_Norms = FUN_REPLACE(Cover_Norms),
		JUL_Op_Cover = FUN_REPLACE(JUL_Op_Cover),Aug_Op_Cover = FUN_REPLACE(Aug_Op_Cover),Backlog = FUN_REPLACE(Backlog),
		OP_M_PLUS_ONE = FUN_REPLACE(OP_M_PLUS_ONE),OP_M_Ideal = FUN_REPLACE(OP_M_Ideal),Nov_Plan = FUN_REPLACE(Nov_Plan),
		M1 = FUN_REPLACE(M1),M2 = FUN_REPLACE(M2),M3 = FUN_REPLACE(M3),M4 = FUN_REPLACE(M4),
		M5 = FUN_REPLACE(M5),M6 = FUN_REPLACE(M6),M7 = FUN_REPLACE(M7),M8 = FUN_REPLACE(M8),M9 = FUN_REPLACE(M9),
		M10 = FUN_REPLACE(M10),M11 = FUN_REPLACE(M11),M12 = FUN_REPLACE(M12),
		Total = FUN_REPLACE(Total),Inhouse_subcon = FUN_REPLACE(Inhouse_subcon),Tech_Family = FUN_REPLACE(Tech_Family),
		MTD_production = FUN_REPLACE(MTD_production),
		M1 = fun_dash_replace_and_comma_remove(M1),M2 = fun_dash_replace_and_comma_remove(M2),M3 = fun_dash_replace_and_comma_remove(M3),
        M4 = fun_dash_replace_and_comma_remove(M4),M5 = fun_dash_replace_and_comma_remove(M5),M6 = fun_dash_replace_and_comma_remove(M6),
        M7 = fun_dash_replace_and_comma_remove(M7),M8 = fun_dash_replace_and_comma_remove(M8),M9 = fun_dash_replace_and_comma_remove(M9),
        M10 = fun_dash_replace_and_comma_remove(M10),M11 = fun_dash_replace_and_comma_remove(M11),M12 = fun_dash_replace_and_comma_remove(M12),
		Total = fun_dash_replace_and_comma_remove(Total);
		/*Line = FUN_REPLACE(Line);*/
	COMMIT;

	WHEN stmt_name='stg_in_std' THEN
	UPDATE stg_in_std SET
		 Country=fun_replace(Country),Doc_No = FUN_REPLACE(Doc_No),RM_CODE = FUN_REPLACE(RM_CODE), lot = FUN_REPLACE(lot),
		 location = FUN_REPLACE(location),std_Status = FUN_REPLACE(std_Status),Quantity = FUN_REPLACE(Quantity);
	COMMIT;

	WHEN stmt_name='stg_mid_night_stock' THEN
	UPDATE stg_mid_night_stock SET
		 RM_CODE=fun_replace(RM_CODE),lot=fun_replace(lot),location=fun_replace(location),MS_status=fun_replace(MS_status),
		 Quantity=fun_replace(Quantity),Document_No=fun_replace(Document_No),Country=fun_replace(Country);
	COMMIT;

	WHEN stmt_name='stg_month_end_stock' THEN
	UPDATE stg_month_end_stock SET
		 COMPONENT_CODE=fun_replace(COMPONENT_CODE),Lot_No=fun_replace(Lot_No),Location=fun_replace(Location),Qty=fun_replace(Qty),
		 stock_Status=fun_replace(stock_Status),Document_No=fun_replace(Document_No),Site=fun_replace(Site);
	COMMIT;

	WHEN stmt_name='stg_mould_master' THEN
	UPDATE stg_mould_master SET
		 No_Of_Cavity=fun_replace(No_Of_Cavity),Per_Cavity=fun_replace(Per_Cavity),Total_Capacity=fun_replace(Total_Capacity),
		 ACTIVE=fun_replace(ACTIVE),Capacity=fun_replace(Capacity);
	COMMIT;

	WHEN stmt_name='stg_mould_master' THEN
	UPDATE stg_mould_master SET
		 No_Of_Cavity=fun_replace(No_Of_Cavity),Per_Cavity=fun_replace(Per_Cavity),Total_Capacity=fun_replace(Total_Capacity),
		 ACTIVE=fun_replace(ACTIVE),Capacity=fun_replace(Capacity);
	COMMIT;

	WHEN stmt_name='stg_open_po' THEN
	UPDATE stg_open_po SET
		 PO_NO=fun_replace(PO_NO),COLUMN_C=fun_replace(COLUMN_C),PO_DATE=fun_replace(PO_DATE),RM_CODE=fun_replace(RM_CODE),
		 ORD_QTY=fun_replace(ORD_QTY),RCVD_QTY=fun_replace(RCVD_QTY),Delivery_Date=fun_replace(Delivery_Date),
		 PAYMENT_TERM_DAYS=fun_replace(PAYMENT_TERM_DAYS),Due_Date=fun_replace(Due_Date),RATE=fun_replace(RATE),
		 PO_AMOUNT=fun_replace(PO_AMOUNT),CURRENCY=fun_replace(CURRENCY);
	COMMIT;

	WHEN stmt_name='stg_pm_material_master' THEN
	UPDATE stg_pm_material_master SET
		COMPONENT_CODE=fun_replace(COMPONENT_CODE),Cover=fun_replace(Cover),MOQ=fun_replace(MOQ),
		Technical_series=fun_replace(Technical_series),Division=fun_replace(Division),Safety_Stock=fun_replace(Safety_Stock),
		A_Class_Relevance=fun_replace(A_Class_Relevance);
	COMMIT;

	WHEN stmt_name='stg_pm_receipts_till_date' THEN
	UPDATE stg_pm_receipts_till_date SET
		 Receipt_NO=fun_replace(Receipt_NO),RECP_Date=fun_replace(RECP_Date),Supp_No=fun_replace(Supp_No),
		 COMPONENT_CODE=fun_replace(COMPONENT_CODE),Receipt_Qty=fun_replace(Receipt_Qty),Order_No=fun_replace(Order_No),
		 Pkg_Slip_No=fun_replace(Pkg_Slip_No),Supp_Lot=fun_replace(Supp_Lot),Lot_Number=fun_replace(Lot_Number);
	COMMIT;

	WHEN stmt_name='stg_rm_material_master' THEN
	UPDATE stg_rm_material_master SET
		 COMPONENT_CODE=fun_replace(COMPONENT_CODE),Local_Imported=fun_replace(Local_Imported),Cover=fun_replace(Cover),
		 Pack_Size=fun_replace(Pack_Size),MOQ=fun_replace(MOQ),Transportation_Mode=fun_replace(Transportation_Mode),
		 sailing_day=fun_replace(sailing_day),Safety_Stock=fun_replace(Safety_Stock);
	COMMIT;

	WHEN stmt_name='stg_rm_receipts_till_date' THEN
	UPDATE stg_rm_receipts_till_date SET
		 Receipt_NO=fun_replace(Receipt_NO),RECP_Date=fun_replace(RECP_Date),Supp_No=fun_replace(Supp_No),
		 COMPONENT_CODE=fun_replace(COMPONENT_CODE),Receipt_Qty=fun_replace(Receipt_Qty),Order_No=fun_replace(Order_No),
		 Pkg_Slip_No=fun_replace(Pkg_Slip_No),Supp_Lot=fun_replace(Supp_Lot), Lot_Number=fun_replace(Lot_Number);
	 COMMIT;

	WHEN stmt_name='stg_ss_master' THEN
	UPDATE stg_ss_master SET
		 COMPONENT_CODE=fun_replace(COMPONENT_CODE),Safety_Stock=fun_replace(Safety_Stock),A_Class_Relevance=fun_replace(A_Class_Relevance),
		 Standard_Safety_Stock=fun_replace(Standard_Safety_Stock),SS_STATUS=fun_replace(SS_STATUS);
	COMMIT;

	WHEN stmt_name='stg_technical_size' THEN
	UPDATE stg_technical_size SET
		 COMPONENT_CODE=fun_replace(COMPONENT_CODE),B=fun_replace(B),C=fun_replace(C),D=fun_replace(D),
		 E=fun_replace(E),F=fun_replace(F),G=fun_replace(G),H=fun_replace(H);
	COMMIT;
 END case;
END$$

DELIMITER ;