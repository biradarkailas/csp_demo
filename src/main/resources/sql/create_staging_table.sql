-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: validation
-- ------------------------------------------------------
-- Server version	8.0.16
USE `validation`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `stg_bill_of_material`
--

DROP TABLE IF EXISTS `stg_bill_of_material`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_bill_of_material` (
  `FG_CODE` varchar(20) DEFAULT NULL,
  `COLUMN_B` bigint(20) DEFAULT NULL,
  `COLUMN_C` bigint(20) DEFAULT NULL,
  `COMPONENT_CODE` varchar(20) DEFAULT NULL,
  `LINK_QUANTITY` varchar(50) DEFAULT NULL,
  `WASTE_PERCENT` varchar(50) DEFAULT NULL,
  `APPLICABLE_FROM` varchar(20) DEFAULT NULL,
  `APPLICABLE_TO` varchar(20) DEFAULT NULL,
  `ACTIVE` tinyint(1) DEFAULT '1',
  `CREATED_BY` varchar(20) DEFAULT 'SYS',
  `CREATED_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_in_std`
--

DROP TABLE IF EXISTS `stg_in_std`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_in_std` (
  `Country` varchar(10) DEFAULT NULL,
  `Doc_No` varchar(40) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `RM_CODE` varchar(20) DEFAULT NULL,
  `lot` varchar(50) DEFAULT NULL,
  `location` varchar(40) DEFAULT NULL,
  `std_Status` varchar(40) DEFAULT NULL,
  `Quantity` double DEFAULT NULL,
  `Column_I` varchar(40) DEFAULT NULL,
  `Column_J` varchar(40) DEFAULT NULL,
  `Column_K` varchar(40) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_mid_night_stock`
--

DROP TABLE IF EXISTS `stg_mid_night_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_mid_night_stock` (
  `RM_CODE` varchar(20) DEFAULT NULL,
  `lot` varchar(50) DEFAULT NULL,
  `location` varchar(50) DEFAULT NULL,
  `MS_status` varchar(10) DEFAULT NULL,
  `Quantity` double DEFAULT NULL,
  `Document_No` varchar(50) DEFAULT NULL,
  `SnapShot_Date` varchar(25) DEFAULT NULL,
  `Country` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_month_end_stock`
--

DROP TABLE IF EXISTS `stg_month_end_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_month_end_stock` (
  `COMPONENT_CODE` varchar(100) DEFAULT NULL,
  `Lot_No` varchar(300) DEFAULT NULL,
  `Location` varchar(500) DEFAULT NULL,
  `stock_Status` varchar(500) DEFAULT NULL,
  `Qty` int(10) DEFAULT NULL,
  `Document_No` varchar(100) DEFAULT NULL,
  `SnapShot_Date` varchar(100) DEFAULT NULL,
  `Site` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_mould_master`
--

DROP TABLE IF EXISTS `stg_mould_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_mould_master` (
  `Mold` varchar(100) DEFAULT NULL,
  `Child_Mold` varchar(100) DEFAULT NULL,
  `No_Of_Cavity` varchar(10) DEFAULT NULL,
  `Per_Cavity` varchar(10) DEFAULT NULL,
  `Total_Capacity` varchar(10) DEFAULT NULL,
  `ACTIVE` varchar(10) DEFAULT NULL,
  `Capacity` varchar(10) DEFAULT NULL,
  `Supplier` varchar(500) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_mps`
--

DROP TABLE IF EXISTS `stg_mps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_mps` (
  `MPS_DIV` varchar(20) DEFAULT NULL,
  `MPS_CATEGORY` varchar(250) DEFAULT NULL,
  `Signature` varchar(500) DEFAULT NULL,
  `Brand` varchar(300) DEFAULT NULL,
  `11_digit_CODE` varchar(20) DEFAULT NULL,
  `BPI_CODE` varchar(20) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `R_P` varchar(5) DEFAULT NULL,
  `ABC` varchar(100) DEFAULT NULL,
  `Cover_Norms` varchar(100) DEFAULT NULL,
  `JUL_Op_Cover` varchar(100) DEFAULT NULL,
  `Aug_Op_Cover` varchar(100) DEFAULT NULL,
  `Backlog` varchar(100) DEFAULT NULL,
  `OP_M_PLUS_ONE` varchar(20) DEFAULT NULL,
  `OP_M_Ideal` varchar(20) DEFAULT NULL,
  `Nov_Plan` varchar(20) DEFAULT NULL,
  `M1` varchar(100) DEFAULT NULL,
  `M2` varchar(100) DEFAULT NULL,
  `M3` varchar(100) DEFAULT NULL,
  `M4` varchar(100) DEFAULT NULL,
  `M5` varchar(100) DEFAULT NULL,
  `M6` varchar(100) DEFAULT NULL,
  `M7` varchar(100) DEFAULT NULL,
  `M8` varchar(100) DEFAULT NULL,
  `M9` varchar(100) DEFAULT NULL,
  `M10` varchar(100) DEFAULT NULL,
  `M11` varchar(100) DEFAULT NULL,
  `M12` varchar(100) DEFAULT NULL,
  `Total` varchar(100) DEFAULT NULL,
  `Location` varchar(50) DEFAULT NULL,
  `Inhouse_subcon` varchar(50) DEFAULT NULL,
  `Tech_Family` varchar(50) DEFAULT NULL,
  `Line` varchar(250) DEFAULT NULL,
  `MTD_production` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_open_po`
--

DROP TABLE IF EXISTS `stg_open_po`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_open_po` (
  `SUPPLIER` varchar(300) DEFAULT NULL,
  `PO_NO` varchar(50) DEFAULT NULL,
  `COLUMN_C` double DEFAULT NULL,
  `PO_DATE` varchar(20) DEFAULT NULL,
  `RM_CODE` varchar(20) DEFAULT NULL,
  `DESCRIPTION` varchar(300) DEFAULT NULL,
  `ORD_QTY` double DEFAULT NULL,
  `RCVD_QTY` double DEFAULT NULL,
  `Delivery_Date` varchar(20) DEFAULT NULL,
  `MODE` varchar(50) DEFAULT NULL,
  `PAYMENT_TERM_DAYS` int(10) DEFAULT NULL,
  `Due_Date` varchar(20) DEFAULT NULL,
  `RATE` double DEFAULT NULL,
  `PO_AMOUNT` double DEFAULT NULL,
  `CURRENCY` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_pm_material_master`
--

DROP TABLE IF EXISTS `stg_pm_material_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_pm_material_master` (
  `COMPONENT_CODE` varchar(100) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Supplier_Desc` varchar(500) DEFAULT NULL,
  `Category` varchar(500) DEFAULT NULL,
  `Cover` int(10) DEFAULT NULL,
  `MOQ` double DEFAULT NULL,
  `Technical_series` varchar(100) DEFAULT NULL,
  `Buffer` double DEFAULT NULL,
  `Division` varchar(20) DEFAULT NULL,
  `Mould` varchar(300) DEFAULT NULL,
  `Safety_Stock` double DEFAULT NULL,
  `A_Class_Relevance` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_pm_receipts_till_date`
--

DROP TABLE IF EXISTS `stg_pm_receipts_till_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_pm_receipts_till_date` (
  `Receipt_NO` varchar(100) DEFAULT NULL,
  `RECP_Date` varchar(20) DEFAULT NULL,
  `Supp_No` varchar(20) DEFAULT NULL,
  `Supplier_Desc` varchar(500) DEFAULT NULL,
  `COMPONENT_CODE` varchar(100) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Receipt_Qty` varchar(30) DEFAULT NULL,
  `Order_No` varchar(100) DEFAULT NULL,
  `Pkg_Slip_No` varchar(100) DEFAULT NULL,
  `Supp_Lot` varchar(100) DEFAULT NULL,
  `Lot_Number` varchar(100) DEFAULT NULL,
  `column_L` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_rm_material_master`
--

DROP TABLE IF EXISTS `stg_rm_material_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_rm_material_master` (
  `COMPONENT_CODE` varchar(100) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Supplier_Desc` varchar(500) DEFAULT NULL,
  `Local_Imported` varchar(10) DEFAULT NULL,
  `Cover` int(10) DEFAULT NULL,
  `Pack_Size` double DEFAULT NULL,
  `MOQ` double DEFAULT NULL,
  `Transportation_Mode` varchar(10) DEFAULT NULL,
  `sailing_day` int(10) DEFAULT NULL,
  `Safety_Stock` double DEFAULT NULL,
  `column_k` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_rm_receipts_till_date`
--

DROP TABLE IF EXISTS `stg_rm_receipts_till_date`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_rm_receipts_till_date` (
  `Receipt_NO` varchar(100) DEFAULT NULL,
  `RECP_Date` varchar(20) DEFAULT NULL,
  `Supp_No` varchar(20) DEFAULT NULL,
  `Supplier_Desc` varchar(500) DEFAULT NULL,
  `COMPONENT_CODE` varchar(100) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Receipt_Qty` varchar(30) DEFAULT NULL,
  `Order_No` varchar(100) DEFAULT NULL,
  `Pkg_Slip_No` varchar(100) DEFAULT NULL,
  `Supp_Lot` varchar(100) DEFAULT NULL,
  `Lot_Number` varchar(100) DEFAULT NULL,
  `column_L` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_ss_master`
--

DROP TABLE IF EXISTS `stg_ss_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_ss_master` (
  `COMPONENT_CODE` varchar(100) DEFAULT NULL,
  `Description` varchar(300) DEFAULT NULL,
  `Supplier` varchar(500) DEFAULT NULL,
  `Safety_Stock` varchar(10) DEFAULT NULL,
  `A_Class_Relevance` varchar(10) DEFAULT NULL,
  `Standard_Safety_Stock` bigint(10) DEFAULT NULL,
  `SS_STATUS` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_technical_size`
--

DROP TABLE IF EXISTS `stg_technical_size`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_technical_size` (
  `COMPONENT_CODE` varchar(20) DEFAULT NULL,
  `B` double DEFAULT NULL,
  `C` double DEFAULT NULL,
  `D` double DEFAULT NULL,
  `E` double DEFAULT NULL,
  `F` double DEFAULT NULL,
  `G` double DEFAULT NULL,
  `H` double DEFAULT NULL,
  `CREATED_BY` varchar(20) DEFAULT 'SYS',
  `CREATED_DATE` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `stg_validated_mps`
--

DROP TABLE IF EXISTS `stg_validated_mps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `stg_validated_mps` (
  `ID` bigint(20) NOT NULL AUTO_INCREMENT,
  `BPI_CODE` varchar(20) DEFAULT NULL,
  `Description` varchar(500) DEFAULT NULL,
  `M1` varchar(100) DEFAULT NULL,
  `M2` varchar(100) DEFAULT NULL,
  `M3` varchar(100) DEFAULT NULL,
  `M4` varchar(100) DEFAULT NULL,
  `M5` varchar(100) DEFAULT NULL,
  `M6` varchar(100) DEFAULT NULL,
  `M7` varchar(100) DEFAULT NULL,
  `M8` varchar(100) DEFAULT NULL,
  `M9` varchar(100) DEFAULT NULL,
  `M10` varchar(100) DEFAULT NULL,
  `M11` varchar(100) DEFAULT NULL,
  `M12` varchar(100) DEFAULT NULL,
  `Inhouse_subcon` varchar(50) DEFAULT NULL,
  `Line` varchar(250) DEFAULT NULL,
  `priority` tinyint(1) DEFAULT '1',
  `active` tinyint(1) DEFAULT '1',
  `CREATED_BY` varchar(150) DEFAULT NULL,
  `CREATED_DATE` datetime DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-07 12:29:57
