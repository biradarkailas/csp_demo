-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: validation
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `sap_table_name`
--

DROP TABLE IF EXISTS `sap_table_name`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `sap_table_name` (
  `id` bigint NOT NULL AUTO_INCREMENT,
  `table_name` varchar(255) NOT NULL,
  `file_name` varchar(255) NOT NULL,
  `is_active` bit(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sap_table_name`
--

LOCK TABLES `sap_table_name` WRITE;
/*!40000 ALTER TABLE `sap_table_name` DISABLE KEYS */;
INSERT INTO `sap_table_name` VALUES (1,'stg_bill_of_material','BOM.csv',_binary ''),(2,'stg_in_std','in-stkd.csv',_binary ''),(3,'stg_mid_night_stock','mid_night_stk_main.csv',_binary ''),(4,'stg_month_end_stock','OPEN_PO.csv',_binary ''),(5,'stg_mould_master','Mold_Master.csv',_binary ''),(6,'stg_mps','PM_MPS.csv',_binary ''),(7,'stg_pm_material_master','PM_MATERIAL_MASTER.CSV',_binary ''),(8,'stg_pm_receipts_till_date','Receipt_till_date.csv',_binary ''),(9,'stg_rm_material_master','RM_MATERIAL_MASTER.CSV',_binary ''),(10,'stg_rm_receipts_till_date','RM_in-receipt.CSV',_binary ''),(11,'stg_ss_master','SS_Master.csv',_binary ''),(12,'stg_technical_size','BYSITE.csv',_binary '');
/*!40000 ALTER TABLE `sap_table_name` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-01-07 13:04:40
